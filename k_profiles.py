#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])

def norm(array):
    m = max(array)
    a = [0]*len(array)
    for i in range(len(array)):
        a[i] = array[i]/m
    return a

def main():
    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nixpath = 'data/Proton2Dx'
    niypath = 'data/Proton2Dy'
    nizpath = 'data/Proton2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nphxpath = 'data/Photon2Dx'
    nphypath = 'data/Photon2Dy'
    nphzpath = 'data/Photon2Dz'
    picspath = 'pics'
    config = utils.get_config("ParsedInput.txt")
    wl = float(config['Wavelength'])
    Xmax = float(config['X_Max'])/wl #mkm to wavelength
    Xmin = float(config['X_Min'])/wl #mkm to wavelength
    Ymax = float(config['Y_Max'])/wl #mkm to wavelength
    Ymin = float(config['Y_Min'])/wl #mkm to wavelength

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    delta = 1

    num = len(sys.argv)
    step = float(config['TimeStep'])*1e15*float(config['BOIterationPass'])
    omega = float(config['Omega'])
    T = 2 * utils.pi/omega
    nt = int(T*1e15/step)
    print nt
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(expath) - nt
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(expath) - nt
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'

    dv = 2.*dx*dy*dz
    axis1 = create_axis(nx, (Xmax-Xmin)/nx, Xmin)
#    i = int(sys.argv[1])
    
    
    for i in range(nmin,nmax):
        fig = plt.figure(num=None, figsize=(20, 10), dpi=256)
        picname = picspath + '/' + "k_pr_%d.png" % (i,)
        print picname
    

        name = expath + '/' + "%06d.txt" % (i,)
        fielde = read_field(name,nx,ny)
        name = nexpath + '/' + "%06d.txt" % (i,)
        fieldn = read_field(name,nx,ny, 1./dv)
        name = npxpath + '/' + "%06d.txt" % (i,)
        fieldp = read_field(name,nx,ny, 1./dv)
        name = nphxpath + '/' + "%06d.txt" % (i,)
        fieldph = read_field(name,nx,ny, 1./dv)
        
        profe = fielde[:][ny/2]
        profn = fieldn[:][ny/2]
        profp = fieldp[:][ny/2]
#        profph = norm(fieldph[:][ny/2])
        
#        for k in range(len(fieldn)):
#            profn[k] /= dv
#            profp[k] /= dv

#        profe = fielde[:][ny/2]
#        profb = fieldb[:][ny/2]
        ax1 = fig.add_subplot(1,1,1)
        el, = ax1.plot(axis1,profn, 'g') 
        pos, = ax1.plot(axis1,profp, 'b')
        #    ax1.plot(axis1,profph)
        ax2 = ax1.twinx()
        ez, = ax2.plot(axis1,profe, 'r')
        ax1.set_xlim([-2,2])
        ax2.set_xlim([-2,2])
        plt.legend([el, pos, ez], ['el', 'pos', 'Ez'], loc = "upper right")
        plt.savefig(picname)
        plt.close()
        #plt.show()
    
if __name__ == '__main__':
    main()
