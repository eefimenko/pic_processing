#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import numpy as np
import os
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

def read_dn(filename):
    f = open(filename, 'r')
    ax_ = []
    ph_ = []
    el_ = []
    pos_ = []
    for line in f:
        tmp = line.split()
        ax_.append(float(tmp[0]))
        ph_.append(float(tmp[1]))
        el_.append(float(tmp[2]))
        pos_.append(float(tmp[3]))
    f.close()
    return np.array(ax_), np.array(ph_), np.array(el_), np.array(pos_)

def main():
    mp.rcParams.update({'font.size': 12})
    
    picspath = '.'
    #dirs = ['90pw', '70pw', '50pw', '20pw']
    #legends = ['90 PW', '70 PW', '50 PW', '20 PW']
    dirs = ['50pw', '40pw', '30pw', '20pw']
    legends = ['50 PW', '40 PW', '30 PW', '20 PW']
    fontsize = 12
    
    fig1 = plt.figure(num=None, figsize = (5,3.5))
    ax1 = fig1.add_subplot(1,1,1)
    fig2 = plt.figure(num=None, figsize = (5,3.5))
    ax2 = fig2.add_subplot(1,1,1)
    fig3 = plt.figure(num=None, figsize = (5,3.5))
    ax3 = fig3.add_subplot(1,1,1)
    fig4 = plt.figure(num=None, figsize = (5,3.5))
    ax4 = fig4.add_subplot(1,1,1)
    
    ax1.set_yscale('log')
    ax2.set_yscale('log')
    ax3.set_yscale('log')
    ax4.set_yscale('log')
    #ax1.set_ylim([1e-8, 2e-3])
    #ax2.set_ylim([1e-8, 2e-3])
    
    ax1.set_xlim([0, math.pi])
    ax2.set_xlim([0, math.pi])
    ax3.set_xlim([0, math.pi])
    ax4.set_xlim([0, math.pi])
    window = 11
    order = 7

    #axins = zoomed_inset_axes(ax1, 2.5, loc= 'upper center')  # zoom = 6
    #axins.set_yscale('log')
    #axins.set_xlim([0.75, 1.5])
    #axins.set_ylim([3e-5, 3e-4])
    
    #mark_inset(ax1, axins, loc1=2, loc2=3, fc="none", ec="0.5")

    #axins1 = zoomed_inset_axes(ax2, 2.5, loc= 'upper right')  # zoom = 6
    #axins1.set_yscale('log')
    #axins1.set_xlim([0.75, 1.5])
    #axins1.set_ylim([8e-4, 9e-3])
    
    #mark_inset(ax2, axins1, loc1=1, loc2=3, fc="none", ec="0.5")
    number = 1
    for d, label in zip(dirs, legends):
        ax_, ph_, el_, pos_ = read_dn(d + '/qe_dn.txt')
        config = utils.get_config(d + '/ParsedInput.txt')
        number = int(config['FieldGenerator.Number'])
        n_cr = float(config['n_cr'])
        #ne_ = float(config['Ne'])
        Emax = float(config['QEDstatistics.Emax'])
        Emin = float(config['QEDstatistics.Emin'])
        N_E = int(config['QEDstatistics.OutputN_E'])
        de = (Emax - Emin)/N_E/1.6e-12/1e9 # erg -> eV -> GeV
        n = sum(ph_)
        sin_a = np.sin(ax_ + 0.5*(ax_[1] - ax_[0]))
        #ax1.plot(ax_, utils.savitzky_golay(np.array(ph_)/n, window, order), linewidth = 0.9, label = label)
        ax1.plot(ax_, np.array(ph_)/n, linewidth = 0.7, label = label)
        ax3.plot(ax_, np.divide(np.array(ph_)/n, sin_a), linewidth = 0.7, label = label)
        #axins.plot(ax_, np.array(ph_)/n, linewidth = 0.7, label = label)
        n = sum(el_)
        #ax2.plot(ax_, utils.savitzky_golay(np.array(el_)/n, window, order), linewidth = 0.9, label = label)
        ax2.plot(ax_, np.array(el_)/n, linewidth = 0.7, label = label)
        ax4.plot(ax_, np.divide(np.array(el_)/n, sin_a), linewidth = 0.7, label = label)
        #axins1.plot(ax_, np.array(el_)/n, linewidth = 0.7, label = label)
        #n = utils.full_number(pos_,de,N_E)
        #ax3.plot(ax_, utils.savitzky_golay(np.array(pos_)/n, window, order), linewidth = 0.9, label = label)
        #ax3.plot(ax_, np.array(pos_)/n, linewidth = 0.9, label = label)

    ax1.legend(loc = 'lower center', ncol=2, fontsize = fontsize-2, frameon = False)
    ax2.legend(loc = 'lower center', ncol=2, fontsize = fontsize-2, frameon = False)
    ax3.legend(loc = 'upper center', fontsize = fontsize-2, frameon = False)
    ax4.legend(loc = 'upper center', fontsize = fontsize-2, frameon = False)
    ax1.set_xlabel('$\\theta$')
    ax2.set_xlabel('$\\theta$')
    ax3.set_xlabel('$\\theta$')
    ax4.set_xlabel('$\\theta$')
    ax1.set_xticks([0, math.pi/4, math.pi/2, 3.*math.pi/4., math.pi])
    ax1.set_xticklabels(['0', '$\pi/4$', '$\pi/2$', '$3\pi/4$', '$\pi$'])
    ax2.set_xticks([0, math.pi/4, math.pi/2, 3.*math.pi/4., math.pi])
    ax2.set_xticklabels(['0', '$\pi/4$', '$\pi/2$', '$3\pi/4$', '$\pi$'])
    ax3.set_xticks([0, math.pi/4, math.pi/2, 3.*math.pi/4., math.pi])
    ax3.set_xticklabels(['0', '$\pi/4$', '$\pi/2$', '$3\pi/4$', '$\pi$'])
    ax4.set_xticks([0, math.pi/4, math.pi/2, 3.*math.pi/4., math.pi])
    ax4.set_xticklabels(['0', '$\pi/4$', '$\pi/2$', '$3\pi/4$', '$\pi$'])
    ax1.set_ylabel('$\partial W/\partial\\theta$')
    ax2.set_ylabel('$\partial W/\partial\\theta$')
    ax3.set_ylabel('$\\frac{1}{\sin \\theta}\\frac{\partial W}{\partial\\theta}$')
    ax4.set_ylabel('$\\frac{1}{\sin \\theta}\\frac{\partial W}{\partial\\theta}$')
    ax1.text(-0.5, ax1.get_ylim()[1], '(a)')
    ax2.text(-0.5, ax2.get_ylim()[1], '(b)')
    ax3.text(-0.5, ax3.get_ylim()[1], '(a)')
    ax4.text(-0.5, ax4.get_ylim()[1], '(b)')
    #axins.set_xticks([])
    #axins.set_yticks([])
    #axins.set_yticklabels([])
    #axins1.set_xticks([])
    #axins1.set_yticks([])
    #axins1.set_yticklabels([])
    
    name1 = 'ph_dn'
    name2 = 'el_dn'
    name3 = 'ph_dn_sin'
    name4 = 'el_dn_sin'
    picname1 = os.path.join(picspath, name1 + "_%d.png" % number)
    picname2 = os.path.join(picspath, name2 + "_%d.png" % number)
    picname3 = os.path.join(picspath, name3 + "_%d.png" % number)
    picname4 = os.path.join(picspath, name4 + "_%d.png" % number)
    
    fig1.tight_layout()
    fig2.tight_layout()
    fig3.tight_layout()
    fig4.tight_layout()
    fig1.savefig(picname1, dpi = 256)
    fig2.savefig(picname2, dpi = 256)
    fig3.savefig(picname3, dpi = 256)
    fig4.savefig(picname4, dpi = 256)
      
#    plt.show()

if __name__ == '__main__':
    main()
