# -*- coding: utf-8 -*-
"""
Created on Thu Dec 24 14:43:49 2020

@author: Алексей
"""
#import sys
import os
import math
import zipfile
import numpy as np
import re
#import matplotlib.pyplot as plt
import sys

def ReadZipTxt(pathZip,fileInZip,nx,ny=-1):    
    archive = zipfile.ZipFile(pathZip,'r')
    file = archive.read(fileInZip)
    archive.close()
    data=file.decode('utf-8').split()
    return np.array(list(map(float,data))).reshape(ny,nx)


def InputDictionary(pathToInput,dictInput):
    regString=r"""Setting (?:variable|string)\S*\s*(?P<nameVar>\S*)\s*=\s*(?P<valueVar>\S+)\s*"""
    regExpr=re.compile(regString)
    inpFile=open(pathToInput,'r')
    
    for line in inpFile:
        foundVar=regExpr.search(line)
        if foundVar:
            curDict=foundVar.groupdict()
            dictInput[curDict['nameVar']]=curDict['valueVar']
    inpFile.close()
    return 0

def Add1DPlot(data,xax,xrangeFrame,ax,color='k',yrangeFrame=None,linestyle='-',linewidth=1,xscale='linear',yscale='linear',marker=None,fillstyle='full',markersize=4):
    ax.set_xlim(xrangeFrame[0],xrangeFrame[1])
    if (yrangeFrame):
        ax.set_ylim(yrangeFrame[0],yrangeFrame[1])
    else:
        ax.set_ylim(data.min(),data.max())
    ax.set_xscale(xscale)
    ax.set_yscale(yscale)
    
    if (marker):
        lines, =ax.plot(xax,data,color=color,linestyle=linestyle,linewidth=linewidth,marker=marker,fillstyle=fillstyle,markersize=markersize)
    else:
        lines, =ax.plot(xax,data,color=color,linestyle=linestyle,linewidth=linewidth)
    return lines

numAnalysedPeriods=2
regime = "zip" # "txt"
if __name__ == "__main__":
    try:
        numAnalysedPeriods=int(sys.argv[2])
        regime=sys.argv[3]
    except:
        print("default parameters")
print("Parameters of work")
print("numAnalysedPeriods=",numAnalysedPeriods)
print("regime=",regime)

labels=["Ideal4","Amp","Shift","Delay"]
simulations=["../../InputUnified_4_30_2020-12-29_23-49-05_Ideal4",
             "../../DifA/InputUnified_4_30_2020-12-23_15-12-56",
             "../../DifShift/InputUnified_4_30_2020-12-25_00-34-31",
             "../../DifDelay/InputUnified_4_30_2020-12-29_15-55-44"]
print(simulations)
En1pEl=[]
En1pPos=[]
En1pPh=[]
### calculation of growth rate in each simulation
for sim in simulations:
    print("Analysis of ",sim)
    ###read input file
    path=sim+os.path.sep
    zipfold=path+"BasicOutput"+os.path.sep+"data.zip"
    pathtxt=sim+os.path.sep+"BasicOutput"+os.path.sep+"data"+os.path.sep
    inputDict={}
    InputDictionary(path+"ParsedInput.txt",inputDict)
    pwr=float(inputDict["PeakPowerPW"])
    w=float(inputDict["Omega"])
    dt=float(inputDict["TimeStep"])
    iterBetwSaves=int(inputDict["BOIterationPass"])
    iterst=int(inputDict["BOIterationStart"])
    nx=int(inputDict["E2y.SetMatrixSize_0"])
    nz=int(inputDict["E2y.SetMatrixSize_1"])
    ngam=int(inputDict["ElectronEn.SetMatrixSize_0"])
    gmin=float(inputDict["ElectronEn.SetBounds_0"])
    gmax=float(inputDict["ElectronEn.SetBounds_1"])
    numst=int(iterst/iterBetwSaves)
    period=2*math.pi/w
    numSavesperPeriod=int(period/(dt*iterBetwSaves)+0.5)

    
    ###calculation of number of saved files
    if regime=="zip":
        zipfold=path+"BasicOutput"+os.path.sep+"data.zip"
        zipFile=zipfile.ZipFile(zipfold,"r")
        names=[]
        for name in zipFile.namelist():
            if ("E2y" in name):
                names.append(name)
        zipFile.close()
    if regime=="txt":
        names=os.listdir(path+"BasicOutput"+os.path.sep+"data"+os.path.sep+"E2y")
        
    
    ###determination of start file and final file to analyse
    if regime=="zip":
        finnumFile=len(names)+numst-2
    if regime=="txt":
        finnumFile=len(names)+numst-1
    
    stnumFile=finnumFile-numAnalysedPeriods*numSavesperPeriod
    numFiles=finnumFile-stnumFile+1
   
    #calculation of averaged spectra
    spectraEl=np.zeros(ngam)
    spectraPos=np.zeros(ngam)
    spectraPh=np.zeros(ngam)
    dgam=(gmax-gmin)/ngam
    enArr=np.linspace(gmin+dgam/2,gmax-dgam/2,ngam)
    if regime=="zip":
        for i in range(stnumFile,finnumFile+1):
            buf=ReadZipTxt(zipfold,("data/ElectronEn/%.6d.txt" % i),ngam)[0]

            spectraEl+=buf*enArr/buf.sum()
            buf=ReadZipTxt(zipfold,("data/PositronEn/%.6d.txt" % i),ngam)[0]
            spectraPos+=buf*enArr/buf.sum()
            buf=ReadZipTxt(zipfold,("data/PhotonEn/%.6d.txt" % i),ngam)[0]
            spectraPh+=buf*enArr/buf.sum()

    if regime=="txt":
        for i in range(stnumFile,finnumFile+1):
            buf=np.loadtxt(pathtxt+"ElectronEn"+os.path.sep+("%.6d.txt"%i))
            spectraEl+=buf*enArr/buf.sum()
            buf=np.loadtxt(pathtxt+"PositronEn"+os.path.sep+("%.6d.txt"%i))
            spectraPos+=buf*enArr/buf.sum()
            buf=np.loadtxt(pathtxt+"PhotonEn"+os.path.sep+("%.6d.txt"%i))
            spectraPh+=buf*enArr/buf.sum()
    spectraEl=spectraEl/numFiles
    spectraPos=spectraPos/numFiles
    spectraPh=spectraPh/numFiles
    
    # determination of 1% energy
    thEn=0.01*spectraEl.sum()
    cursum=0
    i=-1
    while cursum<thEn:
        cursum+=spectraEl[i]
        i-=1
    En1pEl.append(enArr[i+1])
    
    thEn=0.01*spectraPos.sum()
    cursum=0
    i=-1
    while cursum<thEn:
        cursum+=spectraPos[i]
        i-=1
    En1pPos.append(enArr[i+1])
    
    thEn=0.01*spectraPh.sum()
    cursum=0
    i=-1
    while cursum<thEn:
        cursum+=spectraPh[i]
        i-=1
    En1pPh.append(enArr[i+1])
        
    #fig=plt.figure(figsize=(5,4))
    #ax=fig.add_subplot(111)
    #xrangeFrame=(enArr[2],enArr[-1]*1.1)
    #maxy=max(spectraEl.max(),spectraPos.max(),spectraPh.max())
    #yrangeFrame=(maxy*1e-6,maxy*1.1)
    #Add1DPlot(spectraEl, enArr, xrangeFrame, ax,color='green',yrangeFrame=yrangeFrame,linestyle='-',linewidth=1,xscale='log',yscale='log')
    #Add1DPlot(spectraPos, enArr, xrangeFrame,ax, color='red',yrangeFrame=yrangeFrame,linestyle='-',linewidth=1,xscale='log',yscale='log')
    #Add1DPlot(spectraPh, enArr, xrangeFrame, ax,color='purple',yrangeFrame=yrangeFrame,linestyle='-',linewidth=1,xscale='log',yscale='log')
    #ax.axvline(En1pEl[-1],color="green",linestyle=':',linewidth=1)
    #ax.axvline(En1pPos[-1],color="red",linestyle=':',linewidth=1)
    #ax.axvline(En1pPh[-1],color="purple",linestyle=':',linewidth=1)
    #ax.set_xlabel("energy/(0.5MeV)")
    #ax.set_ylabel("Spectra density,a.u.")
    #plt.tight_layout()
    #fig.savefig(sim+os.path.sep+'AvSpectra.png',dpi=300)
    #for ax in fig.axes:
    #    ax.clear()
    #fig.clf()
    #plt.close()
    print(sim, "is processed")
    
file=open("Energy1p.txt","w")
file.write("label\t el1%\t pos1%\t ph1%\n")
for typ,el1,pos1,ph1 in zip(labels,En1pEl,En1pPos,En1pPh):
    file.write("%s %g %g %g\n" % (typ,el1,pos1,ph1))
file.close()


    
    
    
    
    
    
    
