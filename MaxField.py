#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 24 14:43:49 2020

@author: Алексей
"""
#import sys
import os
import math
import zipfile
import numpy as np
import re
import matplotlib.pyplot as plt
import sys

def ReadZipTxt(pathZip,fileInZip,nx,ny=-1):    
    archive = zipfile.ZipFile(pathZip,'r')
    file = archive.read(fileInZip)
    archive.close()
    data=file.decode('utf-8').split()
    return np.array(list(map(float,data))).reshape(ny,nx)


def InputDictionary(pathToInput,dictInput):
    regString=r"""Setting (?:variable|string)\S*\s*(?P<nameVar>\S*)\s*=\s*(?P<valueVar>\S+)\s*"""
    regExpr=re.compile(regString)
    inpFile=open(pathToInput,'r')
    
    for line in inpFile:
        foundVar=regExpr.search(line)
        if foundVar:
            curDict=foundVar.groupdict()
            dictInput[curDict['nameVar']]=curDict['valueVar']
    inpFile.close()
    return 0

def Add1DPlot(data,xax,xrangeFrame,ax,color='k',yrangeFrame=None,linestyle='-',linewidth=1,xscale='linear',yscale='linear',marker=None,fillstyle='full',markersize=4):
    ax.set_xlim(xrangeFrame[0],xrangeFrame[1])
    if (yrangeFrame):
        ax.set_ylim(yrangeFrame[0],yrangeFrame[1])
    else:
        ax.set_ylim(data.min(),data.max())
    ax.set_xscale(xscale)
    ax.set_yscale(yscale)
    
    if (marker):
        lines, =ax.plot(xax,data,color=color,linestyle=linestyle,linewidth=linewidth,marker=marker,fillstyle=fillstyle,markersize=markersize)
    else:
        lines, =ax.plot(xax,data,color=color,linestyle=linestyle,linewidth=linewidth)
    return lines

numAnalysedPeriods=2
regime = "txt" # "txt"
if __name__ == "__main__":
    try:
        numAnalysedPeriods=int(sys.argv[2])
        regime=sys.argv[3]
    except:
        print("default parameters")
print("Parameters of work")
print("numAnalysedPeriods=",numAnalysedPeriods)
print("regime=",regime)

labels=["Amp", "Shift", "Delay", "Ideal", "Amp", "Amp_1.25", "Ideal","Shift","Delay"]
simulations=[
    "amplitude_6beams/6beams_21pw",
    "shift_6beams/6beams_21pw",
    "delay_6beams/6beams_21pw",
    "equal_6beams/6beams_21pw",
#    "amplitude_4beams/4beams_30pw",
#    "shift_4/4beams_30pw",
    "amplitude/12beams_13pw",
    "amplitude_1.25/12beams_13pw",
    "equal/12beams_13pw",
    "shift/12beams_13pw",
    "delay/12beams_13pw",
             #"InputUnified_4_30_2020-12-29_23-49-05_Ideal4",
             #"DifShift/InputUnified_4_30_2020-12-25_00-34-31",
             #"DifDelay/InputUnified_4_30_2020-12-29_15-55-44"
]

#labels=["Ideal","Amp","Shift","Delay"]
#simulations=["InputUnified_4_30_2020-12-29_23-49-05_Ideal4",
#             "DifA/InputUnified_4_30_2020-12-23_15-12-56",
#             "DifShift/InputUnified_4_30_2020-12-25_00-34-31",
#             "DifDelay/InputUnified_4_30_2020-12-29_15-55-44"]
maxBs=[]
maxEs=[]
pwrs=[]

print(simulations)

### calculation of growth rate in each simulation
for sim in simulations:
    print("Analysis of ",sim)
    ###read input file
    path=sim+os.path.sep
    zipfold=path+"BasicOutput"+os.path.sep+"data.zip"
    
    if os.path.isdir(os.path.join(sim, "BasicOutput")):
        pathtxt=os.path.join(sim, "BasicOutput", "data") + os.path.sep
    else:
        pathtxt=os.path.join(sim, "data") + os.path.sep
        
    inputDict={}
    InputDictionary(path+"ParsedInput.txt",inputDict)
    pwr=float(inputDict["PeakPowerPW"])
    w=float(inputDict["Omega"])
    dt=float(inputDict["TimeStep"])
    iterBetwSaves=int(inputDict["BOIterationPass"])
    iterst=int(inputDict["BOIterationStart"])
    nx=int(inputDict["E2y.SetMatrixSize_0"])
    nz=int(inputDict["E2y.SetMatrixSize_1"])
    numst=int(iterst/iterBetwSaves)
    period=2*math.pi/w
    numSavesperPeriod=int(period/(dt*iterBetwSaves)+0.5)
    pwrs.append(pwr)

    
#    ###calculation of number of saved files
#    if regime=="zip":
#        zipfold=path+"BasicOutput"+os.path.sep+"data.zip"
#        zipFile=zipfile.ZipFile(zipfold,"r")
#        names=[]
#        for name in zipFile.namelist():
#            if ("E2y" in name):
#                names.append(name)
#        zipFile.close()
#    if regime=="txt":
#        names=os.listdir(path+"BasicOutput"+os.path.sep+"data"+os.path.sep+"E2y")
        
    
    ###calculation of maximal fields
#    if regime=="zip":
#        finnumFile=len(names)+numst-2
#    if regime=="txt":
#        finnumFile=len(names)+numst-1
    finnumFile=numst+numAnalysedPeriods*numSavesperPeriod
    
#    numdata=finnumFile-stnumFile+1
    
    maxE=0
    maxB=0
    if regime=="zip":
        for i in range(numst,finnumFile+1):
            maxcurE=ReadZipTxt(zipfold,("data/E2y/%.6d.txt" % i),nx,nz).max()
            maxcurB=ReadZipTxt(zipfold,("data/B2y/%.6d.txt" % i),nx,nz).max()
            if maxE<maxcurE:
                maxE=maxcurE
            if maxB<maxcurB:
                maxB=maxcurB
    if regime=="txt":
        for i in range(numst,finnumFile+1):
            maxcurE=np.loadtxt(pathtxt+"E2y"+os.path.sep+("%.6d.txt"%i)).max()
            maxcurB=np.loadtxt(pathtxt+"B2y"+os.path.sep+("%.6d.txt"%i)).max()
            if maxE<maxcurE:
                maxE=maxcurE
            if maxB<maxcurB:
                maxB=maxcurB
    maxEs.append(maxE)
    maxBs.append(maxB)
    
file=open("maxEB.txt","w")
file.write("Label\t max E\t max B\n")
i=0
for typ,mE,mB in zip(labels,maxEs,maxBs):
    file.write("%s %g %g\n" % (typ,mE/(3e11*math.sqrt(pwrs[i]/10)),mB/(0.659*3e11*math.sqrt(pwrs[i]/10))))
    i+=1
file.close()


    
    
    
    
    
    
    
