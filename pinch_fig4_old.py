#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np
import matplotlib as mp

def main():
    power_p = []
    ezx_p = []
    bzx_p = []
    nex_p = []
    nemaxx_p = []
    ncrx_p = []
    jx_min_p = []
    jx_max_p = []
    powx_p = []
    pow1x_p = []
    maxx_p = []
    avx_p = []
    el_powx_p = []
    el_pow1x_p = []
    el_maxx_p = []
    el_avx_p = []
    picspath = 'pics/'
    
    f = open('stat_summary_el.txt', 'r')
    for line in f:
        tmp = line.split()
        power_p.append(float(tmp[0]))
        ezx_p.append(float(tmp[1])/1e11)
        bzx_p.append(float(tmp[2])/1e11)
        maxx_p.append(float(tmp[3]))
        avx_p.append(float(tmp[4])*1e3)
        ncrx_p.append(float(tmp[5]))
        nex_p.append(float(tmp[6])/1e25)
        powx_p.append(float(tmp[7]))
        pow1x_p.append(float(tmp[8]))
        jx_min_p.append(float(tmp[9]))
        jx_max_p.append(float(tmp[10]))
        el_maxx_p.append(float(tmp[11]))
        el_avx_p.append(float(tmp[12])*1e3)
        el_powx_p.append(float(tmp[13]))
        el_pow1x_p.append(float(tmp[14]))
        nemaxx_p.append(float(tmp[15])/1e25)
    f.close()
    nmax = 100
    dp = power_p[-1]/nmax
    p = np.zeros(nmax)
    e = np.zeros(nmax)
    b = np.zeros(nmax)
    for i in range(nmax):
        p[i] = dp*i
        e[i] = 3*math.sqrt(p[i]/10)
        b[i] = 0.653*3*math.sqrt(p[i]/10)

    thr_power = 7.25
    ep = 3*math.sqrt(thr_power/10)
    bp = 0.653*3*math.sqrt(thr_power/10)
    lfontsize = 10
    xticks = [10,15,20,25,30]
    xticklabels = ['$%d$'%(x) for x in xticks]
    fontsize = 14
    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    plt.minorticks_on()
    mp.rcParams.update({'font.size': fontsize})
    #fig.subplots_adjust(bottom=-0.06)
    spx = 2
    spy = 3
    ax = fig.add_subplot(1,1,1)
#    ax.plot(power_p, ezx_p, 'r-o', label='E',fillstyle = 'none')
#    ax.plot(p, e, 'r:', label = 'E')
#    ax.plot([thr_power,power_p[-1]], [ep,ep], 'r--' , label = 'E')
    # ax.plot([thr_power,power_p[-1]], [ep,ep], 'r--')
    ax.plot(power_p, bzx_p, 'b-v', label='B - Stationary',fillstyle = 'none')
    ax.plot(p, b, 'b:', label = 'B - Vacuum')
    ax.plot([thr_power,power_p[-1]], [bp,bp], 'b--', label = 'B - Threshold')
    ax.plot([20, 20], [0, 1.1*max(e)], 'k-', dashes = [2,2])
    # ax.plot([thr_power,power_p[-1]], [bp,bp], 'b--')
    ax.set_xlim([7,power_p[-1]+0.5])
    ax.set_ylim([0,4.5])
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    plt.legend(loc = 'lower right', fontsize = lfontsize, ncol=2, frameon = False)
    yticks = [1,2,3,4]
    ax.set_yticks(yticks)
    ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ax.set_xlabel('$P, PW$')
    ax.set_ylabel('$B, \\times 10^{11} CGS$')
    picname = picspath + '/' + "pinch_field_vs_power.png"
    plt.savefig(picname, dpi=256, bbox_inches='tight')
    plt.close()
    # ax.text(5.8, 4, '(a)')
    rpow_p = []
    rpow1_p = []
    eff_p = []
    eff1_p = []
    
    for i in range(len(power_p)):
        el_powx_p[i] = power_p[i] - el_powx_p[i]
        powx_p[i] = power_p[i] - powx_p[i]
        rpow_p.append(power_p[i] - powx_p[i] - 2*el_powx_p[i])
        print power_p[i], powx_p[i], el_powx_p[i], power_p[i] - powx_p[i] - 2*el_powx_p[i]
        eff_p.append((powx_p[i] + 2*el_powx_p[i])/power_p[i])
        eff1_p.append(powx_p[i]/power_p[i])

    # power1_p = [0, thr_power] + power_p
    # rpow1_p = [0, thr_power] + rpow_p
    # powx1_p = [0, 0] + powx_p
    # el_powx1_p = [0, 0] + el_powx_p
    power1_p = power_p
    rpow1_p = rpow_p
    powx1_p = powx_p
    el_powx1_p = el_powx_p

    
    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    plt.minorticks_on()  
    ax = fig.add_subplot(1,1,1)
    ax.plot(power1_p, rpow1_p, 'b-o', label = 'Reflected',fillstyle = 'none')
    ax.plot(power1_p, powx1_p, 'g-v', label = 'Photons',fillstyle = 'none')
    ax.plot(power1_p, el_powx1_p, 'r-^', label = 'Electrons',fillstyle = 'none')
    ax.plot([20, 20], [0, 1.1*max(powx1_p)], 'k-', dashes = [2,2])
    # ax.plot([thr_power, thr_power], [0, 8], 'b--', label = 'Threshold')
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    yticks = [5,10,15,20,25]
    ax.set_yticks(yticks)
    ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ax.set_xlabel('$P, PW$')
    ax.set_ylabel('Radiated power, PW')
    # ax.set_xticks([0,5,thr_power,10,15])
    plt.legend(loc = 'upper left', fontsize = lfontsize, frameon = False)
    picname = picspath + '/' + "pinch_power_vs_power.png"
    plt.savefig(picname, dpi=256, bbox_inches='tight')
    plt.close()
    # ax.text(5.8, 8, '(b)')
    # ax = fig.add_subplot(spx,spy,3)
    # ax.plot([thr_power] + power_p, [0] + eff1_p, 'g-o', label = 'Conversion')
    # ax.plot([thr_power]+ power_p, [0] + eff_p, 'b-v', label = 'Absorption')
    # ax.set_xlabel('Power, PW')
    # ax.set_ylabel('Efficiency')
    # plt.legend(loc = 'lower right')

    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    plt.minorticks_on()  
    ax = fig.add_subplot(1,1,1)
    
#    ax.plot(power_p, nex_p, 'b-o', label = 'Stationary',fillstyle = 'none')
    ax.plot(power_p, nemaxx_p, 'r-v', label = 'Max',fillstyle = 'none')
    ax.plot([20, 20], [0, 1.1*max(nemaxx_p)], 'k-', dashes = [2,2])
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    yticks = [0,1,2,3,4,5]
    # ax.set_yticks(yticks)
    # ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ax.set_xlabel('$P, PW$')
    ax.set_ylabel('$N_e, \\times 10^{25} cm^{-3}$')
#    plt.legend(loc = 'upper left', fontsize = lfontsize, frameon = False)
    picname = picspath + '/' + "pinch_density_vs_power.png"
    plt.savefig(picname, dpi=256, bbox_inches='tight')
    plt.close()
    # ax.text(5.8, 5, '(d)')
    # fig = plt.figure()
    # nex = fig.add_subplot(1,1,1)
    # nex.set_title('Ne')
    # nex.plot(power_p, ncrx_p)
    # nex.set_xlabel('Power, PW')
    # nex.set_ylabel('$N_e/N_{cr}$')
    # plt.show()
    # plt.close()
    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    plt.minorticks_on()  
    maxx = fig.add_subplot(1,1,1)
    m, = maxx.plot(power_p, maxx_p, 'g-v',fillstyle = 'none')
    mel, = maxx.plot(power_p, el_maxx_p, 'r-^',fillstyle = 'none')
    maxx.set_ylim([0, 1.5])
    maxx.set_xlabel('$P, PW$')
    maxx.set_ylabel('Maximal energy, GeV')
    # maxx.yaxis.label.set_color(m.get_color())
    # maxx.spines["left"].set_color(m.get_color())
    # maxx.tick_params(axis='y', colors=m.get_color())
    yticks = [0.6,0.9,1.3]
    maxx.set_yticks(yticks)
    maxx.set_yticklabels(['$%.1f$'%(x) for x in yticks])
    maxx.legend([m,mel], ['Photons','Electrons'], bbox_to_anchor=(1, 0.4), fontsize = lfontsize, ncol=2, labelspacing=0.05, frameon = False)
    # maxx.text(5.6, 1.5, '(c)')
    picname = picspath + '/' + "pinch_maxenergy_vs_power.png"
    plt.savefig(picname, dpi=256, bbox_inches='tight')
    plt.close()
    
    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    plt.minorticks_on()  
    avx = fig.add_subplot(1,1,1)
    a, = avx.plot(power_p, avx_p, 'g-v',fillstyle = 'none')
    ael, = avx.plot(power_p, el_avx_p, 'r-^',fillstyle = 'none')
    avx.set_ylim([0, 1.1*max(el_avx_p)])
    avx.plot([20, 20], [0, 1.1*max(el_avx_p)], 'k-', dashes = [2,2])
    avx.set_ylabel('Average energy, MeV')
    # avx.yaxis.label.set_color(a.get_color())
    # avx.spines["right"].set_color(a.get_color())
    # avx.spines["left"].set_color(m.get_color())
    # avx.tick_params(axis='y', colors=a.get_color())
    avx.legend([a,ael], ['Photons','Electrons'], bbox_to_anchor=(1, 0.4), fontsize = lfontsize, ncol=2, labelspacing=0.05, frameon = False)
    yticks = [30, 140, 180, 210, 270]
    avx.set_yticks(yticks)
    avx.set_yticklabels(['$%d$'%(x) for x in yticks])
    avx.set_xticks(xticks)
    avx.set_xticklabels(xticklabels)
    picname = picspath + '/' + "pinch_avenergy_vs_power.png"
    plt.savefig(picname, dpi=256, bbox_inches='tight')
    plt.close()

    f = open('charge.txt', 'r')
    power_p = []
    charge = []
    el_flux = []
    ph_flux = []
    for line in f:
        tmp = line.split()
        power_p.append(float(tmp[0]))
        charge.append(float(tmp[1]))
        el_flux.append(float(tmp[2]))
        ph_flux.append(float(tmp[3]))
    f.close()

    f = open('charge_all.txt', 'r')
    power_all_p = []
    charge_all = []
    el_flux_all = []
    ph_flux_all = []
    for line in f:
        tmp = line.split()
        power_all_p.append(float(tmp[0]))
        charge_all.append(float(tmp[1]))
        el_flux_all.append(float(tmp[2])/1e3)
        ph_flux_all.append(float(tmp[3])/1e4)
    f.close()
    
    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    plt.minorticks_on()  
    fl_ax = fig.add_subplot(1,1,1)
    
    gev, = fl_ax.plot(power_p, ph_flux, 'g-v', label = '> 1 GeV', fillstyle = 'none')
    fl_ax_all = fl_ax.twinx()
    full, = fl_ax_all.plot(power_all_p, ph_flux_all, 'r-v', label = 'Full', fillstyle = 'none')
    fl_ax.plot([20, 20], [0, max([max(ph_flux),max(ph_flux_all)])], 'k-', dashes = [2,2])
    fl_ax.set_ylabel('Flux > 1 Gev, $\\times 10^{23} s^{-1}$')
    fl_ax_all.set_ylabel('Full flux, $\\times 10^{27} s^{-1}$')
    fl_ax.set_xlabel('$P, PW$')
#    fl_ax.set_ylim([0,15])
#    yticks = [0,2,4,6,8,10]
#    fl_ax.set_yticks(yticks)
#    fl_ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    fl_ax.set_xticks(xticks)
    fl_ax.set_xticklabels(xticklabels)
    # fl_ax.text(5.8, 9, '(f)')
    plt.legend([gev, full], ['> 1 GeV', 'Full'], loc = 'upper left', fontsize = lfontsize, frameon = False)
#    ch_ax = fl_ax.twinx()
#    ch_ax.plot(power_p, charge, 'b-o', label = 'Charge',fillstyle = 'none')
    # ch_ax.set_xticks(xticks)
    # ch_ax.set_xticklabels(xticklabels)
#    yticks = [10,20,30,40,50,60,70]
    
#    ch_ax.set_yticks(yticks)
#    ch_ax.set_yticklabels(['$%d$'%(x) for x in yticks])
#    ch_ax.set_ylim([0,100])
#    ch_ax.set_xlabel('$P, PW$')
#    ch_ax.set_ylabel('$Charge, pC$')
#    plt.legend(loc = 'lower left', fontsize = lfontsize, frameon = False)
    picname = picspath + '/' + "pinch_flux_vs_power.png"
#    plt.savefig(picname, dpi=256, bbox_inches='tight')
#    plt.close()

#    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
#    plt.minorticks_on()  
#    fl_ax = fig.add_subplot(1,1,1)
    
    gev, = fl_ax.plot(power_p, el_flux, 'g-o', label = '> 1 GeV', fillstyle = 'none')
#    fl_ax_all = fl_ax.twinx()
    full, = fl_ax_all.plot(power_all_p, el_flux_all, 'r-o', label = 'Full', fillstyle = 'none')
    fl_ax.plot([20, 20], [0, max([max(el_flux),max(el_flux_all)])], 'k-', dashes = [2,2])
    fl_ax.set_ylabel('Flux > 1 Gev, $\\times 10^{23} s^{-1}$')
    fl_ax_all.set_ylabel('Full flux, $\\times 10^{26} s^{-1}$')
    fl_ax.set_xlabel('$P, PW$')
#    fl_ax.set_ylim([0,15])
#    yticks = [0,2,4,6,8,10]
#    fl_ax.set_yticks(yticks)
#    fl_ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    fl_ax.set_xticks(xticks)
    fl_ax.set_xticklabels(xticklabels)
    # fl_ax.text(5.8, 9, '(f)')
    plt.legend([gev, full], ['> 1 GeV', 'Full'], loc = 'upper left', fontsize = lfontsize, frameon = False)
#    ch_ax = fl_ax.twinx()
#    ch_ax.plot(power_p, charge, 'b-o', label = 'Charge',fillstyle = 'none')
    # ch_ax.set_xticks(xticks)
    # ch_ax.set_xticklabels(xticklabels)
#    yticks = [10,20,30,40,50,60,70]
    
#    ch_ax.set_yticks(yticks)
#    ch_ax.set_yticklabels(['$%d$'%(x) for x in yticks])
#    ch_ax.set_ylim([0,100])
#    ch_ax.set_xlabel('$P, PW$')
#    ch_ax.set_ylabel('$Charge, pC$')
#    plt.legend(loc = 'lower left', fontsize = lfontsize, frameon = False)
    picname = picspath + '/' + "pinch_elflux_vs_power.png"
    plt.savefig(picname, dpi=256, bbox_inches='tight')
    plt.close()
    
    f = open('jfull.txt', 'r')
    power_p = []
    jp = []
    jn = []
    jf = []
    jfm = []
    for line in f:
        tmp = line.split()
        power_p.append(float(tmp[0]))
        jp.append(float(tmp[1]))
        jn.append(float(tmp[2]))
        jf.append(float(tmp[3]))
        jfm.append(float(tmp[4]))
    f.close()

    f = open('jcenter.txt', 'r')
    power_p = []
    jc = []
    
    for line in f:
        tmp = line.split()
        power_p.append(float(tmp[0]))
        jc.append(float(tmp[1]))
        
    f.close()
    
    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    plt.minorticks_on()  
    ax = fig.add_subplot(1,1,1)
    
    ax.plot(power_p, jp, 'r-o', label='$r < 0.44 \lambda$',fillstyle = 'none')
    ax.plot([20, 20], [0, 35], 'k-', dashes = [2,2])
    # ax.plot(power_p, jn, 'b-o', label='negative')
#    ax.plot(power_p, jf, 'g-v', label='full',fillstyle = 'none')
    ax.plot(power_p, jfm, 'k-^', label='full max',fillstyle = 'none')
    ax.set_ylim([0, 35])
    ax.set_ylabel('$J_z, MA$')
    ax.set_xlabel('$P, PW$')
    yticks = [0,5,10,15,20,25,30, 35]
    ax.set_yticks(yticks)
    ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    # ax.text(5.8, 17, '(e)')
    plt.legend(loc = 'upper left', fontsize = lfontsize, frameon = False)
    picname = picspath + '/' + "pinch_current_vs_power.png"
    plt.savefig(picname, dpi=256, bbox_inches='tight')
    plt.close()

    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    plt.minorticks_on()  
    ax = fig.add_subplot(1,1,1)
    
    ax.plot(power_p, jc, 'r-o', label='$r < 0.1 \lambda$',fillstyle = 'none')
    ax.plot([20, 20], [0, 10], 'k-', dashes = [2,2])
    # ax.plot(power_p, jn, 'b-o', label='negative')
#    ax.plot(power_p, jf, 'g-v', label='full',fillstyle = 'none')
#    ax.plot(power_p, jfm, 'k-^', label='full max',fillstyle = 'none')
    ax.set_ylim([0, 10])
    ax.set_ylabel('$J_z, MA$')
    ax.set_xlabel('$P, PW$')
    yticks = [0,2,4,6,8,10]
    ax.set_yticks(yticks)
    ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    # ax.text(5.8, 17, '(e)')
    plt.legend(loc = 'upper left', fontsize = lfontsize, frameon = False)
    picname = picspath + '/' + "pinch_central_current_vs_power.png"
    plt.savefig(picname, dpi=256, bbox_inches='tight')
    plt.close()
    '''fig = plt.figure()
    maxx = fig.add_subplot(1,1,1)
    m, = maxx.plot(power_p, el_maxx_p, 'b')
    maxx.set_ylim([0, 1.1*max(el_maxx_p)])
    maxx.set_title('Max energy')
    maxx.set_xlabel('Power, PW')
    maxx.set_ylabel('Maximal electron energy, GeV')
    
    avx = maxx.twinx()
    a, = avx.plot(power_p, el_avx_p, 'r')
    avx.set_ylim([0, 1.1*max(el_avx_p)])
    avx.set_ylabel('Average electron energy, GeV')
    avx.legend([m,a], ['Maximal', 'Average'], loc = 'lower right')
    plt.show()
    plt.close()'''
    
    # fig = plt.figure()
    # maxx = fig.add_subplot(1,1,1)
    # m, = maxx.plot(power_p, maxx_p, 'b')
    # maxx.set_ylim([0, 1.1*max(maxx_p)])
    # maxx.set_title('Max energy')
    # maxx.set_xlabel('Power, PW')
    # maxx.set_ylabel('Maximal photon energy, GeV')
    
    # ezx = maxx.twinx()
    # a, = ezx.plot(power_p, ezx_p, 'r')
    # ezx.set_ylim([0, 1.1*max(ezx_p)])
    # ezx.set_ylabel('Stationary E field')
    # plt.legend([m,a], ['Max energy', 'Electric field'], loc = 'lower right')
    # plt.show()
    # plt.close()
    
    # power_p1 = []
    # nmin = []
    # nmax = []
    # f = open('nmin_nmax.txt')
    # for line in f:
    #     tmp = line.split()
    #     power_p1.append(float(tmp[0]))
    #     nmin.append(float(tmp[1]))
    #     nmax.append(float(tmp[2]))
    # f.close()

    # power_p2 = []
    # jneg = []
    # jpos = []
    # f = open('jpos_jneg.txt')
    # for line in f:
    #     tmp = line.split()
    #     power_p2.append(float(tmp[0]))
    #     jpos.append(float(tmp[1]))
    #     jneg.append(float(tmp[2]))
    # f.close()
    # print jpos, jneg
    # mp.rcParams.update({'font.size': 16})
    # fig = plt.figure()
    # maxx = fig.add_subplot(1,2,1)
    # m1, = maxx.plot(power_p1, nmin, 'r-o')
    # maxx.set_ylim([0, 1.05*max(nmin)])
   
    # maxx.set_xlabel('Power, PW')
    # maxx.set_ylabel('Number of particles')

    # jx = maxx.twinx()
    # j1, = jx.plot(power_p2, jpos, 'r--o')
    # jx.set_ylim([0, 1.1*max(jpos)])
    # jx.set_xlabel('Power, PW')
    # jx.set_ylabel('$+J_z, MA$')

    # maxx = fig.add_subplot(1,2,2)
    # m2, = maxx.plot(power_p1, nmax, 'b-o')
    # maxx.set_ylim([0, 1.05*max(nmax)])
    
    # maxx.set_xlabel('Power, PW')
    # # maxx.set_ylabel('Number of particles')

    # jx = maxx.twinx()
    # j2, = jx.plot(power_p2, jneg, 'b--o')
    # jx.set_ylim([0, 1.1*max(jpos)])
    # jx.set_xlabel('Power, PW')
    # jx.set_ylabel('$-J_z, MA$')
    
    # plt.legend([m1,m2,j1,j2], ['$r<0.4 \lambda$', '$r>0.4 \lambda$', '$+J_z$', '$-J_z$'], loc = 'upper left')
    
    # plt.show()
    # plt.close()
    
    # fig = plt.figure()
    # ezx = fig.add_subplot(4,2,1)
    # ezx.set_title('Electric field')
    # ezx.plot(power_p, ezx_p)
    # ezx.set_ylim([0, max(ezx_p)])
    # bzx = fig.add_subplot(4,2,2)
    # bzx.set_title('Magnetic field')
    # bzx.plot(power_p, bzx_p)
    # bzx.set_ylim([0, max(bzx_p)])
    # ncrx = fig.add_subplot(4,2,4)
    # ncrx.set_title('Ne/Ncr')
    # ncrx.plot(power_p, ncrx_p)
    # ncrx.set_ylim([0, max(ncrx_p)])
    # nex = fig.add_subplot(4,2,3)
    # nex.set_title('Ne')
    # nex.plot(power_p, nex_p)
    # nex.set_ylim([0, max(nex_p)])
    # powx = fig.add_subplot(4,2,6)
    # powx.set_title('Power')
    # powx.plot(power_p, powx_p)
    # powx.set_ylim([0, max(powx_p)])
    # pow1x = powx.twinx()
    # pow1x.plot(power_p, pow1x_p, 'g')
    # pow1x.set_ylim([0, max(pow1x_p)])
    # maxx = fig.add_subplot(4,2,7)
    # maxx.set_title('Max energy')
    # maxx.plot(power_p, maxx_p)
    # maxx.set_ylim([0, max(maxx_p)])
    # avx = fig.add_subplot(4,2,8)
    # avx.plot(power_p, avx_p)
    # avx.set_ylim([0, max(avx_p)])
    # avx.set_title('Av energy')
    # jx = fig.add_subplot(4,2,5)
    # jx.set_title('Number of particles')
    # jx.plot(power_p, jx_min_p)
    # jx.plot(power_p, jx_max_p)
    # jx.set_ylim([0, max(jx_max_p)])
    picname = picspath + '/' + "fig4.png"
    # plt.tight_layout()
    plt.savefig(picname, bbox_inches='tight', dpi=128)
    plt.close()
    plt.show()
    
if __name__ == '__main__':
    main()
