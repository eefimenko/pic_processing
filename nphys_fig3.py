#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils
import numpy as np
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

def main():
    path = './'
    config = utils.get_config(path + "/ParsedInput.txt")
    picspath = 'pics/'

    iterations = [0, 446, 0, 506]
    labels = ['a', 'c', 'b', 'd', 'e', 'f']
    wl = float(config['Wavelength'])
    xmax = float(config['X_Max'])/wl #mkm
    xmin = float(config['X_Min'])/wl #mkm
    ymax = float(config['Y_Max'])/wl #mkm
    ymin = float(config['Y_Min'])/wl #mkm
    zmax = float(config['Z_Max'])/wl #mkm
    zmin = float(config['Z_Min'])/wl #mkm
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    power = int(config['PeakPowerPW'])
    dx = float(config['Step_X'])/wl
    dy = float(config['Step_Y'])/wl
    dz = float(config['Step_Z'])/wl
    mult = (1/(2.*dx*dy*dz*wl*wl*wl))*100
    step = float(config['TimeStep'])*1e15*float(config['BOIterationPass'])
    omega = float(config['Omega'])
    ncr = utils.ElectronMass * omega * omega/(8. * math.pi * utils.ElectronCharge * utils.ElectronCharge)
    T = 2 * math.pi/omega
    nt = int(T*1e15/step)
    dt  = step/(T*1e15)
    print nt
    print 'Critical density ', ncr
    figures = [utils.bx_zpath, utils.nezpath]
    cmaps = ['bwr', 'Greens']
    titles = ['$B_\\rho$', '$N_e$']
    log = [False, False]
             

#    n0 = [703, 0, 0] # 675
#    n0 = [686, 0, 0] # 680
#    n0 = [1030, 1090]
    
    spx = 1
    spy = 4

    xlim = [-0.4, 0.4]
    ylim = xlim
    verbose = 1
    # nticks = 5
    # ticks = [xlim[0] + i*(xlim[1]-xlim[0])/(nticks-1) for i in range(nticks)]
    # print ticks
    niter = len(iterations)
        
    # fig = plt.figure(num=None, figsize=(12., 3.))
    # mp.rcParams.update({'font.size': 6})
    # fontsize = 10
    for i in range(niter):
        if i == 1 or i == 3:
            fig = plt.figure(num=None, figsize=(4., 4.))
            mp.rcParams.update({'font.size': 14})
            fontsize = 14
            lw = 0.6
            xticks = [-0.4, 0, 0.4]
            # if i%4 != 0:
            #     ylabel = ''
            #     yticks = []
            # else:
            ylabel = '$y/\lambda$'
            yticks = [-0.4, 0, 0.4]
            clevels = [-0.15, 0.15]
            ax = utils.subplot(fig, iterations[i], path, fmt='Brn%d',
                               shape = (nx,ny), position = (1,1,1),
                               extent = [xmin, xmax, ymin, ymax],
                               cmap = 'bwr', title = '', titletype = 'simple',
                               colorbar = False, logarithmic=False, verbose=verbose,
                               xlim = xlim, ylim = ylim,
                               xlabel = '$x/\lambda$', ylabel = ylabel, yticks = yticks, xticks = xticks,
                               maximum = 'local', fontsize = fontsize,
                               contour=True, clevels = clevels, clinewidth = 0.4)
            for i__ in pax.spines.itervalues():
                i__.set_linewidth(lw)
            
            ax = utils.subplot(fig, iterations[i], path, fmt='Eln%d', 
                               shape = (nx,ny), position = (1,1,1),
                               extent = [xmin, xmax, ymin, ymax],
                               cmap = 'YlGn', title = '', titletype = 'simple',
                               colorbar = True, cbartitle = '$n_e/n_c$', ncbarticks = 2, logarithmic=False, verbose=verbose,
                               xlim = xlim, ylim = ylim, yticks = yticks, xticks = xticks,
                               xlabel = '$x/\lambda$', ylabel = ylabel, vmax = 0.5,
                               maximum = 'local', fontsize = fontsize, mult = mult/ncr)
            ax.text(-0.63, 0.4, '$(%s)$'%(labels[i]), fontsize = 18)
            for i__ in pax.spines.itervalues():
                i__.set_linewidth(lw)
            picname = picspath + '/' + "fig3%s.png"%(labels[i])
            # plt.tight_layout()
            plt.savefig(picname, bbox_inches='tight', dpi=256)
            plt.close()
        else:
            
            fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
            mp.rcParams.update({'font.size': 18})
            legfontsize = 15
            # fontsize = 7
            pxmin = 3.15
            pxmax = 4.5
            
            lw=0.8
            
            lines1 = [3.149, 3.2455, 3.56, 4.017]
            lines2 = [3.237, 3.2955, 3.671, 4.091]
            if i == 0: # figure a
                print i
                pzr = np.fromfile('Pzr_1.txt', dtype=float, sep = ' ')
                eb = np.fromfile('EB_1.txt', dtype=float, sep = ' ')
                gam = np.fromfile('Gam_1.txt', dtype=float, sep = ' ')
                nx_ = len(pzr)
                print nx_
                axis = utils.axis(pxmin,pxmax,nx_)
                pax = fig.add_subplot(1,1,1)
                p__, = pax.plot(axis,pzr, 'r-', linewidth = lw, dashes = [6,1])
                e__, = pax.plot(axis,eb, 'b-', linewidth = lw, dashes = [3,1,3,1])
                pax.plot([3.0, 4.5],[1,1], 'k--', linewidth = lw)
                for line in lines1:
                    pax.plot([line,line], [0, 1e3], 'k--', linewidth = lw)
                for line in lines2:
                    pax.plot([line,line],[0, 1e3], 'k:', linewidth = lw)
                pax1 = pax.twinx()
                g__, = pax1.plot(axis,gam, 'g', linewidth = lw)
                plt.minorticks_on()
                pax.set_yscale('log')
                pax.set_xlabel('t/T')
                pax.set_xticks([3, 3.5, 4])
                pax.set_ylabel('$|E_z|/|B|, |p_z|/|p_\\rho|$')
                pax.set_xticklabels(['$3$', '$3.5$', '$4$'])
                pax.set_ylim([1e-2, 1e2])
                pax.set_yticks([1e-2,1e-1,1, 1e1, 1e2])
                pax.set_yticklabels(['$10^{-2}$','$10^{-1}$','$1$', '$10^1$', '$10^2$'])
                pax.text(2.65, 1e3, '$(a)$')
                pax1.set_yticklabels(['$0$','$0.5$','$1$'])
                pax1.set_ylim([0,1500])
                pax1.set_ylabel('$\gamma, \\times 10^3$')
                pax.set_xlim([3,4.3])
                pax1.set_xlim([3,4.3])
                pax1.set_yticks([0,500,1000])
                pax1.set_yticklabels(['$0$','$0.5$','$1$'])
                lgd = pax.legend([e__,p__,g__], ['$|E_z|/|B|$', '$|p_z|/|p_\\rho|$', '$\gamma$'], loc='lower center', bbox_to_anchor=(0.5, 1), ncol = 2, frameon=False, fontsize = legfontsize, columnspacing = 5, handletextpad= 0.1)
                for i__ in pax.spines.itervalues():
                    i__.set_linewidth(lw)
                for i__ in pax1.spines.itervalues():
                    i__.set_linewidth(lw)
            else: # figure b
                msize = 6
                # difference due to different n_cr normalization: AB to 1/4, me to 1/8
                elmax = np.fromfile('Elmax.txt', dtype=float, sep = ' ')*2 
                bmax = np.fromfile('Bmax.txt', dtype=float, sep = ' ')
                xe_t = [3.15 + x_*0.033 for x_ in range(6)]
                elmax_t = [(lambda x: 460*math.exp(25*(x-3.15)) + 2400)(x_) for x_ in xe_t]
                xb_t = [3.15 + x_*0.045 for x_ in range(6)]
                bmax_t = [(lambda x: 8e-4*math.exp(25*(x-3.15)))(x_) for x_ in xb_t]
                # print x_t, elmax_t
                nx_ = len(bmax)
                print nx_
                axis = utils.axis(pxmin,pxmax,nx_)
                for i_ in range(nx_):
                    bmax[i_] /= 1.75e11
                pax = fig.add_subplot(1,1,1)
                el__, = pax.plot(axis,elmax, 'g-', linewidth = lw, dashes = [6,1])
                # el_t__, = pax.plot(xe_t,elmax_t, 'g-o', linewidth = lw/2, fillstyle = 'none', markersize = msize)
                pax1 = pax.twinx()
                bmax__, = pax1.plot(axis,bmax, 'b', linewidth = lw)
                # bmax_t__, = pax1.plot(xb_t,bmax_t, 'b-^', linewidth = lw/2, fillstyle = 'none', markersize = msize)
                for line in lines1:
                    pax.plot([line,line], [-4000, 10e4], 'k--', linewidth = lw)
                for line in lines2:
                    pax.plot([line,line],[-4000, 10e4], 'k:', linewidth = lw)
                for i__ in pax.spines.itervalues():
                    i__.set_linewidth(lw)
                for i__ in pax1.spines.itervalues():
                    i__.set_linewidth(lw)
                pax.set_xlabel('t/T')
                pax.set_ylabel('$n_e^{max}/n_c, \\times10^4$')
                pax.set_xticks([3, 3.5, 4])
                pax.set_xticklabels(['$3$', '$3.5$', '$4$'])
                pax.set_yticks([0,4e4,8e4])
                pax.set_ylim([0,10e4])
                pax.set_yticklabels(['$0$','$4$','$8$'])
                pax.set_xlim([3, 4.3])
                pax.text(2.8, 13e4, '$(b)$')
                plt.minorticks_on()
                axins = plt.axes([0.15, 0.4, .32, .49])
                # axins = zoomed_inset_axes(pax, 1.5, loc=2)
                
                axins.set_xticks([])
                axins.set_yticks([])
                elins__, = axins.plot(axis,elmax, 'g-', linewidth = lw, dashes = [6,1])
                el_t__, = axins.plot(xe_t,elmax_t, 'g-o', linewidth = lw/2, fillstyle = 'none', markersize = msize)
                axins1 = axins.twinx()
                axins1.set_xticks([])
                axins1.set_yticks([])
                bmaxins__, = axins1.plot(axis,bmax, 'b', linewidth = lw)
                bmax_t__, = axins1.plot(xb_t,bmax_t, 'b-^', linewidth = lw/2, fillstyle = 'none', markersize = msize)
                # # draw a bbox of the region of the inset axes in the parent axes and
                # # connecting lines between the bbox and the inset axes area
                mark_inset(pax, axins, loc1=3, loc2=4, fc="none", ec="0.5")
                x1, x2, y1, y2, y1_1, y2_1 = 3.14, 3.39, 0, 3.4e4, 0, 0.42
                axins.set_xlim([x1, x2])
                axins1.set_xlim([x1, x2])
                axins.set_ylim([y1, y2])
                axins1.set_ylim([y1_1, y2_1])
                
                # pax.ticks_params()
                pax1.set_ylabel('$B_{\\rho}^{max}/B_{vac}^{max}$')
                pax1.set_xlim([3, 4.3])
                pax1.set_yticks([0, 0.5, 1])
                pax1.set_yticklabels(['$0$','$0.5$', '$1$'])
                pax1.set_ylim([0.,1.2])
                lgd = pax.legend([el__, bmax__, el_t__, bmax_t__], ['$n_e^{max}/n_c$', '$B_{\\rho}^{max}/B_{vac}^{max}$', '$460e^{25(t/T-3.15)} + 2400$', '$8\\times10^{-4}e^{25(t/T-3.15)}$'], loc='lower center', bbox_to_anchor=(0.6, 1), ncol = 2, frameon=False, fontsize = legfontsize, columnspacing = 0.1, handletextpad= 0.1)
                #plt.subplots_adjust(left=0.1, right=0.9, bottom=0.0, top=0.9)
            #plt.show()
            picname = picspath + '/' + "fig3%s.png"%(labels[i])
            #plt.tight_layout()
            plt.savefig(picname, dpi=256, bbox_extra_artists=(lgd,), bbox_inches='tight')
            plt.close()

    
if __name__ == '__main__':
    main()
