#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import shutil
import os

def read_file_sp(file, de):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
    if 'tr' in file and not 'fs' in file:
        print 'removed'
        array, de = adjust_array(array, de)
        array[0] = 0
    for i in range(len(array)):
        nph  += array[i]/(i+0.425)/de
    print de
    for i in range(len(array)):
#        array[i] /= de
        array[i] = array[i]/(i+0.425)/de/de
   
    return array, nph

def read_file(file, de):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
    if 'tr' in file and not 'fs' in file:
        print 'removed'
        array, de = adjust_array(array, de)
        array[0] = 0
    for i in range(len(array)):
        nph  += array[i]
    for i in range(len(array)):
#        array[i] *= (i+0.425)
        array[i] /= de
           
    return array, nph

def check_length(file):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
       
    return len(array)

def adjust_array(array, de):
    n = len(array)
    adj_size = 10
    m = n/adj_size
    a = [0]*m
    for i in range(m):
        tmp = 0
        for j in range(adj_size):
            tmp += array[i*adj_size + j]
        a[i] = tmp
    return a, de*adj_size

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def read_one_spectrum(path1, nmin):
    config = utils.get_config(path1 + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV']) 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
    print num, T
    nph_t = []
    nph1_t = []
    ne_t = []
    np_t = []
    step = x0*y0*1e15/T 
    
    dt = y0*float(config['QEDstatistics.OutputIterStep'])*1e15

    path = '/statdata/ph/EnSp/'
    nepath = '/data/NeTrap/'
    nppath = '/data/NposTrap/'
    emin = float(config['QEDstatistics.Emin']) 
    emax = float(config['QEDstatistics.Emax']) 
    ne = int(config['QEDstatistics.OutputN_E'])
    de = (emax - emin)/ne
   
    t = 0
    
    n = check_length(path1 + path + '%.4f.txt'%nmin)
    if 'tr' in path1 and not 'fs' in path1:
        n /= 10
    axis = create_axis(n, de/ev*1e-9)

    spectrum1 = [0]*n
    
    for i in range(nmin,nmin+num):
        name = path1 + path + '%.4f.txt'%i
        print name
        sp1, nph = read_file(name, de)
       
        for k in range(n):
            if nph > 0:
                spectrum1[k] += sp1[k]/nph
    for k in range(n):
        spectrum1[k] /= num
    if 'tr' in path1 and not 'fs' in path1:
        de *= 10
    axis = create_axis(n, de/ev*1e-9)
    return spectrum1, axis

def read_one_spectrum_sp(path1, nmin):
    config = utils.get_config(path1 + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV']) 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
    print num, T
    nph_t = []
    nph1_t = []
    ne_t = []
    np_t = []
    step = x0*y0*1e15/T 
    
    dt = y0*float(config['QEDstatistics.OutputIterStep'])*1e15

    path = '/statdata/ph/EnSpSph/'
    nepath = '/data/NeTrap/'
    nppath = '/data/NposTrap/'
    emin = float(config['QEDstatistics.Emin']) 
    emax = float(config['QEDstatistics.Emax']) 
    ne = int(config['QEDstatistics.OutputN_E'])
    de = (emax - emin)/ne
   
    t = 0
    
    n = check_length(path1 + path + '%.4f.txt'%nmin)
    if 'tr' in path1 and not 'fs' in path1:
        n /= 10
  
    spectrum1 = [0]*n
   
    for i in range(nmin,nmin+num):
        name = path1 + path + '%.4f.txt'%i
        print name
        sp1, nph = read_file_sp(name, de)
       
        for k in range(n):
            if nph > 0:
                spectrum1[k] += sp1[k]/nph
    for k in range(n):
        spectrum1[k] /= num
    if 'tr' in path1 and not 'fs' in path1:
        de *= 10
    axis = create_axis(n, de/ev*1e-9)
    return spectrum1, axis



def main():
    picspath = 'pics'
    num = len(sys.argv) - 1
    n = num/2
    sp = []
    ax = []
    sp_full = []
    ax_full = []
    for k in range(n):
        spectrum, axis = read_one_spectrum(sys.argv[2*k+1], int(sys.argv[2*k+2]))
        spectrum_sp, axis_sp = read_one_spectrum_sp(sys.argv[2*k+1], int(sys.argv[2*k+2]))
        sp.append(spectrum)
        ax.append(axis)
        sp_full.append(spectrum_sp)
        ax_full.append(axis_sp)
        
    config = utils.get_config(sys.argv[1] + "/ParsedInput.txt")
#    x0 = int(config['BOIterationPass'])
#    y0 = float(config['TimeStep']) 
#    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    sp_path = 'spectra_new_%d' % (ppw)
    if not os.path.exists(sp_path):
        os.makedirs(sp_path)
#    ev = float(config['eV']) 
#    T = 2 * math.pi/omega*1e15
#    num = int(T/(x0*y0*1e15))
#    print num, T
#    nph_t = []
#    nph1_t = []
#    ne_t = []
#    np_t = []
#    step = x0*y0*1e15/T 
    
#    nmin = int(sys.argv[2])
#    path = '/statdata/ph/EnSpSph/'
#    nepath = '/data/NeTrap/'
#    nppath = '/data/NposTrap/'
#    emin = float(config['QEDstatistics.Emin']) 
#    emax = float(config['QEDstatistics.Emax']) 
#    ne = int(config['QEDstatistics.OutputN_E'])
#    de = (emax - emin)/ne
#    nmax = utils.num_files(sys.argv[1] + path)
   
#    t = 0
#    n = check_length(sys.argv[1] + path + '%.4f.txt'%nmin)
#    axis = create_axis(n, de/ev*1e-9)

#    spectrum1 = [0]*n
#    for i in range(nmin,nmin+num):
#        name = sys.argv[1] + path + '%.4f.txt'%i
#        print name
#        sp1, nph = read_file(name, de)
#        for k in range(n):
#            spectrum1[k] += sp1[k]/num

#    spectrum2 = [0]*n
#    for i in range(nmin+100,nmin+num+100):
#        name = sys.argv[1] + path + '%.4f.txt'%i
#        print name
#        sp1, nph = read_file(name, de)
#        for k in range(n):
#            spectrum2[k] += sp1[k]/num
    mm = 0
    for k in range(n):
        tmp = len(sp[k])
        for i in range(1,len(sp[k])):
            if sp[k][-i] > 0:
                tmp = i
                print i, sp[k][-i]
                break
            
        if (len(sp[k]) - tmp) * (ax[k][1] - ax[k][0]) > mm:
            mm = (len(sp[k]) - tmp) * (ax[k][1] - ax[k][0])
    mm = 6
    fig = plt.figure(num=None, figsize=(20, 10))
    ax1 = fig.add_subplot(3,1,1)
    for k in range(n):
        lbl = sys.argv[2*k+1]
        ax1.plot(ax[k], sp[k], label = lbl + ' full')
#        ax1.plot(ax_full[k], sp_full[k], label = lbl + ' sphere')
        print sys.argv[2*k+1], sum(sp[k]), sp[k][0]
#    ax1.plot(axis, spectrum2)
#    ax1.set_yscale('log')
    ax1.set_xlim([1e-2, mm])
#    ax1.set_xscale('log')
    ax1.set_yscale('log')
    ax1.legend(loc='upper right', shadow=True)
    ax1 = fig.add_subplot(3,1,2)
    for k in range(n):
        lbl = sys.argv[2*k+1]
#        ax1.plot(ax[k], sp[k], label = lbl + ' full')
        ax1.plot(ax_full[k], sp_full[k], label = lbl + ' sphere')
        print sys.argv[2*k+1], sum(sp[k]), sp[k][0]
#    ax1.plot(axis, spectrum2)
    ax1.set_yscale('log')
#    ax1.set_xscale('log')
    ax1.set_xlim([1e-2, mm])
    ax1.legend(loc='upper right', shadow=True)
    ax1 = fig.add_subplot(3,1,3)
    diff = [0] * len(sp[0])
#    for k in range(n):
#        for i in range(len(sp[0])):
#            diff[i] = sp[k][i] - sp_full[k][i]
#        lbl = sys.argv[2*k+1]
#        ax1.plot(ax[k], diff, label = lbl + ' diff (full - sphere)')
#        print sys.argv[2*k+1], sum(sp[k]), diff[0], diff[1]
    for i in range(len(sp[0])):
        diff[i] = abs(sp[1][i] - sp[0][i])
    ax1.plot(ax[k], diff, label = 'sphere')
    for i in range(len(sp[0])):
        diff[i] = abs(sp_full[1][i] - sp_full[0][i])
    ax1.plot(ax[k], diff, label = 'full')
    ax1.set_xscale('log')
    ax1.set_yscale('log')
#    ax1.plot(axis, spectrum2)
#    ax1.set_yscale('log')
    ax1.set_xlim([1e-2, mm])
    plt.grid()
    ax1.legend(loc='upper right', shadow=True)
    
    plt.savefig(sp_path + '/' + 'sp_%d_%d.png'%(ppw,int(sys.argv[2])))
    plt.close()
    shutil.copy(sys.argv[1]+'/Input.txt', sp_path)
    shutil.make_archive(sp_path, 'zip', sp_path)
#    plt.show()
    
if __name__ == '__main__':
    main()
