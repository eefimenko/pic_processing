#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import utils
import sys
from pylab import *

def norm(array):
    m = abs(max(array))
    if m != 0:
        for i in range(len(array)):
            array[i] /= m
    return array


def main():
    t =[] 
    el_t = []
    ph_t = []
    pos_t = []
    sum_t = []
    f = open('bunches.dat', 'r')
    for line in f:
        tmp = line.split()
        t_ = float(tmp[0])
        t.append(t_)
        if t_< 6.17:
            el_t.append(float(tmp[1]))
            ph_t.append(float(tmp[2]))
            pos_t.append(float(tmp[3]))
            sum_t.append(float(tmp[3])+float(tmp[1]))
        else:
            el_t.append(0)
            ph_t.append(0)
            pos_t.append(0)
            sum_t.append(0)
#    el_t = norm(el_t)
#    ph_t = norm(ph_t)
#    pos_t = norm(pos_t)
    fig = plt.figure(num=None)
    axel = fig.add_subplot(2,1,1)
    axel.plot(t, el_t, 'r', label = 'el')
    axel.plot(t, ph_t, 'b', label = 'ph')
    axel.plot(t, pos_t, 'g', label = 'pos')
    axel.plot(t, sum_t, 'k', label = 'sum')
    plt.legend(loc = 'upper left')
    axel.set_yscale('log')
    axel = fig.add_subplot(2,1,2)
    axel.plot(t, el_t, 'r', label = 'el')
    axel.plot(t, ph_t, 'b', label = 'ph')
    axel.plot(t, pos_t, 'g', label = 'pos')
#    plt.legend(loc = 'upper left')
    axel.set_xlim([5.2,6.2])
    plt.show()

if __name__ == '__main__':
    main()
