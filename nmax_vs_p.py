#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import utils
import sys
from pylab import *

def read_file(path):
    f = open(path, 'r')
    line = f.readline()
    res = [float(x) for x in line.split()]
    return res

def main():
    f03 = 'Ncr03.txt'
    f05 = 'Ncr05.txt'
    fpwr = 'fPwr.txt'
    
    d03 = read_file(f03)
    d05 = read_file(f05)
    dpwr = read_file(fpwr)
    dpwr1 = [10, 15, 20, 50 ,100]
    dnum = [5e24, 8e24, 3e25, 5e25, 2e26]
    fig = plt.figure(num=None)
    ax = fig.add_subplot(1,1,1)
    n03, = ax.plot(dpwr, d03, 'g', label = f03)
    n05, = ax.plot(dpwr, d05, 'b', label = f05)
    nnum, = ax.plot(dpwr1, dnum, 'r', label = 'PIC')
    ax.legend(loc='upper left', shadow=True)
    ax.set_yscale('log')
    plt.show()

if __name__ == '__main__':
    main()
