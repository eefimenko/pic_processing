#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import re

def read_one_file(filename):
    ax = []
    array = []
    array1 = []
    f = open(filename, 'r')
    for line in f:
        tmp = line.split()
        ax.append(float(tmp[0]))
        array.append(float(tmp[2]))
        array1.append(float(tmp[4]))
    return ax, array, array1

def main():
    n = len(sys.argv) - 1
    path = 'energy_corr'
    fig = plt.figure(num=None)
    r1 = []
    r2 = []
    p = []
    ax1 = fig.add_subplot(3,1,1)
    ax2 = fig.add_subplot(3,1,2)
    ax3 = fig.add_subplot(3,1,3)
    for i in range(n):
        ax, r, rr = read_one_file(path + '/' + sys.argv[i+1])
        ax1.plot(r)
        ax2.plot(rr)
        p_ = int(re.search(r'\d+', sys.argv[i+1]).group())
        
        r1.append(r[450])
        r2.append(rr[390])
        p.append(p_)
    ax1.set_xlim([250, 450])
    ax2.set_xlim([250, 450])
    ax3.plot(p, r1, color= 'b')
    ax4 = ax3.twinx()
    ax4.plot(p, r2, color='r')
    f1 = open(path + '/wo_power.dat', 'w')
    f2 = open(path + '/wo_en_1percent.dat', 'w')
    f3 = open(path + '/wo_en_av.dat', 'w')
    for i in range(n):
        f1.write('%lf '%p[i])
        f2.write('%lf '%r1[i])
        f3.write('%lf '%r2[i])
    f1.close()
    f2.close()
    f3.close()
    
    picname = path + '/wo_energy.png'
    plt.savefig(picname)
    plt.show()
    plt.close()
if __name__ == '__main__':
    main()
