#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils
import numpy as np

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def create_subplot(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, text='', text1='', mult=1):
    ax = fig.add_subplot(spx,spy,spi)
#    name = path + '/' + "%06d.txt" % (i,)
    field = utils.bo_file_load(path,i,nx,ny)*mult
    if field == None:
        return
#    maxe = max([max(row) for row in field])
#    mine = min([min(row) for row in field])
    maxe = np.amax(field)
    mine = np.amin(field)
 #   print path, maxe, mine
    ratio = 1e-3
    if maxe == 0:
        maxe = 1
    if maxe == mine:
        v_min_ = maxe
        v_max_ = 1e9*maxe
    else:
        v_max_ = maxe
        v_min_ = maxe*ratio
   
    ticks = [-2,-1,0,1,2]
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = v_min_, vmax = v_max_, norm=clr.LogNorm())
#    ax.text(-2.5, 2.5, text)
    ax.set_title(text + ': %.1e'%(maxe)) 
#    ax.text(1.6, 1.6, text1, fontsize=30)
    ax.set_xlim([-2,2])
    ax.set_ylim([-2,2])
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
    plt.xticks(ticks)
    plt.yticks(ticks)
#    plt.colorbar(surf,  orientation  = 'vertical', ticks = [v_min, v_max])
    return surf
def create_subplot_f(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, text='', text1='', mult=1):
    ax = fig.add_subplot(spx,spy,spi)
#    name = path + '/' + "%06d.txt" % (i,)
    field = utils.bo_file_load(path,i,nx,ny)*mult
    if field == None:
        return
#    maxe = max([max(row) for row in field])
#    v_max_ = max2d(field)
#    print v_max_
    ratio = 1e-3
    ticks = [-2,-1,0,1,2]
#    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = v_max_*ratio, vmax = v_max_)
    maxe = np.amax(field)
    mine = np.amin(field)
#    print path, maxe, mine
    ratio = 1e-6
    if maxe == 0:
        maxe = 1
    if maxe == mine:
        v_min_ = maxe
        v_max_ = 1e9*maxe
    else:
        v_max_ = maxe
        v_min_ = maxe*ratio
  
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = v_min_, vmax = v_max_)
    ax.set_title(text + ': %.2g'%(maxe))
#    ax.text(1.6, 1.6, text1, fontsize=30)
    ax.set_xlim([-2,2])
    ax.set_ylim([-2,2])
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
    plt.xticks(ticks)
    plt.yticks(ticks)
#    plt.colorbar(surf,  orientation  = 'vertical', ticks = [v_min, v_max])
    return surf

 

def find_nmax(dirs, expath):
    nmax = utils.num_files(dirs[0] + ezpath)
    for k in range(1,len(dirs)):
        tmp = utils.num_files(dirs[k] + ezpath)
        if tmp < nmax:
            nmax = tmp
    return nmax

def get_color(j):
    color = ['r', 'g', 'b', 'k', 'c', 'y', 'G', 'B']
    if (j >= len(color)):
        j = j - len(color)
    return color[j]

def main():
    expath = '/data/E2x'
    eypath = '/data/E2y'
    ezpath = '/data/E2z'
    bxpath = '/data/B2x'
    bypath = '/data/B2y'
    bzpath = '/data/B2z'
    nexpath = '/data/Electron2Dx'
    neypath = '/data/Electron2Dy'
    nezpath = '/data/Electron2Dz'
    npxpath = '/data/Positron2Dx'
    npypath = '/data/Positron2Dy'
    npzpath = '/data/Positron2Dz'
    nixpath = '/data/Proton2Dx'
    niypath = '/data/Proton2Dy'
    nizpath = '/data/Proton2Dz'
    npxpath = '/data/Positron2Dx'
    npypath = '/data/Positron2Dy'
    npzpath = '/data/Positron2Dz'
    nphxpath = '/data/Photon2Dx'
    nphypath = '/data/Photon2Dy'
    nphzpath = '/data/Photon2Dz'
    picspath = 'pics_10pw'

    numdirs = int(sys.argv[1])
    dirs = []
    configs = []
    for i in range(numdirs):
        dirs.append(sys.argv[2+i])
        configs.append(utils.get_config(sys.argv[2+i] + "/ParsedInput.txt"))
    delta = 1
    n = numdirs + 1
    num = len(sys.argv) - n
    
    if num == 1:
        print('Plotting all pics')
        nmin = 0	
        nmax = find_nmax(dirs, expath)
    elif num == 2:
        print('Printing from pic ' + sys.argv[1 + n]) 
        nmin = int(sys.argv[1 + n])
        nmax = find_nmax(dirs, expath)
    elif num == 3:
        print('Printing pics from ' + sys.argv[1 + n] + ' to ' + sys.argv[2 + n])
        nmin = int(sys.argv[1 + n])
        nmax = int(sys.argv[2 + n])
    elif num == 4:
        print('Printing pics from ' + sys.argv[1 + n] + ' to ' + sys.argv[2 + n] + ' with delta ' + sys.argv[3 + n])
        print(sys.argv[1 + n])
        nmin = int(sys.argv[1 + n])
        nmax = int(sys.argv[2 + n])
        delta = int(sys.argv[3 + n])
    else:
        nmin = 0
        nmax = 0
        print('Too much parameters')

    print(nmin, nmax, delta)
    
    xmin = []
    xmax = []
    ymin = []
    ymax = []
    zmin = []
    zmax = []
    mult = []
    nx = []
    ny = []
    nz = []
    power = []
    for k in range(numdirs):
        xmax.append(float(configs[k]['X_Max'])*1e4) #mkm
        xmin.append(float(configs[k]['X_Min'])*1e4) #mkm
        ymax.append(float(configs[k]['Y_Max'])*1e4) #mkm
        ymin.append(float(configs[k]['Y_Min'])*1e4) #mkm
        zmax.append(float(configs[k]['Z_Max'])*1e4) #mkm
        zmin.append(float(configs[k]['Z_Min'])*1e4) #mkm
        nx.append(int(configs[k]['MatrixSize_X']))
        ny.append(int(configs[k]['MatrixSize_Y']))
        nz.append(int(configs[k]['MatrixSize_Z']))
        power.append(int(configs[k]['PeakPowerPW']))
        dx = float(configs[k]['Step_X'])*1e4
        dy = float(configs[k]['Step_Y'])*1e4
        dz = float(configs[k]['Step_Z'])*1e4
        mult.append(1/(2.*dx*dy*dz*1e-12))
        step = float(configs[k]['TimeStep'])*1e15*float(configs[k]['BOIterationPass'])
        omega = float(configs[k]['Omega'])
        T = 2 * math.pi/omega
        nt = int(T*1e15/step)
        print(nt)
    spx = numdirs
    spy = 5
    for i in range(nmin,nmax,delta):
        print("\rSaving sc%06d.png, %.0f %% done" % (i,float(i-nmin)/(nmax-nmin)*100))
        sys.stdout.flush()

        fig = plt.figure(num=None, figsize=(20., 20.*spx/spy), dpi=256)
        mp.rcParams.update({'font.size': 14})
        
        for k in range(numdirs):
            
            surf = create_subplot_f(fig,i,dirs[k]+ezpath,nx[k],ny[k],spx,spy,1+k*spy, xmin[k], xmax[k], ymin[k], ymax[k], 0., 0., 'x', 'y', 'Reds', 'Electric field %d pw'%(power[k]))
            surf = create_subplot_f(fig,i,dirs[k]+bzpath,nx[k],ny[k],spx,spy,2+k*spy, xmin[k], xmax[k], ymin[k], ymax[k], 0., 0., 'x', 'y','Reds', 'Magnetic field %d pw'%(power[k]))
            surf = create_subplot(fig,i,dirs[k]+nezpath,nx[k],ny[k],spx,spy,3+k*spy, xmin[k], xmax[k], ymin[k], ymax[k], 0., 0., 'x', 'y','Greens', 'Electrons %d pw'%(power[k]), mult=mult[k])
            surf = create_subplot(fig,i,dirs[k]+npzpath,nx[k],ny[k],spx,spy,4+k*spy, xmin[k], xmax[k], ymin[k], ymax[k], 0., 0., 'x', 'y','Greens', 'Positrons %d pw'%(power[k]),mult=mult[k])
            surf = create_subplot(fig,i,dirs[k]+nphzpath,nx[k],ny[k],spx,spy,5+k*spy, xmin[k], xmax[k], ymin[k], ymax[k], 0., 0., 'x', 'y', 'Blues', 'Photons %d pw'%(power[k]), mult=mult[k])
            
        picname = picspath + '/' + "scz%06d.png" % (i,)
        plt.tight_layout()
        plt.savefig(picname)
        plt.close()

    
if __name__ == '__main__':
    main()
