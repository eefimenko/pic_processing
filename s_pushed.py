#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def find_max(file, mult = 1.):
    max = 0
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]
    for p in array:
        if p > max:
            max = p
    return max

def find_max_from_all_fields(expath, eypath, ezpath, bxpath, bypath, bzpath, i):
    max = 0
    name = expath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = eypath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = ezpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    return max

def find_max_from_all_particles(xpath, ypath, zpath, i, mult = 1.):
    max = 0
    name = xpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    name = ypath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    name = zpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    return max
    
    

def create_subplot(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
    name = path + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny, mult)
    
    maxe = max([max(row) for row in field])
    mine = min([min(row) for row in field])
   
    ratio = 1e-3
    if maxe == mine:
        v_min_ = maxe
        v_max_ = 1e9*maxe
    else:
        v_max_ = maxe
        v_min_ = maxe*ratio
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = v_min_, vmax = v_max_, norm=clr.LogNorm())
    ax.text(0.3, 1.1, 'min=%.1le max=%.1le' % (v_min, maxe), style='italic', horizontalalignment='center', verticalalignment='center', transform = ax.transAxes)
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
    plt.colorbar(surf,  orientation  = 'vertical', ticks = [v_min, v_max])
    return surf

def create_subplot_diff(fig,i,path1,path2,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
    name1 = path1 + '/' + "%06d.txt" % (i,)
    name2 = path2 + '/' + "%06d.txt" % (i,)
    field1 = read_field(name1,nx,ny, mult)
    field2 = read_field(name2,nx,ny, mult)
    field = [[1e-9 for i in range(nx)] for j in range(ny)]
    maxe = max([max(row) for row in field])
    mine = min([min(row) for row in field])
    for i in range(len(field1)):
        for j in range(len(field1[0])):
            field[i][j] = field1[i][j] - field2[i][j]
    ratio = 1e-3
    if maxe == mine:
        v_min_ = maxe
        v_max_ = 1e9*maxe
    else:
        v_max_ = maxe
        v_min_ = maxe*ratio
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = v_min_, vmax = v_max_, norm=clr.LogNorm())
    ax.text(0.3, 1.1, 'min=%.1le max=%.1le' % (v_min, maxe), style='italic', horizontalalignment='center', verticalalignment='center', transform = ax.transAxes)
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
    plt.colorbar(surf,  orientation  = 'vertical', ticks = [v_min, v_max])
    return surf

def create_subplot_f(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
    name = path + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny, mult)
    maxe = max([max(row) for row in field])
    mine = min([min(row) for row in field])
    v_max_ = max2d(field)
    ratio = 1e-3
    if maxe == mine:
        v_min_ = maxe
        v_max_ = 1e9*maxe
    else:
        v_max_ = maxe
        v_min_ = maxe*ratio
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Reds', vmin = v_min_, vmax = v_max_, norm=clr.LogNorm())
    ax.text(-5, 4.5, '%.1le' % maxe, style='italic')
    plt.colorbar(surf, orientation  = 'vertical', ticks = [v_min, v_max])
    return surf

def pulse_value(i,step,amp,tp,delay):
    ans = []
    tau = tp/1.1437
#    delay = 23.1099984185576458468467817
    t = i*step-delay
    if t > 0 and t < math.pi * tau:
        tmp = math.sin(t/tau)
        f = amp*tmp*tmp
    else:
        f = 0.
    return f

def main():
    static = 'pushed_static/'
    dynamic = 'pushed_dynamic/'
    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nixpath = 'data/Proton2Dx'
    niypath = 'data/Proton2Dy'
    nizpath = 'data/Proton2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nphxpath = 'data/Photon2Dx'
    nphypath = 'data/Photon2Dy'
    nphzpath = 'data/Photon2Dz'
    picspath = 'pics'

    config = utils.get_config(static + "ParsedInput.txt")

    BOIterationPass = float(config['BOIterationPass'])
    dt = float(config['TimeStep'])*1e15*BOIterationPass #fs
    maxpower = float(config['PeakPower'])*1e-7*1e-15 #PW
#    duration = float(config['Duration'])*1e15 #fs
#    delay = float(config['delay'])*1e15 #fs

    print "Power = " + str(maxpower) + 'PW'
    print "dt = " + str(dt) + 'fs'
#    print "duration = " + str(duration)
#    print "delay = " + str(delay)
    
    Xmax = float(config['X_Max'])*1e4 #mkm
    Xmin = float(config['X_Min'])*1e4 #mkm
    Ymax = float(config['Y_Max'])*1e4 #mkm
    Ymin = float(config['Y_Min'])*1e4 #mkm
    Zmax = float(config['Z_Max'])*1e4 #mkm
    Zmin = float(config['Z_Min'])*1e4 #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz

    step = (Xmax-Xmin)/nx

    mult = 1/(2.*dx*dy*dz*1e-12)

    delta = 1

    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(expath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(expath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax) + ' files'

    maxf = 0
    maxe = 0
    maxi = 0
    maxp = 0
    maxph = 0
    print 'Reading files'

    for i in range(0,0):
        print "\r%.0f %% done" % (float(i)/nmax*100),
        sys.stdout.flush()        
        tmpf = find_max_from_all_fields(expath, eypath, ezpath, bxpath, bypath, bzpath, i)
        if tmpf > maxf:
            maxf = tmpf
        tmpe = find_max_from_all_particles(nexpath, neypath, nezpath, i, mult)
        if tmpe > maxe:
            maxe = tmpe
        tmpi = find_max_from_all_particles(nixpath, niypath, nizpath, i, mult)
        if tmpi > maxi:
            maxi = tmpi
        tmpp = find_max_from_all_particles(npxpath, npypath, npzpath, i, mult)
        if tmpp > maxp:
            maxp = tmpp
        tmpph = find_max_from_all_particles(nphxpath, nphypath, nphzpath, i, mult)
        if tmpph > maxph:
            maxph = tmpph
#    print maxf, maxe, maxi, maxp, maxph
    if maxi > maxe:
        maxe = maxi

    if maxp > maxe:
        maxe = maxp
    maxph += 1
#    maxf = 0
    fmin = maxf*1e-6
    fmax = maxf
#    cmin = maxe*1e-5
    cmax = maxe
    cmin = maxe*1e-6
    cimax = maxi
    cimin = maxi*1e-6
#    cmax = 4.5e25
    phmin = maxph*1e-6
    phmax = maxph

    for i in range(nmin,nmax,delta):
        print "\rSaving diff%06d.png, %.0f %% done" % (i,float(i-nmin)/(nmax-nmin)*100),
        sys.stdout.flush()

        fig = plt.figure(num=None, figsize=(20, 15), dpi=256)
        mp.rcParams.update({'font.size': 8})
        
        s = create_subplot(fig,i,static+expath,nx,ny,3,4,1, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'z', 'y', 'Blues')
        s = create_subplot(fig,i,static+eypath,nx,ny,3,4,5, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'z', 'x', 'Blues')
        s = create_subplot(fig,i,static+ezpath,nx,ny,3,4,9, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'y', 'x', 'Blues')
        
        s = create_subplot(fig,i,dynamic+expath,nx,ny,3,4,2, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'z', 'y','Greens')
        s = create_subplot(fig,i,dynamic+eypath,nx,ny,3,4,6, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'z', 'x', 'Greens')
        s = create_subplot(fig,i,dynamic+ezpath,nx,ny,3,4,10, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'y', 'x', 'Greens')

#        s = create_subplot_diff(fig,i,dynamic+expath,static+expath,nx,ny,3,6,3, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'z', 'y','Reds')
#        s = create_subplot_diff(fig,i,dynamic+eypath,static+eypath,nx,ny,3,6,9, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'z', 'x', 'Reds')
#        s = create_subplot_diff(fig,i,dynamic+ezpath,static+ezpath,nx,ny,3,6,15, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'y', 'x', 'Reds')
        
        s = create_subplot(fig,i,static+bxpath,nx,ny,3,4,3, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'z', 'y', 'Blues')
        s = create_subplot(fig,i,static+bypath,nx,ny,3,4,7, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'z', 'x', 'Blues')
        s = create_subplot(fig,i,static+bzpath,nx,ny,3,4,11, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'y', 'x', 'Blues')
        
        s = create_subplot(fig,i,dynamic+bxpath,nx,ny,3,4,4, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'z', 'y','Greens')
        s = create_subplot(fig,i,dynamic+bypath,nx,ny,3,4,8, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'z', 'x', 'Greens')
        s = create_subplot(fig,i,dynamic+bzpath,nx,ny,3,4,12, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'y', 'x', 'Greens')
        
#        s = create_subplot_diff(fig,i,dynamic+bxpath,static+bxpath,nx,ny,3,6,6, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'z', 'y','Reds')
#        s = create_subplot_diff(fig,i,dynamic+bypath,static+bypath,nx,ny,3,6,12, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'z', 'x', 'Reds')
#        s = create_subplot_diff(fig,i,dynamic+bzpath,static+bzpath,nx,ny,3,6,18, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'y', 'x', 'Reds')
#        f = pulse_value(i, dt, 1., duration, delay)
#        power = f*f*maxpower
        
#        plt.figtext(0.2,0.93,str('Power in linear centre =%.3e PW' % (power)) , fontsize = 10)
        picname = picspath + '/' + "s%06d.png" % (i,)
        
        plt.savefig(picname)
        plt.close()

if __name__ == '__main__':
    main()
