#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import sys
import math
import os, os.path
import utils

def transpose(array):
    r = len(array)
    c = len(array[0])
    res = [None]*c
    for i in range(c):
        res[i] = [None]*r
        for j in range(r):
            res[i][j] = array[j][i]
    return res

#def num_files(dirname):
#    num = len([name for name in os.listdir(dirname) if os.path.isfile(dirname + '/' +name)])
#    return num

def read_field(file):
    f = open(file, 'r')
    tmp = [[float(x)+1e-9 for x in line.split()] for line in f]
    f.close()
    return tmp[0]

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def read_field2d(file,nx,ny):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = j + i*nx
            row.append(array[index])
        field.append(row)
    return field

def create_pulse(n,step,amp, tp, delay):
    dt = step*2.7
    ans = []
    tau = tp/1.1437
#    delay = 23.1099984185576458468467817
    for i in range(n):
        t = i*dt-delay
        if t > 0 and t < math.pi * tau:
            tmp = math.sin(t/tau)
            f = amp*tmp*tmp
        else:
            f = 0.
        ans.append(f)
    return ans

def normList(L, normalizeTo=1):
    '''normalize values of a list to make its max = normalizeTo'''

    vMax = max(L)
    return [ x/(vMax*1.0)*normalizeTo for x in L]
#def get_config(file):
#    searchfile = open(file, "r")
#    myvars = {}
#    for line in searchfile:
#        if "Setting" in line and "variable" in line and '=' in line:
#            tmp, var = line.partition("=")[::2]
#            rem, name = tmp.partition(":")[::2]
#            myvars[name.strip()] = var
#    searchfile.close()
#    return myvars

def main():

    config = utils.get_config("ParsedInput.txt")

    dt = float(config['TimeStep'])
    ev = float(config['eV'])
    omega = float(config['Omega'])
    duration = float(config['Duration']) #s
    delay = float(config['delay']) #s
    power = float(config['PeakPower'])*1e-7 #W
    Emax = float(config['QuantumParticles.Emax'])
    Emin = float(config['QuantumParticles.Emin'])
    N_E = int(config['QuantumParticles.OutputN_E'])
    N_Phi = int(config['QuantumParticles.OutputN_phi'])
    N_Th = int(config['QuantumParticles.OutputN_theta'])
    outStep = int(config['QuantumParticles.OutputIterStep'])
    tp = duration*1e15
    tdelay = delay*1e15

    path='statdata'
    elpath='/el/'
    elmaxpath='/el_max/'
    posmaxpath='/pos_max/'
    phmaxpath='/ph_max/'
    elsmaxpath='/elSmax/'
    possmaxpath='/posSmax/'
    phsmaxpath='/phSmax/'
    elnmaxpath='/elNmax/'
    posnmaxpath='/posNmax/'
    phnmaxpath='/phNmax/'
    pospath = '/pos/'
    phpath = '/ph/'
    phangle = '/ph_angle/'
    pht = '/phT/'
    picspath='pics/' 

    nmax = utils.num_files(path+elpath)
    
    print 'Found ' + str(nmax) + ' files'

    el_t = []
    pos_t = []
    ph_t = []
    phT = []
    
    T = 2 * utils.pi/omega
    time = dt*outStep/T
    en_step = (Emax-Emin)/N_E/ev*1e-9
    emax = Emax/ev*1e-9
    maxindex = 0
    maxphindex = 0
    maxel = 0.
    maxph = 0.
   
    axis = create_axis(N_E, en_step)
    
    ph_razv = []
    ph_en_razv = []
    num_ph = []
    dnum_ph = []
    tmaxph = 0

    ibegin = 320
    iend   = 400

    aver_spectra = [0]*N_E
    aver_spectra1 = [0]*N_E

    for i in range(nmax):
        name=round(i*time, 4)
        print name
        filename = path + elpath + str("%.4f" % (name, )) + '.txt'
        array = read_field(filename)
        tmp = max(array)
        if tmp > maxel:
            maxel = tmp
        l = [j for j, e in enumerate(array) if e != 1e-9]
        l.append(0)
        tmpindex = max(l)
        
        if tmpindex > maxindex:
            maxindex = tmpindex
            print 'el ' + str(tmpindex) + ' i =' + str(i) 

        filename = path + pospath + str("%.4f" % (name, )) + '.txt'
        array = read_field(filename)
        tmp = max(array)
        if tmp > maxel:
            maxel = tmp
        l = [j for j, e in enumerate(array) if e != 1e-9]
        l.append(0)
        tmpindex = max(l)
        if tmpindex > maxindex:
            maxindex = tmpindex 
            print 'pos ' + str(tmpindex) 

        filename = path + phpath + str("%.4f" % (name, )) + '.txt'
        array = read_field(filename)
        tmp = max(array)
        if tmp > maxph:
            maxph = tmp
            tmaxph = i*time
        l = [j for j, e in enumerate(array) if e != 1e-9]
        tmp = 0.
        for j,e in enumerate(array):
            print j*en_step 
            if (e != 1e-9 and j*en_step > 1):
                tmp += e
               
        num_ph.append(tmp)
        dnum_ph.append(0)

        l.append(0)
        tmpindex = max(l)
        if tmpindex > maxindex:
            maxindex = tmpindex 
            print 'ph ' + str(tmpindex) 
        if tmpindex > maxphindex:
            maxphindex = tmpindex
        
        
        if i > ibegin and i < iend:
            summ = sum(array)
            array1 = normList(array, 1./summ)
            for j, el in enumerate(array1):
                aver_spectra[j] += el/(iend-ibegin)*(j+0.5)*en_step
            for j, el in enumerate(array):
                aver_spectra1[j] += el/(iend-ibegin)*(j+0.5)*en_step
    
    for j,e in enumerate(num_ph):
        if j > 2 and j < len(num_ph) - 2:
            dnum_ph[j] = (num_ph[j+1] - num_ph[j])/(time*T) 

    print dnum_ph

    fig, ax1 = plt.subplots()
    pe, = ax1.plot(dnum_ph, 'g')
    picname = picspath + "num_ph.png"
    ax1.set_yscale('log')
    plt.savefig(picname)
    plt.close()

    max_ph_en_t = 0
    min_ph_en_t = 1e20
    print maxel
    for i in range(1,nmax):
        name=round(i*time, 4)
        filename = path + elpath + str("%.4f" % (name, )) + '.txt'
        print filename
        array = read_field(filename)
        l = [j for j, e in enumerate(array) if e != 1e-9]
        l.append(0)
        tmpindex = max(l)
        elenmax = tmpindex*en_step
        s = sum(array)
        el_t.append(s)
        
        filename = path + pospath + str("%.4f" % (name, )) + '.txt'
        print filename
        array1 = read_field(filename)
        l = [j for j, e in enumerate(array1) if e != 1e-9]
        l.append(0)
        tmpindex = max(l)
        posenmax = tmpindex*en_step
        s = sum(array1)
        pos_t.append(s)

        filename = path + phpath + str("%.4f" % (name, )) + '.txt'
        print filename
        array2 = read_field(filename)
        array3 = array2[:]
        for j in range(len(array3)):
            array3[j] *= (j+0.5)*en_step
            if array3[j] > max_ph_en_t:
                max_ph_en_t = array3[j]
            if array3[j] < min_ph_en_t and array3[j] != 1e-9:
                min_ph_en_t = array3[j]
        

        l = [j for j, e in enumerate(array2) if e != 1e-9]
        l.append(0)
        tmpindex = max(l)
        phenmax = tmpindex*en_step
        s = sum(array2)
        ph_t.append(s)
        ph_razv.append(array2)
        ph_en_razv.append(array3)

        fig, ax1 = plt.subplots()
        
        pe, = ax1.plot(axis, array, 'g')
        pp, = ax1.plot(axis, array1, 'r')
        ax1.set_yscale('log')
        ax1.set_ylim(maxel*1e-5, maxel)
        ax1.set_xlabel('Energy, GeV')
        ax1.set_ylabel('Number of electrons, positrons')
        ax2 = ax1.twinx()
        ph, = ax2.plot(axis, array2, 'b')
        ax2.set_yscale('log')
        ax2.set_ylim(1e5, maxph)
        ax2.set_ylabel('Number of photons')
        picname = picspath + "el%06d.png" % (i,)
        plt.figtext(0.12,0.93,str('t=%.2f T. Max energy: electron=%.3f GeV; positron=%.3f GeV; photon=%.3f GeV ' % (name, elenmax, posenmax, phenmax)) , fontsize = 10)
        plt.legend([pe, pp, ph], ['Electron', 'Positron', 'Photon'], loc=1)
        plt.xlim([0, maxindex*en_step])
        plt.savefig(picname)
        plt.close()

    fig, ax1 = plt.subplots()
    pe, = ax1.plot(axis, aver_spectra, 'g')
    picname = picspath + "aver_spectra.png"
    ax1.set_yscale('log')
    plt.savefig(picname)
    plt.close()

    fig, ax1 = plt.subplots()
    pe, = ax1.plot(axis, aver_spectra1, 'g')
    picname = picspath + "aver_spectra1.png"
    ax1.set_yscale('log')
    plt.savefig(picname)
    plt.close()

    asfile = open('averspectra.dat', 'w')
    as1file = open('averspectra1.dat', 'w')

    for el in aver_spectra:
        asfile.write(str(el) + '\n')
    for el in aver_spectra1:
        as1file.write(str(el) + '\n')

    asfile.close()
    as1file.close()

    fig = plt.figure(num=None, figsize=(20, 10), dpi=256)
    mp.rcParams.update({'font.size': 16})
    ax1 = fig.add_subplot(1,1,1)
    pe, = ax1.plot(el_t, 'g')
    pp, = ax1.plot(pos_t, 'r')
    ax2 = ax1.twinx()
    ph, = ax2.plot(ph_t, 'b')
    pulse = create_pulse(nmax, time, max(ph_t)+1, tp, tdelay)
    pu, = ax2.plot(pulse, 'k')
    picname = picspath + "el_t.png"
    plt.legend([pe, pp, ph, pu], ['Electron', 'Positron', 'Photon', 'Pulse envelope'], loc=1)
    ax1.set_xlabel('Time, periods')
    ax1.set_ylabel('Number of electrons, positrons')
    ax2.set_ylabel('Number of photons')
    plt.savefig(picname)
    plt.close()

    fig = plt.figure(num=None, figsize=(20, 10), dpi=256)
    mp.rcParams.update({'font.size': 16})
    picname = picspath + "ph_t_energy.png"
    ax = fig.add_subplot(1,1,1)
    y = create_axis(nmax, time)
    x = create_axis(maxindex, en_step)

    surf = ax.imshow(transpose(ph_razv), extent=[0, (nmax-1)*time, 0, emax], norm=clr.LogNorm(), vmin = maxph*1e-8, vmax=maxph, aspect='auto', origin='lower')
    max_ph_energy=maxphindex*en_step
    plt.figtext(0.5,0.5,str('Max energy: %.3f GeV t=%.1f periods ' % (max_ph_energy, tmaxph)) , fontsize = 16, color='white')
    ax.set_xlabel('Time, periods')
    ax.set_ylabel('Photon energy, GeV')
    plt.ylim([0, maxphindex*en_step])
    plt.colorbar(surf, orientation  = 'vertical')
    plt.savefig(picname)
    plt.close()

    fig = plt.figure(num=None, figsize=(20, 10), dpi=256)
    mp.rcParams.update({'font.size': 16})
    picname = picspath + "ph_en_t_energy.png"
    ax = fig.add_subplot(1,1,1)
    y = create_axis(nmax, time)
    x = create_axis(maxindex, en_step)

    surf = ax.imshow(transpose(ph_en_razv), extent=[0, (nmax-1)*time, 0, emax], norm=clr.LogNorm(), vmin = max_ph_en_t*1e-6, vmax = max_ph_en_t, aspect='auto', origin='lower')
    max_ph_energy=maxphindex*en_step
    plt.figtext(0.5,0.5,str('Max energy: %.3f GeV t=%.1f periods ' % (max_ph_energy, tmaxph)) , fontsize = 16, color='white')
    ax.set_xlabel('Time, periods')
    ax.set_ylabel('Photon energy, GeV')
    plt.ylim([0, maxphindex*en_step])
    plt.colorbar(surf, orientation  = 'vertical')
    plt.savefig(picname)
    plt.close()

    for i in range(1,nmax):
        name=round(i*time, 4)
        filename = path + phangle + str("%.4f" % (name, )) + '.txt'
        print filename
        array3 = read_field2d(filename, N_Phi, N_Th)
        fig1 = plt.figure(num=None, figsize=(20, 10), dpi=256)
        mp.rcParams.update({'font.size': 14})
        ax = fig1.add_subplot(1,1,1)
        ax.set_xlabel('Phi')
        ax.set_ylabel('Theta')
        picname = picspath + "ph_angle%06d.png" % (i,)
        surf = ax.imshow(array3, extent = [0, 360, 0, 180], cmap = 'jet')
        plt.colorbar(surf,  orientation  = 'vertical')
        plt.figtext(0.12,0.93,str('t=%.2f T' % name) , fontsize = 16)
        plt.savefig(picname)
        plt.close()

#        filename = path + elmaxpath + str("%.4f" % (name, )) + '.txt'
#        print filename
#        array3 = read_field(filename)
#        filename = path + posmaxpath + str("%.4f" % (name, )) + '.txt'
#        print filename
#        array4 = read_field(filename)
#        filename = path + phmaxpath + str("%.4f" % (name, )) + '.txt'
#        print filename
#        array5 = read_field(filename)
#        fig, ax1 = plt.subplots()
#        pe, = ax1.plot(array3, 'g')
#        pp, = ax1.plot(array4, 'r')
#        ax2 = ax1.twinx()
#        ph, = ax2.plot(array5, 'b')
#        plt.legend([pe, pp, ph], ['Electron', 'Positron', 'Photon'], loc=1)
#        picname = picspath + "el_max%06d.png" % (i,)
#        plt.savefig(picname)
#        plt.close()

#        filename = path + elsmaxpath + str("%.4f" % (name, )) + '.txt'
#        print filename
#        array3 = read_field(filename)
#        filename = path + possmaxpath + str("%.4f" % (name, )) + '.txt'
#        print filename
#        array4 = read_field(filename)
#        filename = path + phsmaxpath + str("%.4f" % (name, )) + '.txt'
#        print filename
#        array5 = read_field(filename)
#        fig, ax1 = plt.subplots()
#        pe, = ax1.plot(array3, 'g')
#        pp, = ax1.plot(array4, 'r')
#        ax2 = ax1.twinx()
#        ph, = ax2.plot(array5, 'b')
#        plt.legend([pe, pp, ph], ['Electron', 'Positron', 'Photon'], loc=1)
#        picname = picspath + "el_smax%06d.png" % (i,)
#        plt.savefig(picname)
#       plt.close()        

#        filename = path + elnmaxpath + str("%.4f" % (name, )) + '.txt'
#        print filename
#        array3 = read_field(filename)
#        filename = path + posnmaxpath + str("%.4f" % (name, )) + '.txt'
#        print filename
#        array4 = read_field(filename)
#        filename = path + phnmaxpath + str("%.4f" % (name, )) + '.txt'
#        print filename
#        array5 = read_field(filename)
#        fig, ax1 = plt.subplots()
#        pe, = ax1.plot(array3, 'g')
#        pp, = ax1.plot(array4, 'r')
#        ax2 = ax1.twinx()
#        ph, = ax2.plot(array5, 'b')
#        plt.legend([pe, pp, ph], ['Electron', 'Positron', 'Photon'], loc=1)
#        picname = picspath + "el_nmax%06d.png" % (i,)
#        plt.savefig(picname)
#        plt.close()
        
        filename = path + pht + str("%.4f" % (name, )) + '.txt'
        print filename
        array = read_field(filename)
        s = sum(array)
        phT.append(s)

    en = max(phT)*1e-7
    pulse_energy = 1.05*duration*power
    print en, pulse_energy, en/pulse_energy
    fig, ax1 = plt.subplots()
    pe, = ax1.plot(phT, 'r')
    pulse = create_pulse(nmax, time, 1, tp, tdelay)
    ax2 = ax1.twinx()
    pu, = ax2.plot(pulse, 'g')
    picname = picspath + "el_t.png"
    plt.legend([pe, pu], ['Ph. energy', 'Pulse'], loc=2)
    ax1.set_xlabel('Time, periods')
    ax1.set_ylabel('Full photon energy, erg')
    plt.figtext(0.2,0.93,str('Pulse energy=%.1f J; Photon energy=%.1lf J' % (pulse_energy, en)) , fontsize = 10)
    plt.figtext(0.6,0.5,str('Efficiency=%.1lf%%' % (en/pulse_energy*100)) , fontsize = 16)
    picname = picspath + "phT.png"
    plt.savefig(picname)
    plt.close()

if __name__ == '__main__':
    main()
