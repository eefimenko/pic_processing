#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np
import matplotlib.colors as clr

def main():
    path = '.'
    config = utils.get_config(path + "/ParsedInput.txt")
    ev = float(config['eV'])
    ne = int(config['QEDstatistics.OutputN_E'])
    nt = int(config['QEDstatistics.OutputN_theta'])
    if 'QEDstatistics.ThetaMax' in config:
        tmax = float(config['QEDstatistics.ThetaMax'])*180./math.pi
    else:
        tmax = 0.
    if 'QEDstatistics.ThetaMin' in config:
        tmin = float(config['QEDstatistics.ThetaMin'])*180./math.pi
    else:
        tmin = 0.
#    tmax = float(config['QEDstatistics.ThetaMax'])*180./math.pi
#    tmin = float(config['QEDstatistics.ThetaMin'])*180./math.pi
    emax = float(config['QEDstatistics.Emax'])/ev*1e-9
    emin = float(config['QEDstatistics.Emin'])/ev*1e-9
    de = (emax - emin)/ne
    dt = (tmax - tmin)/nt
    nmin,nmax,delta = utils.get_min_max_iteration(sys.argv,path + utils.elenangsphpath)
    print nmin, nmax, delta

    res = utils.bo_file_load(path + utils.elenangsphpath,nmin,fmt='%.4f',verbose = 1)
    for i in range(nmin+1,nmax,delta):
        res += utils.bo_file_load(path + utils.elenangsphpath,i,fmt='%.4f',verbose = 1)
    name = path + '/el_spectra.txt'
    res.tofile(name,' ', '%.6e')
    fig = plt.figure(num=None, figsize=(20, 5))
    ax1 = fig.add_subplot(1,4,1)
    el = np.reshape(res, (ne,nt), order = 'F')
    ax1.imshow(el, aspect = 'auto',  extent = [tmin, tmax, emin, emax], origin = 'lower', norm = clr.LogNorm(max(res)*1e-3,max(res)))
    axx = fig.add_subplot(1,4,2, polar=True)
    axy = fig.add_subplot(1,4,3)
    axz = fig.add_subplot(1,4,4)
    azimuths_p = np.radians(np.linspace(tmin, tmax, nt))
    azimuths_p1 = np.radians(np.linspace(90+tmax, 90+tmin, nt))
    azimuths_n = -np.radians(np.linspace(tmin, tmax, nt))
    azimuths_n1 = -np.radians(np.linspace(90+tmax, 90+tmin, nt))
    zeniths = np.arange(emin, emax, de)
        
    r_p,thetas_p = np.meshgrid(zeniths, azimuths_p)
    r_p1,thetas_p1 = np.meshgrid(zeniths, azimuths_p1)
    r_n,thetas_n = np.meshgrid(zeniths, azimuths_n)
    r_n1,thetas_n1 = np.meshgrid(zeniths, azimuths_n1)
    x = np.array(el).transpose()
       
    axx.pcolormesh(thetas_p, r_p, x, norm=clr.LogNorm(x.max()*1e-3, x.max()),shading='gouraud')
    axx.pcolormesh(thetas_p1, r_p1, x, norm=clr.LogNorm(x.max()*1e-3, x.max()),shading='gouraud')
    axx.pcolormesh(thetas_n, r_n, x, norm=clr.LogNorm(x.max()*1e-3, x.max()),shading='gouraud')
    axx.pcolormesh(thetas_n1, r_n1, x, norm=clr.LogNorm(x.max()*1e-3, x.max()),shading='gouraud')
    axy.pcolormesh(thetas_p, r_p, x)
    axy.pcolormesh(thetas_p1, r_p1, x)
    axy.pcolormesh(thetas_n, r_n, x)
    axy.pcolormesh(thetas_n1, r_n1, x)
    axx.grid()    

#    axx.set_rscale('log')
    axx.set_ylim([0,emax])
    

    plt.show()
if __name__ == '__main__':
    main()
