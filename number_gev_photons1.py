#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import shutil
import os
import scipy.signal.signaltools as sigtool
import numpy

def read_field2d(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    return sum(tmp1)

def read_file_sph(file, de, de1, emin=0.):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
        
    for i in range(len(array)):
        if (i+0.425)*de > emin:
            nph  += array[i]/((i+0.425)*de1)
   
    return nph

def read_one_spectrum_sph(path1, sh, pp, e=0., nmin=130):
    config = utils.get_config(path1 + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV'])
    n0 = float(config['Ne']) 
    T = 2 * math.pi/omega*1e15
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    num = utils.num_files(path1 + utils.enspsphpath)
    print 'Found ' + str(num) + ' files'
    nph_t = []
    ntr_t = []
    axis = []
    axis1 = []
    axis_shift = []
    number_photons = []
    step = x0*y0*1e15 
    emin = float(config['QEDstatistics.Emin']) 
    emax = float(config['QEDstatistics.Emax']) 
    ne = int(config['QEDstatistics.OutputN_E'])
    de = (emax - emin)/ne/ev/1e9
    de1 = (emax - emin)/ne
    print ev*1e9, 1/(ev*1e9)
    wl = float(config['Wavelength'])
    coeff = 10./(3.3e11*3.3e11)*0.97399*(wl/0.8e-4)*(wl/0.8e-4)
    dmy = int(0.35*wl/dy)
    shift = -int(sh)
    nn = 0
    nnn = 0
    print nmin, nmin+num, e
    for i in range(nmin,nmin+num-2):
#        ezname = path1 + utils.ezpath + '/' + "%06d.txt" % (i,)
#        bzname = path1 + utils.bzpath + '/' + "%06d.txt" % (i,)
        ez = utils.bo_file_load(path1 + utils.ezpath,i,nx,ny, verbose=1)
        bz = utils.bo_file_load(path1 + utils.bzpath,i,nx,ny, verbose=1)
#        ez = read_field2d(ezname,nx,ny)
#        bz = read_field2d(bzname,nx,ny)
        ezv = ez[nx/2][ny/2]
        bzv = bz[nx/2][ny/2-dmy]*1.53
        tmp = (ezv*ezv + bzv*bzv)*coeff
        
        if tmp > 0.993*ppw:
            nnn = 1
            name = path1 + utils.enspsphpath + '%.4f.txt'%i
            print name
            ntrapped = read_trap(path1 + utils.nepath + '%06d.txt' % (i,))
            nph = read_file_sph(name, de, de1, e)
            nn = nph
            nph_t.append(nn)
            ntr_t.append(ntrapped)
            axis.append(i*step)
            axis1.append((i-shift)*step)
            print tmp,nn, ntrapped
        else:
            print i
            if nnn == 1:
                break
    
    if 'pulse' in path1:
        f = open(pp + '/pulse_%dpw_%.0le_%dgev_%.2lf.dat'%(ppw, n0, int(e), sh/15.), 'w')
        label = 'pulse_%d_n0_%.0le_%.2lf'%(ppw, n0, sh/15.)
    else:
        f = open(pp + '/semiinf_%dpw_%dgev_%.2lf.dat'%(ppw, int(e), sh/15.), 'w')
        label = 'semiinf_%d_n0_%.0le_%.2lf'%(ppw, n0, sh/15.)
    for i in range(len(nph_t)):
        if i-shift < len(nph_t):
            f.write("%lf %le %le %le\n"%(axis[i],nph_t[i-shift], ntr_t[i], nph_t[i-shift]/(2.*ntr_t[i])))
            axis_shift.append(axis[i])
            number_photons.append(nph_t[i-shift]/(2.*ntr_t[i]))
    f.close()
    return axis, nph_t, axis1, ntr_t, axis_shift, number_photons, label, ppw

def main():
    picspath = 'pics'
    respath = 'photons1'
    fig1 = plt.figure(num=None, figsize=(10, 10))
    ax11 = fig1.add_subplot(2,1,1)
    ax21 = fig1.add_subplot(2,1,2)
    for sh in range(27,34):
        n0_ = []
        n1_ = []
        ppw_ = []
        
        f = open(respath + '/number_of_photons_%.2lf.dat' % (sh/15.), 'w')
        for k in range(1,len(sys.argv)):
            mp.rcParams.update({'font.size': 16})
            fig = plt.figure(num=None, figsize=(30, 10))
            ax1 = fig.add_subplot(3,1,1)
            ax2 = fig.add_subplot(3,1,2)
            ax4 = fig.add_subplot(3,1,3)
    
            path = sys.argv[k]
            axis, nph_t, axis1, ntr_t, axis_shift0, number_photons0, label, ppw = read_one_spectrum_sph(path, sh, respath, 0.)
            ax1.plot(axis, nph_t, label = label)
            ax1.set_yscale('log')
#            ax1.set_xlim([0, 120])
            ax1.set_ylim([1e-10, 1e12])
            ax4.plot(axis_shift0, number_photons0, label = '0 GeV')
            ax3 = ax1.twinx()
            ax3.plot(axis1, ntr_t, 'r', label = label)
            ax3.set_yscale('log')
            ax3.set_ylim([1e-10, 1e12])
#            ax3.set_xlim([0, 120])
            axis, nph_t, axis1, ntr_t, axis_shift1, number_photons1, label, ppw = read_one_spectrum_sph(path, sh, respath, 1.)
            ax2.plot(axis, nph_t, label = label)
            ax2.set_yscale('log')
#            ax2.set_xlim([0, 120])
            ax4.plot(axis_shift1, number_photons1, label = '1 GeV')
            ax4.set_yscale('log')
            ax4.set_ylim([1e-5, 1e2])
            n = 0
        
            num0 = 0
            num1 = 0
            for i in range(len(number_photons0)):
                print i, number_photons0[i], number_photons1[i]
                num0 += number_photons0[i]
                num1 += number_photons1[i]
                n += 1
            print num0, num1, n
            if n > 0:
                num0 = 15*num0/n
                num1 = 15*num1/n
            f.write('%.0lf %.3le %.3le\n'%(ppw, num0, num1))
            n0_.append(num0)
            n1_.append(num1)
            ppw_.append(ppw)
            plt.legend()
            plt.savefig(respath + '/' + label + '.png')
            plt.close()
        f.close()
        fig = plt.figure(num=None, figsize=(10, 10))
        ax1 = fig.add_subplot(1,1,1)
        n0, = ax1.plot(ppw_, n0_, 'b')
        ax2 = ax1.twinx()
        n1, = ax2.plot(ppw_, n1_, 'r')
        ax1.set_ylabel('Number of radiated photons')
        ax2.set_ylabel('Number of radiated photons > 1Gev')
        ax1.set_xlabel('Power, GeV')
        plt.legend([n0, n1], ['0 GeV(left)', '1 GeV(right)'])
        plt.savefig(respath + '/nphotons_%.2lf.png' % (sh/15.))
        plt.close()
        
        n0, = ax11.plot(ppw_, n0_, label='%.2lf'%(sh/15.))
        n1, = ax21.plot(ppw_, n1_, label='%.2lf'%(sh/15.))
        ax11.set_ylabel('Number of radiated photons')
        ax21.set_ylabel('Number of radiated photons > 1Gev')
        ax11.set_xlabel('Power, GeV')
        ax21.set_xlabel('Power, GeV')
        plt.legend(loc='lower right')
    plt.savefig(respath + '/nphotons.png')
    plt.close()
if __name__ == '__main__':
    main()
