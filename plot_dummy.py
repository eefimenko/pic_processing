#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_file(path):
    f = open(path + '/ParticleTracking/1.txt', 'r')
    res = []
    ax = []
    for line in f:
        tmp = line.split()
        if 'analitycal' in path: 
            ax.append(int(tmp[0])+1500)
        else:
            ax.append(int(tmp[0]))
        res.append(float(tmp[6]))
    return ax,res

def norm(a):
    m = max(a)
    if m != 0:
        for i in range(len(a)):
            a[i] /= m
    return a

def main():
    n = len(sys.argv) - 1
    fig, ax1 = plt.subplots()
    for i in range(n):
        path = sys.argv[i+1]
        ax,res = read_file(path)
        p, = ax1.plot(ax, norm(res), label=path)
	ax1.set_xlim([1400, 1600])
        plt.legend(loc='lower left')
    
    plt.show()

if __name__ == '__main__':
    main()
