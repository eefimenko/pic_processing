#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import sys
import math
import utils
import shutil
import os
import numpy as np

def read_bd(file):
    f = open(file, 'r')
    line = f.readline()
    tmp = [float(x) for x in line.split()]
    s = sum(tmp)
#    s = 1.
    if s > 0:
        for i in range(len(tmp)):
            tmp[i] /= s #* (i+0.5)
    return tmp

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def read_field2d(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]
    print nx,ny
    field = []
    prof = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index])
        field.append(row)

    for j in range(ny):
        ans = 0
        for i in range(nx):
            index = i + j*nx
            ans +=array[index]
        prof.append(ans)
        
    return field, prof

def main():

    config = utils.get_config("ParsedInput.txt")
    bpath = 'statdata/ph/PhGBirth/'
    dpath = 'statdata/ph/PhGBirthR/'
    ezpath = 'data/E2z'
    bzpath = 'data/B2z'
    picspath = 'pics'
   
    delta = 1
    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(bpath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(bpath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax) + ' files'

    nx = int(config['QEDstatistics.nX'])
    ny = int(config['QEDstatistics.nGam'])
    xmax = float(config['QEDstatistics.XMax'])*1e4
    nx1 = int(config['MatrixSize_X'])
    ny1 = int(config['MatrixSize_Y'])
    dx1 = float(config['Step_X'])
    dy1 = float(config['Step_Y'])
    
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
       
    birth = []
    death = []
    ez_t = []
    bz_t = []

    sp_path = 'birthr' 
    if not os.path.exists(sp_path):
        os.makedirs(sp_path)
    shutil.copy("ParsedInput.txt", sp_path)
    shutil.copy("Input.txt", sp_path)

    for i in range(nmin,nmax,delta):
        bname = bpath + '/' + "%.4f.txt" % (i,)
        print bname
        b, pb = read_field2d(bname, nx, ny)
        dname = dpath + '/' + "%.4f.txt" % (i,)
        print dname
        d, pd = read_field2d(dname, nx, ny)
        
        fig = plt.figure(num=None, figsize=(10,30))
        plt.rc('text', usetex=True)
        ax = fig.add_subplot(3,1,1)
    
        ax.imshow(b, aspect = 'auto',  origin = 'lower', norm = clr.LogNorm())
        ax = fig.add_subplot(3,1,2)
        ax.imshow(d, aspect = 'auto',  origin = 'lower', norm = clr.LogNorm())

        ax = fig.add_subplot(3,1,3)
        ax.plot(pb)
        ax.set_yscale('log')
        ax.set_ylim([1e-10, 1e-1])

        plt.savefig(sp_path + '/' + 'birthr_%d.png'%i)
        plt.close()

if __name__ == '__main__':
    main()
