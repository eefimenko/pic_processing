#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np
import matplotlib as mp

def main():
    power_p = []
    ezx_p = []
    bzx_p = []
    nex_p = []
    ncrx_p = []
    jx_min_p = []
    jx_max_p = []
    powx_p = []
    pow1x_p = []
    maxx_p = []
    avx_p = []
    el_powx_p = []
    el_pow1x_p = []
    el_maxx_p = []
    el_avx_p = []
    
    f = open('stat_summary_el.txt', 'r')
    for line in f:
        tmp = line.split()
        power_p.append(float(tmp[0]))
        ezx_p.append(float(tmp[1])/1e11)
        bzx_p.append(float(tmp[2])/1e11)
        maxx_p.append(float(tmp[3]))
        avx_p.append(float(tmp[4]))
        ncrx_p.append(float(tmp[5]))
        nex_p.append(float(tmp[6]))
        powx_p.append(float(tmp[7]))
        pow1x_p.append(float(tmp[8]))
        jx_min_p.append(float(tmp[9]))
        jx_max_p.append(float(tmp[10]))
        el_maxx_p.append(float(tmp[11]))
        el_avx_p.append(float(tmp[12]))
        el_powx_p.append(float(tmp[13]))
        el_pow1x_p.append(float(tmp[14]))
       
    f.close()
    nmax = 100
    dp = power_p[-1]/nmax
    p = np.zeros(nmax)
    e = np.zeros(nmax)
    b = np.zeros(nmax)
    for i in range(nmax):
        p[i] = dp*i
        e[i] = 3*math.sqrt(p[i]/10)
        b[i] = 0.653*3*math.sqrt(p[i]/10)

    thr_power = 7.25
    ep = 3*math.sqrt(thr_power/10)
    bp = 0.653*3*math.sqrt(thr_power/10)
  
   
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.plot(power_p, ezx_p, 'r-o', label='Stationary E')
    ax.plot(p, e, 'r:', label = 'Vacuum E')
    ax.plot([thr_power,power_p[-1]], [ep,ep], 'r--', label = 'Threshold E (%g PW)'%thr_power)
    ax.plot(power_p, bzx_p, 'b-v', label='Stationary B')
    ax.plot(p, b, 'b:', label = 'Vacuum B')
    ax.plot([thr_power,power_p[-1]], [bp,bp], 'b--', label = 'Threshold B (%g PW)'%thr_power)
    ax.set_xlim([7,power_p[-1]+0.5])
    plt.legend(loc = 'lower right')
    
    ax.set_xlabel('Power, PW')
    ax.set_ylabel('$E,B, \\times 10^{11} CGS$')
    plt.show()
    plt.close()

    rpow_p = []
    rpow1_p = []
    eff_p = []
    eff1_p = []
    
    for i in range(len(power_p)):
        el_powx_p[i] = power_p[i] - el_powx_p[i]
        powx_p[i] = power_p[i] - powx_p[i]
        rpow_p.append(power_p[i] - powx_p[i] - 2*el_powx_p[i])
        print power_p[i], powx_p[i], el_powx_p[i], power_p[i] - powx_p[i] - 2*el_powx_p[i]
        eff_p.append((powx_p[i] + 2*el_powx_p[i])/power_p[i])
        eff1_p.append(powx_p[i]/power_p[i])

    # power1_p = [0, thr_power] + power_p
    # rpow1_p = [0, thr_power] + rpow_p
    # powx1_p = [0, 0] + powx_p
    # el_powx1_p = [0, 0] + el_powx_p
    power1_p = power_p
    rpow1_p = rpow_p
    powx1_p = powx_p
    el_powx1_p = el_powx_p
        
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.plot(power1_p, rpow1_p, 'b-o', label = 'Reflected')
    ax.plot(power1_p, powx1_p, 'g-v', label = 'Photons')
    ax.plot(power1_p, el_powx1_p, 'r-^', label = 'Electrons')
    # ax.plot([thr_power, thr_power], [0, 8], 'b--', label = 'Threshold')
    ax.set_xlabel('Power, PW')
    ax.set_ylabel('Radiated power, PW')
    # ax.set_xticks([0,5,thr_power,10,15])
    plt.legend(loc = 'upper center')
    plt.show()
    plt.close()

    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.plot([thr_power] + power_p, [0] + eff1_p, 'g-o', label = 'Conversion')
    ax.plot([thr_power]+ power_p, [0] + eff_p, 'b-v', label = 'Absorption')
    ax.set_xlabel('Power, PW')
    ax.set_ylabel('Efficiency')
    plt.legend(loc = 'lower right')
    plt.show()
    plt.close()

    fig = plt.figure()
    nex = fig.add_subplot(1,1,1)
    nex.set_title('Ne')
    nex.plot(power_p, nex_p, 'b-o')
    nex.set_xlabel('Power, PW')
    nex.set_ylabel('Electron density, $cm^{-3}$')
    plt.show()
    plt.close()

    fig = plt.figure()
    nex = fig.add_subplot(1,1,1)
    nex.set_title('Ne')
    nex.plot(power_p, ncrx_p)
    nex.set_xlabel('Power, PW')
    nex.set_ylabel('$N_e/N_{cr}$')
    plt.show()
    plt.close()

    fig = plt.figure()
    maxx = fig.add_subplot(1,1,1)
    m, = maxx.plot(power_p, maxx_p, 'b-o')
    mel, = maxx.plot(power_p, el_maxx_p, 'b-v')
    maxx.set_ylim([0, 1.1*max(el_maxx_p)])
    maxx.set_xlabel('Power, PW')
    maxx.set_ylabel('$W^{1\%}, GeV$')
    maxx.yaxis.label.set_color(m.get_color())
    maxx.spines["left"].set_color(m.get_color())
    maxx.tick_params(axis='y', colors=m.get_color())
    avx = maxx.twinx()
    a, = avx.plot(power_p, avx_p, 'r-o')
    ael, = avx.plot(power_p, el_avx_p, 'r-v')
    avx.set_ylim([0, 1.1*max(el_avx_p)])
    avx.set_ylabel('$W^{av}, GeV$')
    avx.yaxis.label.set_color(a.get_color())
    avx.spines["right"].set_color(a.get_color())
    avx.spines["left"].set_color(m.get_color())
    avx.tick_params(axis='y', colors=a.get_color())
    avx.legend([m,mel,a,ael], ['$W_{ph}^{1\%}$','$W_{el}^{1\%}$','$W_{ph}^{av}$','$W_{el}^{av}$'], bbox_to_anchor=(1, 0.5))
    plt.show()
    plt.close()

    '''fig = plt.figure()
    maxx = fig.add_subplot(1,1,1)
    m, = maxx.plot(power_p, el_maxx_p, 'b')
    maxx.set_ylim([0, 1.1*max(el_maxx_p)])
    maxx.set_title('Max energy')
    maxx.set_xlabel('Power, PW')
    maxx.set_ylabel('Maximal electron energy, GeV')
    
    avx = maxx.twinx()
    a, = avx.plot(power_p, el_avx_p, 'r')
    avx.set_ylim([0, 1.1*max(el_avx_p)])
    avx.set_ylabel('Average electron energy, GeV')
    avx.legend([m,a], ['Maximal', 'Average'], loc = 'lower right')
    plt.show()
    plt.close()'''
    
    fig = plt.figure()
    maxx = fig.add_subplot(1,1,1)
    m, = maxx.plot(power_p, maxx_p, 'b')
    maxx.set_ylim([0, 1.1*max(maxx_p)])
    maxx.set_title('Max energy')
    maxx.set_xlabel('Power, PW')
    maxx.set_ylabel('Maximal photon energy, GeV')
    
    ezx = maxx.twinx()
    a, = ezx.plot(power_p, ezx_p, 'r')
    ezx.set_ylim([0, 1.1*max(ezx_p)])
    ezx.set_ylabel('Stationary E field')
    plt.legend([m,a], ['Max energy', 'Electric field'], loc = 'lower right')
    plt.show()
    plt.close()
    
    power_p1 = []
    nmin = []
    nmax = []
    f = open('nmin_nmax.txt')
    for line in f:
        tmp = line.split()
        power_p1.append(float(tmp[0]))
        nmin.append(float(tmp[1]))
        nmax.append(float(tmp[2]))
    f.close()

    power_p2 = []
    jneg = []
    jpos = []
    f = open('jpos_jneg.txt')
    for line in f:
        tmp = line.split()
        power_p2.append(float(tmp[0]))
        jpos.append(float(tmp[1]))
        jneg.append(float(tmp[2]))
    f.close()
    print jpos, jneg
    mp.rcParams.update({'font.size': 16})
    fig = plt.figure()
    maxx = fig.add_subplot(1,2,1)
    m1, = maxx.plot(power_p1, nmin, 'r-o')
    maxx.set_ylim([0, 1.05*max(nmin)])
   
    maxx.set_xlabel('Power, PW')
    maxx.set_ylabel('Number of particles')

    jx = maxx.twinx()
    j1, = jx.plot(power_p2, jpos, 'r--o')
    jx.set_ylim([0, 1.1*max(jpos)])
    jx.set_xlabel('Power, PW')
    jx.set_ylabel('$+J_z, MA$')

    maxx = fig.add_subplot(1,2,2)
    m2, = maxx.plot(power_p1, nmax, 'b-o')
    maxx.set_ylim([0, 1.05*max(nmax)])
    
    maxx.set_xlabel('Power, PW')
    # maxx.set_ylabel('Number of particles')

    jx = maxx.twinx()
    j2, = jx.plot(power_p2, jneg, 'b--o')
    jx.set_ylim([0, 1.1*max(jpos)])
    jx.set_xlabel('Power, PW')
    jx.set_ylabel('$-J_z, MA$')
    
    plt.legend([m1,m2,j1,j2], ['$r<0.4 \lambda$', '$r>0.4 \lambda$', '$+J_z$', '$-J_z$'], loc = 'upper left')
    
    plt.show()
    plt.close()
    
    fig = plt.figure()
    ezx = fig.add_subplot(4,2,1)
    ezx.set_title('Electric field')
    ezx.plot(power_p, ezx_p)
    ezx.set_ylim([0, max(ezx_p)])
    bzx = fig.add_subplot(4,2,2)
    bzx.set_title('Magnetic field')
    bzx.plot(power_p, bzx_p)
    bzx.set_ylim([0, max(bzx_p)])
    ncrx = fig.add_subplot(4,2,4)
    ncrx.set_title('Ne/Ncr')
    ncrx.plot(power_p, ncrx_p)
    ncrx.set_ylim([0, max(ncrx_p)])
    nex = fig.add_subplot(4,2,3)
    nex.set_title('Ne')
    nex.plot(power_p, nex_p)
    nex.set_ylim([0, max(nex_p)])
    powx = fig.add_subplot(4,2,6)
    powx.set_title('Power')
    powx.plot(power_p, powx_p)
    powx.set_ylim([0, max(powx_p)])
    pow1x = powx.twinx()
    pow1x.plot(power_p, pow1x_p, 'g')
    pow1x.set_ylim([0, max(pow1x_p)])
    maxx = fig.add_subplot(4,2,7)
    maxx.set_title('Max energy')
    maxx.plot(power_p, maxx_p)
    maxx.set_ylim([0, max(maxx_p)])
    avx = fig.add_subplot(4,2,8)
    avx.plot(power_p, avx_p)
    avx.set_ylim([0, max(avx_p)])
    avx.set_title('Av energy')
    jx = fig.add_subplot(4,2,5)
    jx.set_title('Number of particles')
    jx.plot(power_p, jx_min_p)
    jx.plot(power_p, jx_max_p)
    jx.set_ylim([0, max(jx_max_p)])

    plt.show()
    
if __name__ == '__main__':
    main()
