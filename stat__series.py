#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import math
import utils 
import sys
import os

def summ_over_z(array,n):
    res = [0]*len(array[0])
    for j in range(len(array[0])):
        for i in range(len(array)/2 - n/2, len(array)/2 + n/2):
            res[j] += array[i][j]/n
    return res

def average_gamma(array, dg):
    n = sum(array)
    print 'Av gamma', len(array), n
    s = 0
    for i in range(len(array)):
        s += array[i]*(i+0.5)*dg
    if n != 0:
        return s/n
    else:
        return 0

def distr_width(array, xmin, dx):
    s1 = sum(array)
    s2 = 0
    for i in range(len(array)):
        s2 += array[i]*(i*dx + xmin)*(i*dx+xmin)/s1
    return math.sqrt(s2)

def read_configs(pathlist):
    cfgs = []
    for path in pathlist:
        config = utils.get_config(path + "/ParsedInput.txt")
        cfgs.append(config)
    return cfgs

def read_one_dir(path, config, i):
    wl = float(config['Wavelength'])
    Xmax = float(config['X_Max'])*1e4 #mkm to wavelength
    Xmin = float(config['X_Min'])*1e4 #mkm to wavelength
    Ymax = float(config['Y_Max'])*1e4 #mkm to wavelength
    Ymin = float(config['Y_Min'])*1e4 #mkm to wavelength
    Zmax = float(config['Z_Max'])*1e4 #mkm to wavelength
    Zmin = float(config['Z_Min'])*1e4 #mkm to wavelength

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])

    BOIterationPass = float(config['BOIterationPass'])
    timestep = float(config['TimeStep'])
    dt = float(config['TimeStep'])*1e15*BOIterationPass
    if 'resize' in config.keys():
        factor = int(config['resize'])
    else:
        factor = 1
    dv = 2.*dx*dy*dz*factor
    mult = 1./dv
    n = 1
    axis = utils.create_axis(nx, dx*1e4, Xmin)
    fieldex = utils.bo_file_load(path + '/' + utils.ezpath,i,nx,ny)
    e = summ_over_z(fieldex,n*factor)
    fieldbx = utils.bo_file_load(path + '/' + utils.bzpath,i,nx,ny)
    b = summ_over_z(fieldbx,n*factor)
    ne_z = utils.bo_file_load(path + '/' + utils.nezpath,i,nx,ny,mult=mult)
    ne = summ_over_z(ne_z,n*factor)
    
    return axis, e, b, ne

def main():
    paths = sys.argv[1:]
    print paths
    configs = read_configs(paths)
    picspath = 'pics'
    nmin = 340
    nmax = 360
    for i in range(nmin,nmax):
        fig = plt.figure(num=None, figsize=(20, 10), dpi=256)
        ax = fig.add_subplot(3,1,1)
        ax1 = fig.add_subplot(3,1,2)
        ax2 = fig.add_subplot(3,1,3)
        for k in range(len(paths)):
            iteration = i*int(configs[k]['resize'])
            axis, e, b, n = read_one_dir(paths[k], configs[k], iteration)
            ax.plot(axis,e)
            ax1.plot(axis,b)
            ax2.plot(axis,n, label = int(configs[k]['resize']))
        picname = picspath + '/' + "profile_%02d.png" % i
        ax.set_xlim([-0.5,0.5])
        ax1.set_xlim([-0.5,0.5])
        ax2.set_xlim([-0.5,0.5])
        plt.legend()
        print picname
        plt.savefig(picname)
        plt.close()     

    
if __name__ == '__main__':
    main()
