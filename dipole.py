#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_field(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    return tmp[0]

def get_values(array, nx, ny, x, y):
    ans = []
    for i in range(nx):
        for j in range(ny):
            index = i + j*nx
            if (i == x or x == -1) and (j == y or y == -1):
                ans.append(array[index])
    return ans

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append((i-n/2)*step)
    return axis


def main():
    nx = 512
    ny = 512
    row = -1
    step = 8./nx
    epath = 'data/E2x'
    bpath = 'data/B2x'
    nepath = 'data/Electron2Dx'
    nppath = 'data/Positron2Dx'
    picspath = 'pics'
    maxf = 0.
    maxn = 0.
    nmax = utils.num_files(epath)
    nmax=170
    v = step*step*step*1e-12*2
    nmin = 0
    nmax = 0
    for i in range(nmin,nmax):
        ename = epath + '/' + "%06d.txt" % (i,)
        bname = bpath + '/' + "%06d.txt" % (i,)
        nename = nepath + '/' + "%06d.txt" % (i,)
        npname = nppath + '/' + "%06d.txt" % (i,)
        print "%06d.txt" % (i,)
        e = read_field(ename)
#        b = read_field(bname)
        n = read_field(nename)
        p = read_field(npname)
        prof_e = get_values(e, nx, ny, nx/2, row)
#        prof_b = get_values(b, nx, ny, nx/2, row)
        prof_el = get_values(n, nx, ny, nx/2, row)
        for j in range(len(prof_el)):
            prof_el[j] = prof_el[j]/v + 1
        prof_pos = get_values(p, nx, ny, nx/2, row)
        for j in range(len(prof_pos)):
            prof_pos[j] = prof_pos[j]/v + 1

        tmp = max(prof_e)
        if (tmp > maxf):
            maxf = tmp
#        tmp = max(prof_b)
#        if (tmp > maxf):
#            maxf = tmp
        tmp = max(prof_el)
        if (tmp > maxn):
            maxn = tmp
        tmp = max(prof_pos)
        if (tmp > maxn):
            maxn = tmp
      
    print maxn, maxf
    nmax = utils.num_files(epath)
    nmin = nmax - 40
#    nmax = 180
    for i in range(nmin,nmax):
        print "%06d.png" % (i,)
        ename = epath + '/' + "%06d.txt" % (i,)
        bname = bpath + '/' + "%06d.txt" % (i,)
        nename = nepath + '/' + "%06d.txt" % (i,)
        npname = nppath + '/' + "%06d.txt" % (i,)
        e = read_field(ename)
        b = read_field(bname)
        n = read_field(nename)
        p = read_field(npname)
   
        prof_e = get_values(e, nx, ny, nx/2, row)
        prof_b = get_values(b, nx, ny, nx/2, row)
        prof_el = get_values(n, nx, ny, nx/2, row)
#        prof_e = get_values(e, nx, ny, row, ny/2)
#        prof_b = get_values(b, nx, ny, row, ny/2)
#        prof_el = get_values(n, nx, ny, row, ny/2)
        for j in range(len(prof_el)):
            prof_el[j] = prof_el[j]/v + 1
        prof_pos = get_values(p, nx, ny, nx/2, row)
        for j in range(len(prof_pos)):
            prof_pos[j] = prof_pos[j]/v + 1

        fig, ax1 = plt.subplots()
        axis = create_axis(ny, step)
        pe, = ax1.plot(axis, prof_e, 'g')
#        pb, = ax1.plot(axis, prof_b, 'b')
        ax1.set_xlim(-2, 2)
        ax1.set_ylim(0, 3e11)
        ax2 = ax1.twinx()
#        ax2.set_yscale('log')
        pel, = ax2.plot(axis, prof_el, 'r')
        ax2.set_xlim(-2, 2)
        ax2.set_ylim(0, 6e26)
        pp, = ax2.plot(axis, prof_pos, 'k')
#        ax2.set_ylim(16612.6/v, maxn)
#        plt.legend([pe, pb, pn, pp], ["E", "H", 'El', 'Pos'], loc=2)
        plt.legend([pe, pel, pp], ["E", 'electron', 'positron'], loc=2)
        picname = picspath + '/' + "fprof%06d.png" % (i,)
        plt.savefig(picname)
#    plt.savefig('fig.png')
#    plt.show()
if __name__ == '__main__':
    main()
