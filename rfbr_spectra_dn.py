#!/usr/bin/python
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import sys
import math
import os, os.path
import utils
from tqdm import tqdm
import pickle

def main():

    config = utils.get_config("ParsedInput.txt")
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    dt = float(config['TimeStep'])
    ev = float(config['eV'])
    omega = float(config['Omega'])
    duration = float(config['PulseDuration'])*float(config['Period'])  # s
    delay = float(config['delay'])  # s
    power = float(config['PeakPower'])*1e-7  # W
    powerPW = float(config['PeakPowerPW'])
    Emax = float(config['QEDstatistics.Emax'])
    Emin = float(config['QEDstatistics.Emin'])
    Th_max = float(config['QEDstatistics.ThetaMax'])
    Th_min = float(config['QEDstatistics.ThetaMin'])
    N_E = int(config['QEDstatistics.OutputN_E'])
    N_Phi = int(config['QEDstatistics.OutputN_phi'])
    N_Th = int(config['QEDstatistics.OutputN_theta'])
    density = float(config['Density'])
    print('Density ', density)
    radius = float(config['PlasmaR'])
    de = (Emax - Emin)/N_E/1.6e-12/1e9
    de_ = (Emax - Emin)/N_E
    dth = (Th_max - Th_min)/N_Th  # erg -> eV -> GeV
    ax_e = utils.create_axis(N_E, de)
    ax_th = utils.create_axis(N_Th, dth)

    outStep = int(config['QEDstatistics.OutputIterStep'])
    tp = duration*1e15
    tdelay = delay*1e15
    n_tgt = density * 4./3. * math.pi * radius**3
    
    path = '.'
    elpath = '/el/'
    elmaxpath = '/el_max/'
    posmaxpath = '/pos_max/'
    phmaxpath = '/ph_max/'
    elsmaxpath = '/elSmax/'
    possmaxpath = '/posSmax/'
    phsmaxpath = '/phSmax/'
    elnmaxpath = '/elNmax/'
    posnmaxpath = '/posNmax/'
    phnmaxpath = '/phNmax/'
    pospath = '/pos/'
    phpath = '/ph/'
    phangle = '/ph_angle/'
    pht = '/phT/'
    picspath = 'pics/'
    prefix = ''
    ez_z_path = os.path.join(prefix, 'data/Ez_z')

    dumpfile = os.path.join(path, 'dump.pkl')
    
    archive = os.path.join(path, 'statdata.zip')
    data_archive = os.path.join(path, 'data.zip')
    nmin, nmax = utils.find_min_max_from_archive_directory(utils.enspsphpath,
                                                           'txt',
                                                           archive)
    print('Found ' + str(nmax) + ' files')

    el_en = np.zeros(nmax)
    ph_en = np.zeros(nmax)
    pos_en = np.zeros(nmax)
    el_en_1gev = np.zeros(nmax)
    ph_en_1gev = np.zeros(nmax)
    pos_en_1gev = np.zeros(nmax)
    ez_a = np.zeros(nmax)
    delay = float(config['delay'])
    duration = float(config['PulseDuration'])*float(config['Period'])
    period = float(config['Period'])
    T = 2*math.pi/omega*1e15
    nit = int(config['QEDstatistics.OutputIterStep'])
    dt1 = float(config['TimeStep'])*nit
    ax_ = utils.create_axis(nmax, dt1)
    omega = float(config['Omega'])
    alpha = float(config['alpha'])
    relField = float(config['RelativisticField'])
    verbose = 0
    
    for i in range(nmax):
        t = i*dt1
        if t-delay > 0 and t-delay < period*alpha/2.:
            ez_a[i] = 1.6e11*math.sqrt(powerPW/10.)*math.sin(omega*(t-delay)/alpha)**2/relField
        else:
            ez_a[i] = 0.
        #ez_a[i] = math.exp(-2.*math.log(2)*(t - delay)**2/duration**2)

    ez_t = utils.ts(path, ez_z_path, name='ez', ftype='bin',
                    tstype='center', shape=(nx,ny), verbose=verbose,
                    archive=data_archive)
    print('a_E=', ez_t.max())
    
    if not os.path.exists(picspath):
        os.makedirs(picspath)
    photon_flux = 0.
    electron_flux = 0.
    positron_flux = 0.
    
    i_1gev = int(1./de)    
    if os.path.exists(dumpfile):
        with open(dumpfile, 'rb') as f:
            sp_ph_sum_ = pickle.load(f)
            sp_el_sum_ = pickle.load(f)
            sp_pos_sum_ = pickle.load(f)
            ang_ph_sum_ = pickle.load(f)
            ang_el_sum_ = pickle.load(f)
            ang_pos_sum_ = pickle.load(f)
            ph_en = pickle.load(f)
            ph_en_1gev = pickle.load(f)
            el_en = pickle.load(f)
            el_en_1gev = pickle.load(f)
            pos_en = pickle.load(f)
            pos_en_1gev = pickle.load(f)
    else:
        read, sp_ph_sum_ = utils.bo_file_load(utils.enspsphpath,
                                              0,
                                              fmt='%.4f',
                                              verbose=verbose,
                                              archive=archive)

        read, sp_el_sum_ = utils.bo_file_load(utils.el_enspsphpath,
                                              0,
                                              fmt='%.4f',
                                              verbose=verbose,
                                              archive=archive)

        read, sp_pos_sum_ = utils.bo_file_load(utils.pos_enspsphpath,
                                               0,
                                               fmt='%.4f',
                                               verbose=verbose,
                                               archive=archive)

        ang_ph_sum_ = np.sum(utils.bo_file_load(utils.enangsphpath,
                                                0,
                                                nx=N_E,
                                                ny=N_Th,
                                                fmt='%.4f',
                                                verbose=verbose,
                                                archive=archive)[1], axis=0)

        ang_el_sum_ = np.sum(utils.bo_file_load(utils.el_enangsphpath,
                                                0,
                                                nx=N_E,
                                                ny=N_Th,
                                                fmt='%.4f',
                                                verbose=verbose,
                                                archive=archive)[1], axis=0)

        ang_pos_sum_ = np.sum(utils.bo_file_load(utils.pos_enangsphpath,
                                                 0,
                                                 nx=N_E,
                                                 ny=N_Th,
                                                 fmt='%.4f',
                                                 verbose=verbose,
                                                 archive=archive)[1], axis=0)

        print(len(ang_ph_sum_))


        
        for i in tqdm(range(1, nmax)):
            read, sp_ = utils.bo_file_load(utils.enspsphpath,
                                           i,
                                           fmt='%.4f',
                                           verbose=verbose,
                                           archive=archive)
            
            sp_ph_sum_ += sp_
            ph_en[i] += ph_en[i-1] + np.sum(sp_)
            ph_en_1gev[i] += ph_en_1gev[i-1] + np.sum(sp_[i_1gev:])
            n_ph_ = utils.full_number(sp_, de_, N_E)
            flux = n_ph_/dt1
            photon_flux = max(flux, photon_flux)
            
            read, sp_ = utils.bo_file_load(utils.el_enspsphpath,
                                           i,
                                           fmt='%.4f',
                                           verbose=verbose,
                                           archive=archive)
            sp_el_sum_ += sp_
            el_en[i] += el_en[i-1] + np.sum(sp_)
            el_en_1gev[i] += el_en_1gev[i-1] + np.sum(sp_[i_1gev:])
            n_el_ = utils.full_number(sp_, de_, N_E)
            flux = n_el_/dt1
            electron_flux = max(flux, electron_flux)
            
            read, sp_ = utils.bo_file_load(utils.pos_enspsphpath,
                                           i,
                                           fmt='%.4f',
                                           verbose=verbose,
                                           archive=archive)
            sp_pos_sum_ += sp_
            pos_en[i] += pos_en[i-1] + np.sum(sp_)
            pos_en_1gev[i] += pos_en_1gev[i-1] + np.sum(sp_[i_1gev:])
            n_pos_ = utils.full_number(sp_, de_, N_E)
            flux = n_pos_/dt1
            positron_flux = max(flux, positron_flux)
            
            ang_ph_sum_ += np.sum(utils.bo_file_load(utils.enangsphpath,
                                                     i,
                                                     nx=N_E,
                                                     ny=N_Th,
                                                     fmt='%.4f',
                                                     verbose=verbose,
                                                     archive=archive)[1], axis=0)

            ang_el_sum_ += np.sum(utils.bo_file_load(utils.el_enangsphpath,
                                                     i,
                                                     nx=N_E,
                                                     ny=N_Th,
                                                     fmt='%.4f',
                                                     verbose=verbose,
                                                     archive=archive)[1], axis=0)

            ang_pos_sum_ += np.sum(utils.bo_file_load(utils.pos_enangsphpath,
                                                      i,
                                                      nx=N_E,
                                                      ny=N_Th,
                                                      fmt='%.4f',
                                                      verbose=verbose,
                                                      archive=archive)[1], axis=0)

        with open(dumpfile, 'wb') as f:
            pickle.dump(sp_ph_sum_, f)
            pickle.dump(sp_el_sum_, f)
            pickle.dump(sp_pos_sum_, f)
            pickle.dump(ang_ph_sum_, f)
            pickle.dump(ang_el_sum_, f)
            pickle.dump(ang_pos_sum_,f)
            pickle.dump(ph_en, f)
            pickle.dump(ph_en_1gev, f)
            pickle.dump(el_en, f)
            pickle.dump(el_en_1gev, f)
            pickle.dump(pos_en, f)
            pickle.dump(pos_en_1gev, f)

    en = max(ph_en)*1e-7

    pulse_energy = powerPW*1e22*3*period*(alpha-2*np.sin(2*np.pi*alpha)/((alpha**2-1)*(alpha**2-4)*np.pi))/(16)*1e-7
    print(en, pulse_energy, en/pulse_energy)

    fig, ax1 = plt.subplots()
    pph, = ax1.plot(ax_,ph_en, 'r')
    pel, = ax1.plot(ax_,el_en, 'g')
    ppos, = ax1.plot(ax_,pos_en, 'b')
    pph_1gev, = ax1.plot(ax_,ph_en_1gev, 'r:')
    pel_1gev, = ax1.plot(ax_,el_en_1gev, 'g:')
    ppos_1gev, = ax1.plot(ax_,pos_en_1gev, 'b:')

    print('Max', max(ez_a), ez_t.max())
    ax2 = ax1.twinx()
    pu, = ax2.plot(ax_, ez_a/max(max(ez_a), ez_t.max()), 'grey')
    pu, = ax2.plot(ax_, ez_t/max(max(ez_a), ez_t.max()), 'k', alpha = 0.2)
    
    plt.legend([pph, pel, ppos, pu], ['Ph. energy','El. energy','Pos. energy','Pulse'], loc=2)
    ax1.set_xlabel('Time, periods')
    ax1.set_ylabel('Full photon energy, erg')
    plt.figtext(0.2,0.93,str('Pulse energy=%.1f J; Photon energy=%.1lf J' % (pulse_energy, en)) , fontsize = 10)
    plt.figtext(0.6,0.5,str('Efficiency=%.1lf%%' % (en/pulse_energy*100)) , fontsize = 16)
    picname = picspath + "energy.png"
    plt.savefig(picname)
    plt.close()
    
    fig, ax1 = plt.subplots()
    ax1.set_yscale('log')
    print(np.sum(sp_ph_sum_))
    n_ph_ = utils.full_number(sp_ph_sum_,de_,N_E)
    n_ph_1gev_ = utils.full_number(sp_ph_sum_,de_,N_E, bound=i_1gev)
    pph, = ax1.plot(ax_e,sp_ph_sum_/n_ph_, 'r')
    n_el_ = utils.full_number(sp_el_sum_,de_,N_E)
    n_el_1gev_ = utils.full_number(sp_el_sum_,de_,N_E, bound=i_1gev)
    print(np.sum(sp_el_sum_))
    pel, = ax1.plot(ax_e,sp_el_sum_/n_el_, 'g')
    n_pos_ = utils.full_number(sp_pos_sum_,de_,N_E)
    n_pos_1gev_ = utils.full_number(sp_pos_sum_,de_,N_E, bound=i_1gev)
    ppos, = ax1.plot(ax_e,sp_pos_sum_/n_pos_, 'b')
    print(np.sum(sp_pos_sum_))
    picname = picspath + "spectra.png"
    plt.savefig(picname)
    plt.close()

    print('Number: ',
          'photons - {:e}'.format(n_ph_),
          'electrons - {:e}'.format(n_el_),
          'target + positrons - {:e}'.format(n_tgt + n_pos_),
          'positrons - {:e}'.format(n_pos_),
          'target - {:e}'.format(n_tgt))

    print('Number[>1 GeV]: ',
          'photons - {:e}'.format(n_ph_1gev_),
          'electrons - {:e}'.format(n_el_1gev_),
          'positrons - {:e}'.format(n_pos_1gev_))
    
    with open('qe_spectra.txt', 'w') as f:
        for i in range(len(ax_e)):
            f.write("%lf %le %le %le\n"%(ax_e[i], sp_ph_sum_[i], sp_el_sum_[i], sp_pos_sum_[i]))
    
    with open('efficiency.txt', 'w') as f:
        f.write("%le %le %le\n"%(ph_en[-1]*1e-7/pulse_energy, el_en[-1]*1e-7/pulse_energy, pos_en[-1]*1e-7/pulse_energy))
    
    with open('efficiency_1gev.txt', 'w') as f:
        f.write("%le %le %le\n"%(ph_en_1gev[-1]*1e-7/pulse_energy, el_en_1gev[-1]*1e-7/pulse_energy, pos_en_1gev[-1]*1e-7/pulse_energy))

    with open('number.txt', 'w') as f:
        f.write("%le %le %le\n"%(n_ph_, n_el_, n_pos_))

    with open('number_1gev.txt', 'w') as f:
        f.write("%le %le %le\n"%(n_ph_1gev_, n_el_1gev_, n_pos_1gev_))

    fig, ax1 = plt.subplots()
    ax1.set_yscale('log')
    n = np.sum(ang_ph_sum_)
    print(2*n)
    if n < 1e-30:
        n = 1.
    pph, = ax1.plot(ax_th,ang_ph_sum_/n, 'r')
    n = np.sum(ang_el_sum_)
    print(2*n)
    if n < 1e-30:
        n = 1.
    pel, = ax1.plot(ax_th,ang_el_sum_/n, 'b')
    n = np.sum(ang_pos_sum_)
    print(2*n)
    if n < 1e-30:
        n = 1.
    
    pel, = ax1.plot(ax_th,(ang_pos_sum_+1e-12)/n, 'g')
    picname = picspath + "dn.png"
    plt.savefig(picname)
    plt.close()
    
    with open('qe_dn.txt', 'w') as f:
        for i in range(len(ax_th)):
            f.write("%lf %le %le %le\n"%(ax_th[i], ang_ph_sum_[i], ang_el_sum_[i], ang_pos_sum_[i]))

    print('Maximum flux: ',
          'photons - {:e}'.format(photon_flux),
          'electrons - {:e}'.format(electron_flux),
          'positrons - {:e}'.format(positron_flux))


if __name__ == '__main__':
    main()
