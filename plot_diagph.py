#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def main():
    num = len(sys.argv)
    path  = './BasicOutput/' + utils.dphpath
    delta = 1
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(path)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(path)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax) + ' files'
    config = utils.get_config("./ParsedInput.txt")
    xmin = float(config['diagPh.SetBounds_0'])*180./3.14159
    xmax = float(config['diagPh.SetBounds_1'])*180./3.14159
    nx = int(config['diagPh.SetMatrixSize_0'])
    dx = (xmax-xmin)/nx
    axis = utils.create_axis(nx,dx,xmin)
    
    for i in range(nmin,nmax,delta):
        value = utils.bo_file_load(path,i,nx)
#        print value
        fig = plt.figure()
        ax1 = fig.add_subplot(1,2,1)
        ax1.plot(axis,value)
        ax1 = fig.add_subplot(1,2,2)
        ax1.plot(axis,value)
        ax1.set_xlim([0,1])
        picname = './pics/diagph_%d'%i
        plt.savefig(picname)


if __name__ == '__main__':
    main()
