#!/bin/bash

#remote host mvs100k
#user=evefim
#host=mvs100k.jscc.ru
#wd_remote=/home4/pstorage1/ipfnn1/evgeny/picador/
#user=alba
#host=mvs100k.jscc.ru
#wd_remote=/home4/pstorage1/ipfnn1/alba/mvs10p/
#user=argon
#host=mvs100k.jscc.ru
#wd_remote=/home4/pstorage1/ipfnn1/Muraviev/

#remote host akka
user=argon
host=abisko.hpc2n.umu.se
wd_remote=/home/a/argon/pfs/evefim/dw

ssh_host=${user}@${host}

function create_dir
{
    if [ ! -d $1 ]; 
    then
	mkdir $1
    else
	echo "$1 already exists"
    fi
}

function get_remote_vtk()
{
    list=`ssh $ssh_host "ls ${wd_remote}/$1/VTKOutput/*_$2.vtk"`
    echo $list
    echo "Copy VTKOutput data from $ssh_host:${wd_remote}/$1/VTKOutput"
    ssh $ssh_host "cd ${wd_remote}/$1/VTKOutput && tar -cjf vtk.tar.bz2 *_$2.vtk"
    scp $ssh_host:${wd_remote}/$1/VTKOutput/vtk.tar.bz2 VTKOutput
    cd VTKOutput
    tar -xjf vtk.tar.bz2 && rm vtk.tar.bz2
    cd ..
    ssh $ssh_host "cd ${wd_remote}/$1/VTKOutput && rm vtk.tar.bz2"
}

# reading saved rdir and remote working dir
if [ -f "log.txt" ]; then
    i=0
    while IFS= read -r var
    do
	if [ $i -eq 0 ]; then
	    rdir=$var
	else
	    if [ $i -eq 1 ]; then
		wd_remote=$var
	    else
		echo "Something wrong, exiting"
	    fi
	fi
	i=$((i+1))
    done < "log.txt"
    echo "log.txt exists, continue downloading from $rdir"
else
    if [ -z "$rdir" ]; then
	echo "Unknown remote directory, exiting"
	exit -1
    fi
    save_dir $rdir $wd_remote
fi

echo "Save path: $dir"
echo "Download path: $rdir"
echo "Working directory: $wd_remote"

create_dir VTKOutput

iter=$1

get_remote_vtk $rdir $iter

cd ..
