#!/usr/bin/python
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import utils
import sys
import os
from tqdm import *
from pylab import *

def read_dn(path, filename):
    f = open(path + '/' + filename + '_dn.txt', 'r')
    #print(path + '/' + filename + '_dn.txt')
    axt = []
    s_t_sin_ = []
    s1_t_sin_ = []
    s2_t_sin_ = []
    for line in f:
        array = line.split()
        axt.append(float(array[0]))
        s_t_sin_.append(float(array[1]))
        s1_t_sin_.append(float(array[2]))
        s2_t_sin_.append(float(array[3]))
    return np.array(axt), np.array(s_t_sin_), np.array(s1_t_sin_), np.array(s2_t_sin_)

def read_sp(path, filename):
    f = open(path + '/' + filename + '_sp.txt', 'r')
    #print(path + '/' + filename + '_sp.txt')
    axe = []
    s_e = []
    summ_ensp = []
    for line in f:
        array = line.split()
        axe.append(float(array[0]))
        s_e.append(float(array[1]))
        summ_ensp.append(float(array[2]))
        
    return np.array(axe), np.array(s_e), np.array(summ_ensp) 
        

def plot_dn(picspath, paths, filename, tag):
    fig = plt.figure(num=None, figsize=(15, 4), tight_layout = True)
    ax1 = fig.add_subplot(1,3,1)
    ax2 = fig.add_subplot(1,3,2)
    ax3 = fig.add_subplot(1,3,3)
    m = 0
    for path in paths:
        config = utils.get_config(path + "/ParsedInput.txt")
        ppw = int(config['PeakPowerPW'])
   
        axt, s_t_sin_, s1_t_sin_, s2_t_sin_ = read_dn(path, filename)
        
        m = max([np.amax(s_t_sin_), np.amax(s1_t_sin_), np.amax(s2_t_sin_), m])
        ax1.plot(axt, s_t_sin_, label = '%d PW' % ppw)
        ax3.plot(axt, s2_t_sin_, label = '%d PW' % ppw)
        ax2.plot(axt, s1_t_sin_, label = '%d PW' % ppw)
    

    path = paths[0]
    config = utils.get_config(path + "/ParsedInput.txt")
    ppw = int(config['PeakPowerPW'])
    
    axt, s_t_sin_, s1_t_sin_, s2_t_sin_ = read_dn(path, filename[3:]) # nonlinear -> linear
    
    m = max([np.amax(s_t_sin_), np.amax(s1_t_sin_), np.amax(s2_t_sin_), m])
    ax1.plot(axt, s_t_sin_, label = '%d PW (linear)' % ppw)
    ax3.plot(axt, s2_t_sin_, label = '%d PW (linear)' % ppw)
    ax2.plot(axt, s1_t_sin_, label = '%d PW (linear)' % ppw)
    
    m *= 1.1
    plt.legend(loc = 'upper left', frameon = False)
    ax1.set_xlim([0, 90.])
    ax1.set_ylim([1e-4*m, m])
    ax1.set_yscale(u'log')
    ax1.set_xlabel(u'$\\theta$')
    ax1.set_ylabel(u'dW/$\sin$ $\\theta$ d$\\theta$')
    ax1.text(-20, m, '(c)')
    ax2.set_xlim([0, 90.])
    ax2.set_ylim([1e-4*m, m])
    ax2.set_yscale(u'log')
    ax2.set_xlabel(u'$\\theta$')
    ax2.set_ylabel(u'dW/$\sin$ $\\theta$ d$\\theta$')
    ax2.text(-20, m, '(d)')
    ax3.set_xlim([0, 90.])
    ax3.set_ylim([1e-4*m, m])
    ax3.set_yscale(u'log')
    ax3.set_xlabel(u'$\\theta$')
    ax3.set_ylabel(u'dW/$\sin$ $\\theta$ d$\\theta$')
    ax3.text(-20, m, '(e)')
    picname = picspath + '/' + filename +'_dn_%s.png' % (tag)
    plt.tight_layout()
    plt.savefig(picname)
    plt.close()

def plot_sp(picspath, paths, filename, tag):
    fig = plt.figure(num=None, figsize=(15, 4), tight_layout = True)
    ax1 = fig.add_subplot(1,2,1)
    ax2 = fig.add_subplot(1,2,2)
    m = 0
    
    for path in paths:
        config = utils.get_config(path + "/ParsedInput.txt")
        ppw = int(config['PeakPowerPW'])

        axe, s_e, summ_ensp = read_sp(path, filename)
        m = max([np.amax(s_e), np.amax(summ_ensp), m])
        ax1.plot(axe, s_e, label = '%d PW' % ppw)
        ax2.plot(axe, summ_ensp, label = '%d PW' % ppw)

    
    path = paths[0]
    config = utils.get_config(path + "/ParsedInput.txt")
    ppw = int(config['PeakPowerPW'])
    
    axe, s_e, summ_ensp = read_sp(path, filename[3:]) #linear
    m = max([np.amax(s_e), np.amax(summ_ensp), m])
    ax1.plot(axe, s_e, label = '%d PW (linear)' % ppw)
    ax2.plot(axe, summ_ensp, label = '%d PW (linear)' % ppw)
    
    ax1.set_yscale('log')
    ax2.set_yscale('log')
    m = 1.1*m
    fmax = 1.6
    plt.legend(loc = 'lower left', frameon = False)
    ax1.set_xlim([0, fmax])
    ax1.set_ylim([1e-6*m, m])
    ax1.set_ylabel(u'dW/d$\hbar\omega$')
    ax1.set_xlabel('$\hbar\omega$')
    ax1.text(-0.25*fmax/1.6, m, '(a)')
    ax2.set_xlim([0, fmax])
    ax2.set_ylim([1e-6*m, m])
    ax2.set_ylabel(u'dW/d$\hbar\omega$')
    ax2.set_xlabel('$\hbar\omega$')
    ax2.text(-0.25*fmax/1.6, m, '(b)')
    picname = picspath + '/' + filename +'_sp_%s.png' % (tag)
    plt.tight_layout()
    plt.savefig(picname)
    plt.close()

def main():
    fontsize = 16
    mp.rcParams.update({'font.size': fontsize})
    tag = sys.argv[-1]
    paths = sys.argv[1:-1]
    print(paths)
    picspath = './pics'

    plot_dn(picspath, paths, 'nonlinear_ph', tag)
    plot_dn(picspath, paths, 'nonlinear_el', tag)
    #plot_dn(picspath, paths, 'linear_ph', tag)
    #plot_dn(picspath, paths, 'linear_el', tag)
    plot_sp(picspath, paths, 'nonlinear_ph', tag)
    plot_sp(picspath, paths, 'nonlinear_el', tag)
    #plot_sp(picspath, paths, 'linear_ph', tag)
    #plot_sp(picspath, paths, 'linear_el', tag)
            
if __name__ == '__main__':
    main()
