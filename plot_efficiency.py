#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import os

def read_gamma(filename):
    f = open(filename, 'r')
    tmp = f.readline().split()
    f.close()
    return float(tmp[0]), float(tmp[1]), float(tmp[2])

def main():
    el = []
    ph = []
    pos = []
    ne = []
    dirname = os.path.split(os.getcwd())[1]
    if dirname == '2mkm':
        dirs = ['pulse_nc_0.001_30', 'pulse_nc_0.01_30', 'pulse_nc_0.1_30', 'pulse_nc_1_30', 'pulse_nc_10_30','pulse_nc_30_30','pulse_nc_50_30', 'pulse_nc_100_30', 'pulse_nc_1000_30']
    else:
        dirs = ['pulse_nc_0.001_30', 'pulse_nc_0.01_30', 'pulse_nc_0.1_30', 'pulse_nc_1_30', 'pulse_nc_10_30','pulse_nc_50_30', 'pulse_nc_100_30', 'pulse_nc_1000_30']
    for d in dirs:
        ph_, el_, pos_ = read_gamma(d + '/efficiency.txt')
        el.append(el_)
        ph.append(ph_)
        pos.append(pos_)
        config = utils.get_config(d + '/ParsedInput.txt')
        n_cr = float(config['n_cr'])
        ne_ = float(config['Ne'])
        ne.append(ne_/n_cr)
    fig, ax1 = plt.subplots()
    p_el, = ax1.plot(ne, el, 'r')
    p_ph, = ax1.plot(ne, ph, 'g')
    p_pos, = ax1.plot(ne, pos, 'b')
#    ax1.set_yscale('log')
    ax1.set_xscale('log')
    plt.show()

if __name__ == '__main__':
    main()
