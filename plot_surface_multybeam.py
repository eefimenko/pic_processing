#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import os
import numpy as np

def read_field(path,i):
    xy2d_path = 'BasicOutput/data/XY2D'
    config = utils.get_config(os.path.join(path, "ParsedInput.txt"))
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    wl = float(config['Wavelength'])
    dx = float(config['GridStep'])
    dy = float(config['GridStep'])
    dz = float(config['GridStep'])
    Xmax = float(config['X_Max'])/wl #mkm to wavelength
    Xmin = float(config['X_Min'])/wl #mkm to wavelength
    Ymax = float(config['Y_Max'])/wl #mkm to wavelength
    Ymin = float(config['Y_Min'])/wl #mkm to wavelength
    angle = float(config['OpeningAngle'])
    path_ = os.path.join(path, xy2d_path)
    
    read, fieldn = utils.bo_file_load(path_,i,nx,ny)
    ax = utils.create_axis(nx, (Xmax-Xmin)/nx, Xmin)
    ay = utils.create_axis(nx, (Xmax-Xmin)/nx)
    extent = (Xmin, Xmax, Ymin, Ymax)
    
    return read, ax, angle, fieldn, extent

def find_points(line):
    m = line.max()
    level = m/2.718281828
    i1 = 0
    i2 = len(line)-1
    for i in range(len(line)-1):
        if line[i] < level and line[i+1] >= level:
            i1 = i
        if line[i] >= level and line[i+1] < level:
            i2 = i
    return i1, i2

def main():
    path = sys.argv[1]
    start = int(sys.argv[2])
    end = int(sys.argv[3])
    #fig = plt.figure(num=None)
    #ax1 = fig.add_subplot(1,1,1)
    #ax2 = fig.add_subplot(1,2,2)
    
    for i in range(start, end):
        fig = plt.figure(num=None)
        ax1 = fig.add_subplot(1,1,1)
        iteration = i
        print(iteration)
        read, axis, angle_config, fieldn, extent = read_field(path, iteration)
        if not read:
            break
        print(fieldn.shape, axis[1]-axis[0])
        spot = 1e9
        surf = ax1.imshow(fieldn.transpose()/fieldn.max(), origin = 'lower', extent=extent, vmax=1)
        for i in range(10, fieldn.shape[0]-10):
            i1, i2 = find_points(fieldn[i])
            #ax1.scatter(axis[i1],axis[i],color='r', s=1)
            #ax1.scatter(axis[i2],axis[i],color='r', s=1)
            spot = min(spot, axis[i2] - axis[i1]) 
        ib = 25
        ie = fieldn.shape[0]//3
        
        i1_1, i2_1 = find_points(fieldn[ib])
        i1_2, i2_2 = find_points(fieldn[ie])
        
        length = axis[ie] - axis[ib]
        width = 0.5 * (axis[i1_2] - axis[i1_1] + abs(axis[i2_2] - axis[i2_1]))
        
        #angle = math.atan(width/length)
        #fnum = 1./(2.*math.tan(angle))
        #fnum_config = 1./(2.*math.tan(angle_config))
        #spot_config = 2/(math.pi*angle_config)
        
        #print(angle, fnum, spot)
        divider = utils.make_axes_locatable(ax1)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)
        #ax1.set_title('$\\theta$/2=%.2f (%.2f) $w_0$=%.2f$\lambda$ (%.2f$\lambda$) F#=%.2f (%.2f)' % (angle, angle_config, spot, spot_config, fnum, fnum_config))
        #ax1.legend()
        #plt.show()
        picname = os.path.join(path, 'BasicOutput', 'pics', 'pic%d.png' % iteration)
        print(picname)
        plt.savefig(picname)
        plt.close()
        #ax1.imshow(f)
    

if __name__ == '__main__':
    main()
