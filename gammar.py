#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import sys
import math
import utils
import numpy as np

def filterr(array,boundary):
    shape = array.shape
    fmin = np.zeros(shape)
    fmax = np.zeros(shape)
    for i in range(shape[0]):
        for j in range(shape[1]):
            if j < boundary:
                fmin[i][j] = array[i][j]
            else:
                fmax[i][j] = array[i][j]
    return fmin, fmax

def read_diag(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def find_gamma_av(array, dr, dg):
    s = array.shape
    res = [0] * s[0]
    nx = s[0]
    ny = s[1]
    av = 0
   
    for i in range(nx):
        for j in range(ny):
            if j*dr < 0.1:
                res[i] += array[i][j]
    s = sum(res)
    for i in range(nx):
        av += res[i]*i*dg/s

    return av

def main():
    path = './'
    config = utils.get_config("ParsedInput.txt")
    BOIterationPass = float(config['BOIterationPass'])
    delta = 1
    ppath = 'data/ElGammaR/'
    mpath = 'data/PosPhase/'
    p1path = 'data/ElMomentum/'
    m1path = 'data/PosMomentum/'
    ezpath = 'data/E2z'
    picspath = 'pics'
            
    nmin, nmax, delta = utils.get_min_max_iteration(sys.argv,path+utils.ezpath)
    nmin = nmax - 200
    
    nx1 = int(config['ElGammaR.SetMatrixSize_0'])
    ny1 = int(config['ElGammaR.SetMatrixSize_1'])
    rmin = float(config['ElGammaR.SetBounds_0'])*1e4
    rmax = float(config['ElGammaR.SetBounds_1'])*1e4
    Emin = float(config['ElGammaR.SetBounds_2'])
    Emax = float(config['ElGammaR.SetBounds_3'])
    Xmax = float(config['X_Max']) #mkm to wavelength
    Xmin = float(config['X_Min']) #mkm to wavelength
    Ymax = float(config['Y_Max']) #mkm to wavelength
    Ymin = float(config['Y_Min']) #mkm to wavelength

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
#    Mmin = float(config['ElMomentum.SetBounds_2'])
#    Mmax = float(config['ElMomentum.SetBounds_3'])
    print nx1, ny1
    dr = (rmax-rmin)/nx1
    dg = (Emax-Emin)/ny1
    av_t = []
    phase_t = []
    ppw = int(config['PeakPower'])*1e-22
    maxe = 3.e11 * math.sqrt(ppw/10.)
    ezv_prev = 0
    pfield = utils.bo_file_load(path + utils.elgammarpath,nmin,nx1,ny1,verbose=1, transpose=1)
    for i in range(nmin+1,nmax,delta):
        pfield += utils.bo_file_load(path + utils.elgammarpath,i,nx1,ny1,verbose=1, transpose=1)
        av = find_gamma_av(pfield, dr, dg)
        av_t.append(av)
    pfield /= (nmax - nmin) 
    fig = plt.figure(num=None)
    ax = fig.add_subplot(1,3,1)
    mmax = np.amax(pfield)
    mmin = mmax*1e-3
    ax.imshow(pfield, extent = [rmin,rmax, Emin,Emax], vmin=mmin, vmax = mmax, aspect='auto', origin='lower', norm=clr.LogNorm())
    picname = picspath + '/' + "gamma%06d.png" % (nmin,)
    print picname, mmax
    ax = fig.add_subplot(1,3,2)
    ax.plot(np.sum(pfield, axis=0))
    ax = fig.add_subplot(1,3,3)
    ax.plot(np.sum(pfield, axis=1))
    plt.show()
    plt.savefig(picname)
    print pfield.shape
    f = open('el_r.txt','w')
    el_r = np.sum(pfield, axis=0)
    for i in range(len(el_r)):
        f.write("%lf\n"%(el_r[i]))
    f.close()
    f = open('el_g.txt','w')
    el_g = np.sum(pfield, axis=1)
    for i in range(len(el_g)):
        f.write("%lf\n"%(el_g[i]))
    f.close()

    boundary = 0.4
    
    pmin, pmax = filterr(pfield,int(boundary/dr))
    fig = plt.figure(num=None)
    ax = fig.add_subplot(1,2,1)
    ax.plot(np.sum(pmin, axis=1))
    ax = fig.add_subplot(1,2,2)
    ax.plot(np.sum(pmax, axis=1))
    plt.show()
    f = open('el_g_min.txt','w')
    el_g = np.sum(pmin, axis=1)
    for i in range(len(el_g)):
        f.write("%lf\n"%(el_g[i]))
    f.close()
    f = open('el_g_max.txt','w')
    el_g = np.sum(pmax, axis=1)
    for i in range(len(el_g)):
        f.write("%lf\n"%(el_g[i]))
    f.close()
    print max(av_t), min(av_t)
    f = open('f_gamma_min_max.dat', 'w')
    f.write('%f %f\n' %(min(av_t), max(av_t)))
    f.close()

    fig = plt.figure(num=None)
    ax = fig.add_subplot(1,1,1)
    ax.plot(av_t, 'b')
    plt.show()
    

if __name__ == '__main__':
    main()
