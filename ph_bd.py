#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import sys
import math
import utils
import shutil
import os
import numpy as np

def read_bd(file):
    f = open(file, 'r')
    line = f.readline()
    tmp = [float(x) for x in line.split()]
    s = sum(tmp)
#    s = 1.
    if s > 0:
        for i in range(len(tmp)):
            tmp[i] /= s #* (i+0.5)
    return tmp

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def read_field2d(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]
    print nx,ny
    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def main():

    config = utils.get_config("ParsedInput.txt")
    bpath = 'statdata/ph/PhBirth/'
    dpath = 'statdata/ph/PhDeath/'
    ezpath = 'data/E2z'
    bzpath = 'data/B2z'
    picspath = 'pics'
    Xmax = float(config['X_Max']) #mkm
    Xmin = float(config['X_Min']) #mkm
    Ymax = float(config['Y_Max']) #mkm
    Ymin = float(config['Y_Min']) #mkm
    Zmax = float(config['Z_Max']) #mkm
    Zmin = float(config['Z_Min']) #mkm
    nx1 = int(config['MatrixSize_X'])
    ny1 = int(config['MatrixSize_Y'])
    nz1 = int(config['MatrixSize_Z'])
    dx = (Xmax-Xmin)/nx1
    dy = (Ymax-Ymin)/ny1
    dz = (Zmax-Zmin)/nz1
    delta = 1
    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(bpath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(bpath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax) + ' files'

    nx = int(config['QEDstatistics.nX'])
    xmax = float(config['QEDstatistics.XMax'])*1e4
    nx1 = int(config['MatrixSize_X'])
    ny1 = int(config['MatrixSize_Y'])
    dx1 = float(config['Step_X'])
    dy1 = float(config['Step_Y'])
    wl = float(config['Wavelength'])
    dmy = int(0.35*wl/dy1)
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    peakpowerpw = int(config['PeakPowerPW'])
   
    T = 2 * math.pi/omega*1e15
    
    step = x0*y0*1e15/T
    nT = int(1./step)

    birth = []
    death = []
    ez_t = []
    bz_t = []

    sp_path = 'birth_s_%d' % (peakpowerpw)
    if not os.path.exists(sp_path):
        os.makedirs(sp_path)
    shutil.copy("ParsedInput.txt", sp_path)
    shutil.copy("Input.txt", sp_path)

    for i in range(nmin,nmax,delta):
        bname = bpath + '/' + "%.4f.txt" % (i,)
        print bname
        b = read_bd(bname)
        birth.append(b)
        dname = dpath + '/' + "%.4f.txt" % (i,)
        print dname
        d = read_bd(dname)
        death.append(d)
        ezname = ezpath + '/' + "%06d.txt" % (i,)
        bzname = bzpath + '/' + "%06d.txt" % (i,)
        ez = read_field2d(ezname,nx1,ny1)
        bz = read_field2d(bzname,nx1,ny1)
        ezv = ez[nx1/2][ny1/2-dmy]
        bzv = bz[nx1/2][ny1/2]
        ez_t.append(ezv)
        bz_t.append(bzv)
    axis = create_axis(nmax-nmin,step,nmin*step)
    a = 10.
    print max(ez_t), max(bz_t)
    ezm = a*max(ez_t)
    bzm = a*max(bz_t)
    for i in range(len(ez_t)):
        ez_t[i] /= ezm
        bz_t[i] /= ezm
    print 'E', ez_t
    print 'B', bz_t
    path = '.'
    read, ez = utils.bo_file_load(path + '/' + utils.ezpath,277,nx1,ny1)
    read, bz = utils.bo_file_load(path + '/' + utils.bzpath,270,nx1,ny1)
    ezp = np.abs(ez[nx/2])
    bzp = np.abs(bz[nx/2])
    ezp = ezp/np.amax(ezp)*0.5 + nmin*step
    bzp = bzp/np.amax(bzp)*0.5*1.53 + nmin*step
    e_axis = utils.create_axis(nx1, dx*1e4, Xmin*1e4)
    print len(e_axis), max(ezp)
    fig = plt.figure(num=None)
    plt.rc('text', usetex=True)
    ax = fig.add_subplot(2,2,1)
    
    ax.imshow(birth, extent=[-xmax, xmax, nmin*step, (nmax-1)*step], aspect = 'auto',  origin = 'lower')#, norm = clr.LogNorm())
    ax.plot(e_axis, ezp, 'b')
    ax.plot(e_axis, bzp, 'g')
    ax.autoscale(False)
    ax.set_ylabel('t, T')
    ax.set_xlabel('x, $\mu m$')
    ax.set_xlim([0,0.5])
    ax.set_ylim([nmin*step, (nmax-1)*step])
    ax = fig.add_subplot(2,2,3)
    ax.imshow(death, extent=[-xmax, xmax, nmin*step, (nmax-1)*step], aspect = 'auto',  origin = 'lower')#, norm = clr.LogNorm())
    ax.plot(e_axis, ezp, 'b')
    ax.plot(e_axis, bzp, 'g')
    ax.set_xlim([0,0.5])
    ax.set_ylim([nmin*step, (nmax-1)*step])
    ax.autoscale(False)
    ax.set_ylabel('t, T')
    ax.set_xlabel('x, $\mu m$')

    ax = fig.add_subplot(2,2,2)
    
    ax.imshow(birth, extent=[-xmax, xmax, nmin*step, (nmax-1)*step], aspect = 'auto',  origin = 'lower')#, norm = clr.LogNorm())
    ax.plot(ez_t,axis)
    ax.plot(bz_t, axis)
    ax.autoscale(False)
    ax.set_ylabel('t, T')
    ax.set_xlabel('x, $\mu m$')
    ax.set_xlim([0,0.5])
    ax.set_ylim([nmin*step, (nmax-1)*step])
    ax = fig.add_subplot(2,2,4)
    ax.imshow(death, extent=[-xmax, xmax, nmin*step, (nmax-1)*step], aspect = 'auto',  origin = 'lower')#, norm = clr.LogNorm())
    ax.plot(ez_t,axis)
    ax.plot(bz_t, axis)
    ax.set_xlim([0,0.5])
    ax.set_ylim([nmin*step, (nmax-1)*step])
    ax.autoscale(False)
    ax.set_ylabel('t, T')
    ax.set_xlabel('x, $\mu m$')
    plt.savefig(sp_path + '/' + 'birth_r_%d.png'%peakpowerpw)
    plt.close()

    fb = open(sp_path + '/' + 'ph_birth_r_%d.dat'%peakpowerpw, 'w')
    fd = open(sp_path + '/' + 'ph_death_r_%d.dat'%peakpowerpw, 'w')
    for i in range(len(birth)):
        for j in range(len(birth[0])):
            fb.write('%e ' % birth[i][j])
            fd.write('%e ' % death[i][j])
        fb.write('\n')
        fd.write('\n')
    fb.close()
    fd.close()

if __name__ == '__main__':
    main()
