#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import math
import utils
import sys
import numpy as np
print(np.__version__)
import os
from tqdm import *
print(np.__version__)
import pickle

def sum_over_window(array, window_size):
    res = np.empty_like(array)
    ws_half = window_size//2
    print('ws_half', ws_half)

    summ = 2.*np.sum(array[:ws_half])
    
    for i in range(len(array)):
        if i < ws_half:
            summ = summ + array[i+ws_half] - array[ws_half-i]
        elif i > len(array) - ws_half - 1:
            summ = summ + array[i+ws_half-len(array)] - array[ws_half-i]
        else:
            summ = summ + array[i+ws_half] - array[i-ws_half]
        res[i] = summ
    return res
        

def main():
    picspath = '/home/evgeny/Dropbox/Goszadanie'
    picspath1 = '/home/evgeny/storage/ОтчетГосзадание/Simulations/Electrons/pics'
    datapath = '/home/evgeny/storage/ОтчетГосзадание/Simulations/Electrons/data'
    path = '.'
    config = utils.get_config("ParsedInput.txt")
    fontsize = 14
    
    nit = float(config['BOIterationPass'])
    dt = float(config['TimeStep'])*1e15*nit #fs
    power = float(config['PeakPowerPW'])
    ppw = int(config['PeakPower'])*1e-22 #PW
    omega = float(config['Omega'])
    dt = float(config['TimeStep'])
    T = 2 * math.pi/omega
    dtt = dt/T
    print("Power = " + str(ppw) + 'PW')
    print("dt = " + str(dt) + 'fs')
    wl = float(config['Wavelength'])
    print(np.__version__) 
    Xmax = float(config['X_Max'])/1e-4 #mkm
    Xmin = float(config['X_Min'])/1e-4 #mkm
    Ymax = float(config['Y_Max'])/1e-4 #mkm
    Ymin = float(config['Y_Min'])/1e-4 #mkm
    Zmax = float(config['Z_Max'])/1e-4 #mkm
    Zmin = float(config['Z_Min'])/1e-4 #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    osc = float(config['gamma'])/(780. * math.sqrt(power))

    print('Nx = ' + str(nx)) 
    print('Ny = ' + str(ny))
    print('Nz = ' + str(nz))

    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz
    mult_dv = 1/(2.*dx*dy*dz*1e-12)
    step = (Xmax-Xmin)/nx
    
    nmin, nmax, delta = utils.get_min_max_iteration(sys.argv,
                                                    os.path.join('BasicOutput', 'data', 'Electron_Sum'),
                                                    os.path.join('BasicOutput', 'data', 'Positron_Sum'),
                                                    os.path.join('BasicOutput', 'data', 'Photon_Sum'))
    
    print('Here', nmin, nmax, delta)
    print(nx, dx, ny, dy)

    window = 10. # degrees
    theta_n = 128
    dtheta = 180./theta_n

    window_n = int(window/dtheta)
    if window_n%2 != 0:
        window_n += 1
        
    print(window_n)
    
    filename = os.path.join(datapath, 'profile_osc_%g_%gpw_window_%d.pkl' % (osc, power, window))
    print(filename)
    
    if os.path.exists(filename):
        with open(filename, 'rb') as f:
            ne0 = pickle.load(f)
            ne0_sum = pickle.load(f)
            ne0_sum_ = pickle.load(f)
            npos = pickle.load(f)
            npos_sum = pickle.load(f)
            npos_sum_ = pickle.load(f)
    else:
        electron_profile = np.zeros((theta_n))
        positron_profile = np.zeros((theta_n))
        photon_profile = np.zeros((theta_n))

        # initial distribution
        read, ne0 = utils.bo_file_load(os.path.join('BasicOutput', 'data', 'Electron_Sum'), 0, nx=2,ny=theta_n, ftype='bin',transpose = 1, verbose = 1, archive = None)
        ne0 = np.sum(ne0, axis=1)
        ne0_sum = sum_over_window(ne0, window_n)
        ne0_sum_ = (ne0_sum + ne0_sum[::-1]) * 0.5
        
        read, npos = utils.bo_file_load(os.path.join('BasicOutput', 'data', 'Positron_Sum'), nmax-1, nx=2,ny=theta_n, ftype='bin',transpose = 1, verbose = 1, archive = None)
        npos = np.sum(npos, axis=1)
        print(npos.shape)
        
        npos_sum = sum_over_window(npos, window_n)
        npos_sum_ = (npos_sum + npos_sum[::-1]) * 0.5
        #npos_sum = npos_sum/ne0_sum
        #print(ne0.shape)
        with open(filename, 'wb') as f:
            pickle.dump(ne0, f)
            pickle.dump(ne0_sum, f)
            pickle.dump(ne0_sum_, f)
            pickle.dump(npos, f)
            pickle.dump(npos_sum, f)
            pickle.dump(npos_sum_, f)
            
    mp.rcParams.update({'font.size': fontsize})

    axis = utils.create_axis(theta_n, dtheta)
    
    fig = plt.figure(figsize=(15, 5))
    ax1 = fig.add_subplot(1, 3, 1)
    ax2 = fig.add_subplot(1, 3, 2)
    ax3 = fig.add_subplot(1, 3, 3)
    #ax1.plot(ne0, 'r', label='ne0')
    #ax1.plot(ne0_sum/window_n, 'b', label='ne0_sum')
    ax1.set_ylabel('N$_\mathrm{e}0$')
    ax1.plot(axis, ne0, 'b', label='data')
    ax1.plot(axis, ne0_sum, 'g', label='sum')
    ax1.plot(axis, ne0_sum_, 'r', label='sum aver.')
    ax1.set_ylim([0, 1.05*ne0_sum.max()])
    ax1.set_xlim([0,180])
    ax2.set_ylabel('N$_\mathrm{p}$')
    ax2.plot(axis, npos, 'b', label='data')
    ax2.plot(axis, npos_sum, 'g', label='sum')
    ax2.plot(axis, npos_sum_, 'r', label='sum aver.')
    ax2.set_ylim([0, 1.05*npos_sum.max()])
    ax2.set_xlim([0,180])
    #ax2.imshow(npos_, aspect='auto')
    ax3.set_ylabel('N$_\mathrm{p}$/N$_\mathrm{e}0$')
    ax3.plot(axis, npos/ne0, 'b', label='data')
    ax3.plot(axis, npos_sum/ne0_sum, 'g', label='sum')
    ax3.plot(axis, npos_sum_/ne0_sum_, 'r', label='sum aver.')
    ax3.set_ylim([0, 1.05*(npos_sum/ne0_sum).max()])
    ax3.set_xlim([0,180])
    ax1.legend(loc='center left', fontsize=12, frameon=False)
    ax2.legend(loc='upper left', fontsize=12, frameon=False)
    ax3.legend(loc='lower center', fontsize=12, frameon=False)
    picname = os.path.join(picspath, 'profile_osc_%g_%gpw_window_%d.png' % (osc, power, window))
    print(picname)
    plt.tight_layout()
    plt.savefig(picname, dpi=256)
    picname1 = os.path.join(picspath1, 'profile_osc_%g_%gpw_window_%d.png' % (osc, power, window))
    plt.savefig(picname1, dpi=256)

    
    
if __name__ == '__main__':
    main()
