#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_gamma(filename):
    f = open(filename, 'r')
    tmp = f.readline().split()
    return float(tmp[0]), float(tmp[1]), float(tmp[2]), float(tmp[3]), float(tmp[4]), float(tmp[5]), float(tmp[6])

def read_ne(filename):
    f = open(filename, 'r')
    tmp = f.readline().split()
    return float(tmp[0])

def main():
    n = len(sys.argv) - 1
    power = []
    avgamma = []
    av10gamma = []
    maxne = []
    conc = []
    power = []
    phase = []
    periods = []
    g_min = []
    g_max = []
    g_min_ = []
    g_max_ = []
    for i in range(n):
        path = sys.argv[i+1]
        config = utils.get_config(path + "/ParsedInput.txt")
        pwr = float(config['PeakPowerPW'])
        pwr, gmm, gmm10, cn, phase_, gmin, gmax = read_gamma(path + '/gamma_av.dat')
        cn = read_ne(path + '/conc.dat')
        ne = read_ne(path + '/max_ne.dat')
        ph = read_ne(path + '/phase.dat')
        np = read_ne(path + '/num_periods.dat')
        maxne.append(ne)
        conc.append(cn)
        phase.append(ph/math.pi)
        power.append(pwr)
        periods.append(np)
        av10gamma.append(gmm10)
        g_min.append(gmin)
        g_max.append(gmax)
    pow_ = 7./8.
    for i in range(len(power)):
        g_min_.append(2*math.pow(power[i], pow_)*maxne[0]/math.pow(power[0], pow_))
        g_max_.append(2*math.pow(power[i], pow_)*conc[0]/math.pow(power[0], pow_))

    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(3,1,1)
    p3, = ax1.plot(power, maxne, 'r')
    p4, = ax1.plot(power, conc, 'b')
#    ax1.set_yscale('log')
#    ax1.set_xscale('log')
    plt.legend([p3, p4], ['Ne_max', 'conc_av'], loc=2)
    ax1.set_xlabel('Power')
    ax1.set_ylabel('Ne, cm-3')
    ax2 = fig.add_subplot(3,1,2)
    ax2.plot(power, phase, 'r')
    ax2.set_ylim([0,1])
    ax2.set_xlim([0,100])
    ax1.set_xlim([0,100])
    ax3 = ax2.twinx()
    ax3.plot(power, periods, 'b')
    ax3.set_xlim([0,100])
    ax4 = fig.add_subplot(3,1,3)
    ax4.plot(power, av10gamma, 'r')
    ax4.plot(power, g_min, 'g')
    ax4.plot(power, g_max, 'b')
    ax1.plot(power, g_min_, ':', color = 'r')
    ax1.plot(power, g_max_, ':', color = 'b')
    ax1.set_xlim([10,100])
    ax1.set_yscale('log')
    ax1.set_xscale('log')
    plt.show()

if __name__ == '__main__':
    main()
