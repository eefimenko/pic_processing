#!/bin/bash

# Size of chunk, number of files to compress and download
# from the remote directory at once
num_files=25

function create_dir
{
    if [ ! -d $1 ]; 
    then
	mkdir $1
    else
	echo "$1 already exists"
    fi
}

function get_remote()
{
    if [ "$dlist" == "all" ]
    then
	list=`ssh $key $ssh_host "ls ${wd_remote}/$1/data"`
    else
	list="$dlist"
    fi

    echo $list
    echo "Copy BasicOutput data from $ssh_host:${wd_remote}/$1//data"
    for dir in $list
    do
	create_dir BasicOutput/data/$dir
	cd BasicOutput/data/$dir
	echo "Copying $dir"
	transfer ${wd_remote}/$1//data/$dir "$2"
	cd ../../..
    done
}

function get_remote_pics()
{
    if [ "$dlist" == "all" ]
    then
	list=`ssh $key $ssh_host "ls ${wd_remote}/$1//pics"`
    else
	list="$dlist"
    fi

    echo $list
    cd BasicOutput/pics
    echo "Copy BasicOutput pics from $ssh_host:${wd_remote}/$1//pics"
    transfer ${wd_remote}/$1//pics "$2"
    cd ../..
}

function save_dir()
{
	echo  "$1" > ./log.txt 
	echo  "$2" >> ./log.txt 
}

function save_host()
{
    echo  "$1" > ./host.txt 
    echo  "${@:2:3}" >> ./host.txt 
}

function save_iterations()
{
    echo  "$1" > ./iterations.txt 
}

function save_dlist()
{
    echo  "$1" > ./dlist.txt 
}

function get_remote_vtk()
{
    create_dir VTKOutput
    list=`ssh $key $ssh_host "cd ${wd_remote}/$1/VTKOutput/ && ls $2.vtk"`
    echo $list
    echo "Copy VTKOutput data from $ssh_host:${wd_remote}/$1/VTKOutput"
    for file in $list
    do
	if [ ! -f VTKOutput/${file} ]; then
	    scp $key $ssh_host:${wd_remote}/$1/VTKOutput/${file} VTKOutput
	else
	    echo "${file} esists"
	fi
    done
}

function transfer()
{
    rdir0=$1
    nl=0
    ns=1
    
    while [ $nl -ne $ns ] 
    do
	if [ "$2" == "all" ]
	then
	    ssh $key $ssh_host "cd $rdir0 && stat -c\"%n\" * > .filelist.dat"
	    ssh $key $ssh_host "cd $rdir0 && stat -c\"%s\" * > .sizelist.dat"
	else
	    ssh $key $ssh_host "cd $rdir0 && stat -c\"%n\" $2 > .filelist.dat"
	    ssh $key $ssh_host "cd $rdir0 && stat -c\"%s\" $2 > .sizelist.dat"
	fi
	scp $key $ssh_host:${rdir0}/.filelist.dat . 
	scp $key $ssh_host:${rdir0}/.sizelist.dat . 

	filelist=() # Create array
	while read line # Read a line
	do
	    filelist+=("$line") # Append line to the array
	done < ".filelist.dat"
	sizelist=() # Create array
	while read line # Read a line
	do
	    sizelist+=("$line") # Append line to the array
	done < ".sizelist.dat"
    
	nl=${#filelist[@]}
	ns=${#sizelist[@]}

	if [ $nl -ne $ns ]; then
	    echo "Sizes of .filelist and .sizelist are different. Reread filelist"
	fi
    done

    list2=""
    num=0
    for (( i=0; i <= ((nl-1)); i++ ))
    do
	if [ ! -f ${filelist[$i]} ]; then
	    echo "File ${filelist[$i]} missing, downloading"
	    list2="$list2 ${filelist[$i]}"
	    num=$((num+1))
	else
	    localsize=`stat -c%s ${filelist[$i]}`
	    if [ $localsize -ne ${sizelist[$i]} ]; then
		echo "File ${filelist[$i]} corrupted, recovering"
		list2="$list2 ${filelist[$i]}"
		num=$((num+1))
	    fi
	fi
    done
    echo "Total number of files to download: $num" 
    list3=""
    for file in $list2
    do
	list3="$list3 $file"
	n=`echo -n $list3 | wc -w`

	if [ $n -eq $num_files ]; then
	    echo "Prepairing $n files. Filelist:"
	    echo $list3
	    echo "Archiving part.tar.bz2"
	    ssh $key $ssh_host "cd ${rdir0} && tar -cjf part.tar.bz2 $list3" 
	    echo "Copying part.tar.bz2"
	    scp $key $ssh_host:${rdir0}/part.tar.bz2 . 
	    echo "Unpacking part.tar.bz2"
	    tar -xjf part.tar.bz2 && rm part.tar.bz2 
	    ssh $key $ssh_host "cd ${rdir0} && rm part.tar.bz2" 
	    list3=""
	fi
    done
    n=`echo -n $list3 | wc -w`
    if [ $n -gt 0 ]; then
	echo "Downloading last $n files. Filelist:"
	echo $list3
	ssh $key $ssh_host "cd ${rdir0} && tar -cjf part.tar.bz2 $list3" 
	scp $key $ssh_host:${rdir0}/part.tar.bz2 . 
	echo "Unpacking part.tar.bz2"
	tar -xjf part.tar.bz2 && rm part.tar.bz2
	ssh $key $ssh_host "cd ${rdir0} && rm part.tar.bz2" 
    fi
    rm .filelist.dat
    rm .sizelist.dat
    ssh $key $ssh_host "cd ${rdir0} && rm .filelist.dat" 
    ssh $key $ssh_host "cd ${rdir0} && rm .sizelist.dat" 
}

function get_remote_statdata()
{
    list=`ssh $key $ssh_host "ls ${wd_remote}/$1/statdata"`
#    list="ph"
#    list="ph el"
    echo "Copy statdata from $ssh_host:${wd_remote}/$1/statdata"
    create_dir statdata
    for dir in $list
    do
	list1=`ssh $key $ssh_host "ls ${wd_remote}/$1/statdata/${dir}"`
#	list1="EnSpSph EnAngSp"
#	list1="EnSpSph EnAngSp angSpSph"
	echo "Copy statdata from $ssh_host:${wd_remote}/$1/statdata/${dir}"
	create_dir statdata/$dir
	for dir1 in $list1
	do
	    create_dir statdata/${dir}/${dir1}
	    cd statdata/${dir}/${dir1}
	    echo "Copying ${dir}/${dir1}"
	    transfer ${wd_remote}/$1/statdata/${dir}/${dir1} "$2"
	    cd ../../..
	done
    done
}

function get_remote_pt()
{
    echo "Copy ParticleTracking from $ssh_host:${wd_remote}/$1/ParticleTracking"
    list=`ssh $key $ssh_host "ls ${wd_remote}/$1/ParticleTracking"`
    echo ${list}
    create_dir ParticleTracking
    for dir in $list
    do
	echo "Copying ParticleTracking"
	create_dir ParticleTracking/${dir}
	cd ParticleTracking/${dir}
	transfer ${wd_remote}/$1/ParticleTracking/${dir} "$2"
	cd ../..
    done
   
}

function get_remote_config()
{
    echo "Copy config $ssh_host:${wd_remote}/$1/ParsedInput.txt"
    scp $key $ssh_host:${wd_remote}/$1/ParsedInput.txt . 
    scp $key $ssh_host:${wd_remote}/$1/Input.txt . 
    scp $key $ssh_host:${wd_remote}/$1/FieldEnergy.txt . 
    scp $key $ssh_host:${wd_remote}/$1/PerformanceSummary.txt . 
}

# reading saved rdir and remote working dir
if [ "$#" -eq 1 ]; then
    dir=$1
    rdir=""
fi

if [ "$#" -eq 2 ]; then
    dir=$2
    rdir=$1
fi

create_dir $dir
cd $dir

# reading saved host and key
if [ -f "host.txt" ]; then
    i=0
    while IFS= read -r var
    do
	if [ $i -eq 0 ]; then
	    ssh_host=$var
	else
	    if [ $i -eq 1 ]; then
		key=$var
	    else
		echo "Something wrong, exiting"
	    fi
	fi
	i=$((i+1))
    done < "host.txt"
    echo "host.txt exists, continue downloading from $ssh_host"
else
    while true; do
    read -p "Choose the cluster 1. local 2. Exit :" cl
    case $cl in
        1 ) user=evgeny; host=10.0.11.28; wd_remote=/home/evgeny/d3/results; key=' '; break;;
       	2 ) exit;;
        * ) echo "Please choose from the variants 1-2";;
    esac
    done
    
    ssh_host=${user}@${host}
    save_host $ssh_host $key
fi

# reading saved rdir and remote working dir
if [ -f "log.txt" ]; then
    i=0
    while IFS= read -r var
    do
	if [ $i -eq 0 ]; then
	    rdir=$var
	else
	    if [ $i -eq 1 ]; then
		wd_remote=$var
	    else
		echo "Something wrong, exiting"
	    fi
	fi
	i=$((i+1))
    done < "log.txt"
    echo "log.txt exists, continue downloading from $rdir"
else
    if [ -z "$rdir" ]; then
	echo "Unknown remote directory, exiting"
	exit -1
    fi
    save_dir $rdir $wd_remote
fi

# reading saved iterations
if [ -f "iterations.txt" ]; then
    read -r iterations < "iterations.txt"
    echo "iterations.txt exists, continue downloading $iterations"
else
    while true; do
    read -p "Input which iterations to download: 1. All 2. Selected 3. exit " cl
    case $cl in
        1 ) iterations='all'; break;;
        2 ) read -p "Input regular expression which iterations to download: " iterations; break;;
	3 ) exit;;
        * ) echo "Please choose from the variants 1-3";;
    esac
    done
    save_iterations $iterations
fi

# reading saved download list
if [ -f "dlist.txt" ]; then
    read -r dlist < "dlist.txt"
    echo "list.txt exists, continue downloading $dlist"
else
    while true; do
    read -p "Input which directories to download: 1. All 2. Selected 3. None 4. exit " cl
    case $cl in
        1 ) dlist='all'; break;;
        2 ) list=`ssh $key $ssh_host "ls ${wd_remote}/${rdir}/data"`;
	    echo $list; read -p "Input directory names: " dlist; break;;
	3 ) dlist=' '; break;;
	4 ) exit;;
        * ) echo "Please choose from the variants 1-3";;
    esac
    done
    save_dlist "$dlist"
fi

echo "Ssh host: $ssh_host"
echo "Ssh key: $key"
echo "Save path: $dir"
echo "Download path: $rdir"
echo "Working directory: $wd_remote"
echo "Iterations to download: $iterations"
echo "Directory list to download: $dlist"

create_dir BasicOutput
create_dir BasicOutput/data
create_dir BasicOutput/pics

get_remote_config $rdir

#get_remote_pics $rdir "$iterations"
get_remote $rdir "$iterations"
get_remote_statdata $rdir "$iterations"
#get_remote_pt $rdir $iterations
#get_remote_vtk $rdir electric_field_*00
#get_remote_vtk $rdir *_*00

cd ..
