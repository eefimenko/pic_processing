#!/usr/bin/python
import matplotlib.pyplot as plt
import os
import sys
import math
import utils
import numpy as np
import matplotlib as mp
from matplotlib.font_manager import FontProperties

def main():
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
    path = './'
    picspath = '/home/evgeny/Dropbox/MG/'
    fontsize = 18
    config = utils.get_config(path + "/ParsedInput.txt")
    wl = float(config['Wavelength'])
    xmax = float(config['X_Max'])/wl #mkm
    xmin = float(config['X_Min'])/wl #mkm
    ymax = float(config['Y_Max'])/wl #mkm
    ymin = float(config['Y_Min'])/wl #mkm
    zmax = float(config['Z_Max'])/wl #mkm
    zmin = float(config['Z_Min'])/wl #mkm
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    power = int(config['PeakPowerPW'])
    dx = float(config['Step_X'])/wl
    dy = float(config['Step_Y'])/wl
    dz = float(config['Step_Z'])/wl
    mult_ = 1/(2.*dx*dy*dz*wl*wl*wl)
    step = float(config['TimeStep'])*1e15*float(config['BOIterationPass'])
    omega = float(config['Omega'])
    T = 2 * math.pi/omega
    nt = int(T*1e15/step)
    print(nt)
    iterations = [470, 590, 662]
    #iterations = [344, 380, 760]
    figures = [os.path.join('BasicOutput', utils.ezpath),
               os.path.join('BasicOutput', utils.nezpath),
               os.path.join('BasicOutput', utils.npzpath)
               ]
    relf = omega*3e10*9.1e-28/4.8e-10
    
    cmaps = ['Reds', 'Greens', 'Blues', 'hot_r', 'RdPu']
    titles = ['Electric field', 'Electrons', 'Positrons']
    log = [False, True, True, False]
    mult = [1./relf, 1.e15, mult_*1e15]
   

    spx_ = len(iterations)
    spy_ = len(figures)
    labels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h','i', 'j', 'k', 'l', 'm']
    labels2 = ['47 T', '59 T', '66.2 T']
    nperiods = 5
    steps_per_period = 10
    verbose = 1
#    plt.rcParams.update({'mathtext.default':  'regular' })
    fig = plt.figure(num=None, figsize=(3.7*spy_, 3.1*spx_))
    for k in range(spx_):
        i = iterations[k]
        for j in range(spy_):
           
            mp.rcParams.update({'font.size': fontsize})
            yticks = [-4, -2.,0,2., 4]
            xticks = [-4, -2.,0,2., 4]
            lims = [-4,4]
            
            if k == spx_ -1:
                xlabel = 'x/$\lambda$'
                xticklabels = ['$-4$', '$-2$','$0$','$2$', '$4$']
            else:
                xlabel = ''
                xticklabels = []
                
            if j == 0:    
                ylabel = 'z/$\lambda$'
                yticklabels = ['$-4$', '$-2$','$0$','$2$', '$4$']
            else:
                ylabel = ''
                yticklabels = []
                
            if figures[j] == utils.nezpath or figures[j] == utils.npzpath:
                ncbarticks = None
            else:
                ncbarticks = None

            if k != 2 and j > 0:
                logarithmic = log[j]
                #ncbarticks = 1
                scatter = None
                colorbar = True
                if j == 2 and k == 0:
                    vmin = 0
                    vmax = 1
                    logarithmic = False
                else:
                    vmin = None
                    vmax = None
            else:
                logarithmic = log[j]
                scatter = None
                colorbar=True
                vmin = None
                vmax = None
            mult1 = mult[j] 

            if k == 0:
                title = titles[j]
            else:
                title = None
                
            if j == 0:
                
                    # fx = utils.bo_file_load(dirs[k]+utils.ezpath,
                    #                         i+n0[k],nx[k],ny[k],verbose=1)
                    # fy = utils.bo_file_load(dirs[k]+utils.bzpath,
                    #                         i+n0[k],nx[k],ny[k],verbose=1)
                    # sf = np.square(fx) + np.square(fy)
                    
                ax = utils.subplot(fig, i, path + figures[j],
                                   shape = (nx,ny), position = (spx_,spy_,j+spy_*k+1),
                                   extent = [xmin, xmax, ymin, ymax],
                                   cmap = cmaps[j], title = title,
                                   colorbar = True, logarithmic=logarithmic, verbose=verbose,
                                   xlim = lims, ylim = lims, xticks = xticks, yticks = yticks,yticklabels = yticklabels, xticklabels = xticklabels,
                                   xlabel = xlabel, ylabel = ylabel, fontsize=fontsize, mult = mult[j], transpose = 1, ncbarticks = ncbarticks)
            else:
                ax = utils.subplot(fig, i, path+figures[j],
                                   shape = (nx,ny), position = (spx_,spy_,j+spy_*k+1),
                                   extent = [xmin, xmax, ymin, ymax],
                                   cmap = cmaps[j], title = title, titletype = 'simple',
                                   colorbar = colorbar, logarithmic=logarithmic, verbose=verbose,
                                   xlim = lims, ylim = lims, xticks = xticks, yticks = yticks,yticklabels = yticklabels, xticklabels = xticklabels, vmin=vmin, vmax=vmax,
                                   xlabel = xlabel, ylabel = ylabel,
                                   maximum = 'local', fontsize = fontsize, mult = mult1, transpose = 1, ncbarticks = ncbarticks, scatter=scatter)
                
                                  
                
            #if j == 0:
            ax.text(-5.0*lims[1]/5., 3.8*lims[1]/5., '$(%s)$'%(labels[k*spy_ + j]), fontsize = 16)
            ax.text(1.0*lims[1]/5., 3.8*lims[1]/5., '$%s$'%(labels2[k]), fontsize = 16, bbox = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9))
            #else:
            #    ax.text(-5.0*lims[1]/5., 3.8*lims[1]/5, '$(%s)$'%(labels[k*spy_ + j]), fontsize = 16)
            picname = picspath + '/' + "distribution_2d_dens2.png"
    plt.tight_layout()
    plt.savefig(picname, bbox_inches='tight', dpi=256)
    plt.close()

    
    # picname = picspath + '/' + "fig4_full.png"
    # # plt.tight_layout()
    # plt.savefig(picname, bbox_inches='tight', dpi=128)
    # plt.close()
    # plt.show()
    
if __name__ == '__main__':
    main()
