#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys
import shutil
import os

def read_field(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)/norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def main():
    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
    ex_xpath = 'data/Ex_x'
    ex_ypath = 'data/Ex_y'
    ex_zpath = 'data/Ex_z'
    ey_xpath = 'data/Ey_x'
    ey_ypath = 'data/Ey_y'
    ey_zpath = 'data/Ey_z'
    ez_xpath = 'data/Ez_x'
    ez_ypath = 'data/Ez_y'
    ez_zpath = 'data/Ez_z'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    bx_xpath = 'data/Bx_x'
    bx_ypath = 'data/Bx_y'
    bx_zpath = 'data/Bx_z'
    by_xpath = 'data/By_x'
    by_ypath = 'data/By_y'
    by_zpath = 'data/By_z'
    bz_xpath = 'data/Bz_x'
    bz_ypath = 'data/Bz_y'
    bz_zpath = 'data/Bz_z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nixpath = 'data/Proton2Dx'
    niypath = 'data/Proton2Dy'
    nizpath = 'data/Proton2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nphxpath = 'data/Photon2Dx'
    nphypath = 'data/Photon2Dy'
    nphzpath = 'data/Photon2Dz' 
    nepath = 'data/NeTrap/'
    nppath = 'data/NposTrap/'
    phsp = 'statdata/ph/EnSp/'
    
    picspath = 'pics'
    config = utils.get_config("ParsedInput.txt")
    wl = float(config['Wavelength'])
    peakpower = float(config['PeakPower'])*1e-7*1e-15
    peakpowerpw = int(config['PeakPowerPW'])
    sp_path = 'spectra_%d' % (peakpowerpw)

    Xmax = float(config['X_Max'])/wl #mkm to wavelength
    Xmin = float(config['X_Min'])/wl #mkm to wavelength
    Ymax = float(config['Y_Max'])/wl #mkm to wavelength
    Ymin = float(config['Y_Min'])/wl #mkm to wavelength

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)
    coeff = 10./(3.3e11*3.3e11)*0.97399*(wl/0.8e-4)*(wl/0.8e-4)
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    delta = 1

    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])

    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
    print num, T
    step = x0*y0*1e15/T

    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(expath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(expath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'

    dv = 2*dx*dy*dz
    axis1 = create_axis(nx, (Xmax-Xmin)/nx, Xmin)
    ez_t = []
    bx_t = []
    ne_t = []
    np_t = []
    dmy = int(0.35*wl/dy)
    if not os.path.exists(sp_path):
        os.makedirs(sp_path)
    shutil.copy("ParsedInput.txt", sp_path)
    shutil.copy("Input.txt", sp_path)
    f = open(sp_path + '/n.dat', 'w')
    for i in range(nmin, nmax, delta):
        if i%10 == 0:
            print i
#        name = expath + '/' + "%06d.txt" % (i,)
#        fielde2x = read_field(name,nx,ny)
#        m = max2d(fielde2x)

#        name = ex_xpath + '/' + "%06d.txt" % (i,)
#        fieldex_x = read_field(name,nx,ny)
        
#        name = ey_xpath + '/' + "%06d.txt" % (i,)
#        fieldey_x = read_field(name,nx,ny)
        
        name = ezpath + '/' + "%06d.txt" % (i,)
        fieldez_x = read_field(name,nx,ny)
        ez = fieldez_x[nx/2][ny/2]
        name = bzpath + '/' + "%06d.txt" % (i,)
        fieldbx_x = read_field(name,nx,ny)
        bx = fieldbx_x[nx/2][ny/2-dmy]*1.53
        tmp = coeff*(ez*ez + bx*bx)
        name = nexpath + '/' + "%06d.txt" % (i,)
        ne_x = read_field(name,nx,ny)
        if max2d(ne_x)/dv > 1e27:
            break
        ez_t.append(ez)
        
#        ne_t.append(max2d(ne_x)/dv)
#        name = bxpath + '/' + "%06d.txt" % (i,)
#        fielde2x = read_field(name,nx,ny)
#        m = max2d(fielde2x)
        
        
#        bx_t.append(bx)
        name = npxpath + '/' + "%06d.txt" % (i,)
        np_x = read_field(name,nx,ny)
        np_t.append(max2d(np_x)/dv)
        e = read_trap(nepath + '%06d.txt' % (i,))
        p = read_trap(nppath + '%06d.txt' % (i,))
        shutil.copy(phsp+'/%.4f.txt'%i, sp_path)
        f.write('%d %lf\n'%(i, e+p))
        ne_t.append(e+p)
#        name = by_xpath + '/' + "%06d.txt" % (i,)
#        fieldby_x = read_field(name,nx,ny)
        
#        name = bz_xpath + '/' + "%06d.txt" % (i,)
#        fieldbz_x = read_field(name,nx,ny)
        
    
#        plt.show()
    f.close()
    fig = plt.figure()
    ezx = fig.add_subplot(1,1,1)
    ezx.plot(ez_t, 'r', label = 'E')
#    ezx.plot(bx_t, 'g', label = 'Bx')
    ezx.legend(loc='lower left', shadow=True)
#    ezx.set_xlim([0,20])
    nx = ezx.twinx()
    nx.plot(ne_t, 'b', label = 'Ne+Np')
#    nx.plot(np_t, 'k', label = 'Np')
    nx.set_yscale('log')
    nx.legend(loc='upper left', shadow=True)
    plt.savefig(sp_path + '/' + "e_n.png")
    plt.close()
    shutil.make_archive(sp_path, 'zip', sp_path)
if __name__ == '__main__':
    main()
