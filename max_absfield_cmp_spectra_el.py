#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys
import os

def read_field(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)/norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1e-9)
        field.append(row)
    return field

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])

def calc_current(array,nx,ny,dx,dy,r):
    num = 0.
    for i in range(nx):
        x = (i - nx/2)*dx
        for j in range(ny):
            y = (j-ny/2)*dy
            if x*x + y*y < r*r:
                num += array[i][j]
    return num

def main():
    picspath = 'pics'

    n = len(sys.argv)
    fig = plt.figure()
    ezx = fig.add_subplot(4,2,1)
    ezx.set_title('Electric field')
    bzx = fig.add_subplot(4,2,2)
    bzx.set_title('Magnetic field')
    ncrx = fig.add_subplot(4,2,4)
    ncrx.set_title('Ne/Ncr')
    nex = fig.add_subplot(4,2,3)
    nex.set_title('Ne')
    powx = fig.add_subplot(4,2,6)
    powx.set_title('Power')
    maxx = fig.add_subplot(4,2,7)
    maxx.set_title('Max energy')
    avx = fig.add_subplot(4,2,8)
    avx.set_title('Av energy')
    jx = fig.add_subplot(4,2,5)
    jx.set_title('Number of particles')
   
    electronMass = 9.10938e-28
    electronCharge = 4.8032e-10
    const_a_p = 7.81441e-9
    filename = 'ez_b_ne_ncr_j'
    power_p = []
    ezx_p = []
    bzx_p = []
    nex_p = []
    nemaxx_p = []
    ncrx_p = []
    jx_min_p = []
    jx_max_p = []
    powx_p = []
    pow1x_p = []
    maxx_p = []
    avx_p = []
    el_powx_p = []
    el_pow1x_p = []
    el_maxx_p = []
    el_avx_p = []
      
    for i in range(n-1):
        path = sys.argv[i+1] + '/'
        config = utils.get_config(path + "/ParsedInput.txt")
        dx = float(config['Step_X'])
        dy = float(config['Step_Y'])
        dz = float(config['Step_Z'])
        nx = int(config['MatrixSize_X'])
        ny = int(config['MatrixSize_Y'])
        BOIterationPass = int(config['BOIterationPass'])
        TimeStep = float(config['TimeStep'])
        dt = BOIterationPass*TimeStep*1e15
        step = float(config['TimeStep'])*float(config['BOIterationPass'])
        omega = float(config['Omega'])
       
        T = 2*math.pi/omega
        nper = int(T/step)/2
        dv = 2*dx*dy*dz
        coeffj = 2. * 1.6e-19 * 3e8 /(2*dz*1e-2)*1e-6
        wl = float(config['Wavelength'])
        print 'J = ', coeffj
        ppw = float(config['PeakPowerPW'])
        ncr = electronMass * omega * omega/(8. * math.pi * electronCharge * electronCharge)
        a0 = const_a_p * math.sqrt(ppw*1e22)
        ax_ = []
        ax1_ = []
        ez_t = []
        pow_t = []
        pow1_t = []
        max_t = []
        av_t = []
        br_full_t = []
        br_100mev_t = []
        br_1gev_t = []
        br_full1_t = []
        br_100mev1_t = []
        br_1gev1_t = []
        S = 2 * math.pi * 0.1 * 0.1 * wl * wl * 100 # square of source mm^2, r = 0.1 lambda 
        ds = 1.5e4 # mrad^2
        f = open(path + 'br_t.dat', 'r')
        i = 0
        T1 = 2*math.pi/omega*1e15
        for line in f:
            tmp = line.split()
            ax_.append(float(tmp[0])/T1)
            ez_t.append(float(tmp[1])/1.18997e+8/a0)

            pow_t.append(ppw-float(tmp[2]))
            pow1_t.append(float(tmp[2])/ppw)
            max_t.append(float(tmp[3]))
            av_t.append(float(tmp[4]))
            ax1_.append(float(tmp[0])/T1 - 2.)
            br_full_t.append(float(tmp[5]))
            br_100mev_t.append(float(tmp[6]))
            br_1gev_t.append(float(tmp[7])/1e23)
            br_full1_t.append(float(tmp[5])/S/ds)
            br_100mev1_t.append(float(tmp[6])/S/ds)
            br_1gev1_t.append(float(tmp[7])/S/ds)
        #npow = len(pow_t)
        num_ = nper
        pow_av = utils.avg(pow_t, num_)
        pow1_av = utils.avg(pow1_t, num_)
        max_av = utils.avg(max_t, num_)
        av_av = utils.avg(av_t, num_)
        el_ax_ = []
        el_ax1_ = []
        
        el_pow_t = []
        el_pow1_t = []
        el_max_t = []
        el_av_t = []
        el_br_full_t = []
        el_br_100mev_t = []
        el_br_1gev_t = []
        el_br_full1_t = []
        el_br_100mev1_t = []
        el_br_1gev1_t = []
        f = open(path + 'el_br_t.dat', 'r')
        i = 0
        T1 = 2*math.pi/omega*1e15
        for line in f:
            tmp = line.split()
            el_ax_.append(float(tmp[0])/T1)
            
            el_pow_t.append(ppw-float(tmp[2]))
            el_pow1_t.append(float(tmp[2])/ppw)
            el_max_t.append(float(tmp[3]))
            el_av_t.append(float(tmp[4]))
            el_ax1_.append(float(tmp[0])/T1 - 2.)
            el_br_full_t.append(float(tmp[5]))
            el_br_100mev_t.append(float(tmp[6]))
            el_br_1gev_t.append(float(tmp[7])/1e23)
            el_br_full1_t.append(float(tmp[5])/S/ds)
            el_br_100mev1_t.append(float(tmp[6])/S/ds)
            el_br_1gev1_t.append(float(tmp[7])/S/ds)
        #npow = len(pow_t)
        num_ = nper
        el_pow_av = utils.avg(el_pow_t, num_)
        el_pow1_av = utils.avg(el_pow1_t, num_)
        el_max_av = utils.avg(el_max_t, num_)
        el_av_av = utils.avg(el_av_t, num_)
      
        print a0
        ncr *= a0
        
        nmin,nmax = utils.find_min_max_from_directory(path + utils.ezpath, path + utils.bzpath, path + utils.nezpath)
#        num = nmax - nmin
        ez_t = np.zeros(nmax)
        bz_t = np.zeros(nmax)
        ne_t = np.zeros(nmax)
        ncr_t = np.zeros(nmax)
        j_t = np.zeros(nmax)
        
        print 'min =', nmin, ' max =', nmax
        axis_t = utils.create_axis(nmax, dt/T1)
        
        nmin = utils.read_tseries(path,filename, ez_t, bz_t, ne_t, ncr_t, j_t)
        # ntr_t = utils.ts(path, utils.netpath, name='ne_trapped',
        #                  tstype='sum', verbose=True)
        # axisn_t = utils.create_axis(len(ntr_t), dt/T1)
        for i in range(nmin, nmax):
            print i
            fieldez_x = utils.bo_file_load(path + utils.ezpath,i,nx)
            ez_t[i] = np.amax(fieldez_x)
            fieldbz_x = utils.bo_file_load(path + utils.bzpath,i,nx)
            bz_t[i] = np.amax(fieldbz_x)

            ne_x = utils.bo_file_load(path + utils.nezpath,i,nx,ny)
            curr = calc_current(ne_x,nx,ny,dx,dy,1)
            ne_t[i] = np.amax(ne_x)/dv
            ncr_t[i] = ne_t[i]/ncr
            j_t[i] = curr
        utils.save_tseries(path,filename, nmin, nmax, ez_t, bz_t, ne_t, ncr_t, j_t)
        ez_e = utils.env(ez_t, axis_t)
        bz_e = utils.env(bz_t, axis_t)
        tbounds = [0, 200]    
        ezx.plot(axis_t, ez_e, label = path)
        bzx.plot(axis_t, bz_e, label = path)
        nex.plot(axis_t, ne_t, label = path)
        ncrx.plot(axis_t, ncr_t, label = path)
        maxx.plot(ax1_, max_av, label = path)
        avx.plot(ax1_, av_av, label = path)
        # jx.plot(axisn_t, ntr_t, label = path)
        powx.plot(ax1_, pow_av, label = path)
#        pow1x = powx.twinx()
#        pow1x.plot(ax1_, pow1_av, label = path)
        ezx.set_xlim(tbounds)
        bzx.set_xlim(tbounds)
        nex.set_xlim(tbounds)
        ncrx.set_xlim(tbounds)
        jx.set_xlim(tbounds)
        powx.set_xlim(tbounds)
        maxx.set_xlim(tbounds)
        avx.set_xlim(tbounds)
        power_p.append(ppw)
        if (ppw < 200): 
            amin = -200
            amax = -20
        else:
            amin = 0
            amax = len(ez_t)-1
        ezx_p.append(max(ez_t[amin:amax]))
        bzx_p.append(max(bz_t[amin:amax]))
        nex_p.append(max(ne_t[amin:amax]))
        nemaxx_p.append(max(ne_t))
        ncrx_p.append(max(ncr_t[amin:amax]))
        maxx_p.append(np.average(max_av[amin:amax]))
        avx_p.append(np.average(av_av[amin:amax]))
        jx_max_p.append(max(j_t[amin:amax])*coeffj)
        jx_min_p.append(min(j_t[amin:amax])*coeffj)
        powx_p.append(np.average(pow_av[amin:amax]))
        pow1x_p.append(np.average(pow1_av[amin:amax]))
        el_maxx_p.append(np.average(el_max_av[amin:amax]))
        el_avx_p.append(np.average(el_av_av[amin:amax]))
        el_powx_p.append(np.average(el_pow_av[amin:amax]))
        el_pow1x_p.append(np.average(el_pow1_av[amin:amax]))
        # nex.set_yscale('log')
        '''if (len(ez_t) < 2001):
            amin = -50
            ezx_p.append(np.average(ez_e[amin:]))
            bzx_p.append(np.average(bz_e[amin:]))
            nex_p.append(max(ne_t[amin:]))
            ncrx_p.append(max(ncr_t[amin:]))
            maxx_p.append(max(max_av[amin:]))
            avx_p.append(max(av_av[amin:]))
            jx_max_p.append(max(j_t[amin:])*coeffj)
            jx_min_p.append(min(j_t[amin:])*coeffj)
            powx_p.append(np.average(pow_av[amin:]))
            pow1x_p.append(np.average(pow1_av[amin:]))
            nex.set_yscale('log')
        else:
            amin = 900
            amax = 1200
            ezx_p.append(np.average(ez_e[amin:amax])) 
            bzx_p.append(np.average(bz_e[amin:amax]))
            nex_p.append(max(ne_t[amin:amax]))
            ncrx_p.append(max(ncr_t[amin:amax]))
            maxx_p.append(np.average(max_av[amin:amax]))
            avx_p.append(np.average(av_av[amin:amax]))
            jx_max_p.append(max(ntr_t[amin:amax])*coeffj)
            jx_min_p.append(min(ntr_t[amin:amax])*coeffj)
            powx_p.append(np.average(pow_av[amin:amax]))
            pow1x_p.append(np.average(pow1_av[amin:amax]))'''
            #        nex.set_ylim([1e18, 1e24])
#        print max(ez_t), max(ne_t)
    plt.legend(loc = 'upper right')
#    plt.savefig(picspath + '/' + "ez_t.png")
    plt.show()
    plt.close()
    fig = plt.figure()
    ezx = fig.add_subplot(4,2,1)
    ezx.set_title('Electric field')
    ezx.plot(power_p, ezx_p)
    ezx.set_ylim([0, max(ezx_p)])
    bzx = fig.add_subplot(4,2,2)
    bzx.set_title('Magnetic field')
    bzx.plot(power_p, bzx_p)
    bzx.set_ylim([0, max(bzx_p)])
    ncrx = fig.add_subplot(4,2,4)
    ncrx.set_title('Ne/Ncr')
    ncrx.plot(power_p, ncrx_p)
    ncrx.set_ylim([0, max(ncrx_p)])
    nex = fig.add_subplot(4,2,3)
    nex.set_title('Ne')
    nex.plot(power_p, nex_p)
    nex.plot(power_p, nemaxx_p)
    nex.set_ylim([0, max(nemaxx_p)])
    powx = fig.add_subplot(4,2,6)
    powx.set_title('Power')
    powx.plot(power_p, powx_p)
    powx.set_ylim([0, max(powx_p)])
    pow1x = powx.twinx()
    pow1x.plot(power_p, pow1x_p, 'g')
    pow1x.set_ylim([0, max(pow1x_p)])
    maxx = fig.add_subplot(4,2,7)
    maxx.set_title('Max energy')
    maxx.plot(power_p, maxx_p)
    maxx.set_ylim([0, max(maxx_p)])
    avx = fig.add_subplot(4,2,8)
    avx.plot(power_p, avx_p)
    avx.set_ylim([0, max(avx_p)])
    avx.set_title('Av energy')
    jx = fig.add_subplot(4,2,5)
    jx.set_title('Number of particles')
    jx.plot(power_p, jx_min_p)
    jx.plot(power_p, jx_max_p)
    jx.set_ylim([0, max(jx_max_p)])
    plt.show()
    plt.close()
    f = open('stat_summary_el.txt', 'w')
    for i in range(len(power_p)):
        f.write('%lf %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le\n' % (power_p[i], ezx_p[i], bzx_p[i],
                                                                                   maxx_p[i], avx_p[i], ncrx_p[i],
                                                                                   nex_p[i], powx_p[i], pow1x_p[i],
                                                                                   jx_min_p[i], jx_max_p[i],
                                                                                   el_maxx_p[i], el_avx_p[i],
                                                                                       el_powx_p[i], el_pow1x_p[i], nemaxx_p[i]))
    f.close()
      
if __name__ == '__main__':
    main()
