#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import numpy as np
import os
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

def read_data(filename, norm=0.541579):
    f = open(filename)
    axis = []
    data = []
    theory = [] 
    idx = 0
    
    for line in f:
        tmp = line.split()
        axis.append(float(tmp[0]))
        data.append(float(tmp[1])/norm)
        theory.append(float(tmp[2])/norm)
    return np.array(axis), np.array(data), np.array(theory)
        

def main():
    mp.rcParams.update({'font.size': 12})
    fontsize = 12
    datapath = '/home/evgeny/Dropbox/pinch_thinout/DataForPics'
    picspath = '/home/evgeny/Dropbox/pinch_thinout/'
    fig6path = 'Fig7'

    fig1 = plt.figure(num=None, figsize = (5,3.5))
    ax1 = fig1.add_subplot(1,1,1)
        
    ax1.set_ylim([0, 1.5])
    ax1.set_xlim([0, 60])
    ax1.set_yticks([0, 0.5, 1, 1.5])
    lw = 0.7

    ax_simple, data_simple, theory = read_data(os.path.join(datapath, fig6path, 'Disp_simple.txt'))
    ps, = ax1.plot(ax_simple, data_simple, linewidth = lw, color = 'navy', label = 'simple')
    
    ax_leveling, data_leveling, theory = read_data(os.path.join(datapath, fig6path, 'Disp_leveling.txt'))
    pl, = ax1.plot(ax_leveling, data_leveling, linewidth = lw, color = 'g', label = 'leveling')
    
    ax_globalLev, data_globalLev, theory = read_data(os.path.join(datapath, fig6path, 'Disp_globalLev.txt'))
    pg, = ax1.plot(ax_globalLev, data_globalLev, linewidth = lw, color = 'b', label = 'globalLev')
    
    ax_numberT, data_numberT, theory = read_data(os.path.join(datapath, fig6path, 'Disp_numberT.txt'))
    pn, = ax1.plot(ax_numberT, data_numberT, linewidth = lw, color = 'r', label = 'numberT')
        
    ax_energyT, data_energyT, theory = read_data(os.path.join(datapath, fig6path, 'Disp_energyT.txt'))
    pe, = ax1.plot(ax_energyT, data_energyT, linewidth = lw, color = 'c', label = 'energyT')
    
    ax_merge, data_merge, theory = read_data(os.path.join(datapath, fig6path, 'Disp_merge.txt'))
    pm, = ax1.plot(ax_merge, data_merge, linewidth = lw, color = 'purple', label = 'merge')

    ax_mergeAv, data_mergeAv, theory = read_data(os.path.join(datapath, fig6path, 'Disp_mergeAv.txt'))
    pma, = ax1.plot(ax_mergeAv, data_mergeAv, linewidth = lw, color = 'orange', label = 'mergeAv')

    ax1.annotate('Group 1',
            xy=(49.3, 0.95), xycoords='data',
            xytext=(59, 0.9), textcoords='data',
            arrowprops=dict(arrowstyle="->", linewidth = 0.5),
                 horizontalalignment='right', verticalalignment='top', fontsize = 8)

    ax1.annotate('Group 2',
            xy=(49.3, 0.71), xycoords='data',
            xytext=(59, 0.65), textcoords='data',
            arrowprops=dict(arrowstyle="->", linewidth = 0.5),
                 horizontalalignment='right', verticalalignment='top', fontsize = 8)

    ax1.annotate('Group 3',
            xy=(40, 0.71), xycoords='data',
            xytext=(59, 0.5), textcoords='data',
            arrowprops=dict(arrowstyle="->", linewidth = 0.5),
                 horizontalalignment='right', verticalalignment='top', fontsize = 8)
    ax1.annotate('Group 3',
            xy=(49, 0.26), xycoords='data',
            xytext=(59, 0.5), textcoords='data',
            arrowprops=dict(arrowstyle="->", linewidth = 0.5),
                 horizontalalignment='right', verticalalignment='top', fontsize = 8)
    #ax1.plot([49, 51], [0.26, 0.42], color = 'k', linewidth = 1.2)
    
    pth, = ax1.plot(ax_mergeAv, theory, linewidth = lw, dashes = [5, 2], color = 'k', label = 'theory')
    legend1 = ax1.legend([pth], ['theory'], loc = 'center left', fontsize = fontsize-2, frameon = False)
    plt.legend([ps, pl, pg, pn, pe, pm, pma],
               ['simple', 'leveling', 'globalLev', 'numberT', 'energyT', 'merge', 'mergeAv'],
               loc = 'upper center', fontsize = fontsize-2, frameon = False, ncol = 3)
    #ax1.legend(loc = 'upper center', fontsize = fontsize-2, frameon = False, ncol = 3)
   
    plt.gca().add_artist(legend1)
    ax1.set_xlabel('k')
    ax1.set_ylabel('$\Delta$D, a.u.')
    ax1.text(-10, 1.45, '(b)')
    
    name1 = 'dDall_new'
    
    picname1 = picspath + '/' + name1 + ".png"
    
    #plt.legend(loc = 'upper left', fontsize = 10)
    fig1.tight_layout()
    
    fig1.savefig(picname1, dpi = 256)
    
      
#    plt.show()

if __name__ == '__main__':
    main()
