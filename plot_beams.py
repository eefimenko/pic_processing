#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import utils
import sys
from pylab import *

def write_field2d(a, file):
    print file
    f = open(file, 'w')
    for j in range(len(a[0])):
        for i in range(len(a)):
            f.write('%f ' % a[i][j])
    f.close()

def read_field2d(file,nx,ny,dt,de,mult = 1.):
    f = open(file, 'r')
    array = np.fromfile(f, sep=' ')
    f.close()
    n = 0
    print file, len(array)
    res = np.reshape(array, (nx,ny), order = 'F')
#    jmax = int(10./dt)
    imax = int(1./de)
    jmax = len(res[0])
#    imax = len(res)
    for i in range(imax,len(res)):
        for j in range(jmax):
            n += res[i][j]
    return n

def create_axis(n,step, x0):
    axis = []
#    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def sp_theta(a,level=0,de=1.):
    res = [0]*len(a[0])
    for i in range(len(a)):
        for j in range(len(a[0])):
            if i*de > level:
                res[j] += a[i][j]
    m = max(res)
    for i in range(len(res)):
        res[i] /= m
    return res

def br_theta(a,level,dth,dph):
    res = [0]*len(a[0])
    s = [0]*len(a[0])
    for i in range(len(a)):
        for j in range(len(a[0])):
            for j1 in range(j):
                res[j] += a[i][j1]
                s[j] += math.sin((j1+0.5)*dth)*dth*dph*1e6
    for j in range(len(res)):
        if s[j] > 0:
            res[j] /= s[j]
    return res

def norm(a):
    m = max2d(a)
    for i in range(len(a)):
        for j in range(len(a[0])):
            a[i][j] /= m
            a[i][j] += 1e-10
    return a

def sp_energy(a):
    res = [0]*len(a)
    for i in range(len(a)):
        for j in range(len(a[0])):
            res[i] += a[i][j]
    m = max(res)
    for i in range(len(res)):
        res[i] /= m
    return res

def find_max_nz(a):
    n = len(a)
    for i in range(1,len(a)):
        if a[-i] > 1e-8:
            n = len(a)-i
            break
    return n

def smooth(a,factor):
    n = len(a)
    n1 = n/factor
    if n1%factor != 0:
        print 'Error'
    res = [0] * n1
    for i in range(n1):
        for j in range(factor):
            res[i] += a[i*factor + j]/factor
    return res

def find_angle_max(a,dt):
    res = [0]*len(a)
    for i in range(len(a)):
        maxj = 0
        maxn = 0.
        for j in range(len(a[0])):
            if a[i][j] > maxn:
                maxj = j
                maxn = a[i][j]
#                print 'here', a[i][j], maxn, maxj, j
        res[i] = maxj*dt
#        print i, maxj, res[j]
#    print res
    return res

def smooth_2d(a,factor_ne,factor_nt):
    ne = len(a)
    nt = len(a[0])
    nt1 = nt/factor_nt
    ne1 = ne/factor_ne
    print ne, nt, ne1, nt1
    res = np.zeros((ne1,nt1))

    for j in range(ne1):
        for i in range(nt1):
            for k in range(factor_nt):
                for m in range(factor_ne):
                    res[j][i] += a[j*factor_ne+m][i*factor_nt+k]/factor_ne/factor_nt + 1e-15
    
    return res

def add_sp(res,a):
    num = 0
    for i in range(len(a)):
        num += sum(a[i])/(i+0.425)

    for i in range(len(a)):
        for j in range(len(a[0])):
            res[i][j] += a[i][j]/num
    return res

def max2d(array):
    return max([max(x) for x in array])

def find_max_energy(array,step):
    n = len(array)
    full = 0
    summ = 0
    imax = 0
    max_en = 0
    for i in range(n):
        full += array[i]*step

    for i in range(1,n):
        summ = summ + array[-i]*step
        if summ > 0.01*full:
            imax = i
            break
    if imax == 0:
        max_en = 0
    else:
        max_en = (n-imax) * step

    return max_en

def main():
    path = '.'
    
    picspath = 'pics'
    elpath = '/statdata/el/EnAngSp/'
    phpath = '/statdata/ph/EnAngSp/'
    pospath = '/statdata/pos/EnAngSp/'

    num = len(sys.argv)
  
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(path + elpath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(path + elpath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax-nmin) + ' files'
   
    config = utils.get_config(path + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    T = 2 * math.pi/omega*1e15
    step = x0*y0*1e15/T
    nT = int(1./step)
    print nT
    ev = float(config['eV'])
    ne = int(config['QEDstatistics.OutputN_E'])
    nt = int(config['QEDstatistics.OutputN_theta'])
    tmax = float(config['QEDstatistics.ThetaMax'])*180./3.14159
    tmin = float(config['QEDstatistics.ThetaMin'])*180./3.14159
    emax = float(config['QEDstatistics.Emax'])/ev*1e-9
    emin = float(config['QEDstatistics.Emin'])/ev*1e-9
    peakpower = float(config['PeakPower'])*1e-7*1e-15
    const_a_p = 7.81441e-9
    a0 = const_a_p * math.sqrt(peakpower*1e22)
    de = (emax - emin)/ne
    dt = (tmax - tmin)/nt
    dth = dt * 3.14159/180. 
    dph = 2*3.14159
    factor_nt = 100 #for mom
    factor_ne = 1 #for mom
#    factor_nt = 3 #for angular
#    factor_ne = 2 #for angular
    nt1 = nt/factor_nt
    ne1 = ne/factor_ne
    axe = create_axis(ne, de, 0)
    axt = create_axis(nt, dt, tmin)
    axe_f = create_axis(ne/factor_ne, de*factor_ne, 0)
    axe_f2 = create_axis(ne/factor_ne, de*factor_ne, 0)

    axt_f = create_axis(nt/factor_nt, dt*factor_nt, tmin)
    omega = float(config['Omega'])
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))

    print num, T
    el_t = []
    ph_t = []
    pos_t = []
    for n in range(nmin,nmax):
        picname_el = picspath + '/' + "dne%06d.png" % (n,)
        picname_ph = picspath + '/' + "dnp%06d.png" % (n,)
        picname_pos = picspath + '/' + "dnp%06d.png" % (n,)
        elname = path + elpath + '%.4f.txt' % (n)
        phname = path + phpath + '%.4f.txt' % (n)
        posname = path + pospath + '%.4f.txt' % (n)
        
        el = read_field2d(elname,ne,nt,dt,de)
        ph = read_field2d(phname,ne,nt,dt,de)
        pos = read_field2d(posname,ne,nt,dt,de)
        el_t.append(el)
        ph_t.append(ph)
        pos_t.append(pos)

    fig = plt.figure(num=None)
    axel = fig.add_subplot(1,1,1)
    axel.plot(el_t, 'r', label = 'el')
    axel.plot(ph_t, 'b', label = 'ph')
    axel.plot(pos_t, 'g', label = 'pos')
    plt.legend(loc = 'upper left')
    axel.set_yscale('log')

    f = open('bunches.dat', 'w')
    for i in range(len(el_t)):
        t = i*step
        f.write('%f %e %e %e\n' % (t,el_t[i], ph_t[i], pos_t[i]))
    f.close()
    plt.show()

if __name__ == '__main__':
    main()
