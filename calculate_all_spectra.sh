type="edipole"
size="3mkm"
dirs="nc_0.01"
#dirs="nc_0.001  nc_0.01  nc_0.1  nc_1  nc_10  nc_100  nc_1000  nc_30  nc_50"
for t in $type
do
    for s in $size
    do
        for d in $dirs
        do
	    echo $t/$s/$d
	    cd $t/$s/$d
	    qe_efficiency.py
	    cd -

        done
    done
done
