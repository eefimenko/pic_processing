#!/usr/bin/python
import gzip
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import math
import sys
import utils

def read_energy_file(filename, dt = 1.):
    f = open(filename, 'r')
    axis = []
    energy = []
    for line in f:
        tmp = line.split()
        axis.append(float(tmp[0])*dt*1e15)
        energy.append(float(tmp[1]))
    return axis, energy
    
def main():
    fig = plt.figure(num=None)
    mp.rcParams.update({'font.size': 12})
#    ax0 = fig.add_subplot(2,1,1)
    ax1 = fig.add_subplot(1,1,1)
    savename = 'energy.png'
    num = len(sys.argv)-1
    for i in range(num):
        path = sys.argv[i+1]
        per = path + '/' + 'FieldEnergy.txt'
        config = utils.get_config(path + "/ParsedInput.txt")
#        if not 'f0' in path:
#            rs = int(config.get('FdtdNsg.SpaceFactor',0))
#            rt = int(config.get('FdtdNsg.TimeFactor',0))
#        else:
#            rs = 0
#            rt = 0
        wl = float(config['Wavelength'])
        dx = float(config['Step_X'])
        n = int(wl/dx)
        dt = float(config['TimeStep']) 
        axis, en_per = read_energy_file(per, dt)
#        lbl = 's=%d t=%d n=%d'%(rs, rt, n)
#        pe, = ax0.plot(axis, en_per, label = lbl)
        pe, = ax1.plot(axis, en_per, label = path)
#        ax0.legend(loc='upper right')
        ax1.legend(loc='upper left')
#        ax1.set_yscale('log')
	ax1.set_xlabel('t, fs')	
	ax1.set_ylabel('Energy')	
#        ax1.set_ylim([1e-28, 1e-18])
#        ax1.set_xlim([0, 4])
 
    plt.show()
#    plt.savefig(savename)
#    plt.close()


if __name__ == '__main__':
    main()
