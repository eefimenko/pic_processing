#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def read_field(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)/norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])

def main():
    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
    ex_xpath = 'data/Ex_x'
    ex_ypath = 'data/Ex_y'
    ex_zpath = 'data/Ex_z'
    ey_xpath = 'data/Ey_x'
    ey_ypath = 'data/Ey_y'
    ey_zpath = 'data/Ey_z'
    ez_xpath = 'data/Ez_x'
    ez_ypath = 'data/Ez_y'
    ez_zpath = 'data/Ez_z'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    bx_xpath = 'data/Bx_x'
    bx_ypath = 'data/Bx_y'
    bx_zpath = 'data/Bx_z'
    by_xpath = 'data/By_x'
    by_ypath = 'data/By_y'
    by_zpath = 'data/By_z'
    bz_xpath = 'data/Bz_x'
    bz_ypath = 'data/Bz_y'
    bz_zpath = 'data/Bz_z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nixpath = 'data/Proton2Dx'
    niypath = 'data/Proton2Dy'
    nizpath = 'data/Proton2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nphxpath = 'data/Photon2Dx'
    nphypath = 'data/Photon2Dy'
    nphzpath = 'data/Photon2Dz'
    picspath = 'pics'
    config = utils.get_config("ParsedInput.txt")
    wl = float(config['Wavelength'])
    Xmax = float(config['X_Max'])/wl #mkm to wavelength
    Xmin = float(config['X_Min'])/wl #mkm to wavelength
    Ymax = float(config['Y_Max'])/wl #mkm to wavelength
    Ymin = float(config['Y_Min'])/wl #mkm to wavelength

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    delta = 1
    rsize = 0.25
    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(expath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(expath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'

    dv = dx*dy*dz
    axis1 = create_axis(nx, (Xmax-Xmin)/nx, Xmin)
    ez_t = []
    for i in range(nmin, nmax, delta):
        fig = plt.figure(num=None, figsize=(32, 24), dpi=256)
        
        picname = picspath + '/' + "fcz%06d.png" % (i,)
        print picname
        a2x = fig.add_subplot(3,4,1)
        ax_x = fig.add_subplot(3,4,2)
        ay_x = fig.add_subplot(3,4,3)
        az_x = fig.add_subplot(3,4,4)
        name = ezpath + '/' + "%06d.txt" % (i,)
        fielde2x = read_field(name,nx,ny)
        m = max2d(fielde2x)

        surf = a2x.imshow(fielde2x, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Reds')
        name = ex_zpath + '/' + "%06d.txt" % (i,)
        fieldex_x = read_field(name,nx,ny)
        surf = ax_x.imshow(fieldex_x, extent=[Xmin, Xmax, Ymin, Ymax], vmin = -m, vmax = m, cmap='Spectral')
        name = ey_zpath + '/' + "%06d.txt" % (i,)
        fieldey_x = read_field(name,nx,ny)
        surf = ay_x.imshow(fieldey_x, extent=[Xmin, Xmax, Ymin, Ymax], vmin = -m, vmax = m, cmap='Spectral')
        name = ez_zpath + '/' + "%06d.txt" % (i,)
        fieldez_x = read_field(name,nx,ny)
        surf = az_x.imshow(fieldez_x, extent=[Xmin, Xmax, Ymin, Ymax], vmin = -m, vmax = m, cmap='Spectral')
        ez = fieldez_x[nx/2][ny/2]
        ez_t.append(ez)
        a2x.set_xlim([-rsize, rsize])
        a2x.set_ylim([-rsize, rsize])
        ax_x.set_xlim([-rsize, rsize])
        ax_x.set_ylim([-rsize, rsize])
        ay_x.set_xlim([-rsize, rsize])
        ay_x.set_ylim([-rsize, rsize])
        az_x.set_xlim([-rsize, rsize])
        az_x.set_ylim([-rsize, rsize])
        a2x = fig.add_subplot(3,4,5)
        ax_x = fig.add_subplot(3,4,6)
        ay_x = fig.add_subplot(3,4,7)
        az_x = fig.add_subplot(3,4,8)
        name = bzpath + '/' + "%06d.txt" % (i,)
        fielde2x = read_field(name,nx,ny)
        m = max2d(fielde2x)
        surf = a2x.imshow(fielde2x, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Blues')
        name = bx_zpath + '/' + "%06d.txt" % (i,)
        fieldex_x = read_field(name,nx,ny)
        surf = ax_x.imshow(fieldex_x, extent=[Xmin, Xmax, Ymin, Ymax], vmin = -m, vmax = m, cmap='Spectral')
        name = by_zpath + '/' + "%06d.txt" % (i,)
        fieldey_x = read_field(name,nx,ny)
        surf = ay_x.imshow(fieldey_x, extent=[Xmin, Xmax, Ymin, Ymax], vmin = -m, vmax = m, cmap='Spectral')
        name = bz_zpath + '/' + "%06d.txt" % (i,)
        fieldez_x = read_field(name,nx,ny)
        surf = az_x.imshow(fieldez_x, extent=[Xmin, Xmax, Ymin, Ymax], vmin = -m, vmax = m, cmap='Spectral')
        a2x.set_xlim([-rsize, rsize])
        a2x.set_ylim([-rsize, rsize])
        ax_x.set_xlim([-rsize, rsize])
        ax_x.set_ylim([-rsize, rsize])
        ay_x.set_xlim([-rsize, rsize])
        ay_x.set_ylim([-rsize, rsize])
        az_x.set_xlim([-rsize, rsize])
        az_x.set_ylim([-rsize, rsize])
        name = nezpath + '/' + "%06d.txt" % (i,)
        el_x = fig.add_subplot(3,4,9)
        pos_x = fig.add_subplot(3,4,10)
        fieldel_x = read_field(name,nx,ny)
        m = max2d(fieldel_x)
        surf = el_x.imshow(fieldel_x, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Greens', vmin = m*1e-3, vmax = m, norm=clr.LogNorm())
        el_x.set_xlim([-rsize, rsize])
        el_x.set_ylim([-rsize, rsize])
        name = npzpath + '/' + "%06d.txt" % (i,)
        fieldpos_x = read_field(name,nx,ny)
        m = max2d(fieldpos_x)
        surf = pos_x.imshow(fieldpos_x, extent=[Xmin, Xmax, Ymin, Ymax], cmap='bone_r', vmin = m*1e-3, vmax = m, norm=clr.LogNorm())
        pos_x.set_xlim([-rsize, rsize])
        pos_x.set_ylim([-rsize, rsize]) 
        plt.savefig(picname)
        plt.close()
    
#        plt.show()
#    fig = plt.figure()
#    ezx = fig.add_subplot(1,1,1)
#    ezx.plot(ez_t)
#    ezx.set_xlim([0,20])
#    plt.savefig(picspath + '/' + "ez_t.png")
#    plt.close()
if __name__ == '__main__':
    main()
