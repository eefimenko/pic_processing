#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys
import gc

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])

def norm(array):
    m = max(array)
    a = [0]*len(array)
    for i in range(len(array)):
        a[i] = array[i]/m
    return a

def create_subplot(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, text, text1, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
#    name = path + '/' + "%06d.txt" % (i,)
    field = utils.bo_file_load(path,i,nx,ny, verbose=1)
    if field == None:
        return
#    maxe = max([max(row) for row in field])
#    mine = min([min(row) for row in field])
    maxe = np.amax(field)
    mine = np.amin(field)
    print path, maxe, mine
    ratio = 1e-3
    if maxe == 0:
        maxe = 1
    if maxe == mine:
        v_min_ = maxe
        v_max_ = 1e9*maxe
    else:
        v_max_ = maxe
        v_min_ = maxe*ratio
   
    ticks = [-2,-1,0,1,2]
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = v_min_, vmax = v_max_, norm=clr.LogNorm(), aspect = 'auto', origin = 'lower')
#    ax.text(-2.5, 2.5, text)
    ax.set_title(text + "%.1le" % (maxe*mult)) 
#    ax.text(1.6, 1.6, text1, fontsize=30)
#    ax.set_xlim([-0.5,0.5])
#    ax.set_ylim([-0.5,0.5])
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
#    plt.xticks(ticks)
#    plt.yticks(ticks)
#    plt.colorbar(surf,  orientation  = 'vertical', ticks = [v_min, v_max])
    return surf

def create_subplot_f(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, text, text1, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
#    name = path + '/' + "%06d.txt" % (i,)
    field = utils.bo_file_load(path,i,nx,ny, verbose=1)
    if field == None:
        print 'Error! Empty file', path
        return
#    maxe = max([max(row) for row in field])
#    v_max_ = max2d(field)
#    print v_max_
    ratio = 1e-3
#    ticks = [-2,-1,0,1,2]
#    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = v_max_*ratio, vmax = v_max_)
    maxe = np.amax(field)
    mine = np.amin(field)
    print path, maxe, mine
    ratio = 1e-6
    if maxe == 0:
        maxe = 1
    if maxe == mine:
        v_min_ = maxe
        v_max_ = 1e9*maxe
    else:
        if maxe > 0 and mine < 0:
            v_max_ = maxe
            v_min_ = maxe*ratio
        else:
            m = max([abs(maxe), abs(mine)])
            v_max_ = m
            v_min_ = -m
  
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = v_min_, vmax = v_max_, aspect = 'auto')
    ax.set_title(text + "%.1le"%(maxe))
#    ax.text(1.6, 1.6, text1, fontsize=30)
    ax.set_xlim([-0.5,0.5])
    ax.set_ylim([-0.5,0.5])
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
#    plt.xticks(ticks)
#    plt.yticks(ticks)
#    plt.colorbar(surf,  orientation  = 'vertical', ticks = [v_min, v_max])
    return surf

def main():
    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nixpath = 'data/Proton2Dx'
    niypath = 'data/Proton2Dy'
    nizpath = 'data/Proton2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nphxpath = 'data/Photon2Dx'
    nphypath = 'data/Photon2Dy'
    nphzpath = 'data/Photon2Dz'
    picspath = 'pics'

    n = len(sys.argv) - 3
    nmin = int(sys.argv[1])
    nmax = int(sys.argv[2])
    delta = 1
    for i in range(nmin, nmax, delta):
        fig = plt.figure(num=None, figsize=(20, 5*n), dpi=256)
        picname = picspath + '/' + "cmp_pinch2d_rg_%06d.png" % (i,)
               
        print picname
        for j in range(n):
            path = sys.argv[j+3] + '/'
            config = utils.get_config(path + "/ParsedInput.txt")
            wl = float(config['Wavelength'])
            
            rf = float(config['resize'])
            rr = float(config.get('rescale', 1.))
            print rr, rf, i, i*rf*rr
            iteration = int(i*rf*rr)
            Xmax = float(config['X_Max'])*1e4 #mkm to wavelength
            Xmin = float(config['X_Min'])*1e4 #mkm to wavelength
            Ymax = float(config['Y_Max'])*1e4 #mkm to wavelength
            Ymin = float(config['Y_Min'])*1e4 #mkm to wavelength
            Zmax = float(config['Z_Max'])*1e4 #mkm to wavelength
            Zmin = float(config['Z_Min'])*1e4 #mkm to wavelength
            if 'r' in path:
                Xmax /= rr
                Xmin /= rr
                Ymax /= rr
                Ymin /= rr
                Zmax /= rr
                Zmin /= rr
            print Xmax, Xmin
            
            nx = int(config['MatrixSize_X'])
            ny = int(config['MatrixSize_Y'])
            nz = int(config['MatrixSize_Z'])
            rmin = float(config['ElGammaR.SetBounds_0'])*1e4
            rmax = float(config['ElGammaR.SetBounds_1'])*1e4
            gmin = float(config['ElGammaR.SetBounds_2'])
            gmax = float(config['ElGammaR.SetBounds_3']) 
            nr = int(config['ElGammaR.SetMatrixSize_0'])
            ng = int(config['ElGammaR.SetMatrixSize_1'])
            print 'Nx = ' + str(nx) 
            print 'Ny = ' + str(ny)
            print 'Nz = ' + str(nz)

            dx = (Xmax-Xmin)/nx
            dy = (Ymax-Ymin)/ny
            dz = (Zmax-Zmin)/nz
            print dx

            if 'r' not in path:
                dv = 2.*dx*dy*dz*rf*1e-12
            else:
                dv = 2.*dx*dy*dz*1e-12
                
            fmin = 0
            fmax = 0
            cmin = 0
            cmax = 0
            
            s = create_subplot_f(fig,iteration,path+eypath,nx,ny,n,5,5*j+1, Xmin, Xmax, Ymin, Ymax, fmin, fmax, '', '$z/\lambda$', 'Reds', u'Electric field', 'a')
            s = create_subplot_f(fig,iteration,path+bypath,nx,ny,n,5,5*j+2, Xmin, Xmax, Ymin, Ymax, fmin, fmax, '', '','Reds', u'Magnetic field', 'b')
            s = create_subplot(fig,iteration,path+neypath,nx,ny,n,5,5*j+3, Xmin, Xmax, Ymin, Ymax, cmin, cmax, '', '','Greens', u'Electrons', 'c', 1./dv)
            s = create_subplot(fig,iteration,path+utils.elgammarpath,nr,ng,n,5,5*j+4, gmin, gmax, rmin, rmax, cmin, cmax, '', '','hot', u'Positrons', 'd', 1./dv)
            s = create_subplot_f(fig,iteration,path+utils.jz_zpath,nx,ny,n,5,5*j+5, Xmin, Xmax, Ymin, Ymax, cmin, cmax, '', '','bwr', u'Jz_z', 'e', 1)
            gc.collect()
           
        plt.tight_layout() 
        plt.savefig(picname)
        plt.close()
    
if __name__ == '__main__':
    main()
