#!/usr/bin/python
import math
import matplotlib.pyplot as plt
import numpy as np

def main():
    N = 100
    dph1 = 0.55 * math.pi
    dph2 = 0.4 * math.pi
    dph3 = 0.6 * math.pi
    dv1 = dph1/ (2 * math.pi * 13.375)
    dv2 = dph2/ (2 * math.pi * 14.125)
    dv3 = dph2/ (2 * math.pi * 13.75)
    print(1 - dv1, 1 - dv3, 1 - dv2)
    amp = np.zeros(N)
    ax = np.linspace(0, 2. * math.pi, 100)
    phase = np.linspace(0, 2. * math.pi, N)
    s1 = -np.sin(ax)
    for i, ph in enumerate(phase):
        s2 = np.sin(ax + ph)
        amp[i] = np.amax(s1 + s2)

    fig = plt.figure(figsize = (7,5))
    ax = fig.add_subplot(1,1,1)
    ax.plot(phase, amp)
    ax.axvline(x = dph1, color = 'g')
    ax.axvline(x = dph2, color = 'r')
    plt.show()

    
    

if __name__ == '__main__':
    main()
