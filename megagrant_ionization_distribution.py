#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
from mpl_toolkits.axes_grid1 import make_axes_locatable
import os
import pickle
from tqdm import tqdm
import numpy as np

Wavelength = 0.9e-4 # mkm
k = 2. * np.pi/Wavelength
power = 10 # PW
w_a = 4.16e16

LightVelocity = 3e10
C2 = LightVelocity*LightVelocity

L=1e-4

#nr = 350 # O
#nr = 500 # N


names = ['N']
if names[0] == 'H':
    potentials = [13.6]
    nr = 40000
    nt=2000000 # H
elif names[0] == 'O':
    potentials = [871.12]
    nr = 350
    nt = 50000 # O
else:
    potentials = [666.83]
    nr = 500
    nt = 50000 # N
    
R0 = nr*L
tau = 5
inv_tau = 1./tau
tau2 = (tau*tau+2)
pi_tau = np.pi*tau

Omega = k*LightVelocity 
PeakPower = 10 *1e22 # erg

#nt = 50000 # N,O

#potentials = [871.12] #O
#names = ['O']
#potentials = [666.83] #O
#names = ['N']

#potentials = [666.83, 871.12]
#names = ['N', 'O']



def arg1(R, t):
    return Omega*(t*LightVelocity + R - R0)/LightVelocity

def arg2(R, t):
    return Omega*(t*LightVelocity - R - R0)/LightVelocity

def Block(x, xmin, xmax):
    res = np.logical_and(np.greater_equal(x, xmin), np.less_equal(x, xmax))
    return res.astype(int)
#:
#        return 0.
#    else:
#        return 1.

def a1(R,t):
    return Block(arg1(R,t), 0, np.pi*tau)


def a2(R,t):
    return Block(arg2(R,t), 0, np.pi*tau)


def Pulse(t):
    s_ = np.sin(t*inv_tau)
    return np.sin(t)*s_*s_

Amp = np.sqrt(3*LightVelocity*PeakPower)*LightVelocity/(Omega*Omega)
Amp1 = Amp*Omega
Amp2 = Amp*Omega*Omega*(inv_tau*inv_tau)

def d_m(R,t):
    return Amp*(-a1(R,t) * Pulse(arg1(R,t)) + a2(R,t) * Pulse(arg2(R,t)))

#def d_m_t(R,t):
#    return Amp*Omega*(
#        (np.sin(arg2(R,t))*np.sin(2*arg2(R,t)/tau)/tau + np.cos(arg2(R,t))*np.square(np.sin(arg2(R,t)/tau)))*a2(R,t) -
#        (np.sin(arg1(R,t))*np.sin(2*arg1(R,t)/tau)/tau + np.cos(arg1(R,t))*np.square(np.sin(arg1(R,t)/tau)))*a1(R,t))

def d_m_tt(R,t):
    a2_ = arg2(R,t)
    a1_ = arg1(R,t)
    return -Amp2*(
        (np.sin(a2_)*((tau*tau+2)*np.square(np.sin(a2_*inv_tau))-2*np.square(np.cos(a2_*inv_tau)))-2*tau*np.cos(a2_)*np.sin(2*a2_*inv_tau))*a2(R,t) -
        (np.sin(a1_)*((tau*tau+2)*np.square(np.sin(a1_*inv_tau))-2*np.square(np.cos(a1_*inv_tau)))-2*tau*np.cos(a1_)*np.sin(2*a1_*inv_tau))*a1(R,t))

#def d_pl(R,t):
#    return Amp*(a1(R,t) * Pulse(a1_) + a2(R,t) * Pulse(a2_))

def d_pl_t(R,t):
    a2_ = arg2(R,t)
    a1_ = arg1(R,t)
    
    return Amp1*(
        (np.sin(a2_)*np.sin(2*a2_*inv_tau)*inv_tau + np.cos(a2_)*np.square(np.sin(a2_*inv_tau)))*a2(R,t) +
        (np.sin(a1_)*np.sin(2*a1_*inv_tau)*inv_tau + np.cos(a1_)*np.square(np.sin(a1_*inv_tau)))*a1(R,t))

def dfuncs(R,t):
    a2_ = arg2(R,t)
    a1_ = arg1(R,t)

    a1_tau = a1_*inv_tau
    a2_tau = a2_*inv_tau
    
    ba1_ = Block(a1_, 0, pi_tau)
    ba2_ = Block(a2_, 0, pi_tau)

    sa1_ = np.sin(a1_)
    sa2_ = np.sin(a2_)

    ca1_ = np.cos(a1_)
    ca2_ = np.cos(a2_)

    s2a1_ = np.sin(2.*a1_tau)
    s2a2_ = np.sin(2.*a2_tau)
    
    sq_sa1_ = np.square(np.sin(a1_tau))
    sq_sa2_ = np.square(np.sin(a2_tau))

    sq_ca1_ = 1. - sq_sa1_#np.square(np.cos(a1_tau))
    sq_ca2_ = 1. - sq_sa2_#np.square(np.cos(a2_tau))

    d_m_tt_ = -Amp2 * (
        (sa2_ * (tau2 * sq_sa2_ - 2 * sq_ca2_) - 2 * tau * ca2_ * s2a2_) * ba2_ -
        (sa1_ * (tau2 * sq_sa1_ - 2 * sq_ca1_) - 2 * tau * ca1_ * s2a1_) * ba1_
    )
    
    d_pl_t_= Amp1 * (
        (sa2_ * s2a2_ * inv_tau + ca2_ * sq_sa2_) * ba2_ +
        (sa1_ * s2a1_ * inv_tau + ca1_ * sq_sa1_) * ba1_
    )
    
    d_m_ = Amp * (
        ba2_ * sa2_ * sq_sa2_ -
        ba1_ * sa1_ * sq_sa1_
    )

    return d_m_tt_, d_pl_t_, d_m_
#def d_pl_tt(R,t):
#    return -Amp*Omega*Omega/(tau*tau)*(
#        (np.sin(a2_)*((tau*tau+2)*np.square(np.sin(a2_*inv_tau))-2*np.square(np.cos(a2_*inv_tau)))-2*tau*np.cos(a2_)*np.sin(2*a2_*inv_tau))*a2(R,t) +
#        (np.sin(a1_)*((tau*tau+2)*np.square(np.sin(a1_*inv_tau))-2*np.square(np.cos(a1_*inv_tau)))-2*tau*np.cos(a1_)*np.sin(2*a1_*inv_tau))*a1(R,t))


def Ex_dw(x,y,R_,t):
    return d_m_tt(R_, t)/(np.square(LightVelocity)*R_) * x * y/(R_*R_) + d_pl_t(R_,t)/(LightVelocity*(R_*R_))*3*x*y/(R_*R_) + d_m(R_, t)/((R_*R_*R_)) * 3*x*y/(R_*R_)

def Ey_dw(x,y,R_,t):
    return -d_m_tt(R_, t)/(np.square(LightVelocity)*R_) * x * x/(R_*R_) + d_pl_t(R_,t)/(LightVelocity*(R_*R_))*(3*y*y/(R_*R_) - 1.) + d_m(R_, t)/(R_*R_*R_) * (3*y*y/(R_*R_) - 1)

def Exy(x,y,R_,t):
    R_2 = R_*R_
    R_3 = R_2*R_
    d_m_tt_, d_pl_t_, d_m_ =  dfuncs(R_,t)
    d_m_tt_ = d_m_tt_ / C2 / R_
    d_pl_t_ = d_pl_t_ / R_2 / LightVelocity
    d_m_ = d_m_ / R_3
    #d_m_tt_ = d_m_tt(R_, t)/(C2*R_)
    #d_pl_t_ = d_pl_t(R_,t)/R_2/(LightVelocity)
    #d_m_ = d_m(R_, t)/R_3
    
    xy_R_2 = x * y / R_2
    xx_R_2 = x * x / R_2
    yy_R_2 = 3 * y * y / R_2 - 1
    
    ex = d_m_tt_ * xy_R_2 + d_pl_t_ * 3. * xy_R_2 + d_m_ * 3. * xy_R_2
    ey = -d_m_tt_ * xx_R_2 + d_pl_t_ * yy_R_2 + d_m_ * yy_R_2
    
    return np.sqrt(ex*ex + ey*ey)


def numba_field(x, z, array):
    for t_ in range(nt):
        if t_%100 == 0:
            print(t_)
        for i in range(len(x)):
            x_ = x[i]
            y = z
            R_ = np.sqrt(x[i]*x[i] + z*z)
            t = 2*R0/LightVelocity/nt*t_
            R_2 = R_*R_
            R_3 = R_*R_*R_
            d_m_tt_ = d_m_tt(R_, t)/(C2*R_)
            d_pl_t_ = d_pl_t(R_,t)/(LightVelocity*R_2)
            d_m_ = d_m(R_, t)/R_3
            
            xy_R_2 = x_ * y/R_2
            xx_R_2 = x_ * x_/R_2
            yy_R_2 = 3 * y * y/R_2 - 1
            
            ex = d_m_tt_ * xy_R_2 + d_pl_t_ * 3. * xy_R_2 + d_m_ * 3. * xy_R_2
            ey = -d_m_tt_ * xx_R_2 + d_pl_t_ * yy_R_2 + d_m_ * yy_R_2
    
            field_ = np.sqrt(ex*ex + ey*ey)
            #ex = Ex_dw(x[i],z,np.sqrt(x[i]*x[i] + z*z),2*R0/LightVelocity/nt*t_)
            #ez = Ey_dw(x[i],z,np.sqrt(x[i]*x[i] + z*z),2*R0/LightVelocity/nt*t_)
            #field_ = Exy(x[i],z,np.sqrt(x[i]*x[i] + z*z),2*R0/LightVelocity/nt*t_)
            array[i] = np.maximum(field_, array[i])
            #array[i] = np.maximum(np.sqrt(ex*ex+ez*ez), array[i])
    return array


def field(x, z, array):
    dt = 2.*R0/LightVelocity/nt
    for t_ in tqdm(range(nt)):
        #if t_%100 == 0:
        #    print(t_)
        for i in range(len(x)):
            #ex = Ex_dw(x[i],z,np.sqrt(x[i]*x[i] + z*z),2*R0/LightVelocity/nt*t_)
            #ez = Ey_dw(x[i],z,np.sqrt(x[i]*x[i] + z*z),2*R0/LightVelocity/nt*t_)
            field_ = Exy(x[i], z, np.sqrt(x[i]*x[i] + z*z), dt * t_)
            array[i] = np.maximum(field_, array[i])
            #array[i] = np.maximum(np.sqrt(ex*ex+ez*ez), array[i])
    return array

def prob(E, I):
    E = np.abs(E)
    if E < 1e-5:
        return 0
    return 4. * w_a * math.pow(I/13.6, 2.5)/E * math.exp(-2./3.*math.pow(I/13.6, 1.5)/E)

def ionization(amp, time, pulse, I):
    N = 0
    dt = time[1] - time[0]
    for i in range(len(time)):
        N += dt*prob(pulse[i]*amp, I)*(1 - N)
    return N

def boundary_field(time, pulse, I):
    flow = 0
    fhigh = 100
    level = 0.99
    degree = 0.
    while not np.allclose(degree, level, 1e-10):
        amp = (flow + fhigh)/2.
        degree = ionization(amp, time, pulse, I)
        if degree > level:
            fhigh = amp
        else:
            flow = amp
    return amp

def pulse(time):
    w = 2.*math.pi/(0.9e-4)*3e10
    tau=55
    return np.sin(w*time)*np.sin(w*time/tau)*np.sin(w*time/tau)

def main():
    nx = 64+1
    nz = 64+1
    xmin = 0#-R0/np.sqrt(2.)
    xmax = R0/np.sqrt(2.)
    zmin = 0#-R0/np.sqrt(2.)
    zmax = R0/np.sqrt(2.)
    E_a = 5.14e11/3e4 #V/m -> CGSE
    
    amp = 3e11*np.sqrt(power/10.)
    x = np.linspace(xmin, xmax, num=nx)
    z = np.linspace(zmin, zmax, num=nz)
     
    electric_field = np.zeros((nx,nz))
    degree = np.zeros((nx,nz))

    filename = 'field_%d_%d.pkl' % (nr,nt)
    if os.path.exists(filename):
       with open(filename, 'rb') as f:
            electric_field = pickle.load(f)
    else:
        electric_field = field(x, z, electric_field)
        with open(filename, 'wb') as f:
            pickle.dump(electric_field, f)
    electric_field /= E_a
    electric_field = np.transpose(electric_field)

    
    
    n_time = 10000
    time = np.linspace(0, 78e-15, num=10000)
    pulse_ = pulse(time)
    #fig = plt.figure()
    #ax1 = fig.add_subplot(1,1,1)
    #ax1.plot(time*1e15, pulse_)
    #plt.show()
    #plt.close()
    fontsize = 18
    mp.rcParams.update({'font.size': fontsize})
    
    boundaries = [boundary_field(time, pulse_, I) for I in potentials]
    print(boundaries)
    for boundary, name in zip(boundaries, names):    
        fig = plt.figure(figsize = (6,5))
        ax1 = fig.add_subplot(1,1,1)
        if name == 'H':
            mult = 1e1
            units = 'mm'
        else:
            mult = 1e4
            units = '$\mu m$'
        if name == 'H':
            label = '(a)'
        elif name == 'N':
            label = '(b)'
        else:
            label = '(c)'
        #surf = ax1.imshow(electric_field, cmap='w', extent=[xmin,xmax,zmin,zmax], origin='lower')
        cs = ax1.contour(x*mult,z*mult,electric_field, [boundary/math.sqrt(10), boundary, boundary*math.sqrt(10)] , labels = [100, 10, 1], colors=['b', 'g', 'k'])
        fmt = {}
        strs = ['100', '10', '1']
        for l, s in zip(cs.levels, strs):
            fmt[l] = s
        plt.clabel(cs, fmt = fmt, colors = 'k', fontsize=14)
        #divider = make_axes_locatable(ax1)
        #cax = divider.append_axes("right", size="5%", pad=0.05)
        #cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)
        ax1.set_ylabel('z, %s' % units)
        ax1.set_xlabel('r, %s' % units)
        ax1.text((ax1.get_xlim()[0]-ax1.get_xlim()[1])/5, ax1.get_ylim()[1], label)
        ax1.text(0.9*ax1.get_xlim()[1], 0.9*ax1.get_ylim()[1], name, bbox = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9))
        figname = os.path.join('/home/evgeny/Dropbox', 'ionization_%s.png' % name)
        print(figname)
        plt.tight_layout()
        plt.savefig(figname, dpi=300)
#        plt.show()
    
if __name__ == '__main__':
    main()
