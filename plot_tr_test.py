#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.cm as cm
import matplotlib.colors as colors
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def read_traj(file):
    f = open(file, 'r')
    t_ = []
    x_ = []
    y_ = []
    z_ = []
    px_ = []
    py_ = []
    pz_ = []
    p0 = 2.73e-17
    for line in f:
        tmp = line.split()
        t = float(tmp[0])
        x = float(tmp[1])
        y = float(tmp[2])
        z = float(tmp[3])
        px = float(tmp[4])/p0
        py = float(tmp[5])/p0
        pz = float(tmp[6])/p0
        t_.append(t)
        x_.append(x)
        y_.append(y)
        z_.append(z)
        px_.append(px)
        py_.append(py)
        pz_.append(pz)
    return t_, x_, y_, z_, px_, py_, pz_
  
def main():
    partfile = 'ParticleTracking/1.txt'
    picspath = 'pics'
    config = utils.get_config("ParsedInput.txt")

    Xmax = float(config['X_Max'])*1e4 #mkm
    Xmin = float(config['X_Min'])*1e4 #mkm
    Ymax = float(config['Y_Max'])*1e4 #mkm
    Ymin = float(config['Y_Min'])*1e4 #mkm
    Zmax = float(config['Z_Max'])*1e4 #mkm
    Zmin = float(config['Z_Min'])*1e4 #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz

    step = (Xmax-Xmin)/nx

    mult = 1/(2.*dx*dy*dz*1e-12)

    fig = plt.figure(num=None)#, figsize=(20, 20), dpi=256)
    ax = fig.add_subplot(2,3,1)
    ay = fig.add_subplot(2,3,2)
    az = fig.add_subplot(2,3,3)
    apx = fig.add_subplot(2,3,4)
    apy = fig.add_subplot(2,3,5)
    apz = fig.add_subplot(2,3,6)
    
    t,x,y,z,px,py,pz = read_traj('./' + partfile)
    ax.set_ylabel('x')
    az.set_ylabel('z')
    apx.set_ylabel('px')
    apz.set_ylabel('pz')
    ax.plot(y, z)
    ay.plot(t, y)
    az.plot(t, z)
    apx.plot(t, px)
    apy.plot(t, py)
    apz.plot(t, pz)
    plt.show()    
    
    
if __name__ == '__main__':
    main()
