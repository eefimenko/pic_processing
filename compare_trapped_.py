#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils

def read_diagph(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def read_conc(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    return tmp[0]

def read_field(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def create_axis(n,step,x0=0):
    axis = []
    for i in range(n):
        axis.append(i*step + x0)
    return axis

def create_pulse(n,step,amp, tp, delay):
    dt = step
    ans = []
#    tp = 30
    tau = tp/1.1437
#    delay = 23.1099984185576458468467817
    for i in range(n):
        t = i*dt-delay
        if t > 0 and t < math.pi * tau:
            tmp = math.sin(t/tau)
            f = amp*tmp*tmp
        else:
            f = 0.
        ans.append(f)
    return ans

def get_color(j):
    color = ['r', 'g', 'b', 'k', 'c', 'y', 'G', 'B']
    if (j >= len(color)):
        j = j - len(color)
    return color[j]

def get_dirs_configs(array):
    cfgs = []
    dirs = []
    for j in range(1, len(array)):
        config = utils.get_config(array[j] + "/ParsedInput.txt")
        cfgs.append(config)
        dirs.append(array[j])
    return dirs, cfgs

def get_max_diagPh(dirs):
    n = []
    for path in dirs:
        n.append(utils.num_files(path + '/data/diagPh/'))
    return min(n)

def check_dt(configs):
    x0 = -1
    y0 = -1
    dt0 = -1
    ans = 0
    for config in configs:
        if x0 == -1:
            x0 = float(config['BOIterationPass'])
            y0 = float(config['TimeStep'])
            dt = x0*y0
            dt0 = dt
        else:
            x = float(config['BOIterationPass'])
            y = float(config['TimeStep'])
            dt = x*y
            if dt != dt0:
                print "dt differs", dt0, dt
                ans = 1
    return ans, dt*1e15

def get_axis_and_field(path, config):
    pi = 3.14159
    ne = []
    nect = []

    BOIterationPass = int(config['BOIterationPass'])
     
    try:
        tmp = config['BOIterationStart']
    except KeyError:
        tmp = 0
    BOIterationStart = int(tmp)
    if BOIterationStart%BOIterationPass == 0:
        nmin = BOIterationStart/BOIterationPass
    else:
        nmin = BOIterationStart/BOIterationPass + 1

    step = float(config['TimeStep'])*1e15*float(config['BOIterationPass'])
    omega = float(config['Omega'])
    T = 2 * utils.pi/omega
    nt = int(T*1e15/step)
#    diag = float(config['npos.SetBounds_1'])
#    power = float(config['PeakPowerPW'])
    power = float(config['PeakPower'])*1e-22
#    nx = int(config['npos.SetMatrixSize_0'])
#    step1 = diag/nx
    wl = float(config['Wavelength'])
    nepath = path + '/data/NeTrap'
    necpath = path + '/data/ne'
#    v = 2. * math.pi * step1 * step1 * wl
    num = utils.num_files(nepath)
    nmax = nmin + num
    axis = create_axis(nmax-nmin, step, nmin*step)
    
    dv = pi*wl*wl*0.25*wl*0.5

    i0 = 255 + 1*nt
    i1 = i0 + 3*nt
    n0 = 0
    n1 = 0
    print path, nmax, i0, i1
    netrfile = open(path + '/ntrapped.dat', 'w') 
    for i in range(nmin,nmax):

        nename = nepath + '/' + "%06d.txt" % (i,)
        e = read_field(nename)
        if i == i0:
            n0 = e
            n1 = e
        if i == i1:
            n1 = e
        ne.append(e)
        print i, e
        netrfile.write('%lf %le\n' % ((i-i0)*step, e))
#        nename = necpath + '/' + "%06d.txt" % (i,)
#        e = read_conc(nename) 
#        for j in range(len(e)):
#            e[j] = e[j]/((j+0.5)*v)
#        nect.append(max(e))
    t0 = (i1-i0)*step
    print n0, n1
    if n0 != 0 and n1 != 0:
        alpha = 1./t0 * math.log(n0/n1)
    else:
        alpha = 1.
    netrfile.close()
#    alpha = 1
    print alpha, alpha*T*1e15
    return axis, ne, nect, alpha*T*1e15,power

def sum_array(array):
    res = [0] * len(array[0])
    for i in range(len(array[0])):
        for j in range(len(array)):
            res[i] += array[j][i]
    return res

def sum_all(array):
    res = 0
    for i in range(len(array[0])):
        for j in range(len(array)):
            res += array[j][i]
    return res

def sum_arrayj(array, axis):
    res = [0] * len(array)
    for i in range(len(array[0])):
        for j in range(len(array)):
            res[j] += array[j][i]*axis[i]
    return res

def norm1(array, a=1.):
    maximum = max(array)
    for i,e in enumerate(array):
        array[i] = e/maximum*a
    return array

def get_diagph_spectrum(dirs, configs, i):
    picspath = 'pics'
    expath = '/data/diagPh/'
    fig = plt.figure(num=None)
    mp.rcParams.update({'font.size': 8})
#    ax1 = fig.add_subplot(1,2,1)
#    ax1.set_xlabel('Energy, GeV')
#    ax1.set_ylabel('Number of photons')
#    ax1.set_yscale('log') 
#    plt.text(0.1,0.9 * max(field[0]),'Photon spectra for theta = 0')
#    plt.ylim([2e-4, 1])
#    plt.xlim([0, 5])
#    ax2 = fig.add_subplot(1,2,2)
#    ax2.set_xlabel('Energy, GeV')
#    ax2.set_ylabel('Number of photons')
#    ax2.set_yscale('log') 
#    plt.text(0.1,0.6,'Photon spectra for theta = 90')
#    plt.ylim([2e-4, 1])
#    plt.xlim([0, 1.5])
    ax3 = fig.add_subplot(2,1,1)
    ax3.set_title('Photon spectra for all theta')
    ax3.set_xlabel('Energy, GeV')
    ax3.set_ylabel('Number of photons')
    ax3.set_yscale('log') 
#    plt.ylim([2e-4, 1e3])
    plt.xlim([0, 5])
    ax4 = fig.add_subplot(2,1,2)
    ax4.set_title('Radiation pattern')
    ax4.set_xlabel('Angle')
    ax4.set_ylabel('Photon energy')
    ax4.set_yscale('log') 
#    plt.ylim([2e-4, 1e3])
    plt.xlim([0, 90])
    nph = []
    for j in range(len(dirs)):
        config = configs[j]
        path = dirs[j] + '/' 
        ev = float(config['eV'])
        ang_max = float(config['diagPh.SetBounds_3'])/math.pi*180
        ang_min = float(config['diagPh.SetBounds_2'])
        en_max = float(config['diagPh.SetBounds_1'])/ev*1e-9
        en_min = float(config['diagPh.SetBounds_0'])/ev*1e-9
        en_num = int(config['diagPh.SetMatrixSize_0'])
        ang_num = int(config['diagPh.SetMatrixSize_1'])
        ang_step = (ang_max - ang_min)/ang_num
        en_step = (en_max - en_min)/en_num
    
        en_axis = create_axis(en_num, en_step, en_min)
        ang_axis = create_axis(ang_num, ang_step, ang_min)
    
        name = path + expath + "%06d.txt" % (i,)
        field = read_diagph(name,en_num,ang_num,1./en_step)
        if sum_all(field) == 0:
            print 'Skipped ' + name
            plt.close()
            nph.append(0)
            continue
        sum1 = sum_array(field)
        sumj1 = norm1(sum_arrayj(field, en_axis))
        nph.append(sum_all(field))      
#        p1, = ax1.plot(en_axis, field[0], get_color(j), label = path)
#        p1, = ax2.plot(en_axis, field[ang_num/2], get_color(j), label = path)
        p1, = ax3.plot(en_axis, sum1, get_color(j), label = path)
        p1, = ax4.plot(ang_axis, sumj1, get_color(j), label = path)
#    ax1.legend(loc='upper right', shadow=True)
#    ax2.legend(loc='upper right', shadow=True)
    ax3.legend(loc='upper right', shadow=True)
    ax4.legend(loc='upper right', shadow=True)
    picname = picspath + '/diagph_compare%d.png' % (i,)
    plt.savefig(picname)
    plt.close()
    print nph
    return nph

def main():
    leg = []
    axis = []
    ne = []
    nect = []

    pwr = []
    rate = []
    picspath = 'pics'
    dirs, configs = get_dirs_configs(sys.argv)
    equal = check_dt(configs)
    nph_t = []*len(dirs)
    i0 = 0
    i1 = 0

    if equal == 0:
        for i in range(i0,i1):
            print i
            nph = get_diagph_spectrum(dirs, configs, i)

            for j in range(len(nph_t)):
                nph_t[j].append(nph[j]) 

    else:
        print "Config is different, no diagPh comparison"

    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(1,1,1)

    for j in range(len(nph_t)):
        pe, = ax1.plot(nph_t[j], get_color(j), label = dirs[j])
    plt.savefig(picspath + '/nphoton.png')

    f = open('escape_rate.dat', 'w')

    for j in range(len(dirs)):
        axis_tmp, ne_tmp, nect_tmp, alpha, power = get_axis_and_field(dirs[j], configs[j])
        axis.append(axis_tmp)
        ne.append(ne_tmp)
        nect.append(nect_tmp)
        leg.append(sys.argv[j])
        rate.append(alpha)
        pwr.append(power)
        f.write(str(power) + ' ' + str(alpha) + '\n')
    f.close()
   
    m = 0

    for j in range(len(ne)):
        if axis[j][-1] > m:
            m = axis[j][-1]


    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(1,1,1)
    
    print len(axis[j]), len(ne[j])
    for j in range(len(ne)):
        pe, = ax1.plot(axis[j], ne[j], get_color(j), label = dirs[j])

    ax1.legend(loc='upper left', shadow=True)
    ax1.set_yscale('log')
    ax1.set_xlim(0, m)
    ax1.set_ylabel('Number of trapped particles')
    ax1.set_xlabel('time, fs')
    
#    ax2 = fig.add_subplot(2,1,2)
#    ax2.plot(pwr, rate)
#    ax2.set_ylabel('Escape rate, fs^-1')
#    ax2.set_xlabel('Power, PW')
   

    plt.savefig(picspath + '/concentration_compare.png')

    
    
if __name__ == '__main__':
    main()
