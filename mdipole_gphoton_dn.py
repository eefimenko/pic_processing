#!/usr/bin/python
# -*- coding: utf-8 -*-

#/media/evgeny/TOSHIBA EXT/results/test_spectra/20pw_lin
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mpl
import numpy as np
import utils
import sys
import math
from scipy.interpolate import spline
from matplotlib.lines import Line2D as l2d
from matplotlib.patches import Arc as arc

def find_level(a, level=0.5):
    m = max(a)
    res = 0
    for i in xrange(1,len(a)):
        if a[-i] < level*m and a[-i-1] >= level*m:
            res = i
            break
    return len(a)-i

def norm(a):
    m = max2d(a)
    if m != 0:
        for i in range(len(a)):
            for j in range(len(a[0])):
                a[i][j] /= m
                a[i][j] += 1e-10
    return a

def max2d(array):
    return max([max(x) for x in array])

def find_max_nz(a):
    n = len(a)
    for i in range(1,len(a)):
        if a[-i] > 1e-8:
            n = len(a)-i
            break
    return n

def get_angle_plot(line1, line2, offset = 1, color = None, origin = [0,0], len_x_axis = 1, len_y_axis = 1):

    l1xy = line1.get_xydata()
    print l1xy
    # Angle between line1 and x-axis
    slope1 = (l1xy[1][1] - l1xy[0][1]) / float(l1xy[1][0] - l1xy[0][0])
    angle1 = abs(math.degrees(math.atan(slope1))) # Taking only the positive angle

    l2xy = line2.get_xydata()

    # Angle between line2 and x-axis
    slope2 = (l2xy[1][1] - l2xy[0][1]) / float(l2xy[1][0] - l2xy[0][0])
    angle2 = abs(math.degrees(math.atan(slope2)))

    theta1 = min(angle1, angle2)
    theta2 = max(angle1, angle2)

    angle = theta2 - theta1

    if color is None:
        color = line1.get_color() # Uses the color of line 1 if color parameter is not passed.

    return arc(origin, len_x_axis*offset, len_y_axis*offset, 0, theta1, theta2, color=color, label = str(angle)+u"\u00b0")

def read_field(file,nx,ny,jmin=0,jmax=sys.maxint,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            if i >= jmin and i < jmax:
                row.append(array[index])
            else:
                row.append(0.)
        field.append(row)
    return field

def norm3(a1, a2, a3):
    m1 = max(a1)
    m2 = max(a2)
    m3 = max(a3)
    m = m1
    if (m2 > m):
        m = m2
    if (m3 > m):
        m = m3
    for i in range(len(a1)):
        a1[i] /= m
        a2[i] /= m
        a3[i] /= m
    return a1, a2, a3

def make_patch_spines_invisible(ax):
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)

def sp_theta(a,min_energy=0, max_energy=sys.float_info.max, de=1.):
    res = np.zeros(len(a[0]))
#    print a[0]
#    print de, level
    for i in range(len(a)):
        energy = i*de
        if energy >= min_energy and energy < max_energy:
            for j in range(len(a[0])):
                res[j] += a[i][j]
    m = max(res)
#    print res
#    print m
    if m != 0:
        res /= m
    return res

def smooth(a,factor):
    n = len(a)
    n1 = n/factor
#    if n1%factor != 0:
#        print 'Error'
    res = [0] * n1
    for i in range(n1):
        for j in range(factor):
            res[i] += a[i*factor + j]/factor
    return res

def main():
    path = '.'
    
    picspath = 'pics'
   
    num = len(sys.argv) - 1
    regime = sys.argv[1]
    wtype = 1

    if regime == 'r':
        print 'read file'
    elif regime == 'r_sp':
        print 'read file'
    elif regime == 'r_norm':
        print 'read normalized file'
    elif regime == 'scan':
        print 'scan all spectra from EnAngSph'
    elif regime == 'scan_sp':
        print 'scan all spectra from EnSpSph'
    elif regime == 'scan_norm':
        print 'scan all spectra from EnAngSph and normalize'
    elif regime == 'scan_sp_norm':
        print 'scan all spectra from EnSpSph and normalize'
    elif regime == 'c':
        print 'calculate file'
    elif regime == 'c_sp':
        print 'calculate file'
    elif regime == 'c_sp_norm':
        print 'calculate file'
    elif regime == 'c_norm':
        print 'calculate normalized file'
    elif regime == 'dn_c':
        print 'calculate diagram'
    elif regime == 'dn_r':
        print 'read diagram'    
    else:
        print 'Wrong type, correct r - read or c - calculate, rs - read smoothed file, cs - calculate smoothed file'
        sys.exit(0)

    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(path + utils.enspsphpath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[2] 
        nmin = int(sys.argv[2])
        nmax = utils.num_files(path + utils.enspsphpath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[2] + ' to ' + sys.argv[3]
        nmin = int(sys.argv[2])
        nmax = int(sys.argv[3])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[2] + ' to ' + sys.argv[3] + ' with delta ' + sys.argv[4]
        nmin = int(sys.argv[2])
        nmax = int(sys.argv[3])
        delta = int(sys.argv[4])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax-nmin) + ' files'
    normalize = 'norm' in regime
    
    config = utils.get_config(path + "/ParsedInput.txt")
    x0 = int(config['QEDstatistics.OutputIterStep'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    T = 2 * math.pi/omega*1e15
    step = x0*y0*1e15/T
    dtt = x0*y0*1e15
    nT = int(1./step) # half of the period!
    print nT
    ev = float(config['eV'])
    global ne,nt
    ne = int(config['QEDstatistics.OutputN_E'])
    nt = int(config['QEDstatistics.OutputN_theta'])
    tmax = float(config.get('QEDstatistics.ThetaMax', 3.14159))*180./3.14159
    tmin = float(config.get('QEDstatistics.ThetaMin', 0))*180./3.14159
    emax = float(config['QEDstatistics.Emax'])/ev*1e-9
    emin = float(config['QEDstatistics.Emin'])/ev*1e-9
    wl = float(config['Wavelength'])
    Xmax = float(config['X_Max'])/wl #mkm
    Xmin = float(config['X_Min'])/wl #mkm
    Ymax = float(config['Y_Max'])/wl #mkm
    Ymin = float(config['Y_Min'])/wl #mkm
    Zmax = float(config['Z_Max'])/wl #mkm
    Zmin = float(config['Z_Min'])/wl #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    factor_ne = 5
    factor_nt = 1
    de = (emax - emin)/ne
    dt = (tmax - tmin)/nt
    dth = dt * 3.14159/180. 
    dph = 2*3.14159
    ne1 = ne/factor_ne
    nt1 = nt/factor_nt
    
    N = 2*(nmax-nmin)/nT-1
    print 'Number of periods', N
    spectraT = [np.zeros((ne,nt))for n_ in range(nT)]
    spectra_spT = [np.zeros(ne)for n_ in range(nT)]
    spectra_sp_normT = [np.zeros(ne)for n_ in range(nT)]
    dn_spectra = np.zeros(ne*nt)
    
    if regime == 'c' or regime == 'c_norm':
        for k_ in range(N):
            n0 = 0
            for n_ in range(nT):
                iteration = nmin+k_*nT + n_
                sp_ = utils.bo_file_load(path + utils.enangsphpath,iteration,fmt='%.4f',verbose = 1)
                print sp_.shape, ne,nt
                sp_ = np.reshape(sp_, (ne,nt), order = 'F')
                if n_ == 0 or normalize:
                    spg_ = np.sum(sp_, axis=1)
                    n0 = utils.full_number(spg_,de,ne)
                spectraT[n_] += np.reshape(sp_, (ne,nt), order = 'F')/n0
                
        for n_ in range(nT):
            if normalize:
                sphname = path + '/norm_spectra_nT%d_%d_%d_%d.txt' %(nmin,nmax,nT,n_)
            else:
                sphname = path + '/spectra_nT%d_%d_%d_%d.txt' %(nmin,nmax,nT,n_)
            print 'Saving ', sphname 
            spectraT[n_].tofile(sphname,' ', '%.6e')
    elif regime == 'c_sp' or regime == 'c_sp_norm' :
        for k_ in range(N):
            n0 = 0
            for n_ in range(nT):
                iteration = nmin+k_*nT/2 + n_
                read, sp_ = utils.bo_file_load(path + utils.enspsphpath,iteration,fmt='%.4f',verbose = 1)
                #spgt_, spgnt_ = utils.energy_spectra_to_number(sp_,de,ne)
                spgt_ = sp_ 
                if n_ == 0:
                    n0 = sum(spgt_)
                spectra_spT[n_] += spgt_/n0
                spectra_sp_normT[n_] += spgt_/sum(spgt_)
                
        for n_ in range(nT):
            sphname = path + '/spectra_sp_nT%d_%d_%d_%d.txt' %(nmin,nmax,nT,n_)
            print 'Saving ', sphname 
            spectra_spT[n_].tofile(sphname,' ', '%.6e')
            sphname = path + '/spectra_sp_normnT%d_%d_%d_%d.txt' %(nmin,nmax,nT,n_)
            print 'Saving ', sphname 
            spectra_sp_normT[n_].tofile(sphname,' ', '%.6e')
    elif regime == 'dn_c':
        for n_ in range(nmin,nmax):
            read, sp_ = utils.bo_file_load(path + utils.enangsphpath,n_,fmt='%.4f',verbose = 1)
            dn_spectra += sp_
        sphname = path + '/dn_spectra_%d_%d.txt' %(nmin,nmax)
        print 'Saving ', sphname 
        dn_spectra.tofile(sphname,' ', '%.6e')
    elif regime == 'dn_r':
        sphname = path + '/dn_spectra_%d_%d.txt' %(nmin,nmax)
        print 'Reading ', sphname 
        dn_spectra += np.fromfile(sphname, sep = ' ')
    elif regime == 'r' or regime == 'r_norm':
        for n_ in range(nT):
            if normalize:
                sphname = path + '/norm_spectra_nT%d_%d_%d_%d.txt' %(nmin,nmax,nT,n_)
            else:
                sphname = path + '/spectra_nT%d_%d_%d_%d.txt' %(nmin,nmax,nT,n_)
            print 'Reading ', sphname
            sp_ = np.fromfile(sphname, sep = ' ')
            spectraT[n_] += np.reshape(sp_, (ne,nt))
    elif regime == 'r_sp':
        for n_ in range(nT):
            sphname = path + '/spectra_sp_nT%d_%d_%d_%d.txt' %(nmin,nmax,nT,n_)
            print 'Reading ', sphname 
            spectra_spT[n_] += np.fromfile(sphname, sep = ' ')
            sphname = path + '/spectra_sp_normnT%d_%d_%d_%d.txt' %(nmin,nmax,nT,n_)
            print 'Reading ', sphname 
            spectra_sp_normT[n_] += np.fromfile(sphname, sep = ' ')
                        
    elif regime == 'scan' or regime == 'scan_norm':
        sp_t = []
        for i_ in range(nmin,nmax):
            sp_ = utils.bo_file_load(path + utils.enangsphpath,i_,fmt='%.4f',verbose = 1)
            sp_ = np.reshape(sp_, (ne,nt), order = 'F')
            print sp_.shape
            spg_ = np.sum(sp_, axis=1)
            spgt_ = utils.energy_spectra_to_number(spg_,de,ne,normalize)/(dtt*1e-15)
            sp_t.append(spgt_)
        if normalize:
            picname = picspath + '/spectra_nT_scan_norm.png'
        else:
            picname = picspath + '/spectra_nT_scan.png'
        print 'Saving ', picname
        fig = plt.figure(num=None, figsize=(10, 10), tight_layout = True)
        axe = utils.create_axis(nmax-nmin, 1, nmin)
        axt = utils.create_axis(ne, de, 0)
        axel = fig.add_subplot(1,1,1)
        mmax = max([sp_.max() for sp_ in sp_t])
        axel.imshow(sp_t, aspect = 'auto',  extent = [0, emax, nmin, nmax], origin = 'lower', norm=clr.LogNorm(mmax*1e-3, mmax))
        axel.set_xlabel('W, GeV')
        axel.set_ylabel('Iteration')
        axel.set_ylim([nmin,nmax])
        axel.set_xlim([0,3])
        plt.savefig(picname, dpi=512)
        plt.close()

        # tau = np.zeros(ne)
        # cm = np.zeros(ne)
        # fmax = np.zeros(ne)
        # for j in range(ne):
        #     sumt_ = 0.
        #     sum_ = 0.
        #     t_ = 0.
            
        #     for i in range(len(sp_t)):
        #         sumt_ += sp_t[i][j]*i*dtt
        #         sum_ += sp_t[i][j]
        #         if sp_t[i][j] > fmax[j]:
        #             fmax[j] = sp_t[i][j]
        #     if sum_ != 0:    
        #         t_ = sumt_/sum_
        #     cm[j] = t_
        #     sumt2_ = 0
        #     for i in range(len(sp_t)):
        #         sumt2_ += (i*dtt - t_)*(i*dtt - t_)*sp_t[i][j]
        #     if sum_ != 0:
        #         tau[j] = math.sqrt(sumt2_/sum_) 
        # print tau
        # picname = picspath + '/spectra_nT_tau.png'
        # print 'Saving ', picname
        # fig = plt.figure(num=None, figsize=(10, 5), tight_layout = True)
        # ax = fig.add_subplot(1,1,1)
        # axise = utils.create_axis(ne, de, 0)
        # ax.plot(axise, tau)
        # ax.plot([0, axise[-1]], [1,1], 'g--')
        # ax.set_xlim([0,4])
        # ax.set_ylabel('$\\tau, s$')
        # ax2 = ax.twinx()
        # ax2.plot(axise, fmax, 'r')
        # ax2.set_yscale('log')
        # ax2.set_xlim([0,4])
        # ax2.set_xlabel('$\hbar\omega, GeV$')
        # ax2.set_ylabel('Flux, $s^{-1}$')
        # plt.savefig(picname)
        # plt.close() 
    elif regime == 'scan_sp' or regime == 'scan_sp_norm':
        sp_t = []
        spnorm_t = []
        for i_ in range(nmin,nmax):
            sp_ = utils.bo_file_load(path + utils.enspsphpath,i_,fmt='%.4f',verbose = 1)
            #spgt_, spgnt_ = utils.energy_spectra_to_number(sp_,de,ne)
            sp_ = utils.smooth(sp_, factor_ne)
            sp_t.append(sp_/(dtt*1e-15))
            spnorm_t.append(sp_/(dtt*1e-15))

        picname = picspath + '/spectra_nT_scan_sp.png'
        print 'Saving ', picname
        fig = plt.figure(num=None, figsize=(20, 10), tight_layout = True)
        axe = utils.create_axis(nmax-nmin, 1, nmin)
        axt = utils.create_axis(ne, de, 0)
        axel = fig.add_subplot(1,2,1)
        mmax = max([sp_.max() for sp_ in sp_t])
        axel.imshow(sp_t, aspect = 'auto',  extent = [0, emax, nmin, nmax], origin = 'lower', norm=clr.LogNorm(mmax*1e-6, mmax))
        axel.set_xlabel('W, GeV')
        axel.set_ylabel('Iteration')
        axel.set_ylim([nmin,nmax])
        axel.set_xlim([0,3])
        axel = fig.add_subplot(1,2,2)
        mmax = max([sp_.max() for sp_ in sp_t])
        axel.imshow(spnorm_t, aspect = 'auto',  extent = [0, emax, nmin, nmax], origin = 'lower', norm=clr.LogNorm(mmax*1e-6, mmax))
        axel.set_xlabel('W, GeV')
        axel.set_ylabel('Iteration')
        axel.set_ylim([nmin,nmax])
        axel.set_xlim([0,3])
        plt.savefig(picname, dpi=512)
        plt.close()
        
        if normalize:
            picname = picspath + '/scan_time_norm_%d_%d.png' % (nmin,nmax)
        else:
            picname = picspath + '/scan_nT_time_%d_%d.png' % (nmin,nmax)
        print 'Saving ', picname
        
        tau = np.zeros(ne1)
        tau1 = np.zeros(ne1)
        cm = np.zeros(ne1)
        fmax = np.zeros(ne1)
        ratio = np.zeros(ne1)
        t1=0
        t2=0
        for j in range(ne1):
            sumt_ = 0.
            sum_ = 0.
            t_ = 0.
            mmax = 0.
            timeline = np.zeros(len(sp_t))
            imax = 0
            mmax = 0
            mmin = 1e9
            for i in range(len(sp_t)):
                timeline[i] = sp_t[i][j]
                if timeline[i] < mmin:
                    mmin = timeline[i]
            for i in range(10,len(sp_t)-10):
                if timeline[i] > mmax:
                    mmax = timeline[i]
                    imax = i
                
            isp = len(sp_t)
            print imax
            for i in range(len(sp_t)):
                sumt_ += timeline[i]*i*dtt
                sum_ += timeline[i]
                if sp_t[i][j] > fmax[j]:
                    fmax[j] = timeline[i]
            # timeline = utils.savitzky_golay(timeline, 5, 3) 
            t2 = (isp-1)*dtt
            if mmax == 0:
                t2 = 0
            boundary = mmin + 0.5*(mmax-mmin)
            for i in range(isp-imax):
                if timeline[imax+i-2] > boundary and timeline[imax+i-1] < boundary:
                    t2 = (i+imax+1)*dtt
                    break
            t1 = 0        
            for i in range(imax):
                if timeline[imax-1-i] < boundary and timeline[imax-i] > boundary:
                    t1 = (imax-i-1)*dtt
                    break
            if t2 > t1:
                tau1[j] = t2-t1
            if imax == isp-1:
                tau1[j] = (isp-1)*dtt
            if mmax > 0:
                ratio[j] = mmin/mmax
                            
            # fig = plt.figure()
            # ax = fig.add_subplot(1,1,1)
            # ax.plot(timeline)
            # ax.plot([t1/dtt,t1/dtt], [0, max(timeline)], 'b')
            # ax.plot([t2/dtt,t2/dtt], [0, max(timeline)], 'g')
            # ax.plot([imax,imax], [0, max(timeline)], 'r:')
            # plt.show()
            # plt.close()
            if sum_ != 0:    
                t_ = sumt_/sum_
            cm[j] = t_
            sumt2_ = 0
            for i in range(len(sp_t)):
                sumt2_ += (i*dtt - t_)*(i*dtt - t_)*sp_t[i][j]
            if sum_ != 0:
                tau[j] = math.sqrt(sumt2_/sum_) 
        print tau1
        if normalize:
            picname = picspath + '/scan_norm_tau_%d_%d.png' % (nmin,nmax)
        else:
            picname = picspath + '/scan_tau_%d_%d.png' % (nmin,nmax)
        print 'Saving ', picname
        fig = plt.figure(num=None, figsize=(10, 5), tight_layout = True)
        fontsize = 16
        mpl.rcParams.update({'font.size': fontsize})
        ax = fig.add_subplot(1,1,1)
        axise = utils.create_axis(ne1, de*factor_ne, 0)
        ax1_ = []
        tau1_ = []
        rat_ = []
        for i in range(len(axise)):
            if tau[i] < 5 and tau[i] > 0 and axise[i] > 1:
                ax1_.append(axise[i])
                tau1_.append(tau[i]*1000)
                rat_.append(ratio[i])
                ilast = i
        #ax1_ = ax1_[:-2]
        #tau1_ = tau1_[:-2]
        #rat_ = rat_[:-2]
        tau1__ = utils.savitzky_golay(tau1_, 11,3)
        tau1_smooth = np.poly1d(np.polyfit(np.array(ax1_), np.array(tau1__), 2))
        #ax.plot(axise, tau, 'bx')
        ax.plot(ax1_, tau1_, 'b^')
        t, = ax.plot(ax1_, tau1_smooth(ax1_), 'b-', dashes = [6,2])
        ax.plot([0, axise[-1]], [1000,1000], 'k:')
        ax.set_xlim([0,1.5])
        xticks = [0,0.5,1,1.5,2,2.5,3,3.5]
        xticklabels = ['$%.1f$' %(x) for x in xticks]
        yticks = [200,400,800,600,1000,1200, 1400]
        yticklabels = ['$%d$' %(x) for x in yticks]
        ax.set_ylim([0,1500])
        ax.set_xticks(xticks)
        ax.set_xticklabels(xticklabels)
        ax.set_yticks(yticks)
        ax.set_yticklabels(yticklabels)
        ax.set_ylabel('$\\tau, as$')
        ax.set_xlabel('$\\varepsilon_\gamma, GeV$')
        if normalize:
            filename = 'tau_norm_%d_%d.txt' % (nmin,nmax)
        else:
            filename = 'tau_%d_%d.txt' % (nmin,nmax)
        f = open(filename, 'w')
        for i in range(len(ax1_)):
            f.write('%lf %lf\n'%(ax1_[i], tau1_[i]))
        f.close()
        ax2 = ax.twinx()
        # rat_smooth = np.poly1d(np.polyfit(np.array(ax1_), np.array(rat_), 5))
        # ax2.plot(ax1_, rat_, 'ro')
        # r, = ax2.plot(ax1_, rat_smooth(ax1_), 'r-', dashes = [6,2,2,2])
        #ax2.set_yscale('log')
        # ax2.set_xlim([1,4])
        # ax2.set_ylim([0,1.1])
        # yticks = [0.2,0.4,0.6,0.8,1]
        # yticklabels = ['$%.1f$' %(x) for x in yticks]
        # ax2.set_yticks(yticks)
        # ax2.set_yticklabels(yticklabels)
        # ax2.set_xlabel('$\\varepsilon_\gamma, GeV$')
        # ax2.set_ylabel('$r_c$')
        # ax3 = ax.twinx()
        # ax3.spines["left"].set_position(("axes", -0.15))
        # make_patch_spines_invisible(ax3)
        # # Second, show the right spine.
        # ax3.spines["left"].set_visible(True)
        # ax3.yaxis.set_label_position('left')
        # ax3.yaxis.set_ticks_position('left')
        f, = ax2.plot(axise, utils.savitzky_golay(fmax/max(fmax), 15,3), 'g')
        ax2.set_xlim([1,4])
        ax2.set_ylim([1e-5,3])
        ax2.set_yscale('log')
        yticks = [1,1e-1,1e-2,1e-3,1e-4,1e-5]
        yticklabels = ['$1$', '$10^{-1}$','$10^{-2}$','$10^{-3}$','$10^{-4}$','$10^{-5}$']
        ax2.set_yticks(yticks)
        ax2.set_yticklabels(yticklabels)
        ax2.set_ylabel('$P\'_{\\varepsilon}$')
        plt.legend([t,f], ['$\\tau$', '$P\'_\\varepsilon$'], loc = 'lower left', frameon=False)
        plt.savefig(picname, dpi=512)
        plt.close()
    else:
        print('Unknown regime')
        exit(-1)

    if 'dn' in regime:
        picname = picspath + '/dn_%d_%d.png' % (nmin,nmax)
        print 'Saving ', picname
        sp_ = np.reshape(dn_spectra, (ne,nt), order = 'F')
        sp1_ = utils.smooth_2d(sp_, factor_ne, factor_nt)
        spectra = np.sum(sp1_, axis = 1)
        full_number = utils.full_number(spectra, de*factor_ne, ne1)
        spectra /= full_number
        axe = utils.create_axis(nt1, dt*factor_nt, 0)
        axt = utils.create_axis(ne1, de*factor_ne, 0)
        
        f = open('dn_spectra_%d_%d.txt' % (nmin,nmax), 'w')
        for i_ in range(len(spectra)):
            f.write('%lf %lf\n' %(axt[i_], spectra[i_]))
        f.close()
        
#        fig = plt.figure()
#        ax = fig.add_subplot(1,1,1)
#        ax.plot(axt, spectra)
#        plt.show()
        
        fig = plt.figure(num=None, figsize=(10, 10), tight_layout = True)
        axel = fig.add_subplot(1,1,1)
        mmax = np.amax(dn_spectra)
        axel.imshow(sp1_, aspect = 'auto',  extent = [0, tmax, 0, emax], origin = 'lower')
        #axel.set_xlim([0,1])
        axel.set_ylim([0,3])
        plt.savefig(picname, dpi=512)
        plt.close()
        bound1 = 0.1
        bound2 = 1.0
        s_all_t = sp_theta(sp_, min_energy = 0., max_energy=10., de=de)
        s_t = sp_theta(sp_, min_energy = 0., max_energy=bound1, de=de)
        s1_t = sp_theta(sp_, min_energy = bound1, max_energy=bound2, de=de)
        s2_t = sp_theta(sp_, min_energy = bound2, de=de)
        s_all_t_sin = np.zeros(len(s_t))
        s_t_sin = np.zeros(len(s_t))
        s1_t_sin = np.zeros(len(s_t))
        s2_t_sin = np.zeros(len(s_t))
        s_t_ = np.zeros(len(s_t))
        s1_t_ = np.zeros(len(s_t))
        s2_t_ = np.zeros(len(s_t))
        en_j = np.zeros(len(s_t))
        en1_j = np.zeros(len(s_t))
        en2_j = np.zeros(len(s_t))
        tmp = 0
        tmp1 = 0
        tmp2 = 0
        for j in range(len(s_t)):
            s_all_t_sin[j] = (s_all_t[j]/math.sin((j+0.5)*dth))
            s_t_sin[j] = (s_t[j]/math.sin((j+0.5)*dth))
            s1_t_sin[j] = (s1_t[j]/math.sin((j+0.5)*dth))
            s2_t_sin[j] = (s2_t[j]/math.sin((j+0.5)*dth))
            s_t_[j] = s_t[j]
            s1_t_[j] = s1_t[j]
            s2_t_[j] = s2_t[j]
            tmp = s_t[j]
            tmp1 = s1_t[j]
            tmp2 = s2_t[j]
            en_j[j] = tmp
            en1_j[j] = tmp1
            en2_j[j] = tmp2
            
#        for j in range(len(s_t)):
#            s_all_t_sin[j+len(s_t)] = (s_all_t[-1-j]/math.sin(math.pi/2 + (j+0.5)*dth))
#            s_t_sin[j+len(s_t)] = (s_t[-1-j]/math.sin(math.pi/2 + (j+0.5)*dth))
#            s1_t_sin[j+len(s_t)] = (s1_t[-1-j]/math.sin(math.pi/2 + (j+0.5)*dth))
#            s2_t_sin[j+len(s_t)] = (s2_t[-1-j]/math.sin(math.pi/2 + (j+0.5)*dth))
#            s_t_[j+len(s_t)] = s_t[-1-j]
#            s1_t_[j+len(s_t)] = s1_t[-1-j]
#            s2_t_[j+len(s_t)] = s2_t[-1-j]
        
        lw = 0.3
        factor = 1
        s_all_t_sin = smooth(s_all_t_sin, factor)
        s_t_sin = smooth(s_t_sin, factor)
        s1_t_sin = smooth(s1_t_sin, factor)
        s2_t_sin = smooth(s2_t_sin, factor)
        s_t_ = smooth(s_t_, factor)
        s1_t_ = smooth(s1_t_, factor)
        s2_t_ = smooth(s2_t_, factor)
        
#        s_t_sin, s1_t_sin, s2_t_sin = norm3(s_t_sin, s1_t_sin, s2_t_sin)
    
        factor_ne=1
        factor_nt=1
        axt_ = utils.create_axis(nt/factor_nt, dt*factor_nt*math.pi/180., tmin)
        axt = utils.create_axis(nt/factor_nt/factor, dt*factor_nt*factor, tmin)
        
        f = open('dn_%d_%d.dat'%(nmin,nmax),'w')
        for i_ in range(len(axt)):
            f.write('%lf %lf\n' %(axt[i_], s_all_t_sin[i_]))
        f.close()
        fontsize = 10
        labelsize = 16
        num1 = 11
        lw = 0.5
        smoothed_s_t_sin = utils.savitzky_golay(s_t_sin, num1, 3)
        smoothed_s1_t_sin = utils.savitzky_golay(s1_t_sin, num1, 3)
        smoothed_s2_t_sin = utils.savitzky_golay(s2_t_sin, num1, 3)

        f = open('dn_theta_sin_%d_%d.txt' % (nmin,nmax), 'w')
        for i_ in range(len(smoothed_s_t_sin)):
            f.write('%lf %lf %lf %lf %lf\n' %(axt[i_], s_all_t_sin[i_], s_t_sin[i_], s1_t_sin[i_], s2_t_sin[i_]))
        f.close()
        
        fig = plt.figure(num=None, figsize=(4, 3), tight_layout = True)
        mpl.rcParams.update({'font.size': fontsize})
        picname = picspath + '/dn_omega_a.png'
        ax1 = fig.add_subplot(1,1,1)
        print len(axt), len(axt_), len(s_t_sin)
        ax1.plot(axt, s_t_sin, 'r', label = u'$< %.1f GeV$'%(bound1), linewidth = lw, dashes = [6,1,1,1])
        ax1.plot(axt, s1_t_sin, 'g', label = u'$%.1f - %.0f GeV$'%(bound1, bound2), linewidth = lw, dashes = [2,2])
        ax1.plot(axt, s2_t_sin, 'b', label = u'$> %.0f GeV$'%(bound2), linewidth = lw)
        plt.legend(loc = 'upper right', frameon=False)
        ax1.set_xlim([0, 180.])
        ax1.set_ylim([1e-3, 2])
        
        ax1.set_xticks([0, 45, 90, 135, 180])
        ax1.set_xticklabels(['0', '$\pi/4$', '$\pi/2$', '$3\pi/4$', '$\pi$'])
        ax1.set_yscale(u'log')
        ax1.set_xlabel(u'$\\theta$')
        ax1.set_ylabel('$E_\Omega^\prime$')
#        ax1.set_yticks([1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1])
#        ax1.set_yticklabels(['$10^{-5}$', '$10^{-4}$', '$10^{-3}$', '$10^{-2}$', '$10^{-1}$', '$1$'])
        ax1.set_yticks([1e-3, 1e-2, 1e-1, 1])
        ax1.set_yticklabels(['$10^{-3}$', '$10^{-2}$', '$10^{-1}$', '$1$'])
#        ax1.text(-35, 1, u'$(б)$', fontsize = labelsize)
        plt.savefig(picname, dpi=512)
        plt.close()
        
        fig = plt.figure(num=None, figsize=(4, 3), tight_layout = True)
        mpl.rcParams.update({'font.size': fontsize})
        picname = picspath + '/dn_omega_b.png'
        ax1 = fig.add_subplot(1,1,1)
#        for i in range(len(axt)):
#            axt[i] *= math.pi/180*1000
        ax1.plot(axt, s_t_sin, 'r', label = u'$< %.2f ГэВ$'%(bound1), linewidth = lw, dashes = [6,1,1,1])
        ax1.plot(axt, s1_t_sin, 'g', label = u'$%.2f - %.1f ГэВ$'%(bound1, bound2), linewidth = lw, dashes = [2,2])
        ax1.plot(axt, s2_t_sin, 'b', label = u'$> %.1f ГэВ$'%(bound2), linewidth = lw)
        
        a0 = 2500*math.sqrt(2.)
        alpha = 1/(0.5*a0)*1000
        ax1.plot([alpha, alpha], [0,1], 'k--', linewidth = lw)
        ax1.set_xlim([90, 100.])
        ax1.set_ylim([0, 1.])
        ax1.set_yticks([0, 0.2, 0.4, 0.6,0.8, 1])
        ax1.set_yticklabels(['$0$', '$0.2$', '$0.4$', '$0.6$', '$0.8$', '$1$'])
#        ax1.set_xticks([0, alpha, 2, 4, 6, 8, 10])
#        ax1.set_xticklabels(['$0$', '$\\alpha^*$' ,'$2$', '$4$', '$6$', '$8$', '$10$'])
        ax1.set_ylabel('$E_\Omega^\prime$')
        ax1.set_xlabel(u'$\\theta$, мрад')
        cmap1 = mpl.cm.get_cmap('Reds')
        cmap1.set_under('w', False)
        #    cmap2 = mpl.cm.get_cmap('Greens')
        #    cmap2.set_under('w', False)
#        a = plt.axes([.6, .6, .3, .3])
#        i = 450
#        amax = 1
#        nx = 512
#        ny = 512
#        name = utils.neypath + '/' + "%06d.txt" % (i,)
#        field = read_field(name,nx,ny, jmin=0, jmax=ny/2)
#        surf = a.imshow(norm(field), extent=[Xmin, Xmax, Ymin, Ymax], cmap = 'Greens', vmin = 0.0001, vmax = amax, norm = clr.LogNorm())
#        name = utils.eypath + '/' + "%06d.txt" % (i,)
#        field = read_field(name,nx,ny, jmin=ny/2, jmax=ny)
   
#        surf = a.imshow(norm(field), extent=[Xmin, Xmax, Ymin, Ymax], cmap = cmap1, vmin = 0.0001, vmax = amax)
#        line1 = l2d([0, 0], [-1.8, 1.8], color='k', linestyle='-', linewidth=0.5)
#        line2 = l2d([0, 1.], [0, math.sqrt(3)], color='k', linestyle='-', linewidth=0.5)
#        a.add_line(line1)
#        a.add_line(line2)
#        angle_plot = get_angle_plot(line1, line2, 1)
#        a.add_patch(angle_plot)
#        a.set_xlim([-2,2])
#        a.set_ylim([-2,2])
#        a.text(0.15, 0.7, '$\\theta$', fontsize=18)

#        plt.xticks([])
#        plt.yticks([])

        te = find_max_nz(s_t)*dt*factor_nt
        ax1.text(-2, 1, '$(b)$', fontsize = labelsize)
        #    plt.tight_layout()
        plt.savefig(picname, dpi=512)
        plt.close()

        fig = plt.figure(num=None, figsize=(4, 3), tight_layout = True)
        mpl.rcParams.update({'font.size': fontsize})
        picname = picspath + '/dn_omega_c.png'
        ax1 = fig.add_subplot(1,1,1)
        for i in range(len(axt)):
            axt[i] /= math.pi/180*1000
        print len(axt), len(axt_), len(s_t_), axt[-1]

        smoothed_s_t_ = utils.savitzky_golay(s_t_, num1, 3)
        smoothed_s1_t_ = utils.savitzky_golay(s1_t_, num1, 3)
        smoothed_s2_t_ = utils.savitzky_golay(s2_t_, num1, 3)
        f = open('dn_theta.txt', 'w')
        for i_ in range(len(smoothed_s_t_)):
            f.write('%d %lf %lf %lf\n' %(i_, smoothed_s_t_[i_], smoothed_s1_t_[i_], smoothed_s2_t_[i_]))
        f.close()
        
        ax1.plot(axt, smoothed_s_t_, 'r', label = u'$< %.2f ГэВ$'%(bound1), linewidth = lw, dashes = [6,1,1,1])
        ax1.plot(axt, smoothed_s1_t_, 'g', label = u'$%.2f - %.1f ГэВ$'%(bound1, bound2), linewidth = lw, dashes = [2,2])
        ax1.plot(axt, smoothed_s2_t_, 'b', label = u'$> %.1f ГэВ$'%(bound2), linewidth = lw)
        plt.legend(loc = 'upper center', frameon=False)
        ax1.set_xlim([0, 180.])
        ax1.set_ylim([0, 1])
        ax1.set_yticks([0, 0.2, 0.4, 0.6,0.8, 1])
        ax1.set_yticklabels(['$0$', '$0.2$', '$0.4$', '$0.6$', '$0.8$', '$1$'])
        ax1.set_xticks([0, 45, 90, 135, 180])
        ax1.set_xticklabels(['0', '$\pi/4$', '$\pi/2$', '$3\pi/4$', '$\pi$'])
        #ax1.set_yscale(u'log')
        ax1.set_xlabel(u'$\\theta$')
        ax1.set_ylabel('$E_\\theta^\prime$')
        ax1.text(-35, 1, u'$(а)$', fontsize = labelsize)
        plt.savefig(picname, dpi=512)
        plt.close()
        
        fig = plt.figure(num=None, figsize=(4, 3), tight_layout = True)
        mpl.rcParams.update({'font.size': fontsize})
        picname = picspath + '/dn_omega_d.png'
        ax1 = fig.add_subplot(1,1,1)
        # for i in range(len(axt)):
        #     axt[i] *= math.pi/180*1000
        ax1.plot(axt, smoothed_s_t_, 'r', label = u'$< %.2f ГэВ$'%(bound1), linewidth = lw, dashes = [6,1,1,1])
        ax1.plot(axt, smoothed_s1_t_, 'g-', label = u'$%.2f - %.1f ГэВ$'%(bound1, bound2), linewidth = lw, dashes = [2,2])
        ax1.plot(axt, smoothed_s2_t_, 'b-', label = u'$> %.1f ГэВ$'%(bound2), linewidth = lw)
        ax1.set_xlim([0, 45.])
        ax1.set_ylim([0, 1.])
        ax1.set_ylabel('$E_\\theta^\prime$')
        ax1.set_xlabel('$\\theta$')
        cmap1 = mpl.cm.get_cmap('Reds')
        cmap1.set_under('w', False)
        #    cmap2 = mpl.cm.get_cmap('Greens')
        #    cmap2.set_under('w', False)
#        a = plt.axes([.6, .6, .3, .3])
#        i = 450
#        amax = 1
#        nx = 512
#        ny = 512
#        name = utils.neypath + '/' + "%06d.txt" % (i,)
#        field = read_field(name,nx,ny, jmin=0, jmax=ny/2)
#        surf = a.imshow(norm(field), extent=[Xmin, Xmax, Ymin, Ymax], cmap = 'Greens', vmin = 0.0001, vmax = amax, norm = clr.LogNorm())
#        name = utils.eypath + '/' + "%06d.txt" % (i,)
#        field = read_field(name,nx,ny, jmin=ny/2, jmax=ny)
   
#        surf = a.imshow(norm(field), extent=[Xmin, Xmax, Ymin, Ymax], cmap = cmap1, vmin = 0.0001, vmax = amax)
#        line1 = l2d([0, 0], [-1.8, 1.8], color='k', linestyle='-', linewidth=0.5)
#        line2 = l2d([0, 1.], [0, math.sqrt(3)], color='k', linestyle='-', linewidth=0.5)
#        a.add_line(line1)
#        a.add_line(line2)
#        angle_plot = get_angle_plot(line1, line2, 1)
#        a.add_patch(angle_plot)
#        a.set_xlim([-2,2])
#        a.set_ylim([-2,2])
#        a.text(0.15, 0.7, '$\\theta$', fontsize=18)

#        plt.xticks([])
#        plt.yticks([])
        i1 = find_level(utils.savitzky_golay(en_j, num1, 3), 0.5)
        t1 = [0, utils.savitzky_golay(s_t_, num1, 3)[i1]]
        t2 = [axt[i1], axt[i1]]
        ax1.plot(t2, t1, 'r-', dashes = [2,2], linewidth=lw)
        i2 = find_level(utils.savitzky_golay(en1_j, num1, 3), 0.5)
        t1 = [0, utils.savitzky_golay(s1_t_, num1, 3)[i2]]
        t2 = [axt[i2], axt[i2]]
        ax1.plot(t2, t1, 'g-', dashes = [2,2], linewidth=lw)
        i3 = find_level(utils.savitzky_golay(en2_j, num1, 3), 0.5)
        t1 = [0, utils.savitzky_golay(s2_t_, num1, 3)[i3]]
        t2 = [axt[i3], axt[i3]]
        # print axt_[i1], axt_[i2], axt_[i3]
        ax1.set_xticks([0, axt[i1], axt[i2], axt[i3],45])
        ax1.set_xticklabels(['$0$', '$%.2f$'%(axt_[i1]), '$%.2f$'%(axt_[i2]), '$%.2f$'%(axt_[i3]), '$\pi/4$'])
        ax1.set_yticks([0, 0.2, 0.4, 0.6,0.8, 1])
        ax1.set_yticklabels(['$0$', '$0.2$', '$0.4$', '$0.6$', '$0.8$', '$1$'])
        ax1.plot(t2, t1, 'b-', dashes = [2,2], linewidth=lw)
        te = find_max_nz(s_t)*dt*factor_nt
        ax1.text(-8, 1, '$(b)$', fontsize = labelsize)
        #    plt.tight_layout()
        plt.savefig(picname, dpi=512)
        plt.close()
    if 'scan' not in regime and 'dn' not in regime:
        sp_t = []
        for n_ in range(nT):
            if 'sp' not in regime:
                sp_ = utils.smooth_2d(spectraT[n_], factor_ne, factor_nt)
                spg_ = np.sum(sp_, axis=1)
                sp_t.append(spg_)
            else:
                if normalize:
                    sp_ = utils.smooth(spectra_sp_normT[n_], factor_ne)
                    sp_t.append(sp_)
                else:
                    sp_ = utils.smooth(spectra_spT[n_], factor_ne)
                    sp_t.append(sp_)
                    
            # picname = picspath + '/spectra_nT_%d_%d_%d_%d.png'%(nmin,nmax,nT,n_)
            # print 'Saving ', picname
            # fig = plt.figure(num=None, figsize=(10, 10), tight_layout = True)
            # summ = spectraT[n_]
           
            # axe = utils.create_axis(ne1, de*factor_ne, 0)
            # axt = utils.create_axis(nt1, dt*factor_nt, tmin)
            # axel = fig.add_subplot(1,1,1)
            # axel.imshow(summ, aspect = 'auto',  extent = [tmin, tmax, emin, emax], origin = 'lower', norm=clr.LogNorm(summ.max()*1e-6, summ.max()))
            # axel.set_ylabel('W, GeV')
            # axel.set_xlabel('$\\theta$')
            # axel.set_xlim([0,90])
            # axel.set_ylim([0,3])
            # plt.savefig(picname)
            # plt.close()
        if normalize:
            picname = picspath + '/spectra_nT_time_norm_%d_%d.png' % (nmin,nmax)
        else:
            picname = picspath + '/spectra_nT_time_%d_%d.png' % (nmin,nmax)
        print 'Saving ', picname
        fig = plt.figure(num=None, figsize=(10, 10), tight_layout = True)
        fontsize = 16
        mpl.rcParams.update({'font.size': fontsize})
        axe = utils.create_axis(nT, 1, 0)
        axt = utils.create_axis(ne1, de*factor_ne, 0)
        axel = fig.add_subplot(1,1,1)
        mmax = max([sp_.max() for sp_ in sp_t])
        axel.imshow(sp_t, aspect = 'auto',  extent = [0, emax, 0, nT], origin = 'lower', norm=clr.LogNorm(mmax*1e-3, mmax))
        axel.set_xlabel('W, GeV')
        axel.set_ylabel('Iteration')
        axel.set_ylim([0,nT])
        axel.set_xlim([0,3])
        plt.savefig(picname, dpi=512)
        plt.close()
        sp_en = np.sum(sp_t, axis = 0)/len(sp_t)
        print np.sum(sp_en)*de*factor_ne
        fig = plt.figure(num=None, figsize=(10, 10), tight_layout = True)
        axel = fig.add_subplot(1,1,1)
        axel.plot(axt, sp_en)
        axel.set_yscale('log')
        plt.show()
        fname = 'spectra_nT_time_norm_%d_%d.dat' % (nmin,nmax)
        f = open(fname, 'w')
        for i in range(len(axt)):
            f.write('%lf %le\n'%(axt[i], sp_en[i]))
        f.close()
        
        tau = np.zeros(ne1)
        tau1 = np.zeros(ne1)
        cm = np.zeros(ne1)
        fmax = np.zeros(ne1)
        ratio = np.zeros(ne1)
        t1=0
        t2=0
        for j in range(ne1):
            sumt_ = 0.
            sum_ = 0.
            t_ = 0.
            mmax = 0.
            timeline = np.zeros(len(sp_t))
            imax = 0
            mmax = 0
            mmin = 1e9
            for i in range(len(sp_t)):
                timeline[i] = sp_t[i][j]
                if timeline[i] < mmin:
                    mmin = timeline[i]
            for i in range(10,len(sp_t)-10):
                if timeline[i] > mmax:
                    mmax = timeline[i]
                    imax = i
                
            isp = len(sp_t)
            print imax
            for i in range(len(sp_t)):
                sumt_ += timeline[i]*i*dtt
                sum_ += timeline[i]
                if sp_t[i][j] > fmax[j]:
                    fmax[j] = timeline[i]
            t2 = (isp-1)*dtt
            if mmax == 0:
                t2 = 0
            boundary = mmin + 0.5*(mmax-mmin)
            for i in range(isp-imax):
                if timeline[imax+i-2] > boundary and timeline[imax+i-1] < boundary:
                    t2 = (i+imax+1)*dtt
                    break
            t1 = 0        
            for i in range(imax):
                if timeline[imax-1-i] < boundary and timeline[imax-i] > boundary:
                    t1 = (imax-i-1)*dtt
                    break
            if t2 > t1:
                tau1[j] = t2-t1
            if imax == isp-1:
                tau1[j] = (isp-1)*dtt
            if mmax > 0:
                ratio[j] = mmin/mmax
                            
            # fig = plt.figure()
            # ax = fig.add_subplot(1,1,1)
            # ax.plot(timeline)
            # ax.plot([t1/dtt,t1/dtt], [0, max(timeline)], 'b')
            # ax.plot([t2/dtt,t2/dtt], [0, max(timeline)], 'g')
            # ax.plot([imax,imax], [0, max(timeline)], 'r:')
            # plt.show()
            # plt.close()
            if sum_ != 0:    
                t_ = sumt_/sum_
            cm[j] = t_
            sumt2_ = 0
            for i in range(len(sp_t)):
                sumt2_ += (i*dtt - t_)*(i*dtt - t_)*sp_t[i][j]
            if sum_ != 0:
                tau[j] = math.sqrt(sumt2_/sum_) 
        print tau
        if normalize:
            picname = picspath + '/spectra_nT_norm_tau_%d_%d_%02d.png' % (nmin,nmax,factor_ne)
        else:
            picname = picspath + '/spectra_nT_tau_%d_%d.png' % (nmin,nmax)
        print 'Saving ', picname
        fig = plt.figure(num=None, figsize=(10, 5), tight_layout = True)
        ax = fig.add_subplot(1,1,1)
        axise = utils.create_axis(ne1, de*factor_ne, 0)
        ax1_ = []
        tau1_ = []
        rat_ = []
        for i in range(len(axise)):
            if tau1[i] < 1.5 and tau1[i] > 0:
                ax1_.append(axise[i])
                tau1_.append(tau1[i]*1000)
                rat_.append(ratio[i])
                ilast = i
        #ax1_ = ax1_[:-2]
        #tau1_ = tau1_[:-2]
        #rat_ = rat_[:-2]
        tau1_smooth = np.poly1d(np.polyfit(np.array(ax1_), np.array(tau1_), 4))
        #ax.plot(axise, tau, 'bx')
        ax.plot(ax1_, tau1_, 'b^')
        t, = ax.plot(ax1_, tau1_smooth(ax1_), 'b-', dashes = [6,2])
        ax.plot([0, axise[-1]], [1000,1000], 'k:')
        ax.set_xlim([0,3.5])
        xticks = [0,0.5,1,1.5,2,2.5,3,3.5]
        xticklabels = ['$%.1f$' %(x) for x in xticks]
        yticks = [200,400,800,600,1000]
        yticklabels = ['$%d$' %(x) for x in yticks]
        ax.set_ylim([0,1100])
        ax.set_xticks(xticks)
        ax.set_xticklabels(xticklabels)
        ax.set_yticks(yticks)
        ax.set_yticklabels(yticklabels)
        ax.set_ylabel('$\\tau, as$')
        ax.set_xlabel('$\\varepsilon_\gamma, GeV$')
        if normalize:
            filename = 'tau_norm_%d_%d.txt' % (nmin,nmax)
        else:
            filename = 'tau_%d_%d.txt' % (nmin,nmax)
        f = open(filename, 'w')
        for i in range(len(ax1_)):
            f.write('%lf %lf\n'%(ax1_[i], tau1_[i]))
        f.close()
        ax2 = ax.twinx()
        rat_smooth = np.poly1d(np.polyfit(np.array(ax1_), np.array(rat_), 5))
        ax2.plot(ax1_, rat_, 'ro')
        r, = ax2.plot(ax1_, rat_smooth(ax1_), 'r-', dashes = [6,2,2,2])
        #ax2.set_yscale('log')
        ax2.set_xlim([0,3.5])
        ax2.set_ylim([0,1.1])
        yticks = [0.2,0.4,0.6,0.8,1]
        yticklabels = ['$%.1f$' %(x) for x in yticks]
        ax2.set_yticks(yticks)
        ax2.set_yticklabels(yticklabels)
        ax2.set_xlabel('$\\varepsilon_\gamma, GeV$')
        ax2.set_ylabel('$r_c$')
        ax3 = ax.twinx()
        ax3.spines["left"].set_position(("axes", -0.15))
        make_patch_spines_invisible(ax3)
        # Second, show the right spine.
        ax3.spines["left"].set_visible(True)
        ax3.yaxis.set_label_position('left')
        ax3.yaxis.set_ticks_position('left')
        f, = ax3.plot(axise, fmax, 'g')
        ax3.set_xlim([0,3.5])
        ax3.set_ylim([1e-3,3])
        ax3.set_yscale('log')
#        yticks = [1,1e-1,1e-2,1e-3,1e-4,1e-5]
#        yticklabels = ['$1$', '$10^{-1}$','$10^{-2}$','$10^{-3}$','$10^{-4}$','$10^{-5}$']
        yticks = [1,1e-1,1e-2,1e-3,1e-4,1e-5]
        yticklabels = ['$1$', '$10^{-1}$','$10^{-2}$','$10^{-3}$','$10^{-4}$','$10^{-5}$']
        ax3.set_yticks(yticks)
        ax3.set_yticklabels(yticklabels)
        ax3.set_ylabel('$P\'_{\\varepsilon}$')
        plt.legend([t,f,r], ['$\\tau$', '$P\'_\\varepsilon$', '$r_c$'], loc = 'lower left', frameon=False)
        plt.savefig(picname, dpi=512)
        plt.close()

if __name__ == '__main__':
    main()


