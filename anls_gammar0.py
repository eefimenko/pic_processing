#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import sys
import math
import utils
import numpy as np

def profile(array, de):
    p1 = []
    n = len(array)
    m = len(array[0])
    av_num = m/4
    for i in range(n):
        s = 0
        for j in range(av_num):
            s += array[i][j]
        s /= av_num
        p1.append(s)
    s = sum(p1)
    av = 0
    imax = 0
    max_g = 0. 
    for i in range(len(p1)):
        if p1[i] > max_g:
            max_g = p1[i]
            imax = i
        p1[i] /= s
        av += (i+0.5)*de*p1[i]
    gmax = (imax+0.5)*de
    print av, gmax
    factor = 10
    n1 = n/factor
    p11 = [0]*n1
    for i in range(n1):
        for j in range(factor):
            p11[i] += p1[i*factor + j]
           
    return p11, av, gmax

def create_axis(n,step,x0=0):
    axis = []
    for i in range(n):
        axis.append(i*step + x0)
    return axis

def main():

    config = utils.get_config("ParsedInput.txt")
    ppath = 'data/ElGammaR/'
    ezpath = 'data/E2z'
    bzpath = 'data/B2z'
    picspath = 'pics'
    path = './'
    delta = 1
    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(ppath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(ppath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax) + ' files'
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])

    wl = float(config['Wavelength'])
    nx1 = int(config['ElGammaR.SetMatrixSize_0'])
    ny1 = int(config['ElGammaR.SetMatrixSize_1'])
    zmin = float(config['ElGammaR.SetBounds_0'])*1e4
    zmax = float(config['ElGammaR.SetBounds_1'])*1e4
    Emin = float(config['ElGammaR.SetBounds_2'])
    Emax = float(config['ElGammaR.SetBounds_3'])
        
    de = (Emax - Emin)/ny1
    factor = 10
    gaxis = create_axis(ny1/factor,de*factor,Emin)
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])

    T = 2 * math.pi/omega*1e15
    
    step = x0*y0*1e15/T
    nT = int(1./step)
    n0 = 0
    nf = 0
    i0 = 0
    i_f = nmax
    dmy = int(0.35*wl/dy)
    coeff = 10./(3.3e11*3.3e11)*0.97399*(wl/0.8e-4)*(wl/0.8e-4)
    peakpower = float(config['PeakPower'])*1e-7*1e-15
    print dmy
    print 'nt' + str(nT)
    p1 = []
    m1 = []
    av_t = []
    gmax_t = []
    ez_t = []
    for i in range(nmin,nmax,delta):
        pfield = utils.bo_file_load(path+ppath,i,nx1,ny1,transpose=1,verbose=1)
        ez = utils.bo_file_load(path+ezpath,i,nx,ny)
        bz = utils.bo_file_load(path+bzpath,i,nx,ny)
        ezv = ez[nx/2][ny/2]
        bzv = bz[nx/2][ny/2-dmy]*1.53
        tmp = (ezv*ezv + bzv*bzv)*coeff
        prof, av, gmax = profile(pfield,de)
        p1.append(prof)
        av_t.append(av)
        gmax_t.append(gmax)
        ez_t.append(ezv)
        if n0 == 0 and tmp > 0.993*peakpower:
            n0 = 1.
            i_0 = i
     
        if n0 != 0 and nf == 0 and tmp < 0.993*peakpower:
            nf = 1.
            i_f = i
            print ' iter = ' + str(i_f)
    axis = create_axis(nmax-nmin,step,nmin*step)
    value = [i_f*step]*(ny1/factor)
    fig = plt.figure(num=None)
    plt.rc('text', usetex=True)
    ax = fig.add_subplot(2,1,1)
    maxp1 = np.max(p1)
    ax.imshow(p1, extent=[Emin, Emax, nmin*step, (nmax-1)*step], aspect = 'auto', norm=clr.LogNorm(), vmin = maxp1*1e-3,vmax=maxp1, origin = 'lower')
    ax.set_xlim([0,8000])
#    a1 = np.array(gaxis)
#    a2 = np.array(axis)
#    surf = np.array(p1)
#    ax.pcolor(a1,a2,surf)
    ax.autoscale(False)
    ax.plot(av_t, axis)
    ax.plot(gmax_t, axis)
    ax.plot(gaxis, value, 'r')
    ax.set_ylabel('t, T')
    ax.set_xlabel('$\gamma$')
#    ax.set_xscale('log')
#    ax.set_xlim([Emin,Emax])
#    ax.set_ylim([nmin*step, (nmax-1)*step])
    ax = fig.add_subplot(2,1,2)
    ax.plot(axis, av_t, label = 'av')
    ax.plot(axis, gmax_t, label = 'max')
    ax.plot(value, gaxis, 'r')
    ax.set_xlabel('t, T')
    ax.set_ylabel('$\gamma$')
    ax.set_ylim([0, max(max(gmax_t), max(av_t))])
    ax.legend(loc='lower left')
    ax1 = ax.twinx()
    ax1.plot(axis, ez_t, 'k', label = 'Field')
    ax1.set_ylabel('El field')
    ax1.legend(loc='upper left')
    
#    plt.close()
    plt.show()

if __name__ == '__main__':
    main()
