#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
from matplotlib import cm as cm
import numpy as np
import math
import utils
import sys

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def find_max(file, mult = 1.):
    max = 0
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]
    for p in array:
        if p > max:
            max = p
    return max

def find_max_from_all_fields(expath, eypath, ezpath, bxpath, bypath, bzpath, i, mult = 1.):
    max = 0
    name = expath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    name = eypath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    name = ezpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    return max

def find_max_from_all_particles(xpath, ypath, zpath, i, mult = 1.):
    max = 0
    name = xpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    name = ypath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    name = zpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    return max
    
    

def create_subplot(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
    name = path + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny, mult)
    maxe = max([max(row) for row in field])

    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, norm=clr.LogNorm())
    ax.text(-1.5, 4.5, 'max=%.1le cm$^{-3}$' % (maxe))
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
    plt.colorbar(surf,  orientation  = 'vertical')
    return surf

def create_subplot_f(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max, xl, yl, color, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
    name = path + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny, mult)
    maxe = max([max(row) for row in field])

    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color)
    plt.colorbar(surf, orientation  = 'vertical')
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
    return surf

def pulse_value(i,step,amp,tp,delay):
    ans = []
    tau = tp/1.1437
#    delay = 23.1099984185576458468467817
    t = i*step-delay
    if t > 0 and t < math.pi * tau:
        tmp = math.sin(t/tau)
        f = amp*tmp*tmp
    else:
        f = 0.
    return f

def create_cmap(name):
    cmap = cm.get_cmap(name)
    colors = [('white')] + [(cmap(i)) for i in xrange(1,256)]
    cmap = mp.colors.LinearSegmentedColormap.from_list('new_map', colors, N=256)
    return cmap

def main():
#    plt.rc('text', usetex=True)
    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nixpath = 'data/Proton2Dx'
    niypath = 'data/Proton2Dy'
    nizpath = 'data/Proton2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nphxpath = 'data/Photon2Dx'
    nphypath = 'data/Photon2Dy'
    nphzpath = 'data/Photon2Dz'
    picspath = 'pics'

    config = utils.get_config("ParsedInput.txt")

    BOIterationPass = float(config['BOIterationPass'])
    dt = float(config['TimeStep'])*1e15*BOIterationPass #fs
    maxpower = float(config['PeakPower'])*1e-7*1e-15 #PW
    duration = float(config['Duration'])*1e15 #fs
    delay = float(config['delay'])*1e15 #fs
    ch = 4.8032e-10
    mass = 9.10938e-28
    wl = float(config['Omega'])
    c = 3e10
    print "Power = " + str(maxpower) + 'PW'
    print "dt = " + str(dt) + 'fs'
    print "duration = " + str(duration)
    print "delay = " + str(delay)
    
    Xmax = 4
    Xmin = -4
    Ymax = 4
    Ymin = -4
    nx = 512
    ny = 512
    step = 8./nx

    mult = 1/(2.*step*step*step*1e-12)

    num = 70
    nmin = 70
    nmax = 80

    er = mass*wl*c/ch
    print er
    print 'Found ' + str(nmax) + ' files'

    maxf = 0
    maxe = 0
    maxi = 0
    maxp = 0
    maxph = 0
    print 'Reading files'

    for i in range(0,0):
        if i != 6200:
            print "\r%.0f %% done" % (float(i-nmin)/nmax*100),
            sys.stdout.flush()        
            tmpf = find_max_from_all_fields(expath, eypath, ezpath, bxpath, bypath, bzpath, i, 1./er)
            if tmpf > maxf:
                maxf = tmpf
            tmpe = find_max_from_all_particles(nexpath, neypath, nezpath, i, mult)
            if tmpe > maxe:
                maxe = tmpe
            tmpi = find_max_from_all_particles(nixpath, niypath, nizpath, i, mult)
            if tmpi > maxi:
                maxi = tmpi
            tmpp = find_max_from_all_particles(npxpath, npypath, npzpath, i, mult)
            if tmpp > maxp:
                maxp = tmpp
            tmpph = find_max_from_all_particles(nphxpath, nphypath, nphzpath, i, mult)
            if tmpph > maxph:
                maxph = tmpph
#    print maxf, maxe, maxi, maxp, maxph
    if maxi > maxe:
        maxe = maxi

    if maxp > maxe:
        maxe = maxp
#    maxph += 1
#    maxf = 2.7e11
    fmin = 0
    fmax = 0
#    cmin = maxe*1e-5
    maxe = 0
    cmax = maxe
    cmin = maxe*1e-6
    cimax = maxi
    cimin = maxi*1e-6
#    cmax = 4.5e25
    phmin = maxph*1e-6
    phmax = maxph
    rmap = create_cmap('Reds')
    gmap = create_cmap('Greens')
    bmap = create_cmap('Blues')
    pmap = create_cmap('Purples')
#    cmap = cm.get_cmap('Reds')
#    colors = [('white')] + [(cmap(i)) for i in xrange(1,256)]
#    rmap = mp.colors.LinearSegmentedColormap.from_list('new_map', colors, N=256)
#    colors = [('white')] + [(cmap(i)) for i in xrange(1,256)]
#    rmap = mp.colors.LinearSegmentedColormap.from_list('new_map', colors, N=256)
    
    for i in range(nmin,nmax):
        if i != 6200:
            print "\rSaving s%06d.png, %.0f %% done" % (i,float(i)/nmax*100),
            sys.stdout.flush()

            fig = plt.figure(num=None, figsize=(20, 10), dpi=256)
            mp.rcParams.update({'font.size': 16})
        
            s = create_subplot_f(fig,i,expath,nx,ny,2,4,1, Xmin, Xmax, Ymin, Ymax, 0, fmax, 'z', 'y', rmap, 1./er)
            s = create_subplot_f(fig,i,ezpath,nx,ny,2,4,5, Xmin, Xmax, Ymin, Ymax, 0, fmax, 'y', 'x', rmap, 1./er)
            
            s = create_subplot(fig,i,nexpath,nx,ny,2,4,2, Xmin, Xmax, Ymin, Ymax, cmin, cmax, 'z', 'y', gmap, mult)
            s = create_subplot(fig,i,nezpath,nx,ny,2,4,6, Xmin, Xmax, Ymin, Ymax, cmin, cmax, 'y', 'x', gmap, mult)
            
#            s = create_subplot(fig,i,nixpath,nx,ny,2,4,3, Xmin, Xmax, Ymin, Ymax, cimin, cimax, 'z', 'y','Greens', mult)
#            s = create_subplot(fig,i,nizpath,nx,ny,2,4,8, Xmin, Xmax, Ymin, Ymax, cimin, cimax, 'y', 'x', 'Greens', mult)
            
            s = create_subplot(fig,i,npxpath,nx,ny,2,4,3, Xmin, Xmax, Ymin, Ymax, cmin, cmax, 'z', 'y', bmap, mult)
            s = create_subplot(fig,i,npzpath,nx,ny,2,4,7, Xmin, Xmax, Ymin, Ymax, cmin, cmax, 'y', 'x', bmap, mult)
            
            s = create_subplot(fig,i,nphxpath,nx,ny,2,4,4, Xmin, Xmax, Ymin, Ymax, phmin, phmax, 'z', 'y', pmap, mult)
            s = create_subplot(fig,i,nphzpath,nx,ny,2,4,8, Xmin, Xmax, Ymin, Ymax, phmin, phmax, 'y', 'x', pmap, mult)
            
            f = pulse_value(i, dt, 1., duration, delay)
            power = f*f*maxpower
            
#            plt.figtext(0.2,0.93,str('Power in linear centre =%.3e PW' % (power)) , fontsize = 10)
            plt.figtext(0.15,0.93,str('Electric field') , fontsize = 18)
            plt.figtext(0.35,0.93,str('Electron density') , fontsize = 18)
            plt.figtext(0.55,0.93,str('Positron density') , fontsize = 18)
            plt.figtext(0.75,0.93,str('Photon density') , fontsize = 18)
            picname = picspath + '/' + "lp%06d.png" % (i,)
            
            plt.savefig(picname)
            plt.close()
#        plt.show()
if __name__ == '__main__':
    main()
