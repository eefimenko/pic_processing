#!/usr/bin/python
import matplotlib as mp
mp.use("Agg")
import matplotlib.pyplot as plt
import sys
import os, os.path
import math
import numpy as np
from tqdm import *

def create_axis(n,step):
    axis = np.zeros(n)
    for i in range(n):
        axis[i] = i*step
    return axis

def get_config(file):
    searchfile = open(file, "r")
    myvars = {}
    for line in searchfile:
        if "Setting" in line and ("variable" in line or "string" in line) and '=' in line:
            tmp, var = line.partition("=")[::2]
            rem, name = tmp.partition(":")[::2]
            myvars[name.strip()] = var.strip()
    searchfile.close()
    return myvars

def num_files(dirname):
    num = len([name for name in os.listdir(dirname) if os.path.isfile(dirname + '/' +name)])
    return num

def fromfile(file,nx=1,ny=1, ftype='txt', verbose = 0, dtype='f'):
    if 'txt' in ftype:
        sep = ' '
        f = open(file, 'r')
    else:
        sep = ''
        f = open(file, 'rb')
    array = np.fromfile(f, dtype=dtype, count=-1, sep=sep)
#    print array.shape, nx,ny
    if ny != 1:
        array = np.reshape(array, (nx,ny), order = 'F')
    return array

def bo_file_load(path,iteration,nx=1,ny=1,ftype='txt',fmt='%06d',transpose = 0, verbose = 0):
    name = path + '/' + fmt % (iteration) + '.' + ftype
    if verbose != 0:
        print name
    if os.path.exists(name):
        field = fromfile(name,nx,ny,ftype)
    else:
        field = None
    if transpose == 1:
        field = np.transpose(field)
    return field

def main():
    n = len(sys.argv) - 1
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    bpath = 'data/B2z'
    nepath = 'data/NeTrap'
    picspath = 'pics'
    
    for i_ in range(n):
    	path = sys.argv[i_+1]
        label = path
        print path 
        config = get_config(path + '/' + "ParsedInput.txt")
        BOIterationPass = float(config['BOIterationPass'])
        step = float(config['TimeStep'])*1e15*BOIterationPass
        nmax = num_files(path + '/' + nepath)
        print nmax
        Ne = np.zeros(nmax)
        Bz = np.zeros(nmax)
        
        for i in tqdm(range(nmax)):
            nename = path + '/' + nepath + '/' + "%06d.txt" % (i,)
            nbzname = path + '/' + bpath + '/' + "%06d.txt" % (i,)
            e = np.sum(bo_file_load(path + '/' + nepath, i))
            p = np.amax(fromfile(nbzname))
            Ne[i] = e
            Bz[i] = p
        axis = create_axis(nmax, step)
        pn, = ax1.plot(axis, Ne, label = label)
        pb, = ax2.plot(axis, Bz, label = label, dashes = [2,2])

    ax1.legend(loc='upper right')
    ax1.set_yscale('log')
    ax1.set_xlim(0., 45.)
    ax2.set_ylim(0., 1e12)
    ax1.set_ylabel('$N_e$')
    ax2.set_ylabel('$B$, CGSE')
    plt.savefig(picspath+ '/trapped_cmp.png')
    plt.close()
if __name__ == '__main__':
    main()
