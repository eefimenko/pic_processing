#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import math
from utils import *
import sys
import os

def summ_over_z(array):
    res = [0]*len(array)
    for j in range(len(array[0])):
        for i in range(len(array)):
            res[i] += array[i][j]
    return res

def average_gamma(array, dg):
    n = sum(array)
    print 'Av gamma', len(array), n
    s = 0
    for i in range(len(array)):
        s += array[i]*(i+0.5)*dg
    if n != 0:
        return s/n
    else:
        return 0

def average(array, j, n):
    if j < n/2 or j > len(array)-1-n/2:
        return 0
    return sum(array[j-n/2:j+n/2])/n
    
def find_width(x,array):
    n = len(array)
    m = 0
    imax = 0
    for i in range(n):
        if array[i] > m:
            m = array[i]
            imax = i
    n_ = 5
    for j in range(imax):
        if average(array, imax-j, n_) < 0.5*m:
            x0 = x[imax-j]
            break
    for j in range(n-imax):
        if average(array,imax + j,n_) < 0.5*m:
            x1 = x[imax+j]
            break
    return x0,x1,m

def distr_width(array, xmin, dx):
    n = len(array)
    x = [i*dx + xmin for i in range(n)]
    arr_s = savitzky_golay(array, 3, 1)
    x0, x1,m = find_width(x,array)
    x0_s, x1_s, m_s = find_width(x,arr_s)      
    s2 = x1 - x0
#    fig = plt.figure()
#    ax = fig.add_subplot(1,1,1)
    
#    ax.plot(x,array)
#    ax.plot(x,arr_s)
#    ax.plot([x0, x0],[0, m], 'r')
#    ax.plot([x1, x1],[0, m], 'r')
#    ax.plot([x0_s, x0_s],[0, m], 'g')
#    ax.plot([x1_s, x1_s],[0, m], 'g')
#    ax.set_xlim([-0.1e-4, 0.1e-4])
#    plt.show()
    return s2

def main():
    picspath = 'pics'
    config = get_config("ParsedInput.txt")
    wl = float(config['Wavelength'])
    Xmax = float(config['X_Max']) #mkm to wavelength
    Xmin = float(config['X_Min']) #mkm to wavelength
    Ymax = float(config['Y_Max']) #mkm to wavelength
    Ymin = float(config['Y_Min']) #mkm to wavelength
    Zmax = float(config['Z_Max']) #mkm to wavelength
    Zmin = float(config['Z_Min']) #mkm to wavelength

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)
    if 'resize' in config.keys():
        factor = int(config['resize'])
    else:
        factor = 1
    if 'rescale' in config.keys():
        rescale = int(config['rescale'])
    else:
        rescale = 1
    print factor
    path = os.getcwd()
    Xmax /= rescale
    Xmin /= rescale
    Ymax /= rescale
    Ymin /= rescale
    Zmax /= rescale
    Zmin /= rescale
        
    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz

    dx1 = dx*1e4
    if os.path.exists(ppath):
        nx1 = int(config['ElPhase.SetMatrixSize_0'])
        ny1 = int(config['ElPhase.SetMatrixSize_1'])
        zmin = float(config['ElPhase.SetBounds_0'])*1e4
        zmax = float(config['ElPhase.SetBounds_1'])*1e4
        Emin = float(config['ElPhase.SetBounds_2'])
        Emax = float(config['ElPhase.SetBounds_3'])
        dg = (Emax-Emin)/ny1
    
#    Mmin = float(config['ElMomentum.SetBounds_2'])
#    Mmax = float(config['ElMomentum.SetBounds_3'])
    BOIterationPass = float(config['BOIterationPass'])
    ftype = str(config['BODataFormat'])
    timestep = float(config['TimeStep'])
    dt = float(config['TimeStep'])*1e15*BOIterationPass
    

    delta = 1

    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = num_files(eypath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = num_files(eypath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    xmin = 0.04
    dirname = os.path.split(os.getcwd())[1]
    if 'r' not in dirname:
        dv = 2.*dx*dy*dz*factor
    else:
        dv = 2.*dx*dy*dz*rescale

    axis = create_axis(nmax-nmin, dt, 0)

    e2max = []
    b2max = []
    nemax = []
    jzmax = []
    gammaav = []
    wp = []
    ndtwp = []
    ndxne = []
    dxne = []
    print ftype
    ymin__ = ny/4
    ymax__ = 3*ny/4
    zmin__ = nz/4
    zmax__ = 3*nz/4
    for i in range(nmin, nmax, delta):
        fielde2x = bo_file_load(eypath,i,ny,nz,ftype=ftype)
        e2max.append(np.amax(fielde2x[ymin__:ymax__,zmin__:zmax__]))
        fieldb2x = bo_file_load(bypath,i,ny,nz,ftype=ftype)
        b2max.append(np.amax(fieldb2x[ymin__:ymax__,zmin__:zmax__]))
        nefield = bo_file_load(nezpath,i,ny,nz,ftype=ftype)/dv
        nemax_ = np.amax(nefield)
        nemax.append(nemax_)
        res = np.sum(nefield, axis = 1)
        jzfield = bo_file_load(jz_zpath,i,ny,nz,ftype=ftype)
        jzmax.append(max(np.amax(jzfield), abs(np.amin(jzfield))))
        w = distr_width(res,Xmin,dx)        
        print i, w/dx
        ndxne.append(w/dx)
        dxne.append(w)
        if os.path.exists(ppath):
            pfield = bo_file_load(ppath,i,nx1,ny1,ftype=ftype)
            res = np.sum(pfield, axis = 0)          
            
            print 'Max = ', max(res), max2d(pfield)
            gammaav.append(average_gamma(res, dg))
            wp_ = math.sqrt(4*math.pi*ElectronCharge*ElectronCharge*nemax_/ElectronMass)
            wp.append(wp_)
            ndtwp.append(2*math.pi/wp_/timestep)
        else:
            gammaav.append(0)
            wp.append(0)
            ndtwp.append(0)

    fig = plt.figure()
    ax = fig.add_subplot(4,1,1)
    e, = ax.plot(axis, e2max)
    
    ax.set_yscale('log')
    ay = ax.twinx()
    ne, = ay.plot(axis, nemax, 'r')
    ay.set_yscale('log')
    plt.legend([e,ne],['|E|','Ne'], loc = 'upper left')
    ax = fig.add_subplot(4,1,2)
    g, = ax.plot(axis, gammaav)
    ay = ax.twinx()
    wp_, = ay.plot(axis, ndtwp, 'r')
    plt.legend([g,wp_],['av gamma','Tp/dt'], loc = 'upper left')
    ax = fig.add_subplot(4,1,3)
    b, = ax.plot(axis, b2max)
    ay = ax.twinx()
    n_, = ay.plot(axis, ndxne, 'r')
    plt.legend([b, n_],['|B|', 'N dx/ne'], loc = 'upper left')
    ax = fig.add_subplot(4,1,4)
    b, = ax.plot(axis, jzmax)
    plt.tight_layout()
    plt.show()
    f = open('analyse_inst.dat', 'w')
    for i in range(len(nemax)):
        f.write('%le %le %le %le %le %le %le %le %le %le\n' % (axis[i], e2max[i], b2max[i], nemax[i], gammaav[i], wp[i], ndtwp[i], ndxne[i], dxne[i], jzmax[i]))
    f.close()
    
if __name__ == '__main__':
    main()
