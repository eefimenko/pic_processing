#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import shutil
import os
import scipy.signal.signaltools as sigtool
import numpy as np
import copy
from matplotlib import gridspec
import matplotlib.colors as clr
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

def cvalue(array):
    s = array.shape
    dx = 2
    dy = 2
    xmin = s[0]/2-dx
    xmax = s[0]/2+dx
    ymin = s[1]/2-dy
    ymax = s[1]/2+dy
    
    return np.amax(array[xmin:xmax, ymin:ymax])

def main():
    fs = 24
    picspath = '.'
    dirs = ['ideal_36PW',
            '12beams_0.4_36PW',
            '12beams_0.2_36PW',
            '6beams_0.4_36PW'
    ]
    
    labels = dirs
    
    fig1 = plt.figure(num=None, figsize = (10, 10))
    ax1 = fig1.add_subplot(2,2,1)
    ax2 = fig1.add_subplot(2,2,2)
    ax3 = fig1.add_subplot(2,2,3)
    ax4 = fig1.add_subplot(2,2,4)
    
    
    mp.rcParams.update({'font.size': fs})
    
    #t_linear_start = 9
    #t_linear_end = 13
    fontsize = 12

     
    for i in range(len(dirs)):
        path = dirs[i]
        print(path)
        config = utils.get_config(path + "/ParsedInput.txt")
        x0 = int(config['BOIterationPass'])
        y0 = float(config['TimeStep']) 
        omega = float(config['Omega'])
        ppw = int(config['PeakPowerPW'])
        ev = float(config['eV'])
        dx = float(config['Step_X'])
        dy = float(config['Step_Y'])
        dz = float(config['Step_Z'])
        nx = int(config['MatrixSize_X'])
        ny = int(config['MatrixSize_Y'])
        nz = int(config['MatrixSize_Z'])
        r = float(config['R'])*1e4
        ne_ = float(config['Ne'])
        dv = 2*dx*dy*dz 
        T = 2 * math.pi/omega*1e15
        num = int(T/(x0*y0*1e15))
        dt = (x0*y0*1e15)/T
        dt__ = dt
        wl = float(config['Wavelength'])
        #ncr = electronMass * omega * omega/(8. * math.pi * electronCharge * electronCharge)
        archive = os.path.join(path, 'BasicOutput', 'data.zip')
        if path == '27pw_default/27pw_default':
            archive = None
            
#        np_t = utils.ts(path, utils.npzpath, name='npz',
#                        tstype='max', shape = (nx,ny), verbose=False, archive=archive)/dv
        ne_t = utils.ts(path, 'data/Electron_xy', name='nez',
                        tstype='max', shape = (nx,ny), verbose=False, ftype='bin', archive=archive)/dv
        bz_t = utils.ts(path, 'data/Bx_xy', name='bz',
                        tstype='max', shape = (nx,ny), verbose=False, ftype='bin', archive=archive)
        ez_t = utils.ts(path, 'data/Ez_xy', name='ez',
                        tstype='max', shape = (nx,ny), verbose=False, ftype='bin', archive=archive)
        #netr_t = utils.ts(path, utils.netpath, name='ne_trapped',
        #                  tstype='sum', verbose=False, archive=archive)
#        npostr_t = utils.ts(path, utils.npostpath, name='npos_trapped',
#                            tstype='sum', verbose=False, archive=archive)
        #label = '%d PW %g mkm %e cm-3' % (ppw, r, ne_)
        label = labels[i]
        
        axb_ = utils.create_axis(len(ez_t), dt__)
        lw = 1
#        
        f1, = ax1.plot(axb_, np.array(ez_t), linewidth = lw, label = label)
          
        axb_ = utils.create_axis(len(bz_t), dt__)
        f2, = ax2.plot(axb_, np.array(bz_t), linewidth = lw, label = label)
          
        axb_ = utils.create_axis(len(ne_t), dt__)
        
        f3, = ax3.plot(axb_, np.array(ne_t), linewidth = lw, label = label)
        #f3, = axins1.plot(axb_, np.array(ne_t), linewidth = lw)
          
        #axb_ = utils.create_axis(len(netr_t), dt__)
       
        #f4, = ax4.plot(axb_, np.array(netr_t), linewidth = lw, label = label)
        #f4, = axins.plot(axb_, np.array(netr_t), linewidth = lw)
        

        ax3.set_yscale('log')
        ax4.set_yscale('log')
        ax1.set_ylabel('|E/E$_0$| ')
        #ax1.set_ylim([0,3])
        #ax2.set_ylim([0,3])
        ax2.set_ylabel('|B/B$_0$| ')
        ax3.set_ylabel('n$_e$, cm$^{-3}$')
        ax4.set_ylabel('N$_{tr}$')

       

        ax1.set_xlabel('t/T')
        ax2.set_xlabel('t/T')
        ax3.set_xlabel('t/T')
        ax4.set_xlabel('t/T')

    #t_min = 3
    #t_max = 18
    #ax3.axvline(x = t_linear_start, linewidth = 0.7, color = 'gray')
    #ax3.axvline(x = t_linear_end, linewidth = 0.7, color = 'gray')
    #ax3.fill_between([t_linear_start, t_linear_end], 0, 1e30, facecolor='blue', alpha=0.2)
    #ax3.fill_between([t_linear_end, t_max], 0, 1e28, facecolor='red', alpha=0.2)
    #ax3.text(t_linear_start + 0.5, 5e13, 'Linear\nregime', fontsize = fontsize)
    #ax3.text(t_linear_end + 0.5, 5e13, 'Nonlinear\nregime', fontsize = fontsize)
    ax3.legend(loc = 'upper left', fontsize = fontsize-2, frameon = False)
    #ax3.text(0, 5e26, '(a)', fontsize = fontsize)
    
    #ax4.axvline(x = t_linear_start, linewidth = 0.7, color = 'gray')
    #ax4.axvline(x = t_linear_end, linewidth = 0.7, color = 'gray')
    #ax4.fill_between([t_linear_start, t_linear_end], 0, 1e30, facecolor='blue', alpha=0.2)
    #ax4.fill_between([t_linear_end, t_max], 0, 1e28, facecolor='red', alpha=0.2)
#    ax4.text(t_linear_start + 1, 5e2, 'Linear\nregime', fontsize = fontsize)
#    ax4.text(t_linear_end + 1, 5e2, 'Nonlinear\nregime', fontsize = fontsize)
    #ax4.legend(loc = 'upper left', fontsize = fontsize-2, frameon = False)
    #ax4.text(0, 5e11, '(b)', fontsize = fontsize)
    
    #ax1.set_xlim([t_min,t_max])
    #ax2.set_xlim([t_min,t_max])
    #ax3.set_xlim([t_min,t_max])
    #ax3.set_ylim([1e13, 1e28])
    #ax3.set_xticks([5, 9, 13, 17])
    #ax4.set_xlim([t_min,t_max])
    #ax4.set_ylim([1e2, 1e12])
    #ax4.set_yticks([1e3, 1e7, 1e11])
    #ax4.set_xticks([5, 9, 13, 17])
    '''axins1.set_xticks([])
    axins1.set_yticks([])
    axins1.set_yticklabels([])
    axins1.set_yticklabels([])
    axins.set_xticks([])
    axins.set_yticks([])
    axins.set_yticklabels([])
    axins.set_yticklabels([])'''
    
    name = 'xcels_compare'
    picname = picspath + '/' + name + ".png"
    #plt.legend(loc = 'upper left', fontsize = 10)
    fig1.tight_layout()
    fig1.savefig(picname, dpi = 256)
    print(picname)
    
   
if __name__ == '__main__':
    main()
