#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import math
from utils import *
import sys
import os

def summ_over_z(array):
    res = [0]*len(array)
    for j in range(len(array[0])):
        for i in range(len(array)):
            res[i] += array[i][j]
    return res

def average_gamma(array, dg):
    n = sum(array)
    print 'Av gamma', len(array), n
    s = 0
    for i in range(len(array)):
        s += array[i]*(i+0.5)*dg
    if n != 0:
        return s/n
    else:
        return 0

def distr_width(array, xmin, dx):
    s1 = sum(array)
    s2 = 0
    for i in range(len(array)):
        s2 += array[i]*(i*dx + xmin)*(i*dx+xmin)/s1
    return math.sqrt(s2)

def main():
    picspath = 'pics'
    num = len(sys.argv) - 1
    config = get_config(sys.argv[1] + "/ParsedInput.txt")
    wl = float(config['Wavelength'])
    Xmax = float(config['X_Max'])*1e4 #mkm to wavelength
    Xmin = float(config['X_Min'])*1e4 #mkm to wavelength
    Ymax = float(config['Y_Max'])*1e4 #mkm to wavelength
    Ymin = float(config['Y_Min'])*1e4 #mkm to wavelength
    Zmax = float(config['Z_Max'])*1e4 #mkm to wavelength
    Zmin = float(config['Z_Min'])*1e4 #mkm to wavelength

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])

    if os.path.exists(ppath):
        nx1 = int(config['ElPhase.SetMatrixSize_0'])
        ny1 = int(config['ElPhase.SetMatrixSize_1'])
        zmin = float(config['ElPhase.SetBounds_0'])*1e4
        zmax = float(config['ElPhase.SetBounds_1'])*1e4
        Emin = float(config['ElPhase.SetBounds_2'])
        Emax = float(config['ElPhase.SetBounds_3'])
        dg = (Emax-Emin)/ny1
    
#    Mmin = float(config['ElMomentum.SetBounds_2'])
#    Mmax = float(config['ElMomentum.SetBounds_3'])
    BOIterationPass = float(config['BOIterationPass'])
    ftype = str(config['BODataFormat'])
    timestep = float(config['TimeStep'])
    dt = float(config['TimeStep'])*1e15*BOIterationPass
    
    nmax = 100000000
    delta = 1
    for i in xrange(num):
        nmax_ = num_files(sys.argv[i+1] + '/' + expath)
        if nmax_ < nmax:
            nmax = nmax_
    nmin = 0        
    dv = 4.*dx*dy*dz
    axis = create_axis(nx, dx*1e4, Xmin)
    nmax = 40
    bound = [-0.04, 0.04]
    print ftype
    for i in range(nmin, nmax, delta):
        fig = plt.figure(figsize = (20,10))
        picname = sys.argv[1]+ '/pics/pinch_cmp_%d.png'%(i)
        for k in xrange(num):
            path = sys.argv[k+1]
            profe = None
            profb = None
            profne = None
            ae = fig.add_subplot(num,4,4*k+1)
            fielde2x = bo_file_load(path+ '/' + expath,i,ny,nz,ftype=ftype)
            if fielde2x != None:
                ae.imshow(fielde2x, extent = ([Xmin, Xmax, Zmin, Zmax]), aspect = 'auto')
                ae.set_ylim(bound)
                profe = fielde2x[:,0]
            ab = fig.add_subplot(num,4,4*k+2)
            fieldb2x = bo_file_load(path+ '/' + bxpath,i,ny,nz,ftype=ftype)
            if fieldb2x != None:
                ab.imshow(fieldb2x, extent = ([Xmin, Xmax, Zmin, Zmax]), aspect = 'auto')
                ab.set_ylim(bound)
                profb = fieldb2x[:,0]
            an = fig.add_subplot(num,4,4*k+3)
            nefield = bo_file_load(path+ '/' + nexpath,i,ny,nz,ftype=ftype,mult=1./dv) 
            if nefield != None:
                m = np.amax(nefield)
                an.imshow(nefield, extent = ([Xmin, Xmax, Zmin, Zmax]), aspect = 'auto', norm=clr.LogNorm(vmin=m*1e-3,vmax=m))
                profne = nefield[:,0]
                an.set_ylim(bound)
            ap = fig.add_subplot(num,4,4*k+4)
            apn = ap.twinx()
            if profe != None:
                e, = ap.plot(axis, profe, 'r')
            if profb != None:
                b, = ap.plot(axis, profb, 'g')
            if profne != None:
                ne, = apn.plot(axis, profne, 'b')
            ap.set_xlim(bound)
            apn.set_xlim(bound)
#            plt.legend([e,b,ne], ['Electric field', 'Magnetic field', 'Density'])
        plt.savefig(picname)
        plt.close()
#            res = np.sum(nefield, axis = 1)

#        print distr_width(res,Xmin,dx)/dx
#        ndxne.append(distr_width(res,Xmin,dx)/dx)
#        dxne.append(distr_width(res,Xmin,dx))
#        if os.path.exists(ppath):
#            pfield = bo_file_load(ppath,i,nx1,ny1)
#            res = np.sum(pfield, axis = 0)          
            
#            print 'Max = ', max(res), max2d(pfield)
#            gammaav.append(average_gamma(res, dg))
#            wp_ = math.sqrt(4*math.pi*ElectronCharge*ElectronCharge*nemax_/ElectronMass)
#            wp.append(wp_)
#            ndtwp.append(2*math.pi/wp_/timestep)
#        else:
#            gammaav.append(0)
#            wp.append(0)
#            ndtwp.append(0)

#    fig = plt.figure()
#    ax = fig.add_subplot(3,1,1)
#    e, = ax.plot(axis, e2max)
    
#    ax.set_yscale('log')
#    ay = ax.twinx()
#    ne, = ay.plot(axis, nemax, 'r')
#    ay.set_yscale('log')
#    plt.legend([e,ne],['|E|','Ne'], loc = 'upper left')
#    ax = fig.add_subplot(3,1,2)
#    g, = ax.plot(axis, gammaav)
#    ay = ax.twinx()
#    wp_, = ay.plot(axis, ndtwp, 'r')
#    plt.legend([g,wp_],['av gamma','Tp/dt'], loc = 'upper left')
#    ax = fig.add_subplot(3,1,3)
#    b, = ax.plot(axis, b2max)
#    ay = ax.twinx()
#    n_, = ay.plot(axis, ndxne, 'r')
#    plt.legend([b, n_],['|B|', 'N dx/ne'], loc = 'upper left')
#    plt.tight_layout()
#    plt.show()
#    f = open('analyse_inst.dat', 'w')
#    for i in range(len(nemax)):
#        f.write('%le %le %le %le %le %le %le %le %le\n' % (axis[i], e2max[i], b2max[i], nemax[i], gammaav[i], wp[i], ndtwp[i], ndxne[i], dxne[i]))
#    f.close()
    
if __name__ == '__main__':
    main()
