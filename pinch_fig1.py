#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np
import matplotlib as mp
from matplotlib import rcParams

rcParams.update({'figure.autolayout': True})
def main():
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
    power_p = []
    ezx_p = []
    bzx_p = []
    nex_p = []
    nemaxx_p = []
    ncrx_p = []
    jx_min_p = []
    jx_max_p = []
    powx_p = []
    pow1x_p = []
    maxx_p = []
    avx_p = []
    el_powx_p = []
    el_pow1x_p = []
    el_maxx_p = []
    el_avx_p = []
    picspath = 'pics/'
    
    f = open('stat_summary_el.txt', 'r')
    for line in f:
        tmp = line.split()
        power_p.append(float(tmp[0]))
        ezx_p.append(float(tmp[1])/1e11)
        bzx_p.append(float(tmp[2])/1e11)
        maxx_p.append(float(tmp[3]))
        avx_p.append(float(tmp[4])*1e3)
        ncrx_p.append(float(tmp[5]))
        nex_p.append(float(tmp[6])/1e26)
        powx_p.append(float(tmp[7]))
        pow1x_p.append(float(tmp[8]))
        jx_min_p.append(float(tmp[9]))
        jx_max_p.append(float(tmp[10]))
        el_maxx_p.append(float(tmp[11]))
        el_avx_p.append(float(tmp[12])*1e3)
        el_powx_p.append(float(tmp[13]))
        el_pow1x_p.append(float(tmp[14]))
        nemaxx_p.append(float(tmp[15])/1e26)
    f.close()
    nmax = 100
    dp = power_p[-1]/nmax
    p = np.zeros(nmax)
    e = np.zeros(nmax)
    b = np.zeros(nmax)
    for i in range(nmax):
        p[i] = dp*i
        e[i] = 3*math.sqrt(p[i]/10)
        b[i] = 0.653*3*math.sqrt(p[i]/10)

    thr_power = 7.25
    ep = 3*math.sqrt(thr_power/10)
    bp = 0.653*3*math.sqrt(thr_power/10)
    lfontsize = 12
    xticks = [10,15,20,25,30]
    xticklabels = ['$%d$'%(x) for x in xticks]
    fontsize = 16
    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    plt.minorticks_on()
    mp.rcParams.update({'font.size': fontsize})
        
    ax = fig.add_subplot(1,1,1)
#    ax.plot(power_p, ezx_p, 'r-o', label='E',fillstyle = 'none')
#    ax.plot(p, e, 'r:', label = 'E')
#    ax.plot([thr_power,power_p[-1]], [ep,ep], 'r--' , label = 'E')
    # ax.plot([thr_power,power_p[-1]], [ep,ep], 'r--')
    ax.plot(power_p, bzx_p, 'r', label='Nonlinear',fillstyle = 'none')
    ax.plot(p, b, 'b', label = 'Linear', dashes = [6,1])
#    ax.plot([thr_power,power_p[-1]], [bp,bp], 'b--', label = 'B - Threshold')
    ax.plot([20, 20], [0, 1.1*max(e)], 'k-', dashes = [2,2])
    # ax.plot([thr_power,power_p[-1]], [bp,bp], 'b--')
    ax.set_xlim([7,power_p[-1]+0.5])
    ax.set_ylim([0,4])
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    plt.legend(loc = 'lower left', fontsize = lfontsize, frameon = False)
    yticks = [1,2,3,4]
    ax.set_yticks(yticks)
    ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ax.set_xlabel('P [PW]')
    ax.set_ylabel('B [$\\times10^{11}$ G]')
    ax.text(3, 4, '$(d)$')
    picname = picspath + '/' + "pinch_fig1d.png"
    plt.savefig(picname, dpi=256)
    plt.close()
    
    
    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    plt.minorticks_on()  
    ax = fig.add_subplot(1,1,1)
    
    ax.plot(power_p, nemaxx_p, 'r', fillstyle = 'none')
    ax.plot([20, 20], [0, 1.1*max(nemaxx_p)], 'k-', dashes = [2,2])
    ax.set_xlim([7,power_p[-1]+0.5])
    ax.set_ylim([0,4])
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    yticks = [0,1,2,3,4]
    ax.set_yticks(yticks)
    ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ax.set_xlabel('P [PW]')
    ax.set_ylabel('n$_p$ [$\\times 10^{26}$ cm$^{-3}$]')
    ax.text(2.5, 4, '$(a)$')
    plt.legend(loc = 'upper left', fontsize = lfontsize, frameon = False)
    picname = picspath + '/' + "pinch_fig1a.png"
    plt.savefig(picname, dpi=256)
    plt.close()
    
   
    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    plt.minorticks_on()  
    maxx = fig.add_subplot(1,1,1)
    m, = maxx.plot(power_p, maxx_p, 'g',fillstyle = 'none')
    mel, = maxx.plot(power_p, el_maxx_p, 'r',fillstyle = 'none', dashes = [6,1])
    maxx.plot([20, 20], [0, 1.5], 'k-', dashes = [2,2])
    maxx.set_ylim([0, 1.5])
    maxx.set_xlabel('P [PW]')
    maxx.set_ylabel('W$_{1\\%}$ [GeV]')
    maxx.set_xlim([7,power_p[-1]+0.5])
    maxx.set_ylim([0,1.5])
    maxx.set_xticks(xticks)
    maxx.set_xticklabels(xticklabels)
    
    yticks = [0.3,0.9,0.6,1.2,1.5]
    maxx.set_yticks(yticks)
    maxx.set_yticklabels(['$%.1f$'%(x) for x in yticks])
    maxx.legend([m,mel], ['Photons','Electrons'], loc = 'lower left', fontsize = lfontsize, labelspacing=0.05, frameon = False)
    maxx.text(1, 1.5, '$(b)$')
    picname = picspath + '/' + "pinch_fig1b.png"
    plt.savefig(picname, dpi=256)
    plt.close()
    
    f = open('jfull.txt', 'r')
    power_p = []
    jp = []
    jn = []
    jf = []
    jfm = []
    for line in f:
        tmp = line.split()
        power_p.append(float(tmp[0]))
        jp.append(float(tmp[1]))
        jn.append(float(tmp[2]))
        jf.append(float(tmp[3]))
        jfm.append(float(tmp[4]))
    f.close()

    f = open('jcenter.txt', 'r')
    power_p = []
    jc = []
    
    for line in f:
        tmp = line.split()
        power_p.append(float(tmp[0]))
        jc.append(float(tmp[1]))
        
    f.close()
    
    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    plt.minorticks_on()  
    ax = fig.add_subplot(1,1,1)
    
    ax.plot(power_p, jp, 'r', label='$r < 0.44 \lambda$',fillstyle = 'none')
    ax.plot([20, 20], [0, 35], 'k-', dashes = [2,2])
    # ax.plot(power_p, jn, 'b-o', label='negative')
#    ax.plot(power_p, jf, 'g-v', label='full',fillstyle = 'none')
    ax.plot(power_p, jc, 'g', label='$r < 0.1 \lambda$',fillstyle = 'none', dashes = [6,1])
    ax.set_ylim([0, 35])
    ax.set_xlim([7,power_p[-1]+0.5])
    ax.set_ylabel('J$_z$ [MA]')
    ax.set_xlabel('P [PW]')
    yticks = [0,10,20,30]
    ax.set_yticks(yticks)
    ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    ax.text(2, 33, '$(c)$')
    plt.legend(loc = 'upper left', fontsize = lfontsize, frameon = False)
    picname = picspath + '/' + "pinch_fig1c.png"
    plt.savefig(picname, dpi=256)
    plt.close()

    
    
if __name__ == '__main__':
    main()
