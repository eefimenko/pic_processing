dir=$1
cd $dir
# plot series first
ufn_series.py
# compare number of electrons and positrons
#cmp_small_trapped.py
# calclucale brill
h_brill.py 1
combine_spectra.py
gammasource_brill.py
prepare_data.py .
cd ..
