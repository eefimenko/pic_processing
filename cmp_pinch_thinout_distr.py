#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils
import numpy as np

def main():
  
    dirs = ['27pw_simple', '27pw_default','27pw_merge', '27pw_number']
    iterations = [278, 278, 278, 278]
    ratios = [1e-7, 1e-3, 1e-3, 1e-3]
    configs = []
    numdirs = len(dirs)
    
    for i in range(numdirs):
        configs.append(utils.get_config(dirs[i] + "/ParsedInput.txt"))
    
    n = numdirs + 1
    
    picspath = '/home/evgeny/Dropbox/pinch_thinout'
        
    xmin = []
    xmax = []
    ymin = []
    ymax = []
    zmin = []
    zmax = []
    mult = []
    nx = []
    ny = []
    nz = []
    power = []
    for k in range(numdirs):
        wl = float(configs[k]['Wavelength'])
        Xmax = float(configs[k]['X_Max'])/wl
        Xmin = float(configs[k]['X_Min'])/wl
        Ymax = float(configs[k]['Y_Max'])/wl
        Ymin = float(configs[k]['Y_Min'])/wl
        Zmax = float(configs[k]['Z_Max'])/wl
        Zmin = float(configs[k]['Z_Min'])/wl
        xmax.append(Xmax) #mkm
        xmin.append(Xmin) #mkm
        ymax.append(Ymax) #mkm
        ymin.append(Ymin) #mkm
        zmax.append(Zmax) #mkm
        zmin.append(Zmin) #mkm
        nx.append(int(configs[k]['MatrixSize_X']))
        ny.append(int(configs[k]['MatrixSize_Y']))
        nz.append(int(configs[k]['MatrixSize_Z']))
        power.append(int(configs[k]['PeakPowerPW']))
        dx = (Xmax-Xmin)/int(configs[k]['MatrixSize_X'])
        dy = (Ymax-Ymin)/int(configs[k]['MatrixSize_Y'])
        dz = (Zmax-Zmin)/int(configs[k]['MatrixSize_Z'])
        print dx,dy,dz
        dv = 2.*dx*dy*dz*wl*wl*wl
        mult.append(1./dv)
        mult.append(1./dv)
        step = float(configs[k]['TimeStep'])*1e15*float(configs[k]['BOIterationPass'])
        omega = float(configs[k]['Omega'])
        T = 2 * math.pi/omega
        nt = int(T*1e15/step)
        print nt

    for t_ in iterations:
        print (t_ - 3000)*step/(T*1e15)
    #figures = [utils.ezpath, utils.bzpath, utils.nezpath, utils.npzpath, utils.nphzpath, utils.nizpath]
    #cmaps = ['Reds', 'Reds', 'Greens','Greens', 'Blues', 'Greens']
    #titles = ['Electric field %d pw', 'Magnetic field %d pw', 'Electrons %d pw','Positrons %d pw', 'Photons %d pw', 'Ions %d pw']
    #log = [False, False, True, True, True, True]
    #mult = [1,1,mult[0],mult[0],mult[0], mult[0]]
    figures = [utils.neypath, utils.nezpath]
    cmaps = ['Greens', 'Greens']
    titles = [u'conservative', u'simple', u'merge']
    log = [True, True]
    labels = [u'$(a)$', u'$(b)$', u'$(c)$', u'$(d)$', u'$(e)$', u'$(f)$', u'$(g)$', u'$(h)$', u'$(i)$', u'$(j)$']

    n0 = [0] * numdirs
    spx = numdirs
    spy = len(figures)
    
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})

    fig = plt.figure(num=None, figsize=(6.*float(spx)/float(spy), 5.), dpi=256)
    mp.rcParams.update({'font.size': 12})
        
    for k in range(spx):
        for j in range(spy):
            figure = figures[j]
            cmap = cmaps[j]
            if j == 0 and k == 0:
                ylabel = '$z/\lambda$'
                yticks = [-2, -1, 0, 1, 2]
                xlabel = ''
                xticks = []
                
                               #                    label = '$(i)$'
            elif j == 1 and k == 0:
                ylabel = '$y/\lambda$'
                yticks = [-2, -1, 0, 1, 2]
                xlabel = '$x/\lambda$'
                xticks = [-2, -1, 0, 1, 2]
                #                    label = '$(j)$'
            elif j == 0:
                ylabel = ''
                xlabel = ''
                xticks = []
                yticks = []
                #                    label = '$(k)$'
            elif j == 1:
                ylabel = ''
                xlabel = '$x/\lambda$'
                xticks = [-2, -1, 0, 1, 2]
                #                    label = '$(l)$'
            ax = utils.subplot(fig, iterations[k], dirs[k]+'/'+figure,
                               shape = (nx[k],ny[k]), position = (spy,spx,k+1+j*spx),
                               extent = [xmin[k], xmax[k], ymin[k], ymax[k]], ratio = ratios[k],
                               cmap = cmap, fontsize = 12, normalize = False,
                               label = labels[j + k*spy],
                               colorbar = True, logarithmic=log[j], verbose=0, transpose=1,
                               xlim = [-2,2], ticks = [-2, -1, 0, 1, 2], ylim = [-2,2], xticklabels = xticks, yticklabels = yticks,
                               vmin = 1e20, vmax = 1e25, 
                               xlabel = xlabel, ylabel = ylabel, mult = mult[j + k*spy])
            ax.text(-1.*2,0.8*2, labels[k+j*spx], fontsize = 12)
    picname = picspath + '/' + "cmp_pinch_thinout_distr.png"
    plt.tight_layout()
    plt.savefig(picname, dpi=512)
    plt.close()

    
if __name__ == '__main__':
    main()
