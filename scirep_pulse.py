#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import matplotlib.image as mpimg
import matplotlib as mp
import shutil
import os
import scipy.signal.signaltools as sigtool
import numpy as np
import copy
import matplotlib.colors as clr

def main():
   
    picspath = 'pics'
    path = '.'
    enpath = '/statdata/ph/EnSp/'
    angpath = '/statdata/ph/angSpSph/'
    angpath_el = '/statdata/el/angSpSph/'
    angpath_pos = '/statdata/el/angSpSph/'
    ezpath = 'data/E2z'
    nexpath = 'data/Electron2Dx'
    config = utils.get_config(path + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV'])
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    dv = 2*dx*dy*dz 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
    dt = (x0*y0*1e15)/T
    num = len(sys.argv)

    ax_ = []
    ax1_ = []
    # ax2_ = []
    ez_t = []
    ne_t = []
    pow_t = []
    max_t = []
    av_t = []
    br_full_t = []
    br_100mev_t = []
    br_1gev_t = []
    br_full1_t = []
    br_100mev1_t = []
    br_1gev1_t = []
   
    t = 0
    omega = float(config['Omega'])
    delay = float(config['delay'])
    duration = float(config.get('Duration', 0))
    T = 2*math.pi/omega*1e15
    phi = int(config['QEDstatistics.OutputN_phi'])
    theta = int(config['QEDstatistics.OutputN_theta'])
    nit = int(config['QEDstatistics.OutputIterStep'])
    dt1 = float(config['TimeStep'])*nit
    wl = float(config['Wavelength'])
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    ev = float(config['eV'])
    ne = int(config['QEDstatistics.OutputN_E'])
    nt = int(config['QEDstatistics.OutputN_theta'])
    tmax = float(config.get('QEDstatistics.ThetaMax',0))*180./math.pi
    tmin = float(config.get('QEDstatistics.ThetaMin',0))*180./math.pi
    
    emax = float(config['QEDstatistics.Emax'])/ev*1e-9
    emin = float(config['QEDstatistics.Emin'])/ev*1e-9
    de = (emax - emin)/ne
    dt = (tmax - tmin)/nt
    dee1 = de*1e9*ev # erg
    dtt1 = dt*math.pi/180.
    dv = 2*dx*dy*dz
    electronMass = 9.10938e-28
    electronCharge = 4.8032e-10
    ncr = electronMass * omega * omega/(8. * math.pi * electronCharge * electronCharge)
    const_a_p = 7.81441e-9
    a0 = const_a_p * math.sqrt(ppw*1e22)
    print a0, 1.18997e+8*a0
    mp.rcParams.update({'font.size': 16})
    S = 2 * math.pi * 0.1 * 0.1 * wl * wl * 100 # square of source mm^2, r = 0.1 lambda 
    ds = 1.5e4 # mrad^2
    f = open(path + '/br_t.dat', 'r')
    i = 0
    for line in f:
        tmp = line.split()
        ax_.append(float(tmp[0])/T)
        ez_t.append(float(tmp[1]))
        pow_t.append(float(tmp[2]))
        max_t.append(float(tmp[3])/(0.511e-3))
        av_t.append(float(tmp[4]))
        ax1_.append((float(tmp[0])-39*dt1*1e15)/T)
        br_full_t.append(float(tmp[5]))
        br_100mev_t.append(float(tmp[6]))
        br_1gev_t.append(float(tmp[7])/1e23)
        br_full1_t.append(float(tmp[5])/S/ds)
        br_100mev1_t.append(float(tmp[6])/S/ds)
        br_1gev1_t.append(float(tmp[7])/S/ds)
    # f = open('t_ez_ne.dat')
    # for line in f:
        # tmp = line.split()
        # ax2_.append(float(tmp[0])*T)
        # ne_t.append(float(tmp[2]))
    
    filename = 'jz_anls_new'
    nmin,nmax = utils.find_min_max_from_directory(path + '/' + utils.jz_zpath)
    jplus = np.zeros(nmax)
    jminus = np.zeros(nmax)
    jfull = np.zeros(nmax)
    jmax = np.zeros(nmax)
    nmin = utils.read_tseries(path,filename, jplus, jminus, jfull, jmax)
    c2 = 3.33e-10*dx*dy*1e-6
    for i in range(len(jfull)):
        jfull[i] = abs(jfull[i])*c2

    np_t = utils.ts(path, utils.npzpath, name='npz',
                    tstype='max', shape = (nx,ny), verbose=True)/dv
    netr_t = utils.ts(path, utils.netpath, name='ne_trapped',
                         tstype='sum', verbose=True)/1e10
    npostr_t = utils.ts(path, utils.npostpath, name='npos_trapped',
                         tstype='sum', verbose=True)/1e10
    print 'Max ', max(npostr_t)
    print de, int(2./de), (2.*1e-3/de)
    fraction = 2.*1e-3/de
    gev2_pos = int(2./de)
    print max(ez_t), max(br_1gev_t), max(br_1gev1_t)
    print br_1gev1_t[gev2_pos]*fraction 
   
    current_dir = os.path.realpath(path)
    field_coeff = 1
    if 'shifted' in current_dir or 'distorted' in current_dir or '12beams' in current_dir:
        field_coeff = 0.9 
    
    ez_a = [0] * len(ez_t)
    for i in range(len(ez_t)):
        t = i*dt1
        ez_a[i] = field_coeff*math.exp(-2.*math.log(2)*(t - delay)/duration*(t - delay)/duration)*3*math.sqrt(ppw/10.)
#    name = path + '/spectra.txt'
#    spectra = np.fromfile(name, sep = ' ')
#    print spectra.shape
#    gs = gridspec.GridSpec(1, 2, width_ratios=[2, 1])
    fig = plt.figure(num=None, figsize = (10,5))
    ax = fig.add_subplot(1,1,1)
    mp.rcParams.update({'font.size': 15})
    fig.subplots_adjust(left=0.25)
    ax2 = ax.twinx()
    ax2.spines["left"].set_position(("axes", -0.18))
    utils.make_patch_spines_invisible(ax2)
    # Second, show the right spine.
    ax2.spines["left"].set_visible(True)
    ax2.yaxis.set_label_position('left')
    ax2.yaxis.set_ticks_position('left')

    ax4 = ax.twinx()
    ax4.spines["left"].set_position(("axes", -0.09))
    utils.make_patch_spines_invisible(ax4)
    # Second, show the right spine.
    ax4.spines["left"].set_visible(True)
    ax4.yaxis.set_label_position('left')
    ax4.yaxis.set_ticks_position('left')
    f, = ax.plot(ax1_, netr_t, 'dimgray')
    fp, = ax.plot(ax1_, npostr_t, 'k')
    f1, = ax2.plot(ax_, utils.env(np.array(ez_t)/1e11, ax_), 'B')
    f2, = ax2.plot(ax_, ez_a, 'b:')
    thr = 3*math.sqrt(7.1/10.)
    f3, = ax2.plot([ax_[0], ax_[-1]], [thr,thr], 'b--')
    ax2.set_ylabel('$|E|, \\times 10^{11} CGS$ ', color='B')
    ax4.set_ylabel('$J_z, MA$')
    ax.set_ylabel('$N_p, \\times 10^{10}$')
    ax.set_xlabel('Time, T')
    # ax2.set_ylim([0,1])
#    f4, = ax4.plot(ax1_, jfull, 'k')
    f4, = ax4.plot(ax1_, utils.env(jfull, ax1_), 'r')
    print len(br_1gev_t)
    ax1 = ax.twinx()
    thrne = 7e24
    p1, = ax1.plot(ax1_, np_t, 'g')
    ax1.set_ylabel('$n_{p}, cm^{-3}$', color='g')
    n3, = ax1.plot([ax1_[0], ax1_[-1]], [thrne,thrne], 'g--')
    ax1.set_xlim([5,45])
    #ax1.set_ylim([0,20])
    ax.set_xlim([5,45])
    #ax.set_ylim([0,5])
    #ax1.set_ylim([2e22, 1e26])
    ax1.set_yscale('log')
    bounds = [5, 14, 18, 23, 32, 36, 45]
    axticks = [5, 14, 18, 23, 32, 36, 45]
    axticklabels = ['$%d$'%(x) for x in axticks]
    ax.set_xticks(axticks)
    ax.set_xticklabels(axticklabels)
    #for k in range(1,len(bounds)-1):
    #    ax.plot([bounds[k], bounds[k]], [0, 7], 'k:')
    ax.yaxis.label.set_color(fp.get_color())
    ax.spines["left"].set_color(fp.get_color())
    ax.tick_params(axis='y', colors=fp.get_color())
    ax1.yaxis.label.set_color(p1.get_color())
    ax1.spines["right"].set_color(p1.get_color())
    ax1.tick_params(axis='y', colors=p1.get_color())
    ax2.yaxis.label.set_color(f1.get_color())
    ax2.spines["left"].set_color(f1.get_color())
    ax2.tick_params(axis='y', colors=f1.get_color())
    ax4.yaxis.label.set_color(f4.get_color())
    ax4.spines["left"].set_color(f4.get_color())
    ax4.tick_params(axis='y', colors=f4.get_color())
    ax.set_yticks([0,2,4,6])
    ax.set_yticklabels(['$0$','$2$','$4$','$6$'])
    ax4.set_yticks([0,2,4,6,8,10])
    ax4.set_yticklabels(['$0$','$2$','$4$','$6$','$8$', '$10$'])
    ax2.set_yticks([0,1,2,3])
    ax2.set_yticklabels(['$0$','$1$','$2$','$3$'])
    ax1.set_yticks([1e21, 5*ncr, 1e22, 50*ncr, 1e23, 500*ncr,1e24, 5000*ncr, 1e25, 3e25])
    ax1.set_yticklabels(['$10^{21}$','$5n_{c}$','$10^{22}$','$50n_{c}$','$10^{23}$','$500n_{c}$','$10^{24}$','$5000n_{c}$', '$10^{25}$', '$3 \\times 10^{25}$'])
#    ax.set_title('Ideal dipole wave')
#    plt.legend([f, fp, f1, f2, p1, n3, f3], ["Electrons", 'Positrons', 'E', 'Vacuum E', '$N_{pos}$', 'Stationary $N_{pos}$', 'Stationary E'], loc='lower center')
#    plt.legend([f1, f2, p1, f, fp, f4, n3, f3], ['$|E|$', 'Vacuum $|E|$', '$n_{p}$','$N_e^{\lambda/2}$', '$N_p^{\lambda/2}$', '$J_z$', 'Stationary $n_{p}$', 'Stationary $|E|$', '$J_z$'], loc='upper left')
    plt.legend([f1, f2, p1, f, fp, f4], ['$|E|$', 'Vacuum $|E|$', '$n_{p}$','$N_e}$', '$N_p}$', '$J_z$'], loc='upper left', fontsize = 15)
    ax.text(-5, 7, '$(b)$', fontsize = 16)
#    plt.show()
    name = "fig6b"
    picname = picspath + '/' + name + ".png"
    plt.tight_layout()
    plt.savefig(picname)
#    picname = picspath + '/' + name + ".pdf"
#    plt.savefig(picname)
    plt.close()
  

if __name__ == '__main__':
    main()
