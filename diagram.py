#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def read_conc(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    return tmp[0]

def read_concentration(file, dv):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    e = tmp[0]
    for j in range(len(e)):
            e[j] = e[j]/((j+0.5)*dv)
    return e

def read_field(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def find_max_photon_energy_1percent(array, step):
    summ = 0
    for j, e in enumerate(array):
        if e != 1e-9:
            summ += j*e
    part_sum = 0
    N = len(array)
    for i in range(N):
        if array[N-i-1] != 1e-9:
            part_sum += array[N-i-1]*(N - i - 1)
            
            if part_sum > 0.01 * summ:
                break
    
    return (N-i-1)*step


def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def create_pulse(n,step,amp, tp, delay):
    dt = step
    ans = []
#    tp = 30
    tau = tp/1.1437
#    delay = 23.1099984185576458468467817
    for i in range(n):
        t = i*dt-delay
        if t > 0 and t < math.pi * tau:
            tmp = math.sin(t/tau)
            f = amp*tmp*tmp
        else:
            f = 0.
        ans.append(f*f)
    return ans

def read_field2d(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def normList(L, normalizeTo=1):
    '''normalize values of a list to make its max = normalizeTo'''

    vMax = max(L)
    return [ x/(vMax*1.0)*normalizeTo for x in L]

def read_energy(file):
    f = open(file, 'r')
    tmp = [[float(x)+1e-9 for x in line.split()] for line in f]
    f.close()
    return tmp[0]

def max2d(array):
    m = 0
    for i in range(len(array)):
        for j in range(len(array[0])):
            if array[i][j] > m:
                m = array[i][j]
    return m, max([max(row) for row in array])

def main():

    config = utils.get_config("ParsedInput.txt")
    BOIterationPass = float(config['BOIterationPass'])
    c = 2.99792e+10
        
    coeff = 10./(3.3e11*3.3e11)*0.97399

    Xmax = float(config['X_Max'])*1e4 #mkm
    Xmin = float(config['X_Min'])*1e4 #mkm
    Ymax = float(config['Y_Max'])*1e4 #mkm
    Ymin = float(config['Y_Min'])*1e4 #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    
    row = -1

    stepx = 5.656854249492380319955242562457442546/nx
    dv = 2. * math.pi * stepx * stepx * 0.8e-4 * 1e-8

    step = float(config['TimeStep'])*1e15*BOIterationPass
    dt = float(config['TimeStep'])
    omega = float(config['Omega'])
#    Emax = float(config['QuantumParticles.Emax'])
    Emax = 10
    Emin = 0
    N_E = 100
    outStep = 100000
#    Emin = float(config['QuantumParticles.Emin'])
#    N_E = int(config['QuantumParticles.OutputN_E'])
#    outStep = int(config['QuantumParticles.OutputIterStep'])
    Xmax = float(config['X_Max']) #mkm to wavelength
    Xmin = float(config['X_Min']) #mkm to wavelength
    Ymax = float(config['Y_Max']) #mkm to wavelength
    Ymin = float(config['Y_Min']) #mkm to wavelength

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    print 'dx = ' + str(dx) 
    print 'dy = ' + str(dy)
    print 'dz = ' + str(dz)
    peakpower = float(config['PeakPower'])*1e-7*1e-15
    r = float(config['R'])
    Ne = float(config['Ne'])
    wl = float(config['Wavelength'])
    dv = 2*dx*dy*dz
    coeff = 10./(3.3e11*3.3e11)*0.97399*(wl/0.8e-4)*(wl/0.8e-4)
    v = 4./3.*math.pi*r*r*r
    nemax = v*Ne
    print peakpower, r, nemax
    ev = float(config['eV'])
    expath = 'data/E2x'
    bxpath = 'data/B2x'
    eypath = 'data/E2y'
    bypath = 'data/B2y'
    ezpath = 'data/E2z'
    bzpath = 'data/B2z'
    denspath = 'data/Electron2Dx'
    necpath = 'data/ne'
    path='statdata'
    elpath='/el/'
    elmaxpath='/el_max/'
    posmaxpath='/pos_max/'
    phmaxpath='/ph_max/'
    
    nepath = 'data/NeTrap'
    nppath = 'data/NposTrap'
    picspath = 'pics'
#    concpath = 'data/ne'
    maxf = 0.
    maxn = 0.
    nmax = 160
    T = 2 * utils.pi/omega
    time = dt/T
    nt = int(1./time/BOIterationPass)
    print 'nt = ' + str(nt)
    en_step = (Emax-Emin)/N_E/ev*1e-9
    emax = Emax/ev*1e-9
    Ne = []
    Ne_max = []
    Npos = []
    power = []
    BOIterationPass = int(config['BOIterationPass'])
    try:
        tmp = config['BOIterationStart']
    except KeyError:
        tmp = 0
    BOIterationStart = int(tmp)
    if BOIterationStart%BOIterationPass == 0:
        nmin = BOIterationStart/BOIterationPass
    else:
        nmin = BOIterationStart/BOIterationPass + 1

    nesize = float(config['ne.SetBounds_1'])
    nenum = int(config['ne.SetMatrixSize_0'])
    step1 = nesize/nenum
    v1 = 2. * math.pi * step1 * step1 * wl

    nmax = nmin+utils.num_files(nepath)
    eprev = 0
    powerfile = open('power.dat', 'w')
    energyfile = open('energy.dat', 'w')
    energymfile = open('energym.dat', 'w')
    nefile = open('ne.dat', 'w')
    dndtfile = open('dndt.dat', 'w')
#    concfile = open('conc.dat', 'w')
   # nmax = 200
    print nmax, nmin
    
    power_t = []
    energy_t = []
    energym_t = []
    ne_t = []
    dndt_t = []
    conc_t = []
    fe = []
    fh = []
    e0 = 0
    n0 = 0
    nf = 0
    i0 = 0
    i_f = nmax
    dmy = int(0.35*wl/dy)
    print dmy
    maxe = 0
    ezv_prev = 0.
    for i in range(nmin,nmax):
        if i != 600020:
            print "%06d.txt" % (i,)
            #exname = expath + '/' + "%06d.txt" % (i,)
            #eyname = eypath + '/' + "%06d.txt" % (i,)
            ezname = ezpath + '/' + "%06d.txt" % (i,)
            #bxname = bxpath + '/' + "%06d.txt" % (i,)
            #byname = bypath + '/' + "%06d.txt" % (i,)
            bzname = bzpath + '/' + "%06d.txt" % (i,)
#            concname = concpath + '/' + "%06d.txt" % (i,)

            #ex = read_field2d(exname,nx,ny)
            #ey = read_field2d(eyname,nx,ny)
            ez = read_field2d(ezname,nx,ny)
            #bx = read_field2d(bxname,nx,ny)
            #by = read_field2d(byname,nx,ny)
            bz = read_field2d(bzname,nx,ny)
            densname = denspath + '/' + "%06d.txt" % (i,)
            dens = read_field2d(densname,nx,ny, 1./dv)
            m, max_dens = max2d(dens)
            print m, max_dens
            #exv = ex[nx/2][ny/2]
            #eyv = ey[nx/2][ny/2]
            ezv = ez[nx/2][ny/2]
#            conc = read_concentration(concname, dv)
#            conctmp = conc[0]
#            print conc[0]
            #bxv = bx[nx/2][ny/2]
            #byv = by[nx/2][ny/2]
            bzv = bz[nx/2][ny/2-dmy]*1.53
            tmp = (ezv*ezv + bzv*bzv)*coeff
            if maxe < ezv:
                maxe = ezv
            #power.append(tmp)
            nename = nepath + '/' + "%06d.txt" % (i,)
#            npname = nppath + '/' + "%06d.txt" % (i,)
            Ne_max.append(nemax)
            fe.append(abs(ezv))
            fh.append(abs(bzv))
            e = read_field(nename)
#            p = read_field(npname)
            p = 0
            if i == 0:
                e0 = e
 
            if tmp < 1.1*peakpower:
                power.append(tmp)
                Ne.append(e)
                Npos.append(p)
            else:
                power.append(0)
                Ne.append(0)
                Npos.append(0)

            enext = e
            if i*BOIterationPass % outStep == 666666666660:
                name=round(i*BOIterationPass*time, 4)
                filename = path + elpath + str("%.4f" % (name, )) + '.txt'
                array = read_energy(filename)
                l = [j for j, e in enumerate(array) if e != 1e-9]
                l.append(0)
                maxen0 = max(l)*en_step

                maxen = find_max_photon_energy_1percent(array, en_step)
             #   print tmp, maxen0, maxen, enext, eprev
                
                
                                
                if enext != eprev:
                    dndt = (enext - e0)/((enext - eprev)/step)
                else:
                    dndt = 0.
                dndt_t.append(dndt)
                if tmp < 1.1*peakpower:
                    dndtfile.write(str(dndt) + '\n')
                    powerfile.write(str(tmp)+'\n')
                    energyfile.write(str(maxen)+'\n')
                    energymfile.write(str(maxen0)+'\n')
                    nefile.write(str(enext)+'\n')
#                    concfile.write(str(conctmp) + '\n')
#                    conc_t.append(conctmp)
                    power_t.append(tmp)
                    energy_t.append(maxen)
                    energym_t.append(maxen0)
                    ne_t.append(enext)
                else:
                    power_t.append(0)
                    energy_t.append(0)
                    energym_t.append(0)
                    ne_t.append(0)
                    
#                print tmp, 0.98*peakpower

            if n0 == 0 and tmp > 0.993*peakpower:
                n0 = enext
                i_0 = i
                print n0, i_0

            if n0 != 0 and nf == 0 and tmp < 0.993*peakpower:
                nf = enext
                i_f = i
                print 'Max concentration = ' + str(max_dens) + ' iter = ' + str(i_f)
                ff = open('max_ne.dat', 'w')
                ff.write(str(max_dens)+'\n')
                ff.close()
                e = read_trap(nepath + '/%06d.txt'%(i))
                print 'Trapped', e
                exit(-1)
                nename = necpath + '/' + "%06d.txt" % (i,)
                e = read_conc(nename)
                for j in range(len(e)):
                    e[j] = e[j]/((j+0.5)*v1)
                    conc = max(e)
                ff = open('conc.dat', 'w')
                ff.write(str(conc)+'\n')
                ff.close()
                phase = math.asin(ezv/maxe)
                if ezv < ezv_prev:
                    phase = math.pi - phase
                ff = open('phase.dat', 'w')
                ff.write(str(phase)+'\n')
                ff.close()
                ff = open('num_periods.dat', 'w')
                ff.write(str(int((i_f-i_0)/nt))+'\n')
                ff.close()
                print ' iter = ' + str(i_f)
                print ' phase = ' + str(phase/math.pi)
                print ' maxe = ' + str(maxe) + ' ezv = ' + str(ezv)
            eprev = enext
            ezv_prev = ezv
            power_prev = tmp
            print tmp
        else:
            Ne.append(e)
            Ne_max.append(e)
            Npos.append(p)
            power.append(tmp)
    
    powerfile.close()
    energyfile.close()
    energymfile.close()
    nefile.close()
    dndtfile.close()
#    concfile.close()
#    normpower = normList(power, coeff)
#    pulse = create_pulse(nmax, step, 1, tp, tdelay)
    fig, ax1 = plt.subplots()
    axis = create_axis(nmax, step)
#    pe, = ax1.plot(axis, Ne, 'g')
#    pb, = ax1.plot(axis, Npos, 'b')
#    pm, = ax1.plot(axis, Ne_max, 'r')
#    ax1.set_yscale('log')
#    ax2 = ax1.twinx()
    #pu, = ax2.plot(axis, pulse, 'r-')
    pe, = ax1.plot(fe, 'b')
    ph, = ax1.plot(fh, 'g')
    ax1.legend([pe, ph], ['E', 'H'], loc=2)
#    ax1.set_xlim([80, 100])
    plt.savefig('pics/field.png')
    plt.close()

#    print power_t
    '''
    fig, ax1 =  plt.subplots()
    ppp, = ax1.plot(power_t, ne_t)
    plt.savefig('pics/nn.png')
    plt.close()

    fig, ax1 =  plt.subplots()
    ppp, = ax1.plot(energy_t)
    ppp1, = ax1.plot(energym_t)
    plt.savefig('pics/energy.png')
    plt.close()

    fig, ax1 =  plt.subplots()
    ppp, = ax1.plot(conc_t)
    ax2 = ax1.twinx()
    pph, = ax2.plot(power_t,'r')
    ax1.set_yscale('log')
    plt.savefig('pics/conc_axis_t.png')
    plt.close()
    '''    
    print '------'
    print 'nf,n0 =',nf, n0
    print 'i_f,i_0 =',i_f, i_0
    if nf == 0:
        nf = n0
    gamma = math.log(nf/n0)/((i_f - i_0)*step)*T*1e15
    print gamma
    gfile = open('gamma.dat', 'w')
    gfile.write(str(gamma))
    gfile.close()
#    nmfile = open('ntrappedmax.dat', 'w')
#    nmfile.write(str(nf))
#    nmfile.close()
#    phfile = open('phmax1p.dat', 'w')
#    phfile.write(str(max(energy_t)))
#    phfile.close()

if __name__ == '__main__':
    main()
