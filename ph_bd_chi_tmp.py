#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import sys
import math
import utils
import numpy as np

def read_field2d(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def read_bd(file):
    f = open(file, 'r')
    line = f.readline()
    tmp = [float(x) for x in line.split()]
    s = sum(tmp)
    if s > 0:
        for i in range(len(tmp)):
            tmp[i] /= s
    return tmp

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def main():

    config = utils.get_config("ParsedInput.txt")
    bpath = 'statdata/el/ElChiPh/'
    dpath = 'statdata/ph/PhChiEl/' 
    ezpath = 'data/E2z'
    bzpath = 'data/B2z'
    picspath = 'pics'
   
    delta = 1
    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(bpath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(bpath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax) + ' files'

    nx = int(config['QEDstatistics.nChi'])
    xmax = float(config['QEDstatistics.ChiPhMax'])
    nx1 = int(config['MatrixSize_X'])
    ny1 = int(config['MatrixSize_Y'])
    dx1 = float(config['Step_X'])
    dy1 = float(config['Step_Y'])
  
    x0 = int(config['QEDstatistics.OutputIterStep'])
    y0 = float(config['TimeStep']) 
  
    birth = []
    death = []
    step = x0*y0*1e15

    for i in range(nmin,nmax,delta):
        bname = bpath + '/' + "%.4f.txt" % (i,)
        print bname
        b = read_bd(bname)
        birth.append(b)
        dname = dpath + '/' + "%.4f.txt" % (i,)
        print dname
        d = read_bd(dname)
        death.append(d)
    axis = create_axis(nmax-nmin,step,nmin*step)
   
    fig = plt.figure(num=None)
    plt.rc('text', usetex=True)
    ax = fig.add_subplot(2,1,1)
    
    surf = ax.imshow(birth, extent=[0, xmax, nmin*step, (nmax-1)*step], norm=clr.LogNorm(), aspect = 'auto',  origin = 'lower')
  
    ax.autoscale(False)
    ax.set_ylabel('t, T')
    ax.set_xlabel('$\chi$')
    ax.set_ylim([nmin*step, (nmax-1)*step])
#    ax.set_xlim([0, 20])
#    plt.colorbar(surf,  orientation  = 'vertical')
    ax = fig.add_subplot(2,1,2)
    surf = ax.imshow(death, extent=[0, xmax, nmin*step, (nmax-1)*step], norm=clr.LogNorm(), aspect = 'auto',  origin = 'lower')
#    ax.set_xlim([0, 20])
  
    ax.autoscale(False)
    ax.set_ylabel('t, T')
    ax.set_xlabel('$\chi$')
    ax.set_ylim([nmin*step, (nmax-1)*step])
#    plt.colorbar(surf,  orientation  = 'vertical')
    plt.show()

if __name__ == '__main__':
    main()
