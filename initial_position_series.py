#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import math
import utils
import sys
import numpy as np
import os
from tqdm import *

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            row.append(array[index]+ 1e-12)
        field.append(row)
    return field

def find_max(file, mult = 1.):
    max = 0
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]
    for p in array:
        if p > max:
            max = p
    return max

def find_max_from_all_fields(expath, eypath, ezpath, bxpath, bypath, bzpath, i):
    max = 0
    name = expath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = eypath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = ezpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    return max

def find_max_from_all_particles(xpath, ypath, zpath, i, mult = 1.):
    max = 0
    name = xpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    name = ypath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    name = zpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    return max
    
def max2d(array):
    return max([max(x) for x in array])    

def create_subplot(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, text, text1, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
#    name = path + '/' + "%06d.txt" % (i,)
    field = utils.bo_file_load(path,i,nx,ny, verbose=1, transpose=1)
    if field == None:
        return
#    maxe = max([max(row) for row in field])
#    mine = min([min(row) for row in field])
    maxe = np.amax(field)
    mine = np.amin(field)
    print(path, maxe, mine)
    ratio = 1e-3
    if maxe == 0:
        maxe = 1
    if maxe == mine:
        v_min_ = maxe
        v_max_ = 1e9*maxe
    else:
        v_max_ = maxe
        v_min_ = maxe*ratio
   
    ticks = [-2,-1,0,1,2]
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = v_min_, vmax = v_max_, norm=clr.LogNorm())
#    ax.text(-2.5, 2.5, text)
    ax.set_title(text) 
#    ax.text(1.6, 1.6, text1, fontsize=30)
    ax.set_xlim([-2,2])
    ax.set_ylim([-2,2])
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
    plt.xticks(ticks)
    plt.yticks(ticks)
#    plt.colorbar(surf,  orientation  = 'vertical', ticks = [v_min, v_max])
    return surf
def create_subplot_f(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, text, text1, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
#    name = path + '/' + "%06d.txt" % (i,)
    field = utils.bo_file_load(path,i,nx,ny, verbose=1, transpose=1)
    if field == None:
        return
#    maxe = max([max(row) for row in field])
#    v_max_ = max2d(field)
#    print v_max_
    ratio = 1e-3
    ticks = [-2,-1,0,1,2]
#    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = v_max_*ratio, vmax = v_max_)
    maxe = np.amax(field)
    mine = np.amin(field)
    print(path, maxe, mine)
    ratio = 1e-6
    if maxe == 0:
        maxe = 1
    if maxe == mine:
        v_min_ = maxe
        v_max_ = 1e9*maxe
    else:
        v_max_ = maxe
        v_min_ = maxe*ratio
  
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = v_min_, vmax = v_max_)
    ax.set_title(text)
#    ax.text(1.6, 1.6, text1, fontsize=30)
    ax.set_xlim([-2,2])
    ax.set_ylim([-2,2])
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
    plt.xticks(ticks)
    plt.yticks(ticks)
#    plt.colorbar(surf,  orientation  = 'vertical', ticks = [v_min, v_max])
    return surf


def pulse_value(i,step,amp,tp,delay):
    ans = []
    tau = tp/1.1437
#    delay = 23.1099984185576458468467817
    t = i*step-delay
    if t > 0 and t < math.pi * tau:
        tmp = math.sin(t/tau)
        f = amp*tmp*tmp
    else:
        f = 0.
    return f

def main():
    picspath = 'series1'
    path = '.'
    config = utils.get_config("ParsedInput.txt")

    BOIterationPass = float(config['BOIterationPass'])
    dt = float(config['TimeStep'])*1e15*BOIterationPass #fs
    
    ppw = int(config['PeakPower'])*1e-22 #PW
    omega = float(config['Omega'])
    dt = float(config['TimeStep'])*BOIterationPass
    T = 2 * math.pi/omega
    dtt = dt/T
    print("Power = " + str(ppw) + 'PW')
    print("dt = " + str(dt) + 'fs')
    wl = float(config['Wavelength'])
     
    Xmax = float(config['X_Max'])/wl #mkm
    Xmin = float(config['X_Min'])/wl #mkm
    Ymax = float(config['Y_Max'])/wl #mkm
    Ymin = float(config['Y_Min'])/wl #mkm
    Zmax = float(config['Z_Max'])/wl #mkm
    Zmin = float(config['Z_Min'])/wl #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print('Nx = ' + str(nx)) 
    print('Ny = ' + str(ny))
    print('Nz = ' + str(nz))

    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz
    mult_dv = 1/(2.*dx*dy*dz*1e-12)
    step = (Xmax-Xmin)/nx
    const_a_p = 7.81441e-9
    
    a0 = const_a_p * math.sqrt(ppw*1e22)
    a00 = 3.e11 * math.sqrt(ppw/10.)
    print(a0, a00)
    archive = None#os.path.join(path, 'BasicOutput', 'data.zip')
    second_row = [utils.ezpath, utils.bzpath, utils.nezpath, utils.npzpath]
    second_row = [os.path.join('BasicOutput', x) for x in second_row]
    nmin, nmax, delta = utils.get_min_max_iteration(sys.argv,
                                                    #utils.eypath, utils.bypath, utils.neypath, utils.nphypath,
                                                    *second_row, archive = archive)
    print('Here', nmin, nmax, delta)
    
    if not os.path.exists(picspath):
        print(picspath, ' does not exist, creating')
        os.makedirs(picspath)
    extent = [Xmin, Xmax, Ymin, Ymax]
    bounds = [Xmin, Xmax]#, Ymin, Ymax]
    ticks = None#[-1, 0.5,0,0.5, 1]
    #first_row = [utils.eypath, utils.bypath, utils.neypath, utils.npypath, utils.nphypath, utils.neypath]
    
    titles = ['Electric field', 'Magnetic field', 'Electrons', 'Positrons', 'Regions']
    cmaps = ['Reds', 'Reds', 'Greens', 'Greens', 'Blues', 'Reds']
    mult = [1./(0.653*a00), 1./a00, mult_dv, mult_dv, mult_dv, 1.]
    logarithmic = [False, False, True, True, True, False]
    spx = 1
    spy = len(second_row)
    
    for i in tqdm(range(nmin,nmax,delta)):
        picname = picspath + '/' + "qe%06d.png" % (i,)
        if os.path.exists(picname):
            continue
#        print "\rSaving %s, %.0f %% done" % (picname,float(i-nmin)/(nmax-nmin)*100),
#        sys.stdout.flush()
        t = i * dtt
        
        fig = plt.figure(num=None, figsize=(10.*spy/spx, 5.), dpi=256)
        mp.rcParams.update({'font.size': 10})
        for j in range(spy):
            #utils.subplot(fig, i, first_row[j], shape = (nx,ny), position = (spx,spy, j + 1),
            #              extent = extent, cmap = cmaps[j], title = titles[j], titletype = 'max',
            #              colorbar = True, logarithmic=logarithmic[j], verbose=0,
            #              xlim = bounds, ylim = bounds, ticks = ticks, transpose = 1,
            #              xlabel = '$x/\lambda$', ylabel = '$z/\lambda$', mult = mult[j], archive=archive)
            utils.subplot(fig, i, second_row[j], shape = (nx,ny), position = (spx,spy+1, j + 1),
                          extent = extent, cmap = cmaps[j], title = titles[j], titletype = 'max',
                          colorbar = True, logarithmic=logarithmic[j], verbose=0, transpose = 1,
                          xlim = bounds, ylim = bounds, ticks = ticks,
                          xlabel = '$x/\lambda$', ylabel = '$y/\lambda$', mult = mult[j], archive=archive)
        utils.subplot(fig, i, 'statdata/el/InitialPos', fmt = '%d.0000', shape = (nx,ny),
                      position = (spx,spy+1, spy+1),
                      extent = extent, cmap = cmaps[j], title = titles[j], titletype = 'max',
                      colorbar = True, logarithmic=True, verbose=0, transpose = 1,
                      xlim = bounds, ylim = bounds, ticks = ticks,
                      xlabel = '$x/\lambda$', ylabel = '$y/\lambda$', mult = mult[j], archive=archive)
      
        plt.tight_layout()
        plt.savefig(picname, dpi=64)
        plt.close()

if __name__ == '__main__':
    main()
