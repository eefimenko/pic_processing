#!/usr/bin/python
import gzip
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import math
import sys
import os

def convert_exy_toephi(ex,ey,dx,dy):
    (nx,ny) = ex.shape
    print nx,ny
    n0 = nx/2
    m0 = ny/2
    ephi = np.zeros((nx,ny))
    er = np.zeros((nx,ny))
    for i_ in xrange(nx):
        i = i_ - n0
        x = i*dx
        for j_ in xrange(ny):
            j = j_ - m0
            y = j*dy
            if i != 0:
                phi = math.atan2(y,x)
            else:
                phi = 0.5*math.pi
            s = math.sin(phi)
            c = math.cos(phi)
           
            r = math.sqrt(x*x + y*y)
            ephi[i_][j_] = r*(-ex[i_][j_]*s + ey[i_][j_]*c)
            er[i_][j_] = (ex[i_][j_]*c + ey[i_][j_]*s)
    return ephi, er

def fromfile(file,nx=1,ny=1,ftype='txt', verbose = 0):
    if 'txt' in ftype:
        sep = '\t'
        f = open(file, 'r')
    else:
        sep = ''
        f = open(file, 'rb')
    array = np.fromfile(f, dtype='f', count=-1, sep=sep)
    print array.shape
    if ny != 1:
        array = np.reshape(array, (nx,ny), order = 'F')
    return array

def bo_file_load(path,iteration,nx=1,ny=1,ftype='txt',fmt='%06d.%s',mult=1., verbose = 0):
    name = path + '/' + fmt % (iteration,ftype)
    if verbose != 0:
        print name
    if os.path.exists(name):
        field = fromfile(name,nx,ny,ftype)*mult
    else:
        field = None
    return field

def filter_gen(field, dx, dy, xmin, xmax):
    (nx,ny) = field.shape
    for i in range(nx):
        for j in range(ny):
            if i*dx < xmin or i*dx > xmax or j*dy < xmin or j*dy > xmax:
                field[i][j] = 0.
    return field
    
            
def main():
    path = sys.argv[1]
    iteration = int(sys.argv[2])
    width = 4 # mkm
    gen_width = 0 # mkm
    
    nx = 512
    ny = 512
    nz = 512
    dx = width/nx
    dy = width/ny
    dz = width/nz
    xmin = gen_width
    xmax = width - gen_width
    xm1 = width/2 - gen_width
    ym1 = width/2 - gen_width
    xm = width/2
    ym = width/2
    zm = width/2
    ex = bo_file_load(path + '/data/Ex2D' , iteration, nx, ny, verbose = 1)
    print ex.shape
#    ex = filter_gen(ex, dx, dy, xmin, xmax)
    ey = bo_file_load(path + '/data/Ey2D', iteration, nx, ny, verbose = 1)
#    ey = filter_gen(ey, dx, dy, xmin, xmax)
    ez = bo_file_load(path + '/data/Ez2D', iteration, nx, ny, verbose = 1)
#    ez = filter_gen(ez, dx, dy, xmin, xmax)
#    bx = bo_file_load(path + '/data/Bx2D' , iteration, nx, ny, verbose = 1)
#    print bx.shape
#    bx = filter_gen(bx, dx, dy, xmin, xmax)
#    by = bo_file_load(path + '/data/By2D', iteration, nx, ny, verbose = 1)
#    by = filter_gen(by, dx, dy, xmin, xmax)
#    bz = bo_file_load(path + '/data/Bz2D', iteration, nx, ny, verbose = 1)
#    bz = filter_gen(bz, dx, dy, xmin, xmax)
    elx = bo_file_load(path + '/data/Electron2Dx', iteration, nx, ny, verbose = 1)
#    elx = filter_gen(elx, dx, dy, xmin, xmax)
    ely = bo_file_load(path + '/data/Electron2Dy', iteration, nx, ny, verbose = 1)
#    ely = filter_gen(ely, dx, dy, xmin, xmax)
    elz = bo_file_load(path + '/data/Electron2Dz', iteration, nx, ny, verbose = 1)
#    elz = filter_gen(elz, dx, dy, xmin, xmax)
    phx = bo_file_load(path + '/data/Photon2Dx', iteration, nx, ny, verbose = 1)
#    phx = filter_gen(phx, dx, dy, xmin, xmax)
    phy = bo_file_load(path + '/data/Photon2Dy', iteration, nx, ny, verbose = 1)
#    phy = filter_gen(phy, dx, dy, xmin, xmax)
    phz = bo_file_load(path + '/data/Photon2Dy', iteration, nx, ny, verbose = 1)
#    phz = filter_gen(phy, dx, dy, xmin, xmax)
    posx = bo_file_load(path + '/data/Positron2Dx', iteration, nx, ny, verbose = 1)
#    posx = filter_gen(posx, dx, dy, xmin, xmax)
    posy = bo_file_load(path + '/data/Positron2Dy', iteration, nx, ny, verbose = 1)
#    posy = filter_gen(posy, dx, dy, xmin, xmax)
    posz = bo_file_load(path + '/data/Positron2Dz', iteration, nx, ny, verbose = 1)
#    posz = filter_gen(posz, dx, dy, xmin, xmax)
    
#    ephi, er = convert_exy_toephi(ex,ey,dx,dy)
#    mm = np.amax(ez)
#    print np.amax(ephi)/mm,np.amax(er)/mm, mm
     
    fig = plt.figure(num=None, figsize=(10,5) )
    axx = fig.add_subplot(3,5,1)
    axx.set_title('E: x = 0')
    surf = axx.imshow(ex, extent=[-xm, xm, -ym, ym], origin='lower')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('y')
    axx.set_ylabel('x')
    axx.set_xlim([-xm1, xm1])
    axx.set_ylim([-xm1, xm1])
    
    axx = fig.add_subplot(3,5,6)
    axx.set_title('E: y = 0')
    surf = axx.imshow(ey, extent=[-ym, ym, -zm, zm], origin='lower')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('y')
    axx.set_ylabel('x')
    axx.set_xlim([-xm1, xm1])
    axx.set_ylim([-xm1, xm1])
    
    axx = fig.add_subplot(3,5,11)
    axx.set_title('E: z = 0')
    surf = axx.imshow(ez, extent=[-ym, ym, -zm, zm], origin='lower')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('y')
    axx.set_ylabel('x')
    axx.set_xlim([-xm1, xm1])
    axx.set_ylim([-xm1, xm1])
    
    '''axx = fig.add_subplot(3,5,2)
    axx.set_title('B: x = 0')
    surf = axx.imshow(bx, extent=[-xm, xm, -ym, ym], origin='lower')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('y')
    axx.set_ylabel('x')
    axx.set_xlim([-xm1, xm1])
    axx.set_ylim([-xm1, xm1])
    
    axx = fig.add_subplot(3,5,7)
    axx.set_title('B: y = 0')
    surf = axx.imshow(by, extent=[-ym, ym, -zm, zm], origin='lower')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('y')
    axx.set_ylabel('x')
    axx.set_xlim([-xm1, xm1])
    axx.set_ylim([-xm1, xm1])
    
    axx = fig.add_subplot(3,5,12)
    axx.set_title('B: z = 0')
    surf = axx.imshow(bz, extent=[-ym, ym, -zm, zm], origin='lower')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('y')
    axx.set_ylabel('x')
    axx.set_xlim([-xm1, xm1])
    axx.set_ylim([-xm1, xm1])'''
    
    axx = fig.add_subplot(3,5,3)
    axx.set_title('Electron: x = 0')
    surf = axx.imshow(elx, extent=[-xm, xm, -ym, ym], origin='lower')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('y')
    axx.set_ylabel('x')
    axx.set_xlim([-xm1, xm1])
    axx.set_ylim([-xm1, xm1])
    
    axx = fig.add_subplot(3,5,8)
    axx.set_title('Electron: y = 0')
    surf = axx.imshow(ely, extent=[-ym, ym, -zm, zm], origin='lower')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('y')
    axx.set_ylabel('x')
    axx.set_xlim([-xm1, xm1])
    axx.set_ylim([-xm1, xm1])
    
    axx = fig.add_subplot(3,5,13)
    axx.set_title('Electron: z = 0')
    surf = axx.imshow(elz, extent=[-ym, ym, -zm, zm], origin='lower')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('y')
    axx.set_ylabel('x')
    axx.set_xlim([-xm1, xm1])
    axx.set_ylim([-xm1, xm1])

    '''axx = fig.add_subplot(3,5,4)
    axx.set_title('Positron: x = 0')
    surf = axx.imshow(posx, extent=[-xm, xm, -ym, ym], origin='lower')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('y')
    axx.set_ylabel('x')
    axx.set_xlim([-xm1, xm1])
    axx.set_ylim([-xm1, xm1])
    
    axx = fig.add_subplot(3,5,9)
    axx.set_title('Positron: y = 0')
    surf = axx.imshow(posy, extent=[-ym, ym, -zm, zm], origin='lower')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('y')
    axx.set_ylabel('x')
    axx.set_xlim([-xm1, xm1])
    axx.set_ylim([-xm1, xm1])
    
    axx = fig.add_subplot(3,5,14)
    axx.set_title('Positron: z = 0')
    surf = axx.imshow(posz, extent=[-ym, ym, -zm, zm], origin='lower')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('y')
    axx.set_ylabel('x')
    axx.set_xlim([-xm1, xm1])
    axx.set_ylim([-xm1, xm1])'''

    
    axx = fig.add_subplot(3,5,5)
    axx.set_title('Photon: x = 0')
    surf = axx.imshow(phx, extent=[-xm, xm, -ym, ym], origin='lower')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('y')
    axx.set_ylabel('x')
    axx.set_xlim([-xm1, xm1])
    axx.set_ylim([-xm1, xm1])
    
    axx = fig.add_subplot(3,5,10)
    axx.set_title('Photon: y = 0')
    surf = axx.imshow(phy, extent=[-ym, ym, -zm, zm], origin='lower')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('y')
    axx.set_ylabel('x')
    axx.set_xlim([-xm1, xm1])
    axx.set_ylim([-xm1, xm1])
    
    axx = fig.add_subplot(3,5,15)
    axx.set_title('Photon: z = 0')
    surf = axx.imshow(phz, extent=[-ym, ym, -zm, zm], origin='lower')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('y')
    axx.set_ylabel('x')
    axx.set_xlim([-xm1, xm1])
    axx.set_ylim([-xm1, xm1])

    plt.show()

'''    fig = plt.figure(num=None)
    axx = fig.add_subplot(3,4,1)
    surf = axx.imshow(ex_x, extent=[-ym, ym, -zm, zm], origin='lower', aspect = 'auto')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('y')
    axx.set_ylabel('z')

    axx = fig.add_subplot(3,4,2)
    surf = axx.imshow(ex_y, extent=[-ym, ym, -zm, zm], origin='lower', aspect = 'auto')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('y')
    axx.set_ylabel('z')

    axz = fig.add_subplot(3,4,3)
    surf = axz.imshow(ex_z, extent=[-ym, ym, -zm, zm], origin='lower', aspect = 'auto')
    plt.colorbar(surf, orientation  = 'vertical')
    axz.set_xlabel('y')
    axz.set_ylabel('z')
    
    axa = fig.add_subplot(3,4,4)
    surf = axa.imshow(ex_a, extent=[-ym, ym, -zm, zm], origin='lower', aspect = 'auto')
    plt.colorbar(surf, orientation  = 'vertical')
    axa.set_xlabel('y')
    axa.set_ylabel('z')
    
    axx = fig.add_subplot(3,4,5)
    surf = axx.imshow(ey_x, extent=[0, xm, 0, zm], origin='lower', aspect = 'auto')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('x')
    axx.set_ylabel('z')

    axx = fig.add_subplot(3,4,6)
    surf = axx.imshow(ey_y, extent=[0, xm, 0, zm], origin='lower', aspect = 'auto')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('x')
    axx.set_ylabel('z')

    axz = fig.add_subplot(3,4,7)
    surf = axz.imshow(ey_z, extent=[0, xm, 0, zm], origin='lower', aspect = 'auto')
    plt.colorbar(surf, orientation  = 'vertical')
    axz.set_xlabel('x')
    axz.set_ylabel('z')
    
    axa = fig.add_subplot(3,4,8)
    surf = axa.imshow(ey_a, extent=[0, xm, 0, zm], origin='lower', aspect = 'auto')
    plt.colorbar(surf, orientation  = 'vertical')
    axa.set_xlabel('x')
    axa.set_ylabel('z')
    
    axx = fig.add_subplot(3,4,9)
    surf = axx.imshow(ez_x, extent=[-xm, xm, -ym, ym], origin='lower', aspect = 'auto')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('x')
    axx.set_ylabel('y')

    axx = fig.add_subplot(3,4,10)
    surf = axx.imshow(ez_y, extent=[-xm, xm, -ym, ym], origin='lower', aspect = 'auto')
    plt.colorbar(surf, orientation  = 'vertical')
    axx.set_xlabel('x')
    axx.set_ylabel('y')

    axz = fig.add_subplot(3,4,11)
    surf = axz.imshow(ez_z, extent=[-xm, xm, -ym, ym], origin='lower', aspect = 'auto')
    plt.colorbar(surf, orientation  = 'vertical')
    axz.set_xlabel('x')
    axz.set_ylabel('y')
    
    axa = fig.add_subplot(3,4,12)
    surf = axa.imshow(ez_a, extent=[-xm, xm, -ym, ym], origin='lower', aspect = 'auto')
    plt.colorbar(surf, orientation  = 'vertical')
    axa.set_xlabel('x')
    axa.set_ylabel('y')
    '''

    
 
if __name__ == '__main__':
    main()
