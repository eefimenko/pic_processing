#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys
import os

def calc_current(array,nx,ny,dx,dy,r):
    num = 0.
    for i in range(nx):
        x = (i - nx/2)*dx
        for j in range(ny):
            y = (j-ny/2)*dy
            if x*x + y*y < r*r:
                num += array[i][j]
    return num

def main():
    filename = 'ez_b_ne_ncr_j'
    path = sys.argv[1] + '/'
    config = utils.get_config(path + "/ParsedInput.txt")
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    BOIterationPass = int(config['BOIterationPass'])
    TimeStep = float(config['TimeStep'])
    dt = BOIterationPass*TimeStep*1e15
    step = float(config['TimeStep'])*float(config['BOIterationPass'])
    omega = float(config['Omega'])
    T = 2*math.pi/omega
    nper = int(T/step)/2
    dv = 2*dx*dy*dz
    coeffj = 2. * 1.6e-19 * 3e8 /(2*dz*1e-2)*1e-6
    print 'J = ', coeffj
    ppw = float(config['PeakPowerPW'])
    ncr = utils.ElectronMass * omega * omega/(8. * math.pi * utils.ElectronCharge * utils.ElectronCharge)
    const_a_p = 7.81441e-9
    a0 = const_a_p * math.sqrt(ppw*1e22)
        
    print a0
    ncr *= a0
        
    nmin,nmax = utils.find_min_max_from_directory(path + utils.ezpath, path + utils.bzpath, path + utils.nezpath)

    ez_t = np.zeros(nmax)
    bz_t = np.zeros(nmax)
    ne_t = np.zeros(nmax)
    ncr_t = np.zeros(nmax)
    j_t = np.zeros(nmax)
    print 'min =', nmin, ' max =', nmax
           
    nmin = utils.read_tseries(path,filename, ez_t, bz_t, ne_t, ncr_t, j_t)
    for i in range(nmin, nmax):
        print i, 'of ', nmax
        read, fieldez_x = utils.bo_file_load(path + utils.ezpath,i,nx)
        ez_t[i] = np.amax(fieldez_x)
        read, fieldbz_x = utils.bo_file_load(path + utils.bzpath,i,nx)
        bz_t[i] = np.amax(fieldbz_x)
        
        read, ne_x = utils.bo_file_load(path + utils.nezpath,i,nx,ny)
        curr = calc_current(ne_x,nx,ny,dx,dy,1)
        ne_t[i] = np.amax(ne_x)/dv
        ncr_t[i] = ne_t[i]/ncr
        j_t[i] = curr

    utils.save_tseries(path,filename, nmin, nmax, ez_t, bz_t, ne_t, ncr_t, j_t)
     
if __name__ == '__main__':
    main()
