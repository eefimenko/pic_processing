#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys
import os
from matplotlib.patches import Ellipse

def calc_current(array,nx,ny,dx,dy,r):
    num = 0.
    for i in range(nx):
        x = (i - nx/2)*dx
        for j in range(ny):
            y = (j-ny/2)*dy
            if x*x + y*y < r*r:
                num += array[i][j]
    return num

def main():
    fontsize = 12
    mp.rcParams.update({'font.size': fontsize})
    filename = 'ez_b_ne_ncr_j'
    pairs = []
    for path in sys.argv[1:]:
        #path = sys.argv[1] + '/'
        config = utils.get_config(path + "/ParsedInput.txt")
        dx = float(config['Step_X'])
        dy = float(config['Step_Y'])
        dz = float(config['Step_Z'])
        nx = int(config['MatrixSize_X'])
        ny = int(config['MatrixSize_Y'])
        BOIterationPass = int(config['BOIterationPass'])
        TimeStep = float(config['TimeStep'])
        dt = BOIterationPass*TimeStep*1e15
        step = float(config['TimeStep'])*float(config['BOIterationPass'])
        omega = float(config['Omega'])
        T = 2*math.pi/omega
        nper = int(T/step)/2
        dv = 2*dx*dy*dz
        coeffj = 2. * 1.6e-19 * 3e8 /(2*dz*1e-2)*1e-6
        c2 = 3.33e-10*dx*dy*1e-6
        print 'J = ', coeffj
        ppw = float(config['PeakPowerPW'])
        number = int(config.get('FieldGenerator.Number', 0))
        ncr = utils.ElectronMass * omega * omega/(8. * math.pi * utils.ElectronCharge * utils.ElectronCharge)
        const_a_p = 7.81441e-9
        a0 = const_a_p * math.sqrt(ppw*1e22)
        
        print a0
        ncr *= a0
        f_read = open(path + "/ez_b_ne_ncr_j.txt", "r")
        last_line = f_read.readlines()[-1]
        nmin = 0
        nmax = int(last_line.split()[0]) + 1
        print(last_line.split()[0])
        #    nmin,nmax = utils.find_min_max_from_directory(path + utils.ezpath, path + utils.bzpath, path + utils.nezpath)
        
        ez_t = np.zeros(nmax)
        bz_t = np.zeros(nmax)
        ne_t = np.zeros(nmax)
        ncr_t = np.zeros(nmax)
        j_t = np.zeros(nmax)
        jplus = np.zeros(nmax)
        jminus = np.zeros(nmax)
        jfull = np.zeros(nmax)
        jmax = np.zeros(nmax)
        print 'min =', nmin, ' max =', nmax
        
        filename = 'ez_b_ne_ncr_j'   
        utils.read_tseries(path,filename, ez_t, bz_t, ne_t, ncr_t, j_t)
        filename = 'jz_anls_new'
        utils.read_tseries(path,filename, jplus, jminus, jfull, jmax)
        jmax_ = np.amax(abs(jfull[-200:]))*c2
        nemax_ = np.amax(ne_t[-200:])/1e25
        
        if 'nonlinear' not in path:
            desc = u'%d ПВт' %(int(ppw))
            marker = 'o'
        else:
            desc = u'%d ПВт, %d п.' %(int(ppw), number)
            marker = '*'
            #        print(np.amax(jfull[-300:])*c2, np.amax(ne_t[-300:]))
        pairs.append((jmax_, nemax_, desc, marker))
        
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    for p in pairs:
        ax.plot(p[0], p[1], marker=p[3], markersize=10)
        if p[2] == u'30 ПВт, 6 п.':
            plt.text(p[0]+1, p[1]-0,s=p[2], fontsize = 10)
        elif p[2] == u'30 ПВт, 10 п.':
            plt.text(p[0]+1, p[1]-1,s=p[2], fontsize = 10)
        else:
            plt.text(p[0]+1, p[1]-0.5,s=p[2], fontsize = 10)
            #    ax.plot(bz_t)
#    ax = fig.add_subplot(1,3,2)
#    ax.plot(ne_t)
#    ax = fig.add_subplot(1,3,3)
#    ax.plot(jmax)
#    ax.set_yscale('log')
    ax.set_ylim([0, 30])
    ax.set_xlim([0, 50])
    ax.set_ylabel(u'$N_e$, $\\times 10^{25}$ см$^{-3}$')
    ax.set_xlabel(u'$J_z$, МА')
    ax = plt.gca()
    plt.text(5, 5, u'Слои', color = 'green')
    ax.add_patch(Ellipse((8, 2), width=16, height=8, alpha = 0.1,angle=20,
                         edgecolor='green',
                         facecolor='green',
                         linewidth=5))
    plt.text(35, 28, u'Пинчевание', color = 'red')
    ax.add_patch(Ellipse((30, 20), width=44, height=26, alpha = 0.1,angle=-45,
                         edgecolor='red',
                         facecolor='red',
                         linewidth=5))
    
    plt.show()
    
    
if __name__ == '__main__':
    main()
