#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils
import os
import numpy as np

def main():
    picspath = 'pics'
    path = './'
    archive = os.path.join('BasicOutput', 'data.zip')
    
    config = utils.get_config(path + "ParsedInput.txt")
    nmin, nmax, delta = utils.get_min_max_iteration(sys.argv,utils.ezpath, archive=archive)
    
    if (len(sys.argv) > 1):
	nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    xmax = float(config['X_Max'])*1e4 #mkm
    xmin = float(config['X_Min'])*1e4 #mkm
    ymax = float(config['Y_Max'])*1e4 #mkm
    ymin = float(config['Y_Min'])*1e4 #mkm
    zmax = float(config['Z_Max'])*1e4 #mkm
    zmin = float(config['Z_Min'])*1e4 #mkm
    dt = float(config['TimeStep'])*1e15
    axis = utils.create_axis(nmax, dt)
    extent = [xmin, xmax, ymin, ymax]
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    power = float(config['PeakPowerPW'])
    dx = float(config['Step_X'])*1e4
    dy = float(config['Step_Y'])*1e4
    dz = float(config['Step_Z'])*1e4
    mult = 1./(2.*dx*dy*dz*1e-12)
    c2 = 3.33e-10*dx*dy*1e-8*1e-6
    filename = 'jz_anls_new'
    filename_center = 'jz_center'
    nmin,nmax = utils.find_min_max_from_archive_directory(utils.jz_zpath, 'txt', archive=archive)
    jplus = np.zeros(nmax)
    jminus = np.zeros(nmax)
    jfull = np.zeros(nmax)
    jmax = np.zeros(nmax)
    jcenter = np.zeros(nmax)
    print nmin, nmax

    nr = 10
    nmin = utils.read_tseries(path,filename, jplus, jminus, jfull, jmax, verbose = True, start = nmin)
    for i in range(nmin,nmax,delta):
        print i
        read, jz_x = utils.bo_file_load(utils.jz_zpath,i,nx, verbose=True, archive=archive)
        jpos = np.sum(jz_x.clip(min=0))
        jneg = np.sum(jz_x.clip(max=0))
        jfull[i] = jpos + jneg
        if jfull[i] > 0:
            jplus[i] = jpos
            jminus[i] = abs(jneg)
        else:
            jplus[i] = abs(jneg)
            jminus[i] = jpos
        
        jmax[i] = np.amax(jz_x)
    
    utils.save_tseries(path,filename, nmin, nmax, jplus, jminus, jfull, jmax)
    nmin,nmax = utils.find_min_max_from_archive_directory(utils.jz_zpath, 'txt', archive=archive)
    nmin = utils.read_tseries(path,filename_center, jcenter, start = nmin, verbose = True)
    for i in range(nmin,nmax,delta):
        print nmin, i
        read, jz_x = utils.bo_file_load(utils.jz_zpath,i,nx,ny, verbose = True, archive=archive)
        sum_ = 0
        print jz_x.shape
        (nx_, ny_) = jz_x.shape
        for j in range(nx_/2-nr, nx_/2+nr):
            for k in range(ny_/2-nr, ny_/2+nr):
                r_ = math.sqrt((j-nx_/2)**2 + (k - ny_/2)**2)
                if r_ < nr:
                    sum_ += jz_x[j,k]
        jcenter[i] = np.abs(sum_)* c2
        print jcenter[i]
    
    utils.save_tseries(path,filename_center,nmin, nmax, jcenter)
    
    jplus_e = utils.env(jplus, axis)
    jminus_e = utils.env(jminus, axis)
    jfull_e = utils.env(jfull, axis)
    fig = plt.figure()
    ax = fig.add_subplot(1,3,1)
    ax.plot(jplus, 'r')
    ax.plot(jminus, 'b')
    ax.plot(jfull, 'g')
    ax.plot(jplus_e, 'r')
    ax.plot(jminus_e, 'b')
    ax.plot(jfull_e, 'g')
    ax = fig.add_subplot(1,3,2)
    ax.plot(jmax, 'r')
    ax = fig.add_subplot(1,3,3)
    ax.plot(jcenter, 'r')
    plt.show()
    plt.close()

    
if __name__ == '__main__':
    main()
