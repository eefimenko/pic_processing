#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import shutil
import os
import scipy.signal.signaltools as sigtool
import numpy 
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

def read_ang(file,nx,ny,mult=1.):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index]*mult)
        field.append(row)
    return field

def norm(a, mult=1):
    m = max(a)
    for i in range(len(a)):
        a[i] *= mult/m
    return a


def read_field(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)/norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1e-9)
        field.append(row)
    return field

def read_file(file, de):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
    if 'tr' in file and not 'fs' in file:
        array[0] = 0
#        print 'removed'
        array, de = adjust_array(array, de)
        
    for i in range(len(array)):
        nph  += array[i]
    for i in range(len(array)):
        array[i] *= (i+0.425)
   
    return array, nph

def read_file_sph(file, de):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
    if 'tr' in file and not 'fs' in file:
        array[0] = 0
#        print 'removed'
        array, de = adjust_array(array, de)
       
    for i in range(len(array)):
        nph  += array[i]/(i+0.425)/de
    for i in range(len(array)):
        array[i] /= de
#        array[i] = array[i]/(i+0.425)/de
   
    return array, nph

def check_length(file):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
       
    return len(array)

def adjust_array(array, de):
    n = len(array)
    adj_size = 10
    m = n/adj_size
    a = [0]*m
    for i in range(m):
        tmp = 0
        for j in range(adj_size):
            tmp += array[i*adj_size + j]
        a[i] = tmp
    return a, de*adj_size

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def find_max_energy_full(array,step):
    n = len(array)
    full = 0
    imax = 0
    summ = 0
    nfull = 0
    av_en = 0
    n_av = 0
    for i in range(n):
        nfull += array[i]/(i+0.425)
        full += array[i]*step
    
    if nfull > 0:
        av_en = full/nfull
        n_av = int(av_en/step)

    for i in range(1,n):
        summ = summ + array[-i]*(n-i+0.425)*step
        if summ > 0.01*full:
            imax = i
            break
    if imax == 0:
        max_en = 0
    else:
        max_en = (n-imax+0.37) * step
#    en1 = max_en*1e-3
#    ratio = en1/step
#    print max_en, av_en
    ratio = 1
    return av_en*1000, max_en, array[n_av], array[-imax]

def read_one_spectrum_sph(path1, nmin):
    config = utils.get_config(path1 + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV']) 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
#    print num, T
    nph_t = []
    nph1_t = []
    ne_t = []
    np_t = []
    step = x0*y0*1e15/T 
    
    dt = y0*float(config['QEDstatistics.OutputIterStep'])*1e15/T

    path = '/statdata/ph/EnSpSph/'
#    nepath = '/data/NeTrap/'
#    nppath = '/data/NposTrap/'
    emin = float(config['QEDstatistics.Emin']) 
    emax = float(config['QEDstatistics.Emax']) 
    ne = int(config['QEDstatistics.OutputN_E'])
    de = (emax - emin)/ne
   
    t = 0
    
    n = check_length(path1 + path + '%.4f.txt'%nmin)
    if 'tr' in path1 and not 'fs' in path1:
        n /= 10
        de *= 10
    axis = create_axis(n, de/ev*1e-9)

    spectrum1 = [0]*n
    nn = 0
    for i in range(nmin,nmin+num):
        name = path1 + path + '%.4f.txt'%i
#        print name
        sp1, nph = read_file_sph(name, de)
        nn += nph
        for k in range(n):
            if nph > 0:
                spectrum1[k] += sp1[k]/nph
    for k in range(n):
        spectrum1[k] /= num
    return spectrum1, axis

def read_one_spectrum(path1, nmin):
    config = utils.get_config(path1 + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV']) 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
#    print num, T
    nph_t = []
    nph1_t = []
    ne_t = []
    np_t = []
    step = x0*y0*1e15/T 
    
    dt = y0*float(config['QEDstatistics.OutputIterStep'])*1e15

    path = '/statdata/ph/EnSp/'
#    nepath = '/data/NeTrap/'
#    nppath = '/data/NposTrap/'
    emin = float(config['QEDstatistics.Emin']) 
    emax = float(config['QEDstatistics.Emax']) 
    ne = int(config['QEDstatistics.OutputN_E'])
    de = (emax - emin)/ne
   
    t = 0
    
    n = check_length(path1 + path + '%.4f.txt'%nmin)
    if 'tr' in path1 and not 'fs' in path1:
        n /= 10
        de *= 10
    axis = create_axis(n, de/ev*1e-9)

    spectrum1 = [0]*n
    nn = 0
    for i in range(nmin,nmin+num):
        name = path1 + path + '%.4f.txt'%i
#        print name
        sp1, nph = read_file(name, de)
        nn += nph
        for k in range(n):
            if nph > 0:
                spectrum1[k] += sp1[k]/nph
    for k in range(n):
        spectrum1[k] /= n
    return spectrum1, axis

def max2d(array):
    return max([max(x) for x in array])

def sum_ang_energy(array):
    nx = len(array)
    ny = len(array[0])
    res = 0
    for i in range(nx):
        for j in range(ny):
            res = res + array[i][j]
    return res

def main():
    picspath = 'pics'
    path = '.'
    enpath = '/statdata/ph/EnSp/'
    angpath = '/statdata/ph/angSpSph/'
    angpath_el = '/statdata/el/angSpSph/'
    angpath_pos = '/statdata/el/angSpSph/'
    ezpath = 'data/E2z'
    nexpath = 'data/Electron2Dx'
    config = utils.get_config(path + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV'])
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    dv = 2*dx*dy*dz 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
    dt = (x0*y0*1e15)/T
    num = len(sys.argv)

    ax_ = []
    ax1_ = []
    ax2_ = []
    ez_t = []
    ne_t = []
    pow_t = []
    max_t = []
    av_t = []
    br_full_t = []
    br_100mev_t = []
    br_1gev_t = []
    br_full1_t = []
    br_100mev1_t = []
    br_1gev1_t = []
   
    t = 0
    omega = float(config['Omega'])
    T = 2*math.pi/omega*1e15
    phi = int(config['QEDstatistics.OutputN_phi'])
    theta = int(config['QEDstatistics.OutputN_theta'])
    nit = int(config['QEDstatistics.OutputIterStep'])
    dt1 = float(config['TimeStep'])*nit
    wl = float(config['Wavelength'])
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    dv = 2*dx*dy*dz
    electronMass = 9.10938e-28
    electronCharge = 4.8032e-10
    ncr = electronMass * omega * omega/(8. * math.pi * electronCharge * electronCharge)
    const_a_p = 7.81441e-9
    a0 = const_a_p * math.sqrt(ppw*1e22)
    print a0
    mp.rcParams.update({'font.size': 16})
    S = 2 * math.pi * 0.1 * 0.1 * wl * wl * 100 # square of source mm^2, r = 0.1 lambda 
    ds = 1.5e4 # mrad^2
    f = open('br_t.dat', 'r')
    i = 0
    for line in f:
        tmp = line.split()
        ax_.append(float(tmp[0])/T)
        ez_t.append(float(tmp[1]))
#        i += 1
#        if i < 26:
#            continue
        pow_t.append(float(tmp[2]))
        max_t.append(float(tmp[3]))
        av_t.append(float(tmp[4]))
        ax1_.append((float(tmp[0])-39*dt1*1e15)/T)
        br_full_t.append(float(tmp[5]))
        br_100mev_t.append(float(tmp[6]))
        br_1gev_t.append(float(tmp[7]))
        br_full1_t.append(float(tmp[5])/S/ds)
        br_100mev1_t.append(float(tmp[6])/S/ds)
        br_1gev1_t.append(float(tmp[7])/S/ds)
    f = open('t_ez_ne.dat')
    for line in f:
        tmp = line.split()
        ax2_.append(float(tmp[0])*T)
        ne_t.append(float(tmp[2]))
 
    fig = plt.figure(num=None)
    mp.rcParams.update({'font.size': 12})
    picname = picspath + '/' + "kim_br_t.png"
    ax = fig.add_subplot(2,1,1)
    
    f, = ax.plot(ax1_, br_full_t, 'r', label = 'Full: %.0e'%max(br_full_t))
    f, = ax.plot(ax1_, br_100mev_t, 'g', label = '> 0.1 MeV: %.0e'%max(br_100mev_t))
    f, = ax.plot(ax1_, br_1gev_t, 'b', label = '> 1 GeV: %.0e'%max(br_1gev_t))
    ax.set_ylabel('Emittance, ph/s/mm^2/mrad^2')
    ax.set_xlabel('Time, T')
    ax.set_yscale('log')
    plt.legend(loc = 'upper left')
    ax1 = ax.twinx()
#    p, = ax1.plot(ax2_, ne_t, 'k')
#    ax1.set_ylabel('$N_e/N_{cr}$')
    ax1.set_ylabel('Photon flux, ph/s')
    f, = ax1.plot(ax1_, br_1gev_t, 'k')
    
#    ax1 = ax.twinx()
#    p, = ax1.plot(ax1_, br_full1_t, 'k')
#    p, = ax1.plot(ax1_, br_100mev1_t, 'k')
#    ax1.set_ylabel('Radiated power, PW')
    
    ax.set_xlim([0,25])
#    ax1.set_xlim([10,80])
#    ax1.set_ylim([1e-11,1])
#    ax = fig.add_subplot(3,1,2)
#    f, = ax.plot(ax1_, br_full1_t, 'r')
#    q, = ax.plot(ax1_, br_100mev1_t, 'b')
    

#    ax.set_ylabel('Brilliance, ph/s/mm^2/mrad^2')
#   ax.set_yscale('log')
#    ax1.set_yscale('log')
#    ax1.set_xlim([10,80])
#    ax.set_xlim([10,80])
#    plt.legend([f, q, p], ["br full", "br 100m", "ne"], loc='upper left')
#    ax1.set_yscale('log')
    ax = fig.add_subplot(2,1,2)
    f, = ax.plot(ax1_, max_t, 'r')
    ax.set_ylabel('Max photon energy, GeV')
    ax.set_xlabel('Time, T')
    ax1 = ax.twinx()
    ez_t = norm(ez_t, max(pow_t))
    f1, = ax1.plot(ax_, ez_t, 'k')
    p1, = ax1.plot(ax1_, pow_t, 'g')
    ax1.set_ylabel('$\gamma$-source power, PW')
    ax1.set_xlim([0,25])
    ax.set_xlim([0,25])
    plt.legend([f, f1, p1], ["Max photon energy", '|Ez|', '$\gamma$-source power'], loc='upper left')
#    plt.show()
    plt.tight_layout()
    plt.savefig(picname)
    plt.close()

   
if __name__ == '__main__':
    main()
