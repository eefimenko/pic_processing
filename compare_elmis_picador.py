#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
#import numpy as np
import math
import utils
import sys
import numpy as np
import os

def main():
    num = len(sys.argv) - 1
    fig = plt.figure(num=None, figsize=(10,5) )
    ax1 = fig.add_subplot(2,2,2)
    ax1.set_title('E')
#    ax1.set_ylim([0,5e12])
    ax2 = fig.add_subplot(2,2,1)
    ax2.set_title('B')
#    ax2.set_ylim([0,5e12])
    ax3 = fig.add_subplot(2,2,3)
    ax3.set_title('Ne')
#    ax4 = fig.add_subplot(2,2,4)
#    ax4.set_title('Npos')
    for i in range(num):
        path = sys.argv[i+1]
        if 'elmis' in path:
            dx = 8.e-4/512
            dy = 8.e-4/512
            dz = 0.01e-4
            dt = 0.02 # fs
        else:
            config = utils.get_config(path + "/ParsedInput.txt")
            dx = float(config['Step_X'])
            dy = float(config['Step_Y'])
            dz = float(config['Step_Z'])
            dt = float(config['TimeStep'])*1e15
        dv = 2*dx*dy*dz
        nmin,nmax = utils.find_min_max_from_directory(path + '/data/B2z')
        E = np.zeros(nmax)
        B = np.zeros(nmax)
        Ne = np.zeros(nmax)
        taxis = np.zeros(nmax)
        for i in range(nmax):
            taxis[i] = i*dt

        nmin = utils.read_tseries(path,'e_b_ne_cmp', E, B, Ne)
        for iteration in range(nmin,nmax):
            ez = utils.bo_file_load(path + '/data/E2z', iteration, verbose = 1)
            bz = utils.bo_file_load(path + '/data/B2z', iteration, verbose = 1)
            nez = utils.bo_file_load(path + '/data/Electron2Dz', iteration, verbose = 1)
#            npz = utils.bo_file_load(path + '/data/Positron2Dz', iteration, verbose = 1)
            E[iteration] = np.amax(ez)
            B[iteration] = np.amax(bz)
            Ne[iteration] = np.amax(nez)/dv
#            Np[iteration] = np.amax(npz)/dv
        utils.save_tseries(path,'e_b_ne_cmp', nmin, nmax, E, B, Ne)
#        print taxis.shape, E.shape
        ax1.plot(taxis, E)
        ax2.plot(taxis, B)
        if max(Ne) > 0:
            ax3.plot(taxis, Ne, label = path)
        ax3.set_yscale('log')
#        ax4.plot(Np)
#    plt.legend()
    plt.show()
        
if __name__ == '__main__':
    main()
