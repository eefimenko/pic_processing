#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from mayavi import mlab
import utils
import sys
from pylab import *

def read_field2d(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def max2d(array):
    return max([max(x) for x in array])

def read_field(file,nx,ny,mult=1.):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index]*mult)
        field.append(row)
    return field

def read_1dfield(file,nx,step,mult=1.):
    f = open(file, 'r')
    line = f.readline()
    tmp = [float(x) for x in line.split()]
    n = len(tmp)
    for i in range(n):
        tmp[i] = tmp[i]/(i+0.5)/step
    num = sum(tmp)
    return num,tmp

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def sum_energy(array,energy,step):
    nmin = int(energy/step)
    summ = 0
    for i in range(nmin, len(array)):
        summ = summ + array[i]
    return summ

def find_max_energy_sph(array,step):
    n = len(array)
    full = 0
    imax = 0
    summ = 0
    nfull = 0
    av_en = 0
    n_av = 0
    for i in range(n):
        nfull += array[i]
        full += array[i]*step*(i+0.5)
    
    if nfull > 0:
        av_en = full/nfull
        n_av = int(av_en/step)

    for i in range(1,n):
        summ = summ + array[-i]*(n-i+0.5)*step
        if summ > 0.01*full:
            imax = i
            break
    if imax == 0:
        max_en = 0
    else:
        max_en = (n-imax+0.5) * step
#    en1 = max_en*1e-3
#    ratio = en1/step
#    print max_en, step, en1, ratio, imax
    ratio = 1
    return av_en, max_en, array[n_av], array[-imax]    

def sum_phi(array):
    nx = len(array)
    ny = len(array[0])
    res = [0] * nx
    i_05 = 0
    for i in range(nx):
        for j in range(ny):
            res[i] = res[i] + array[i][j]
    m = max(res)
    if m > 0:
        for i in range(nx):
            res[i] = res[i]/m
    return res

def find_angle_size(array):
    nx = len(array)
    ny = len(array[0])
    print nx, ny
    res = [0] * ny
    res1 = [0] * int(ny/2)
    j_05_1 = 0
    j_05_2 = 0
    k_05_1 = 0
    k_05_2 = 0
    jmax = 0
    i = nx/4
    
    for j in range(ny):
        res[j] = array[i][j]
    m = max(res)
    if m > 0:
        for j in range(ny):
            res[j] /= m
        for j in range(ny - 1):
            if res[j] < 0.5 and res[j+1] > 0.5:
                j_05_1 = j + (0.5-res[j])/(res[j+1] - res[j])
            if res[j] > 0.5 and res[j+1] < 0.5:
                j_05_2 = j + (0.5-res[j])/(res[j+1] - res[j])
    i = 3*nx/4
    
    for j in range(ny):
        res[j] = array[i][j]
    m = max(res)
    if m > 0:
        for j in range(ny):
            res[j] /= m
        for j in range(ny - 1):
            if res[j] < 0.5 and res[j+1] > 0.5:
                k_05_1 = j + (0.5-res[j])/(res[j+1] - res[j])
            if res[j] > 0.5 and res[j+1] < 0.5:
                k_05_2 = j + (0.5-res[j])/(res[j+1] - res[j])
#    if m > 0:
#        for j in range(ny):
#            res[j] = array[i][j]/m
#            if j < ny/2:
#                res1[j] = res[j]
#                print res[j], j
#        m1 = max(res)
#        for j in range(ny/2):
#            if res[j] == m1:
#                jmax = j
#           if res[j] < 0.5 and res[j+1] > 0.5:
#               j_05_1 = j + (0.5-res[j])/(res[j+1] - res[j])
    return res, j_05_1*180./ny, j_05_2*180./ny, k_05_1*180./ny, k_05_2*180./ny 
   
def create_th_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def sum_ang_energy(array):
    nx = len(array)
    ny = len(array[0])
    res = 0
    for i in range(nx):
        for j in range(ny):
            res = res + array[i][j]
    return res

def main():
    path = sys.argv[1]
    
    picspath = 'pics'
    angpath = '/statdata/ph/angSpSph/'
    enpath = '/statdata/ph/EnSpSph/'
    ezpath = '/data/E2z'

    config = utils.get_config(path + "/ParsedInput.txt")
    ev = float(config['eV'])
    phi = int(config['QEDstatistics.OutputN_phi'])
    theta = int(config['QEDstatistics.OutputN_theta'])
    dphi = 2*math.pi/phi
    dtheta = math.pi/theta
    wl = float(config['Wavelength'])
#    S = 4 * math.pi * 0.5 * 0.5 * 0.8e-3 * 0.8e-3 # square of source mm^2, r = 0.5 lambda
    S = 2 * math.pi * 0.1 * 0.1 * wl * wl * 100 # square of source mm^2, r = 0.1 lambda 
    ds = 1.5e4 # mrad^2
    emin = float(config['QEDstatistics.Emin'])
    emax = float(config['QEDstatistics.Emax'])
    tmin = float(config['QEDstatistics.ThetaMin'])
    tmax = float(config['QEDstatistics.ThetaMax'])
    pmin = 0.
    pmax = 360.
    ne = int(config['QEDstatistics.OutputN_E'])
    nit = int(config['QEDstatistics.OutputIterStep'])
    dt = float(config['TimeStep'])*nit
    omega = float(config['Omega'])
    nx1 = int(config['MatrixSize_X'])
    ny1 = int(config['MatrixSize_Y'])
    T = 2 * math.pi/omega
    nnn = int(T/dt)
    de = emax*1e-9/ev/ne #GeV
    print S, ds, 1./(S*ds)
    nmin = int(sys.argv[2])
    nmax = int(sys.argv[3])
#    nmin = 0	
#    nmax = utils.num_files(path + angpath)
#    nmax = 1000   
#    nmax = nmin + nnn 
#    nmin = n
#    nmax = n+1
#    nmax = 390
    br_full = []
    br_100mev = []
    br_1Gev = []
    pow_t = []
    max_t = []
    av_t = []
    n_emax_t = []
    n_av_t = []
    num_old = 0
    ez_t = []
    angle_t = []
    angle1_t = []
    ang = read_field(path + angpath + '%.4f.txt' % (nmin,), phi, theta)
    result, j_05_1, j_05_2, k_05_1, k_05_2  = find_angle_size(ang)
    res = [0]*len(result)
    print nmin, nmax
   
    for n in range(nmin,nmax):
        print path + angpath + '%.4f.txt' % (n,)
        ang = read_field(path + angpath + '%.4f.txt' % (n,), phi, theta)
        num, energy = read_1dfield(path + enpath + '%.4f.txt' % (n,), ne, emax/ne)
        ezname = path + ezpath + '/' + "%06d.txt" % (n,)
        ez = read_field2d(ezname,nx1,ny1)
        ezv = ez[nx1/2][ny1/2]
        ez_t.append(ezv)
        axis = create_axis(ne, emax*1e-9/ev/ne)
        fig = plt.figure(num=None)
        mp.rcParams.update({'font.size': 8})
#        ax = fig.add_subplot(1,3,1)
#        surf = ax.imshow(ang, extent =[0, 1, 0, 2])
#        plt.colorbar(surf, orientation  = 'vertical')
#        ax = fig.add_subplot(1,3,2)
#        surf = ax.plot(axis, energy)
#        ax.set_xlim(0,3)
        picname = picspath + '/' + "br%06d.png" % (n,)

        
#        summ_en = sum_phi(ang)
        result, j_05_1, j_05_2, k_05_1, k_05_2  = find_angle_size(ang)
        print 'Angle = ' + str(j_05_1) + ' ' + str(j_05_2)
#        for k in range(len(result)):
#            res[k] += result[k]/nnn
#        th_ax = create_th_axis(theta,2*math.pi/theta)
#        ax = fig.add_subplot(1,3,3, polar=True)
        angle = 0
        if j_05_2 < 0 or j_05_1 < 0 or j_05_2 < j_05_1:
            angle = 0
        else:
            if j_05_2 > 90 and j_05_1 < 90:
                angle = j_05_2 - j_05_1
            else:
                angle = 0
        angle1 = 0
        if k_05_2 < 0 or k_05_1 < 0 or k_05_2 < k_05_1:
            angle1 = 0
        else:
            if k_05_2 > 90 and k_05_1 < 90:
                angle1 = k_05_2 - k_05_1
            else:
                angle1 = 0
        angle_t.append(angle)
        angle1_t.append(angle1)

        th_ax = create_th_axis(theta,180./theta)
        ax = fig.add_subplot(3,1,1)
        mmm = max2d(ang)
        surf = ax.imshow(ang, extent = [0,180, pmin, pmax], norm = clr.LogNorm(mmm*1e-3, mmm), aspect = 'auto')
        plt.colorbar(surf)
        ax = fig.add_subplot(3,1,2)
        ax.plot(th_ax, result)
        ax.plot([j_05_1, j_05_2], [0.5,0.5])
#        ax.set_xlim([80,100])
       
        br_full.append(num/dt)
        num1 = sum_energy(energy, 0.1, emax*1e-9/ev/ne)
        br_100mev.append(num1/dt)
        num2 = sum_energy(energy, 1, emax*1e-9/ev/ne)
        br_1Gev.append(num2/dt)
        if angle > 0:
            ds = 2 * math.pi * (1 - math.cos(0.5*angle*math.pi/180.))*1e6 # mrad^2
        elif angle1 > 0:
            ds = 2 * math.pi * (1 - math.cos(0.5*angle1*math.pi/180.))*1e6 # mrad^2
        else:
            ds = 1e6
        power = sum_ang_energy(ang)/dt*1e-7*1e-15
        pow_t.append(power)
        av_energy, max_energy, n_av, n_emax = find_max_energy_sph(energy,de)
        max_t.append(max_energy)
        av_t.append(av_energy)
        n_emax_t.append(num/dt/S/ds)
        n_av_t.append(n_av/dt/S/ds)
        # Create a sphere
        r = 1
        pi = np.pi
        cos = np.cos
        sin = np.sin
        phi1, theta1 = np.mgrid[0.0:pi:theta*1j, 0.0:2.0*pi:phi*1j]
        x = r*sin(phi1)*cos(theta1)
        y = r*sin(phi1)*sin(theta1)
        z = r*cos(phi1)
        temp = cos(2*phi1)*cos(2*phi1)*sin(theta1)*sin(theta1)*cos(theta1)*cos(theta1)*(3*cos(theta1)*cos(theta1) -1);
        #Set colours and render
        ax = fig.add_subplot(1,1,1, projection='3d')
        mlab.mesh(x,y,z,temp)
#        ax.plot_surface(
#            x, y, z, temp)

#        ax.set_xlim([-1,1])
#        ax.set_ylim([-1,1])
#        ax.set_zlim([-1,1])
        ax.set_aspect("equal")
        plt.savefig(picname)
        plt.close()
        
    fig = plt.figure(num=None)
    mp.rcParams.update({'font.size': 8})
    picname = picspath + '/' + "dn%06d.png" % (nmin,)
    th_ax = create_th_axis(theta,180./theta)
    print theta, 180./theta
    ax = fig.add_subplot(1,1,1, polar=True)
#    ax.plot(th_ax, res, linestyle='-', marker='x')
    ax.plot(th_ax, res)
   
    plt.savefig(picname)
    plt.close()

    fig = plt.figure(num=None)
    mp.rcParams.update({'font.size': 8})
    picname = picspath + '/' + "br_t.png"
    ax = fig.add_subplot(4,1,1)
    axis_t = create_axis(nmax-nmin, dt*1e15)
    f, = ax.plot(axis_t, br_full, 'r')
    ax1 = ax.twinx()
    f, = ax.plot(axis_t, n_emax_t, 'b')
#    m, = ax.plot(axis_t, br_100mev, 'b')
#    g, = ax.plot(axis_t, br_1Gev, 'g')
    print max(br_full), max(br_100mev), max(br_1Gev), max(br_full)/S/ds, max(br_100mev)/S/ds, max(br_1Gev)/S/ds 
    ax.set_ylabel('Photon flux, ph/s')
#    ax.set_yscale('log')
    ax1 = ax.twinx()
    p, = ax1.plot(axis_t, pow_t, 'k')
#    pow_av = 0
#    num = 30
#    for i in range(num):
#        pow_av = pow_av + pow_t[-i]
#    pow_av = pow_av/num
#    print pow_av
    ax1.set_ylabel('Radiated power, PW')
#    plt.legend([f, m, g, p], ["full", ">100MeV", '>1GeV', 'Power'], loc=3)

    ax = fig.add_subplot(4,1,2)
    f, = ax.plot(axis_t, n_emax_t, 'r')
    q, = ax.plot(axis_t, n_av_t, 'b')
    print max(n_emax_t), max(n_av_t) 
    ax.set_ylabel('Brilliance, ph/s/mm^2/mrad^2')
#    ax.set_yscale('log')
    ax1 = ax.twinx()
    p, = ax1.plot(axis_t, max_t, 'k')
    e, = ax1.plot(axis_t, av_t, 'g')
    ax1.set_ylabel('Max photon energy, GeV')
    plt.legend([f, q, p, e], ["br max", "br max", "max energy", 'av energy'], loc=3)
#    ax1.set_yscale('log')
    ax = fig.add_subplot(4,1,3)
    f, = ax.plot(axis_t, n_emax_t, 'r')
    ax1 = ax.twinx()
    f1, = ax1.plot(axis_t, ez_t, 'g')
    plt.legend([f, f1], ["br", 'Ez'], loc=3)
    ax = fig.add_subplot(4,1,4)
    f, = ax.plot(axis_t, angle_t, 'r')
    f, = ax.plot(axis_t, angle1_t, 'b')
    ax1 = ax.twinx()
    f1, = ax1.plot(axis_t, ez_t, 'g')
    plt.legend([f, f1], ["angle", 'Ez'], loc=3)
    plt.savefig(picname)
    plt.close()

    f = open('br_t.dat', 'w')
    for i in range(len(axis_t)):
        a = 0
        if angle_t[i] > 0:
            a = angle_t[i]
            ds = 2 * math.pi * (1 - math.cos(0.5*angle_t[i]*math.pi/180.))*1e6 # mrad^2
        elif angle1_t[i] > 0:
            a = angle1_t[i]
            ds = 2 * math.pi * (1 - math.cos(0.5*angle1_t[i]*math.pi/180.))*1e6 # mrad^2
        else:
            a = 0
            ds = 1e6
        f.write('%lf %le %le %lf %lf %le %le %le %le %le %le %lf\n'% (axis_t[i], ez_t[i], pow_t[i], max_t[i], av_t[i], br_full[i], br_100mev[i], br_1Gev[i], br_full[i]/S/ds, br_100mev[i]/S/ds, br_1Gev[i]/S/ds, a))
    f.close()
#    plt.show()
if __name__ == '__main__':
    main()
