#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mpl
import numpy as np
import utils
import sys
from multiprocessing.dummy import Pool as ThreadPool 
from pylab import *
reload(sys)
sys.setdefaultencoding('utf8')

ppath = ''
ne = 0
nt = 0

def write_field2d(a, file):
    f = open(file, 'w')
    for j in range(len(a[0])):
        for i in range(len(a)):
            f.write('%e ' % a[i][j])
    f.close()

def read_field2d(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    array = np.fromfile(f, sep=' ')
    f.close()

    res = np.reshape(array, (nx,ny), order = 'F')
    num = 0
    for i in range(len(res)):
        num += sum(res[i])/(i+0.425)

    if num != 0:
        res /= num

    return res

def read_saved(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    array = np.fromfile(f, sep=' ')
    f.close()
#    print file, len(array)
    res = np.reshape(array, (nx,ny), order = 'F')
    return res

def create_axis(n,step, x0):
    axis = []
#    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def sp_theta(a,min_energy=0, max_energy=sys.float_info.max, de=1.):
    res = np.zeros(len(a[0]))
#    print a[0]
#    print de, level
    for i in range(len(a)):
        energy = i*de
        if energy >= min_energy and energy < max_energy:
            for j in range(len(a[0])):
                res[j] += a[i][j]
    m = max(res)
#    print res
#    print m
    if m != 0:
        for i in range(len(res)):
            res[i] /= m
    return res

def br_theta(a,level,dth,dph):
    res = [0]*len(a[0])
    s = [0]*len(a[0])
    for i in range(len(a)):
        for j in range(len(a[0])):
            for j1 in range(j):
                res[j] += a[i][j1]
                s[j] += math.sin((j1+0.5)*dth)*dth*dph*1e6
    for j in range(len(res)):
        if s[j] > 0:
            res[j] /= s[j]
    return res

def sp_energy(a):
    res = [0]*len(a)
    for i in range(len(a)):
        for j in range(len(a[0])):
            res[i] += a[i][j]
    m = max(res)
    if m != 0:
        for i in range(len(res)):
            res[i] /= m
    return res

def find_max_nz(a):
    n = len(a)
    for i in range(1,len(a)):
        if a[-i] > 1e-8:
            n = len(a)-i
            break
    return n

def smooth(a,factor):
    n = len(a)
    n1 = n/factor
#    if n1%factor != 0:
#        print 'Error'
    res = [0] * n1
    for i in range(n1):
        for j in range(factor):
            res[i] += a[i*factor + j]/factor
    return res

def find_angle_max(a,dt):
    res = [0]*len(a)
    for i in range(len(a)):
        maxj = 0
        maxn = 0.
        for j in range(len(a[0])):
            if a[i][j] > maxn:
                maxj = j
                maxn = a[i][j]
#                print 'here', a[i][j], maxn, maxj, j
        res[i] = maxj*dt
#        print i, maxj, res[j]
#    print res
    return res

def smooth_2d(a,factor_ne,factor_nt):
    ne = len(a)
    nt = len(a[0])
    nt1 = nt/factor_nt
    ne1 = ne/factor_ne
#    print ne, nt, ne1, nt1
    res = np.zeros((ne1,nt1))

    for j in range(ne1):
        for i in range(nt1):
            for k in range(factor_nt):
                for m in range(factor_ne):
                    res[j][i] += a[j*factor_ne+m][i*factor_nt+k]/factor_ne/factor_nt + 1e-15
    
    return res

def add_sp(res,a):
    for i in range(len(a)):
        for j in range(len(a[0])):
            res[i][j] += a[i][j]
    return res

def find_max_energy(array,step):
    n = len(array)
    full = np.sum(array)
    summ = 0
    imax = n

    for i in range(1,n):
        summ = summ + array[-i]
        if summ > 0.01*full:
            imax = i
            break

    return (n-imax) * step


def main():
    path = '.'
    
    picspath = '/home/evgeny/Dropbox/tmp_pics'
    elpath = '/statdata/el/EnAngSp/'
    phpath = '/statdata/ph/EnAngSp/'
    pospath = '/statdata/pos/EnAngSp/'

    num = len(sys.argv) - 1
    regime = sys.argv[1]
    wtype = 1

    if regime == 'r':
        print 'read file'
    elif regime == 'rs':
        print 'read smoothed file'
    elif regime == 'c':
        print 'calculate file'
    elif regime == 'cs':
        print 'calculate smoothed file'
    else:
        print 'Wrong type, correct r - read or c - calculate, rs - read smoothed file, cs - calculate smoothed file'
        sys.exit(0)

    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(path + elpath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[2] 
        nmin = int(sys.argv[2])
        nmax = utils.num_files(path + elpath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[2] + ' to ' + sys.argv[3]
        nmin = int(sys.argv[2])
        nmax = int(sys.argv[3])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[2] + ' to ' + sys.argv[3] + ' with delta ' + sys.argv[4]
        nmin = int(sys.argv[2])
        nmax = int(sys.argv[3])
        delta = int(sys.argv[4])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax-nmin) + ' files'
  
    config = utils.get_config(path + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep'])
    ppw = int(config['PeakPowerPW'])

    ev = float(config['eV'])
    global ne,nt
    ne = int(config['QEDstatistics.OutputN_E'])
    nt = int(config['QEDstatistics.OutputN_theta'])
    tmax = float(config['QEDstatistics.ThetaMax'])*180./3.14159
    tmin = float(config['QEDstatistics.ThetaMin'])*180./3.14159
    emax = float(config['QEDstatistics.Emax'])/ev*1e-9
    emin = float(config['QEDstatistics.Emin'])/ev*1e-9

    de = (emax - emin)/ne
    dt = (tmax - tmin)/nt
    dth = dt * 3.14159/180. 
    dph = 2*3.14159
    factor_nt = 1 #for mom
    factor_ne = 1 #for mom

    nt1 = nt/factor_nt
    ne1 = ne/factor_ne
    axe = create_axis(ne, de, 0)
    axt = create_axis(nt, dt, tmin)
    axe_f = create_axis(ne/factor_ne, de*factor_ne, 0)
    axe_f2 = create_axis(ne/factor_ne, de*factor_ne, 0)

    axt_f = create_axis(nt/factor_nt, dt*factor_nt, tmin)

    summ = np.zeros((ne,nt)) 
    sphname = path + phpath + 'saved%d.txt' %(nmax-nmin)
    if regime == 'c':
        for i in range(nmin, nmax):
            phname = path + '/' + phpath + '/'+ '%.4f.txt' % (i)
            res = read_field2d(phname,ne,nt)
            summ = add_sp(summ, res)
        summ /= (nmax - nmin)
        write_field2d(summ,sphname)
    elif regime == 'r':
        summ = read_saved(sphname, ne,nt)
    else:
        print('Unknown regime')
        exit(-1)

    fig = plt.figure(num=None, figsize=(15, 5), tight_layout = True)
    
    print 'Av energy', np.sum(summ)*de
    summ /= np.amax(summ)
    factor_ne=1
    factor_nt=1
    axe = create_axis(ne/factor_ne, de*factor_ne, 0)
    axt = create_axis(nt/factor_nt, dt*factor_nt, tmin)
    axel = fig.add_subplot(1,3,1)
    axel.imshow(summ + 1e-30, aspect = 'auto',  extent = [tmin, tmax, emin, emax], origin = 'lower', norm=clr.LogNorm(summ.max()*1e-5, summ.max()),
                interpolation = 'bicubic')
    axel.set_ylabel('W, GeV')
    axel.set_xlabel(u'$\\theta$')
    axel.set_xlim([0,90])
    axel.set_ylim([0,1.5])

    s_t = sp_theta(summ, min_energy = 0.5, de=de)
    s1_t = sp_theta(summ, min_energy = 0.1, max_energy = 0.5, de=de)
    s2_t = sp_theta(summ, max_energy = 0.1, de=de)
    s_e = np.sum(summ, axis = 1)
    print 'Max energy ', find_max_energy(s_e,de)
    
    s_t_sin = np.zeros(len(s_t))
    s1_t_sin = np.zeros(len(s_t))
    s2_t_sin = np.zeros(len(s_t))
    for j in range(len(s_t)):
        if s_t[j] != 0:
            s_t_sin[j] = s_t[j]/math.sin((j+0.5)*dth)
        s1_t_sin[j] = s1_t[j]/math.sin((j+0.5)*dth)
        s2_t_sin[j] = s2_t[j]/math.sin((j+0.5)*dth)
    
    ax1 = fig.add_subplot(1,3,2)
    ax1.plot(axt, utils.savitzky_golay(s_t_sin,7,1), 'r', label = u'> 0.5 GeV')
    ax1.plot(axt, utils.savitzky_golay(s2_t_sin,7,1), 'g', label = u'0.1-0.5 GeV')
    ax1.plot(axt, utils.savitzky_golay(s1_t_sin,7,1), 'b', label = u'< 0.1 GeV')
    plt.legend(loc = 'upper left', frameon = False)
    ax1.set_xlim([0, 90.])
    m = 1.1*max([np.amax(s_t_sin), np.amax(s1_t_sin), np.amax(s2_t_sin)])
    ax1.set_ylim([1e-4*m, m])
    ax1.set_yscale(u'log')
    ax1.set_xlabel(u'$\\theta$')
    ax1.set_ylabel(u'dW/$\sin$ $\\theta$ d$\\theta$')
    
    ax1 = fig.add_subplot(1,3,3)

    ax1.plot(axe, s_e, 'r')
    ax1.set_yscale('log')
    m = 1.1*max(s_e)
    ax1.set_ylim([1e-4*m, m])
    plt.legend(loc = 'upper left', frameon = False)
    ax1.set_xlim([0, 1.5])
    ax1.set_ylabel(u'dW/d$\hbar\omega$')
    ax1.set_xlabel('$\hbar\omega$')
    
    picname = picspath + '/dn%d_%d_%dPW.png'%(nmax,nmin,ppw)
    plt.tight_layout()
    plt.savefig(picname)
    plt.close()

if __name__ == '__main__':
    main()


