#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np
import matplotlib as mp

def f(p):
    return 3./p**0.8 * 400. * p**0.5*0.511e-3

def main():
    fontsize = 10
    labelsize = 16
    msize = 3
    lw = 0.5
    n = 91
    pmax = 100
    pmin = 10
    dp = 1
    pav = [pmin + dp*i for i in range(n)]
    
    av_en = np.fromfile('en_av.dat', sep = ' ')
    # for i in range(len(av_en)):
    #     av_en[i] /= 0.511
    power = np.fromfile('power.dat', sep = ' ')
    wo_av_en = np.fromfile('wo_en_av.dat', sep = ' ')
    # for i in range(len(wo_av_en)):
    #     wo_av_en[i] /= 0.511
    wo_power = np.fromfile('wo_power.dat', sep = ' ')
    pwr_wo = np.fromfile('PWO.txt', sep = ' ')
    wo_max_en = np.fromfile('MaxSphWO.txt', sep = ' ')
    # for i in range(len(wo_max_en)):
    #     wo_max_en[i] /= 0.511e-3
    pwr = np.fromfile('P.txt', sep = ' ')
    max_en = np.fromfile('MaxSph.txt', sep = ' ')
    # for i in range(len(max_en)):
    #     max_en[i] /= 0.511e-3
    phenav = np.fromfile('PhEnav_.txt', sep = ', ')/3*0.511
    m1 = np.zeros(len(power))
    for i in range(len(power)):
        m1[i] = 0.6*math.sqrt(power[i])#/0.511e-3
    print len(pav),len(phenav)
    fig = plt.figure(num=None, figsize=(4., 3), dpi=512)
    mp.rcParams.update({'font.size': fontsize})
    ax1 = fig.add_subplot(1,1,1)
    ax1.plot(pwr_wo,wo_max_en, 'b^', fillstyle = 'none', markersize = msize)
    ax1.plot(pwr,max_en, 'b', linewidth = lw)
    ax1.plot(power, m1, 'b:', linewidth = lw)
    ax1.set_xscale('log')
    ax1.set_yscale('log')
    ax1.set_xlim([8,110])
    ax1.set_ylim([0.5,7.5])
    ax1.arrow(30,3.5,-15,0,color='k', head_width = 0.2)
    xticks = [10,30,70,100]
    xticklabels = ['$%d$'%(x) for x in xticks]
    ax1.set_xticks(xticks)
    ax1.set_xticklabels(xticklabels)
    yticks = [0.5,1.5,5]
    yticklabels = ['$%.1f$'%(x) for x in yticks]
    ax1.set_yticks(yticks)
    ax1.set_yticklabels(yticklabels)
    ax1.set_ylabel('$\\varepsilon_{max}, GeV$')
    ax1.set_xlabel('P,$PW$')
    ax1.text(5, 6.3, '$(a)$', fontsize = labelsize)
    ax2 = ax1.twinx()
    ax2.plot(power,av_en, 'r--', linewidth = lw)
    ax2.plot(pav,phenav, 'g-', linewidth = lw, dashes=[6,1,2,1])
    ax2.plot(wo_power,wo_av_en, 'rs', fillstyle = 'none', markersize = msize)
    ax2.arrow(55,83,30,0,color='k', head_width = 2.5)
    ax2.set_xscale('log')
    ax2.set_yscale('log')
    ax2.set_xlim([8,110])
    ax2.set_ylim([30,250])
    xticks = [10,20,50,100]
    xticklabels = ['$%d$'%(x) for x in xticks]
    ax2.set_xticks(xticks)
    ax2.set_xticklabels(xticklabels)
    yticks = [50,100,150,200]
    yticklabels = ['$%d$'%(x) for x in yticks]
    ax2.set_yticks(yticks)
    ax2.set_yticklabels(yticklabels)
    plt.tight_layout()
    ax2.set_ylabel('$\\varepsilon_{av}, MeV$')
    plt.tight_layout()
    
    plt.savefig('energy.png', dpi=512)
    plt.close()
    # 10 PW
    en = np.fromfile('En_10.txt', sep = ' ')
    art = np.fromfile('ART_10.txt', sep = ' ')
    art_s = utils.savitzky_golay(art, 21, 3, deriv=0, rate=1)
    calc = np.fromfile('Calc_10.txt', sep = ' ')
    calc_s = utils.savitzky_golay(calc, 21, 3, deriv=0, rate=1)
#    for i in range(len(en)):
#        en[i] /= 0.511e-3
    
    fig = plt.figure(num=None, figsize=(4., 3), dpi=512)
    mp.rcParams.update({'font.size': fontsize})
    ax1 = fig.add_subplot(1,1,1)
    p, = ax1.plot(en, art_s, 'r', linewidth = lw)
    p, = ax1.plot(en, calc_s, 'b-', linewidth = lw, dashes = [6,2])
    plt.minorticks_on()
    ax1.set_yscale('log')
    ax1.set_xlim([0,2.5])
    ax1.set_ylim([1e-5,0.4])
    ax1.set_xlabel('$\\varepsilon_\gamma, GeV$')
    ax1.set_ylabel('$dE/d\\varepsilon_\gamma$')
    yticks = [1e-1,1e-2,1e-3,1e-4,1e-5]
    yticklabels = ['$10^{-1}$','$10^{-2}$','$10^{-3}$','$10^{-4}$','$10^{-5}$']
    ax1.set_yticks(yticks)
    ax1.set_yticklabels(yticklabels)
    xticks = [0, 1, 2]
    xticklabels = ['$0$', '$1$', '$2$']
    # xticklabels = ['$%.1f$'%(x) for x in xticks]
    ax1.set_xticks(xticks)
    ax1.set_xticklabels(xticklabels)
    ax1.text(-0.4, 0.2, '$(b)$', fontsize = labelsize)
    axins = plt.axes([0.59, 0.64, .35, .3])
    axins.plot(en, art_s,'r', linewidth = lw)
    axins.plot(en, calc_s,'b-', linewidth = lw, dashes = [6,2])
    value = f(10) 
    axins.plot([value, value], [0, max(max(art),max(calc))], 'g-', linewidth = lw, dashes = [6,1,2,1])
    axins.set_xlim([0,1])
    axins.set_ylim([0,0.23])
    xticks = [0, 0.5, 1]
    xticklabels = ['$0$', '$0.5$', '$1$']
    yticks = [0.1, 0.2]
    yticklabels = ['$%.1f$'%(x) for x in yticks]
    plt.minorticks_on()
    axins.set_xticks(xticks)
    axins.set_xticklabels(xticklabels)
    axins.set_yticks(yticks)
    axins.set_yticklabels(yticklabels)
    plt.tight_layout()
    plt.savefig('spectra10.png', dpi=512)
    plt.close()
    # 30 PW
    en = np.fromfile('En_30.txt', sep = ' ')
    art = np.fromfile('ART_30.txt', sep = ' ')
    art_s = utils.savitzky_golay(art, 21, 3, deriv=0, rate=1)
    calc = np.fromfile('Calc_30.txt', sep = ' ')
    calc_s = utils.savitzky_golay(calc, 21, 3, deriv=0, rate=1)
    # for i in range(len(en)):
    #     en[i] /= 0.511e-3
    lw = 0.5   
    fig = plt.figure(num=None, figsize=(4., 3), dpi=512)
    mp.rcParams.update({'font.size': fontsize})
    ax1 = fig.add_subplot(1,1,1)
    # p, = ax1.plot(en, art, 'r', linewidth = lw)
    # p, = ax1.plot(en, calc, 'b-', linewidth = lw, dashes = [6,2])
    p, = ax1.plot(en, art_s, 'r', linewidth = lw)
    p, = ax1.plot(en, calc_s, 'b-', linewidth = lw, dashes = [6,2])
    
    plt.minorticks_on()
    ax1.set_yscale('log')
    ax1.set_xlim([0,4])
    ax1.set_ylim([1e-5,0.4])
    ax1.set_xlabel('$\\varepsilon_\gamma, GeV$')
    ax1.set_ylabel('$dE/d\\varepsilon_\gamma$')
    yticks = [1e-1,1e-2,1e-3,1e-4,1e-5]
    yticklabels = ['$10^{-1}$','$10^{-2}$','$10^{-3}$','$10^{-4}$','$10^{-5}$']
    ax1.set_yticks(yticks)
    ax1.set_yticklabels(yticklabels)
    xticks = [0, 1, 2, 3, 4]
    xticklabels = ['$%d$'%(x) for x in xticks]
    ax1.set_xticks(xticks)
    ax1.set_xticklabels(xticklabels)
    ax1.text(-0.64, 0.2, '$(c)$', fontsize = labelsize)
    axins = plt.axes([0.59, 0.64, .35, .3])
    axins.plot(en, art_s,'r', linewidth = lw)
    axins.plot(en, calc_s,'b-', linewidth = lw, dashes = [6,2])
    value = f(30) 
    axins.plot([value, value], [0, max(max(art),max(calc))], 'g-', linewidth = lw, dashes = [6,1,2,1])
    axins.set_xlim([0,1.5])
    axins.set_ylim([0,0.23])
    xticks = [0, 0.5, 1]
    xticklabels = ['$0$', '$0.5$', '$1$']
    yticks = [0.1, 0.2]
    yticklabels = ['$%.1f$'%(x) for x in yticks]
    plt.minorticks_on()
    axins.set_xticks(xticks)
    axins.set_xticklabels(xticklabels)
    axins.set_yticks(yticks)
    axins.set_yticklabels(yticklabels)
    plt.tight_layout()
    plt.savefig('spectra30.png', dpi=512)

    # 100 PW
    en = np.fromfile('En_100.txt', sep = ' ')
    art = np.fromfile('ART_100.txt', sep = ' ')
    art_s = utils.savitzky_golay(art, 31, 3, deriv=0, rate=1)
    calc = np.fromfile('Calc_100.txt', sep = ' ')
    calc_s = utils.savitzky_golay(calc, 31, 3, deriv=0, rate=1)
    # for i in range(len(en)):
    #     en[i] /= 0.511e-3
    lw = 0.5   
    fig = plt.figure(num=None, figsize=(4., 3), dpi=512)
    mp.rcParams.update({'font.size': fontsize})
    ax1 = fig.add_subplot(1,1,1)
    p, = ax1.plot(en, art_s, 'r', linewidth = lw)
    p, = ax1.plot(en, calc_s, 'b-', linewidth = lw, dashes = [6,2])
    plt.minorticks_on()
    ax1.set_yscale('log')
    ax1.set_xlim([0,7])
    ax1.set_ylim([1e-5,0.4])
    ax1.set_xlabel('$\\varepsilon_\gamma, GeV$')
    ax1.set_ylabel('$dE/d\\varepsilon_\gamma$')
    yticks = [1e-1,1e-2,1e-3,1e-4,1e-5]
    yticklabels = ['$10^{-1}$','$10^{-2}$','$10^{-3}$','$10^{-4}$','$10^{-5}$']
    ax1.set_yticks(yticks)
    ax1.set_yticklabels(yticklabels)
    xticks = [0, 2, 4, 6]
    xticklabels = ['$%d$'%(x) for x in xticks]
    ax1.set_xticks(xticks)
    ax1.set_xticklabels(xticklabels)
    ax1.text(-1.2, 0.2, '$(d)$', fontsize = labelsize)
    axins = plt.axes([0.59, 0.64, .35, .3])
    axins.plot(en, art_s,'r', linewidth = lw)
    axins.plot(en, calc_s,'b-', linewidth = lw, dashes = [6,2])
    value = f(100) 
    axins.plot([value, value], [0, max(max(art),max(calc))], 'g-', linewidth = lw, dashes = [6,1,2,1])
    axins.set_xlim([0,2.5])
    axins.set_ylim([0,0.23])
    plt.minorticks_on()
    xticks = [0, 1, 2]
    xticklabels = ['$%d$'%(x) for x in xticks]
    yticks = [0.1, 0.2]
    yticklabels = ['$%.1f$'%(x) for x in yticks]
    axins.set_xticks(xticks)
    axins.set_xticklabels(xticklabels)
    axins.set_yticks(yticks)
    axins.set_yticklabels(yticklabels)
    plt.tight_layout()
    plt.savefig('spectra100.png', dpi=512)
    plt.close()
    # Rad
    fig = plt.figure(num=None, figsize=(4., 3), dpi=512)
    mp.rcParams.update({'font.size': fontsize})
    ax1 = fig.add_subplot(1,1,1)
    dw = np.fromfile('dWdd_100_.txt', sep = ', ')
    pair = np.fromfile('Wpaird_100_.txt', sep = ', ')
    ax = [1./len(dw)*i for i in range(len(dw))]
    plt.minorticks_on()
    ax1.plot(ax,dw,'r-', linewidth = lw)
    ax1.set_xlabel('$\delta$')
    ax1.set_ylabel('$W_\delta^\prime$')
    xticks = [0, 0.5, 1]
    xticklabels = ['$%.1f$'%(x) for x in xticks]
    ax1.set_xticks(xticks)
    ax1.set_xticklabels(xticklabels)
    yticks = [200, 400, 600]
    yticklabels = ['$%d$'%(x) for x in yticks]
    ax1.set_yticks(yticks)
    ax1.set_yticklabels(yticklabels)
    ax2 = ax1.twinx()
    plt.minorticks_on()
    ax2.plot(ax,pair, 'b--', linewidth = lw)
    ax2.set_ylabel('$W_d$')
    yticks = [0, 5, 10]
    yticklabels = ['$%d$'%(x) for x in yticks]
    ax2.set_yticks(yticks)
    ax2.set_yticklabels(yticklabels)
    plt.tight_layout()
    plt.savefig('decay.png', dpi=512)
    plt.close()

if __name__ == '__main__':
    main()
