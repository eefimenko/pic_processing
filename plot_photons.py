#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils

def get_dirs_configs(array):
    cfgs = []
    dirs = []
    for j in range(1, len(array)):
        config = utils.get_config(array[j] + "/ParsedInput.txt")
        cfgs.append(config)
        dirs.append(array[j])
    return dirs, cfgs

def read_file(file):
    f = open(file, 'r')
    axis = []
    num = []
   
    for line in f:
        tmp = line.split()
        axis.append(tmp[0])
        num.append(tmp[1])
   
    return axis, num

def get_color(j):
    color = ['r', 'g', 'b', 'k', 'c', 'y']
    if (j >= len(color)):
        j = j - len(color)
    return color[j]

def read_peakpower(config):
    pp = float(config['PeakPowerPW'])
    return '%.0lf PW' % (pp)
 
def main():
   
    dirs, configs = get_dirs_configs(sys.argv)
    print dirs
    axis = []
    nph = []
    ntr = []
    leg = []
    
    for j in range(len(dirs)):
        _axis, _nph = read_file(dirs[j] + '/nphotons.dat')
        _axis, _ntr = read_file(dirs[j] + '/ntrapped.dat')
        axis.append(_axis)
        nph.append(_nph)
        ntr.append(_ntr)
        leg.append(read_peakpower(configs[j]))

    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(2,1,1)
    
    ax1.set_title('Number of trapped electrons')
    for j in range(len(ntr)):
        pe, = ax1.plot(axis[j], ntr[j], get_color(j), label = leg[j])

    ax1.legend(loc='upper right', shadow=True)
#    ax1.set_yscale('log')
#    ax1.set_xlim(0, m)
    
    ax2 = fig.add_subplot(2,1,2)
    
    ax2.set_title('Number of photons')
    for j in range(len(nph)):
        pe, = ax2.plot(axis[j], nph[j], get_color(j), label = leg[j])

    ax2.legend(loc='upper right', shadow=True)
#    ax2.set_yscale('log')
#    ax2.set_xlim(0, m)
    plt.show()
#    plt.savefig(picspath + '/concentration_compare.png')

    
    
if __name__ == '__main__':
    main()
