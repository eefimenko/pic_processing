#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def main():
    a = 0.1
    b = [0.5, 1, 1.5]
    for i in range(len(b)):
        l = math.sqrt(a*a + b[i]*b[i])
        print a/l, math.asin(a/l)*180/math.pi

if __name__ == '__main__':
    main()
