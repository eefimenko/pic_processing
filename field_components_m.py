#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def read_diag(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def read_field(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)/norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]
   
    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def read_field_abs(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)/norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(abs(array[index]))
        field.append(row)
    return field

def read_maxfield(file):
    axis = []
    ez_t = []
    bx_t = []
    ne_t = []
    np_t = []
    f = open(file, 'r')
    for line in f:
        a = line.split()
        axis.append(float(a[0]))
        ez_t.append(float(a[1]))
        bx_t.append(float(a[2]))
        ne_t.append(float(a[3]))
        np_t.append(float(a[4]))
    return axis, ez_t, bx_t, ne_t, np_t

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])
def min2d(array):
    return min([min(x) for x in array])

def main():
    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
    ex_xpath = 'data/Ex_x'
    ex_ypath = 'data/Ex_y'
    ex_zpath = 'data/Ex_z'
    ey_xpath = 'data/Ey_x'
    ey_ypath = 'data/Ey_y'
    ey_zpath = 'data/Ey_z'
    ez_xpath = 'data/Ez_x'
    ez_ypath = 'data/Ez_y'
    ez_zpath = 'data/Ez_z'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    bx_xpath = 'data/Bx_x'
    bx_ypath = 'data/Bx_y'
    bx_zpath = 'data/Bx_z'
    by_xpath = 'data/By_x'
    by_ypath = 'data/By_y'
    by_zpath = 'data/By_z'
    bz_xpath = 'data/Bz_x'
    bz_ypath = 'data/Bz_y'
    bz_zpath = 'data/Bz_z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nixpath = 'data/Proton2Dx'
    niypath = 'data/Proton2Dy'
    nizpath = 'data/Proton2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nphxpath = 'data/Photon2Dx'
    nphypath = 'data/Photon2Dy'
    nphzpath = 'data/Photon2Dz'
    picspath = 'pics'
    ppath = 'data/ElPhase/'
    mpath = 'data/PosPhase/'
    p1path = 'data/ElMomentum/'
    m1path = 'data/PosMomentum/'
   

    config = utils.get_config("ParsedInput.txt")
    wl = float(config['Wavelength'])
    Xmax = float(config['X_Max'])/wl #mkm to wavelength
    Xmin = float(config['X_Min'])/wl #mkm to wavelength
    Ymax = float(config['Y_Max'])/wl #mkm to wavelength
    Ymin = float(config['Y_Min'])/wl #mkm to wavelength

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])

    nx1 = int(config['ElPhase.SetMatrixSize_0'])
    ny1 = int(config['ElPhase.SetMatrixSize_1'])
    zmin = float(config['ElPhase.SetBounds_0'])*1e4
    zmax = float(config['ElPhase.SetBounds_1'])*1e4
    Emin = float(config['ElPhase.SetBounds_2'])
    Emax = float(config['ElPhase.SetBounds_3'])
    Mmin = float(config['ElMomentum.SetBounds_2'])
    Mmax = float(config['ElMomentum.SetBounds_3'])

    delta = 1

    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(expath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(expath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    xmin = 0.04
    dv = dx*dy*dz
    axis1 = create_axis(nx, (Xmax-Xmin)/nx, Xmin)
    for i in range(nmin, nmax, delta):
        fig = plt.figure(num=None, figsize=(20, 15), dpi=128)

    
#        fig = plt.figure(num=None, figsize=(32, 12), dpi=128)
        
        picname = picspath + '/' + "fcm%06d.png" % (i,)
        print picname
#        a2x = fig.add_subplot(3,4,1)
#        ax_x = fig.add_subplot(3,4,2)
#        ay_x = fig.add_subplot(3,4,3)
#        az_x = fig.add_subplot(3,4,4)
        name = expath + '/' + "%06d.txt" % (i,)
        fielde2x = read_field(name,ny,nz)
        m = max2d(fielde2x)
        a2x = fig.add_subplot(3,4,1)
        surf = a2x.imshow(fielde2x, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Reds')
        a2x.set_title('|E|, %.2le / %.2le\n'%(min2d(fielde2x), max2d(fielde2x)))
        plt.colorbar(surf,  orientation  = 'vertical')
        name = ex_xpath + '/' + "%06d.txt" % (i,)
        fieldex_x = read_field(name,ny,nz)
        ax_x = fig.add_subplot(3,4,2)
        
        surf = ax_x.imshow(fieldex_x, extent=[Xmin, Xmax, Ymin, Ymax], vmin = -m, vmax = m, cmap='Spectral')
        ax_x.set_title('Ex, %.2le / %.2le\n'%(min2d(fieldex_x), max2d(fieldex_x)))
        plt.colorbar(surf,  orientation  = 'vertical')
        name = ey_xpath + '/' + "%06d.txt" % (i,)
        fieldey_x = read_field(name,ny,nz) 
        ay_x = fig.add_subplot(3,4,3)
        
        surf = ay_x.imshow(fieldey_x, extent=[Xmin, Xmax, Ymin, Ymax], vmin = -m, vmax = m, cmap='Spectral')
        ay_x.set_title('Ey, %.2le / %.2le\n'%(min2d(fieldey_x), max2d(fieldey_x)))
        plt.colorbar(surf,  orientation  = 'vertical')
        name = ez_xpath + '/' + "%06d.txt" % (i,)
        fieldez_x = read_field(name,ny,nz)
        az_x = fig.add_subplot(3,4,4)
        surf = az_x.imshow(fieldez_x, extent=[Xmin, Xmax, Ymin, Ymax], vmin = -m, vmax = m, cmap='Spectral')
        az_x.set_title('Ez, %.2le / %.2le\n'%(min2d(fieldez_x), max2d(fieldez_x)))
        plt.colorbar(surf,  orientation  = 'vertical')
       
        
        
        name = bxpath + '/' + "%06d.txt" % (i,)
        fielde2x = read_field(name,ny,nz)
        m = max2d(fielde2x) 
        b2x = fig.add_subplot(3,4,5)
        surf = b2x.imshow(fielde2x, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Blues')
        b2x.set_title('|B|, %.2le / %.2le\n'%(min2d(fielde2x), max2d(fielde2x)))
        plt.colorbar(surf,  orientation  = 'vertical')
        name = bx_xpath + '/' + "%06d.txt" % (i,)
        fieldex_x = read_field(name,ny,nz)
        bx_x = fig.add_subplot(3,4,6)
        surf = bx_x.imshow(fieldex_x, extent=[Xmin, Xmax, Ymin, Ymax], vmin = -m, vmax = m, cmap='Spectral')
        bx_x.set_title('Bx, %.2le / %.2le\n'%(min2d(fieldex_x), max2d(fieldex_x)))
        plt.colorbar(surf,  orientation  = 'vertical')
        name = by_xpath + '/' + "%06d.txt" % (i,)
        fieldey_x = read_field(name,ny,nz)
        by_x = fig.add_subplot(3,4,7)
        surf = by_x.imshow(fieldey_x, extent=[Xmin, Xmax, Ymin, Ymax], vmin = -m, vmax = m, cmap='Spectral')
        by_x.set_title('By, %.2le / %.2le\n'%(min2d(fieldey_x), max2d(fieldey_x)))
        plt.colorbar(surf,  orientation  = 'vertical')
        name = bz_xpath + '/' + "%06d.txt" % (i,)
        fieldez_x = read_field(name,ny,nz)
        bz_x = fig.add_subplot(3,4,8)
        surf = bz_x.imshow(fieldez_x, extent=[Xmin, Xmax, Ymin, Ymax], vmin = -m, vmax = m, cmap='Spectral')
        bz_x.set_title('Bz, %.2le / %.2le\n'%(min2d(fieldez_x), max2d(fieldez_x)))
        plt.colorbar(surf,  orientation  = 'vertical')
        
      
        name = nexpath + '/' + "%06d.txt" % (i,)
        fieldel_x = read_field(name,ny,nz)
        m = max2d(fieldel_x)
        
        el_x = fig.add_subplot(3,4,9)
        surf = el_x.imshow(fieldel_x, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Greens', vmin = m*1e-3, vmax = m, norm=clr.LogNorm())
        el_x.set_title('Electron density, %.2le\n'%(max2d(fieldel_x)))
        plt.colorbar(surf,  orientation  = 'vertical')
        el_x.set_xlim([-xmin, xmin])
        el_x.set_ylim([-xmin, xmin])
#        name = npxpath + '/' + "%06d.txt" % (i,)
#        fieldpos_x = read_field(name,nx,ny)
#        m = max2d(fieldpos_x)
#        surf = pos_x.imshow(fieldpos_x, extent=[Xmin, Xmax, Ymin, Ymax], cmap='bone_r', vmin = m*1e-3, vmax = m, norm=clr.LogNorm())
#        pos_x.set_xlim([-1, 1])
#        pos_x.set_ylim([-0.5, 0.5])

        pname = ppath + '/' + "%06d.txt" % (i,)
        p1name = p1path + '/' + "%06d.txt" % (i,)
        pfield = read_diag(pname,nx1,ny1)
        p1field = read_diag(p1name,nx1,ny1)
        m = max2d(pfield)
        gamma = fig.add_subplot(3,4,10)
        gamma.imshow(pfield, extent = [zmin,zmax, Emin,Emax], aspect='auto', cmap='Greens', origin='lower', vmin = m*1e-2, vmax = m, norm=clr.LogNorm())
        gamma.set_title('Electrons gamma')
        plt.colorbar(surf,  orientation  = 'vertical')
        gamma.set_xlim([-xmin, xmin])
        m = max2d(p1field)
        momentum = fig.add_subplot(3,4,11)
        momentum.imshow(p1field, extent = [zmin,zmax, Mmin,Mmax], aspect='auto', cmap='Greens', origin='lower', vmin = m*1e-2, vmax = m, norm=clr.LogNorm())
        momentum.set_title('Electrons pz')
        plt.colorbar(surf,  orientation  = 'vertical')
        momentum.set_xlim([-xmin, xmin])
        ezx = fig.add_subplot(3,4,12)        
       
        a2x.set_xlim([-xmin, xmin])
        a2x.set_ylim([-xmin, xmin])
        ax_x.set_xlim([-xmin, xmin])
        ax_x.set_ylim([-xmin, xmin])
        ay_x.set_xlim([-xmin, xmin])
        ay_x.set_ylim([-xmin, xmin])
        az_x.set_xlim([-xmin, xmin])
        az_x.set_ylim([-xmin, xmin])
       
        b2x.set_xlim([-xmin, xmin])
        b2x.set_ylim([-xmin, xmin])
        bx_x.set_xlim([-xmin, xmin])
        bx_x.set_ylim([-xmin, xmin])
        by_x.set_xlim([-xmin, xmin])
        by_x.set_ylim([-xmin, xmin])
        bz_x.set_xlim([-xmin, xmin])
        bz_x.set_ylim([-xmin, xmin])
      
#        axis, ez_t, bx_t, ne_t, np_t = read_maxfield('max_field_ne.dat')
#        ezx.plot(axis, ez_t, 'r', label = 'ez')
#        ezx.plot(axis, bx_t, 'b', label = 'bx')
#        ezx.legend(loc='lower left', shadow=True)
#        el = ezx.twinx()
#        el.plot(axis, ne_t, 'g', label = 'el')
#        el.plot(axis, np_t, 'k', label = 'pos')
#        step = axis[1] - axis[0]
#        axis1 = [i*step, i*step]
#        data1 = [min(ne_t), max(ne_t)]
#        axis2 = [axis[0], axis[-1]]
#        data2 = [0, 0]
#        ezx.plot(axis2, data2, 'k')
#        el.plot(axis1, data1, 'K', linewidth=2.0)
#        el.legend(loc='upper left', shadow=True)
#        el.set_yscale('log')
#        plt.subplots_adjust(left=0.0, right=1.0, bottom=0.0, top=1.0)
        
        plt.savefig(picname)
        plt.close()
    
#        plt.show()
    
if __name__ == '__main__':
    main()
