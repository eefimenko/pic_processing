#!/usr/bin/python
import matplotlib as mp
#mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import sys
import math
import os, os.path
import utils
from tqdm import tqdm
import pickle

def main():
    picdir = '/home/evgeny/Dropbox/artXCELS/'
    fontsize = 16
            
    mp.rcParams.update({'font.size': fontsize})
    mp.rcParams['mathtext.fontset'] = 'custom'
    mp.rcParams['mathtext.it'] = 'Arial:italic'
    mp.rcParams['mathtext.rm'] = 'Arial'
    #mp.rcParams['figure.constrained_layout.use'] = True
    
    
    nbeams = [12, 6, 4]
    angles = [0.4, 0.2]
    maxs = [0.03, 0.05]
    path = 'ideal_36PW'
    config = utils.get_config(os.path.join(path, "ParsedInput.txt"))
    dumpfile = os.path.join(path, 'dump.pkl')
    
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    dt = float(config['TimeStep'])
    ev = float(config['eV'])
    omega = float(config['Omega'])
    duration = float(config['PulseDuration'])*float(config['period'])  # s
    delay = float(config.get('delay',2e-14))  # s
    power = float(config['PeakPower'])*1e-7  # W
    powerPW = float(config['PeakPowerPW'])
    Emax = float(config['QEDstatistics.Emax'])
    Emin = float(config['QEDstatistics.Emin'])
    Th_max = float(config['QEDstatistics.ThetaMax'])
    Th_min = float(config['QEDstatistics.ThetaMin'])
    Phi_max = 2. * math.pi #float(config['QEDstatistics.PhiMax'])
    Phi_min = 0 #float(config['QEDstatistics.PhiMin'])
    N_E = int(config['QEDstatistics.OutputN_E'])
    N_Phi = int(config['QEDstatistics.OutputN_phi'])
    N_Th = int(config['QEDstatistics.OutputN_theta'])
    density = float(config['Ne'])
    print('Density ', density)
    radius = float(config['R'])
    de = (Emax - Emin)/N_E/1.6e-12/1e9
    de_ = (Emax - Emin)/N_E
    dth = (Th_max - Th_min)/N_Th  # erg -> eV -> GeV
    dphi = (Phi_max - Phi_min)/N_Phi
    ax_e = utils.create_axis(N_E, de)
    ax_th = utils.create_axis(N_Th, dth, dth/2.)
    ax_phi = utils.create_axis(N_Phi, dphi, 0)

    outStep = int(config['QEDstatistics.OutputIterStep'])
    tp = duration*1e15
    tdelay = delay*1e15
    n_tgt = density * 4./3. * math.pi * radius**3
    delay = float(config.get('delay', 2.2e-14))
    duration = float(config['PulseDuration'])*float(config['period'])
    period = float(config['period'])
    T = 2*math.pi/omega*1e15
    nit = int(config['QEDstatistics.OutputIterStep'])
    dt1 = float(config['TimeStep'])*nit
   
    omega = float(config['Omega'])
    alpha = float(config['alpha'])
    relField = float(config['RelativisticField'])
    verbose = 0

    if os.path.exists(dumpfile):
        with open(dumpfile, 'rb') as f:
            sp_ph_sum_ = pickle.load(f)
            sp_el_sum_ = pickle.load(f)
            sp_pos_sum_ = pickle.load(f)
            ang_ph_sum_0 = pickle.load(f)
            ang_el_sum_ = pickle.load(f)
            ang_pos_sum_ = pickle.load(f)
            ph_en = pickle.load(f)
            ph_en_1gev = pickle.load(f)
            el_en = pickle.load(f)
            el_en_1gev = pickle.load(f)
            pos_en = pickle.load(f)
            pos_en_1gev = pickle.load(f)
            ph_flux = pickle.load(f)
            el_flux = pickle.load(f)
            pos_flux = pickle.load(f)
            ph_flux_1gev = pickle.load(f)
            el_flux_1gev = pickle.load(f)
            pos_flux_1gev = pickle.load(f)
            ang_sp_sph0 = pickle.load(f)
            el_ang_sp_sph0 = pickle.load(f)
            pos_ang_sp_sph0 = pickle.load(f)

    
    fig = plt.figure(num=None, figsize=(10, 4), dpi=128)
    #ax1 = fig.add_axes([0.1,0.15,0.38,0.8])
    #ax2 = fig.add_axes([0.6,0.15,0.38,0.8])
    axes_ = [[0.1,0.22,0.38,0.68], [0.6,0.22,0.38,0.68]]
    labels = ['(a)', '(b)']
    
    #fig = plt.figure(num=None, figsize=(10, 4), dpi=128)
    
    for i, angle in enumerate(angles):
        ax = fig.add_axes(axes_[i])
        num = np.sum(ang_ph_sum_0)
        sp_theta = np.sum(ang_sp_sph0, axis=0)/num/np.sin(ax_th)
        
        ##ax1.plot(ax_phi, np.sum(ang_sp_sph0, axis=1)/num/maxs[i], label='ideal', dashes=[2, 2])
        ax.plot(ax_th*1000 - dth/2.*1000, sp_theta/np.max(sp_theta[:len(ax_th)//2]), label='ideal', dashes=[2, 2])
        for n in nbeams:
            path = '%dbeams_%g_36PW' % (n, angle)
            config = utils.get_config(os.path.join(path, "ParsedInput.txt"))
            dumpfile = os.path.join(path, 'dump.pkl')
            if os.path.exists(dumpfile):
                with open(dumpfile, 'rb') as f:
                    sp_ph_sum_ = pickle.load(f)
                    sp_el_sum_ = pickle.load(f)
                    sp_pos_sum_ = pickle.load(f)
                    ang_ph_sum_ = pickle.load(f)
                    ang_el_sum_ = pickle.load(f)
                    ang_pos_sum_ = pickle.load(f)
                    ph_en = pickle.load(f)
                    ph_en_1gev = pickle.load(f)
                    el_en = pickle.load(f)
                    el_en_1gev = pickle.load(f)
                    pos_en = pickle.load(f)
                    pos_en_1gev = pickle.load(f)
                    ph_flux = pickle.load(f)
                    el_flux = pickle.load(f)
                    pos_flux = pickle.load(f)
                    ph_flux_1gev = pickle.load(f)
                    el_flux_1gev = pickle.load(f)
                    pos_flux_1gev = pickle.load(f)
                    ang_sp_sph = pickle.load(f)
                    el_ang_sp_sph = pickle.load(f)
                    pos_ang_sp_sph = pickle.load(f)
            num = np.sum(ang_ph_sum_)
            sp_theta = np.sum(ang_sp_sph, axis=0)/num/np.sin(ax_th)
            #ax1.plot(ax_phi, np.sum(ang_sp_sph, axis=1)/num/maxs[i], label='%d beams' % n)
            ax.plot(ax_th*1000 - dth/2.*1000, sp_theta/np.amax(sp_theta[:len(ax_th)//2]), label='%d beams' % n)
        #ax.set_yscale('log')
        #ax1.legend(loc='upper left', frameon=False, ncol=2, fontsize=fontsize-2)
        #ax1.set_ylabel('$\\frac{\partial W}{\partial \\varphi}$, a.u.')
        #ax1.set_xlabel('$\\varphi$')
        #ax1.set_xlim([0, 2*math.pi])
        #ax1.set_ylim([0, 1.4])
        #ax1.set_yticks([0, 0.5, 1])
        ax.set_ylabel('$\\frac{1}{sin\\theta} \\frac{\partial W}{\partial \\theta}$, a.u.')
        #ax1.text((#ax1.get_xlim()[0]-#ax1.get_xlim()[1])/4, #ax1.get_ylim()[1], '(a)')
        print((ax.get_xlim()[0]-ax.get_xlim()[1])/5, ax.get_ylim()[1])
        ax.text(-4, 1.2, labels[i])
        #ax1.set_xticks([0, math.pi/2, math.pi, 3*math.pi/2, 2*math.pi])
        #ax1.set_xticklabels(["0", "$\pi/2$", "$\pi$", "$3\pi/2$", "$2\pi$"])
        #ax.set_xticks([0, math.pi/2, math.pi])
        #ax.set_xticklabels(["0", "$\pi/2$", "$\pi$"])
        ax.set_xlim([0, 20])
        ax.set_ylim([0, 1.2])
        #ax.set_xticks([0, math.pi/2, math.pi])
        #ax.set_xticklabels(["0", "$\pi/2$", "$\pi$"])
        ax.set_xlabel('$\\theta$, mrad')
        ax.legend(loc='upper right', frameon=False, ncol=2, fontsize=fontsize-2)
        #plt.tight_layout(pad=7)
        #plt.subplots_adjust(bottom=-0.1, right=0.8, top=0.9)
    figname = os.path.join(picdir, "dn_fine_f.eps")
    print(figname)
    plt.savefig(figname, dpi=1200, format='eps')
        #plt.show()


if __name__ == '__main__':
    main()
