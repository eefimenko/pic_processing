#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_file(filename, mult=1):
    f = open(filename, 'r')
    tmp = f.readline().split()
    arr = []
    for i in range(len(tmp)):
        arr.append(float(tmp[i])*mult)
    return arr

def read_dwave(filename):
    f = open(filename, 'r')
    arr = []
    amp = []
    power = []
    for line in f:
        tmp = line.split()
        power_ = float(tmp[0]) 
        amp_ = math.sqrt(float(tmp[0])*1e22)*7.81441e-9
        print power_, amp_
        arr_ = float(tmp[1])
        arr.append(arr_)
        amp.append(amp_)
        power.append(power_)
    return amp, power, arr

def form_dwave():
    arr = [0.86, 6.74, 8.5]
    amp = []
    amp1 = []
    power = [25, 70, 100]
    for i in range(len(power)):
        amp_ = math.sqrt(power[i]*1e22)*7.81441e-9/1.53
        amp1_ = math.sqrt(power[i]*1e22)*7.81441e-9
        amp.append(amp_)
        amp1.append(amp1_)
    return amp, amp1, power, arr

def main():
    power = []
    gamma = []
    pwa = read_file('a.txt', mult=2)
    pwg = read_file('G.txt')
    da, dp, arr = read_dwave('gamma.dat')
    da1, da11, dp1, arr1 = form_dwave()  
    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(1,1,1)
    p, = ax1.plot(pwa, pwg, 'r', label='Plane wave')
    d, = ax1.plot(da, arr, 'b', label='E-dipole wave')
    d, = ax1.plot(da1, arr1, 'g', label='M-dipole wave (amplitude)')
    d1, = ax1.plot(da11, arr1, 'k', label='M-dipole wave (power)')
    ax1.set_xlabel('a0')
    ax1.set_ylabel('Growth rate, $\Gamma T$')
#    ax1.set_yscale('log')
#    ax1.set_xscale('log')
    plt.legend(loc='upper left')
    ax1.set_xlim([0, 12000])
    plt.show()

if __name__ == '__main__':
    main()
