#!/usr/bin/python
import matplotlib as mp
#mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import sys
import math
import os, os.path
import utils
from tqdm import tqdm
import pickle
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import gridspec

def main():
    mp.rcParams.update({'font.size': 18})
    picdir = '/home/evgeny/Dropbox/artXCELS/'
    config = utils.get_config("ParsedInput.txt")
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    dt = float(config['TimeStep'])
    ev = float(config['eV'])
    Xmin = float(config["X_Min"]) * 1e4
    Xmax = float(config["X_Max"]) * 1e4
    Ymin = float(config["Y_Min"]) * 1e4
    Ymax = float(config["Y_Max"]) * 1e4
    Zmin = float(config["Z_Min"]) * 1e4
    Zmax = float(config["Z_Max"]) * 1e4
    omega = float(config['Omega'])
    duration = float(config['PulseDuration'])*float(config['period'])  # s
    delay = float(config.get('delay',2e-14))  # s
    power = float(config['PeakPower'])*1e-7  # W
    powerPW = float(config['PeakPowerPW'])
    Emax = float(config['QEDstatistics.Emax'])
    Emin = float(config['QEDstatistics.Emin'])
    Th_max = float(config['QEDstatistics.ThetaMax'])
    Th_min = float(config['QEDstatistics.ThetaMin'])
    Phi_max = 2. * math.pi #float(config['QEDstatistics.PhiMax'])
    Phi_min = 0 #float(config['QEDstatistics.PhiMin'])
    N_E = int(config['QEDstatistics.OutputN_E'])
    N_Phi = int(config['QEDstatistics.OutputN_phi'])
    N_Th = int(config['QEDstatistics.OutputN_theta'])
    density = float(config['Ne'])
    print('Density ', density)
    radius = float(config['R'])
    de = (Emax - Emin)/N_E/1.6e-12/1e9
    de_ = (Emax - Emin)/N_E
    dth = (Th_max - Th_min)/N_Th  # erg -> eV -> GeV
    ax_e = utils.create_axis(N_E, de)
    ax_th = utils.create_axis(N_Th, dth)

    outStep = int(config['QEDstatistics.OutputIterStep'])
    tp = duration*1e15
    tdelay = delay*1e15
    n_tgt = density * 4./3. * math.pi * radius**3
    
    path = '.'
    elpath = '/el/'
    elmaxpath = '/el_max/'
    posmaxpath = '/pos_max/'
    phmaxpath = '/ph_max/'
    elsmaxpath = '/elSmax/'
    possmaxpath = '/posSmax/'
    phsmaxpath = '/phSmax/'
    elnmaxpath = '/elNmax/'
    posnmaxpath = '/posNmax/'
    phnmaxpath = '/phNmax/'
    pospath = '/pos/'
    phpath = '/ph/'
    phangle = '/ph_angle/'
    pht = '/phT/'
    picspath = 'pics/'
    prefix = ''
    ez_z_path = os.path.join(prefix, 'data/Ez_z')

    dumpfile = os.path.join(path, 'dump.pkl')
    
    archive = os.path.join(path, 'statdata.zip')
    data_archive = os.path.join(path, 'BasicOutput/data.zip')
    nmin, nmax = utils.find_min_max_from_archive_directory(utils.enspsphpath,
                                                           'txt',
                                                           archive)
    print('Found ' + str(nmax) + ' files')

    delay = float(config.get('delay', 2.2e-14))
    duration = float(config['PulseDuration'])*float(config['period'])
    angle = float(config['OpeningAngle'])
    nbeams = int(config['MultibeamGenerator.PulsesNumber'])
    period = float(config['period'])
    T = 2*math.pi/omega*1e15
    nit = int(config['QEDstatistics.OutputIterStep'])
    dt1 = float(config['TimeStep'])*nit
    ax_ = utils.create_axis(nmax, dt1/period)
    omega = float(config['Omega'])
    alpha = float(config['alpha'])
    relField = float(config['RelativisticField'])
    verbose = 0
    n_c = float(config['n_cr'])
    
    
        #ez_a[i] = math.exp(-2.*math.log(2)*(t - delay)**2/duration**2)

    ez_t = utils.ts(path, 'data/Ez_xy', name='ez', ftype='bin',
                    tstype='center', shape=(nx,ny), verbose=verbose,
                    archive=data_archive)
    print('a_E=', ez_t.max())
    ne_t = utils.ts(path, 'data/Electron_xy', name='ne', ftype='bin',
                    tstype='max', shape=(nx,ny), verbose=verbose,
                    archive=data_archive)*n_c
    np_t = utils.ts(path, 'data/Photon_xy', name='np', ftype='bin',
                    tstype='max', shape=(nx,ny), verbose=verbose,
                    archive=data_archive)*n_c
    iteration = 375
    
    #for i in range(nmax):
    #    t = i*dt1
    #    if t-delay > 0 and t-delay < period*alpha/2.:
    #        ez_a[i] = 3e11*math.sqrt(powerPW/10.)*math.sin(omega*(t-delay)/alpha)**2/relField
    #    else:
    #        ez_a[i] = 0.
            
    

    #pulse_energy = powerPW*1e22*3*period*(alpha-2*np.sin(2*np.pi*alpha)/((alpha**2-1)*(alpha**2-4)*np.pi))/(16)*1e-7
    #print(en, pulse_energy, en/pulse_energy)
    
    fig = plt.figure(num=None, figsize=(16, 4))
    gs = gridspec.GridSpec(1, 3, width_ratios=[4, 3, 3])
    ax1 = plt.subplot(gs[0,0])
    ax_z = plt.subplot(gs[0,1])
    ax_x = plt.subplot(gs[0,2])
    #fig = plt.figure(num=None, figsize=(6, 4))
    #ax1 = fig.add_subplot(1,1,1)
    pe_, = ax1.plot(ax_, ne_t, 'r', label='N$_\mathrm{e}$')
    pp_, = ax1.plot(ax_, np_t, 'g', label='N$_\mathrm{p}$')
    ax1.axvline(x=iteration*dt1/period)
    ax1.set_yscale('log')
    ax2 = ax1.twinx()
   
    pf_, = ax2.plot(ax_, np.abs(ez_t)/1000, 'b', alpha = 0.2, label='|E$_\mathrm{z}$|/a$_0$')
        
    ax1.set_xlabel('t/T')
    ax2.set_ylabel('|E$_\mathrm{z}$|/a$_0$, $\\times 10^3$')
    ax1.set_ylabel('N$_\mathrm{e}$, N$_\mathrm{p}$, cm$^{-3}$')
    ax1.set_xlim([0,40])
    ax2.set_ylim([0, 1.1 * np.amax(np.abs(ez_t))/1000])
    ax1.text((ax1.get_xlim()[0]-ax1.get_xlim()[1])/3, ax1.get_ylim()[1], '(a)')
    #picname = os.path.join('.', "dynamics_%d_%g.png" % (nbeams, angle))
    plt.legend([pe_, pp_, pf_], ['N$_\mathrm{e}$', 'N$_\mathrm{p}$', '|E$_\mathrm{z}$|/a$_0$'], loc='upper left', frameon=False, fontsize=14)
    #plt.tight_layout()
    #plt.savefig(picname)
    #plt.show()
    #plt.close()

    #iteration = 375
    cmap1 = mp.cm.get_cmap('gist_heat_r').copy()
    cmap1.set_under('w', False)
    cmap1.set_bad('w', 1)
    archive = os.path.join(path, 'BasicOutput', 'data.zip')
    
    #fig = plt.figure(figsize=(9, 4))
    #ax = fig.add_subplot(1,2,1)
    read, densElZ = utils.bo_file_load(os.path.join('data/Photon_xy'), iteration, nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive)
    densElZ = densElZ * n_c
    m = np.amax(densElZ)
    
    surf = ax_z.imshow(densElZ, cmap = cmap1, extent=[Xmin, Xmax, Ymin, Ymax], norm=clr.LogNorm(clip='True', vmin = 1e-3*m, vmax = m))
    divider = make_axes_locatable(ax_z)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)
    ax_z.set_xlabel('x, $\mu$m')
    ax_z.set_ylabel('y, $\mu$m')
    ax_z.set_xticks([-2, -1, 0, 1, 2])
    ax_z.text(-3, 2, '(b)')

    r = 1.9
    r0 = 1.5
    phi_0 = 0
    
    if nbeams <= 6:
        alpha_step = 2 * math.pi/nbeams
        for n_ in range(nbeams):
            alpha = alpha_step * n_ + phi_0
            x_ = - r * math.cos(alpha)
            y_ = r * math.sin(alpha)
            x0_ = - r0 * math.cos(alpha)
            y0_ = r0 * math.sin(alpha)
            ax_z.annotate('', xy = (x0_, y0_), xytext = (x_, y_), arrowprops=dict(facecolor='b', width = 4, headwidth = 8, shrink=0.05))
    else:
        alpha_step = 2 * math.pi/(nbeams/2)
        for n_ in range(nbeams//2):
            alpha = alpha_step * n_ + phi_0
            x_ = - r * math.cos(alpha)
            y_ = r * math.sin(alpha)
            x0_ = - r0 * math.cos(alpha)
            y0_ = r0 * math.sin(alpha)
            ax_z.annotate('', xy = (x0_, y0_), xytext = (x_, y_), arrowprops=dict(facecolor='b', width = 6, headwidth = 8, shrink=0.05))
            
        for n_ in range(nbeams//2, nbeams):
            alpha = alpha_step * (n_ + 0.5) + phi_0
            x_ = - r * math.cos(alpha)
            y_ = r * math.sin(alpha)
            x0_ = - r0 * math.cos(alpha)
            y0_ = r0 * math.sin(alpha)
            ax_z.annotate('', xy = (x0_, y0_), xytext = (x_, y_), arrowprops=dict(facecolor='g', width = 4, headwidth = 8, shrink=0.05))
    
    #ax = fig.add_subplot(1,2,2)
    read, densElX = utils.bo_file_load(os.path.join('data/Photon_yz'), iteration, nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive)
    densElX = densElX * n_c
    m = np.amax(densElX)
    
    surf = ax_x.imshow(densElX, cmap = cmap1, extent=[Xmin, Xmax, Ymin, Ymax], norm=clr.LogNorm(clip='True', vmin = 1e-3*m, vmax = m))
    ax_x.set_xlabel('y, $\mu$m')
    ax_x.set_ylabel('z, $\mu$m')
    ax_x.set_xticks([-2, -1, 0, 1, 2])
    divider = make_axes_locatable(ax_x)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)
    ax_x.text(-3, 2, '(c)')

    if nbeams <= 6:
        x_ = r
        x0_ = r0 
        ax_x.annotate('', xy = (x0_, 0), xytext = (x_, 0), arrowprops=dict(facecolor='b', shrink=0.05))
        ax_x.annotate('', xy = (-x0_, 0), xytext = (-x_, 0), arrowprops=dict(facecolor='b', shrink=0.05))
    else:
        alpha = angle
        x_ = r 
        y_ = r * math.sin(alpha)
        x0_ = r0 
        y0_ = r0 * math.sin(alpha)
        
        ax_x.annotate('', xy = (x0_, y0_), xytext = (x_, y_), arrowprops=dict(facecolor='b', shrink=0.05))
        ax_x.annotate('', xy = (-x0_, y0_), xytext = (-x_, y_), arrowprops=dict(facecolor='b', shrink=0.05))
        ax_x.annotate('', xy = (x0_, -y0_), xytext = (x_, -y_), arrowprops=dict(facecolor='g', shrink=0.05))
        ax_x.annotate('', xy = (-x0_, -y0_), xytext = (-x_, -y_), arrowprops=dict(facecolor='g', shrink=0.05))
    #picname1 = os.path.join('.', "dynamics2d_%d_%g.png" % (nbeams, angle))
    #plt.tight_layout()
    #print(picname1)
    #plt.savefig(picname1)
    #plt.show()
    #plt.close()
    picname2 = os.path.join(picdir, "dynamics_full_%d_%g.png" % (nbeams, angle))
    print(picname2)
    plt.tight_layout()
    plt.savefig(picname2, dpi = 300)
    #os.system('convert +append ' + picname + ' ' + picname1 + ' ' + picname2)
    os.system('convert -trim ' + picname2 + ' ' + picname2)
    print(picname2)
    picname2 = os.path.join(picdir, "dynamics_full_%d_%g.eps" % (nbeams, angle))
    plt.savefig(picname2, dpi = 300, format='eps')
if __name__ == '__main__':
    main()
