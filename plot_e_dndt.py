#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils

def read_field(path,name):
    file = path + '/' + name
    print 'Open ' + file
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = [row[0] for row in tmp]
    return tmp1

def smooth(array, num):
    l = len(array)
    res = [0]*l
    for j in range(num/2, l-num/2-1):
        tmp = 0
        for k in range(num):
            tmp += array[j-num/2 + k]
        res[j] = tmp/num
#        print array[j], res[j]
    return res

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis
   
def main():

    config = utils.get_config("ParsedInput.txt")
    step = float(config['TimeStep'])*1e15*10./3.
    pathlist = ['.']
    color = ['r', 'g', 'b']
    fig, ax1 =  plt.subplots()
#    plt.rc('text', usetex=True)
    for i,path in enumerate(pathlist):
        power_tt = []
        ne_tt = []
        power_t = read_field(path, 'dndt.dat')
        ne_t = read_field(path, 'e.dat')
        h_t = read_field(path, 'h.dat')
        axis = create_axis(len(power_t), step)
        axish = create_axis(len(h_t), step) 
        for j, e in enumerate(power_t):
            if abs(e) < 0.1 or j < 200:
                power_t[j] = 0
            else:
                power_t[j] = 1./e
        n_t = smooth(power_t, 3)
#        n_t = power_t
        for j, e in enumerate(h_t):
            h_t[j] /= 1.53
        print len(axis), len(h_t) 
#        ppp, = ax1.plot(power_t, 'r')
        ne_t = [0]*len(n_t)
        ppp, = ax1.plot(axis, n_t, 'r')
        ax1.set_ylim([0,5])
#        ppp, = ax1.plot(axis, power_t, 'g')
        ax2 = ax1.twinx()
        ne_t = [0]*len(ne_t)
        h_t = [0]*len(ne_t)
        ppp1, = ax2.plot(axis, ne_t, 'g')
        ppp2, = ax2.plot(axish, h_t, 'b')
        plt.xlim([5, 6])
        ax2.set_ylim([0,1.2e12])
        mp.rcParams.update({'font.size': 18})
        ax1.legend([ppp, ppp1, ppp2], ["$\Gamma T$", "E", 'B'], loc=3)
        ax1.set_xlabel(u'Время, T')
        ax1.set_ylabel('$\Gamma T$')
        ax2.set_ylabel(u'E,B, СГС')
#        plt.xlim([14000, 14500])
        
#        ppp, = ax1.plot(power_t, ne_t, color[i+1])
#    ax1.set_yscale('log')
#    ax1.set_xscale('log')
    plt.savefig('pics/nn1.png')
    plt.close()
    
if __name__ == '__main__':
    main()
