#!/usr/bin/python
from array import array
import struct
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import math
import utils
import sys
import scipy.ndimage as ndimage
k = 0
LightVelocity = 2.99792e+10
Omega = 1

def R(x,y,z):
    R = math.sqrt(x*x + y*y + z*z)
    return R

def exf(x,y,z):
    r = R(x,y,z)
    if r < 0.0001:
        r = 0.0001
    return x*z/(r*r) * f2(r)*2.

def eyf(x,y,z):
    r = R(x,y,z)
    if r < 0.0001:
        r = 0.0001
    return y*z/(r*r) * f2(r)*2.

def ezf(x,y,z):
    r = R(x,y,z)
    if r < 0.0001 :
        r = 0.0001
    return 2. * (f3(r) + z*z/(r*r)*f2(r))
  
def bxf(x,y,z):
    r = R(x,y,z)
    if r < 0.0001 :
        r = 0.0001
    return y/r * 2. * f1(r)

def byf(x,y,z):
    r = R(x,y,z)
    if r < 0.0001 :
        r = 0.0001
    return -x/r * 2. * f1(r)
    
def f1(r):
    return -k*k/r * math.cos(k*r) + k/(r*r) * math.sin(k*r)

def f2(r):
    return (-k*k/r + 3./(r*r*r))*math.sin(k*r) - 3.*k/(r*r)*math.cos(k*r)

def f3(r):
    return (k*k/r - 1./(r*r*r))*math.sin(k*r) + k/(r*r)*math.cos(k*r)

def Sin(r,t,k):
    return math.sin(k*r + Omega*t)

def Cos(r,t,k):
    return math.cos(k*r + Omega*t)

def Ex(x,y,z,R,t,k):
    return z * x / (R*R*R) * ( Sin(R,t,k)*(-1 + 3./(k*k*R*R)) - Cos(R,t,k)*3./(k*R))

def Ey(x,y,z,R,t,k):
    return z * y / (R*R*R) * ( Sin(R,t,k)*(-1 + 3./(k*k*R*R)) - Cos(R,t,k)*3./(k*R))

def Ez(x,y,z,R,t,k):
    return -( (x*x + y*y)/(R*R*R) * Sin(R,t,k) + 1./(k*k*R*R*R) * (-1. + 3.*z*z/(R*R))*Sin(R,t,k) + 1./(k*R*R) * (1. - 3.*z*z/(R*R)) * Cos(R,t,k) )

def Bx(x,y,z,R,t,k):
    return - y /(R*R) * (Sin(R,t,k) + 1./(k*R)*Cos(R,t,k))

def By(x,y,z,R,t,k):
    return - x /(R*R) * (Sin(R,t,k) + 1./(k*R)*Cos(R,t,k))

def write_VTK_header(fe, name, nx, ny, nz, xmin, dx, ymin, dy, zmin, dz):
    fe.write("# vtk DataFile Version 3.0\n")
    fe.write("VolumeData\n")
    fe.write("BINARY\n")
    fe.write("DATASET STRUCTURED_POINTS\n")
    fe.write("DIMENSIONS %d %d %d\n"%(nx,ny,nz))
    fe.write("ASPECT_RATIO %f %f %f\n"%(dx,dy,dz))
    fe.write("ORIGIN %f %f %f\n"%(xmin,ymin,zmin))
    fe.write("POINT_DATA  %d\n"%(nx*ny*nz))
    fe.write("SCALARS %s float 1\n" % (name))
    fe.write("LOOKUP_TABLE default\n")

def save_analitycal_fields(nx, ny, nz, xmin, dx, ymin, dy, zmin, dz):
    fe = open('E.vtk', 'wb')
    fb = open('B.vtk', 'wb')
    write_VTK_header(fe, 'E', nx/2, ny, nz, xmin, dx, ymin, dy, zmin, dz)
    write_VTK_header(fb, 'B', nx/2, ny, nz, 0, dx, ymin, dy, zmin, dz)
    for i in range(nx/2):
        xe = xmin + i*dx
        xb = xmin + (i+nx/2)*dx
        for j in range(ny):
            y = ymin + j*dy
            for n in range(nz):
                z = zmin + n*dz
                ex = exf(xe,y,z)
                ey = eyf(xe,y,z)
                ez = ezf(xe,y,z)
                bx = bxf(xb,y,z)
                by = byf(xb,y,z)
                bz = 0
                fes = math.sqrt(ex*ex + ey*ey + ez*ez)
                fbs = math.sqrt(bx*bx + by*by + bz*bz)
                se = struct.pack(">f", fes)
                sb = struct.pack(">f", fbs)
                fe.write(se)
                fb.write(sb)
                # fb.write(bx)
                # fb.write(by)
                # fb.write(bz)
    fe.close()
    fb.close()

def main():
    wl = 0.9 #mkm
    global k
    global Omega
    k = 2*math.pi/wl
    
    xmax = 1. #mkm
    xmin = -xmax #mkm
    ymax = 1. #mkm
    ymin = -ymax #mkm
    zmax = 1. #mkm
    zmin = -zmax #mkm

    nx = 64
    ny = 64
    nz = 64

    
    dx = (xmax-xmin)/nx
    dy = (ymax-ymin)/ny
    dz = (zmax-zmin)/nz
   
    save_analitycal_fields(nx, ny, nz, xmin, dx, ymin, dy, zmin, dz)
   

if __name__ == '__main__':
    main()
