#!/usr/bin/python
from matplotlib import gridspec
import matplotlib as mp
#mp.use('Agg')
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import utils
import sys
import os
from tqdm import *
from pylab import *

def main():
    fontsize = 18
    picpath = '/home/evgeny/Dropbox/m-dipole'
    mp.rcParams.update({'font.size': fontsize})
    
    path_15pw = 'DipoleB_Wire_1_2021-05-21_21-15-52'
    path_10pw = 'DipoleB_Wire__2021-05-21_17-24-09'
    path_10pw = 'DipoleB_Wire__2021-04-22_23-51-45' # 3pw
    config_15pw = utils.get_config(path_15pw + "/ParsedInput.txt")
    config_10pw = utils.get_config(path_15pw + "/ParsedInput.txt")
    
    wl = float(config_10pw['Wavelength'])
    nx = int(config_10pw['MatrixSize_X'])
    ny = int(config_10pw['MatrixSize_Y'])
    dt = float(config_10pw['TimeStep'])/float(config_10pw['Period'])*int(config_10pw['BOIterationPass'])
    xmax = float(config_10pw['X_Max'])/wl 
    xmin = float(config_10pw['X_Min'])/wl 
    ymax = float(config_10pw['Y_Max'])/wl
    ymin = float(config_10pw['Y_Min'])/wl
    zmax = float(config_10pw['Z_Max'])/wl
    zmin = float(config_10pw['Z_Min'])/wl
    
    bz_z_path = 'BasicOutput/data/Bz_z'
    bz_y_path = 'BasicOutput/data/Bz_y'
    ex_z_path = 'BasicOutput/data/Ex_z'
    ex_y_path = 'BasicOutput/data/Ex_y'
    el_z_path = 'BasicOutput/data/Electron_z'
    pos_z_path = 'BasicOutput/data/Positron_z'
    ion_z_path = 'BasicOutput/data/Ion_z'
    el_y_path = 'BasicOutput/data/Electron_y'
    pos_y_path = 'BasicOutput/data/Positron_y'
    ion_y_path = 'BasicOutput/data/Ion_y'
    verbose = False
    
    print 'Read magnetic field in the center vs time!'
    bz_15pw_t = utils.ts(path_15pw, bz_z_path, name='bz', ftype = 'bin', 
                         tstype='center', shape = (nx,ny), verbose=verbose)
    bz_10pw_t = utils.ts(path_10pw, bz_z_path, name='bz', ftype = 'bin', 
                         tstype='center', shape = (nx,ny), verbose=verbose)
    ex_15pw_t = utils.ts(path_15pw, ex_z_path, name='ex', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose)
    ne_15pw_t = utils.ts(path_15pw, el_z_path, name='el', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose)
    np_15pw_t = utils.ts(path_15pw, pos_z_path, name='pos', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose)
    ne_10pw_t = utils.ts(path_10pw, el_z_path, name='el', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose)
    np_10pw_t = utils.ts(path_10pw, pos_z_path, name='pos', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose)
    ax_15pw = utils.create_axis(len(bz_15pw_t), dt)
    ax_10pw = utils.create_axis(len(bz_10pw_t), dt)
    bz_15pw_env_t = utils.env(bz_15pw_t,ax_15pw)
    bz_10pw_env_t = utils.env(bz_10pw_t,ax_10pw)

    # make the colormaps
    cmap1 = mp.cm.get_cmap('Greens')
    cmap1.set_under('w', 0)
    cmap1.set_bad('w', False)
    #cmap1 = mpl.colors.LinearSegmentedColormap.from_list('my_cmap',['green','blue'],256)
    #cmap2 = mpl.colors.LinearSegmentedColormap.from_list('my_cmap2',[color1,color2],256)

    cmap1._init() # create the _lut array, with rgba values

    # create your alpha array and fill the colormap with them.
    # here it is progressive, but you can create whathever you want
    alphas = np.ones(cmap1.N+3)
    for i in range(10):
        alphas[i] = 0.
    
    cmap1._lut[:,-1] = alphas
    cmap1.set_under('w', 0)
    cmap1.set_bad('w', False)
    fig = plt.figure(figsize = (16,4))
    ax = plt.subplot2grid((1,7),(0,0), colspan=2)
    bound = 1.5
    ratio = 1
    window = 30
    iter_compression = 190
    print(iter_compression * dt)
    read, b_field = utils.bo_file_load(path_15pw + '/' + bz_y_path,iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1)
    read, el_field = utils.bo_file_load(path_15pw + '/' + el_y_path,iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1)
    read, b_field_10pw = utils.bo_file_load(path_10pw + '/' + bz_y_path,iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1)
    read, el_field_10pw = utils.bo_file_load(path_10pw + '/' + el_y_path,iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1)
    ax.set_xlim([-bound,bound])
    ax.set_ylim([-bound,bound])

    for i in range(iter_compression-window/2, iter_compression+window/2 + 1):
        if i == iter_compression:
            continue
        read, el_field_ = utils.bo_file_load(path_15pw + '/' + el_y_path,i,nx,ny, ftype='bin', verbose=1, transpose=1)
        read, el_field_10pw_ = utils.bo_file_load(path_10pw + '/' + el_y_path,i,nx,ny, ftype='bin', verbose=1, transpose=1)
        el_field += el_field_
        el_field_10pw += el_field_10pw_

    el_field /= (window + 1)
    el_field_10pw /= (window + 1)
    
    max_ = np.amax(b_field)
    min_ = np.amin(b_field)
    mf = max(max_, abs(min_))
    
    ax.imshow(b_field[:,nx/2:-1], origin = 'lower', cmap='bwr', extent=[0,xmax,ymin,ymax], vmin=-mf, vmax = mf, alpha = 0.5)
    m = np.amax(el_field)
    ax.imshow(el_field[:,nx/2:-1], origin = 'lower', cmap=cmap1, vmin = 1e-4*m, vmax = m*ratio, norm=clr.LogNorm(), extent=[0,xmax,ymin,ymax])
    
    ax.imshow(b_field_10pw[:,:nx/2], origin = 'lower', cmap='bwr', extent=[xmin,0,ymin,ymax], vmin=-mf, vmax = mf, alpha = 0.5)
    
    surf = ax.imshow(el_field_10pw[:,:nx/2], origin = 'lower', cmap=cmap1, vmin = 1e-4*m, vmax = m*ratio, norm=clr.LogNorm(), extent=[xmin,0,ymin,ymax])
    ax.text(bound-0.9, -bound+0.05, '15 PW')
    ax.text(-bound, -bound+0.05, '10 PW')
    ax.text(-bound, bound-0.25, '(a)')
    ax.set_ylabel('$z/\lambda$')
    ax.set_xlabel('$x/\lambda$')
    ax.set_yticks([-1, 0, 1])
    ax.axvline(x=0, color='k', linewidth = 1., dashes = [3,3])
    #ax.axvline(x=0, color='grey', linewidth = 0.5)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)
    cbar.ax.tick_params(labelsize=fontsize-2)
    cbar.set_ticks([0.1,1,10, 100])
    cbar.set_ticklabels(['0.1','1','10','100'])
    
    ax2 = plt.subplot2grid((1,7),(0,2), colspan=2, sharey = ax)
    iter_rings = 640
    print(iter_rings * dt)
    read, b_field = utils.bo_file_load(path_15pw + '/' + bz_y_path,iter_rings,nx,ny, ftype='bin', verbose=1, transpose=1)
    read, el_field = utils.bo_file_load(path_15pw + '/' + el_y_path,iter_rings,nx,ny, ftype='bin', verbose=1, transpose=1)
    read, b_field_10pw = utils.bo_file_load(path_10pw + '/' + bz_y_path,iter_rings,nx,ny, ftype='bin', verbose=1, transpose=1)
    read, el_field_10pw = utils.bo_file_load(path_10pw + '/' + el_y_path,iter_rings,nx,ny, ftype='bin', verbose=1, transpose=1)

    for i in range(iter_rings-window/2, iter_rings+window/2 + 1):
        if i == iter_compression:
            continue
        read, el_field_ = utils.bo_file_load(path_15pw + '/' + el_y_path,i,nx,ny, ftype='bin', verbose=1, transpose=1)
        read, el_field_10pw_ = utils.bo_file_load(path_10pw + '/' + el_y_path,i,nx,ny, ftype='bin', verbose=1, transpose=1)
        el_field += el_field_
        el_field_10pw += el_field_10pw_

    el_field /= (window + 1)
    el_field_10pw /= (window + 1)
    
    print(np.amax(el_field_10pw))
    ax2.set_xlim([-bound,bound])
    ax2.set_ylim([-bound,bound])
    ax2.set_yticks([-1, 0, 1])
    ax2.set_yticklabels([])
    max_ = np.amax(b_field)
    min_ = np.amin(b_field)
    mf = max(max_, abs(min_))
    
    ax2.imshow(b_field[:,nx/2:-1], origin = 'lower', cmap='bwr', extent=[0,xmax,ymin,ymax], vmin=-mf, vmax = mf, alpha = 0.5)
    m = np.amax(el_field)
    #print(m)
    surf = ax2.imshow(el_field[:,nx/2:-1], origin = 'lower', cmap=cmap1, vmin = 1e-4*m, vmax = m*ratio, norm=clr.LogNorm(), extent=[0,xmax,ymin,ymax])
    
    ax2.imshow(b_field_10pw[:,:nx/2], origin = 'lower', cmap='bwr', extent=[xmin,0,ymin,ymax], vmin=-mf, vmax = mf, alpha = 0.5)
    ax2.imshow(el_field_10pw[:,:nx/2], origin = 'lower', cmap=cmap1, vmin = 1e-4*m, vmax = m*ratio, norm=clr.LogNorm(), extent=[xmin,0,ymin,ymax])
    divider = make_axes_locatable(ax2)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)
    cbar.ax.tick_params(labelsize=fontsize-2)
    cbar.set_ticks([0.1,1,10, 100])
    cbar.set_ticklabels(['0.1','1','10', '100'])
    #ax2.set_ylabel('$z/\lambda$')
    ax2.text(bound-0.9, -bound+0.05, '15 PW')
    ax2.text(-bound, -bound+0.05, '10 PW')
    ax2.text(-bound, bound-0.25, '(b)')
    ax2.set_xlabel('$x/\lambda$')
    ax2.axvline(x=0, color='k', linewidth = 1., dashes = [3,3])
    #ax2.axvline(x=0.015, color='k', linewidth = 0.5)
    #ax2.axvline(x=-0.015, color='k', linewidth = 0.5)
    
    axis_2 = plt.subplot2grid((1,7),(0,4), colspan=3)
    
    lin_bound = 10
    axis_2.set_yscale('linear')
    axis_2.set_ylim((lin_bound, 850))
    axis_2.spines['bottom'].set_visible(False)
    axis_2.yaxis.set_ticks_position('left')
    axis_2.yaxis.set_label_position('left')
    axis_2.xaxis.set_visible(False)
    ne15_, = axis_2.plot(ax_15pw, ne_15pw_t, 'r')
    np15_, = axis_2.plot(ax_15pw, np_15pw_t, 'b')
    ne10_, = axis_2.plot(ax_10pw, ne_10pw_t, 'g')
    np10_, = axis_2.plot(ax_10pw, np_10pw_t, 'c')
    
    divider = make_axes_locatable(axis_2)

    axis_3 = divider.append_axes("bottom", size=0.7, pad=0, sharex=axis_2)
    axis_3.set_yscale('log')
    axis_3.set_ylim((0.1, lin_bound))
    axis_3.set_yticks([0.1, 1, 10])
    axis_3.set_yticklabels(['0.1', '1', '10'])
    #axLin.plot(np.sin(xdomain), xdomain)
    axis_3.spines['top'].set_visible(False)
    #axis_3.xaxis.set_visible(False)
    axis_3.yaxis.set_ticks_position('left')
    axis_3.yaxis.set_label_position('left')
    #plt.setp(axLin.get_xticklabels(), visible=True)
    
    
    ne15__, = axis_3.plot(ax_15pw, ne_15pw_t, 'r')
    np15__, = axis_3.plot(ax_15pw, np_15pw_t, 'b')
    ne10__, = axis_3.plot(ax_10pw, ne_10pw_t, 'g')
    np10__, = axis_3.plot(ax_10pw, np_10pw_t, 'c')
    #axis_2.set_ylim([0, 850])
    axis_2.set_xlim([0, 35])
    axis_2.set_ylabel('$n_{e^-}/n_c$, $n_{e^+}/n_c$')
    axis_2.set_xlabel('$t/T$')
    
    #axis_2.set_yscale('log')
    #axis_2.set_xlim([])
    
    axis = axis_2.twinx()
    axis.yaxis.set_ticks_position('right')
    axis.yaxis.set_label_position('right')
    axis.yaxis.tick_right()
    #axis.plot(ax_15pw, bz_15pw_env_t, 'k', label = '15 PW')
    bz15_, = axis.plot(ax_15pw, np.abs(bz_15pw_t)/np.amax(bz_15pw_t), 'grey', alpha=0.5)
    ph = [axis.plot([],marker="", ls="")[0]]*2
    axis.set_ylabel('$|B_z|/A_0$')
    axis.set_xlabel('$t/T$')
    #axis.plot(ax_10pw, bz_10pw_env_t, 'g', label = '10 PW')
    axis.set_ylim([0, 1.5])
    axis.set_xlim([0, 35])
    axis.text(0, 3.85, '(c)')
    
    leg = plt.legend([ph[0], ne10_, np10_, ph[1], ne15_, np15_],
    #leg = plt.legend([bz15_, ne10_, ne15_, bz15_, np15_, np10_],
               ['10 PW', '$n_{e^-}$', '$n_{e^+}$', '15 PW', '$n_{e^-}$', '$n_{e^+}$'],
                     frameon = False, loc = 'upper right', ncol = 2, fontsize = fontsize-2, labelspacing = 0.2)
    for vpack in leg._legend_handle_box.get_children():
        for hpack in vpack.get_children()[:1]:
            hpack.get_children()[0].set_width(0)
    plt.tight_layout()
    picname = os.path.join(picpath, 'fig_target.png')
    print(picname)
    plt.savefig(picname, dpi=300)
    #plt.show()
   
#    plt.show()
if __name__ == '__main__':
    main()
