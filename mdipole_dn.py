#!/usr/bin/python
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import utils
import sys
import os
from tqdm import *
from pylab import *

def sp_theta(a,min_energy=0, max_energy=sys.float_info.max, de=1.):
    res = np.zeros(len(a[0]))

    for i in range(len(a)):
        energy = i*de
        if energy >= min_energy and energy < max_energy:
            for j in range(len(a[0])):
                res[j] += a[i][j]
    m = np.amax(res)
    if m != 0:
        res /= m

    return res

def read_field2d(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    array = np.fromfile(f, sep=' ')
    f.close()

    res = np.reshape(array, (nx,ny), order = 'F')
    num = 0
    for i in range(len(res)):
        num += sum(res[i])/(i+0.425)

    if num != 0:
        res /= num

    return res

def read_ensp(file,nx,de,mult = 1.):
    f = open(file, 'r')
    array = np.fromfile(f, sep=' ')
    f.close()

    num = np.sum(array)
    
    for i in range(len(array)):
        array[i] *= (i+0.425)

    if num != 0:
        array /= num

    return array

def find_max_energy(array,step):
    n = len(array)
    full = np.sum(array)
    summ = 0
    imax = n

    for i in range(1,n):
        summ = summ + array[-i]
        if summ > 0.01*full:
            imax = i
            break

    return (n-imax) * step

def process_dn(path, picspath, iterations, filename, ppath):
    config = utils.get_config(path + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep'])
    ppw = int(config['PeakPowerPW'])

    ev = float(config['eV'])
    ne = int(config['QEDstatistics.OutputN_E'])
    nt = int(config['QEDstatistics.OutputN_theta'])
    tmax = float(config['QEDstatistics.ThetaMax'])*180./3.14159
    tmin = float(config['QEDstatistics.ThetaMin'])*180./3.14159
    emax = float(config['QEDstatistics.Emax'])/ev*1e-9
    emin = float(config['QEDstatistics.Emin'])/ev*1e-9

    de = (emax - emin)/ne
    dt = (tmax - tmin)/nt
    dth = dt * 3.14159/180. 
    dph = 2*3.14159
    
    axe = utils.create_axis(ne, de, 0)
    axt = utils.create_axis(nt, dt, tmin)
  
    summ = np.zeros((ne,nt))
    summ_ensp = np.zeros(ne)
    print 'Iterations', iterations
    for i in range(iterations[0], iterations[1]):
        phname = path + '/statdata/' + ppath + '/EnAngSp/'+ '%.4f.txt' % (i)
        res = read_field2d(phname,ne,nt)
        phname = path + '/statdata/' + ppath + '/EnSp/'+ '%.4f.txt' % (i)
        ensp = read_ensp(phname,ne,nt)
        summ += res
        summ_ensp += ensp/np.amax(ensp)
    summ /= (iterations[1] - iterations[0])
    summ_ensp /= (iterations[1] - iterations[0])
    av_energy = np.sum(summ)*de
    av_energy_full = np.sum(summ_ensp)*de
    s_e = np.sum(summ, axis = 1)
    max_energy = find_max_energy(s_e,de)
    max_energy_full = find_max_energy(summ_ensp,de)
    
    print 'Max energy ', max_energy, 'Max energy full ', max_energy_full
    print 'Av energy', av_energy, 'Av energy full ', av_energy_full

    f = open(path + '/' + filename + '.txt', 'w')
    f.write('%lf %lf %lf %lf\n' % (av_energy, max_energy, av_energy_full, max_energy_full))
    f.close()
    
#    summ /= np.amax(summ)
    
    fig = plt.figure(num=None, figsize=(15, 5), tight_layout = True)
    axel = fig.add_subplot(1,3,1)
    axel.imshow(summ + 1e-30, aspect = 'auto',  extent = [tmin, tmax, emin, emax], origin = 'lower', norm=clr.LogNorm(summ.max()*1e-5, summ.max()),
                interpolation = 'bicubic')
    axel.set_ylabel('W, GeV')
    axel.set_xlabel(u'$\\theta$')
    axel.set_xlim([0,90])
    axel.set_ylim([0,1.5])

    s_t = sp_theta(summ, min_energy = 0.5, de=de)
    s1_t = sp_theta(summ, de=de)
    s2_t = sp_theta(summ, max_energy = 0.5, de=de)
        
    s_t_sin = np.zeros(len(s_t))
    s1_t_sin = np.zeros(len(s_t))
    s2_t_sin = np.zeros(len(s_t))
    for j in range(len(s_t)):
        if s_t[j] != 0:
            s_t_sin[j] = s_t[j]/math.sin((j+0.5)*dth)
        s1_t_sin[j] = s1_t[j]/math.sin((j+0.5)*dth)
        s2_t_sin[j] = s2_t[j]/math.sin((j+0.5)*dth)


    s_t_sin_ = utils.savitzky_golay(s_t_sin,3,1)
    s1_t_sin_ = utils.savitzky_golay(s1_t_sin,3,1)
    s2_t_sin_ = utils.savitzky_golay(s2_t_sin,3,1)
    ax1 = fig.add_subplot(1,3,2)
    ax1.plot(axt, s_t_sin_, 'r', label = u'> 0.5 GeV')
    ax1.plot(axt, s2_t_sin_, 'g', label = u'< 0.5 GeV')
    ax1.plot(axt, s1_t_sin_, 'b', label = u'full')
    plt.legend(loc = 'upper left', frameon = False)
    ax1.set_xlim([0, 90.])
    m = 1.1*max([np.amax(s_t_sin), np.amax(s1_t_sin), np.amax(s2_t_sin)])
    ax1.set_ylim([1e-4*m, m])
    ax1.set_yscale(u'log')
    ax1.set_xlabel(u'$\\theta$')
    ax1.set_ylabel(u'dW/$\sin$ $\\theta$ d$\\theta$')
    
    f = open(path + '/' + filename + '_dn.txt', 'w')
    for i in range(len(s_t_sin_)):
        f.write('%lf %lf %lf %lf\n' % (axt[i], s_t_sin_[i], s1_t_sin_[i], s2_t_sin_[i]))
    f.close()
    
    ax1 = fig.add_subplot(1,3,3)

    ax1.plot(axe, s_e, 'r', label = 'radiated')
    ax1.plot(axe, summ_ensp, 'r', dashes = [3,3], label = 'full')
    ax1.set_yscale('log')
    m = 1.1*max(s_e)
#    ax1.set_ylim([1e-4*m, m])
    plt.legend(loc = 'upper right', frameon = False)
    ax1.set_xlim([0, 3])
    ax1.set_ylabel(u'dW/d$\hbar\omega$')
    ax1.set_xlabel('$\hbar\omega$')
    f = open(path + '/' + filename + '_sp.txt', 'w')
    for i in range(len(axe)):
        f.write('%lf %lf %lf\n' % (axe[i], s_e[i], summ_ensp[i]))
    f.close()
    
    picname = picspath + '/' + filename +'_%d.png' % (ppw)
    plt.tight_layout()
    plt.savefig(picname)
    plt.close()

def main():
   
    path = './'
    picspath = './pics'

    config = utils.get_config(path + "/ParsedInput.txt")
    ev = float(config['eV'])
    wl = float(config['Wavelength'])

    nit = int(config['BOIterationPass'])
    dt = float(config['TimeStep'])*nit
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    duration = float(config.get('Duration',15))
    Xmax = float(config['X_Max']) #mkm
    Xmin = float(config['X_Min']) #mkm
    Ymax = float(config['Y_Max']) #mkm
    Ymin = float(config['Y_Min']) #mkm
    Zmax = float(config['Z_Max']) #mkm
    Zmin = float(config['Z_Min']) #mkm

    ne_xmax = float(config['ne.SetBounds_1'])
    ne_nx = int(config['ne.SetMatrixSize_0'])
    ne_dx = ne_xmax/ne_nx*1e4
    ne_axis = utils.create_axis(ne_nx, ne_dx)
    
    
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)
    T = 2 * math.pi/omega
    step = float(config['TimeStep'])*float(config['BOIterationPass'])
    nT = int(T/step)
    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz
    dmy = int(0.35*wl/dy)
    dv = 2.*dx*dy*dz
    e_axis = utils.create_axis(nx, dx*1e4, Xmin*1e4)
    
    nx1 = int(config['MatrixSize_X'])
    ny1 = int(config['MatrixSize_Y'])
    T = 2 * math.pi/omega
    nnn = int(T/dt)

    ftype = config['BODataFormat']
    print 'Read electric field in the center vs time'
    ez_c_t = utils.ts(path, utils.ezpath, name='ec', ftype = ftype,
                    tstype='center', shape = (nx1,ny1), verbose=True)
    print 'Read max electric field vs time'
    ez_m_t = utils.ts(path, utils.eypath, name='em', ftype = ftype,
                    tstype='max', shape = (nx1,ny1))
    print 'Read electric field in max in dipole wave vs time'
    ez_md_t = utils.ts(path, utils.ezpath, name='em_d', ftype = ftype,
                      tstype='func', func = lambda x: x[nx1/2, ny1/2-dmy],
                      shape = (nx1,ny1), verbose=True)
    print 'Read magnetic field in the center vs time'
    bz_c_t = utils.ts(path, utils.bzpath, name='bc', ftype = ftype,
                    tstype='center', shape = (nx1,ny1), verbose=True)
    print 'Read max magnetic field vs time'
    bz_m_t = utils.ts(path, utils.bypath, name='bm', ftype = ftype,
                    tstype='max', shape = (nx1,ny1), verbose=True)
    print 'Read Ne in the center vs time'
    nez_c_t = utils.ts(path, utils.nezpath, name='nec', ftype = ftype,
                    tstype='center', shape = (nx1,ny1), verbose=True)
    print 'Read max Ne vs time'
    nez_m_t = utils.ts(path, utils.nezpath, name='nem', ftype = ftype,
                    tstype='max', shape = (nx1,ny1), verbose=True)
    xmin = 0
    xmax = -1
    axis_t = utils.create_axis(ez_c_t.shape[0], 1./nT)
    p = 0
    p_prev = 0
    i0 = 0
    i1 = 0
    threshold = 0.97
    nl_power = np.zeros(len(ez_m_t))
    
    for i in range(len(ez_m_t)):
        p_prev = p
        p = ((bz_m_t[i]/3e11)**2 + (ez_m_t[i]/0.653/3e11)**2)*10.
        
        if i0 == 0 and p > threshold * ppw and p_prev < threshold * ppw:
            i0 = i
        if i1 == 0 and p < threshold * ppw and p_prev > threshold * ppw:
            i1 = i
        nl_power[i] = ((bz_c_t[i]/3e11)**2 + (ez_md_t[i]/0.653/3e11)**2)*10.
    nt = (i1 - i0)/(nT/2)-1
    if nt == 0:
        nt = 1
    nt2 = nT/2
    print i0, i1, nt, nT/2
    extent = [Xmin*1e4, Xmax*1e4, Ymin*1e4, Ymax*1e4]
    
    
    iterations = [i0+60, i0 + nt*nT/2 + 60]
       
    process_dn(path, picspath, iterations, 'linear_ph', ppath = 'ph')
    process_dn(path, picspath, iterations, 'linear_el', ppath = 'el')

#    nt = 6
#    iterations = [i1+60, i1 + nt*nT/2 + 60]
       
#    process_dn(path, picspath, iterations, 'transient_ph', ppath = 'ph')
#    process_dn(path, picspath, iterations, 'transient_el', ppath = 'el')
#    exit(-1)
    
    if ppw > 15:        
        i2 = i1 + 120
    else:
        i2 = i1 + 200
    i3 = len(ez_m_t)-1
    nt = 6#(i3 - i2)/(nT/2)-1
    tmp = (i2+nt2)/nt2
    i22 = tmp*nt2
    tmp = (i1+nt2)/nt2
    i11 = tmp*nt2
    print i3, i2, nt, nT/2, (i2+nt2)/nt2
    
    if ppw != 35:
        iterations = [i22,i22 + nt*nT/2]
        process_dn(path, picspath, iterations, 'nonlinear_ph', ppath = 'ph')
        process_dn(path, picspath, iterations, 'nonlinear_el', ppath = 'el')
        iterations = [i11,i11 + nt*nT/2]
        process_dn(path, picspath, iterations, 'transient_ph', ppath = 'ph')
        process_dn(path, picspath, iterations, 'transient_el', ppath = 'el')
    else:
        nt = 10
        iterations = [360,360 + nt*nT/2]
        process_dn(path, picspath, iterations, 'nonlinear1_ph', ppath = 'ph')
        process_dn(path, picspath, iterations, 'nonlinear1_el', ppath = 'el')

        nt = (i3 - 600)/(nT/2)
        iterations = [600,600 + nt*nT/2]
        process_dn(path, picspath, iterations, 'nonlinear2_ph', ppath = 'ph')
        process_dn(path, picspath, iterations, 'nonlinear2_el', ppath = 'el')
        
    
            
if __name__ == '__main__':
    main()
