#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils

def read_spectra(d, nmin, nmax):
    fname = d + '/spectra_nT_time_norm_%d_%d.dat' % (nmin,nmax)
    f = open(fname, 'r')
    energy = []
    axis = []
    for line in f:
        tmp = line.split()
        axis.append(float(tmp[0]))
        energy.append(float(tmp[1]))
    f.close()
    return axis, energy

def main():
    picspath = 'pics'
    dirs = ['/home/evgeny/d2/mdipole/test_25pw/', '/home/evgeny/d3/results/10pw', '/home/evgeny/d3/results/25pw']
    nmin = [180, 200, 300]
    nmax = [270, 260, 390]
    labels = [u'm-тип 25 ПВт', u'e-тип 10 ПВт', u'e-тип 25 ПВт']
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
 
    fig = plt.figure(num=None, figsize=(7, 5.))
    mp.rcParams.update({'font.size': 14})
    ax1 = fig.add_subplot(1,1,1)
    for i in range(len(dirs)):
        axis, spectra = read_spectra(dirs[i], nmin[i], nmax[i])
        p, = ax1.plot(axis, spectra, label = labels[i])
               
    ax1.set_ylabel('$dW/d\\varepsilon_{\gamma}$')
    ax1.set_xlabel(u'$\\varepsilon_{\gamma}$ ГэВ')
    ax1.set_xlim([0, 3.5])
    ax1.set_ylim([1e-6, 1])
    ax1.set_yscale('log')
    plt.legend(frameon=False, loc = 'upper right', numpoints=1)
#    plt.tight_layout()
    picname = picspath + '/' + "gamma_mdipole_vs_edipole_spectra.png"
    plt.savefig(picname, dpi=512)
    plt.close()
   
            
            
   
if __name__ == '__main__':
    main()
