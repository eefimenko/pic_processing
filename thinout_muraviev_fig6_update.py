#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import numpy as np
import os
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

def read_data(filename, reference = None, norm=2.707895e2, dt = 1./8.):
    f = open(filename)
    axis = []
    data = []
    idx = 0
    
    for line in f:
        tmp = float(line)/1e20
        value = tmp*tmp/norm
        if reference is not None:
            value -= reference[idx]
        data.append(value)
        axis.append(idx*dt)
        idx += 1
    return np.array(axis), np.array(data)
        

def main():
    mp.rcParams.update({'font.size': 12})
    fontsize = 12
    datapath = '/home/evgeny/Dropbox/pinch_thinout/DataForPicsAfterReview'
    picspath = '/home/evgeny/Dropbox/pinch_thinout/'
    fig6path = 'Fig6'

    fig1 = plt.figure(num=None, figsize = (5,3.5))
    ax1 = fig1.add_subplot(1,1,1)
        
    ax1.set_ylim([0, 1.5])
    ax1.set_xlim([0, 4])
    ax1.set_yticks([0, 0.5, 1, 1.5])
    lw = 0.7

    categories = ['Group 1', 'Group 2', 'Group 3']
    
    ax_ref, data_ref = read_data(os.path.join(datapath, fig6path, 'EvolEl_none.txt'))
    
    ax_simple, data_simple = read_data(os.path.join(datapath, fig6path, 'EvolEl_simple.txt'), reference = data_ref)
    ps, = ax1.plot(ax_simple, data_simple, linewidth = lw, color = 'navy', label = 'simple')
    
    ax_leveling, data_leveling = read_data(os.path.join(datapath, fig6path, 'EvolEl_leveling.txt'), reference = data_ref)
    pl, = ax1.plot(ax_leveling, data_leveling, linewidth = lw, color = 'g', label = 'leveling')
    
    ax_globalLev, data_globalLev = read_data(os.path.join(datapath, fig6path, 'EvolEl_globalLev.txt'), reference = data_ref)
    pg, = ax1.plot(ax_globalLev, data_globalLev, linewidth = lw, color = 'b', label = 'globalLev')
    
    ax_merge, data_merge = read_data(os.path.join(datapath, fig6path, 'EvolEl_merge.txt'), reference = data_ref)
    pm, = ax1.plot(ax_merge, data_merge, linewidth = lw, color = 'purple', label = 'merge')

    ax_cons, data_cons = read_data(os.path.join(datapath, fig6path, 'EvolEl_conserv.txt'), reference = data_ref)
    pm, = ax1.plot(ax_cons, data_cons, linewidth = lw, color = 'k', label = 'conserv')

    ax_cons2, data_cons2 = read_data(os.path.join(datapath, fig6path, 'EvolEl_conserv2.txt'), reference = data_ref)
    pm, = ax1.plot(ax_cons2, data_cons2, linewidth = lw, color = 'k', label = 'conserv2', dashes = [3,1])

    ax_numberT, data_numberT = read_data(os.path.join(datapath, fig6path, 'EvolEl_numberT.txt'), reference = data_ref)
    pn, = ax1.plot(ax_numberT, data_numberT, linewidth = lw, color = 'r', label = 'numberT')
        
    ax_energyT, data_energyT = read_data(os.path.join(datapath, fig6path, 'EvolEl_energyT.txt'), reference = data_ref)
    pe, = ax1.plot(ax_energyT, data_energyT, linewidth = lw, color = 'c', label = 'energyT')

    ax_mergeAv, data_mergeAv = read_data(os.path.join(datapath, fig6path, 'EvolEl_mergeAv.txt'), reference = data_ref)
    pma, = ax1.plot(ax_mergeAv, data_mergeAv, linewidth = lw, color = 'orange', label = 'mergeAv')

    ax1.annotate('Group 1',
                 xy=(1.4, 0.98), xycoords='data',
                 xytext=(0.5, 0.9), textcoords='data',
                 arrowprops=dict(arrowstyle="->", linewidth = 0.5),
                 horizontalalignment='right', verticalalignment='top', fontsize = 8)

    ax1.annotate('Group 2',
            xy=(1.59, 0.65), xycoords='data',
            xytext=(0.5, 0.65), textcoords='data',
            arrowprops=dict(arrowstyle="->", linewidth = 0.5),
                 horizontalalignment='right', verticalalignment='top', fontsize = 8)

    ax1.annotate('Group 3',
                 xy=(1.38, 0.21), xycoords='data',
                 xytext=(0.5, 0.3), textcoords='data',
                 arrowprops=dict(arrowstyle="->", linewidth = 0.5),
                 horizontalalignment='right', verticalalignment='top', fontsize = 8)
    
    ax1.annotate('Group 4',
                 xy=(1.58, 0.42), xycoords='data',
                 xytext=(0.5, 0.45), textcoords='data',
                 arrowprops=dict(arrowstyle="->", linewidth = 0.5),
                 horizontalalignment='right', verticalalignment='top', fontsize = 8)
        
    ax1.legend(loc = 'upper center', fontsize = fontsize-3, frameon = False, ncol = 3)
       
    ax1.set_xlabel('Time, $1/\Gamma$')
    ax1.set_ylabel('$\Delta$D, a.u.')
    ax1.text(-0.65, 1.45, '(a)')    
    name1 = 'dD_50_new_after_review'
    
    picname1 = picspath + '/' + name1 + ".png"
    
    #plt.legend(loc = 'upper left', fontsize = 10)
    fig1.tight_layout()
    
    fig1.savefig(picname1, dpi = 256)
    
      
#    plt.show()

if __name__ == '__main__':
    main()
