#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import numpy as np
import os
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

def read_spectra(filename):
    f = open(filename, 'r')
    ax_ = []
    ph_ = []
    el_ = []
    pos_ = []
    for line in f:
        tmp = line.split()
        ax_.append(float(tmp[0]))
        ph_.append(float(tmp[1]))
        el_.append(float(tmp[2]))
        pos_.append(float(tmp[3]))
    f.close()
    return ax_, np.array(ph_), np.array(el_), np.array(pos_)

def main():
    mp.rcParams.update({'font.size': 12})
    
    picspath = '/home/evgeny/Dropbox/pinch_thinout/'
    dirs = ['27pw_default', '27pw_simple', '27pw_merge', '27pw_number', '27pw_energy']#, '27pw_leveling', '27pw_conservation']
    legends = ['globalLev/\nleveling/\nconserve', 'simple', 'merge', 'numberT', 'energyT'] #, 'leveling', 'conserve']
    fontsize = 12
    
    fig1 = plt.figure(num=None, figsize = (5,3.5))
    ax1 = fig1.add_subplot(1,1,1)
    fig2 = plt.figure(num=None, figsize = (5,3.5))
    ax2 = fig2.add_subplot(1,1,1)
    
    ax1.set_yscale('log')
    ax2.set_yscale('log')
    
    ax1.set_ylim([1e-8, 1e-2])
    ax2.set_ylim([1e-8, 1e-2])
    
    ax1.set_xlim([0, 6])
    ax2.set_xlim([0, 6])
    window = 11
    order = 7

    axins = zoomed_inset_axes(ax1, 2.5, loc= 'upper center')  # zoom = 6
    axins.set_yscale('log')
    axins.set_xlim([0.75, 1.5])
    axins.set_ylim([3e-5, 3e-4])
    
    mark_inset(ax1, axins, loc1=2, loc2=3, fc="none", ec="0.5")

    axins1 = zoomed_inset_axes(ax2, 2.5, loc= 'upper right')  # zoom = 6
    axins1.set_yscale('log')
    axins1.set_xlim([0.75, 1.5])
    axins1.set_ylim([8e-4, 9e-3])
    
    mark_inset(ax2, axins1, loc1=1, loc2=3, fc="none", ec="0.5")
    
    for d, label in zip(dirs, legends):
        ax_, ph_, el_, pos_ = read_spectra(d + '/qe_spectra.txt')
        config = utils.get_config(d + '/ParsedInput.txt')
        n_cr = float(config['n_cr'])
        ne_ = float(config['Ne'])
        Emax = float(config['QEDstatistics.Emax'])
        Emin = float(config['QEDstatistics.Emin'])
        N_E = int(config['QEDstatistics.OutputN_E'])
        de = (Emax - Emin)/N_E/1.6e-12/1e9 # erg -> eV -> GeV
        n = utils.full_number(ph_,de,N_E)
        #ax1.plot(ax_, utils.savitzky_golay(np.array(ph_)/n, window, order), linewidth = 0.9, label = label)
        ax1.plot(ax_, np.array(ph_)/n, linewidth = 0.7, label = label)
        axins.plot(ax_, np.array(ph_)/n, linewidth = 0.7, label = label)
        n = utils.full_number(el_,de,N_E)
        #ax2.plot(ax_, utils.savitzky_golay(np.array(el_)/n, window, order), linewidth = 0.9, label = label)
        ax2.plot(ax_, np.array(el_)/n, linewidth = 0.7, label = label)
        axins1.plot(ax_, np.array(el_)/n, linewidth = 0.7, label = label)
        #n = utils.full_number(pos_,de,N_E)
        #ax3.plot(ax_, utils.savitzky_golay(np.array(pos_)/n, window, order), linewidth = 0.9, label = label)
        #ax3.plot(ax_, np.array(pos_)/n, linewidth = 0.9, label = label)

    ax1.legend(loc = 'lower right', fontsize = fontsize-2, frameon = False)
    ax2.legend(loc = 'lower left', fontsize = fontsize-2, frameon = False)
    ax1.set_xlabel('$\hbar\omega$, GeV')
    ax2.set_xlabel('$\hbar\omega$, GeV')
    ax1.set_ylabel('S[$\omega$], a.u.')
    ax2.set_ylabel('S[$\omega$], a.u.')
    ax1.text(-1.25, 1e-2, '(a)')
    ax2.text(-1.25, 1e-2, '(b)')
    axins.set_xticks([])
    axins.set_yticks([])
    axins.set_yticklabels([])
    axins1.set_xticks([])
    axins1.set_yticks([])
    axins1.set_yticklabels([])
    
    name1 = '27pw_thinout_ph_spectra'
    name2 = '27pw_thinout_el_spectra'
    picname1 = picspath + '/' + name1 + ".png"
    picname2 = picspath + '/' + name2 + ".png"
    #plt.legend(loc = 'upper left', fontsize = 10)
    fig1.tight_layout()
    fig2.tight_layout()
    fig1.savefig(picname1, dpi = 256)
    fig2.savefig(picname2, dpi = 256)
      
#    plt.show()

if __name__ == '__main__':
    main()
