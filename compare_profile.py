#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])

def main():
    dir1 = 'inst50_n'
    dir2 = 'inst50_n2'

    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nixpath = 'data/Proton2Dx'
    niypath = 'data/Proton2Dy'
    nizpath = 'data/Proton2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nphxpath = 'data/Photon2Dx'
    nphypath = 'data/Photon2Dy'
    nphzpath = 'data/Photon2Dz'
    picspath = 'pics'
    config1 = utils.get_config(dir1 + "/ParsedInput.txt")
    config2 = utils.get_config(dir2 + "/ParsedInput.txt")
    wl = float(config1['Wavelength'])
    Xmax = float(config1['X_Max'])/wl #mkm to wavelength
    Xmin = float(config1['X_Min'])/wl #mkm to wavelength
    Ymax = float(config1['Y_Max'])/wl #mkm to wavelength
    Ymin = float(config1['Y_Min'])/wl #mkm to wavelength

    nx1 = int(config1['MatrixSize_X'])
    ny1 = int(config1['MatrixSize_Y'])
    nz1 = int(config1['MatrixSize_Z'])
    nx2 = int(config2['MatrixSize_X'])
    ny2 = int(config2['MatrixSize_Y'])
    nz2 = int(config2['MatrixSize_Z'])

    print 'nx1 = ' + str(nx1) + ' nx2 = '  + str(nx2)
    print 'ny1 = ' + str(ny1) + ' ny2 = '  + str(ny2)
    print 'nz1 = ' + str(nz1) + ' nz2 = '  + str(nz2)

    dx1 = float(config1['Step_X'])
    dy1 = float(config1['Step_Y'])
    dz1 = float(config1['Step_Z'])
    dx2 = float(config2['Step_X'])
    dy2 = float(config2['Step_Y'])
    dz2 = float(config2['Step_Z'])
   
    dv1 = 2*dx1*dy1*dz1
    dv2 = 2*dx2*dy2*dz2
    axis1 = create_axis(nx1, (Xmax-Xmin)/nx1, Xmin)
    axis2 = create_axis(nx2, (Xmax-Xmin)/nx2, Xmin)
    i = 2700
    for i in range(2700, 2720):
        i1 = i
        i2 = 2*i
    #    fig = plt.figure(num=None, figsize=(20, 10), dpi=256)
        fig = plt.figure()
        picname = picspath + '/' + "cmp_prof%06d.png" % (i,)
        print picname
        
        ezx = fig.add_subplot(2,2,1)
        bxx = fig.add_subplot(2,2,2)
        nex = fig.add_subplot(2,2,3)
        npx = fig.add_subplot(2,2,4)
        
        name = dir1 + '/' + bzpath + '/' + "%06d.txt" % (i1,)
        fieldb1 = read_field(name,nx1,ny1)
        name = dir2 + '/' + bzpath + '/' + "%06d.txt" % (i2,)
        fieldb2 = read_field(name,nx2,ny2)
        profb1 = fieldb1[:][ny1/2]
        profb2 = fieldb2[:][ny2/2]
        name = dir1 + '/' + ezpath + '/' + "%06d.txt" % (i1,)
        fielde1 = read_field(name,nx1,ny1)
        name = dir2 + '/' + ezpath + '/' + "%06d.txt" % (i2,)
        fielde2 = read_field(name,nx2,ny2)
        profe1 = fielde1[:][ny1/2]
        profe2 = fielde2[:][ny2/2]
        
        name = dir1 + '/' + nezpath + '/' + "%06d.txt" % (i1,)
        fieldn1 = read_field(name,nx1,ny1)
        name = dir2 + '/' + nezpath + '/' + "%06d.txt" % (i2,)
        fieldn2 = read_field(name,nx2,ny2)
        profn1 = fieldn1[:][ny1/2]
        profn2 = fieldn2[:][ny2/2]
        
        name = dir1 + '/' + npzpath + '/' + "%06d.txt" % (i1,)
        fieldp1 = read_field(name,nx1,ny1)
        name = dir2 + '/' + npzpath + '/' + "%06d.txt" % (i2,)
        fieldp2 = read_field(name,nx2,ny2)
        profp1 = fieldp1[:][ny1/2]
        profp2 = fieldp2[:][ny2/2]
        for k in range(len(fieldn1)):
            profn1[k] /= dv1
            profp1[k] /= dv1
        for k in range(len(fieldn2)):
            profn2[k] /= dv2
            profp2[k] /= dv2

        ezx.set_xlim([-0.5, 0.5])
        bxx.set_xlim([-0.05, 0.05])
        nex.set_xlim([-0.5, 0.5])
        npx.set_xlim([-0.05, 0.05])
#    az.set_ylim([-0.5, 0.5])
#    az.set_xlabel('x/lambda')
#    az.set_ylabel('y/lambda')
#    baz.set_xlim([-0.5, 0.5])
#    baz.set_ylim([-0.5, 0.5])
#    baz.set_xlabel('x/lambda')
#    baz.set_ylabel('y/lambda')
#    naz.set_xlim([-0.5, 0.5])
#    naz.set_ylim([-0.5, 0.5])
#    naz.set_xlabel('x/lambda')
#    naz.set_ylabel('y/lambda')
           
        ezx.plot(axis1,profe1,'r', label = 'x1')
        ezx.plot(axis2,profe2,'b', label = 'x2')
        
        bxx.plot(axis1,profb1,'r', label = 'x1')
        bxx.plot(axis2,profb2,'b', label = 'x2') 

        nex.plot(axis1,profn1,'r', label = 'x1')
        nex.plot(axis2,profn2,'b', label = 'x2')

        npx.plot(axis1,profn1,'r', label = 'x1')
        npx.plot(axis2,profn2,'b', label = 'x2')
        plt.savefig(picname)
        plt.close()
#    plt.show()
    
if __name__ == '__main__':
    main()
