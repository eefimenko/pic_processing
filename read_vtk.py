#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np
from vtk import vtkStructuredPointsReader
from vtk.util import numpy_support as VN
from mayavi import mlab

def main():
    filename = sys.argv[1]
    reader = vtkStructuredPointsReader()
    reader.SetFileName(filename)
    reader.ReadAllVectorsOn()
    reader.ReadAllScalarsOn()
    reader.Update()

    data = reader.GetOutput()
    dim = data.GetDimensions()
    
    vec = list(dim)
    vec = [i-1 for i in dim]
    vec.append(3)
    u = data.GetPointData()
    print u
    print dim,vec
    u = VN.vtk_to_numpy(data.GetPointData().GetArray('density'))
    
    u = u.reshape(dim,order='F')
    print np.amax(u)
    u_y = u[:,:,dim[2]/2]
    fig = plt.figure()
    ax = fig.add_subplot(1,2,1)
    surf = ax.imshow(u_y)
    plt.colorbar(surf)
    plt.show()
    figure = mlab.figure('DensityPlot')
    pts = mlab.contour3d(u, contours=10, transparent=True)
    mlab.axes()
    mlab.show()
    
if __name__ == '__main__':
    main()
