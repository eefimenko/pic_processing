#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils
import numpy as np
import glob
import pickle
import os

def read_weights(path, iteration, ptype = 'el', prefix = 'weights', bins = 100, logbins = False, logfactor = 1.1, step = None, processes = None, nprocesses = None):
    picklename = '%s/weights/%s/%s_%d.pkl' % (path,ptype,prefix,iteration)
    wmin = 1e9
    wmax = 0.
    
    if os.path.exists(picklename):
        with open(picklename) as f:
            axis = pickle.load(f)
            weights = pickle.load(f)
    else:
        if processes == None:
            filelist = glob.glob('%s/weights/%s/*_%d.txt' % (path, ptype, iteration))
        else:
            filelist = []
            for i in processes:
                for j in processes:
                    for k in processes:
                        num = i + j*8 + k*8*8
                        filelist.append('%s/weights/%s/%d_%d.txt' % (path, ptype, num, iteration))
            
        for filename in filelist:
            print filename
            with open(filename) as f:
                content = f.readline().split()
                for item in content:
                    val = float(item)
                    if val > wmax:
                        wmax = val
                    if val < wmin:
                        wmin = val

        if logbins:
            nbins = int(math.log(wmax/wmin)/math.log(logfactor))+1
            axis = np.zeros(nbins)
            step = np.zeros(nbins)
            factor = wmin
            for i in range(nbins):
                axis[i] = factor
                step[i] = (logfactor - 1.) * factor
                factor *= logfactor
        else:
            if step is None:
                nbins = bins
            else:
                nbins = int(wmax/step) + 1
            
            axis = np.zeros(nbins)
            dw = wmax/(nbins - 1)
            for i in range(nbins):
                axis[i] = dw * i
        
        weights = np.zeros(nbins)
        
        count = 0
        if logbins:
            for filename in filelist:
                print filename
                with open(filename) as f:
                    content = f.readline().split()
                    for item in content:
                        val = float(item)
                        idx = int(math.log(val/wmin)/math.log(logfactor))
                        weights[idx] += 1
                        count += 1
        else:
            for filename in filelist:
                print filename
                with open(filename) as f:
                    content = f.readline().split()
                    for item in content:
                        val = float(item)
                        idx = int(val/dw)
                        weights[idx] += 1
                        count += 1
                    
        #weights /= float(count)
        
        #if logbins:
        #    weights /= step
                       
        with open(picklename, 'w') as f:
            pickle.dump(axis, f)
            pickle.dump(weights, f)
            
    return axis, weights

def smooth_tail(array, boundary = 500, window = 15):
    for i in range(boundary, len(array)):
        array[i] = np.sum(array[i-window/2:i+window/2])/window
    return array

def test_log():
    wmax = 10
    wmin = 0.1
    logfactor = 1.1
    nbins = int(math.log(wmax/wmin)/math.log(logfactor))+1
    print nbins
    axis = np.zeros(nbins)
    weights = np.zeros(nbins)
    mult = wmin
    for i in range(nbins):
        axis[i] = mult
        print mult
        mult *= logfactor

    idx = int(math.log(10/wmin)/math.log(logfactor))
    print idx
    #for fact in [0.1,1,2,3]:
        
    return

def main():
    linear = True
    picspath = '/home/evgeny/Dropbox/pinch_thinout'
    ptype = 'ph'
    
    lw = 1.0
    initial = 4.76837e-06
    max_factor = initial
    logfactor = 1.1#2**0.25

    a0 = 2500. * math.sqrt(27./10.)
    n_c = 1.34e21 * a0/2.
    
    
    if linear:
        dirs = ['27pw_default', '27pw_conservation', '27pw_merge', '27pw_energy', '27pw_number', '27pw_leveling', '27pw_simple']
        #dirs = ['27pw_merge']
        iterations = [2600]
        ratios = [1e-3]
        titles = ['globalLev', 'conserv', 'merge', 'energyT', 'numberT', 'leveling', 'simple']
        filename = "cmp_pinch_thinout_weight_dens_%s_%g.png" % (ptype,logfactor)
    else:
        dirs = ['27pw_default', '27pw_conservation', '27pw_merge', '27pw_energy', '27pw_number', '27pw_leveling']
        iterations = [3200]
        ratios = [1e-3]
        titles = ['globalLev', 'conserv', 'merge', 'energyT', 'numberT', 'leveling']
        filename = "cmp_pinch_thinout_weight_dens_%s_%g_nl.png" % (ptype, logfactor)
    configs = []
    numdirs = len(dirs)
    
    mp.rcParams.update({'font.size': 12})
    fig1 = plt.figure(num=None, figsize = (5,3.5))
    ax1 = fig1.add_subplot(1,1,1)
    ax1.plot([initial/max_factor, initial/max_factor],[0, 1e7], color = 'k', linewidth = lw, dashes = [4,2], label = "initial weight")
    ax1.plot([275./max_factor, 275./max_factor],[0, 1e7], color = 'k', linewidth = lw, dashes = [1,1], label = "average weight")
    config = utils.get_config(dirs[0] + "/ParsedInput.txt")
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    dv = dx*dy*dz
    critical_factor = n_c * dv/max_factor
    print critical_factor
    
    if ptype == 'el':
        ax1.plot([critical_factor, critical_factor],[0, 1e7], color = 'k', linewidth = lw, dashes = [6,1,2,1], label = "critical weight")
    f_np = open('full_%s_number.txt' % ptype, 'w')
    for i in range(numdirs):
        config = utils.get_config(dirs[i] + "/ParsedInput.txt")
        for iteration in iterations:
            axis, weights = read_weights(dirs[i], iteration, prefix = 'logbins_%g_weights' % logfactor, logfactor = logfactor, ptype = ptype, step = 50., logbins = True)
            #if max_factor == 0:
            #   max_factor = np.sum(axis * weights)/np.sum(weights)
            #print(max_factor)
            av = np.sum(axis * weights)/np.sum(weights)
            dw = axis[1] - axis[0]
            #axis += dw/2
            axis /= max_factor
            av /= max_factor
            print(av, axis[0],axis[-1], initial/max_factor/axis[0])
            #weights /= (logfactor-1)*axis
            #if 'simple' not in dirs[i]:
            #    weights = smooth_tail(weights)
            f_np.write('%s %.2f\n' % (titles[i], np.sum(weights)/1.e7))
            if 'simple' in dirs[i]:
                p = ax1.plot(axis, weights, label = titles[i], linewidth = lw - 0.2)
            else:
                p = ax1.plot(axis, weights, label = titles[i], linewidth = lw)
                ax1.plot([axis[0], axis[0]], [0, weights[0]], color = p[-1].get_color(), linewidth = lw)
                ax1.plot([axis[-1], axis[-1]], [0, weights[-1]], color = p[-1].get_color(), linewidth = lw)
#            if 'default' in dirs[i]:
#                ax1.plot([axis[-1], axis[-1]], [0, weights[-1]], color = p[-1].get_color(), linewidth = lw)
#            if 'energy' in dirs[i]:
            
#            ax1.plot([av, av], [0, 100], color = p[-1].get_color(), dashes = [2,1])
        
        print len(weights)

    f_np.close()
    initial = 4.76837e-06
    print "%g" % (275/initial)
    ax1.plot([initial/max_factor, initial/max_factor],[0, 1e7], color = 'k', linewidth = lw, dashes = [4,2])
    ax1.plot([275./max_factor, 275./max_factor],[0, 1e7], color = 'k', linewidth = lw, dashes = [1,1])
    if ptype == 'el':
        ax1.plot([critical_factor, critical_factor],[0, 1e7], color = 'k', linewidth = lw, dashes = [6,1,2,1])
    ax1.set_xscale('log')
    ax1.set_yscale('log')
    ax1.set_xlabel('w')
    ax1.set_ylabel('dN/dln(w)')
    ax1.set_ylim([0.8, 1e10])
    ax1.set_yticks([1, 1e2, 1e4, 1e6, 1e8])
    ax1.set_yticklabels(['$1$', '$10^2$', '$10^4$', '$10^6$', '$10^8$'])
    ax1.set_xticks([1e-3, 1, 1e3, 1e6, 1e9, 1e12])
    ax1.set_xticklabels(['$10^{-3}$', '$1$', '$10^3$', '$10^6$', '$10^9$', '$10^{12}$'])
#    ax1.set_xlim([5e-2, 2e6])
#    ax1.set_xticks([0.1, 1,10,100,1000,10000, 100000, 1000000])
    if ptype == 'el':
        ax1.text(6e-18*275/initial, 5e9, '(b)')
    else:
        ax1.text(6e-18*275/initial, 5e9, '(a)')
    plt.legend(loc = 'upper left', frameon = False, fontsize = 9.5, ncol = 3, columnspacing = 1.5)
    #plt.show()
    picname = picspath + '/' + filename
    plt.tight_layout()
    print(picname)
    plt.savefig(picname, dpi=512)
    plt.close()

    
if __name__ == '__main__':
    main()
