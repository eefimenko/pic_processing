#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np
import matplotlib as mp
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

def read_ang_spectra(name):
    f = open(name, 'r')
    axt = []
    spectra = []
    for line in f:
        tmp = line.split()
        axt.append(float(tmp[0]))
        spectra.append(float(tmp[1]))
    return np.array(axt)/(180)*math.pi, np.array(spectra)/max(spectra[1:len(spectra)/2])

def read_spectra(d, nmin, nmax):
    fname = d + '/spectra_nT_time_norm_%d_%d.dat' % (nmin,nmax)
    f = open(fname, 'r')
    energy = []
    axis = []
    for line in f:
        tmp = line.split()
        axis.append(float(tmp[0]))
        energy.append(float(tmp[1]))
    f.close()
    return axis, energy

def main():
    power_p = []
    ezx_p = []
    bzx_p = []
    nex_p = []
    nemaxx_p = []
    ncrx_p = []
    jx_min_p = []
    jx_max_p = []
    powx_p = []
    pow1x_p = []
    maxx_p = []
    avx_p = []
    el_powx_p = []
    el_pow1x_p = []
    el_maxx_p = []
    el_avx_p = []
    picspath = 'pics/'
    path_19pw = '19pw_new/'
    path_30pw = '30pw_new/'
    f_nonlinear = 'dn_400_460.dat'
    f_linear = 'dn_200_260.dat'
    lfontsize = 12
    fontsize = 16
    mp.rcParams.update({'font.size': fontsize})
    ax19, sp19 = read_ang_spectra(path_19pw + f_nonlinear)
    ax19_l, sp19_l = read_ang_spectra(path_19pw + f_linear)
    print sp19_l
    ax30, sp30 = read_ang_spectra(path_30pw + f_nonlinear)
    ax30_l, sp30_l = read_ang_spectra(path_30pw + f_linear)
    num1 = 11
    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    ax = fig.add_subplot(1,1,1)
    ax.plot(ax19_l, sp19_l, 'b', label = 'Linear', dashes = [2,2])
    ax.plot(ax19, sp19, 'g', label = 'Nonlinear 19 PW', dashes = [6,2])
    ax.plot(ax30, sp30, 'r', label = 'Nonlinear 27 PW')
#    ax.plot(ax30_l, sp30_l, 'b--')
    ax.set_yscale('log')
#    ax.set_xscale('log')
    ax.set_ylim([2e-5,1.1])
#    ax.set_xticks([0.01, 0.1, math.pi/4, math.pi/2])
#    ax.set_xticklabels(['$10^{-2}$', '$10^{-1}$', '$\pi/4$', '$\pi/2$'])
    ax.set_xticks([0, math.pi/8, math.pi/4, 3.*math.pi/8, math.pi/2])
    ax.set_xticklabels(['$0$', '$\pi/8$', '$\pi/4$','$3\pi/8$', '$\pi/2$'])
    ax.set_xlim([0,math.pi/2.])
    ax.set_yticks([1e-4, 1e-3, 1e-2, 1e-1, 1])
    ax.set_yticklabels(['$10^{-4}$', '$10^{-3}$', '$10^{-2}$', '$10^{-1}$', '$1$'])
    ax.set_xlabel('$\\theta$')
    ax.set_ylabel('$W_\Omega^\prime$')
    ax.text(-0.4, 1, '$(b)$')
    plt.legend(bbox_to_anchor = (1.05,0.78), fontsize = lfontsize, frameon=False, labelspacing = 0.1)
#    plt.tight_layout())
    picname = picspath + '/' + "pinch_fig5b.png"
    plt.tight_layout()
    plt.savefig(picname, dpi=256)
    plt.close()
    
    f = open('stat_summary_el.txt', 'r')
    for line in f:
        tmp = line.split()
        power_p.append(float(tmp[0]))
        ezx_p.append(float(tmp[1])/1e11)
        bzx_p.append(float(tmp[2])/1e11)
        maxx_p.append(float(tmp[3]))
        avx_p.append(float(tmp[4])*1e3)
        ncrx_p.append(float(tmp[5]))
        nex_p.append(float(tmp[6])/1e25)
        powx_p.append(float(tmp[7]))
        pow1x_p.append(float(tmp[8]))
        jx_min_p.append(float(tmp[9]))
        jx_max_p.append(float(tmp[10]))
        el_maxx_p.append(float(tmp[11]))
        el_avx_p.append(float(tmp[12])*1e3)
        el_powx_p.append(float(tmp[13]))
        el_pow1x_p.append(float(tmp[14]))
        nemaxx_p.append(float(tmp[15])/1e25)
    f.close()
    nmax = 100
    dp = power_p[-1]/nmax
    p = np.zeros(nmax)
    e = np.zeros(nmax)
    b = np.zeros(nmax)
    for i in range(nmax):
        p[i] = dp*i
        e[i] = 3*math.sqrt(p[i]/10)
        b[i] = 0.653*3*math.sqrt(p[i]/10)

    thr_power = 7.25
    ep = 3*math.sqrt(thr_power/10)
    bp = 0.653*3*math.sqrt(thr_power/10)
   
    xticks = [10,15,20,25,30]
    xticklabels = ['$%d$'%(x) for x in xticks]
    fontsize = 14
    
    rpow_p = []
    rpow1_p = []
    eff_p = []
    eff1_p = []
    
    for i in range(len(power_p)):
        el_powx_p[i] = power_p[i] - el_powx_p[i]
        powx_p[i] = power_p[i] - powx_p[i]
        rpow_p.append(power_p[i] - powx_p[i] - 2*el_powx_p[i])
        print power_p[i], powx_p[i], el_powx_p[i], power_p[i] - powx_p[i] - 2*el_powx_p[i]
        eff_p.append((powx_p[i] + 2*el_powx_p[i])/power_p[i])
        eff1_p.append(powx_p[i]/power_p[i])

    power1_p = power_p
    rpow1_p = rpow_p
    powx1_p = powx_p
    el_powx1_p = el_powx_p

    
    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    plt.minorticks_on()  
    ax = fig.add_subplot(1,1,1)
    ax.plot(power1_p, rpow1_p, 'b-o', label = 'Reflected',fillstyle = 'none')
    ax.plot(power1_p, powx1_p, 'g-v', label = 'Photons',fillstyle = 'none')
    ax.plot(power1_p, el_powx1_p, 'r-^', label = 'Electrons',fillstyle = 'none')
    ax.plot([20, 20], [0, 1.1*max(powx1_p)], 'k-', dashes = [2,2])
    # ax.plot([thr_power, thr_power], [0, 8], 'b--', label = 'Threshold')
    ax.set_xlim([7,power_p[-1]+0.5])
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    yticks = [5,10,15,20,25]
    ax.set_yticks(yticks)
    ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ax.set_xlabel('$P, PW$')
    ax.set_ylabel('$P_r, PW$')
    # ax.set_xticks([0,5,thr_power,10,15])
    ax.text(1, 25, '$(d)$')
    plt.legend(loc = 'upper left', fontsize = lfontsize, frameon = False)
    picname = picspath + '/' + "pinch_fig5d.png"
    plt.savefig(picname, dpi=256, bbox_inches='tight')
    
    plt.close()
 
    f = open('charge.txt', 'r')
    power_p = []
    charge = []
    el_flux = []
    ph_flux = []
    for line in f:
        tmp = line.split()
        power_p.append(float(tmp[0]))
        charge.append(float(tmp[1]))
        el_flux.append(float(tmp[2]))
        ph_flux.append(float(tmp[3]))
    f.close()

    f = open('charge_all.txt', 'r')
    power_all_p = []
    charge_all = []
    el_flux_all = []
    ph_flux_all = []
    for line in f:
        tmp = line.split()
        power_all_p.append(float(tmp[0]))
        charge_all.append(float(tmp[1]))
        el_flux_all.append(float(tmp[2])/1e3)
        ph_flux_all.append(float(tmp[3])/1e4)
    f.close()
    
    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    fl_ax = fig.add_subplot(1,1,1)
    plt.minorticks_on()  
    gev, = fl_ax.plot(power_p, ph_flux, 'g-v', label = '> 1 GeV', fillstyle = 'none')
    fl_ax_all = fl_ax.twinx()
    plt.minorticks_on()
    full, = fl_ax_all.plot(power_all_p, ph_flux_all, 'r-v', label = 'Full', fillstyle = 'none')
    fl_ax.plot([20, 20], [0, max([max(ph_flux),max(ph_flux_all)])], 'k-', dashes = [2,2])
    fl_ax.set_ylabel('$F_{1Gev}, \\times 10^{23} s^{-1}$')
    fl_ax_all.set_ylabel('$F_{full}, \\times 10^{27} s^{-1}$')
    fl_ax.set_xlabel('$P, PW$')
    fl_ax.set_xlim([7,power_p[-1]+0.5])
    fl_ax.set_xticks(xticks)
    fl_ax.set_xticklabels(xticklabels)
    yticks = [0,2,4,6,8]
    fl_ax.set_yticks(yticks)
    fl_ax.set_yticklabels(['$%d$'%(i_) for i_ in yticks])
    yticks = [0,2,4,6]
    fl_ax_all.set_yticks(yticks)
    fl_ax_all.set_yticklabels(['$%d$'%(i_) for i_ in yticks])
    # fl_ax.text(5.8, 9, '(f)')
    plt.legend([gev, full], ['> 1 GeV', 'Full'], loc = 'upper left', fontsize = lfontsize, frameon = False)
    picname = picspath + '/' + "pinch_fig5c.png"
    fl_ax.text(1, 8, '$(c)$')
    plt.savefig(picname, dpi=256, bbox_inches='tight')
    plt.close()

    dirs = ['/home/evgeny/d3/results/19pw/', '/home/evgeny/d3/results/19pw', '/home/evgeny/d3/results/27pw']
    nmin = [300, 600, 460]
    nmax = [360, 660, 490]
    dashes = [[2,2], [6,2], []]
    labels = [u'Linear 19 PW', u'Nonlinear 19 PW', u'Nonlinear 27 PW']
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
 
    fig = plt.figure(num=None, figsize=(4., 3.))
    ax1 = fig.add_subplot(1,1,1)
    plt.minorticks_on()  
    for i in range(len(dirs)):
        axis, spectra = read_spectra(dirs[i], nmin[i], nmax[i])
        p, = ax1.plot(axis, spectra, label = labels[i], dashes = dashes[i])
               
    ax1.set_ylabel('$dW/d\\varepsilon_{\gamma}$')
    ax1.set_xlabel(u'$\\varepsilon_{\gamma}$  [GeV]')
    ax1.set_xlim([0, 3.5])
    ax1.set_xticks([0, 1, 2, 3])
    ax1.set_xticklabels(['$0$', '$1$', '$2$', '$3$'])
    ax1.set_yscale('log')
    ax1.set_ylim([1e-6, 1])
#    ax1.set_yticks([1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1])
    ax1.set_yticks([1e-6, 1e-4, 1e-2, 1])
    ax1.set_yticklabels(['$10^{-6}$', '$10^{-4}$', '$10^{-2}$', '$1$'])
#    ax1.set_yticklabels(['$10^{-6}$', '$10^{-5}$', '$10^{-4}$', '$10^{-3}$', '$10^{-2}$', '$10^{-1}$', '$1$'])
    ax1.text(-1,1, '$(a)$')
    plt.legend(frameon=False, loc = 'upper right', fontsize = lfontsize, numpoints=1, labelspacing = 0.1)
    plt.tight_layout()
    picname = picspath + '/' + "pinch_fig5a.png"
    print picname
    plt.savefig(picname, dpi=256)
    plt.close()
if __name__ == '__main__':
    main()
