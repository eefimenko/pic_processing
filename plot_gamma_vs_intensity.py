#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils

def read_gamma(filename):
    f = open(filename, 'r')
    intensity = []
    gamma = []
    for line in f:
        tmp = line.split()
        if float(tmp[0]) < 30:
            intensity.append(3*math.sqrt(float(tmp[0])/10.))
            gamma.append(float(tmp[1]))
    f.close()
    return intensity, gamma

def main():
    picspath = 'pics'
    intensity, gamma = read_gamma("gamma.dat")
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
    fig = plt.figure(num=None, figsize=(7, 5.))
    mp.rcParams.update({'font.size': 14})
    ax1 = fig.add_subplot(1,1,1)
    p, = ax1.plot(intensity, gamma, 'r', label = u'Идеальная волна')
    p1, = ax1.plot([2.7], [0.232], 'bv', markersize = 10, label = u'12 пучков 10 ПВт')
    p1, = ax1.plot([3.04], [1.05], 'go', markersize = 10, label = u' 6 пучков 20 ПВт')
    p1, = ax1.plot([3.54], [1.74], 'k^', markersize = 10, label = u' 4 пучка  40 ПВт')
    ax1.set_ylabel('$\Gamma T$')
    ax1.set_xlabel(u'Электрическое поле, $\\times 10^{11}$ СГС')
    ax1.set_xlim([2.5, 4])
    ax1.set_ylim([0, 2.5])
    plt.legend(frameon=False, loc = 'upper left', numpoints=1)
    plt.tight_layout()
    picname = picspath + '/' + "kostyukov_ideal_vs_beams.png"
    plt.savefig(picname, dpi=512)
    plt.close()
    

if __name__ == '__main__':
    main()
