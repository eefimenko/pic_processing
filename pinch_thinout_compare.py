#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import shutil
import os
import scipy.signal.signaltools as sigtool
import numpy as np
import copy
from matplotlib import gridspec
import matplotlib.colors as clr
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

def cvalue(array):
    s = array.shape
    dx = 2
    dy = 2
    xmin = s[0]/2-dx
    xmax = s[0]/2+dx
    ymin = s[1]/2-dy
    ymax = s[1]/2+dy
    
    return np.amax(array[xmin:xmax,ymin:ymax])

def main():
    fs = 24
    picspath = '/home/evgeny/Dropbox/pinch_thinout'
    dirs = ['27pw_default', '27pw_simple', '27pw_merge', '27pw_number']#, '27pw_leveling', '27pw_conservation']
    legends = ['globalLev/\nleveling/\nconserve', 'simple', 'merge/\nenergyT', 'numberT'] #, 'leveling', 'conserve']
    
    fig1 = plt.figure(num=None, figsize = (2.5,1.75))
    ax1 = fig1.add_subplot(1,1,1)
    fig2 = plt.figure(num=None, figsize = (2.5,1.75))
    ax2 = fig2.add_subplot(1,1,1)
    fig3 = plt.figure(num=None, figsize = (5,3.5))
    ax3 = fig3.add_subplot(1,1,1)
    fig4 = plt.figure(num=None, figsize = (5,3.5))
    ax4 = fig4.add_subplot(1,1,1)
    
    mp.rcParams.update({'font.size': fs})
    
    t_linear_start = 9
    t_linear_end = 13
    fontsize = 12
    axins = zoomed_inset_axes(ax4, 2.5, loc= 'lower right')  # zoom = 6
    axins.set_yscale('log')
    axins.set_xlim([12, 14])
    axins.set_ylim([1e8, 1e10])
    
    mark_inset(ax4, axins, loc1=1, loc2=3, fc="none", ec="0.5")
    
    for i in range(len(dirs)):
        path = dirs[i]
        print path
        config = utils.get_config(path + "/ParsedInput.txt")
        x0 = int(config['BOIterationPass'])
        y0 = float(config['TimeStep']) 
        omega = float(config['Omega'])
        ppw = int(config['PeakPowerPW'])
        ev = float(config['eV'])
        dx = float(config['Step_X'])
        dy = float(config['Step_Y'])
        dz = float(config['Step_Z'])
        nx = int(config['MatrixSize_X'])
        ny = int(config['MatrixSize_Y'])
        nz = int(config['MatrixSize_Z'])
        dv = 2*dx*dy*dz 
        T = 2 * math.pi/omega*1e15
        num = int(T/(x0*y0*1e15))
        dt = (x0*y0*1e15)/T
        dt__ = dt
        wl = float(config['Wavelength'])
        #ncr = electronMass * omega * omega/(8. * math.pi * electronCharge * electronCharge)
    
        np_t = utils.ts(path, utils.npzpath, name='npz',
                        tstype='max', shape = (nx,ny), verbose=True)/dv
        ne_t = utils.ts(path, utils.nezpath, name='nez',
                        tstype='max', shape = (nx,ny), verbose=True)/dv
        bz_t = utils.ts(path, utils.bzpath, name='bz',
                        tstype='func', shape = (nx,ny), verbose=True, func = cvalue)/(3.e11*math.sqrt(ppw/10.)*0.653)
        ez_t = utils.ts(path, utils.ezpath, name='ez',
                        tstype='max', shape = (nx,ny), verbose=True)/(3.e11*math.sqrt(ppw/10.))
        netr_t = utils.ts(path, utils.netpath, name='ne_trapped',
                          tstype='sum', verbose=True)
        npostr_t = utils.ts(path, utils.npostpath, name='npos_trapped',
                            tstype='sum', verbose=True)

        axb_ = utils.create_axis(len(ez_t), dt__)
        lw = 0.7
#        if 'number' == legends[i]:
#            lw = 2.0
        f1, = ax1.plot(axb_, np.array(ez_t), linewidth = lw, label = legends[i])

        axb_ = utils.create_axis(len(bz_t), dt__)
        f2, = ax2.plot(axb_, np.array(bz_t), linewidth = 0.7, label = legends[i])

        axb_ = utils.create_axis(len(ne_t), dt__)
        f3, = ax3.plot(axb_, np.array(ne_t), linewidth = 0.7, label = legends[i])

        axb_ = utils.create_axis(len(netr_t), dt__)
        if legends[i] != 'simple':
            gr = math.log(netr_t[t_linear_end*num]/netr_t[t_linear_start*num])/(t_linear_end - t_linear_start)
        else:
            gr = math.log(netr_t[(t_linear_end - 2)*num]/netr_t[t_linear_start*num])/(t_linear_end - t_linear_start - 2)
        print gr 
        f4, = ax4.plot(axb_, np.array(netr_t), linewidth = lw, label = legends[i])
        f4, = axins.plot(axb_, np.array(netr_t), linewidth = 0.7)
        

        ax3.set_yscale('log')
        ax4.set_yscale('log')
        ax1.set_ylabel('|E/E$_0$| ')
        ax2.set_ylabel('|B/B$_0$| ')
        ax3.set_ylabel('n$_e$, cm$^{-3}$')
        ax4.set_ylabel('N$_{tr}$')

       

        ax1.set_xlabel('t/T')
        ax2.set_xlabel('t/T')
        ax3.set_xlabel('t/T')
        ax4.set_xlabel('t/T')

    t_min = 3
    t_max = 18
    ax3.axvline(x = t_linear_start, linewidth = 0.7, color = 'gray')
    ax3.axvline(x = t_linear_end, linewidth = 0.7, color = 'gray')
    ax3.fill_between([t_linear_start, t_linear_end], 0, 1e30, facecolor='blue', alpha=0.2)
    ax3.fill_between([t_linear_end, t_max], 0, 1e27, facecolor='red', alpha=0.2)
    ax3.text(t_linear_start + 0.5, 5e13, 'Linear\nregime', fontsize = fontsize)
    ax3.text(t_linear_end + 0.5, 5e13, 'Nonlinear\nregime', fontsize = fontsize)
    ax3.legend(loc = 'upper left', fontsize = fontsize-2, frameon = False)
    ax3.text(0, 5e26, '(a)', fontsize = fontsize)
    
    ax4.axvline(x = t_linear_start, linewidth = 0.7, color = 'gray')
    ax4.axvline(x = t_linear_end, linewidth = 0.7, color = 'gray')
    ax4.fill_between([t_linear_start, t_linear_end], 0, 1e30, facecolor='blue', alpha=0.2)
    ax4.fill_between([t_linear_end, t_max], 0, 1e27, facecolor='red', alpha=0.2)
#    ax4.text(t_linear_start + 1, 5e2, 'Linear\nregime', fontsize = fontsize)
#    ax4.text(t_linear_end + 1, 5e2, 'Nonlinear\nregime', fontsize = fontsize)
    ax4.legend(loc = 'upper left', fontsize = fontsize-2, frameon = False)
    ax4.text(0, 5e11, '(b)', fontsize = fontsize)
    
    ax1.set_xlim([t_min,t_max])
    ax2.set_xlim([t_min,t_max])
    ax3.set_xlim([t_min,t_max])
    ax3.set_ylim([1e13, 1e27])
    ax3.set_xticks([5, 9, 13, 17])
    ax4.set_xlim([t_min,t_max])
    ax4.set_ylim([1e2, 1e12])
    ax4.set_yticks([1e3, 1e7, 1e11])
    ax4.set_xticks([5, 9, 13, 17])
    axins.set_xticks([])
    axins.set_yticks([])
    axins.set_yticklabels([])
    axins.set_yticklabels([])
    
    name3 = '27pw_thinout_compare_a'
    name4 = '27pw_thinout_compare_b'
    picname3 = picspath + '/' + name3 + ".png"
    picname4 = picspath + '/' + name4 + ".png"
    #plt.legend(loc = 'upper left', fontsize = 10)
    fig3.tight_layout()
    fig4.tight_layout()
    fig3.savefig(picname3, dpi = 256)
    fig4.savefig(picname4, dpi = 256)
   
if __name__ == '__main__':
    main()
