#!/usr/bin/python
import matplotlib as mp
mp.use("Agg")
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np
from tqdm import *

def read_conc(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    return tmp[0]

def read_field(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def create_pulse(n,step,amp, tp, delay):
    dt = step
    ans = []
#    tp = 30
    tau = tp/1.1437
#    delay = 23.1099984185576458468467817
    for i in range(n):
        t = i*dt-delay
        if t > 0 and t < math.pi * tau:
            tmp = math.sin(t/tau)
            f = amp*tmp*tmp
        else:
            f = 0.
        ans.append(f)
    return ans

def main():
    path = './'
    savepath = './pics'
    if len(sys.argv) == 2:
        savepath = sys.argv[1]
        
    fig, ax1 = plt.subplots()
    
    config = utils.get_config(path + "ParsedInput.txt")
    delay = float(config['delay']) #s
    BOIterationPass = float(config['BOIterationPass'])
    
    tdelay = delay*1e15

    Xmax = float(config['X_Max'])*1e4 #mkm
    Xmin = float(config['X_Min'])*1e4 #mkm
    Ymax = float(config['Y_Max'])*1e4 #mkm
    Ymin = float(config['Y_Min'])*1e4 #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
        
    row = -1
    step = float(config['TimeStep'])*1e15*BOIterationPass
    print step
    epath = 'data/E2z'
    bpath = 'data/B2z'
    nepath = 'data/NeTrap'
    nppath = 'data/NposTrap'
    picspath = 'pics'
    necpath = 'data/ne'
    npcpath = 'data/npos'
    maxf = 0.
    maxn = 0.
  
    step1 = 5.656854249492380319955242562457442546/nx
    v = 2. * math.pi * step1 * step1 * 0.8e-4 * 1e-8
    axis1 = create_axis(nx, step/0.8)
    
    Ne = []
    Bz = []
    nect = []
    npct = []
    nmax = utils.num_files(path+nepath)
    print nmax
    for i in tqdm(range(360,400)):
        nbzname = path + bpath + '/' + "%06d.txt" % (i,)
        p = np.amax(utils.fromfile(nbzname))
        Bz.append(p)
        
    pe, = ax1.plot(Bz, 'r')
#        pb, = ax1.plot(axis, Npos, label = 'Pos + %s' % path)
    ax1.legend(loc='upper right')
    ax1.set_ylabel('$B$, CGSE')
    picname = savepath + '/bz_field.png'
    print picname
    plt.savefig(picname)
    plt.close()
if __name__ == '__main__':
    main()
