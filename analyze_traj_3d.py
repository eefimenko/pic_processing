#!/usr/bin/python
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.cm as cm
import matplotlib.colors as colors
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def read_traj(file, end=1000000):
    f = open(file, 'r')
    x_ = []
    y_ = []
    z_ = []
    e_ = []
    r_ = []
    px_ = []
    py_ = []
    pz_ = []
    ev = 1.6e-12
    c = 2.99792e+10
    for line in f:
        tmp = line.split()
        i = int(tmp[0])
        x = float(tmp[1])*1e4
        y = float(tmp[2])*1e4
        z = float(tmp[3])*1e4
        px = float(tmp[4])
        py = float(tmp[5])
        pz = float(tmp[6])
        r = math.sqrt(x*x + y*y + z*z)
        pr = math.sqrt(px*px + py*py)
        e = math.sqrt(px*px + py*py + pz*pz)*c/ev*1e-9
        x_.append(x)
        y_.append(y)
        z_.append(z)
        px_.append(px)
        py_.append(py)
        pz_.append(pz)
        r_.append(r)
        e_.append(e)
        if i > end:
            break
    return x_, y_, z_, px_, py_, pz_, r_, e_

def normalize(a, n = 1.):
    if n != 0:
        for i in range(len(a)):
            a[i] /= n
    return a

def check_type(pz):
    p0 = pz[0]
    
    for i in range(1, len(pz)):
        if p0*pz[i] < 0:
            return 1
    return 0

def main():
    expath = 'data/E2x'
    partpath = 'ParticleTracking/Electrons'
#    partpath = 'ParticleTracking' 
    picspath = 'pics'
    num = 300

    config = utils.get_config("ParsedInput.txt")

    BOIterationPass = float(config['BOIterationPass'])
    dt = float(config['TimeStep'])*1e15*BOIterationPass #fs
    maxpower = float(config['PeakPower'])*1e-7*1e-15 #PW
    peakpower = float(config['PeakPower'])*1e-7*1e-15
    const_a_p = 7.81441e-9
    a0 = const_a_p * math.sqrt(peakpower*1e22)
    print a0
    print "Power = " + str(maxpower) + 'PW'
    print "dt = " + str(dt) + 'fs'
#    print "duration = " + str(duration)
#    print "delay = " + str(delay)
    
    Xmax = float(config['X_Max'])*1e4 #mkm
    Xmin = float(config['X_Min'])*1e4 #mkm
    Ymax = float(config['Y_Max'])*1e4 #mkm
    Ymin = float(config['Y_Min'])*1e4 #mkm
    Zmax = float(config['Z_Max'])*1e4 #mkm
    Zmin = float(config['Z_Min'])*1e4 #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz

    step = (Xmax-Xmin)/nx

    mult = 1/(2.*dx*dy*dz*1e-12)

    delta = 1
    jet = plt.get_cmap('jet') 
    cNorm  = colors.Normalize(vmin=0.5, vmax=a0*1e-3*0.5)
    scalarMap = cm.ScalarMappable(norm=cNorm, cmap=jet)
    print scalarMap.get_clim()
    fig = plt.figure(num=None, figsize=(30, 10), dpi=256)
    ax = fig.add_subplot(2,3,1)
    ay = fig.add_subplot(2,3,2)
    az = fig.add_subplot(2,3,3)
    ax1 = fig.add_subplot(2,3,4)
    ay1 = fig.add_subplot(2,3,5)
    az1 = fig.add_subplot(2,3,6)
    fig1 = plt.figure()
    ax3d = fig1.gca(projection='3d')
      
    n = 0
    n1 = 0
    n2 = 0
    for i in range(1,num):
        if i%100 == 0:
            print i
        x,y,z,px,py,pz,r,e = read_traj('./' + partpath + '/%d.txt' % (i), 3530)
        tr = 1
#        print len(x)
        if len(x) < 200:
            tr = 0
            continue
#        if len(r) < 1000:
#            tr = 0
#            continue
#        for i in range(len(r)):
#            if abs(r[i]) > 2.:
#                tr = 0
#                break
#        if tr == 0:
#            continue
#        n += 1
#        if len(e) == 0:
#            continue
#        if e[0] < 0.5:
#            continue
#        e = normalize(e, a0*1e-3)
#        print e[0]
        start = 0
        type_ = check_type(pz)
        
        ax.plot(x[start:], z[start:])
        ay.plot(y[start:], z[start:])
        az.plot(x[start:], y[start:])
        ax3d.plot(x,y,z)
#        ax1.plot(x[start:], z[start:])
#        ay1.plot(y[start:], z[start:])
        az1.plot(e[start:])
                    #        break
#    ax.set_xlim([-1, 1])
    xmin = -0.5
    xmax = 0.5
    ymin = -0.5
    ymax = 0.5
    zmin = -0.5
    zmax = 0.5
    ax.set_xlabel('x')
    ax.set_ylabel('z')
    ax.axhline(y=0, color = 'gray')
    ax.axvline(x=0, color = 'gray')
#    ax.set_xlim([-xmin, xmax])
#    ax.set_ylim([-ymin, ymax])
#    ay.set_xlim([-1, 1])
    ay.set_xlabel('y')
    ay.set_ylabel('z')
    ay.axhline(y=0, color = 'gray')
    ay.axvline(x=0, color = 'gray')
#    ay.set_xlim([-ymin, ymax])
#    ay.set_ylim([-zmin, zmax])
    az.set_xlabel('x')
    az.set_ylabel('y')
    az.axhline(y=0, color = 'gray')
    az.axvline(x=0, color = 'gray')
#    az.set_xlim([-xmin, xmax])
#    az.set_ylim([-zmin, zmax])
#    ax1.set_xlim([-1, 1])
#    ay1.set_xlim([-1, 1])
    
#    print float(n)/num
    print n1, n2 
    picname = picspath + '/' + "trajectories.png"
    picname3d = picspath + '/' + "trajectories3d.png"
    fig.savefig(picname)
    fig1.savefig(picname3d)
    plt.close()
    
if __name__ == '__main__':
    main()
