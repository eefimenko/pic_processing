#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
from mathgl import *

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def find_max(file, mult = 1.):
    max = 0
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]
    for p in array:
        if p > max:
            max = p
    return max

def create_subplot(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
    name = path + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny, mult)
    maxe = max([max(row) for row in field])
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = v_min, vmax = v_max, norm=clr.LogNorm())
    ax.text(-5, 4.5, 'min=%.1le max=%.1le' % (v_min, maxe), style='italic')
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
    plt.colorbar(surf,  orientation  = 'vertical', ticks = [v_min, v_max])
    return surf

def create_subplot_f(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
    name = path + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny, mult)
    maxe = max([max(row) for row in field])
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Reds', vmin = v_min, vmax = v_max, norm=clr.LogNorm())
    ax.text(-3, 4.5, '%.1le' % maxe, style='italic')
    plt.colorbar(surf, orientation  = 'vertical', ticks = [v_min, v_max])
    return surf

def main():
    expath = 'data/diagPhi'
    picspath = 'pics'

    Xmax = 180
    Xmin = 0
    Ymax = 0.02
    Ymin = 0
    nx = 180
    ny = 1000
    stepx = (Xmax - Xmin)/nx
    stepy = (Ymax - Ymin)/ny 

    for i in range(nmin,nmax):
        print "pic%06d.png" % (i,)
        fig = plt.figure(num=None, figsize=(40, 20), dpi=256)
        mp.rcParams.update({'font.size': 8})
        create_subplot(fig,i,path,nx,ny,1,1,1, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, mult=1)
        picname = picspath + '/' + "s%06d.png" % (i,)
        
        plt.savefig(picname)
        plt.close()
#        plt.show()
if __name__ == '__main__':
    main()
