#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys
import os

def read_field(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)/norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1e-9)
        field.append(row)
    return field

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])

def calc_current(array,nx,ny,dx,dy,r):
    num = 0.
    for i in range(nx):
        x = (i - nx/2)*dx
        for j in range(ny):
            y = (j-ny/2)*dy
            if x*x + y*y < r*r:
                num += array[i][j]
    return num

def main():
    picspath = 'pics'

    n = len(sys.argv)
    fig = plt.figure()
    ezx = fig.add_subplot(3,2,1)
    ezx.set_title('Electric field')
    bzx = fig.add_subplot(3,2,2)
    bzx.set_title('Magnetic field')
    ncrx = fig.add_subplot(3,2,4)
    ncrx.set_title('Ne/Ncr')
    nex = fig.add_subplot(3,2,3)
    nex.set_title('Ne')
    jx = fig.add_subplot(3,2,5)
    jx.set_title('Number of particles')
   
    electronMass = 9.10938e-28
    electronCharge = 4.8032e-10
    const_a_p = 7.81441e-9
    filename = 'ez_b_ne_ncr_j'

      
    for i in range(n-1):
        path = sys.argv[i+1] + '/'
        config = utils.get_config(path + "/ParsedInput.txt")
        dx = float(config['Step_X'])
        dy = float(config['Step_Y'])
        dz = float(config['Step_Z'])
        nx = int(config['MatrixSize_X'])
        ny = int(config['MatrixSize_Y'])
        BOIterationPass = int(config['BOIterationPass'])
        TimeStep = float(config['TimeStep'])
        dt = BOIterationPass*TimeStep*1e15
        step = float(config['TimeStep'])*float(config['BOIterationPass'])
        omega = float(config['Omega'])
        T = 2*math.pi/omega
        nper = int(T/step)/2
        dv = 2*dx*dy*dz
        coeffj = 2. * 1.6e-19 * 3e8 /(2*dz*1e-2)*1e-6
        print 'J = ', coeffj
        ppw = float(config['PeakPowerPW'])
        ncr = electronMass * omega * omega/(8. * math.pi * electronCharge * electronCharge)
        a0 = const_a_p * math.sqrt(ppw*1e22)
        
        print a0
        ncr *= a0
        
        nmin,nmax = utils.find_min_max_from_directory(path + utils.ezpath, path + utils.bzpath, path + utils.nezpath)
#        num = nmax - nmin
        ez_t = np.zeros(nmax)
        bz_t = np.zeros(nmax)
        ne_t = np.zeros(nmax)
        ncr_t = np.zeros(nmax)
        j_t = np.zeros(nmax)
        print 'min =', nmin, ' max =', nmax
        axis_t = utils.create_axis(nmax, dt)
       
        nmin = utils.read_tseries(path,filename, ez_t, bz_t, ne_t, ncr_t, j_t)
        for i in range(nmin, nmax):
            print i, 'of ', nmax
            fieldez_x = utils.bo_file_load(path + utils.ezpath,i,nx)
            ez_t[i] = np.amax(fieldez_x)
            fieldbz_x = utils.bo_file_load(path + utils.bzpath,i,nx)
            bz_t[i] = np.amax(fieldbz_x)

            ne_x = utils.bo_file_load(path + utils.nezpath,i,nx,ny)
            curr = calc_current(ne_x,nx,ny,dx,dy,1)
            ne_t[i] = np.amax(ne_x)/dv
            ncr_t[i] = ne_t[i]/ncr
            j_t[i] = curr

        utils.save_tseries(path,filename, nmin, nmax, ez_t, bz_t, ne_t, ncr_t, j_t)
        ez_e = utils.env(ez_t, axis_t)
        bz_e = utils.env(bz_t, axis_t)
        ne_e = utils.avg(ne_t, 20)
        ncr_e = utils.avg(ncr_t, 20)     
        #ezx.plot(axis_t, ez_t, label = path) 
        ezx.plot(axis_t, ez_t, label = path)
        #bzx.plot(axis_t, bz_t, label = path)
        bzx.plot(axis_t, bz_t, label = path)
        #nex.plot(axis_t, ne_t, label = path)
        nex.plot(axis_t, ne_t, label = path)
        #ncrx.plot(axis_t, ncr_t, label = path)
        ncrx.plot(axis_t, ncr_t, label = path)
        jx.plot(axis_t, j_t*coeffj, label = path)
        tbounds = [0,300]
        #tbounds = [0,axis_t[-1]]
        ezx.set_xlim(tbounds)
        bzx.set_xlim(tbounds)
        nex.set_xlim(tbounds)
        ncrx.set_xlim(tbounds)
        jx.set_xlim(tbounds)
        nex.set_yscale('log')
#        nex.set_ylim([1e18, 1e24])
#        print max(ez_t), max(ne_t)
    plt.legend(loc = 'upper right')
#    plt.savefig(picspath + '/' + "ez_t.png")
    plt.show()
    plt.close()
if __name__ == '__main__':
    main()
