#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_value(path,name):
    file = path + '/' + name
    print 'Open ' + file
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    return tmp[0][0]
    
def main():
    
    pathlist = ['10PW_temp', '15PW_temp', '20PW_temp', '40PW_temp', '50PW_temp', '80PW_temp', '100PW_temp', '200PW_temp', '300PW_temp']
    color = ['r', 'g', 'b']
    fig, ax1 =  plt.subplots()
    gamma = []
    gammath = []
    ntrapped = []
    maxph = []
    axis = []
    err = []
    maxphth = []
    gamma.append(0)
    gammath.append(0)
    ntrapped.append(0)
    axis.append(0)
    maxph.append(0)
    maxphth.append(0)
    err.append(0)
#    gamma.append(0)
#    ntrapped.append(0)
#    axis.append(8)
#    maxph.append(1.106)
    for path in pathlist:
        cname = path + '/ParsedInput.txt'
        config = utils.get_config(cname)
        power = float(config['PeakPower'])*1e-7*1e-15
        g = read_value(path, 'gamma.dat')
        n = read_value(path, 'ntrappedmax.dat')
        e = read_value(path, 'phmax1p.dat')
        Emax = float(config['QuantumParticles.Emax'])
        Emin = float(config['QuantumParticles.Emin'])
        N_E = int(config['QuantumParticles.OutputN_E']) 
        ev = float(config['eV'])
        en_step = (Emax-Emin)/N_E/ev*1e-9
        if power < 7:
            gamma.append(0)
            gammath.append(0)
        else:
            gamma.append(g)
            gammath.append(math.sqrt(power-8)*0.335)
        ntrapped.append(n)
        axis.append(power)
        maxph.append(e)
        maxphth.append(math.sqrt(power)*0.39)
        err.append(en_step)
       
#        ppp, = ax1.plot(power_t, ne_t, color[i+1])
    plt.rc('text', usetex=True)
    fig, ax1 =  plt.subplots()
    ax1.set_xlabel('Power, GW')
    ax1.set_ylabel('\gamma, fs^{-1}')
#    g, = ax1.plot(axis, gammath, 'g')
    n,b,c = ax1.errorbar(axis, gamma, color='b', fmt='o-')
    ax1.spines['left'].set_color('b')
    ax1.yaxis.label.set_color('b')
    ax1.tick_params(axis='y', colors='b')
    ax2 = ax1.twinx()
    e,b,c = ax2.errorbar(axis, maxph, yerr = err, color='r', fmt='x-')
    ax1.spines['right'].set_color('r')
    ax2.yaxis.label.set_color('r')
    ax2.tick_params(axis='y', colors='r')
    ax2.set_ylabel('Max photon energy, GeV')
    ax2.set_yscale('log')
    ax2.set_xscale('log')
    ax1.legend([n, e], ['\gamma', 'Photon energy'], loc=2)
    plt.savefig('pics/gamma.png')
    plt.close()
    
    
if __name__ == '__main__':
    main()
