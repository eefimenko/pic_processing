#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils

def read_file(filename):
    f = open(filename, 'r')
    ez = []
    br = []
    po = []
    ax = []
    for line in f:
        tmp = line.split()
        ax.append(float(tmp[0]))
        br.append(float(tmp[1]))
        ez.append(float(tmp[2]))
        po.append(float(tmp[3]))
    return ax, br, ez, po

def make_patch_spines_invisible(ax):
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)
        
def main():
    ax_, br, ez, po = read_file('res.dat')
    fig = plt.figure(num=None, figsize = (10,5))
    mp.rcParams.update({'font.size': 15})
    ax = fig.add_subplot(1,1,1)
    fig.subplots_adjust(left=0.2)
    ax2 = ax.twinx()
    ax2.spines["left"].set_position(("axes", -0.15))
    make_patch_spines_invisible(ax2)
    # Second, show the right spine.
    ax2.spines["left"].set_visible(True)
    ax2.yaxis.set_label_position('left')
    ax2.yaxis.set_ticks_position('left')
        
    f, = ax.plot(ax_, br, 'r')
    f1, = ax2.plot(ax_, ez, 'B')
    ax2.set_ylabel('$|E_z|/a_0$ ', color='B')
    ax.set_ylabel('Photon flux, $\cdot 10^{23}$ s$^{-1}$', color='r')
    ax.set_xlabel('Time, T')
    ax2.set_ylim([0,1])

    ax1 = ax.twinx()
    
    p1, = ax1.plot(ax_, po, 'g')
    ax1.set_ylabel('$\gamma$-source power, PW', color='g')
    ax1.set_xlim([5,25])
    ax1.set_ylim([0,30])
    ax.set_xlim([5,25])
    ax.yaxis.label.set_color(f.get_color())
    ax1.yaxis.label.set_color(p1.get_color())
    ax2.yaxis.label.set_color(f1.get_color())
    ax.tick_params(axis='y', colors=f.get_color())
    ax1.tick_params(axis='y', colors=p1.get_color())
    ax2.tick_params(axis='y', colors=f1.get_color())
    plt.legend([f, f1, p1], ["Flux ($\hbar \omega$ > 2 GeV)", '$|E_z|/a_0$', '$\gamma$-source power'], loc='upper left')

    plt.show()

if __name__ == '__main__':
    main()
