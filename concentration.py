#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_field2d(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)/norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def read_field(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    return tmp[0]

def create_axis(n,step, x0=0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def main():
    nx = 512
    ny = 512
    row = -1
    nepath = 'data/ne'
    nppath = 'data/npos'
    bzpath = 'data/B2z'
    ezpath = 'data/E2z'
    picspath = 'pics'
    maxf = 0.
    maxn = 0.
    
    config = utils.get_config('./ParsedInput.txt')
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    
    diag = float(config['ne.SetBounds_1'])
    ndiag = int(config['ne.SetMatrixSize_0'])
    wl = float(config['Wavelength'])
    step = diag/ndiag
    v = 2. * math.pi * step * step * wl 
    axis = create_axis(nx, step/wl)
    l = 2.
    nmax = utils.num_files(nepath)
    Xmax = float(config['X_Max'])/wl #mkm to wavelength
    Xmin = float(config['X_Min'])/wl #mkm to wavelength
    ppw = float(config['PeakPower'])*1e-22
    axis1 = create_axis(nx, (Xmax-Xmin)/nx, Xmin)
    a00 = 3.e11 * math.sqrt(ppw/10.)
    nmin,nmax,delta = utils.get_min_max_iteration(sys.argv, utils.ezpath)
    print nmin, nmax, delta
    read, eav = utils.bo_file_load(utils.nepath,0,nx)
    for i in range(nmin, nmax, delta):
#        print "%06d.txt" % (i,)
#        nename = nepath + '/' + "%06d.txt" % (i,)
#        npname = nppath + '/' + "%06d.txt" % (i,)
#        ezname = ezpath + '/' + "%06d.txt" % (i,)
#        bzname = bzpath + '/' + "%06d.txt" % (i,)
        read, e = utils.bo_file_load(utils.nepath,i,nx)
        read, p = utils.bo_file_load(utils.nppath,i,nx)
        read, ez = utils.bo_file_load(utils.ezpath,i,nx,ny)
        read, bz = utils.bo_file_load(utils.bzpath,i,nx,ny)
        eav += e
        ezp = ez[nx/2]
        bzp = bz[nx/2]
        for k in range(len(ezp)):
            ezp[k] /= a00
            bzp[k] /= a00
        ma = 0
        mi = 1e50
        for j in range(len(e)):
            e[j] = e[j]/((j+0.5)*v)
            p[j] = p[j]/((j+0.5)*v)
            if axis[j] < l:
                if e[j] > ma:
                    ma = e[j]
                if p[j] > ma:
                    ma = p[j]
                if e[j] < mi:
                    mi = e[j]
                if p[j] < mi:
                    mi = p[j]
        bound = 1.5
        fig = plt.figure(num=None)
        ax1 = fig.add_subplot(2,1,1)
        ax2 = fig.add_subplot(2,1,2)
        pe, = ax1.plot(axis, e, 'g')
        pp, = ax1.plot(axis, p, 'b')
        ax11 = ax1.twinx()
        pez, = ax11.plot(axis1, ezp, 'r')
        pbz, = ax11.plot(axis1, bzp, 'k')
        ax1.set_xlim(0., bound)
        ax11.set_xlim(0., bound)
        ax11.set_ylim(0., bound)
        ax1.set_yscale('log')
        ax1.set_ylim(mi, ma)
        ax1.legend([pe, pp], ["El", "Pos"], loc=1)
        pe, = ax2.plot(axis, e, 'g', marker = 'x')
        pp, = ax2.plot(axis, p, 'b')
        ax21 = ax2.twinx()
        pez, = ax21.plot(axis1, ezp, 'r')
        pbz, = ax21.plot(axis1, bzp, 'k')
        ax2.set_xlim(0., bound)
        ax21.set_xlim(0., bound)
        ax2.set_ylim(0, ma)
        ax21.set_ylim(0, 1)
        ax2.legend([pe, pp, pez, pbz], ["El", "Pos", 'E', 'B'], loc=1)
        picname = picspath + '/' + "n%06d.png" % (i,)
        plt.savefig(picname)
        print picname
        plt.close()
    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(1,1,1)
    pe, = ax1.plot(axis, eav, 'g')
    
    ax11 = ax1.twinx()
    pez, = ax11.plot(axis1, ezp, 'r')
    pbz, = ax11.plot(axis1, bzp, 'k')
    ax1.set_xlim(0., bound)
    ax11.set_xlim(0., bound)
    ax11.set_ylim(0., bound)
   
    ax1.set_ylim(mi, ma)
    ax1.legend([pe, pp], ["El", "Pos"], loc=1)
    
    picname = picspath + '/' + "n.png" 
    plt.savefig(picname)
    print picname
    plt.close()
    
if __name__ == '__main__':
    main()
