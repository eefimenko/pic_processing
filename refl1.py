#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def Env(R,t):
    t0 = 2.5e-15
    alpha=2.276/t0
    LightVelocity = 29979245800.0
    xm = 4.e-4
    delay = math.sqrt(3*xm*xm)/LightVelocity
    return 0.5*(1. - math.tanh(-alpha*(t + R/LightVelocity - delay)))

def Shape(R,t):
    Wavelength = 0.8e-4
    LightVelocity = 29979245800.0
    k = 2 * math.pi / Wavelength
    xm = 4.e-4
    delay = math.sqrt(3*xm*xm)/LightVelocity
    return Env(R,t) * math.sin(k *(R + LightVelocity*t - delay)) / R

def E(a,x,y,z,t):
    r2 = x*x + y*y + z*z 
    r = math.sqrt(r2)
    common = Shape(r, t) / (r2)
    ex = a*z * x * common
    ey = a*z * y * common
    ez = a*(y * y + x * x) * common
    return ex,ey,ez

def read_field(file,nx,ny,mult=1.):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index]*mult)
        field.append(row)
    return field

def main():
    datapath = 'data/E2z'
    picspath = 'pics'
    nx=512
    ny=512
    nmin = 0	
    nmax = utils.num_files(datapath)
    config = utils.get_config("ParsedInput.txt")
    c = 29979245800.0
    lw = float(config['Wavelength'])
    res = float(config['Analysis.res'])
    res_t = float(config['TimeStep'])
    xpos = (int(config['PML.Size_X'])+1)*(float(config['Step_X'])) + float(config['X_Min'])
    x0 = int(config['PML.Size_X'])+1
    BOIterationPass = float(config['BOIterationPass'])
    a = float(config['Amp'])
    ni=lw/c/res/res_t
    nIter=int(ni)
    if((ni-nIter)>0.5):
        nIter=nIter+1
    print nIter
    print 'Found ' + str(nmax) + ' files'
    fc = []
    fc1 = []
#    ex_t = []
#    ey_t = []
#    ez_t = []
    e = []
    for i in range(nmin,nmax):
        t=i*BOIterationPass*res_t
        name = datapath + '/' + "%06d.txt" % (i,)
        field = read_field(name,nx,ny,mult=1.)
        fc.append(field[x0][ny/2])
        fc1.append(field[x0+2][ny/2])
        print "\rOpen " + name
        ex,ey,ez = E(a,xpos, 0,0, t)
#        ex_t.append(extmp)
#        ey_t.append(eytmp)
#        ez_t.append(eztmp)
        e.append(math.sqrt(ex*ex + ey*ey + ez*ez))
#        fig = plt.figure()
#        ax = fig.add_subplot(1,1,1)
#        surf = ax.imshow(field, extent=[-nx/2, nx/2, -ny/2, ny], aspect='auto', origin='lower')
#        picname = picspath + '/' + "ref%06d.png" % (i,)
#        plt.savefig(picname)
#        plt.close()
        
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    pe, = ax.plot(fc, 'r')
    pe1, = ax.plot(fc1, 'g')
#    pex, = ax.plot(ex_t)
#    pey, = ax.plot(ey_t)
#    pez, = ax.plot(ez_t)
    pee, = ax.plot(e, 'b')
    picname = picspath + '/' + "field.png"
    plt.savefig(picname)
    plt.close()
if __name__ == '__main__':
    main()
