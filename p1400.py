# state file generated using paraview version 5.0.1

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

#### import the simple module from the paraview
from paraview.simple import *
import sys
import math
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

iteration = int(sys.argv[1])
path = '.'
a0 = 3e11*math.sqrt(11./10.)


# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [668, 608]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.OrientationAxesLabelColor = [0.0, 0.0, 0.0]
renderView1.OrientationAxesOutlineColor = [0.0, 0.0, 0.0]
renderView1.CenterOfRotation = [-3.90624336432666e-07, -3.90624336432666e-07, -3.90624336432666e-07]
renderView1.KeyLightAzimuth = 80.0
renderView1.FillLightElevation = 90.0
renderView1.FillLightAzimuth = -100.0
renderView1.BackLightAzimuth = -30.0
renderView1.MaintainLuminance = 1
renderView1.StereoType = 0
renderView1.CameraPosition = [-0.00043494872880235717, -0.00035185859054944743, 0.0003717134992351044]
renderView1.CameraFocalPoint = [-3.906243364326677e-07, -3.906243364326759e-07, -3.9062433643265976e-07]
renderView1.CameraViewUp = [0.37899248067624175, 0.4098278793322526, 0.8297022411159906]
renderView1.CameraParallelScale = 0.000173871129387752
renderView1.Background = [1.0, 1.0, 1.0]

# init the 'GridAxes3DActor' selected for 'AxesGrid'
renderView1.AxesGrid.Visibility = 1
renderView1.AxesGrid.XTitle = 'x, cm'
renderView1.AxesGrid.YTitle = 'y, cm'
renderView1.AxesGrid.ZTitle = 'z, cm'
renderView1.AxesGrid.XTitleColor = [0.0, 0.0, 0.0]
renderView1.AxesGrid.XTitleBold = 1
renderView1.AxesGrid.XTitleFontSize = 14
renderView1.AxesGrid.YTitleColor = [0.0, 0.0, 0.0]
renderView1.AxesGrid.YTitleBold = 1
renderView1.AxesGrid.YTitleFontSize = 14
renderView1.AxesGrid.ZTitleColor = [0.0, 0.0, 0.0]
renderView1.AxesGrid.ZTitleBold = 1
renderView1.AxesGrid.ZTitleFontSize = 14
renderView1.AxesGrid.FacesToRender = 54
renderView1.AxesGrid.GridColor = [0.0, 0.0, 0.0]
renderView1.AxesGrid.AxesToLabel = 7
renderView1.AxesGrid.XLabelColor = [0.0, 0.0, 0.0]
renderView1.AxesGrid.XLabelFontSize = 14
renderView1.AxesGrid.YLabelColor = [0.0, 0.0, 0.0]
renderView1.AxesGrid.YLabelFontSize = 14
renderView1.AxesGrid.ZLabelColor = [0.0, 0.0, 0.0]
renderView1.AxesGrid.ZLabelFontSize = 14
renderView1.AxesGrid.XAxisNotation = 'Scientific'
renderView1.AxesGrid.XAxisPrecision = 0
renderView1.AxesGrid.XAxisUseCustomLabels = 1
renderView1.AxesGrid.XAxisLabels = [0.0, 5e-05, -5e-05, -0.0001, 0.0001]
renderView1.AxesGrid.YAxisNotation = 'Scientific'
renderView1.AxesGrid.YAxisPrecision = 0
renderView1.AxesGrid.YAxisUseCustomLabels = 1
renderView1.AxesGrid.YAxisLabels = [5e-05, 0.0, -5e-05]
renderView1.AxesGrid.ZAxisNotation = 'Scientific'
renderView1.AxesGrid.ZAxisPrecision = 0

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'Legacy VTK Reader'
infile = '%s/VTKOutput/Photon_density_%d.vtk' % (path, iteration)
print 'Reading ' + infile
photon_density_vtk = LegacyVTKReader(FileNames=[infile])
phamin, phamax = photon_density_vtk.PointData.GetArray('density').GetRange()
print phamin,phamax
# create a new 'Calculator'
calculator4 = Calculator(Input=photon_density_vtk)
calculator4.ResultArrayName = 'Result3'
calculator4.Function = 'log10(density) + 18.321'

# create a new 'Legacy VTK Reader'
infile = '%s/VTKOutput/Electron_density_%d.vtk' % (path, iteration)
print 'Reading ' + infile
electron_density_vtk = LegacyVTKReader(FileNames=[infile])
amin, amax = electron_density_vtk.PointData.GetArray('density').GetRange()
print amin,amax

# create a new 'Calculator'
calculator3 = Calculator(Input=electron_density_vtk)
calculator3.ResultArrayName = 'Result2'
calculator3.Function = 'log10(density) + 18.321'

# create a new 'Contour'
contour2 = Contour(Input=calculator3)
contour2.ContourBy = ['POINTS', 'Result2']
el_threshold = math.log10(amax)+17.3
if el_threshold < 19.5:
    el_threshold = 19.5
print 'Electron density contour', el_threshold, math.log10(amax)+18.321 
contour2.Isosurfaces = [el_threshold]

contour2.PointMergeMethod = 'Uniform Binning'

# create a new 'Legacy VTK Reader'
infile = '%s/VTKOutput/electric_field_value_%d.vtk' % (path, iteration)
print 'Reading ' + infile
electric_field_value_vtk = LegacyVTKReader(FileNames=[infile])

# create a new 'Calculator'
calculator1 = Calculator(Input=electric_field_value_vtk)
calculator1.Function = 'electric_field_value/%s' % (str(a0))
efmin, efmax = electric_field_value_vtk.PointData.GetArray('electric_field_value').GetRange()

print efmin/a0,efmax/a0
# create a new 'Slice'
slice2 = Slice(Input=calculator1)
slice2.SliceType = 'Plane'
slice2.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice2.SliceType.Normal = [0.0, 0.0, 1.0]

# create a new 'Slice'
slice1 = Slice(Input=calculator1)
slice1.SliceType = 'Plane'
slice1.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice1.SliceType.Origin = [0, 0, 0]

# create a new 'Legacy VTK Reader'
infile = '%s/VTKOutput/magnetic_field_value_%d.vtk' % (path, iteration)
print 'Reading ' + infile
magnetic_field_value_vtk = LegacyVTKReader(FileNames=[infile])

# create a new 'Calculator'
calculator2 = Calculator(Input=magnetic_field_value_vtk)
calculator2.ResultArrayName = 'Result1'
calculator2.Function = 'magnetic_field_value/%s' % (str(a0))
print calculator2.Function
mfmin, mfmax = magnetic_field_value_vtk.PointData.GetArray('magnetic_field_value').GetRange()
print mfmin/a0,mfmax/a0
# create a new 'Slice'
slice7 = Slice(Input=calculator2)
slice7.SliceType = 'Plane'
slice7.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice7.SliceType.Normal = [0.0, 1.0, 0.0]

# create a new 'Contour'
contour1 = Contour(Input=calculator4)
contour1.ContourBy = ['POINTS', 'Result3']
contour1.ComputeNormals = 0
ph_threshold = math.log10(phamax)+16.7
if ph_threshold < 19:
    ph_threshold = 19
print 'Photon density contour', ph_threshold, math.log10(phamax)+17.321
contour1.Isosurfaces = [ph_threshold]
contour1.PointMergeMethod = 'Uniform Binning'

# create a new 'Slice'
slice5 = Slice(Input=contour1)
slice5.SliceType = 'Plane'
slice5.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice5.SliceType.Origin = [0.0, 0.0, -9e-05]
slice5.SliceType.Normal = [0.0, 0.0, 1.0]

# create a new 'Slice'
slice4 = Slice(Input=contour1)
slice4.SliceType = 'Plane'
slice4.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice4.SliceType.Origin = [0, 0, 0]
slice4.SliceType.Normal = [0.0, 0.0, 1.0]

# create a new 'Slice'
slice3 = Slice(Input=contour1)
slice3.SliceType = 'Plane'
slice3.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice3.SliceType.Origin = [0.0, 0.0, 9.9e-05]
slice3.SliceType.Normal = [0.0, 0.0, 1.0]

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get color transfer function/color map for 'electricfieldvalue'
electricfieldvalueLUT = GetColorTransferFunction('electricfieldvalue')
electricfieldvalueLUT.RGBPoints = [563.007202148438, 1.0, 1.0, 1.0, 103646072191.155, 1.0, 0.501960784314, 0.0, 206981826239.577, 0.501960784314, 0.0, 0.0, 310317580288.0, 0.0, 0.0, 0.0]
electricfieldvalueLUT.ColorSpace = 'RGB'
electricfieldvalueLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'electricfieldvalue'
electricfieldvaluePWF = GetOpacityTransferFunction('electricfieldvalue')
electricfieldvaluePWF.Points = [563.007202148438, 0.0, 0.5, 0.0, 168994613735.556, 0.0, 0.5, 0.0, 264857043992.558, 0.197368428111076, 0.5, 0.0, 310317580288.0, 1.0, 0.5, 0.0]
electricfieldvaluePWF.ScalarRangeInitialized = 1

# get color transfer function/color map for 'Result1'
result1LUT = GetColorTransferFunction('Result1')
result1LUT.RGBPoints = [1.68587109375e-07, 1.0, 1.0, 1.0, 0.005176542340855937, 0.764706, 0.905882, 1.0, 0.010698001811625831, 0.509804, 0.807843, 1.0, 0.016219461282395622, 0.254902, 0.705882, 1.0, 0.021740964752364506, 0.0, 0.603922, 1.0, 0.02726242422313445, 0.0, 0.505882, 0.933333, 0.03278388369390414, 0.0, 0.403922, 0.85098, 0.03830534316467404, 0.0, 0.301961, 0.764706, 0.04382682155509944, 0.0, 0.203922, 0.678431, 0.049348306105412555, 0.0, 0.101961, 0.592157, 0.05486976557618198, 0.0, 0.0, 0.509804, 0.0603912250469525, 0.0, 0.0, 0.423529, 0.06591268451772195, 0.0, 0.0, 0.337255, 0.07143414398849195, 0.0, 0.0, 0.254902, 0.07695564745846054, 0.0, 0.0, 0.168627, 0.08247710692923103, 0.0, 0.0, 0.082353, 0.0879985664, 0.0, 0.0, 0.0]

for i in range(len(result1LUT.RGBPoints)/4):
    result1LUT.RGBPoints[i*4] *= mfmax/a0/0.0879985664
    
result1LUT.ColorSpace = 'Lab'
result1LUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'Result1'
result1PWF = GetOpacityTransferFunction('Result1')
result1PWF.Points = [1.68587109375e-07, 0.0, 0.5, 0.0, 0.0879985664, 1.0, 0.5, 0.0]
for i in range(len(result1PWF.Points)/4):
    result1PWF.Points[i*4] *= mfmax/a0/0.0879985664
result1PWF.ScalarRangeInitialized = 1

# get color transfer function/color map for 'Result'
resultLUT = GetColorTransferFunction('Result')
resultLUT.RGBPoints = [0.00047234974333333363, 1.0, 1.0, 1.0, 0.005527914382020369, 1.0, 0.886275, 0.768627, 0.010920510933643744, 1.0, 0.768627, 0.521569, 0.016313107485267243, 1.0, 0.65098, 0.27451, 0.02170574700921136, 1.0, 0.533333, 0.027451, 0.0270983435608348, 1.0, 0.411765, 0.0, 0.03249094011245814, 0.905882, 0.294118, 0.0, 0.0378835366640818, 0.815686, 0.176471, 0.0, 0.04327615169380307, 0.72549, 0.058824, 0.0, 0.048668772739649126, 0.635294, 0.0, 0.0, 0.05406136929127282, 0.545098, 0.0, 0.0, 0.05945396584289619, 0.45098, 0.0, 0.0, 0.06484656239451957, 0.360784, 0.0, 0.0, 0.07023915894614294, 0.270588, 0.0, 0.0, 0.07563179847008722, 0.180392, 0.0, 0.0, 0.0810243950217106, 0.090196, 0.0, 0.0, 0.08641699157333332, 0.0, 0.0, 0.0]
for i in range(len(resultLUT.RGBPoints)/4):
    resultLUT.RGBPoints[i*4] *= efmax/a0/0.08641699157333332

resultLUT.ColorSpace = 'Lab'
resultLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'Result'
resultPWF = GetOpacityTransferFunction('Result')
resultPWF.Points = [0.00047234974333333363, 0.0, 0.5, 0.0, 0.08641699157333332, 1.0, 0.5, 0.0]
for i in range(len(resultPWF.Points)/4):
    resultPWF.Points[i*4] *= efmax/a0/0.08641699157333332
resultPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from slice1
slice1Display = Show(slice1, renderView1)
# trace defaults for the display properties.
slice1Display.ColorArrayName = ['POINTS', 'Result']
slice1Display.LookupTable = resultLUT
slice1Display.Ambient = 0.95
slice1Display.Position = [0.0001, 0.0, 0.0]
slice1Display.GlyphType = 'Arrow'

# show color legend
slice1Display.SetScalarBarVisibility(renderView1, True)

# show data from slice2
slice2Display = Show(slice2, renderView1)
# trace defaults for the display properties.
slice2Display.ColorArrayName = ['POINTS', 'Result']
slice2Display.LookupTable = resultLUT
slice2Display.Interpolation = 'Flat'
slice2Display.Specular = 1.0
slice2Display.Ambient = 1.0
slice2Display.BackfaceRepresentation = 'Surface'
slice2Display.Position = [0.0, 0.0, -0.0001]
slice2Display.GlyphType = 'Arrow'

# show color legend
slice2Display.SetScalarBarVisibility(renderView1, True)

# show data from slice7
slice7Display = Show(slice7, renderView1)
# trace defaults for the display properties.
slice7Display.ColorArrayName = ['POINTS', 'Result1']
slice7Display.LookupTable = result1LUT
slice7Display.Ambient = 1.0
slice7Display.Position = [0.0, 0.0001, 0.0]
slice7Display.GlyphType = 'Arrow'

# show color legend
slice7Display.SetScalarBarVisibility(renderView1, True)

# show data from calculator1
calculator1Display = Show(calculator1, renderView1)
# trace defaults for the display properties.
calculator1Display.Representation = 'Outline'
calculator1Display.ColorArrayName = ['POINTS', 'electric_field_value']
calculator1Display.LookupTable = electricfieldvalueLUT
calculator1Display.GlyphType = 'Arrow'
calculator1Display.ScalarOpacityUnitDistance = 1.35316469341319e-06
calculator1Display.Slice = 127

# show data from contour1
contour1Display = Show(contour1, renderView1)
# trace defaults for the display properties.
contour1Display.ColorArrayName = [None, '']
contour1Display.DiffuseColor = [1.0, 0.0, 1.0]
contour1Display.Opacity = 0.18
contour1Display.PointSize = 1.0
contour1Display.GlyphType = 'Arrow'

# show data from contour2
contour2Display = Show(contour2, renderView1)
# trace defaults for the display properties.
contour2Display.ColorArrayName = [None, '']
contour2Display.DiffuseColor = [0.0, 1.0, 0.0]
contour2Display.GlyphType = 'Arrow'

# show data from slice3
slice3Display = Show(slice3, renderView1)
# trace defaults for the display properties.
slice3Display.ColorArrayName = [None, '']
slice3Display.DiffuseColor = [1.0, 0.0, 0.498039215686275]
slice3Display.GlyphType = 'Arrow'

# show data from slice4
slice4Display = Show(slice4, renderView1)
# trace defaults for the display properties.
slice4Display.ColorArrayName = [None, '']
slice4Display.DiffuseColor = [1.0, 0.0, 0.498039215686275]
slice4Display.GlyphType = 'Arrow'

# show data from slice5
slice5Display = Show(slice5, renderView1)
# trace defaults for the display properties.
slice5Display.ColorArrayName = [None, '']
slice5Display.DiffuseColor = [1.0, 0.0, 0.498039215686275]
slice5Display.GlyphType = 'Arrow'

# setup the color legend parameters for each legend in this view

# get color legend/bar for resultLUT in view renderView1
resultLUTColorBar = GetScalarBar(resultLUT, renderView1)
resultLUTColorBar.Position = [0.9, 0.57]
resultLUTColorBar.Position2 = [0.12, 0.36]
resultLUTColorBar.Title = 'E/a0'
resultLUTColorBar.ComponentTitle = ''
resultLUTColorBar.TitleColor = [0.0, 0.0, 0.0]
resultLUTColorBar.TitleFontSize = 10
resultLUTColorBar.LabelColor = [0.0, 0.0, 0.0]
resultLUTColorBar.LabelFontSize = 10
resultLUTColorBar.AutomaticLabelFormat = 0
resultLUTColorBar.LabelFormat = '%4.1g'
resultLUTColorBar.RangeLabelFormat = '%4.1g'
resultLUTColorBar.DrawAnnotations = 0

# get color legend/bar for result1LUT in view renderView1
result1LUTColorBar = GetScalarBar(result1LUT, renderView1)
result1LUTColorBar.Position = [0.9, 0.09]
result1LUTColorBar.Position2 = [0.12, 0.35]
result1LUTColorBar.Title = 'B/a0'
result1LUTColorBar.ComponentTitle = ''
result1LUTColorBar.TitleColor = [0.0, 0.0, 0.0]
result1LUTColorBar.TitleFontSize = 10
result1LUTColorBar.LabelColor = [0.0, 0.0, 0.0]
result1LUTColorBar.LabelFontSize = 10
result1LUTColorBar.AutomaticLabelFormat = 0
result1LUTColorBar.LabelFormat = '%4.1g'
result1LUTColorBar.RangeLabelFormat = '%4.1g'

WriteImage(path + "/test_%d.png"%(iteration))
