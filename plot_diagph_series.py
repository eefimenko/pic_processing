#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def main():
    num = len(sys.argv)-2
    iteration = int(sys.argv[-1])
    bopath  = '/BasicOutput/' + utils.dphpath
    
    fig = plt.figure(figsize = (20,10))
    ax1 = fig.add_subplot(1,2,1)
    ax2 = fig.add_subplot(1,2,2)
    for i in range(num):
        path = sys.argv[i+1]
        config = utils.get_config(path + "/ParsedInput.txt")
        xmin = float(config['diagPh.SetBounds_0'])*180./3.14159
        xmax = float(config['diagPh.SetBounds_1'])*180./3.14159
        nx = int(config['diagPh.SetMatrixSize_0'])
        n = int(config['MatrixSize_X'])
        dx = (xmax-xmin)/nx
        axis = utils.create_axis(nx,dx,xmin)
        value = utils.bo_file_load(path + bopath,iteration,nx)
        ax1.plot(axis,value)
        ax2.plot(axis,value, label = 'n = %d, number of electrons = %.2le' %(n,sum(value)) )
        ax1.set_xlim([0,1])
    plt.legend(loc='upper right')
    picname = './pics/diagph_%d'%iteration
    plt.savefig(picname)


if __name__ == '__main__':
    main()
