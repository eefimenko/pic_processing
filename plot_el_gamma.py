#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import matplotlib as mp
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from matplotlib import cm
import dw

def read_file(filename):
    a = []
    f = open(filename, 'r')
    for line in f:
        a.append(float(line))
    return a

def main():
    n = len(sys.argv) - 1
    mp.rcParams.update({'font.size': 16})
    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(1,2,1)
    ax1.set_xlim([0, 2])
    ax1.set_xlabel('$r/\lambda$')
    ax1.set_ylabel('$dN/dr$')
    ax2 = fig.add_subplot(1,2,2)
    ax2.set_yscale('log')
    ax2.set_ylim([1e-6, 3e-3])
    ax2.set_xlim([0, 4000])
    ax2.set_xlabel('$\gamma$')
    ax2.set_ylabel('$dN/d\gamma$')

    fig1 = plt.figure(num=None)
    ax11 = fig1.add_subplot(1,2,1)
    ax11.set_yscale('log')
    ax11.set_ylim([1e-6, 3e-3])
    ax11.set_xlim([0, 4000])
    ax11.set_xlabel('$\gamma$')
    ax11.set_ylabel('$dN/d\gamma$')
    ax21 = fig1.add_subplot(1,2,2)
    ax21.set_yscale('log')
    ax21.set_ylim([1e-6, 3e-3])
    ax21.set_xlim([0, 4000])
    ax21.set_xlabel('$\gamma$')
    ax21.set_ylabel('$dN/d\gamma$')
    power = np.zeros(n)
    fig3 = plt.figure()
    ax3 = fig3.gca(projection='3d')
    nmin = []
    nmax = []

    rsurf = []
    gsurf = []
    labels = []
    handles = []
    for i in range(n):
        path = sys.argv[i+1]
        config = utils.get_config(path + "/ParsedInput.txt")
        pwr = float(config['PeakPowerPW'])
        power[i] = pwr
        rmax = float(config['ElGammaR.SetBounds_1'])
        nr = int(config['ElGammaR.SetMatrixSize_0'])
        wl = float(config['Wavelength'])
        dr = rmax/nr/wl
        axr = utils.create_axis(nr, dr)
        gmax = float(config['ElGammaR.SetBounds_3'])
        ng = int(config['ElGammaR.SetMatrixSize_1'])
        dg = gmax/ng
        axg = utils.create_axis(ng, dg)
        el_r = read_file(path + "/el_r.txt")
        
        el_g = read_file(path + "/el_g.txt")
       
        el_g_min = read_file(path + "/el_g_min.txt")
        el_g_max = read_file(path + "/el_g_max.txt")
        n = sum(el_g)
        for k in range(len(el_g)):
            el_g[k] /= n*dg
        n = sum(el_g_min)
        nmin.append(n)
        for k in range(len(el_g_min)):
            el_g_min[k] /= n*dg
        n = sum(el_g_max)
        nmax.append(n)
        for k in range(len(el_g_max)):
            el_g_max[k] /= n*dg
        n = sum(el_r)
        for k in range(len(el_r)):
            el_r[k] /= n*dr
        rsurf.append(el_r)
        gsurf.append(el_g)
        print len(axr), len(el_r)
        h, = ax1.plot(axr, el_r, label = pwr)
        handles.append(h)
        labels.append(str(pwr) + ' PW')
        ax2.plot(axg, el_g, label = pwr)
        ax11.plot(axg, el_g_min, label = pwr)
        ax21.plot(axg, el_g_max, label = pwr)
        ax3.plot(axr, el_r, zs=-pwr, zdir='z')
        ax3.view_init(elev=20., azim=-35)
    ez = []
    by = []
    ax_f = ax1.twinx()
    lambda_ = 0.9
    k = 2*math.pi/lambda_
    
    for x in axr:
        ez.append(abs(dw.ezf(k,x*lambda_,0,0)))
        by.append(abs(dw.byf(k,x*lambda_,0,0)))
    m = max(ez)
    for i in range(len(ez)):
        ez[i] /= m
        by[i] /= m
        
    e, = ax_f.plot(axr, ez, 'k')
    b, = ax_f.plot(axr, by, 'gray')
    mm, = ax_f.plot([0.44, 0.44], [0,10], 'k--')
    ax2.legend(handles, labels)
    handles.append(e)
    labels.append('$|E|$')
    handles.append(b)
    labels.append('$|B|$')
    handles.append(mm)
    labels.append('$r_{E=0}$')
    ax_f.set_xlim([0,1.5])
    ax_f.set_ylim([0,10])
    xticks = [0, 0.2, 0.44, 0.6, 0.8, 1., 1.2, 1.4]
    ax_f.set_xticks(xticks)
    ax_f.set_xticklabels(xticks)
    ax1.legend(handles, labels)
    fig1 = plt.figure(num=None)
    ax11 = fig1.add_subplot(1,1,1)
    ax11.plot(power, nmin, 'b-o', label = '$r < 0.44 \lambda$')
    ax11.plot(power, nmax, 'g-v', label = '$r > 0.44 \lambda$')
    ax11.set_xlabel('Power, PW')
    ax11.set_ylabel('N')
    plt.legend(loc='upper left')
    plt.show()
    fig1 = plt.figure(num=None)
#    ax11 = fig1.gca(projection='3d')
    ax11 = fig1.add_subplot(1,2,1)
#    ax11.imshow(rsurf, aspect='auto', origin='lower')
    Z = np.array(rsurf)
    X, Y = np.meshgrid(axr, power)
#    print X.shape, Y.shape, Z.shape 
#    ax11.plot_surface(X, Y, Z, cmap=cm.hot_r, linewidth=0, antialiased=False)
    ax11.pcolormesh(X,Y,Z)
    ax11.set_xlim([0,2])
    ax12 = fig1.add_subplot(1,2,2)
    ax12.imshow(rsurf, aspect='auto', origin='lower')
    plt.show()
    f = open('nmin_nmax.txt', 'w')
    for i in range(len(power)):
        f.write('%lf %le %le\n' % (power[i], nmin[i], nmax[i]))
    f.close()
   

if __name__ == '__main__':
    main()
