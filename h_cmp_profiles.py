#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])

def norm(array):
    m = max(array)
    a = [0]*len(array)
    for i in range(len(array)):
        a[i] = array[i]/m
    return a

def main():
    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nixpath = 'data/Proton2Dx'
    niypath = 'data/Proton2Dy'
    nizpath = 'data/Proton2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nphxpath = 'data/Photon2Dx'
    nphypath = 'data/Photon2Dy'
    nphzpath = 'data/Photon2Dz'
    picspath = 'pics'
    config = utils.get_config("ParsedInput.txt")
    wl = float(config['Wavelength'])
    Xmax = float(config['X_Max'])/wl #mkm to wavelength
    Xmin = float(config['X_Min'])/wl #mkm to wavelength
    Ymax = float(config['Y_Max'])/wl #mkm to wavelength
    Ymin = float(config['Y_Min'])/wl #mkm to wavelength

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    delta = 1

    num = len(sys.argv)
    step = float(config['TimeStep'])*1e15*float(config['BOIterationPass'])
    omega = float(config['Omega'])
    T = 2 * utils.pi/omega
    nt = int(T*1e15/step)
    print nt
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(expath) - nt
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(expath) - nt
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'

    dv = 2.*dx*dy*dz
    axis1 = create_axis(nx, (Xmax-Xmin)/nx, Xmin)
    i = int(sys.argv[1])
    j = i+((int(sys.argv[2])-i)/nt + 1) * nt
    print i, j, int(sys.argv[2]), nt, int(sys.argv[2])/nt
    fig = plt.figure(num=None, figsize=(20, 10), dpi=256)
    picname = picspath + '/' + "h_cmp_pr_%d.png" % (i,)
    print picname
    
#        name = bxpath + '/' + "%06d.txt" % (i,)
#        fieldb = read_field(name,nx,ny)
#        name = expath + '/' + "%06d.txt" % (i,)
#        fielde = read_field(name,nx,ny)
    name = nexpath + '/' + "%06d.txt" % (i,)
    fieldn = read_field(name,nx,ny, 1./dv)
    name = npxpath + '/' + "%06d.txt" % (i,)
    fieldp = read_field(name,nx,ny, 1./dv)
    name = nphxpath + '/' + "%06d.txt" % (i,)
    fieldph = read_field(name,nx,ny, 1./dv)
    name = nexpath + '/' + "%06d.txt" % (j,)
    fieldn1 = read_field(name,nx,ny, 1./dv)
    name = npxpath + '/' + "%06d.txt" % (j,)
    fieldp1 = read_field(name,nx,ny, 1./dv)
    name = nphxpath + '/' + "%06d.txt" % (j,)
    fieldph1 = read_field(name,nx,ny, 1./dv)

    profn = norm(fieldn[:][ny/2])
    profp = norm(fieldp[:][ny/2])
    profph = norm(fieldph[:][ny/2])
    profn1 = norm(fieldn1[:][ny/2])
    profp1 = norm(fieldp1[:][ny/2])
    profph1 = norm(fieldph1[:][ny/2])

#        for k in range(len(fieldn)):
#            profn[k] /= dv
#            profp[k] /= dv

#        profe = fielde[:][ny/2]
#        profb = fieldb[:][ny/2]
    ax1 = fig.add_subplot(3,2,1)
    ax1.plot(axis1,profn, label = 'el') 
    ax1.plot(axis1,profn1, label = 'el + T') 
    ax2 = fig.add_subplot(3,2,3)
    ax2.plot(axis1,profp, label = 'pos')
    ax2.plot(axis1,profp1, label = 'pos + T')
    ax3 = fig.add_subplot(3,2,5)
    ax3.plot(axis1,profph, label = 'ph')
    ax3.plot(axis1,profph1, label = 'ph + T')
#        ax2 = ax1.twinx()
#        ax2.plot(axis1,profe, 'r', label = 'E')
#        ax2.plot(axis1,profb, 'b', label = 'B')
        
    ax1.set_xlabel('z/lambda')
    ax1.legend(loc='upper left', shadow=True)
    ax2.set_xlabel('z/lambda')
    ax2.legend(loc='upper left', shadow=True)
    ax3.set_xlabel('z/lambda')
    ax3.legend(loc='upper left', shadow=True)
#        ax2.legend(loc='upper right', shadow=True)
#        plt.figtext(0.75,0.93,str('Ne = %.1e cm^-3 E = %.1e B = %.1e' % (max2d(fieldn),max2d(fielde),max2d(fieldb))) , fontsize = 12)
        
#        name = bypath + '/' + "%06d.txt" % (i,)
#        fieldb = read_field(name,nx,ny)
#        name = eypath + '/' + "%06d.txt" % (i,)
#        fielde = read_field(name,nx,ny)
#        name = neypath + '/' + "%06d.txt" % (i,)
#        fieldn = read_field(name,nx,ny)
        
     
#        name = bzpath + '/' + "%06d.txt" % (i,)
#        fieldb = read_field(name,nx,ny)
#        name = ezpath + '/' + "%06d.txt" % (i,)
#        fielde = read_field(name,nx,ny)
    name = nezpath + '/' + "%06d.txt" % (i,)
    fieldn = read_field(name,nx,ny, 1./dv)
    name = npzpath + '/' + "%06d.txt" % (i,)
    fieldp = read_field(name,nx,ny, 1./dv)
    name = nphzpath + '/' + "%06d.txt" % (i,)
    fieldph = read_field(name,nx,ny, 1./dv)
    name = nezpath + '/' + "%06d.txt" % (j,)
    fieldn1 = read_field(name,nx,ny, 1./dv)
    name = npzpath + '/' + "%06d.txt" % (j,)
    fieldp1 = read_field(name,nx,ny, 1./dv)
    name = nphzpath + '/' + "%06d.txt" % (j,)
    fieldph1 = read_field(name,nx,ny, 1./dv)
     
    profn = norm(fieldn[:][ny/2])
    profp = norm(fieldp[:][ny/2])
    profph = norm(fieldph[:][ny/2])
    profn1 = norm(fieldn1[:][ny/2])
    profp1 = norm(fieldp1[:][ny/2])
    profph1 = norm(fieldph1[:][ny/2])
#        for k in range(len(fieldn)):
#            profn[k] /= dv
#            profp[k] /= dv

#        profe = fielde[:][ny/2]
#        profb = fieldb[:][ny/2]
    ax1 = fig.add_subplot(3,2,2)
    ax1.plot(axis1,profn, label = 'el')
    ax1.plot(axis1,profn1, label = 'el + T')
    ax2 = fig.add_subplot(3,2,4)
    ax2.plot(axis1,profp, label = 'pos')
    ax2.plot(axis1,profp1, label = 'pos + T')
    ax3 = fig.add_subplot(3,2,6)
    ax3.plot(axis1,profph, label = 'ph')
    ax3.plot(axis1,profph1, label = 'ph + T')
    f1 = open('el_%d.txt' %(i,), 'w')
    f2 = open('elT_%d.txt' %(i,), 'w')
    f3 = open('pos_%d.txt' %(i,), 'w')
    f4 = open('posT_%d.txt' %(i,), 'w')
    f5 = open('ph_%d.txt' %(i,), 'w')
    f6 = open('phT_%d.txt' %(i,), 'w')
    for i in range(len(profn)):
        f1.write(str(profn[i])+'\n')
        f2.write(str(profn1[i])+'\n')
        f3.write(str(profp[i])+'\n')
        f4.write(str(profp1[i])+'\n')
        f5.write(str(profph[i])+'\n')
        f6.write(str(profph1[i])+'\n')
    f1.close()
    f2.close()
    f3.close()
    f4.close()
    f5.close()
    f6.close()
            
#        ax2 = ax1.twinx()
#        ax2.plot(axis1,profe, 'r', label = 'E')
#        ax2.plot(axis1,profb, 'b', label = 'B')
        
       
#        ax2.legend(loc='upper right', shadow=True)

#        ax1 = fig.add_subplot(1,3,3)
#        ax1.plot(axis1,profn,'g', label = 'el')
#        ax1.plot(axis1,profp,'k', label = 'pos')
#        ax2 = ax1.twinx()
#        ax2.plot(axis1,profe, 'r', label = 'E')
#        ax2.plot(axis1,profb, 'b', label = 'B')
        
    ax1.set_xlabel('y/lambda')
    ax1.legend(loc='upper left', shadow=True)
    ax2.set_xlabel('y/lambda')
    ax2.legend(loc='upper left', shadow=True)
    ax3.set_xlabel('y/lambda')
    ax3.legend(loc='upper left', shadow=True)
#        ax2.legend(loc='upper right', shadow=True)
        
    plt.savefig(picname)
    plt.close()
#        plt.show()
    
if __name__ == '__main__':
    main()
