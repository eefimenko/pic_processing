#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def main():
    n = len(sys.argv) - 2
    if n < 1:
        print "Too less parameters"
        print "Usage: <dirs> <name of the summary file>"
        exit(-1)
    boundary = float(sys.argv[-1])
     
    ds = 1e6
    ds1 = 2 * math.pi * (1 - math.cos(0.05))*1e6
    ds2 = 2 * math.pi * (1 - math.cos(0.23))*1e6
    ds3 = 2 * math.pi * (1 - math.cos(0.42))*1e6
    sfile = 'summary_%.2f.txt' % (boundary)
    f00 = open('brill_full.tex', 'w')
    f00.write("\\begin{table}") 
    f00.write("\\begin{tabular}{l | c | c | c | c | c | c | c | c | r }\n")
    f00.write("\\hline\n")
    f00.write("P, PW & $T_p$,fs & $N_0, cm^{-3}$ & Type & Space & Phase & Flux & Emittance & $N_{ph}$, $\\times 10^9$ & Efficiency \\\\ \n")
    f00.write("\\hline\n")
    f01 = open('brill_0.1.tex', 'w')
    f01.write("\\begin{table}") 
    f01.write("\\begin{tabular}{l | c | c | c | c | c | c | c | c | r }\n")
    f01.write("\\hline\n")
    f01.write("P, PW & $T_p$,fs & $N_0, cm^{-3}$ & Type & Space & Phase & Flux & Emittance & $N_{ph}$, $\\times 10^9$ & Efficiency \\\\ \n")
    f01.write("\\hline\n")
    f = open('brill_%.2f.tex' % (boundary), 'w')
    f.write("\\begin{table}") 
    f.write("\\begin{tabular}{l | c | c | c | c | c | c | c | c | r }\n")
    f.write("\\hline\n")
    f.write("P, PW & $T_p$,fs & $N_0, cm^{-3}$ & Type & Space & Phase & Flux & Emittance & $N_{ph}$, $\\times 10^9$ & Efficiency \\\\ \n")
    f.write("\\hline\n")
    
    for i in range(n):
        path = sys.argv[i+1]
        if 'shifted' in path:
            space = '+'
            phase = '+'
            wtype = '12 beams'
        elif 'distorted' in path:
            space = ''
            phase = '+'
            wtype = '12 beams'
        else:
            space = ''
            phase = ''
            wtype = 'Ideal'
            
        config = utils.get_config(path + "/ParsedInput.txt")
        pwr = float(config['PeakPowerPW'])
        dur = float(config['Duration'])
        wl = float(config['Wavelength'])
        ne = float(config['Ne'])
        S = 2 * math.pi * 0.1 * 0.1 * wl * wl * 100 # square of source mm^2, r = 0.1 lambda
        s = open(path + '/' + sfile, 'r')
        pulse_energy = float(s.readline()) 
        sen_full = float(s.readline())
        sen_100mev = float(s.readline())
        sen_1gev = float(s.readline())
        max_br_full = float(s.readline())
        max_br_100mev = float(s.readline())
        max_br_1Gev = float(s.readline())
        max_br_2gev_line = float(s.readline())
        sum_br_full = float(s.readline())
        sum_br_100mev = float(s.readline())
        sum_br_1Gev = float(s.readline())
        sum_br_2gev_line = float(s.readline())
        sum_en = sen_full
        f00.write("%d & %.2lg & %.2lg & %s & %s & %s & %.2lg & %.2lg & %.2lg & %.2lg \\\\ \n" % (pwr, dur*1e15, ne, wtype, space, phase, max_br_full, max_br_full/S/ds3, sum_br_full*1e-9, sen_full/pulse_energy))
        f01.write("%d & %.2lg & %.2lg & %s & %s & %s & %.2lg & %.2lg & %.2lg & %.2lg \\\\ \n" % (pwr, dur*1e15, ne, wtype, space, phase, max_br_100mev, max_br_100mev/S/ds2, sum_br_100mev*1e-9, sen_100mev/pulse_energy))
        f.write("%d & %.2lg & %.2lg & %s & %s & %s & %.2lg & %.2lg & %.2lg & %.2lg \\\\ \n" % (pwr, dur*1e15, ne, wtype, space, phase, max_br_1Gev, max_br_1Gev/S/ds1, sum_br_1Gev*1e-9, sen_1gev/pulse_energy))
        s.close()
        print 'Full energy = ', sen_full, 'full_energy(check) = ', sen_full/sum_en, ' 100 mev energy = ', sen_100mev/sum_en, '%.0f Gev energy = '%(boundary), sen_1gev/sum_en
        print 'Efficiency: full = ', sen_full/pulse_energy, '100 mev = ', sen_100mev/pulse_energy, ' %.0f Gev = '%(boundary), sen_1gev/pulse_energy
    
        print 'Emittance:  full = ', max_br_full/S/ds3, ' 100mev= ', max_br_100mev/S/ds2, ' %.0f Gev = '%(boundary), max_br_1Gev/S/ds1
        print 'Flux:  full = ', max_br_full, ' 100mev= ', max_br_100mev, ' %.0f Gev = '%(boundary), max_br_1Gev
        print 'Emittance line: %.0f Gev = '%(boundary), max_br_2gev_line/S/ds1
        print 'Max: %.0f Gev = ' % (boundary), max_br_2gev_line, max_br_1Gev
        print 'Number:  full = ', sum_br_full, ' 100mev= ', sum_br_100mev, ' %.0f Gev = %.2f 10^9 '%(boundary, sum_br_1Gev*1e-9)
    f00.write("\\hline\n")
    f00.write("\\end{tabular}\n")
    f00.write("\\caption{Data for all photons}")
    f00.write("\\label{full}\n")
    f00.write("\\end{table}")
    f01.write("\\hline\n")
    f01.write("\\end{tabular}\n")
    f01.write("\\caption{Data for 100 meV photons}")
    f01.write("\\label{0.1}\n")
    f01.write("\\end{table}")
    f.write("\\hline\n")
    f.write("\\end{tabular}\n")
    f.write("\\caption{Data for %.2f GeV photons}" %(boundary))
    f.write("\\label{%.2f}\n"%(boundary))
    f.write("\\end{table}")
    
    
    f.close()

if __name__ == '__main__':
    main()
