#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def main():
    low_current = [6., 10., 15., 25., 27., 34.,44.,53.]
    high_current = [7., 12., 18., 33., 38., 44.,55.,68.]
    power = [10., 12., 15., 17., 22., 25.,30.,35.]
    bennett = []

    n = len(power)
    
    for i in range(n):
        bennett.append(1.7e4*2500.*math.sqrt(power[i]/10)*1e-6*0.5)
    
    fig, ax1 = plt.subplots()
    p, = ax1.plot(power, low_current, 'g')
    p, = ax1.plot(power, high_current, 'b')
    p, = ax1.plot(power, bennett, 'r')
    plt.show()

if __name__ == '__main__':
    main()
