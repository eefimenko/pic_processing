#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_gamma(filename):
    f = open(filename, 'r')
    tmp = f.readline().split()
    f.close()
    return float(tmp[0]), float(tmp[1]), float(tmp[2])

def main():
    savepath = './pics'
    if len(sys.argv) == 2:
        savepath = sys.argv[1]
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
    el = []
    ph = []
    pos = []
    ne = []
    types = ['edipole']
    types_labels = [u'е-волна']
    dirs = ['nc_0.001', 'nc_0.01', 'nc_0.1', 'nc_1', 'nc_10','nc_30', 'nc_50', 'nc_100', 'nc_1000']
    updirs = ['1mkm', '2mkm', '3mkm']
    updir_labels = [u'2 мкм', u'4 мкм', u'6 мкм']
    dashes = [[3,2], [], [5,2,1,2]]
    fig = plt.figure(figsize = (8,5))
    ax1 = fig.add_subplot(1,1,1)
#    fig2 =  plt.figure(figsize = (10,5))
#    ax2 = fig2.add_subplot(1,1,1)
#    ax3 = fig.add_subplot(2,2,3)
#    ax4 = fig.add_subplot(2,2,4)
    m = 0
    for type_ in types:
        for i,ud in enumerate(updirs):
            el = []
            ph = []
            pos = []
            el_1gev = []
            ph_1gev = []
            pos_1gev = []
            ne = []
            number = []
            for d in dirs:
                ph_, el_, pos_ = read_gamma(type_ + '/' + ud + '/' + d + '/efficiency.txt')
                el.append(el_)
                ph.append(ph_)
                pos.append(pos_)
                ph_, el_, pos_ = read_gamma(type_ + '/' + ud + '/' + d + '/efficiency_1gev.txt')
                el_1gev.append(el_)
                ph_1gev.append(ph_)
                pos_1gev.append(pos_)
                config = utils.get_config(type_ + '/' + ud + '/' + d + '/ParsedInput.txt')
                n_cr = float(config['n_cr'])
                ne_ = float(config['Ne'])
                r = float(config['R'])
                ne.append(ne_/n_cr)
                number.append(4./3.* math.pi * r**3 * ne_)
            ax1.plot(ne, ph, 'r', dashes = dashes[i], label = '$\hbar\omega$ ' + updir_labels[i])
            ax1.plot(ne, el, 'g', dashes = dashes[i], label = '$e^-$ ' + updir_labels[i])
            ax1.plot(ne, pos, 'b', dashes = dashes[i], label = '$e^+$ ' + updir_labels[i])
#            ax2.plot(number, ph, 'r', dashes = dashes[m])
#            ax2.plot(number, el, 'g', dashes = dashes[m])
#            ax2.plot(number, pos, 'b', dashes = dashes[m])
#            ax2.plot(ne, ph_1gev, 'r', dashes = dashes[m])
#            ax2.plot(ne, el_1gev, 'g', dashes = dashes[m])
#            ax2.plot(ne, pos_1gev, 'b', dashes = dashes[m])
#            ax4.plot(number, ph_1gev, 'r', dashes = dashes[m])
#            ax4.plot(number, el_1gev, 'g', dashes = dashes[m])
#            ax4.plot(number, pos_1gev, 'b', dashes = dashes[m])
        m += 1
#    ax1.set_yscale('log')
    ax1.set_xscale('log')
    ax1.set_yscale('log')
    ax1.set_ylim([1e-4, 1]) 
    ax1.set_xlabel(u'$n_0/n_{cr}$')
    ax1.set_ylabel(u'Эффективность конверсии')
    plt.legend(loc = 'lower left', frameon = False, ncol = 3)
    plt.tight_layout()
#    ax2.set_xscale('log')
#    ax2.set_yscale('log')
#    ax2.set_xlabel(u'$n_0,$см$^{-3}$')
#    ax2.set_ylim([1e-3, 1])
#    ax3.set_xscale('log')
#    ax3.set_yscale('log')
#    ax3.set_ylim([1e-3, 1])
#    ax4.set_xscale('log')
#    ax4.set_yscale('log')
#    ax4.set_ylim([1e-3, 1])
    plt.tight_layout()
    picname = savepath + '/coversion_ideal.png'
    print picname
    plt.savefig(picname)
    plt.show()

if __name__ == '__main__':
    main()

    
