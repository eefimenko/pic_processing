#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def create_subplot(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, xl, yl, color, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
    name = path + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny, mult)
    prof1 = field[len(field)/2]
    prof2 = []
    for n in range(len(field)):
        prof2.append(field[n][len(field[0])/2])
    maxe = max([max(row) for row in field])
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, norm=clr.LogNorm())
    ax.text(0.3, 1.1, 'max=%.1le' % (maxe), style='italic', horizontalalignment='center', verticalalignment='center', transform = ax.transAxes)
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
    plt.colorbar(surf,  orientation  = 'vertical')
    return prof1, prof2

def create_subplot_prof(fig,i,spx,spy,spi,p1, xl, yl, vmin, dirs, log = 0):
    ax = fig.add_subplot(spx,spy,spi)
    j = 0
    vmax = 0
    for k in range(len(p1)):
        print i,j
        surf = ax.plot(p1[k], get_color(j), label = dirs[k])
        j += 1
        tmp = max(p1[k])
        if tmp > vmax:
            vmax = tmp
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
    ax.set_xlim([0, len(p1[0])])
    ax.set_ylim([vmin, vmax])
    if log == 1:
        ax.set_yscale('log')
    ax.legend(loc='lower left')
    

def find_nmax(dirs, expath):
    nmax = utils.num_files(dirs[0] + expath)
    for k in range(1,len(dirs)):
        tmp = utils.num_files(dirs[k] + expath)
        if tmp < nmax:
            nmax = tmp
    return nmax

def get_color(j):
    color = ['r', 'g', 'b', 'k', 'c', 'y', 'G', 'B']
    if (j >= len(color)):
        j = j - len(color)
    return color[j]

def main():
    expath = '/data/E2x'
    eypath = '/data/E2y'
    ezpath = '/data/E2z'
    bxpath = '/data/B2x'
    bypath = '/data/B2y'
    bzpath = '/data/B2z'
    nexpath = '/data/Electron2Dx'
    neypath = '/data/Electron2Dy'
    nezpath = '/data/Electron2Dz'
    npxpath = '/data/Positron2Dx'
    npypath = '/data/Positron2Dy'
    npzpath = '/data/Positron2Dz'
    nixpath = '/data/Proton2Dx'
    niypath = '/data/Proton2Dy'
    nizpath = '/data/Proton2Dz'
    npxpath = '/data/Positron2Dx'
    npypath = '/data/Positron2Dy'
    npzpath = '/data/Positron2Dz'
    nphxpath = '/data/Photon2Dx'
    nphypath = '/data/Photon2Dy'
    nphzpath = '/data/Photon2Dz'
    picspath = 'pics'

    numdirs = int(sys.argv[1])
    dirs = []
    configs = []
    for i in range(numdirs):
        dirs.append(sys.argv[2+i])
        configs.append(utils.get_config(sys.argv[2+i] + "/ParsedInput.txt"))
    delta = 1
    n = numdirs + 1
    num = len(sys.argv) - n
    
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = find_nmax(dirs, expath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1 + n] 
        nmin = int(sys.argv[1 + n])
        nmax = find_nmax(dirs, expath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1 + n] + ' to ' + sys.argv[2 + n]
        nmin = int(sys.argv[1 + n])
        nmax = int(sys.argv[2 + n])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1 + n] + ' to ' + sys.argv[2 + n] + ' with delta ' + sys.argv[3 + n]
        print sys.argv[1 + n]
        nmin = int(sys.argv[1 + n])
        nmax = int(sys.argv[2 + n])
        delta = int(sys.argv[3 + n])
    else:
        nmin = 0
        nmax = 0
        print 'Too much parameters'

    print nmin, nmax, delta
    
    xmin = []
    xmax = []
    ymin = []
    ymax = []
    zmin = []
    zmax = []
    mult = []
    nx = []
    ny = []
    nz = []
    for k in range(numdirs):
        xmax.append(float(configs[k]['X_Max'])*1e4) #mkm
        xmin.append(float(configs[k]['X_Min'])*1e4) #mkm
        ymax.append(float(configs[k]['Y_Max'])*1e4) #mkm
        ymin.append(float(configs[k]['Y_Min'])*1e4) #mkm
        zmax.append(float(configs[k]['Z_Max'])*1e4) #mkm
        zmin.append(float(configs[k]['Z_Min'])*1e4) #mkm
        nx.append(int(configs[k]['MatrixSize_X']))
        ny.append(int(configs[k]['MatrixSize_Y']))
        nz.append(int(configs[k]['MatrixSize_Z']))
        dx = float(configs[k]['Step_X'])*1e4
        dy = float(configs[k]['Step_Y'])*1e4
        dz = float(configs[k]['Step_Z'])*1e4
        mult.append(1/(2.*dx*dy*dz*1e-12))
        step = float(configs[k]['TimeStep'])*1e15*float(configs[k]['BOIterationPass'])
        omega = float(configs[k]['Omega'])
        T = 2 * utils.pi/omega
        nt = int(T*1e15/step)
        print nt
    spx = numdirs+2
    spy = 6
    for i in range(nmin,nmax,delta):
        print "\rSaving sc%06d.png, %.0f %% done" % (i,float(i-nmin)/(nmax-nmin)*100),
        sys.stdout.flush()

        fig = plt.figure(num=None, figsize=(20., 20.*spx/spy), dpi=256)
        mp.rcParams.update({'font.size': 8})
        ex1 = []
        ex2 = []
        bx1 = []
        bx2 = []
        el1 = []
        el2 = []
        pos1 = []
        pos2 = []
        pr1 = []
        pr2 = []
        ph1 = []
        ph2 = []
        for k in range(numdirs):
            
            p1, p2 = create_subplot(fig,i,dirs[k]+expath,nx[k],ny[k],spx,spy,1+k*spy, xmin[k], xmax[k], ymin[k], ymax[k], 'z', 'y', 'Reds')
            ex1.append(p1)
            ex2.append(p2)
            p1, p2 = create_subplot(fig,i,dirs[k]+bxpath,nx[k],ny[k],spx,spy,2+k*spy, xmin[k], xmax[k], ymin[k], ymax[k], 'z', 'y','Reds')
            bx1.append(p1)
            bx2.append(p2)
            p1, p2 = create_subplot(fig,i,dirs[k]+nexpath,nx[k],ny[k],spx,spy,3+k*spy, xmin[k], xmax[k], ymin[k], ymax[k], 'z', 'y','Greens', mult[k])
            el1.append(p1)
            el2.append(p2)
            p1, p2 = create_subplot(fig,i,dirs[k]+nixpath,nx[k],ny[k],spx,spy,4+k*spy, xmin[k], xmax[k], ymin[k], ymax[k], 'z', 'y','Greens', mult[k])
            pr1.append(p1)
            pr2.append(p2)
            p1, p2 = create_subplot(fig,i,dirs[k]+npxpath,nx[k],ny[k],spx,spy,5+k*spy, xmin[k], xmax[k], ymin[k], ymax[k], 'z', 'y','Greens', mult[k])
            pos1.append(p1)
            pos2.append(p2)
            p1, p2 = create_subplot(fig,i,dirs[k]+nphxpath,nx[k],ny[k],spx,spy,6+k*spy, xmin[k], xmax[k], ymin[k], ymax[k], 'z', 'y', 'Blues', mult[k])
            ph1.append(p1)
            ph2.append(p2)
        create_subplot_prof(fig,i,spx,spy,numdirs*spy + 1,ex1, 'z', 'Electric field', 0, dirs)
        create_subplot_prof(fig,i,spx,spy,numdirs*spy + 1 + spy,ex2, 'y', 'Electric field', 0, dirs)
        create_subplot_prof(fig,i,spx,spy,numdirs*spy + 2,bx1, 'z', 'Magnetic field', 0, dirs)
        create_subplot_prof(fig,i,spx,spy,numdirs*spy + 2 + spy,bx2, 'y', 'Magnetic field', 0, dirs)
        create_subplot_prof(fig,i,spx,spy,numdirs*spy + 3,el1, 'z', 'El', 1e15, dirs, 1)
        create_subplot_prof(fig,i,spx,spy,numdirs*spy + 3 + spy,el2, 'y', 'El', 1e15, dirs, 1)
        create_subplot_prof(fig,i,spx,spy,numdirs*spy + 4,pr1, 'z', 'Proton', 1e15, dirs, 1)
        create_subplot_prof(fig,i,spx,spy,numdirs*spy + 4 + spy,pr2, 'y', 'Proton', 1e15, dirs, 1)
        create_subplot_prof(fig,i,spx,spy,numdirs*spy + 5,pos1, 'z', 'Positron', 1e15, dirs, 1)
        create_subplot_prof(fig,i,spx,spy,numdirs*spy + 5 + spy,pos2, 'y', 'Positron', 1e15, dirs, 1)
        create_subplot_prof(fig,i,spx,spy,numdirs*spy + 6,ph1, 'z', 'Photon', 1e15, dirs, 1)
        create_subplot_prof(fig,i,spx,spy,numdirs*spy + 6 + spy,ph2, 'y', 'Photon', 1e15, dirs, 1)
        picname = picspath + '/' + "sc%06d.png" % (i,)
        
        plt.savefig(picname)
        plt.close()

    
if __name__ == '__main__':
    main()
