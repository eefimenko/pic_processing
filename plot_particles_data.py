#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def read_file(path):
    Wavelength = 0.9e-4
    f = open(path, 'r')
    axis = []
    ex = []
    ey = []
    ez = []
    bx = []
    by = []
    bz = []
    x = []
    y = []
    z = []
    for line in f:
        v = line.split()
        axis.append(float(v[0])*2.00138e-17/3e-15)
        ex.append(float(v[1]))
        ey.append(float(v[2]))
        ez.append(float(v[3]))
        bx.append(float(v[4]))
        by.append(float(v[5]))
        bz.append(float(v[6]))
        x.append(float(v[7])/Wavelength)
        y.append(float(v[8])/Wavelength)
        z.append(float(v[9])/Wavelength)
    return axis,ex,ey,ez,bx,by,bz,x,y,z

def main():
    path = 'ParticleTracking'
    picspath = 'pics'
    number_of_particles = 5 
    fig = plt.figure()
    
    ax1 = fig.add_subplot(2,2,1)
    ax1.set_title('ez')
    ax2 = fig.add_subplot(2,2,2)
    ax2.set_title('er')
    ax3 = fig.add_subplot(2,2,3)
    ax3.set_title('ep')
    ax4 = fig.add_subplot(2,2,4)
    ax4.set_title('phi')
    ax4.grid()
    for i in range(number_of_particles):
        axis,ex,ey,ez,bx,by,bz,x,y,z = read_file(path + '/Dummy%d/%d.txt'%(i+1,i+1))
        
        phi = np.zeros(len(x))
        er = np.zeros(len(x))
        ep = np.zeros(len(x))
#        er = np.zeros(len(x))
#        ep = np.zeros(len(x))
        for j in range(len(x)):
            if y[j] == 0:
                phi[j] = math.pi/2
            else:
                phi[j] = abs(math.atan(x[j]/y[j]))
            s = math.sin(phi[j])
            c = math.cos(phi[j])
            r = math.sqrt(x[j]*x[j] + y[j]*y[j])
            ep[j] = r*abs(-ex[j]*s + ey[j]*c)
            er[j] = abs(ex[j]*c + ey[j]*s)
#            phi[j] = math.sqrt(x[j]*x[j] + y[j]*y[j])
        ax1.plot(axis,ez)
        ax2.plot(axis,er)
        ax3.plot(axis,ep)
        ax4.plot(axis,phi, label = i)
#        ax4.plot([axis[0], axis[1500]], [phi[0], phi[0]])
#    plt.legend(loc = 'upper left')
    plt.show()

if __name__ == '__main__':
    main()
