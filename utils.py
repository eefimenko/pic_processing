import os, os.path
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
from scipy.interpolate import interp1d
from mpl_toolkits.axes_grid1 import make_axes_locatable
import math
from matplotlib import ticker
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
from tqdm import *
import zipfile
import re

LightVelocity = 29979245800.0
ElectronMass=0.91e-27
ElectronCharge = 4.8032e-10
expath = 'data/E2x'
eypath = 'data/E2y'
ezpath = 'data/E2z'
ex_xpath = 'data/Ex_x'
ex_ypath = 'data/Ex_y'
ex_zpath = 'data/Ex_z'
ey_xpath = 'data/Ey_x'
ey_ypath = 'data/Ey_y'
ey_zpath = 'data/Ey_z'
ez_xpath = 'data/Ez_x'
ez_ypath = 'data/Ez_y'
ez_zpath = 'data/Ez_z'
bxpath = 'data/B2x'
bypath = 'data/B2y'
bzpath = 'data/B2z'
bx_xpath = 'data/Bx_x'
bx_ypath = 'data/Bx_y'
bx_zpath = 'data/Bx_z'
by_xpath = 'data/By_x'
by_ypath = 'data/By_y'
by_zpath = 'data/By_z'
bz_xpath = 'data/Bz_x'
bz_ypath = 'data/Bz_y'
bz_zpath = 'data/Bz_z'
jxpath = 'data/J2x'
jypath = 'data/J2y'
jzpath = 'data/J2z'
jx_xpath = 'data/Jx_x'
jx_ypath = 'data/Jx_y'
jx_zpath = 'data/Jx_z'
jy_xpath = 'data/Jy_x'
jy_ypath = 'data/Jy_y'
jy_zpath = 'data/Jy_z'
jz_xpath = 'data/Jz_x'
jz_ypath = 'data/Jz_y'
jz_zpath = 'data/Jz_z'
nexpath = 'data/Electron2Dx'
neypath = 'data/Electron2Dy'
nezpath = 'data/Electron2Dz'
npxpath = 'data/Positron2Dx'
npypath = 'data/Positron2Dy'
npzpath = 'data/Positron2Dz'
nixpath = 'data/Proton2Dx'
niypath = 'data/Proton2Dy'
nizpath = 'data/Proton2Dz'
npxpath = 'data/Positron2Dx'
npypath = 'data/Positron2Dy'
npzpath = 'data/Positron2Dz'
nphxpath = 'data/Photon2Dx'
nphypath = 'data/Photon2Dy'
nphzpath = 'data/Photon2Dz'
ppath = 'data/ElPhase/'
mpath = 'data/PosPhase/'
p1path = 'data/ElMomentum/'
m1path = 'data/PosMomentum/'
dphpath = 'data/diagPh'
netpath = 'data/NeTrap/'
npostpath = 'data/NposTrap/'
elgammarpath = 'data/ElGammaR/'
nepath = 'data/ne'
nppath = 'data/npos'
enspsphpath = 'statdata/ph/EnSpSph/'
el_enspsphpath = 'statdata/el/EnSpSph/'
pos_enspsphpath = 'statdata/pos/EnSpSph/'
ensppath = 'statdata/ph/EnSp/'
el_ensppath = 'statdata/el/EnSp/'
pos_ensppath = 'statdata/pos/EnSp/'
angspsphpath = 'statdata/ph/angSpSph/'
enangsphpath = 'statdata/ph/EnAngSp/'
el_enangsphpath = 'statdata/el/EnAngSp/'
pos_enangsphpath = 'statdata/pos/EnAngSp/'
elenspsphpath = 'statdata/el/EnSpSph/'
el_angspsphpath = 'statdata/el/angSpSph/'
elenangsphpath = 'statdata/el/EnAngSp/'
pos_angspsphpath = 'statdata/pos/angSpSph/'

def make_patch_spines_invisible(ax):
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)
        
def get_config(file):
    searchfile = open(file, "r")
    myvars = {}
    for line in searchfile:
        if "Setting" in line and ("variable" in line or "string" in line) and '=' in line:
            tmp, var = line.partition("=")[::2]
            rem, name = tmp.partition(":")[::2]
            myvars[name.strip()] = var.strip()
    searchfile.close()
    return myvars

def num_files(dirname):
    num = len([name for name in os.listdir(dirname) if os.path.isfile(os.path.join(dirname,name))])
    return num

def find_min_max_from_directory(*dirnames):
    min_ = []
    max_ = []
    
    for dirname in dirnames:
        try:
            l = sorted([int(name.split('.')[0]) for name in os.listdir(dirname) if os.path.isfile(os.path.join(dirname,name))])
        except ValueError:
            print (dirname)
            print( os.listdir(dirname))
            exit(-1)
        min_.append(min(l))
        max_.append(max(l))
    return max(min_),min(max_)+1

def find_min_max_from_archive_directory(dirname, ftype, archive):
    files = []

    with zipfile.ZipFile(archive) as archivefile:
        content = archivefile.namelist()
        
        # Iterate over the file names
        for fileName in content:
        # Check filename endswith csv
            if fileName.startswith(dirname) and fileName.endswith(ftype):
                files.append(int(os.path.basename(fileName).split('.')[0]))
      
    return min(files),max(files)+1

def find_min_max_iteration_from_archive(*dirnames, **kwargs):
    ftype = kwargs.get("ftype", 'txt')
    archive = kwargs.get("archive", None)

    if archive is None:
        print('No archive, exiting')
        return -1, -1
    
    min_ = []
    max_ = []
    
    for dirname in dirnames:
        print(dirname)
        files = []
        with zipfile.ZipFile(archive) as archivefile:
            content = archivefile.namelist()
        
            # Iterate over the file names
            for fileName in content:
                # Check filename endswith csv
                if fileName.startswith(dirname) and fileName.endswith(ftype):
                    files.append(int(os.path.basename(fileName).split('.')[0]))
        min_.append(min(files))
        max_.append(max(files))
    return max(min_),min(max_)+1

def get_color(j):
    color = ['r', 'g', 'b', 'k', 'c', 'y', 'G', 'B']
    if (j >= len(color)):
        j = j - len(color)
    return color[j]

def create_axis(n,step,x0=0):
    axis = np.zeros(n)
    for i in range(n):
        axis[i] = x0 + i*step
    return axis

def axis(xmin,xmax,nx):
    dx = (xmax - xmin)/nx
    return create_axis(nx,dx,xmin)

def normalize(a):
    m = max(a)
    if m != 0:
        for i in range(len(a)):
            a[i] /= m 
    return a

def fromfile(file,nx=1,ny=1, ftype='txt', verbose = 0, dtype='f', archive = None):
    #print('fromfile:', archive)
    if archive is None:
        if 'txt' in ftype:
            sep = ' '
            f = open(file, 'r')
        else:
            sep = ''
            f = open(file, 'rb')
        array = np.fromfile(f, dtype=dtype, count=-1, sep=sep)
        #print(array.shape, array.dtype)
        #print(array)
    else: # archived files 
        with zipfile.ZipFile(archive) as archivefile:
            #print(archivefile.namelist())
            with archivefile.open(file, mode="r") as myfile:
                if 'txt' in ftype:
                    data = myfile.read()
                    array = np.fromstring(data, dtype=dtype, sep=' ')
                else:
                    data = myfile.read()
                    array = np.frombuffer(data, dtype=dtype, offset=0)
    if ny != 1 and array.size != 0:
        array = np.reshape(array, (nx,ny), order = 'F')
    return array

def bo_file_load(path,iteration,nx=1,ny=1,ftype='txt',fmt='%06d',transpose = 0, verbose = 0, archive = None):
    name = os.path.join(path, fmt % (iteration) + '.' + ftype)
    read = True
    
    if verbose != 0:
        print(name)
    if archive is None:
        if os.path.exists(name):
            field = fromfile(name,nx,ny,ftype, archive=archive)
            if transpose == 1:
                field = np.transpose(field)
        else:
            field = None
            read = False
    else:
        if os.path.exists(archive):
            #print('BOfileload:', archive)
            field = fromfile(name,nx,ny,ftype, archive=archive)
            if transpose == 1:
                field = np.transpose(field)
        else:
            field = None
            read = False
    
    return read, field

def get(path,iteration,nx=1,ny=1,ftype='txt',fmt='%06d', transpose = 0, verbose = 0, func = lambda x: x, default=0, archive = None):
    read, field = bo_file_load(path,iteration,nx,ny,ftype,fmt,transpose,verbose, archive = archive)
    if read:
        if field.size == 0:
            print('Warning: file withe 0 size')
            return 0
        else:
            return func(field)
    else:
        return default
    
def transpose(array):
    nx = len(array)
    ny = len(array[0])
    array1 = [[array[i][j] for i in xrange(nx)] for j in xrange(ny)]
    print(len(array), len(array1))
    print(len(array[0]), len(array1[0]))
    return array1

def max2d(array):
    return max([max(x) for x in array])

def min2d(array):
    return min([min(x) for x in array])

def absmax2d(array):
    m1 = max2d(array)
    m2 = min2d(array)
    if abs(m1) > abs(m2):
        return m1
    else:
        return m2

def ntrapped(path, i, particles = 'electrons', ftype = 'txt', verbose = False):
    if verbose:
        print(path)
    if particles == 'electrons':
        directory = netpath
    else:
        directory = npostpath
    read, n = bo_file_load(os.path.join(path, directory), i, ftype = ftype)
    return sum(n)

def get_min_max_iteration(argv,*path,**kwargs):
    n = kwargs.get('n', 0)
    archive = kwargs.get('archive', None)
    num = len(argv) - n
    delta = 1
    print(argv,path)
    if archive is None:
        nmin,nmax = find_min_max_from_directory(*path)
    else:
        nmin,nmax = find_min_max_iteration_from_archive(*path, archive=archive)
        
    if num == 1:
        print('Plotting all pics')
    elif num == 2:
        print('Plotting from pic ' + argv[1+n])
        nmin_ = int(argv[1+n])
        if nmin_ > nmin:
            nmin = nmin_
    elif num == 3:
        print('Plotting pics from ' + argv[1+n] + ' to ' + argv[2+n])
        nmin_ = int(argv[1+n])
        nmax_ = int(argv[2+n])
        if nmin_ > nmin:
            nmin = nmin_
        if nmax_ < nmax:
            nmax = nmax_
    elif num == 4:
        print('Plotting pics from ' + argv[1+n] + ' to ' + argv[2+n] + ' with delta ' + argv[3+n])
        nmin_ = int(argv[1+n])
        nmax_ = int(argv[2+n])
        if nmin_ > nmin:
            nmin = nmin_
        if nmax_ < nmax:
            nmax = nmax_
        delta = int(argv[3+n])
    else:
        print('To much parameters')

    return nmin,nmax,delta

def cvalue(array):
    s = array.shape
    return array[s[0]//2][s[1]//2]

def ts(rootpath,datapath,**options):
    path = os.path.join(rootpath, datapath)
    name = options.get('name')
    tstype = options.get('tstype','max')
    rewrite = options.get('rewrite', False)
    verbose = options.get('verbose', False)
    shape = options.get('shape', (1,1))
    func_ = options.get('func', np.amax)
    ftype = options.get('ftype', 'txt')
    archive = options.get('archive', None)
    
    if tstype == 'max':
        func = np.amax
    elif tstype == 'center':
        func = cvalue
        if shape == (1,1):
            print('utils.ts: shape is not set, error')
            exit(-1)
    elif tstype == 'func':
        func = func_
        
    elif tstype == 'sum':
        func = np.sum
    else:
        print('utils.ts: Unknown type, Fallback to max')
        func = np.amax
    filename = name + '_' + tstype + '_ts'
    print(archive)
    if archive is None:
        nmin,nmax = find_min_max_from_directory(path)
        read_path = path
    else:
        nmin,nmax = find_min_max_from_archive_directory(datapath, ftype, archive)
        read_path = datapath
    print(nmin, nmax)
    arr_t = np.zeros(nmax)

    nmin = read_tseries(rootpath, filename, arr_t, rewrite=rewrite, verbose=verbose, ftype=ftype)
    print(nmin)
                 
    for i in tqdm(range(nmin, nmax)):
         arr_t[i] = get(read_path,i,nx = shape[0], ny=shape[1], func=func, ftype = ftype, verbose = verbose, archive = archive)
    save_tseries(rootpath,filename, nmin, nmax, arr_t)
    return arr_t

def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

def read_tseries(path,name,*arrays,**options):
    rewrite = options.get('rewrite', False)
    verbose = options.get('verbose', False)
    ftype = 'txt'
    start = options.get('start', 0)
        
    if os.path.exists(os.path.join(path, name + '.' + ftype)) and rewrite == False:
        if verbose:
            print('Found ' + name)
        
        f = open(os.path.join(path, '%s.txt'%(name)), 'r')
       
        num = len(arrays)
        i = -1
        for line in f:
            values = line.split()
            i = int(values[0])
            for j in range(num):
                arrays[j][i] = float(values[j+1])
        f.close()
        return i + 1
    else:
        if verbose:
            print('Creating ' + name)
        return start

def save_tseries(path,name,nmin = 0,nmax = 0,*arrays):
    f = open(os.path.join(path, '%s.txt' % (name)), 'a')
    
    num = len(arrays)
    if nmax == 0:
        nmax = len(arrays[0])
    for i in range(nmin,nmax):
        f.write('%d'%(i))
        for j in range(num):
            f.write(' %le'%(arrays[j][i]))
        f.write('\n')
    f.close()

def avg(array, num):
    n = len(array)
    avg = np.zeros(n)
    h = num/2
    for i in range(0,h):
        avg[i] = np.sum(array[0:i+h])/(i+1+h)
    for i in range(h,n-h):
        avg[i] = np.sum(array[(i-h):(i+h)])/num
    for i in range(n-h,n):
        avg[i] = np.sum(array[i-h:n])/(n-i+h)
    return avg

def env(array, axis):
    res = np.zeros(array.shape)
    u_x = [axis[0],]
    u_y = [array[0],]
    max_set = 0
    nmax = 5
    for i in range(1,array.shape[0]-1):
        if max_set < nmax:
            max_set += 1
            continue
        if np.sign(array[i] - array[i-1]) == 1 and np.sign(array[i] - array[i+1]) == 1:
            u_x.append(axis[i])
            u_y.append(array[i])
            max_set = 0
    u_x.append(axis[-1])
    u_y.append(u_y[-1])
    f = interp1d(u_x, u_y, kind = 'linear', bounds_error = False, fill_value=0.0)
    for i in range(array.shape[0]):
        res[i] = f(axis[i])
    return res

def convert_xy_to_rphi(fx,fy,dx,dy):
    if not fx.shape == fy.shape:
        exit(-1)
    nx,ny = fx.shape
    nr = np.zeros(fx.shape)
    nphi = np.zeros(fx.shape)
    for i in range(nx):
        x = (i-nx/2)*dx+1e-11
        for j in range(ny):
            y = (j-ny/2)*dx+1e-11
            r = math.sqrt(x*x + y*y)
            nphi[i][j] = -fx[i][j]*y/r + fy[i][j]*x/r
            nr[i][j] = fx[i][j]*x/r + fy[i][j]*y/r
    return nr, nphi
    
def subplot(fig, i, path, **options):
    nx,ny = options.get("shape", (256,256))
    spx,spy,spi = options.get("position",(1,1,1))
    xmin, xmax, ymin, ymax = options.get("extent", [-2.,2.,-2.,2.])
    xlabel = options.get("xlabel", 'x')
    ylabel = options.get("ylabel", 'y')
    cmap = options.get("cmap", 'jet')
    title = options.get("title", '')
    titletype = options.get("titletype", None)
    mult = options.get("mult", 1.)
    ratio = options.get("ratio", 1e-3)
    xlim = options.get("xlim", [xmin,xmax])
    ylim = options.get("ylim", [ymin,ymax])
    logarithmic = options.get("logarithmic", False)
    colorbar = options.get("colorbar", True)
    cbartitle = options.get("cbartitle", None)
    cbarticks = options.get("cbarticks", None)
    ncbarticks = options.get("ncbarticks", None)
    cbarticklabels = options.get("cbarticks", None)
    ticks = options.get("ticks", None)
    xticks = options.get("xticks", None)
    yticks = options.get("yticks", None)
    xticklabels = options.get("xticklabels", None)
    yticklabels = options.get("yticklabels", None)
    verbose = options.get("verbose", 0)
    transpose = options.get("transpose", 0)
    field = options.get("field", None)
    maximum = options.get("maximum", 'global')
    fontsize = options.get("fontsize", 10)
    fradius = options.get("filter_radius", 0)
    forigin = options.get("filter_origin", (nx/2,ny/2))
    # Options related to contour plots
    contour = options.get("contour", False)
    cinline = options.get("cinline", 0)
    cnum = options.get("cnum", 5)
    clevels = options.get("clevels", [])
    clinewidth = options.get("clinewidth", None)
    fmt = options.get("fmt", '%06d')
    ftype = options.get("ftype", 'txt')
    vmin = options.get("vmin", None)
    vmax = options.get("vmax", None)
    normalize = options.get("normalize", None)
    normalize_value = options.get("normalize_value", 1.)
    interpolation = options.get("interpolation", None)
    zoom = options.get("zoom", None)
    fft = options.get("fft", False)
    func = options.get("func", None)
    archive = options.get("archive", None)
    scatter = options.get("scatter", False)
    
    if not ticks == None:
        xticks = ticks
        yticks = ticks
    else:
        if xticks == None:
            nticks = options.get("nticks", 5)
            xticks = [xlim[0] + k*(xlim[1]-xlim[0])/(nticks-1) for k in range(nticks)]
        
        if yticks == None:
            nticks = options.get("nticks", 5)
            yticks = [ylim[0] + k*(ylim[1]-ylim[0])/(nticks-1) for k in range(nticks)]
        
    if verbose == 1:
        print('Ticks ', xticks, yticks)
    dx = (xmax-xmin)/nx
    dy = (ymax-ymin)/ny
     
    if maximum == 'local':
        xarea_min = int((xlim[0] - xmin)/dx)
        xarea_max = int((xlim[1] - xmin)/dx)
        yarea_min = int((ylim[0] - ymin)/dy)
        yarea_max = int((ylim[1] - ymin)/dy)
    else:
        xarea_min = 0
        xarea_max = int((xmax - xmin)/dx)
        yarea_min = 0
        yarea_max = int((ymax - ymin)/dy)
        
    if verbose == 1:
        print('Area ', xarea_min, xarea_max, yarea_min, yarea_max)
     
    mp.rcParams.update({'font.size': fontsize})
    ax = fig.add_subplot(spx,spy,spi)
    
    if field is None:
        read, field = bo_file_load(path,i,nx,ny,fmt=fmt,ftype=ftype,verbose=verbose, transpose=transpose, archive=archive)
        if read == False:
            print('None ', path, ' iteration ', i, 'found' )
            return
        else:
            field *= mult
    if fft:
        field = np.fft.fft2(field)
        field = np.fft.fftshift(field)
        field = np.abs(field)
         
    if normalize:
        mmax = np.amax(field)
        field /= mmax
        field *= normalize_value
    if verbose == 1:
        print('Shape ', field.shape)
    
    if fradius > 0:
        for i in range(nx):
            x = dx*(i - forigin[0])
            for j in range(ny):
                y = dy*(j - forigin[1])
                if (x*x + y*y) < fradius*fradius:
                    field[i,j] = 0.
    maxe = np.amax(field[xarea_min:xarea_max, yarea_min:yarea_max])
    mine = np.amin(field[xarea_min:xarea_max, yarea_min:yarea_max])
    
    if verbose:
        print('Maxe, mine ', maxe, mine)
    vmin_,vmax_ = None,None
    
    if maxe > 0 and mine < 0:
        vmax_ = max(abs(maxe),abs(mine))
        vmin_ = -vmax_
    else:
        vmax_ = maxe
        vmin_ = mine
    if logarithmic:
        if maxe == 0:
            maxe = 1
        if maxe == mine:
            vmin_ = maxe
            vmax_ = 1e9*maxe
        else:
            vmax_ = maxe
            vmin_ = maxe*ratio
    #print vmin_, vmax_
    if vmin == None:
        vmin = vmin_
    #else:
    #    vmin *= vmin_
    if vmax == None:
        vmax = vmax_
    #else:
    #    vmax *= vmax_

    norm = None
    if logarithmic:
        norm = clr.LogNorm(clip='True', vmin = vmin, vmax = vmax)
        cmap1 = mp.cm.get_cmap(cmap)
        cmap1.set_under('w', False)
        cmap1.set_bad('w', 1)
    else:
        cmap1 = cmap
    #scatter = False
    if scatter:
        cmap_ = mp.cm.get_cmap(cmap1)
        color_ = cmap_(1.)
        print(color_)
        ax_ = create_axis(nx,dx,xmin)
        ay_ = create_axis(ny,dy,ymin)
        x_sc = []
        y_sc = []
        for i_ in range(nx):
            for j_ in range(ny):
                if field[i_, j_] > 0:
                    x_sc.append(ax_[j_])
                    y_sc.append(-ay_[i_])
        if logarithmic:
            surf = ax.imshow(field, extent=[xmin,xmax,ymin,ymax], cmap = cmap1, norm=norm, interpolation = interpolation)
        else:
            surf = ax.imshow(field, extent=[xmin,xmax,ymin,ymax], cmap = cmap1, vmin = vmin, vmax = vmax, norm=norm, interpolation = interpolation)
        ax.scatter(x_sc,y_sc, s=2, c=[color_])
    elif contour:
        ax_ = create_axis(nx,dx,xmin)
        ay_ = create_axis(ny,dy,ymin)
        X, Y = np.meshgrid(ax_, ay_)
        
        if len(clevels) == 0:
            surf = ax.contour(X,Y,field, cnum, inline = cinline, linewidths = clinewidth)
        else:
            clevels = [x*maxe for x in clevels]
            if verbose:
                 print('Contour levels ', clevels, maxe)
                 
            surf = ax.contour(X,Y,field, clevels, inline = cinline, linewidths = clinewidth)
        if cinline == 1:
            ax.clabel(surf, fontsize=9, inline=1)
    else:
        if logarithmic:
            surf = ax.imshow(field, extent=[xmin,xmax,ymin,ymax], cmap = cmap1, norm=norm, interpolation = interpolation)
        else:
            surf = ax.imshow(field, extent=[xmin,xmax,ymin,ymax], cmap = cmap1, vmin = vmin, vmax = vmax, norm=norm, interpolation = interpolation)

    if title != '':
        if titletype == 'max':
            ax.set_title(title + ': %.1e'%(maxe))
        else:
            ax.set_title(title)

    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xticks(xticks)
    ax.set_yticks(yticks)
    if xticklabels != None:
        ax.set_xticklabels(xticklabels)
    if yticklabels != None:
        ax.set_yticklabels(yticklabels)
    if zoom:
        zoom_val = options.get("zoom_val", 1)
        axins = zoomed_inset_axes(ax, zoom_val, loc=1)  # zoom = 6
        zoom_xlim = options.get("zoom_xlim", [xmin,xmax])
        zoom_ylim = options.get("zoom_ylim", [ymin,ymax])
        if logarithmic:
            zoom_surf = axins.imshow(field, extent=[xmin,xmax,ymin,ymax], cmap = cmap1, norm=norm, interpolation = interpolation)
        else:
            zoom_surf = axins.imshow(field, extent=[xmin,xmax,ymin,ymax], cmap = cmap1, vmin = vmin, vmax = vmax, norm=norm, interpolation = interpolation)
        axins.set_xlim(zoom_xlim)
        axins.set_ylim(zoom_ylim)
        axins.yaxis.get_major_locator().set_params(nbins=7)
        axins.xaxis.get_major_locator().set_params(nbins=7)

        plt.xticks(visible=False)
        plt.yticks(visible=False)

        # draw a bbox of the region of the inset axes in the parent axes and
        # connecting lines between the bbox and the inset axes area
        mark_inset(ax, axins, loc1=2, loc2=3, fc="none", ec="0.5")
    #print('1', ax)
    if colorbar:
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)

        if cbarticks is not None:
            cbar.ax.set_yticks(cbarticks)
        if ncbarticks != None:
            tick_locator = ticker.MaxNLocator(nbins=ncbarticks)
            cbar.locator = tick_locator
            cbar.update_ticks()
            
        if cbartitle != None:
            cbar.ax.set_ylabel(cbartitle)
        for l in cbar.ax.yaxis.get_ticklabels():
            l.set_family("Times New Roman")
#        ticks = cbar.ax.get_yticklabels()
#        
#        cticklabels = ['%s'%(t) for t in ticks]
#        print cticklabels
#        cbar.ax.set_yticklabels(cticklabels)
    #print('2', ax)
    if func == None:
        return ax
    else:
        return ax, func(field)

def full_number(sp,de,ne,bound=0):
    n = len(sp)
    if n != ne:
        print('Wrong data: ne != n', ne, n)
        exit(-1)
    full_energy = sum(sp)
    full_number = 0
    for i in range(bound,n):
        full_number += sp[i]/(de*(i+0.425))
        
    return full_number

def energy_spectra_to_number(sp,de,ne):
    n = len(sp)
    if n != ne:
        print('Wrong data: ne != n', ne, n)
        exit(-1)
    full_energy = sum(sp)
    full_number = 0
    nsp = np.zeros(n)
    nspn = np.zeros(n)
    for i in range(n):
        dn = sp[i]/(de*(i+0.425))
        full_number += dn
        nsp[i] = dn/de
        nspn[i] = dn/de
    if normalize and full_number > 0:
        for i in range(n):
            nspn[i] /= full_number
    return nsp, nspn

def number_spectra_to_energy(sp,de,ne):
    n = len(sp)
    if n != ne:
        print('Wrong data: ne != n', ne, n)
        exit(-1)
    
    nsp = np.zeros(n)
    
    for i in range(n):
        nsp[i] = sp[i]*(de*(i+0.425))

    return nsp

def smooth_2d(a,factor_ne,factor_nt):
    ne = len(a)
    nt = len(a[0])
    nt1 = nt/factor_nt
    ne1 = ne/factor_ne
#    print ne, nt, ne1, nt1
    res = np.zeros((ne1,nt1))

    for j in range(ne1):
        for i in range(nt1):
            for k in range(factor_nt):
                for m in range(factor_ne):
                    res[j][i] += a[j*factor_ne+m][i*factor_nt+k]/factor_ne/factor_nt
    
    return res

def smooth(a,factor):
    n = len(a)
    n1 = n/factor
    res = np.zeros(n1)
    for m in range(factor/2):
        res[0] += a[m]/(factor/2)
        res[n1-1] += a[(n1-1)*factor+m]/(factor/2)
        
    for j in range(1,n1-1):
        for m in range(factor):
            res[j] += a[j*factor+m-factor/2]/factor
    return res

def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    r"""Smooth (and optionally differentiate) data with a Savitzky-Golay filter.
    The Savitzky-Golay filter removes high frequency noise from data.
    It has the advantage of preserving the original shape and
    features of the signal better than other types of filtering
    approaches, such as moving averages techniques.
    Parameters
    ----------
    y : array_like, shape (N,)
        the values of the time history of the signal.
    window_size : int
        the length of the window. Must be an odd integer number.
    order : int
        the order of the polynomial used in the filtering.
        Must be less then `window_size` - 1.
    deriv: int
        the order of the derivative to compute (default = 0 means only smoothing)
    Returns
    -------
    ys : ndarray, shape (N)
        the smoothed signal (or it's n-th derivative).
    Notes
    -----
    The Savitzky-Golay is a type of low-pass filter, particularly
    suited for smoothing noisy data. The main idea behind this
    approach is to make for each point a least-square fit with a
    polynomial of high order over a odd-sized window centered at
    the point.
    Examples
    --------
    t = np.linspace(-4, 4, 500)
    y = np.exp( -t**2 ) + np.random.normal(0, 0.05, t.shape)
    ysg = savitzky_golay(y, window_size=31, order=4)
    import matplotlib.pyplot as plt
    plt.plot(t, y, label='Noisy signal')
    plt.plot(t, np.exp(-t**2), 'k', lw=1.5, label='Original signal')
    plt.plot(t, ysg, 'r', label='Filtered signal')
    plt.legend()
    plt.show()
    References
    ----------
    .. [1] A. Savitzky, M. J. E. Golay, Smoothing and Differentiation of
       Data by Simplified Least Squares Procedures. Analytical
       Chemistry, 1964, 36 (8), pp 1627-1639.
    .. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
       W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
       Cambridge University Press ISBN-13: 9780521880688
    """
    import numpy as np
    from math import factorial
    
    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m[::-1], y, mode='valid')

def R(x,y,z):
    R = math.sqrt(x*x + y*y + z*z)
    return R

def exf(x,y,z,k):
    r = R(x,y,z)
    if r < 1e-10:
        r = 1e-10
    return x*z/(r*r) * f2(r,k)*2.

def eyf(x,y,z,k):
    r = R(x,y,z)
    if r < 1e-10:
        r = 1e-10
    return y*z/(r*r) * f2(r,k)*2.

def ezf(x,y,z,k):
    r = R(x,y,z)
    if r < 1e-10:
        r = 1e-10
    return 2. * (f3(r,k) + z*z/(r*r)*f2(r,k))
  
def bxf(x,y,z,k):
    r = R(x,y,z)
    if r < 1e-10:
        r = 1e-10
    return y/r * 2. * f1(r,k)

def byf(x,y,z,k):
    r = R(x,y,z)
    if r < 1e-10:
        r = 1e-10
    return -x/r * 2. * f1(r,k)
    
def f1(r,k):
    return -k*k/r * math.cos(k*r) + k/(r*r) * math.sin(k*r)

def f2(r,k):
    return (-k*k/r + 3./(r*r*r))*math.sin(k*r) - 3.*k/(r*r)*math.cos(k*r)

def f3(r,k):
    return (k*k/r - 1./(r*r*r))*math.sin(k*r) + k/(r*r)*math.cos(k*r)

def Sin(r,t,k):
    return np.sin(k*r + t)

def Cos(r,t,k):
    return np.cos(k*r + t)

def Ex(x,y,z,R,t,k):
    return z * x / (R*R*R) * ( Sin(R,t,k)*(-1 + 3./(k*k*R*R)) - Cos(R,t,k)*3./(k*R))

def Ey(x,y,z,R,t,k):
    return z * y / (R*R*R) * ( Sin(R,t,k)*(-1 + 3./(k*k*R*R)) - Cos(R,t,k)*3./(k*R))

def Ez(x,y,z,R,t,k):
    return -( (x*x + y*y)/(R*R*R) * Sin(R,t,k) + 1./(k*k*R*R*R) * (-1. + 3.*z*z/(R*R))*Sin(R,t,k) + 1./(k*R*R) * (1. - 3.*z*z/(R*R)) * Cos(R,t,k) )

def Bx(x,y,z,R,t,k):
    return - y /(R*R) * (Sin(R,t,k) + 1./(k*R)*Cos(R,t,k))

def By(x,y,z,R,t,k):
    return - x /(R*R) * (Sin(R,t,k) + 1./(k*R)*Cos(R,t,k))

