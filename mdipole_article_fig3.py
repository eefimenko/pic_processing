#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np
import matplotlib as mp
from matplotlib.font_manager import FontProperties

def main():
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
    path = './'
    picspath = 'pics/'
    fontsize = 22
    config = utils.get_config(path + "/ParsedInput.txt")
    wl = float(config['Wavelength'])
    xmax = float(config['X_Max'])/wl #mkm
    xmin = float(config['X_Min'])/wl #mkm
    ymax = float(config['Y_Max'])/wl #mkm
    ymin = float(config['Y_Min'])/wl #mkm
    zmax = float(config['Z_Max'])/wl #mkm
    zmin = float(config['Z_Min'])/wl #mkm
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    power = int(config['PeakPowerPW'])
    dx = float(config['Step_X'])/wl
    dy = float(config['Step_Y'])/wl
    dz = float(config['Step_Z'])/wl
    mult = 1/(2.*dx*dy*dz*wl*wl*wl)
    step = float(config['TimeStep'])*1e15*float(config['BOIterationPass'])
    omega = float(config['Omega'])
    T = 2 * math.pi/omega
    nt = int(T*1e15/step)
    print nt
    iterations = [309, 550, 760]
    #iterations = [344, 380, 760]
    figures = [utils.eypath, utils.bypath, utils.neypath, utils.nphypath]
    cmaps = ['Reds', 'Blues', 'hot_r', 'RdPu']
    titles = ['Electric field', 'Magnetic field', 'Electrons', 'Photons']
    log = [False, False, False, False]
    mult = [1e-11,1e-11, mult*1e-24, mult*1e-24]
   

    spx_ = len(iterations)
    spy_ = len(figures)
    labels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h','i', 'j', 'k', 'l', 'm']
    nperiods = 5
    steps_per_period = 15
    verbose = 1
#    plt.rcParams.update({'mathtext.default':  'regular' })
    fig = plt.figure(num=None, figsize=(4.*spy_, 3.4*spx_))
    for k in range(spx_):
        i = iterations[k]
        for j in range(spy_):
           
            mp.rcParams.update({'font.size': fontsize})
            yticks = [-1, -0.5,0,0.5, 1]
            xticks = [-1, -0.5,0,0.5, 1]
           
            
            if k == spx_ -1:
                xlabel = 'x/$\lambda$'
                xticklabels = ['$-1$', '$-0.5$','$0$','$0.5$', '$1$']
            else:
                xlabel = ''
                xticklabels = []
                
            if j == 0:    
                ylabel = 'y/$\lambda$'
                yticklabels = ['$-1$', '$-0.5$','$0$','$0.5$', '$1$']
            else:
                ylabel = ''
                yticklabels = []
                
            if figures[j] == utils.neypath or figures[j] == utils.nphypath:
                ncbarticks = 3
            else:
                ncbarticks = 5
                
            if j == 0:
                
                    # fx = utils.bo_file_load(dirs[k]+utils.ezpath,
                    #                         i+n0[k],nx[k],ny[k],verbose=1)
                    # fy = utils.bo_file_load(dirs[k]+utils.bzpath,
                    #                         i+n0[k],nx[k],ny[k],verbose=1)
                    # sf = np.square(fx) + np.square(fy)
                ax = utils.subplot(fig, i, path + figures[j],
                                   shape = (nx,ny), position = (spx_,spy_,j+spy_*k+1),
                                   extent = [xmin, xmax, ymin, ymax],
                                   cmap = cmaps[j], title = '', #titles[j] % (power),
                                   colorbar = True, logarithmic=log[j], verbose=verbose,
                                   xlim = [-1.,1.], ylim = [-1.,1.], xticks = xticks, yticks = yticks,yticklabels = yticklabels, xticklabels = xticklabels,
                                   xlabel = xlabel, ylabel = ylabel, fontsize=fontsize, mult = mult[j], transpose = 1, ncbarticks = ncbarticks)
            else:
                if (figures[j] == utils.neypath or figures[j] == utils.nphypath) and k == 0:
                    read, field = utils.bo_file_load(path + '/' + figures[j], i, nx, ny, transpose = 1, verbose = 1)
                    for itp in range(1,nperiods):
                        read, field_ = utils.bo_file_load(path + '/' + figures[j], i + itp*steps_per_period, nx, ny,  transpose = 1)
                        field += field_
                        if figures[j] == utils.neypath:
                            read, field_ = utils.bo_file_load(path + '/' + utils.nexpath, i + itp*steps_per_period, nx, ny,  transpose = 1)
                        else:
                            read, field_ = utils.bo_file_load(path + '/' + utils.nphxpath, i + itp*steps_per_period, nx, ny,  transpose = 1)
                        field += field_
                    field *= mult[j]
                else:
                    field = None
                ax = utils.subplot(fig, i, path+figures[j],
                                   shape = (nx,ny), position = (spx_,spy_,j+spy_*k+1),
                                   extent = [xmin, xmax, ymin, ymax],
                                   cmap = cmaps[j], title = '', titletype = 'simple',
                                   colorbar = True, logarithmic=log[j], verbose=verbose,
                                   xlim = [-1.,1.], ylim = [-1.,1.], xticks = xticks, yticks = yticks,yticklabels = yticklabels, xticklabels = xticklabels,
                                   xlabel = xlabel, ylabel = ylabel, vmax = 0.5, vmin=0.5,
                                   maximum = 'local', fontsize = fontsize, mult = mult[j], transpose = 1, ncbarticks = ncbarticks, field = field)
                
                                  
                
            if j == 0:
                ax.text(-1.0, 0.7, '$(%s)$'%(labels[k*spy_ + j]), fontsize = 24)
            else:
                ax.text(-1.0, 0.7, '$(%s)$'%(labels[k*spy_ + j]), fontsize = 24)
            picname = picspath + '/' + "mdipole_article_fig3.png"
    plt.tight_layout()
    plt.savefig(picname, bbox_inches='tight', dpi=256)
    plt.close()

    
    # picname = picspath + '/' + "fig4_full.png"
    # # plt.tight_layout()
    # plt.savefig(picname, bbox_inches='tight', dpi=128)
    # plt.close()
    # plt.show()
    
if __name__ == '__main__':
    main()
