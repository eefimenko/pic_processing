#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import sys
import math
import utils

def read_diag(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def main():

    config = utils.get_config("ParsedInput.txt")
    BOIterationPass = float(config['BOIterationPass'])
    delta = 1
    ppath = 'data/ElPhase/'
    mpath = 'data/PosPhase/'
    p1path = 'data/ElMomentum/'
    m1path = 'data/PosMomentum/'
    picspath = 'pics'
    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(ppath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(ppath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax) + ' files'
        
   

    nx1 = int(config['ElPhase.SetMatrixSize_0'])
    ny1 = int(config['ElPhase.SetMatrixSize_1'])
    zmin = float(config['ElPhase.SetBounds_0'])*1e4
    zmax = float(config['ElPhase.SetBounds_1'])*1e4
    Emin = float(config['ElPhase.SetBounds_2'])
    Emax = float(config['ElPhase.SetBounds_3'])
    Mmin = float(config['ElMomentum.SetBounds_2'])
    Mmax = float(config['ElMomentum.SetBounds_3'])
    
    for i in range(nmin,nmax,delta):
        pname = ppath + '/' + "%06d.txt" % (i,)
        mname = mpath + '/' + "%06d.txt" % (i,)
        pfield = read_diag(pname,nx1,ny1)
#        mfield = read_diag(mname,nx1,ny1)
        p1name = p1path + '/' + "%06d.txt" % (i,)
#        m1name = m1path + '/' + "%06d.txt" % (i,)
        p1field = read_diag(p1name,nx1,ny1)
#        m1field = read_diag(m1name,nx1,ny1)
        fig = plt.figure(num=None)
        ax = fig.add_subplot(2,1,1)
        ax.imshow(pfield, extent = [zmin,zmax, Emin,Emax], aspect='auto', origin='lower', norm=clr.LogNorm())
        ay = fig.add_subplot(2,1,2)
        ay.imshow(p1field, extent = [zmin,zmax, Emin,Emax], aspect='auto', origin='lower', norm=clr.LogNorm())
#        ax = fig.add_subplot(2,2,2)
#        ax.imshow(p1field, extent = [zmin,zmax, Mmin,Mmax], aspect='auto', origin='lower', norm=clr.LogNorm())
#        ay = fig.add_subplot(2,2,4)
#        ay.imshow(m1field, extent = [zmin,zmax, Mmin,Mmax], aspect='auto', origin='lower', norm=clr.LogNorm())
        picname = picspath + '/' + "phase%06d.png" % (i,)
        print picname
        plt.savefig(picname)
        plt.close()
        #plt.show()

if __name__ == '__main__':
    main()
