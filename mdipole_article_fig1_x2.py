#!/usr/bin/python
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.cm as cm
import matplotlib.colors as colors
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
import math
import utils
import sys

def add_arrow_to_line2D(
    axes, line, arrow_locs=[0, 0.2, 0.4, 0.6, 0.8],
        arrowstyle='-|>', arrowsize=1, width = None, transform=None):
    """
    Add arrows to a matplotlib.lines.Line2D at selected locations.

    Parameters:
    -----------
    axes: 
    line: Line2D object as returned by plot command
    arrow_locs: list of locations where to insert arrows, % of total length
    arrowstyle: style of the arrow
    arrowsize: size of the arrow
    transform: a matplotlib transform instance, default to data coordinates

    Returns:
    --------
    arrows: list of arrows
    """
    if not isinstance(line, mlines.Line2D):
        raise ValueError("expected a matplotlib.lines.Line2D object")
    x, y = line.get_xdata(), line.get_ydata()

    arrow_kw = {
        "arrowstyle": arrowstyle,
        "mutation_scale": 10 * arrowsize,
    }

    color = line.get_color()
    use_multicolor_lines = isinstance(color, np.ndarray)
    if use_multicolor_lines:
        raise NotImplementedError("multicolor lines not supported")
    else:
        arrow_kw['color'] = color

    if width == None:
        linewidth = line.get_linewidth()
        if isinstance(linewidth, np.ndarray):
            raise NotImplementedError("multiwidth lines not supported")
        else:
            arrow_kw['linewidth'] = linewidth
    else:
        arrow_kw['linewidth'] = width

    if transform is None:
        transform = axes.transData

    arrows = []
    for loc in arrow_locs:
        s = np.cumsum(np.sqrt(np.diff(x) ** 2 + np.diff(y) ** 2))
        n = np.searchsorted(s, s[-1] * loc)
        arrow_tail = (x[n], y[n])
        arrow_head = (np.mean(x[n:n + 2]), np.mean(y[n:n + 2]))
        p = mpatches.FancyArrowPatch(
            arrow_tail, arrow_head, transform=transform,
            **arrow_kw)
        axes.add_patch(p)
        arrows.append(p)
    return arrows

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def read_traj(file, end=1000000):
    f = open(file, 'r')
    x_ = []
    y_ = []
    z_ = []
    e_ = []
    r_ = []
    mx_ = []
    my_ = []
    mz_ = []
    ev = 1.6e-12
    c = 2.99792e+10
    for line in f:
        tmp = line.split()
        i = int(tmp[0])
        x = float(tmp[1])
        y = float(tmp[2])
        z = float(tmp[3])
        px = float(tmp[4])
        py = float(tmp[5])
        pz = float(tmp[6])
        r = math.sqrt(x*x + y*y)
        pr = math.sqrt(px*px + py*py)
        e = math.sqrt(px*px + py*py + pz*pz)*c/ev/0.511e6
        mx = y*pz - py*z
        my = -(x*pz - px*z)
        mz = x*py - px*y
        x_.append(x)
        y_.append(y)
        z_.append(z)
        mx_.append(mx)
        my_.append(my)
        mz_.append(mz)
        r_.append(r)
        e_.append(e)
        
        if i > end:
            break
    return np.array(x_), np.array(y_), np.array(z_), np.array(r_), np.array(e_), np.array(mx_), np.array(my_), np.array(mz_)

def normalize(a, n = 1.):
    if n != 0:
        for i in range(len(a)):
            a[i] /= n
    return a
  
def main():
    path = './'
    expath = 'data/E2x'
    partpath = 'ParticleTracking/Electrons'
#    partpath = 'ParticleTracking' 
    picspath = './pics'
    num = 10000

    config = utils.get_config("ParsedInput.txt")

    BOIterationPass = float(config['BOIterationPass'])
    dt = float(config['TimeStep'])*1e15*BOIterationPass #fs
    maxpower = float(config['PeakPower'])*1e-7*1e-15 #PW
    peakpower = float(config['PeakPower'])*1e-7*1e-15
    const_a_p = 7.81441e-9
    a0 = const_a_p * math.sqrt(peakpower*1e22)
    print a0
    print "Power = " + str(maxpower) + 'PW'
    print "dt = " + str(dt) + 'fs'
#    print "duration = " + str(duration)
#    print "delay = " + str(delay)
    
    Xmax = float(config['X_Max']) #mkm
    Xmin = float(config['X_Min']) #mkm
    Ymax = float(config['Y_Max']) #mkm
    Ymin = float(config['Y_Min']) #mkm
    Zmax = float(config['Z_Max']) #mkm
    Zmin = float(config['Z_Min']) #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz
    wl = float(config['Wavelength']) 
    step = (Xmax-Xmin)/nx

    mult = 1/(2.*dx*dy*dz)

    delta = 1
    jet = plt.get_cmap('jet') 
    cNorm  = colors.Normalize(vmin=0.5, vmax=a0*1e-3*0.5)
    scalarMap = cm.ScalarMappable(norm=cNorm, cmap=jet)
    print scalarMap.get_clim()
   
    
    n = 0
    ii = 0
    zout = 0.4
    start = 0
    end=-1
    bound=0.1
    boundz=0.25
    xmax = 1.0
    zmax = 0.06
    N = 60
    i0 = 990
#    read, nez = utils.bo_file_load(path + utils.nezpath,i0,nx,ny)
#    for i in range(N-1):
#        read, dnez = utils.bo_file_load(path + utils.nezpath,i0+i+1,nx,ny)
#        nez += dnez
        
#    nez /= N*(2.*dx*dy*dz)
    read, nex = utils.bo_file_load(path + utils.nexpath,i0,nx,ny)
    for i in range(N-1):
        read, dnex = utils.bo_file_load(path + utils.nexpath,i0+i+1,nx,ny)
        nex += dnex
    nex /= N*(2.*dx*dy*dz)*1e23
    
    extent=[Xmin/wl, Xmax/wl, Ymin/wl, Ymax/wl]
   
#    trajectories = [71, 199, 392, 7459, 7626, 7725, 8032]
#    trajectories = []
    mp.rcParams.update({'font.size': 16})
    fig = plt.figure(num=None, figsize=(5.5, 5))
#    axy = fig.add_subplot(1,2,1)
    az = fig.add_subplot(1,1,1)
    
    clrs = ['g', 'b', 'fuchsia', 'c', 'm', 'lime', 'w']
    
#    for n_, iteration  in enumerate(trajectories):
#        print iteration
#        i_ = iteration
#        x,y,z,r,e,mx,my,mz = read_traj('./' + partpath + '/%d.txt' % (i_), end = 5500)
#        x /= wl
#        y /= wl
#        z /= wl
#        r /= wl
        
#        if r[0] < 0.35:
#            type_ = 0
#        else:
#            type_ = 1
#        tr = 1
#        for i_ in range(len(z)):
#            if abs(z[i_]) > 0.5 or r[i_] > 1.0:
#                tr = 0
#                continue
#        if abs(z[-1]) > 0.3:
#            tr = 0
#            continue
#        if len(r) < 900:
#            tr = 0
#            continue
#
#        if len(e) == 0:
#           continue

#        if tr == 0:
#            continue
#        if x[0] < 0:
#            r *= -1
#        ii += 1
#        cmap = 'plasma'
#        axy.scatter(x[start:end], y[start:end], cmap=cmap, edgecolor='none', s=5)
#        line, = axy.plot(x[start:end], y[start:end], color = clrs[n_])
#        add_arrow_to_line2D(axy, line, arrowstyle='->', width = 2)
#        axy.plot(x[start], y[start], marker='o')
#        for k_ in range(1,len(x)/150):
#            axy.plot(x[start+k_*150], y[start+k_*150], marker='o', color='g')
        
#    axy.set_xlim([-xmax,xmax])
#    axy.set_ylim([-xmax,xmax])
#    axy.axhline(y=0, color='k')
#    axy.axvline(x=0, color='k')
#    axy.set_xlabel('$x/\lambda$')
#    axy.set_ylabel('$y/\lambda$')
#    axy.imshow(nez, extent=extent, cmap='hot_r', origin='lower', aspect = 'auto')
#    line, = az.plot(r[start:end], z[start:end], color = clrs[n_])
#    add_arrow_to_line2D(az, line, arrowstyle='->', width = 2)
        #az.scatter(r[start:end], z[start:end], edgecolor='none', s=5)
    az.set_xlim([-xmax,xmax])
    az.set_ylim([-zmax,zmax])
    az.set_xticks([-1, -0.5, 0, 0.5, 1])
    az.set_yticks([-0.06, -0.03, 0, 0.03, 0.06])
    az.axvline(x=0, color='gray')
    az.axhline(y=dz/wl, color='g')
    az.axhline(y=-dz/wl, color='g')
    az.axhline(y=0, color='gray')
    az.set_xlabel('$r/\lambda$')
    az.set_ylabel('$z/\lambda$')
    az.text(-xmax*1.6, zmax, '(c)')
    surf = az.imshow(nex.transpose(), extent=extent, cmap='hot_r', origin='lower', aspect = 'auto')
    divider = make_axes_locatable(az)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)
#    az.legend()


        
#    print float(n)/num
        
    picname = picspath + '/' + "mdipole_article_trajectories_x2.png"
    print picname
    plt.tight_layout()
    plt.savefig(picname, dpi=256)
    plt.close()
    
if __name__ == '__main__':
    main()
