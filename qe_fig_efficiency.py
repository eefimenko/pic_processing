#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_gamma(filename):
    f = open(filename, 'r')
    tmp = f.readline().split()
    f.close()
    return float(tmp[0]), float(tmp[1]), float(tmp[2])

def main():
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
    el = []
    ph = []
    pos = []
    ne = []
    dirs = ['pulse_nc_0.001_30', 'pulse_nc_0.01_30', 'pulse_nc_0.1_30', 'pulse_nc_1_30', 'pulse_nc_10_30','pulse_nc_30_30', 'pulse_nc_50_30', 'pulse_nc_100_30', 'pulse_nc_1000_30']
    updirs = ['1mkm', '2mkm', '3mkm']
    dashes = [[2,2], [6,1], [3,1]]
    fig = plt.figure()
    ax1 = fig.add_subplot(3,2,1)
    ax2 = fig.add_subplot(3,2,3)
    ax3 = fig.add_subplot(3,2,5)
    ax4 = fig.add_subplot(3,2,2)
    ax5 = fig.add_subplot(3,2,4)
    ax6 = fig.add_subplot(3,2,6)
    m = 0
    for ud in updirs:
        el = []
        ph = []
        pos = []
        el_1gev = []
        ph_1gev = []
        pos_1gev = []
        ne = []
        number = []
        for d in dirs:
            ph_, el_, pos_ = read_gamma(ud + '/' + d + '/efficiency.txt')
            el.append(el_)
            ph.append(ph_)
            pos.append(pos_)
            ph_, el_, pos_ = read_gamma(ud + '/' + d + '/efficiency_1gev.txt')
            el_1gev.append(el_)
            ph_1gev.append(ph_)
            pos_1gev.append(pos_)
            config = utils.get_config(ud + '/' + d + '/ParsedInput.txt')
            n_cr = float(config['n_cr'])
            ne_ = float(config['Ne'])
            r = float(config['R'])
            ne.append(ne_/n_cr)
            number.append(4./3.* math.pi * r**3 * ne_)
        if ud == '1mkm':
            ax_full = ax1
            ax_1gev = ax4
        elif ud == '2mkm':
            ax_full = ax2
            ax_1gev = ax5
        elif ud == '3mkm':
            ax_full = ax3
            ax_1gev = ax6
        ax_full.plot(ne, ph, 'r', label = u'Фотоны')
        ax_full.plot(ne, el, 'g', label = u'Электроны')
        ax_full.plot(ne, pos, 'b', label = u'Позитроны')
        
        ax_1gev.plot(ne, ph_1gev, 'r', label = u'Фотоны')
        ax_1gev.plot(ne, el_1gev, 'g', label = u'Электроны')
        ax_1gev.plot(ne, pos_1gev, 'b', label = u'Позитроны')
       
        m += 1
#    ax1.set_yscale('log')
    ax1.set_xscale('log')
    ax2.set_xscale('log')
    ax3.set_xscale('log')
    ax4.set_xscale('log')
    ax5.set_xscale('log')
    ax6.set_xscale('log')
    ax1.set_yscale('log')
    ax2.set_yscale('log')
    ax3.set_yscale('log')
    ax4.set_yscale('log')
    ax5.set_yscale('log')
    ax6.set_yscale('log')
    ax1.legend(loc = 'lower left')
    ax2.legend(loc = 'lower left')
    ax3.legend(loc = 'lower left')
    ax4.legend(loc = 'lower left')
    ax5.legend(loc = 'lower left')
    ax6.legend(loc = 'lower left')
    ax1.set_ylim([1e-3, 1])
    ax2.set_ylim([1e-3, 1])
    ax3.set_ylim([1e-3, 1])
#    
#    ax3.set_ylim([1e-3, 1])

#    ax4.set_ylim([1e-3, 1])
    plt.show()

if __name__ == '__main__':
    main()
