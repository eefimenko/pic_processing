#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import sys
import math
import utils
import numpy as np

def filterr(array,boundary):
    shape = array.shape
    fmin = np.zeros(shape)
    fmax = np.zeros(shape)
    for i in range(shape[0]):
        for j in range(shape[1]):
            if j < boundary:
                fmin[i][j] = array[i][j]
            else:
                fmax[i][j] = array[i][j]
    return fmin, fmax

def read_diag(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def find_gamma_av(array, dr, dg):
    s = array.shape
    res = [0] * s[0]
    nx = s[0]
    ny = s[1]
    av = 0
   
    for i in range(nx):
        for j in range(ny):
            if j*dr < 0.1:
                res[i] += array[i][j]
    s = sum(res)
    for i in range(nx):
        av += res[i]*i*dg/s

    return av

def calculate_current(pz,dpz):
    n = len(pz)/2
    j_pos = 0
    j_neg = 0
    c2 = 3.33e-10*1e-6
    for i in range(n):
        p = i*dpz
        v = p/math.sqrt(utils.ElectronMass*utils.ElectronMass + p*p/(utils.LightVelocity*utils.LightVelocity))
        j_pos += pz[n-i]*v*utils.ElectronCharge
        j_neg += pz[n+i]*v*utils.ElectronCharge
    return c2*j_pos, c2*j_neg

def main():
    path = './'
    ppath = 'data/PzR/'
    pspath = 'data/PzRs/'
    mpath = 'data/PosPhase/'
    p1path = 'data/ElMomentum/'
    m1path = 'data/PosMomentum/'
    ezpath = 'data/E2z'
    picspath = 'pics'
<<<<<<< HEAD
            
    nmin, nmax, delta = utils.get_min_max_iteration(sys.argv,path+utils.ezpath)
    nmax = 314
    nmin = 301
=======
    config = utils.get_config("ParsedInput.txt")
    BOIterationPass = float(config['BOIterationPass'])
   
    delta = 1
    nmax = 301
    nmin = 293
>>>>>>> eba800e4ed35a1d534651be7dc7fe476b3151de3
    
    nmin, nmax, delta = utils.get_min_max_iteration(sys.argv,path+ppath)
    nx1 = int(config['PzR.SetMatrixSize_0'])
    ny1 = int(config['PzR.SetMatrixSize_1'])
    rmin = float(config['PzR.SetBounds_0'])*1e4
    rmax = float(config['PzR.SetBounds_1'])*1e4
    Emin = float(config['PzR.SetBounds_2'])
    Emax = float(config['PzR.SetBounds_3'])
    Xmax = float(config['X_Max']) #mkm to wavelength
    Xmin = float(config['X_Min']) #mkm to wavelength
    Ymax = float(config['Y_Max']) #mkm to wavelength
    Ymin = float(config['Y_Min']) #mkm to wavelength
    wl = float(config['Wavelength'])
    dx = float(config['Step_X'])/wl
    dy = float(config['Step_Y'])/wl
    dz = float(config['Step_Z'])/wl
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    xmax = float(config['X_Max'])/wl #mkm
    xmin = float(config['X_Min'])/wl #mkm
    ymax = float(config['Y_Max'])/wl #mkm
    ymin = float(config['Y_Min'])/wl #mkm
    zmax = float(config['Z_Max'])/wl #mkm
    zmin = float(config['Z_Min'])/wl #mkm
#    Mmin = float(config['ElMomentum.SetBounds_2'])
#    Mmax = float(config['ElMomentum.SetBounds_3'])
    print nx1, ny1
    dr = (rmax-rmin)/nx1
    dg = (Emax-Emin)/ny1
    
    ppw = int(config['PeakPower'])*1e-22
    maxe = 3.e11 * math.sqrt(ppw/10.)
    figures = [utils.jz_zpath, utils.neypath, utils.nezpath]
    figures1 = [utils.ez_zpath, utils.ez_xpath]
    cmaps = ['bwr', 'Greens', 'Greens']
    cmaps1 = ['bwr', 'bwr']
    titles = ['Jz_z', 'Electrons(xz)', 'Electrons(xy)']
    titles1 = ['Ez_z', 'Ez_x']
    log = [False, False, True]
    log1 = [False, False]
    mult = [1,1/(2.*dx*dy*dz*1e-12),1/(2.*dx*dy*dz*1e-12)]
    mult1 = [1,1]
    spy = len(figures)
    for i in range(nmin,nmax,delta):
        pfield = utils.bo_file_load(path + ppath,i,nx1,ny1,verbose=1, transpose=1)
        fig = plt.figure(num=None, figsize = (15,20))
        ax = fig.add_subplot(4,3,1)
        mmax = np.amax(pfield)
        mmin = mmax*1e-3
        ax.imshow(pfield, extent = [rmin,rmax, Emin,Emax], vmin=mmin, vmax = mmax, aspect='auto', origin='lower', norm=clr.LogNorm())
        ax.plot([rmin,rmax],[0,0], 'w')
        ax.set_xlim([rmin,rmax])
        ax.set_ylim([Emin/2,Emax/2])
        ax.set_ylabel('$P_z$')
        ax.set_xlabel('$r, \mu m$')
        ax = fig.add_subplot(4,3,2)
        ax_ = utils.axis(rmin,rmax,nx1)
        ax.plot(ax_,np.sum(pfield, axis=0))
        ax = fig.add_subplot(4,3,3)
        ax_ = utils.axis(Emin,Emax,ny1)
        ax.set_xlabel('$r, \mu m$')
        pz = np.sum(pfield, axis=1)
        pz1 = np.sum(pfield[:,0:50], axis=1)
        pz2 = np.sum(pfield[:,0:25], axis=1)
        print 'len', len(pz), np.sum(pz[:len(pz/2)]), np.sum(pz[len(pz)/2:])
#        j_pos, j_neg = calculate_current(pz,dg)
#        print j_pos, j_neg, j_pos - j_neg
        ax.plot(ax_,pz/np.amax(pz))
        ax.plot(ax_,pz1/np.amax(pz1))
        ax.plot(ax_,pz2/np.amax(pz2))
        ax.plot([0, 0], [0, 1], 'k')
        ax.set_title('N: %.1le : %.1le' % (np.sum(pz1[:len(pz1/2)]), np.sum(pz1[len(pz1)/2:])))
        ax.set_yscale('log')
        ax.set_ylim([1e-4, 1])
        ax.set_xlim([-1e-13, 1e-13])
        ax.set_xlabel('$P_z$')
        
        pfield = utils.bo_file_load(path + pspath,i,nx1,ny1,verbose=1, transpose=1)
        ax = fig.add_subplot(4,3,4)
        mmax = np.amax(pfield)
        mmin = mmax*1e-3
        ax.imshow(pfield, extent = [rmin,rmax, Emin,Emax], vmin=mmin, vmax = mmax, aspect='auto', origin='lower', norm=clr.LogNorm())
        ax.plot([rmin,rmax],[0,0], 'w')
        ax.set_xlim([rmin,rmax])
        ax.set_ylim([Emin/2,Emax/2])
        ax.set_ylabel('$P_z$')
        ax.set_xlabel('$r, \mu m$')
        ax = fig.add_subplot(4,3,5)
        ax_ = utils.axis(rmin,rmax,nx1)
        ax.plot(ax_,np.sum(pfield, axis=0))
        ax = fig.add_subplot(4,3,6)
        ax_ = utils.axis(Emin,Emax,ny1)
        ax.set_xlabel('$r, \mu m$')
        pz = np.sum(pfield, axis=1)
        pz1 = np.sum(pfield[:,0:50], axis=1)
        pz2 = np.sum(pfield[:,0:25], axis=1)
        print 'len', len(pz), np.sum(pz[:len(pz/2)]), np.sum(pz[len(pz)/2:])
#        j_pos, j_neg = calculate_current(pz,dg)
#        print j_pos, j_neg, j_pos - j_neg
        ax.plot(ax_,pz/np.amax(pz))
        ax.plot(ax_,pz1/np.amax(pz1))
        ax.plot(ax_,pz2/np.amax(pz2))
        ax.plot([0, 0], [0, 1], 'k')
        ax.set_title('N: %.1le : %.1le' % (np.sum(pz1[:len(pz1/2)]), np.sum(pz1[len(pz1)/2:])))
        ax.set_yscale('log')
        ax.set_ylim([1e-4, 1])
        ax.set_xlim([-1e-13, 1e-13])
        ax.set_xlabel('$P_z$')
        for j in range(spy):
            utils.subplot(fig, i, figures[j],
                          shape = (nx,ny), position = (4,3,j+7),
                          extent = [xmin, xmax, ymin, ymax],
                          cmap = cmaps[j], title = titles[j],
                          colorbar = False, logarithmic=log[j], verbose=0,
                          xlim = [-0.2,0.2], ylim = [-0.2,0.2], ticks = [-0.2, -0.1,0,0.1, 0.2],
                          xlabel = '$x/\lambda$', ylabel = '$y/\lambda$', mult = mult[j], interpolation = 'none')
        for j in range(len(figures1)):
            utils.subplot(fig, i, figures1[j],
                          shape = (nx,ny), position = (4,3,j+10),
                          extent = [xmin, xmax, ymin, ymax],
                          cmap = cmaps1[j], title = titles1[j],
                          colorbar = False, logarithmic=log1[j], verbose=0,
                          xlim = [-0.2,0.2], ylim = [-0.2,0.2], ticks = [-0.2, -0.1,0,0.1, 0.2],
                          xlabel = '$x/\lambda$', ylabel = '$y/\lambda$', mult = mult1[j], interpolation = 'none')
        picname = picspath + '/' + "pzr%06d.png" % (i,)
        plt.savefig(picname)
        
    
    

if __name__ == '__main__':
    main()
