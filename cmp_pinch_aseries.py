#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils
import numpy as np

def main():
  
    dirs = ['27pw_x8r', '27pw_x8r','27pw_x8r', '27pw_x8r', '27pw_x8r']
    iterations = [3000, 3020, 3040, 3050, 3060]
    configs = []
    numdirs = len(dirs)
    
    for i in range(numdirs):
        configs.append(utils.get_config(dirs[i] + "/ParsedInput.txt"))
    
    n = numdirs + 1
    
    picspath = 'pics'
        
    xmin = []
    xmax = []
    ymin = []
    ymax = []
    zmin = []
    zmax = []
    mult = []
    nx = []
    ny = []
    nz = []
    power = []
    for k in range(numdirs):
        if 'resize' in configs[k].keys():
            factor = int(configs[k]['resize'])
        else:
            factor = 1
        if 'rescale' in configs[k].keys():
            rescale = int(configs[k]['rescale'])
        else:
            rescale = 1
        print rescale
        wl = float(configs[k]['Wavelength'])
        Xmax = float(configs[k]['X_Max'])/wl/rescale
        Xmin = float(configs[k]['X_Min'])/wl/rescale
        Ymax = float(configs[k]['Y_Max'])/wl/rescale
        Ymin = float(configs[k]['Y_Min'])/wl/rescale
        Zmax = float(configs[k]['Z_Max'])/wl/rescale
        Zmin = float(configs[k]['Z_Min'])/wl/rescale
        xmax.append(Xmax) #mkm
        xmin.append(Xmin) #mkm
        ymax.append(Ymax) #mkm
        ymin.append(Ymin) #mkm
        zmax.append(Zmax) #mkm
        zmin.append(Zmin) #mkm
        nx.append(int(configs[k]['MatrixSize_X']))
        ny.append(int(configs[k]['MatrixSize_Y']))
        nz.append(int(configs[k]['MatrixSize_Z']))
        power.append(int(configs[k]['PeakPowerPW']))
        dx = (Xmax-Xmin)/int(configs[k]['MatrixSize_X'])
        dy = (Ymax-Ymin)/int(configs[k]['MatrixSize_Y'])
        dz = (Zmax-Zmin)/int(configs[k]['MatrixSize_Z'])
        print dx,dy,dz
        if 'r' not in dirs[k]:
            dv = 2.*dx*dy*dz*wl*wl*wl*factor
        else:
            dv = 2.*dx*dy*dz*wl*wl*wl*rescale
        mult.append(1.)  
        mult.append(1./dv)
        step = float(configs[k]['TimeStep'])*1e15*float(configs[k]['BOIterationPass'])
        omega = float(configs[k]['Omega'])
        T = 2 * math.pi/omega
        nt = int(T*1e15/step)
        print nt

    for t_ in iterations:
        print (t_ - 3000)*step/(T*1e15)
    #figures = [utils.ezpath, utils.bzpath, utils.nezpath, utils.npzpath, utils.nphzpath, utils.nizpath]
    #cmaps = ['Reds', 'Reds', 'Greens','Greens', 'Blues', 'Greens']
    #titles = ['Electric field %d pw', 'Magnetic field %d pw', 'Electrons %d pw','Positrons %d pw', 'Photons %d pw', 'Ions %d pw']
    #log = [False, False, True, True, True, True]
    #mult = [1,1,mult[0],mult[0],mult[0], mult[0]]
    figures = [utils.bypath, utils.neypath]
    cmaps = ['Reds', 'Greens']
    titles = [u'x1', u'x2', u'x4', u'x8', u'x16',u'x32', '','','', '', '', '']
    log = [False, True]
#    mult = [1, mult[0]]
    labels = [u'$(а)$', u'$(б)$', u'$(в)$', u'$(г)$', u'$(д)$', u'$(е)$', u'$(ж)$', u'$(з)$', u'$(и)$', u'$(к)$']
#    values  = [1, 2.71/3.0, 3.04/(3.0*math.sqrt(2.)), 3.54/6., 1, 1, 1, 1]
#    for i in range(4):
#        print 7.2/(values[i]*values[i])
#    n0 = [703, 0, 0] # 675
#    n0 = [686, 0, 0] # 680
#    n0 = [1030, 1090]
    n0 = [0] * numdirs
    spx = numdirs
    spy = len(figures)
    nmin = 201
    nmax = 202
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})

    fig = plt.figure(num=None, figsize=(5.*float(spx)/float(spy), 5.), dpi=256)
    mp.rcParams.update({'font.size': 12})
        
    for k in range(spx):
        for j in range(spy):
            figure = figures[j]
            cmap = cmaps[j]
            if j == 0 and k == 0:
                ylabel = '$z/\lambda$'
                #                    label = '$(i)$'
            elif j == 1 and k == 0:
                ylabel = '$z/\lambda$'
                #                    label = '$(j)$'
            elif j == 0 and k == 1:
                ylabel = '$z/\lambda$'
                #                    label = '$(k)$'
            elif j == 1 and k == 1:
                ylabel = '$z/\lambda$'
                #                    label = '$(l)$'
            ax = utils.subplot(fig, iterations[k], dirs[k]+'/'+figure,
                               shape = (nx[k],ny[k]), position = (spy,spx,k+1+j*spx),
                               extent = [xmin[k], xmax[k], ymin[k], ymax[k]], ratio = 1e-3,
                               cmap = cmap, fontsize = 10, normalize = False,
                               vmax = 1, label = labels[j + k*spy],
                               colorbar = True, logarithmic=log[j], verbose=0, transpose=1,
                               xlim = [-0.25,0.25], ylim = [-0.25,0.25], ticks = [-0.25, 0, 0.25], 
                               xlabel = '$x/\lambda$', ylabel = ylabel, mult = mult[j + k*spy])
            ax.text(-1.8*0.25,1.5*0.25, labels[k+j*spx], fontsize = 12)
    picname = picspath + '/' + "cmp_pinch8_n.png"
    plt.tight_layout()
    plt.savefig(picname, dpi=512)
    plt.close()

    
if __name__ == '__main__':
    main()
