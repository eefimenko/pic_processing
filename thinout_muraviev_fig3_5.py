#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import numpy as np
import os
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

def read_data(filename):
    f = open(filename)
    ax = []
    data = []
    for line in f:
        tmp = line.split()
        ax.append(float(tmp[0]))
        data.append(float(tmp[1]))
    return np.array(ax), np.array(data)
        

def main():
    mp.rcParams.update({'font.size': 12})
    fontsize = 12
    datapath = '/home/evgeny/Dropbox/pinch_thinout/DataForPics'
    picspath = '/home/evgeny/Dropbox/pinch_thinout/'
    fig2path = 'Fig2+4'
    fig3path = 'Fig3+5'

    fig1 = plt.figure(num=None, figsize = (5,3.5))
    ax1 = fig1.add_subplot(1,1,1)
    
    fig2 = plt.figure(num=None, figsize = (5,3.5))
    ax2 = fig2.add_subplot(1,1,1)
    
    ax1.set_ylim([-5, 0.1])
    ax2.set_ylim([-70, 0.1])
    
    ax1.set_xlim([0, 350])
    ax2.set_xlim([0, 1000])
    
    #axins = zoomed_inset_axes(ax1, 2.5, loc= 'upper center')  
    #axins.set_yscale('log')
    #axins.set_xlim([0.75, 1.5])
    #axins.set_ylim([3e-5, 3e-4])
    
    #mark_inset(ax1, axins, loc1=2, loc2=3, fc="none", ec="0.5")

    #axins1 = zoomed_inset_axes(ax2, 2.5, loc= 'upper right')  
    #axins1.set_yscale('log')
    #axins1.set_xlim([0.75, 1.5])
    #axins1.set_ylim([8e-4, 9e-3])
    
    #mark_inset(ax2, axins1, loc1=1, loc2=3, fc="none", ec="0.5")
    msize = 3
    mewidth = 0.5
    dashes = [3,2]
    lw = 0.5
    
    ax_simple, data_simple = read_data(os.path.join(datapath, fig3path, 'simple.txt'))
    ax1.plot(ax_simple[:-1], data_simple[:-1], linewidth = lw, dashes = [4,3], markersize = msize, marker = 'D', color = 'gold', fillstyle = 'none', label = 'simple')
    ax2.plot(ax_simple, data_simple, linewidth = lw, dashes = dashes, markersize = msize, marker = 'D', color = 'gold', fillstyle = 'none', label = 'simple')

    ax_leveling, data_leveling = read_data(os.path.join(datapath, fig3path, 'leveling.txt'))
    ax1.plot(ax_leveling, data_leveling, linewidth = lw, dashes = dashes, markersize = msize, marker = 'D', color = 'lime', fillstyle = 'none', label = 'leveling')
    ax2.plot(ax_leveling, data_leveling, linewidth = lw, dashes = dashes, markersize = msize, marker = 'D', color = 'lime', fillstyle = 'none', label = 'leveling')

    ax_globalLev, data_globalLev = read_data(os.path.join(datapath, fig3path, 'globalLev.txt'))
    ax1.plot(ax_globalLev[:-1], data_globalLev[:-1], linewidth = lw, dashes = dashes, markeredgewidth=mewidth, markersize = msize, marker = 'D', color = 'g', fillstyle = 'none', label = 'globalLev')
    ax2.plot(ax_globalLev, data_globalLev, linewidth = lw, dashes = dashes, markeredgewidth=mewidth, markersize = msize, marker = 'D', color = 'g', fillstyle = 'none', label = 'globalLev')

    ax_numberT, data_numberT = read_data(os.path.join(datapath, fig3path, 'numberT.txt'))
    ax1.plot(ax_numberT, data_numberT, linewidth = lw, dashes = dashes, markeredgewidth=mewidth, markersize = msize, marker = 'D', color = 'm', fillstyle = 'none', label = 'numberT')
    ax2.plot(ax_numberT, data_numberT, linewidth = lw, dashes = dashes, markeredgewidth=mewidth, markersize = msize, marker = 'D', color = 'm', fillstyle = 'none', label = 'numberT')
    
    ax_energyT, data_energyT = read_data(os.path.join(datapath, fig3path, 'energyT.txt'))
    ax1.plot(ax_energyT, data_energyT, linewidth = lw, dashes = dashes, markeredgewidth=mewidth, markersize = msize, marker = 'D', color = 'b', fillstyle = 'none', label = 'energyT')
    ax2.plot(ax_energyT, data_energyT, linewidth = lw, dashes = dashes, markeredgewidth=mewidth, markersize = msize, marker = 'D', color = 'b', fillstyle = 'none', label = 'energyT')

    ax_conserve, data_conserve = read_data(os.path.join(datapath, fig3path, 'conserve.txt'))
    ax1.plot(ax_conserve, data_conserve, linewidth = lw, dashes = dashes, markeredgewidth=mewidth, markersize = msize, marker = 'D', color = 'r', fillstyle = 'none', label = 'conserv')
    ax2.plot(ax_conserve, data_conserve, linewidth = lw, dashes = dashes, markeredgewidth=mewidth, markersize = msize, marker = 'D', color = 'r', fillstyle = 'none', label = 'conserv')

    ax_merge, data_merge = read_data(os.path.join(datapath, fig3path, 'merge.txt'))
    ax2.plot(ax_merge, data_merge, linewidth = lw, dashes = [4, 3], markeredgewidth=mewidth, markersize = msize, marker = 'D', color = 'c', fillstyle = 'none', label = 'merge')

    ax_mergeAv, data_mergeAv = read_data(os.path.join(datapath, fig3path, 'mergeAv.txt'))
    ax2.plot(ax_mergeAv, data_mergeAv, linewidth = lw, dashes = dashes, markeredgewidth=mewidth, markersize = msize, marker = 'D', color = 'orange', fillstyle = 'none', label = 'mergeAv')

    
    
    ax1.plot([1, 340], [0, -4.1], color = 'k', linewidth = lw, label = 'linear fit')
    ax2.plot([1, 1000], [0, -12], color = 'k', linewidth = lw, label = 'linear fit')
    
    ax1.legend(loc = 'lower left', fontsize = fontsize-2, frameon = False)
    ax2.legend(loc = 'lower right', fontsize = fontsize-2, frameon = False)
    ax1.set_xlabel('k')
    ax2.set_xlabel('k')
    ax1.set_ylabel('$\Delta$T, %')
    ax2.set_ylabel('$\Delta$T, %')
    ax1.text(-50, -0.1, '(b)')
    ax2.text(-150, -1.4, '(b)')
    #axins.set_xticks([])
    #axins.set_yticks([])
    #axins.set_yticklabels([])
    #axins1.set_xticks([])
    #axins1.set_yticks([])
    #axins1.set_yticklabels([])
    
    name1 = 'dtemp_1000_350_nomerge_new'
    name2 = 'dtemp_1000_1000_merge_new'
    picname1 = picspath + '/' + name1 + ".png"
    picname2 = picspath + '/' + name2 + ".png"
    #plt.legend(loc = 'upper left', fontsize = 10)
    fig1.tight_layout()
    fig2.tight_layout()
    fig1.savefig(picname1, dpi = 256)
    fig2.savefig(picname2, dpi = 256)
      
#    plt.show()

if __name__ == '__main__':
    main()
