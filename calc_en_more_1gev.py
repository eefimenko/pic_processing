#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_file(filename, energy):
    f = open(filename, 'r')
    axis = []
    spectra = []
    ratio = 0
    for line in f:
        tmp = line.split()
        axis.append(float(tmp[0]))
        spectra.append(float(tmp[1]))
    summ = sum(spectra)*(axis[1]-axis[0])
    de = axis[1]-axis[0]
    summ1 = 0
    for i in range(len(spectra)):
        summ1 += spectra[i]/(i+0.425)
#    print 'Summ1 ', summ1
    for i in range(len(spectra)):
        spectra[i] /= summ
    summ = sum(spectra)*(axis[1]-axis[0])
    for i in range(len(spectra)):
        if (axis[1]-axis[0])*i > energy:
            ratio += spectra[i]*(axis[1]-axis[0])
#    print summ
    return axis, spectra,ratio

def main():
    n = len(sys.argv) - 1
    axis = []
    spectra = []
    power = []
    ratio = []
    ticks = []
    ticklabels = []
    fig = plt.figure()
    ax1 = fig.add_subplot(2,1,1)
    ax2 = fig.add_subplot(2,1,2)
    for i in range(n):
        path = sys.argv[i+1]
        pwr = int(path.strip('/').split('_')[2])
#        print path + '/sp_sphere_%d.dat'%pwr
        axis_,spectra_, ratio_ = read_file(path + '/sp_sphere_%d.dat'%(pwr), 1.17)
        spectra.append(spectra_)
        axis.append(axis_)
        p, = ax1.plot(axis_, spectra_, label = pwr)
        ticks.append(pwr)
        ticklabels.append(str(pwr))
        
#        print ratio_
        ratio.append(ratio_)
        power.append(pwr)
    ax1.set_yscale('log')
    ax2.plot(power, ratio)
    ax2.plot([power[0], power[-1]], [0.01, 0.01])
    ax2.set_xticks(ticks)
    ax2.set_xticklabels(ticklabels)
    plt.show()

if __name__ == '__main__':
    main()
