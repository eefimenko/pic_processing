#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import sys
import math
import utils

def main():
    power = [9., 12., 15., 20., 25., 30., 35.]
    ast = [0.86, 0.78, 0.73, 0.51, 0.19, 0.13, 0.07]
    nst = [0.24, 0.36, 0.4, 0.7, 21., 19.2, 18.]
    epower = [3, 5.5, 8., 15., 17.5, 23., 28.]
    max_en = [1.38, 1.53, 1.55, 1., 0.89, 0.96, 0.95]
    ax_ = [22, 22]
    az_ = [0.1, 3e11]
    az1_ = [0, 1]
    
    e_p = []
    for i in range(len(power)):
        e_p.append(3.e11*math.sqrt(power[i]/10.)*ast[i])
    fig = plt.figure(num=None, figsize = (20, 7))
    plt.rcParams.update({'font.size': 20})
    ax2 = fig.add_subplot(1,2,1)
    nl, = ax2.plot(power, max_en, linestyle = ':', color = 'b', linewidth = 3, marker = '.', markersize = 15)
    ax2.set_ylabel(u'Максимальная энергия фотонов, ГэВ')
    ax2.set_xlabel(u'Мощность, ПВт')
#    ax2.set_ylabel(u'Max photon energy, GeV')
#    ax2.set_xlabel(u'Power, PW')
    ax1 = ax2.twinx()
    al, = ax1.plot(power, e_p, linestyle = ':', color = 'r', linewidth = 3, marker = '.', markersize = 15)
    al1, = ax1.plot(ax_, az_, color = 'g')
#    ax1.set_ylabel(u'Stationary field $E_z$, CGSE')
#    ax1.set_xlabel(u'Power, PW')
    ax1.set_ylabel(u'$E_z$, СГС')
    ax1.set_xlabel(u'Мощность, ПВт')
    ax2.set_ylim([0, 2])
#    ax2.set_yscale('log')
    plt.legend([al, nl],['$E_z$', u'Макс. энергия фотонов'], loc = 'lower left')
    ax1 = fig.add_subplot(1,2,2)
    al, = ax1.plot(power, ast, linestyle = ':', color = 'r', linewidth = 3, marker = '.', markersize = 15)
    ax1.set_ylabel(u'$E_z/a_0$')
    al11, = ax1.plot(ax_, az1_, color = 'g')
#    ax1.set_xlabel(u'Power, PW')
    ax1.set_xlabel(u'Мощность, ПВт')
    ax2 = ax1.twinx()
    nl, = ax2.plot(power, nst, linestyle = ':', color = 'b', linewidth = 3, marker = '.', markersize = 15)
    ax2.set_ylabel(u'$N_e/(N_{cr} a_0)$')
#    ax2.set_xlabel(u'Power, PW')
    ax2.set_xlabel(u'Мощность, ПВт')
#    ax2.set_ylim([0, 2])
#    ax2.set_yscale('log')
    plt.legend([al, nl],['$E_z/a_0$', '$N_e/(N_cr a_0)$'], loc = 'lower left')
    plt.tight_layout()
    plt.savefig('st.png')
   
#    plt.show()

if __name__ == '__main__':
    main()
