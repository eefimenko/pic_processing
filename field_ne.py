#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
from pylab import *


Xmax = 4
Xmin = -4
Ymax = 4
Ymin = -4
nx = 512
ny = 512
step = 8./nx

def read_field(file,nx,ny):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def find_max(file):
    max = 0
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    array = tmp[0]
    for p in array:
        if p > max:
            max = p
    return max

def find_max_from_all_fields(expath, eypath, ezpath, bxpath, bypath, bzpath, i):
    max = 0
    name = expath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = eypath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = ezpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    return max

def find_max_from_all_particles(nexpath, neypath, nezpath, npxpath, npypath, npzpath, i):
    max = 0
    name = nexpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = neypath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = nezpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = npxpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = npypath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = npzpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    return max

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append((i-n/2)*step)
    return axis

def create_subplot(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max):
    ax = fig.add_subplot(spx,spy,spi)
    name = path + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny)
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], vmin = v_min, vmax = v_max, norm=clr.LogNorm())
    return surf

def create_combined_subplot(fig,i,path1,path2,spx,spy,spi, v_minf, v_maxf, v_minn, v_maxn):
    global step, nx, ny, Xmin, Xmax, Ymin, Ymax
    
    x = create_axis(nx,step)
    y = create_axis(ny,step)
    X,Y = np.meshgrid(x,y);

    # generate the colors for your colormap
    color1 = colorConverter.to_rgba('white')
    color2 = colorConverter.to_rgba('red')
    cmap1 = mp.colors.LinearSegmentedColormap.from_list('my_cmap',['white','gren'],256)
    cmap2 = mp.colors.LinearSegmentedColormap.from_list('my_cmap2',[color1,color2],256)
    
    cmap2._init() # create the _lut array, with rgba values
    
    # create your alpha array and fill the colormap with them.
    # here it is progressive, but you can create whathever you want
    alphas = np.linspace(0, 0.8, cmap2.N+3)
    cmap2._lut[:,-1] = alphas

    ax = fig.add_subplot(spx,spy,spi)
    name = path1 + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny)
    surf = ax.imshow(field, cmap=cmap2,extent=[Xmin, Xmax, Ymin, Ymax], vmin = v_minf, vmax = v_maxf, norm=clr.LogNorm())
    plt.colorbar(surf, orientation  = 'vertical')
    hold(True)
    name = path2 + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny)
    surf = ax.imshow(field,  alpha=.8,  cmap=cmap1, extent=[Xmin, Xmax, Ymin, Ymax], vmin = v_minn, vmax = v_maxn, norm=clr.LogNorm())
    plt.colorbar(surf, orientation  = 'horizontal')
    return surf

def main():
    
    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    picspath = 'pics'
    global step, nx, ny, Xmin, Xmax, Ymin, Ymax
    
    maxn = 0
    maxf = 0
#    for i in range(nmax):
#    i = 20
    nmax = 80
    for i in range(nmax):
        print "%06d.txt" % (i,)
        maxf = 0
        maxn = 0
        tmpf = find_max_from_all_fields(expath, eypath, ezpath, bxpath, bypath, bzpath, i)
        if tmpf > maxf:
            maxf = tmpf
        tmpn = find_max_from_all_particles(nexpath, neypath, nezpath, npxpath, npypath, npzpath, i)
        if tmpn > maxn:
            maxn = tmpn
    print maxn, maxf

    nmax = 80
    for i in range(nmax):
        print "tmp%06d.png" % (i,)
        fig = plt.figure(num=None, figsize=(10, 10), dpi=256)
        mp.rcParams.update({'font.size': 8})
        s = create_combined_subplot(fig,i,expath,nexpath,1,1,1, tmpf/1e4, tmpf, 1, tmpn)
#        s = create_combined_subplot(fig,i,expath,nexpath,1,1,1, 1, tmpf, 1, tmpn)
    #    plt.colorbar(s, orientation  = 'vertical')
        picname = picspath + '/' + "tmp%06d.png" % (i,)
        plt.savefig(picname)
        plt.close()

    nmax = 0
    for i in range(nmax):
        print "pic%06d.png" % (i,)
        fig = plt.figure(num=None, figsize=(40, 40), dpi=256, frameon=False)
        mp.rcParams.update({'font.size': 16})
        
        s = create_subplot(fig,i,expath,nx,ny,3,3,1, Xmin, Xmax, Ymin, Ymax, tmpf/1e4, tmpf)
        plt.colorbar(s, orientation  = 'vertical')
        
        s = create_subplot(fig,i,eypath,nx,ny,3,3,4, Xmin, Xmax, Ymin, Ymax, tmpf/1e4, tmpf)
        plt.colorbar(s, orientation  = 'vertical')
        
        s = create_subplot(fig,i,ezpath,nx,ny,3,3,7, Xmin, Xmax, Ymin, Ymax, tmpf/1e4, tmpf)
        plt.colorbar(s, orientation  = 'vertical')
        
        s = create_subplot(fig,i,nexpath,nx,ny,3,3,2, Xmin, Xmax, Ymin, Ymax, tmpn/1e4, tmpn)
        plt.colorbar(s, orientation  = 'vertical')
        
        s = create_subplot(fig,i,neypath,nx,ny,3,3,5, Xmin, Xmax, Ymin, Ymax, tmpn/1e4, tmpn)
        plt.colorbar(s, orientation  = 'vertical')
        
        s = create_subplot(fig,i,nezpath,nx,ny,3,3,8, Xmin, Xmax, Ymin, Ymax, tmpn/1e4, tmpn)
        plt.colorbar(s, orientation  = 'vertical')
        
        s = create_subplot(fig,i,npxpath,nx,ny,3,3,3, Xmin, Xmax, Ymin, Ymax, tmpn/1e4, tmpn)
        plt.colorbar(s, orientation  = 'vertical')
        
        s = create_subplot(fig,i,npypath,nx,ny,3,3,6, Xmin, Xmax, Ymin, Ymax, tmpn/1e4, tmpn)
        plt.colorbar(s, orientation  = 'vertical')
        
        s = create_subplot(fig,i,npzpath,nx,ny,3,3,9, Xmin, Xmax, Ymin, Ymax, tmpn/1e4, tmpn)
        plt.colorbar(s, orientation  = 'vertical')
        picname = picspath + '/' + "pic%06d.png" % (i,)
        plt.savefig(picname)
        plt.close()
#        plt.show()
if __name__ == '__main__':
    main()
