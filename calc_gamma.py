#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np

def read_concentration(file, dv):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    e = tmp[0]
    for j in range(len(e)):
            e[j] = e[j]/((j+0.5)*dv)
    return e

def read_field(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def find_max_photon_energy_1percent(array, step):
    summ = 0
    for j, e in enumerate(array):
        summ += j*e
    part_sum = 0
    N = len(array)
    for i in range(N):
        if array[N-i-1] > 0:
            part_sum += array[N-i-1]*(N - i - 1)
            
            if part_sum > 0.01 * summ:
                break
    
    return (N-i-1)*step


def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def create_pulse(n,step,amp, tp, delay):
    dt = step
    ans = []
#    tp = 30
    tau = tp/1.1437
#    delay = 23.1099984185576458468467817
    for i in range(n):
        t = i*dt-delay
        if t > 0 and t < math.pi * tau:
            tmp = math.sin(t/tau)
            f = amp*tmp*tmp
        else:
            f = 0.
        ans.append(f*f)
    return ans

def read_field2d(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def normList(L, normalizeTo=1):
    '''normalize values of a list to make its max = normalizeTo'''

    vMax = max(L)
    return [ x/(vMax*1.0)*normalizeTo for x in L]

def read_energy(file):
    f = open(file, 'r')
    tmp = [[float(x)+1e-9 for x in line.split()] for line in f]
    f.close()
    return tmp[0]

def read_diag(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def sum_array(array):
    res = [0] * len(array[0])
    for i in range(len(array[0])):
        for j in range(len(array)):
            res[i] += array[j][i]
    return res

def find_max_photon_energy_1percent(array, step):
    summ = 0
    nmax = 0
    nmax1p = 0
    for j, e in enumerate(array):
        summ += j*e
    part_sum = 0
    N = len(array)
    for i in range(N):
        if array[N-i-1] > 0:
            part_sum += array[N-i-1]*(N - i - 1)
            
            if part_sum > 0.01 * summ:
                nmax1p = N-i-1
                break
    for i in range(N):
        if array[N-i-1] > 0:
            nmax = N-i-1
            break
    return nmax1p*step, nmax*step

def main():

    config = utils.get_config("ParsedInput.txt")
#    duration = float(config['Duration']) #s
    delay = float(config['delay']) #s
    BOIterationPass = float(config['BOIterationPass'])
    c = 2.99792e+10
    pi = 3.14159
    wl = float(config['Wavelength'])
    coeff = 10./(3.3e11*3.3e11)*0.97399*(wl/0.8e-4)*(wl/0.8e-4)
    dx1 = float(config['Step_X'])
    dy1 = float(config['Step_Y'])
   
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    dmy = int(0.35*wl/dy1)
#    tp = duration*1e15
#    tdelay = delay*1e15
#    nx = 256
#    ny = 256
    row = -1

    stepx = 5.656854249492380319955242562457442546/nx
    dv = 2. * math.pi * stepx * stepx * 0.8e-4 * 1e-8

    step = float(config['TimeStep'])*1e15*BOIterationPass
    dt = float(config['TimeStep'])
    omega = float(config['Omega'])
    peakpower = float(config['PeakPower'])*1e-7*1e-15
    r = float(config['R'])
    Ne = float(config['Ne'])
    v = 4./3.*math.pi*r*r*r
    nemax = v*Ne
    print peakpower, r, nemax
    ev = float(config['eV'])
    expath = 'data/E2x'
    bxpath = 'data/B2x'
    eypath = 'data/E2y'
    bypath = 'data/B2y'
    ezpath = 'data/E2z'
    bzpath = 'data/B2z'
    denspath = 'data/Electron2Dz'
    path='statdata'
    elpath='/el/'
    elmaxpath='/el_max/'
    posmaxpath='/pos_max/'
    phmaxpath='/ph_max/'
    
    dpath = 'data/diagPh/'

    nepath = 'data/NeTrap'
    nppath = 'data/NposTrap'
    picspath = 'pics'
    concpath = 'data/ne'
    maxf = 0.
    maxn = 0.
    nmax = 160
    T = 2 * math.pi/omega
    time = dt/T
  
    Ne = []
    Ne_max = []
    Npos = []
    power = []
    nmin,nmax = utils.find_min_max_from_directory(ezpath, bzpath)
    print nmin, nmax
    nmin = 160
    eprev = 0
    powerfile = open('power.dat', 'w')
    energyfile = open('energy.dat', 'w')
    energymfile = open('energym.dat', 'w')
    nefile = open('ne.dat', 'w')
    dndtfile = open('dndt.dat', 'w')
    concfile = open('conc.dat', 'w')
#    nmax = 80
    print nmax
    
    power_t = []
    energy_t = []
    energym_t = []
    ne_t = []
    dndt_t = []
    conc_t = []
    e0 = 0
    n0 = 0
    nf = 0
    i_0 = 0
    i_f = nmax

    nx1 = int(config['diagPh.SetMatrixSize_0'])
    ny1 = int(config['diagPh.SetMatrixSize_1'])
    Emax = float(config['diagPh.SetBounds_1'])/ev*1e-9
    de = Emax/nx1
    print Emax
#    peakpower = 20
    for i in range(nmin,nmax):
        print "%06d.txt" % (i,)
#        ezname = ezpath + '/' + "%06d.txt" % (i,)
#        bzname = bzpath + '/' + "%06d.txt" % (i,)
#        dname = dpath + '/' + "%06d.txt" % (i,)
#        print nx,ny
        read, ez = utils.bo_file_load(utils.ezpath,i,nx,ny)
        read, bz = utils.bo_file_load(utils.bzpath,i,nx,ny)
#        field = read_diag(dname,nx1,ny1)
        print(nx, ny)
        ezv = bz[nx/2][ny/2] # corrected for mdipole wave
        bzv = ez[nx/2][ny/2-dmy]*1.53 # corrected for mdipole wave
        tmp = (ezv*ezv + bzv*bzv)*coeff

#        sum1 = sum_array(field)
#        axis = create_axis(len(sum1), de)
        
#        if max(sum1) > 0:
#            fig = plt.figure(num=None)
#            ax = fig.add_subplot(1,1,1)
#            p1, = ax.plot(axis, sum1, 'r')
#            ax.set_yscale('log') 
#            picname = picspath + '/' + 'sp%06d.png' % (i,)
#            plt.savefig(picname)
#            plt.close()
#        maxen,maxen0 = find_max_photon_energy_1percent(sum1, de)
#        print maxen, maxen0
#        nename = nepath + '/' + "%06d.txt" % (i,)
        e = np.sum(utils.bo_file_load(utils.netpath,i,nx))
       
        if i == 0:
            e0 = e
        if tmp < 1.1*peakpower:
            power.append(tmp)
            Ne.append(e)
        else:
            power.append(0)
            Ne.append(0)

        enext = e
#        if enext != eprev:
#            dndt = (enext - e0)/((enext - eprev)/step)
#        else:
#            dndt = 0.
#        dndt_t.append(dndt)
        if tmp < 1.1*peakpower:
#            dndtfile.write(str(dndt) + '\n')
            powerfile.write(str(tmp)+'\n')
#            energyfile.write(str(maxen)+'\n')
#            energymfile.write(str(maxen0)+'\n')
            nefile.write(str(enext)+'\n')
#            concfile.write(str(conctmp) + '\n')
#            conc_t.append(conctmp)
            power_t.append(tmp)
#            energy_t.append(maxen)
#            energym_t.append(maxen0)
            ne_t.append(enext)
        else:
            power_t.append(0)
#            energy_t.append(0)
#            energym_t.append(0)
            ne_t.append(0)
                    
#print tmp, 0.98*peakpower
        print tmp, n0, 0.99*peakpower
        if n0 == 0 and tmp > 0.99*peakpower:
            n0 = enext
            i_0 = i
            print 'here = ', n0, tmp, i
            
        if n0 != 0 and nf == 0 and tmp < 0.98*peakpower:
            nf = enext
            i_f = i
            print 'here1 = ', n0, tmp, i
            break
        eprev = enext
        power_prev = tmp
    if nf == 0:
	nf = enext
        i_f = i	 
    powerfile.close()
    energyfile.close()
    energymfile.close()
    nefile.close()
    dndtfile.close()
    concfile.close()

    fig, ax1 =  plt.subplots()
    ppp, = ax1.plot(power_t, ne_t)
    ax1.set_yscale('log')
    plt.savefig('pics/nn.png')
    plt.close()

    print '------'
    print 'nf,n0 =',nf, n0
    print 'i_f,i_0 =',i_f, i_0
    if nf == 0:
        nf = n0
    gamma = math.log(nf/n0)/((i_f - i_0)*step)*T*1e15
    gfile = open('gamma.dat', 'w')
    gfile.write(str(gamma))
    gfile.close()
#    nmfile = open('ntrappedmax.dat', 'w')
#    nmfile.write(str(nf))
#    nmfile.close()
#    phfile = open('phmax1p.dat', 'w')
#    phfile.write(str(max(energy_t)))
#    phfile.close()

if __name__ == '__main__':
    main()
