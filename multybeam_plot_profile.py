#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import os
import utils


def main():
    print('Here')
    path = sys.argv[1]
    iteration = int(sys.argv[2])

    config = utils.get_config(os.path.join(path, "ParsedInput.txt"))
    wl = float(config['Wavelength'])
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    xmax = float(config['X_Max'])/wl
    xmin = float(config['X_Min'])/wl
    ymax = float(config['Y_Max'])/wl
    ymin = float(config['Y_Min'])/wl

    read, energy = utils.bo_file_load(os.path.join(path, 'BasicOutput/data/XZ2D'), iteration, nx, ny, verbose=1)
    print(energy.shape, ny//2)
    
    fig = plt.figure(figsize=(10, 7))
    ax1 = fig.add_subplot(1, 2, 1)
    ax1.imshow(energy, extent=[xmin, xmax, ymin, ymax], origin='lower')
    ax2 = fig.add_subplot(1, 2, 2)
    ax2.plot(energy[:, ny//2])
    ax2.axvline(x=72)

    plt.show()


if __name__ == '__main__':
    main()
