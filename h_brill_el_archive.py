#!/usr/bin/python
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import utils
import sys
import os
from pylab import *

def convert_energy_to_number(array,step):
    for i in range(array.shape[0]):
        array[i] /= (i+0.425)*step
    return array

def sum_energy(array,energy,step):
    nmin = int(energy/step)
    summ = 0
    summ_energy = 0
    for i in range(nmin, len(array)):
        summ += array[i]
        summ_energy += array[i]*(i + 0.425)*step
    return summ, summ_energy

def sum_energy_line(array,energy,step,width=0.001):
    fraction = energy*width/step
    pos = int(energy/step)
    # print width, pos, fraction
    # print array[pos], array[pos+1], array[pos-1]
    summ = array[pos]*fraction
    summ_energy = array[pos]*(pos + 0.425)*step*fraction
    return summ, summ_energy

def find_max_energy_sph(array,step):
    n = len(array)
    full = 0
    imax = 0
    summ = 0
    nfull = 0
    av_en = 0
    n_av = 0
    for i in range(n):
        nfull += array[i]
        full += array[i]*step*(i+0.5)
    
    if nfull > 0:
        av_en = full/nfull
        n_av = int(av_en/step)

    for i in range(1,n):
        summ = summ + array[-i]*(n-i+0.5)*step
        if summ > 0.01*full:
            imax = i
            break
    if imax == 0:
        max_en = 0
    else:
        max_en = (n-imax+0.5) * step

    return av_en, max_en, array[n_av], array[-imax]    

def sum_phi(array):
    nx = len(array)
    ny = len(array[0])
    res = [0] * nx
    i_05 = 0
    for i in range(nx):
        for j in range(ny):
            res[i] = res[i] + array[i][j]
    m = max(res)
    if m > 0:
        for i in range(nx):
            res[i] = res[i]/m
    return res

def main():
    if len(sys.argv) == 2:
        boundary = float(sys.argv[1])
    else:
        boundary = 2
    print 'Boundary value = %.2f GeV' % (boundary)
    
    path = './'
    picspath = 'pics'

    config = utils.get_config(path + "/ParsedInput.txt")
    ev = float(config['eV'])
    phi = int(config['QEDstatistics.OutputN_phi'])
    theta = int(config['QEDstatistics.OutputN_theta'])
    dphi = 2*math.pi/phi
    dtheta = math.pi/theta
    wl = float(config['Wavelength'])

    S = 2 * math.pi * 0.1 * 0.1 * wl * wl * 100 # square of source mm^2, r = 0.1 lambda 
    ds = 1e6
    ds1 = 2 * math.pi * (1 - math.cos(0.05))*1e6
    ds2 = 2 * math.pi * (1 - math.cos(0.23))*1e6
    ds3 = 2 * math.pi * (1 - math.cos(0.42))*1e6
    emin = float(config['QEDstatistics.Emin'])
    emax = float(config['QEDstatistics.Emax'])
    ne = int(config['QEDstatistics.OutputN_E'])
    nit = int(config['QEDstatistics.OutputIterStep'])
    dt = float(config['TimeStep'])*nit
    omega = float(config['Omega'])
    ppw = int(config['PeakPower'])
    duration = float(config.get('Duration',15))

    nx1 = int(config['MatrixSize_X'])
    ny1 = int(config['MatrixSize_Y'])
    T = 2 * math.pi/omega
    nnn = int(T/dt)
    de = emax*1e-9/ev/ne #GeV
    archive = os.path.join('BasicOutput', 'data.zip')
    
    print 'Read electric field in the center vs time'
    #ez_t = utils.ts(path, utils.ezpath, name='ez',
    #                tstype='center', shape = (nx1,ny1), verbose=True)
    
    print 'Read electron spectra'
    #nmin,nmax = utils.find_min_max_from_directory(path + utils.angspsphpath,
    #                                              path + utils.enspsphpath)
    nmin = 0
    nmax = 1170
    ez_t = np.zeros(nmax)# !
    en_t = np.zeros(nmax)
    br_full = np.zeros(nmax)
    br_100mev = np.zeros(nmax)
    br_1Gev = np.zeros(nmax)
    en_full = np.zeros(nmax)
    en_100mev = np.zeros(nmax)
    en_1Gev = np.zeros(nmax)
    br_2gev_line = np.zeros(nmax)
    pow_t = np.zeros(nmax)
    max_t = np.zeros(nmax)
    av_t = np.zeros(nmax)
    n_emax_t = np.zeros(nmax)
    n_av_t = np.zeros(nmax)
    
    filename = 'el_brill_%.2f' % (boundary)
    nmin = utils.read_tseries(path, 'ez_center_ts', ez_t)
    
    nmin = utils.read_tseries(path, filename, en_t,
                              br_full, br_100mev, br_1Gev,
                              en_full, en_100mev, en_1Gev,
                              br_2gev_line, pow_t, max_t, av_t, n_emax_t, n_av_t)
    for n in range(nmin,nmax):
        read, ang = utils.bo_file_load(path + utils.elangspsphpath,n,phi,theta, fmt='%.4f', verbose=1)
        if not read:
            continue
        # Calculate radiated power
        power = np.sum(ang)/dt*1e-7*1e-15
        pow_t[n] = power
        
        read, energy = utils.bo_file_load(path + utils.elenspsphpath, n, fmt='%.4f', verbose=1)
        en_t[n] = sum(energy)
        number = convert_energy_to_number(energy, emax/ne)
        num = sum(number)
        
        # Calculate max(1%) and average energy
        av_energy, max_energy, n_av, n_emax = find_max_energy_sph(number,de)
        max_t[n] = max_energy
        av_t[n] = av_energy
        n_emax_t[n] = num/dt/S/ds
        n_av_t[n] = n_av/dt/S/ds
        
        axis = utils.create_axis(ne, de)

        # Calculate full energy and number of emitted electrons
        num_, en_ = sum_energy(number, 0, de)
        en_full[n] = en_*ev/1e-9
        br_full[n] = num/dt

        # Calculate energy and number of emitted electrons > 0.1 GeV
        num1, en1_ = sum_energy(number, 0.1, de)
        en_100mev[n] = en1_*ev/1e-9
        br_100mev[n] = num1/dt

        # Calculate energy and number of emitted electrons > {boundary} GeV
        num2, en2_ = sum_energy(number, boundary, de)
        en_1Gev[n] = en2_*ev/1e-9
        br_1Gev[n]  = num2/dt
        num_line, en_line = sum_energy_line(number, boundary, emax*1e-9/ev/ne)
        br_2gev_line[n] = num_line/dt
    utils.save_tseries(path,filename,nmin,nmax,en_t,
                       br_full, br_100mev, br_1Gev,
                       en_full, en_100mev, en_1Gev,
                       br_2gev_line, pow_t, max_t, av_t, n_emax_t, n_av_t)
    sen_full = np.sum(en_full)
    sen_100mev = np.sum(en_100mev)
    sen_1gev = np.sum(en_1Gev)
    sum_en = np.sum(en_t)
    
    pulse_energy = 1.05*duration*ppw
    print 'Full energy = ', sum_en, 'full_energy(check) = ', sen_full/sum_en, ' 100 mev energy = ', sen_100mev/sum_en, '%.0f Gev energy = '%(boundary), sen_1gev/sum_en
    print 'Efficiency: full = ', sen_full/pulse_energy, '100 mev = ', sen_100mev/pulse_energy, ' %.0f Gev = '%(boundary), sen_1gev/pulse_energy
    
    print 'Emittance:  full = ', max(br_full)/S/ds3, ' 100mev= ', max(br_100mev)/S/ds2, ' %.0f Gev = '%(boundary), max(br_1Gev)/S/ds1
    print 'Flux:  full = ', max(br_full), ' 100mev= ', max(br_100mev), ' %.0f Gev = '%(boundary), max(br_1Gev)
    print 'Emittance line: %.0f Gev = '%(boundary), max(br_2gev_line)/S/ds1
    print 'Max: %.0f Gev = ' % (boundary), max(br_2gev_line), max(br_1Gev)
    print 'Number:  full = ', sum(br_full)*dt, ' 100mev= ', sum(br_100mev)*dt, ' %.0f Gev = %.2f 10^9 '%(boundary, sum(br_1Gev)*dt*1e-9)
    
    sfile = 'el_summary_%.2f.txt' % (boundary)
    summary = open(sfile, 'w')
    summary.write(str(pulse_energy) + '\n')
    summary.write(str(sen_full) + '\n')
    summary.write(str(sen_100mev) + '\n')
    summary.write(str(sen_1gev) + '\n')
    summary.write(str(max(br_full)) + '\n')
    summary.write(str(max(br_100mev)) + '\n')
    summary.write(str(max(br_1Gev)) + '\n')
    summary.write(str(max(br_2gev_line)) + '\n')
    summary.write(str(sum(br_full)*dt) + '\n')
    summary.write(str(sum(br_100mev)*dt) + '\n')
    summary.write(str(sum(br_1Gev)*dt) + '\n')
    summary.write(str(sum(br_2gev_line)*dt) + '\n')
    summary.close()

    axis_t = utils.create_axis(nmax, dt*1e15)
    axise_t = utils.create_axis(ez_t.shape[0], dt*1e15)
    f = open('el_br_t.dat', 'w')
    for i in range(min(len(axis_t), len(axise_t))):
        a = 0
        ds = 1e6
        f.write('%lf %le %le %lf %lf %le %le %le %le %le %le %lf\n'% (axis_t[i], ez_t[i], pow_t[i], max_t[i], av_t[i], br_full[i], br_100mev[i], br_1Gev[i], br_full[i]/S/ds3, br_100mev[i]/S/ds2, br_1Gev[i]/S/ds1, a))
    f.close()
    
    fig = plt.figure(num=None)
    mp.rcParams.update({'font.size': 8})
    picname = picspath + '/' + "el_br_t.png"
    ax = fig.add_subplot(4,1,1)
    
    print axis_t.shape, br_full.shape
    f, = ax.plot(axis_t, br_full, 'r')
    ax1 = ax.twinx()
    f, = ax.plot(axis_t, n_emax_t, 'b')
#    m, = ax.plot(axis_t, br_100mev, 'b')
#    g, = ax.plot(axis_t, br_1Gev, 'g')
#    print max(br_full), max(br_100mev), max(br_1Gev), max(br_full)/S/ds, max(br_100mev)/S/ds, max(br_1Gev)/S/ds 
    ax.set_ylabel('Electron flux, ph/s')
#    ax.set_yscale('log')
    ax1 = ax.twinx()
    p, = ax1.plot(axis_t, pow_t, 'k')
    pow_av = 0
    num = 15
    for i in range(num):
        pow_av = pow_av + pow_t[-i]
    pow_av = pow_av/num
    print pow_av
    ax1.set_ylabel('Radiated power, PW')
#    plt.legend([f, m, g, p], ["full", ">100MeV", '>1GeV', 'Power'], loc=3)

    ax = fig.add_subplot(4,1,2)
    f, = ax.plot(axis_t, n_emax_t, 'r')
    q, = ax.plot(axis_t, n_av_t, 'b')
    print max(n_emax_t), max(n_av_t) 
    ax.set_ylabel('Brilliance, ph/s/mm^2/mrad^2')
#    ax.set_yscale('log')
    ax1 = ax.twinx()
    p, = ax1.plot(axis_t, max_t, 'k')
    e, = ax1.plot(axis_t, av_t, 'g')
    ax1.set_ylabel('Max electron energy, GeV')
    plt.legend([f, q, p, e], ["br max", "br max", "max energy", 'av energy'], loc=3)
#    ax1.set_yscale('log')
    ax = fig.add_subplot(4,1,3)
    f, = ax.plot(axis_t, n_emax_t, 'r')
    ax1 = ax.twinx()
    f1, = ax1.plot(axise_t, ez_t, 'g')
    plt.legend([f, f1], ["br", 'Ez'], loc=3)
    ax = fig.add_subplot(4,1,4)
    f1, = ax.plot(axise_t, ez_t, 'g')
    plt.legend([f, f1], ["angle", 'Ez'], loc=3)
    plt.savefig(picname)
    plt.close()

   
#    plt.show()
if __name__ == '__main__':
    main()
