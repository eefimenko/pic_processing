#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import sys
import math
import utils

def read_diag(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def profile(array):
    p1 = []
    n = len(array)
    m = len(array[0])
    av_num = m/8
    for i in range(n):
        s = 0
        for j in range(av_num):
            s += array[i][m/2 - av_num/2 + j]
        s /= av_num
        p1.append(s)
    s = sum(p1)
    for i in range(len(p1)):
        p1[i] /= s
    return p1

def summarray(array):
    n = len(array)
    m = len(array[0])
    s = 0
    for i in range(n):
        for j in range(m):
            if j > m/4 and j < m*3/4: 
                s += array[i][j]
    return s

def average(p, de):
    s = 0
    n = 0
    for i in range(len(p)):
        s += (i+0.5)*de*p[i]
        n += p[i]
    s /= n
    return s, n

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def width(array, de):
    n = len(array)
    m = 0.5 * max(array)
    for i in range(5, n-1):
        if array[i] > m and array[i+1]<m:
            return i*de
        
    return 0

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def main():

    config = utils.get_config("ParsedInput.txt")
    BOIterationPass = float(config['BOIterationPass'])
    delta = 1

    num = len(sys.argv) - 1 
        
    print 'Found ' + str(num) + ' files'
        
    ppath = 'data/ElPhase/'
    mpath = 'data/PosPhase/'
    p1path = 'data/ElMomentum/'
    m1path = 'data/PosMomentum/'
    picspath = 'pics'
    nepath = '/data/NeTrap/'
    
    nx1 = int(config['ElPhase.SetMatrixSize_0'])
    ny1 = int(config['ElPhase.SetMatrixSize_1'])
    zmin = float(config['ElPhase.SetBounds_0'])*1e4
    zmax = float(config['ElPhase.SetBounds_1'])*1e4
    Emin = float(config['ElPhase.SetBounds_2'])
    Emax = float(config['ElPhase.SetBounds_3'])
    Mmin = float(config['ElMomentum.SetBounds_2'])
    Mmax = float(config['ElMomentum.SetBounds_3'])
    
    de = (Emax - Emin)/ny1
    gaxis = create_axis(ny1,de,Emin)
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])

    T = 2 * math.pi/omega*1e15
    
    step = x0*y0*1e15/T
    p1 = []
    m1 = []
    for i in range(num):
        k = int(sys.argv[i+1])
        pname = ppath + '/' + "%06d.txt" % (k,)
#        mname = mpath + '/' + "%06d.txt" % (k,)
        pfield = read_diag(pname,nx1,ny1)
#        mfield = read_diag(mname,nx1,ny1)
        p1.append(profile(pfield))
#        m1.append(profile(mfield))
    nmin = int(min(sys.argv[1:]))
    nmax = int(max(sys.argv[1:]))+1
    print nmin,nmax
    taxis = create_axis(nmax-nmin, step, nmin*step)
    e_t = []
    e1_t = []
    for i in range(nmin,nmax):
        e = read_trap('.' + nepath + '%06d.txt'%(i))
        e_t.append(e)
        pname = ppath + '/' + "%06d.txt" % (i,)
        pfield = read_diag(pname,nx1,ny1)
        e1 = summarray(pfield)
        e1_t.append(e1)
    print len(taxis), len(e_t)
    fig = plt.figure(num=None)
    plt.rc('text', usetex=True)
    ax = fig.add_subplot(2,2,1)
    ax1 = fig.add_subplot(2,2,2)
    ax2 = fig.add_subplot(2,1,2)
    
    for i in range(num):
        t = int(sys.argv[i+1])*step
        w = width(p1[i], de)
        label1 = '%.3lf T $w_{1/2}=$%.1lf' % (t, w)
        ax.plot(gaxis,p1[i], label = label1)
        ax1.plot(gaxis,p1[i], label = label1)
        print average(p1[i], de), w, t
    ax2.plot(taxis, e_t, label = '$r=\lambda/2$')
    ax2.plot(taxis, e1_t, label = '$r=\lambda/10$')    
    
#    ax.set_yscale('log')
    ax.set_xlim([0, 5000])
    ax1.set_xlim([0, 5000])
    ax1.set_yscale('log')
    ax.set_xlabel('$\gamma$')
    ax1.set_xlabel('$\gamma$')
    ax.set_ylabel('Number of electrons')
    ax1.set_ylabel('Number of electrons')
    ax2.set_ylabel('Number of electrons in cylinder h = $\lambda$')
    ax2.set_xlabel('t, T')
    ax.legend(loc='upper right', shadow=True)
    ax2.legend(loc='upper left', shadow=True)
    picname = picspath + '/' + "cmpgamma.png"
    print picname
    plt.savefig(picname)
    plt.close()
        #plt.show()

if __name__ == '__main__':
    main()
