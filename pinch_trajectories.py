#!/usr/bin/python
from matplotlib import gridspec
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.cm as cm
import matplotlib.colors as colors
import matplotlib as mp
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import math
import utils
import sys

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def read_traj(file):
    f = open(file, 'r')
    x_ = []
    y_ = []
    z_ = []
    e_ = []
    r_ = []
    mx_ = []
    my_ = []
    mz_ = []
    efield = []
    bfield = []
    ev = 1.6e-12
    c = 2.99792e+10
    for line in f:
        tmp = line.split()
        num_ = len(tmp)
        x = float(tmp[1])
        y = float(tmp[2])
        z = float(tmp[3])
        px = float(tmp[4])
        py = float(tmp[5])
        pz = float(tmp[6])
        if num_ == 13:
            ex_ = float(tmp[7])
            ey_ = float(tmp[8])
            ez_ = float(tmp[9])
            bx_ = float(tmp[10])
            by_ = float(tmp[11])
            bz_ = float(tmp[12])
            efield.append(math.sqrt(ex_*ex_ + ey_*ey_ + ez_*ez_))
            bfield.append(math.sqrt(bx_*bx_ + by_*by_ + bz_*bz_))
        r = math.sqrt(x*x + y*y)
        pr = math.sqrt(px*px + py*py)
        e = math.sqrt(px*px + py*py + pz*pz)*c/ev/0.511e6
        mx = y*pz - py*z
        my = -(x*pz - px*z)
        mz = x*py - px*y
        x_.append(x)
        y_.append(y)
        z_.append(z)
        mx_.append(mx)
        my_.append(my)
        mz_.append(mz)
        r_.append(r)
        e_.append(e)
    return np.array(x_), np.array(y_), np.array(z_), np.array(r_), np.array(e_), np.array(mx_), np.array(my_), np.array(mz_), np.array(efield), np.array(bfield)

def normalize(a, n = 1.):
    if n != 0:
        for i in range(len(a)):
            a[i] /= n
    return a
  
def main():
    expath = 'data/E2x'
    partpath = 'ParticleTracking/Electrons'
#    partpath = 'ParticleTracking' 
    picspath = 'pics'
    path_27pw = '27pw_x1/'
    path_27pw_field = '27pw'
    iter_27pw_field = 407
    path_19pw = '19pw_traj/'
    path_19pw_field = '19pw'
    iter_19pw_field = 995
    numbers_27pw = [1412, 9347, 3820,  9359, 1225, 745, 1130, 1432, 1503, 1507, 2047, 2476, 3755, 3820, 4377, 6589, 8291, 8335, 8798, 8831, 9269, 9672, 9728, 9788]
    numbers_19pw = [334, 5764, 2015, 5143] # 19pw
    num = 2#len(numbers)

    config = utils.get_config(path_27pw + "ParsedInput.txt")
    wl = float(config['Wavelength'])
    k = 2.*math.pi/wl
    BOIterationPass = float(config['BOIterationPass'])
    dt = float(config['TimeStep'])*1e15*BOIterationPass #fs
    maxpower = float(config['PeakPower'])*1e-7*1e-15 #PW
    peakpower = float(config['PeakPower'])*1e-7*1e-15
    const_a_p = 7.81441e-9
    a0 = const_a_p * math.sqrt(peakpower*1e22)
    print a0
    print "Power = " + str(maxpower) + 'PW'
    print "dt = " + str(dt) + 'fs'
#    print "duration = " + str(duration)
#    print "delay = " + str(delay)
    
    Xmax = float(config['X_Max']) #mkm
    Xmin = float(config['X_Min']) #mkm
    Ymax = float(config['Y_Max']) #mkm
    Ymin = float(config['Y_Min']) #mkm
    Zmax = float(config['Z_Max']) #mkm
    Zmin = float(config['Z_Min']) #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz
    wl = float(config['Wavelength']) 
    step = (Xmax-Xmin)/nx
    x_ = [Xmin + i_*dx for i_ in range(nx)]
    axis_ = [x__/wl for x__ in x_]
    Esw = np.array([math.sqrt(utils.exf(x__,0,0,k)**2 + utils.eyf(x__,0,0,k)**2 + utils.ezf(x__,0,0,k)**2) for x__ in x_])
    Bsw = np.array([math.sqrt(utils.bxf(x__,0,0,k)**2 + utils.byf(x__,0,0,k)**2) for x__ in x_])
    mult = 1/(2.*dx*dy*dz)

    delta = 1
    jet = plt.get_cmap('jet') 
    cNorm  = colors.Normalize(vmin=0.5, vmax=a0*1e-3*0.5)
    scalarMap = cm.ScalarMappable(norm=cNorm, cmap=jet)
    print scalarMap.get_clim()
   
    
    n = 0
    ii = 0
    zout = 0.4
    start = 0
    end=500
    bound=0.1
    boundz=0.25
    xmax = 0.3
    zmax = 0.4
    fig = plt.figure(num=None, figsize=(7.5, 7.5))
    ax = plt.subplot2grid((2,2),(0,0))
    ax2 = plt.subplot2grid((2,2),(0,1))
#    fig1 = plt.figure(num=None, figsize=(10, 5))
    ax1 = plt.subplot2grid((2,2),(1,0),colspan=2)
#    ay = fig.add_subplot(2,2,2)
#    axy = fig.add_subplot(2,2,3)
#    az = fig.add_subplot(2,2,4)
    Esw /= 5.*np.amax(Esw)
    Bsw /= 7.7*np.amax(Bsw)
    ax_b = ax.twinx()
    ax2_b= ax2.twinx()
    ax_b.plot(axis_, Esw, 'k--')
    ax_b.plot(axis_, Bsw, 'r--')
    ax_b.set_ylim([0,0.8])
    ax2_b.plot(axis_, Esw, 'k--')
    ax2_b.plot(axis_, Bsw, 'r--')
    ax2_b.set_ylim([0,0.8])
    print Esw
    
    color_19pw = ['m', 'g']
    color_27pw = ['r', 'b']
    b_field = utils.bo_file_load(path_27pw_field + '/' + utils.bzpath,iter_27pw_field,nx,ny, verbose=1, transpose=1)
    print b_field.shape
    profile_27pw = b_field[nx/2]/(3e11*math.sqrt(27./10.))*0.2
    b_axis = utils.create_axis(nx, dx/wl, Xmin/wl)
    ax2_b.plot(b_axis, profile_27pw, color = 'r', dashes = [1,1])
    ax2_b.yaxis.set_ticks_position('none') 
    ax_b.yaxis.set_ticks_position('none') 
    b_field = utils.bo_file_load(path_19pw_field + '/' + utils.bzpath,iter_19pw_field,nx,ny, verbose=1, transpose=1)
    print b_field.shape
    profile_19pw = b_field[nx/2]/(3e11*math.sqrt(19./10.))*0.2
    b_axis = utils.create_axis(nx, dx/wl, Xmin/wl)
    ax_b.plot(b_axis, profile_19pw, color = 'r', dashes = [1,1])
    
    for i in range(0,num):
        print i
       
        i_ = numbers_19pw[i]
        x,y,z,r,e,mx,my,mz,efield,bfield = read_traj(path_19pw + partpath + '/%d.txt' % (i_))
        x /= wl
        y /= wl
        z /= wl
        bfield /= 0.65*3e11*math.sqrt(19./10.) 
        ax.plot(x[start:end], z[start:end], color = color_19pw[i])
        ax.plot(x[start], z[start], marker='o', color='b')
        
        if len(bfield) > 0:
            ax1.plot(x[start:end], bfield[start:end], color = color_19pw[i])
            ax1.plot(x[start], bfield[start], marker='o', color='b')
    ax1.set_xlim([-xmax,xmax])
    ax1.set_xlabel('$x/\lambda$')
    ax1.set_ylabel('$B/B_0$')
    ax.set_xlim([-xmax,xmax])
    ax.set_ylim([-zmax,zmax])
    ax.axvline(x=0, color='gray')
    ax.axhline(y=0, color='gray')
    
    ax.set_xlabel('$x/\lambda$')
    ax.set_ylabel('$z/\lambda$')
    ax2.set_xlim([-xmax,xmax])
    ax2.set_ylim([-zmax,zmax])
    ax2.axvline(x=0, color='gray')
    ax2.axhline(y=0, color='gray')
    ax2.set_xlabel('$x/\lambda$')
    ax2.set_ylabel('$z/\lambda$')
    
    for i in range(0,num):
        print i
       
        i_ = numbers_27pw[i]
        x,y,z,r,e,mx,my,mz,efield,bfield = read_traj(path_27pw + partpath + '/%d.txt' % (i_))
        x /= wl
        y /= wl
        z /= wl
        bfield /= 0.65*3e11*math.sqrt(27./10.) 
        ax2.plot(x[start:end], z[start:end], color = color_27pw[i])
        ax2.plot(x[start], z[start], marker='o', color='b')
        
        if len(bfield) > 0:
            ax1.plot(x[start:end], bfield[start:end], color = color_27pw[i])
            ax1.plot(x[start], bfield[start], marker='o', color='b')
    ax1.set_xlim([-xmax,xmax])
    ax1.set_xlabel('$x/\lambda$')
    ax1.set_ylabel('$B/B_0$')

    picname = picspath + '/' + "trajectories_for_pinch.png"
    print picname
    fig.tight_layout()
    fig.savefig(picname, dpi=256)

if __name__ == '__main__':
    main()
