#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_gamma(filename):
    f = open(filename, 'r')
    tmp = f.readline().split()
    f.close()
    return float(tmp[0]), float(tmp[1]), float(tmp[2])

def main():
    el = []
    ph = []
    pos = []
    ne = []
    dirs = ['nc_0.001', 'nc_0.01', 'nc_0.1', 'nc_1', 'nc_10','nc_30', 'nc_50', 'nc_100', 'nc_1000']
    updirs = ['2mkm']
    dashes = [[2,2], [6,1], [3,1]]
    fig = plt.figure()
    ax1 = fig.add_subplot(2,2,1)
    ax2 = fig.add_subplot(2,2,2)
    ax3 = fig.add_subplot(2,2,3)
    ax4 = fig.add_subplot(2,2,4)
    m = 0
    for ud in updirs:
        el = []
        ph = []
        pos = []
        el_1gev = []
        ph_1gev = []
        pos_1gev = []
        ne = []
        number = []
        for d in dirs:
            ph_, el_, pos_ = read_gamma(ud + '/' + d + '/efficiency.txt')
            el.append(el_)
            ph.append(ph_)
            pos.append(pos_)
            ph_, el_, pos_ = read_gamma(ud + '/' + d + '/efficiency_1gev.txt')
            el_1gev.append(el_)
            ph_1gev.append(ph_)
            pos_1gev.append(pos_)
            config = utils.get_config(ud + '/' + d + '/ParsedInput.txt')
            n_cr = float(config['n_cr'])
            ne_ = float(config['Ne'])
            r = float(config['R'])
            ne.append(ne_/n_cr)
            number.append(4./3.* math.pi * r**3 * ne_)
        ax1.plot(ne, ph, 'r', dashes = dashes[m])
        ax1.plot(ne, el, 'g', dashes = dashes[m])
        ax1.plot(ne, pos, 'b', dashes = dashes[m])
        ax2.plot(number, ph, 'r', dashes = dashes[m])
        ax2.plot(number, el, 'g', dashes = dashes[m])
        ax2.plot(number, pos, 'b', dashes = dashes[m])
        ax3.plot(ne, ph_1gev, 'r', dashes = dashes[m])
        ax3.plot(ne, el_1gev, 'g', dashes = dashes[m])
        ax3.plot(ne, pos_1gev, 'b', dashes = dashes[m])
        ax4.plot(number, ph_1gev, 'r', dashes = dashes[m])
        ax4.plot(number, el_1gev, 'g', dashes = dashes[m])
        ax4.plot(number, pos_1gev, 'b', dashes = dashes[m])
        m += 1
#    ax1.set_yscale('log')
    ax1.set_xscale('log')
    ax1.set_yscale('log')
    ax1.set_ylim([1e-3, 1])
    ax2.set_xscale('log')
    ax2.set_yscale('log')
    ax2.set_ylim([1e-3, 1])
    ax3.set_xscale('log')
#    ax3.set_yscale('log')
#    ax3.set_ylim([1e-3, 1])
    ax4.set_xscale('log')
#    ax4.set_yscale('log')
#    ax4.set_ylim([1e-3, 1])
    plt.show()

if __name__ == '__main__':
    main()
