#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1)
        field.append(row)
    return field

def smooth(a, factor):
    nx = len(a)
    ny = len(a[0])
    nx1 = nx/factor
    res = [[0 for j in range(ny)] for i in range(nx1)]
    print len(a), len(a[0])
    print len(res), len(res[0])
    for i in xrange(nx1):
        for j in xrange(ny):
            for k in range(factor):
                res[i][j] += a[i*factor + k][j]/factor
    return res

def sum_over_z(a):
    res = [0] * len(a)
    res1 = [0] * len(a)

    for i in range(len(a)):
        res1[i] = a[i][len(a[0])/2]
        for j in range(len(a[0])):
            res[i] += a[i][j]
    return res, res1

def sum_over_y(a):
    res = [0] * len(a[0])
    res1 = [0] * len(a[0])

    for i in range(len(a[0])):
        res1[i] = a[len(a)/2][i]
        for j in range(len(a)):
            res[i] += a[j][i]
    return res, res1

def create_subplot(fig,i,path,nx,ny,spx,spy,spi,  Ymin, Ymax, Xmin, Xmax, xl, yl, color, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
    name = path + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny, mult)
    if 'Electron2Dx_fine' in path:
        print path
        field = smooth(field, 1)
    prof1 = field[len(field)/2]
#    prof2 = []
#    for n in range(len(field)):
#        prof2.append(field[n][len(field[0])/2])
#    prof2, prof3 = sum_over_z(field) # for e-dipole wave
    prof2, prof3 = sum_over_y(field) # for m-dipole wave
    maxe = max([max(row) for row in field])
#    print len(field), len(field[0])
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, norm=clr.LogNorm(), aspect = 'auto')
    ax.text(0.3, 1.1, 'max=%.1le' % (maxe), style='italic', horizontalalignment='center', verticalalignment='center',  transform = ax.transAxes)
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
    plt.colorbar(surf,  orientation  = 'vertical')
#    print len(prof1), len(prof2)
    return prof1, prof2, prof3

def create_subplot_prof(fig,i,spx,spy,spi,p1,axes, xl, yl, vmin, dirs, log = 0):
    ax = fig.add_subplot(spx,spy,spi)
    j = 0
    vmax = 0
    for k in range(len(p1)):
        print i,j
        p1[k] = utils.normalize(p1[k])
        surf = ax.plot(axes[k], p1[k], get_color(j), label = dirs[k])
        j += 1
        tmp = max(p1[k])
        if tmp > vmax:
            vmax = tmp
        
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
    ax.set_xlim([axes[1][0]/4, axes[1][-1]/4])
#    ax.set_ylim([vmin, vmax])
    if log == 1:
        ax.set_yscale('log')
    ax.legend(loc='lower left')
    

def find_nmax(dirs, expath):
    nmax = utils.num_files(dirs[0] + expath)
    for k in range(1,len(dirs)):
        tmp = utils.num_files(dirs[k] + expath)
        if tmp < nmax:
            nmax = tmp
    return nmax

def get_color(j):
    color = ['r', 'g', 'b', 'k', 'c', 'y', 'G', 'B']
    if (j >= len(color)):
        j = j - len(color)
    return color[j]

def main():
    nexpath = '/data/Electron2Dy'
    nphxpath = '/data/Photon2Dy'
    nexfpath = '/data/Electron2Dx_fine'
    nphxfpath = '/data/Photon2Dx_fine'
    picspath = 'pics'
    path = '.'
    delta = 1
    fmin = 0
    fmax = 0
    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(path + nexpath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(path + nexpath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax-nmin) + ' files'

    print nmin, nmax, delta
    config = utils.get_config(path + "/ParsedInput.txt")

    BOIterationPass = float(config['BOIterationPass'])
    dt = float(config['TimeStep'])*1e15*BOIterationPass #fs
    maxpower = float(config['PeakPower'])*1e-7*1e-15 #PW

    print "Power = " + str(maxpower) + 'PW'
    print "dt = " + str(dt) + 'fs'
    
    Xmax = float(config['X_Max'])*1e4 #mkm
    Xmin = float(config['X_Min'])*1e4 #mkm
    Ymax = float(config['Y_Max'])*1e4 #mkm
    Ymin = float(config['Y_Min'])*1e4 #mkm
    Zmax = float(config['Z_Max'])*1e4 #mkm
    Zmin = float(config['Z_Min'])*1e4 #mkm
    zmin_fine = float(config['Photon2Dx_fine.SetBounds_2'])*1e4
    zmax_fine = float(config['Photon2Dx_fine.SetBounds_3'])*1e4
    ymin_fine = float(config['Photon2Dx_fine.SetBounds_0'])*1e4
    ymax_fine = float(config['Photon2Dx_fine.SetBounds_1'])*1e4
    nyp_fine = int(config['Photon2Dx_fine.SetMatrixSize_0'])
    nye_fine = int(config['Electron2Dx_fine.SetMatrixSize_0'])
    nzp_fine = int(config['Photon2Dx_fine.SetMatrixSize_1'])
    nze_fine = int(config['Electron2Dx_fine.SetMatrixSize_1'])
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    nz_fine = nzp_fine

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)
    print 'nyp_fine', nyp_fine 
    print 'nye_fine', nye_fine 
    print 'nz_fine', nz_fine 
    print 'ymax_fine', ymax_fine 
    print 'ymin_fine', ymin_fine 
    print 'zmax_fine', zmax_fine 
    print 'zmin_fine', zmin_fine 
   
    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz
    dyp_fine = (ymax_fine - ymin_fine)/nyp_fine
    dye_fine = (ymax_fine - ymin_fine)/nye_fine
    dze_fine = (zmax_fine - zmin_fine)/nze_fine
    dzp_fine = (zmax_fine - zmin_fine)/nzp_fine
    dz_fine = dzp_fine
    step = (Xmax-Xmin)/nx

    mult = 1/(2.*dx*dy*dz*1e-12)
    multp_fine = 1/(2.*0.1*dx*dyp_fine*dz_fine*1e-12)
    multe_fine = 1/(2.*0.1*dx*dye_fine*dz_fine*1e-12)
    print 'mult', mult
    print 'multp_fine', multp_fine
    print 'multe_fine', multe_fine
    for i in range(nmin,nmax,delta):
        prof = []
        axes = []
        print "\rSaving s%06d.png, %.0f %% done" % (i,float(i-nmin)/(nmax-nmin)*100),
        sys.stdout.flush()

        fig = plt.figure(num=None, figsize=(20, 10), dpi=256)
        mp.rcParams.update({'font.size': 8})
        
        p1, p2, _p3 = create_subplot(fig,i,path+nphxpath,ny,nz,2,3,1, Xmin, Xmax, Ymin, Ymax, 'z', 'y', 'Blues', mult)
        p1f, p2f, p3f = create_subplot(fig,i,path+nphxfpath,nyp_fine,nz_fine,2,3,2, ymin_fine, ymax_fine, zmin_fine, zmax_fine, 'z', 'x', 'Blues', multp_fine)
        prof.append(p2)
#        axes.append(utils.create_axis(ny,dy,Ymin))
        axes.append(utils.create_axis(nz,dz,Zmin))
        prof.append(p2f)
#        axes.append(utils.create_axis(nyp_fine,dyp_fine,ymin_fine))
        axes.append(utils.create_axis(nzp_fine,dzp_fine,zmin_fine))
        prof.append(p3f)
#        axes.append(utils.create_axis(nyp_fine,dyp_fine,ymin_fine))
        axes.append(utils.create_axis(nzp_fine,dzp_fine,zmin_fine))
        create_subplot_prof(fig,i,2,3,3,prof,axes, '', '', 0, ['rough', 'fine summed', 'fine z=0'], log = 0)
        prof = []
        axes = []
        p1, p2, _p3 = create_subplot(fig,i,path+nexpath,ny,nz,2,3,4, Xmin, Xmax, Ymin, Ymax, 'z', 'y', 'Greens', mult)
        p1f, p2f, p3f = create_subplot(fig,i,path+nexfpath,nye_fine,nz_fine,2,3,5, ymin_fine, ymax_fine, zmin_fine, zmax_fine, 'z', 'x', 'Greens', multe_fine)
        prof.append(p2)
#        axes.append(utils.create_axis(ny,dy,Ymin))
        axes.append(utils.create_axis(nz,dz,Zmin))
        prof.append(p2f)
#        axes.append(utils.create_axis(nyp_fine,dyp_fine,ymin_fine))
        axes.append(utils.create_axis(nzp_fine,dzp_fine,zmin_fine))
        prof.append(p3f)
#        axes.append(utils.create_axis(nyp_fine,dyp_fine,ymin_fine))
        axes.append(utils.create_axis(nzp_fine,dzp_fine,zmin_fine))
        create_subplot_prof(fig,i,2,3,6,prof,axes, '', '', 0, ['rough', 'fine summed', 'fine z=0'], log = 0)
        picname = picspath + '/' + "sfine%06d.png" % (i,)
        
        plt.savefig(picname)
        plt.close()
if __name__ == '__main__':
    main()
