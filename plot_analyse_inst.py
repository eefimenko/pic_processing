#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_file(filename):
    f = open(filename, 'r')
    axis = []
    e2max = []
    b2max = []
    nemax = []
    jzmax = []
    gammaav = []
    wp = []
    ndtwp = []
    ndxne = []
    dxne = []
#    if 'nc_2' in filename:
#        num = 4
#    else:
#        num = 1
    a0 = math.sqrt(27./10)*3e11
    for line in f:
        tmp = line.split()
        axis.append(float(tmp[0]))
        e2max.append(float(tmp[1])/a0)
        b2max.append(float(tmp[2])/(a0*0.65))
        nemax.append(float(tmp[3]))
        gammaav.append(float(tmp[4]))
        wp.append(float(tmp[5]))
        ndtwp.append(float(tmp[6]))
        ndxne.append(float(tmp[7]))
        dxne.append(float(tmp[8])*1e4/0.9)
        jzmax.append(float(tmp[9]))
    t0 = axis[0]
    for i in range(len(axis)):
        axis[i] -= t0

    return axis, e2max, b2max, nemax, gammaav, wp, ndtwp, ndxne, dxne, jzmax

def main():
    n = len(sys.argv) - 1
    fig = plt.figure()
    ax1 = fig.add_subplot(4,2,1)
    ax1.set_title('|E| max')
#    ax1.set_yscale('log')
#    ax1.set_ylim([0, 1.8])
    ax2 = fig.add_subplot(4,2,2)
    ax2.set_title('|B| max')
#    ax2.set_ylim([0, 1.8])
#    ax2.set_yscale('log')
    ax3 = fig.add_subplot(4,2,6)
    ax3.set_title('Ne max')
    ax3.set_yscale('log')
#    ax3.set_ylim([1e25, 1e28])
    ax4 = fig.add_subplot(4,2,4)
    ax4.set_title('Jz')
    ax4.set_yscale('log')
#    ax4.set_ylim([0,1500])
    ax5 = fig.add_subplot(4,2,5)
    ax5.set_title('Wp')
#    ax5.set_yscale('log')
#    ax7.set_yscale('log')
    ax8 = fig.add_subplot(4,2,8)
    ax8.set_title('Radius of the current, R/lambda')
    ax6 = fig.add_subplot(4,2,3)
    ax6.set_title('Number dt per Tp')
#    ax6.set_yscale('log')
    ax6.set_ylim([0,10])
    ax7 = fig.add_subplot(4,2,7)
    ax7.set_title('Number dx per radius')
    bound = 1.5
    ax1.set_xlim([0,bound])
    ax2.set_xlim([0,bound])
    ax3.set_xlim([0,bound])
    ax4.set_xlim([0,bound])
    ax5.set_xlim([0,bound])
    ax6.set_xlim([0,bound])
    ax7.set_xlim([0,bound])
    ax8.set_xlim([0,bound])
    
    for i in range(n):
        path = sys.argv[i+1]
        axis, e2max, b2max, nemax, gammaav, wp, ndtwp, ndxne, dxne, jzmax = read_file(path + '/' + 'analyse_inst.dat')
        e1, = ax1.plot(axis, e2max)
        e2, = ax2.plot(axis, b2max)
        e3, = ax3.plot(axis, nemax)
        e4, = ax4.plot(axis, jzmax)
        e5, = ax5.plot(axis, wp) 
        e7, = ax7.plot(axis, ndxne, label = path)
        e8, = ax8.plot(axis, dxne)
        e6, = ax6.plot(axis, ndtwp)
       
    a = 0.4
    axis_ = [0, axis[-1]]
    tmp_ = [a, a]
    e6, = ax6.plot(axis_, tmp_, 'k', label = str(a))
    a = 1
#    axis_ = [0, axis[-1]]
#    tmp_ = [a, a]
#    e7, = ax7.plot(axis_, tmp_, 'k', label = str(a))
    plt.legend(loc = 'upper right')
    plt.show()

if __name__ == '__main__':
    main()
