#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 24 14:43:49 2020

@author: Алексей
"""
#import sys
import matplotlib as mp
mp.use('Agg')
import os
import math
import zipfile
import numpy as np
import re
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import sys
from mpl_toolkits.axes_grid1 import make_axes_locatable

def ReadZipTxt(pathZip,fileInZip,nx,ny=-1):    
    archive = zipfile.ZipFile(pathZip,'r')
    file = archive.read(fileInZip)
    archive.close()
    data=file.decode('utf-8').split()
    return np.array(list(map(float,data))).reshape(ny,nx)


def InputDictionary(pathToInput,dictInput):
    regString=r"""Setting (?:variable|string)\S*\s*(?P<nameVar>\S*)\s*=\s*(?P<valueVar>\S+)\s*"""
    regExpr=re.compile(regString)
    inpFile=open(pathToInput,'r')
    
    for line in inpFile:
        foundVar=regExpr.search(line)
        if foundVar:
            curDict=foundVar.groupdict()
            dictInput[curDict['nameVar']]=curDict['valueVar']
    inpFile.close()
    return 0

def Add1DPlot(data,xax,xrangeFrame,ax,color='k',yrangeFrame=None,linestyle='-',linewidth=1,xscale='linear',yscale='linear',marker=None,fillstyle='full',markersize=4):
    ax.set_xlim(xrangeFrame[0],xrangeFrame[1])
    if (yrangeFrame):
        ax.set_ylim(yrangeFrame[0],yrangeFrame[1])
    else:
        ax.set_ylim(data.min(),data.max())
    ax.set_xscale(xscale)
    ax.set_yscale(yscale)
    
    if (marker):
        lines, =ax.plot(xax,data,color=color,linestyle=linestyle,linewidth=linewidth,marker=marker,fillstyle=fillstyle,markersize=markersize)
    else:
        lines, =ax.plot(xax,data,color=color,linestyle=linestyle,linewidth=linewidth)
    return lines

numAnalysedPeriods=2
regime = "txt" # "txt"
if __name__ == "__main__":
    try:
        numAnalysedPeriods=int(sys.argv[2])
        regime=sys.argv[3]
    except:
        print("default parameters")
print("Parameters of work")
print("numAnalysedPeriods=",numAnalysedPeriods)
print("regime=",regime)

picdir = "/home/evgeny/Dropbox/reportMegagrant"
#labels=["Amp", "Ideal", "Shift", "Delay"]
#simulations=["DifA/InputUnified_4_30_2020-12-23_15-12-56",
#             "InputUnified_4_30_2020-12-29_23-49-05_Ideal4",
#             "DifShift/InputUnified_4_30_2020-12-25_00-34-31",
#            "DifDelay/InputUnified_4_30_2020-12-29_15-55-44"]

labels=["Amp", "Shift", "Delay", "Ideal", "Amp", "Amp_1.25", "Ideal","Shift","Delay"]
simulations=[
#    "amplitude_6beams/6beams_21pw",
#    "shift_6beams/6beams_21pw",
#    "delay_6beams/6beams_21pw",
#    "equal_6beams/6beams_21pw",
    "amplitude_4beams/4beams_30pw",
    "shift_4/4beams_30pw",
    "delay_4beams/4beams_30pw",
    "equal_4beams/4beams_30pw",
#    "amplitude/12beams_13pw",
#    "amplitude_1.25/12beams_13pw",
#    "equal/12beams_13pw",
#    "shift/12beams_13pw",
#    "delay/12beams_13pw",
             #"InputUnified_4_30_2020-12-29_23-49-05_Ideal4",
             #"DifShift/InputUnified_4_30_2020-12-25_00-34-31",
             #"DifDelay/InputUnified_4_30_2020-12-29_15-55-44"
]

print(simulations)
En1pEl=[]
En1pPos=[]
En1pPh=[]
fontsize = 14
mp.rcParams.update({'font.size': fontsize})

### calculation of growth rate in each simulation
for idx, sim in enumerate(simulations):
    print("Analysis of ",sim)
    ###read input file
    path=sim+os.path.sep
    zipfold=path+"BasicOutput"+os.path.sep+"data.zip"
    if os.path.isdir(os.path.join(sim, "BasicOutput")):
        pathtxt=os.path.join(sim, "BasicOutput", "data") + os.path.sep
    else:
        pathtxt=os.path.join(sim, "data") + os.path.sep
    
    inputDict={}
    InputDictionary(path+"ParsedInput.txt",inputDict)
    pwr=float(inputDict["PeakPowerPW"])
    number = int(inputDict["FieldGenerator.Number"])
    phi_0 = float(inputDict.get("FieldGenerator.Phi0", 0))
    print(phi_0)
    pulseAmplitudes = []
    for n_ in range(number):
        pulseAmplitudes.append(float(inputDict.get("FieldGenerator.A_%d" % n_, 1)))
        
    if labels[idx] == "Delay":
        for n_ in range(number):
            if float(inputDict.get("FieldGenerator.Delay_%d" % n_, 1)) != 1:
                pulseAmplitudes[n_] = 1.5
    if labels[idx] == "Shift":
        for n_ in range(number):
            if float(inputDict.get("FieldGenerator.Shift_%d" % n_, 1)) != 1:
                pulseAmplitudes[n_] = 1.5            
    print(pulseAmplitudes)
       
    w=float(inputDict["Omega"])
    dt=float(inputDict["TimeStep"])
    iterBetwSaves=int(inputDict["BOIterationPass"])
    iterst=int(inputDict["BOIterationStart"])
    nx=int(inputDict["E2y.SetMatrixSize_0"])
    nz=int(inputDict["E2y.SetMatrixSize_1"])
    Xmin = float(inputDict["X_Min"]) * 1e4
    Xmax = float(inputDict["X_Max"]) * 1e4
    Ymin = float(inputDict["Y_Min"]) * 1e4
    Ymax = float(inputDict["Y_Max"]) * 1e4
    Zmin = float(inputDict["Z_Min"]) * 1e4
    Zmax = float(inputDict["Z_Max"]) * 1e4
    ngam=int(inputDict["ElectronEn.SetMatrixSize_0"])
    gmin=float(inputDict["ElectronEn.SetBounds_0"])
    gmax=float(inputDict["ElectronEn.SetBounds_1"])
    numst=int(iterst/iterBetwSaves)
    period=2*math.pi/w
    numSavesperPeriod=int(period/(dt*iterBetwSaves)+0.5)
    nphi = int(inputDict["PhotonThetaPhi.SetMatrixSize_0"])
    ntheta = int(inputDict["PhotonThetaPhi.SetMatrixSize_1"])
    phi_min = float(inputDict["PhotonThetaPhi.SetBounds_0"])
    phi_max = float(inputDict["PhotonThetaPhi.SetBounds_1"])
    theta_min = float(inputDict["PhotonThetaPhi.SetBounds_2"])
    theta_max = float(inputDict["PhotonThetaPhi.SetBounds_3"])
    ###calculation of number of saved files
    if regime=="zip":
        zipfold=path+"BasicOutput"+os.path.sep+"data.zip"
        zipFile=zipfile.ZipFile(zipfold,"r")
        names=[]
        for name in zipFile.namelist():
            if ("E2y" in name):
                names.append(name)
        zipFile.close()
    if regime=="txt":
        if os.path.isdir(os.path.join(path, "BasicOutput")):
            names=os.listdir(os.path.join(path, "BasicOutput", "data", "E2y"))
        else:
            names=os.listdir(os.path.join(path, "data", "E2y"))
                            
        
    
    ###determination of start file and final file to analyse
    if regime=="zip":
        finnumFile=len(names)+numst-2
    if regime=="txt":
        finnumFile=len(names)+numst-1
    
    stnumFile=finnumFile-numAnalysedPeriods*numSavesperPeriod
    numFiles=finnumFile-stnumFile+1
   
    #calculation of averaged spectra
    spectraEl=np.zeros(ngam)
    densElZ = np.zeros((nx,nz))
    densElX = np.zeros((nx,nz))
    densElY = np.zeros((nx,nz))
    densPhX = np.zeros((nx,nz))
    densPhZ = np.zeros((nx,nz))
    densPhY = np.zeros((nx,nz))

    dnEl = np.zeros((ntheta, nphi))
    dnPh = np.zeros((ntheta, nphi))
    
    spectraPos=np.zeros(ngam)
    spectraPh=np.zeros(ngam)
    dgam=(gmax-gmin)/ngam
    enArr=np.linspace(gmin+dgam/2,gmax-dgam/2,ngam)
    if regime=="zip":
        for i in range(stnumFile,finnumFile+1):
            buf=ReadZipTxt(zipfold,("data/ElectronEn/%.6d.txt" % i),ngam)[0]
            number_el = buf.sum()
            spectraEl+=buf*enArr/number_el
            dens = ReadZipTxt(zipfold,("data/Electron2Dz/%.6d.txt" % i),nx, nz)
            densElZ += dens/number_el
            dens = ReadZipTxt(zipfold,("data/Electron2Dx/%.6d.txt" % i),nx, nz)
            densElX += dens/number_el
            dens = ReadZipTxt(zipfold,("data/Electron2Dy/%.6d.txt" % i),nx, nz)
            densElY += dens/number_el
            
            buf=ReadZipTxt(zipfold,("data/PositronEn/%.6d.txt" % i),ngam)[0]
            spectraPos+=buf*enArr/buf.sum()
            buf=ReadZipTxt(zipfold,("data/PhotonEn/%.6d.txt" % i),ngam)[0]
            number_ph = buf.sum()
            spectraPh+=buf*enArr/number_ph
            dens = ReadZipTxt(zipfold,("data/Photon2Dz/%.6d.txt" % i),nx, nz)
            densPhZ += dens/number_ph
            dens = ReadZipTxt(zipfold,("data/Photon2Dx/%.6d.txt" % i),nx, nz)
            densPhX += dens/number_ph
            dens = ReadZipTxt(zipfold,("data/Photon2Dy/%.6d.txt" % i),nx, nz)
            densPhY += dens/number_ph

            dn = ReadZipTxt(zipfold,("data/ElectronThetaPhi/%.6d.txt" % i), nphi, ntheta)
            dnEl += dn/number_el
            dn = ReadZipTxt(zipfold,("data/PhotonThetaPhi/%.6d.txt" % i), nphi, ntheta)
            dnPh += dn/number_ph
            
    if regime=="txt":
        for i in range(stnumFile,finnumFile+1):
            buf=np.loadtxt(pathtxt+"PositronEn"+os.path.sep+("%.6d.txt"%i))
            spectraPos+=buf*enArr/buf.sum()

            buf=np.loadtxt(pathtxt+"ElectronEn"+os.path.sep+("%.6d.txt"%i))
            number_el = buf.sum()
            spectraEl+=buf*enArr/number_el
            
            dens = np.loadtxt(pathtxt+"Electron2Dz"+os.path.sep+("%.6d.txt"%i))
            dens = dens.reshape(nx, nz)
            densElZ += dens/number_el
            dens = np.loadtxt(pathtxt+"Electron2Dx"+os.path.sep+("%.6d.txt"%i))
            dens = dens.reshape(nx, nz)
            densElX += dens/number_el
            dens = np.loadtxt(pathtxt+"Electron2Dy"+os.path.sep+("%.6d.txt"%i))
            dens = dens.reshape(nx, nz)
            densElY += dens/number_el
            
            buf=np.loadtxt(pathtxt+"PhotonEn"+os.path.sep+("%.6d.txt"%i))
            number_ph = buf.sum()
            spectraPh+=buf*enArr/number_ph
            dens = np.loadtxt(pathtxt+"Photon2Dz"+os.path.sep+("%.6d.txt"%i))
            dens = dens.reshape(nx, nz)
            densPhZ += dens/number_ph
            dens = np.loadtxt(pathtxt+"Photon2Dx"+os.path.sep+("%.6d.txt"%i))
            dens = dens.reshape(nx, nz)
            densPhX += dens/number_ph
            dens = np.loadtxt(pathtxt+"Photon2Dy"+os.path.sep+("%.6d.txt"%i))
            dens = dens.reshape(nx, nz)
            densPhY += dens/number_ph

            dn = np.loadtxt(pathtxt+"ElectronThetaPhi"+os.path.sep+("%.6d.txt"%i))
            dn = dn.reshape(ntheta, nphi)
            dnEl += dn/number_el
            dn = np.loadtxt(pathtxt+"PhotonThetaPhi"+os.path.sep+("%.6d.txt"%i))
            dn = dn.reshape(ntheta, nphi)
            dnPh += dn/number_ph
    spectraEl=spectraEl/numFiles
    spectraPos=spectraPos/numFiles
    spectraPh=spectraPh/numFiles
    
    # determination of 1% energy
    thEn=0.01*spectraEl.sum()
    cursum=0
    i=-1
    while cursum<thEn:
        cursum+=spectraEl[i]
        i-=1
    En1pEl.append(enArr[i+1])
    
    thEn=0.01*spectraPos.sum()
    cursum=0
    i=-1
    while cursum<thEn:
        cursum+=spectraPos[i]
        i-=1
    En1pPos.append(enArr[i+1])
    
    thEn=0.01*spectraPh.sum()
    cursum=0
    i=-1
    while cursum<thEn:
        cursum+=spectraPh[i]
        i-=1
    En1pPh.append(enArr[i+1])

    fig=plt.figure(figsize=(5,3))
    ax=fig.add_subplot(111)
    xrangeFrame=(enArr[2],enArr[-1]*1.1)
    maxy=max(spectraEl.max(),spectraPos.max(),spectraPh.max())
    yrangeFrame=(maxy*1e-6,maxy*1.1)
    Add1DPlot(spectraEl, enArr, xrangeFrame, ax,color='green',yrangeFrame=yrangeFrame,linestyle='-',linewidth=1,xscale='log',yscale='log')
    Add1DPlot(spectraPos, enArr, xrangeFrame,ax, color='red',yrangeFrame=yrangeFrame,linestyle='-',linewidth=1,xscale='log',yscale='log')
    Add1DPlot(spectraPh, enArr, xrangeFrame, ax,color='purple',yrangeFrame=yrangeFrame,linestyle='-',linewidth=1,xscale='log',yscale='log')
    ax.axvline(En1pEl[-1],color="green",linestyle=':',linewidth=1)
    ax.axvline(En1pPos[-1],color="red",linestyle=':',linewidth=1)
    ax.axvline(En1pPh[-1],color="purple",linestyle=':',linewidth=1)
    ax.set_xlabel("energy/(0.5MeV)")
    ax.set_ylabel("Spectra density,a.u.")
    plt.tight_layout()
    np.savetxt(os.path.join(sim,"en_array.txt"), enArr)
    np.savetxt(os.path.join(sim,"el_spectra.txt"), spectraEl)
    np.savetxt(os.path.join(sim,"ph_spectra.txt"), spectraPh)
    figname = os.path.join(picdir, 'AvSpectra_%d_pw_%d_beams_%s.png' % (int(pwr), number, labels[idx]))
    
    print(figname)
    fig.savefig(figname ,dpi=300)
    for ax in fig.axes:
        ax.clear()
    fig.clf()
    plt.close()
    
    cmap1 = mp.cm.get_cmap('hot_r')
    cmap1.set_under('w', False)
    cmap1.set_bad('w', 1)
    
    fig=plt.figure(figsize=(12,7))
    ax=fig.add_subplot(231)
    densElZ /= np.amax(densElZ)
    surf = ax.imshow(densElZ, cmap = cmap1, extent=[Xmin, Xmax, Ymin, Ymax], norm=clr.LogNorm(clip='True'), vmin = 1e-3, vmax = 1)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)
    ax.set_xlabel('x, $\mu$m')
    ax.set_ylabel('y, $\mu$m')
    ax.text(-3, 2, '(a)')

    r = 1.9
    r0 = 1.5
    
    if number <= 6:
        alpha_step = 2 * math.pi/number
        for n_ in range(number):
            alpha = alpha_step * n_ + phi_0
            x_ = - r * math.cos(alpha)
            y_ = r * math.sin(alpha)
            x0_ = - r0 * math.cos(alpha)
            y0_ = r0 * math.sin(alpha)
            ax.annotate('', xy = (x0_, y0_), xytext = (x_, y_), arrowprops=dict(facecolor='b', width = 4 * pulseAmplitudes[n_]**2, headwidth = 8 * pulseAmplitudes[n_]**2, shrink=0.05))
    else:
        alpha_step = 2 * math.pi/(number/2)
        for n_ in range(number/2):
            alpha = alpha_step * n_ + phi_0
            x_ = - r * math.cos(alpha)
            y_ = r * math.sin(alpha)
            x0_ = - r0 * math.cos(alpha)
            y0_ = r0 * math.sin(alpha)
            ax.annotate('', xy = (x0_, y0_), xytext = (x_, y_), arrowprops=dict(facecolor='b', width = 6 * pulseAmplitudes[n_]**2, headwidth = 8 * pulseAmplitudes[n_]**2, shrink=0.05))
            
        for n_ in range(number/2, number):
            alpha = alpha_step * (n_ + 0.5) + phi_0
            x_ = - r * math.cos(alpha)
            y_ = r * math.sin(alpha)
            x0_ = - r0 * math.cos(alpha)
            y0_ = r0 * math.sin(alpha)
            ax.annotate('', xy = (x0_, y0_), xytext = (x_, y_), arrowprops=dict(facecolor='g', width = 4 * pulseAmplitudes[n_]**2, headwidth = 8 * pulseAmplitudes[n_]**2, shrink=0.05))
    
    ax=fig.add_subplot(232)
    densElX /= np.amax(densElX)
    surf = ax.imshow(densElX, cmap = cmap1, extent=[Xmin, Xmax, Ymin, Ymax], norm=clr.LogNorm(clip='True'), vmin = 1e-3, vmax = 1)
    ax.set_xlabel('y, $\mu$m')
    ax.set_ylabel('z, $\mu$m')
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)
    ax.text(-3, 2, '(b)')

    if number <= 6:
        x_ = r
        x0_ = r0 
        ax.annotate('', xy = (x0_, 0), xytext = (x_, 0), arrowprops=dict(facecolor='b', shrink=0.05))
        ax.annotate('', xy = (-x0_, 0), xytext = (-x_, 0), arrowprops=dict(facecolor='b', shrink=0.05))
    else:
        alpha = 23.16 * math.pi/180.
        x_ = r 
        y_ = r * math.sin(alpha)
        x0_ = r0 
        y0_ = r0 * math.sin(alpha)
        
        ax.annotate('', xy = (x0_, y0_), xytext = (x_, y_), arrowprops=dict(facecolor='b', shrink=0.05))
        ax.annotate('', xy = (-x0_, y0_), xytext = (-x_, y_), arrowprops=dict(facecolor='b', shrink=0.05))
        ax.annotate('', xy = (x0_, -y0_), xytext = (x_, -y_), arrowprops=dict(facecolor='g', shrink=0.05))
        ax.annotate('', xy = (-x0_, -y0_), xytext = (-x_, -y_), arrowprops=dict(facecolor='g', shrink=0.05))

    ax=fig.add_subplot(233)
    densElY /= np.amax(densElY)
    surf = ax.imshow(densElY, cmap = cmap1, extent=[Xmin, Xmax, Ymin, Ymax], norm=clr.LogNorm(clip='True'), vmin = 1e-3, vmax = 1)
    ax.set_xlabel('x, $\mu$m')
    ax.set_ylabel('z, $\mu$m')
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)
    ax.text(-3, 2, '(c)')

    if number <= 6:
        x_ = r
        x0_ = r0 
        ax.annotate('', xy = (x0_, 0), xytext = (x_, 0), arrowprops=dict(facecolor='b', shrink=0.05))
        ax.annotate('', xy = (-x0_, 0), xytext = (-x_, 0), arrowprops=dict(facecolor='b', shrink=0.05))
    else:
        alpha = 23.16 * math.pi/180.
        x_ = r 
        y_ = r * math.sin(alpha)
        x0_ = r0 
        y0_ = r0 * math.sin(alpha)
        
        ax.annotate('', xy = (x0_, y0_), xytext = (x_, y_), arrowprops=dict(facecolor='b', shrink=0.05))
        ax.annotate('', xy = (-x0_, y0_), xytext = (-x_, y_), arrowprops=dict(facecolor='b', shrink=0.05))
        ax.annotate('', xy = (x0_, -y0_), xytext = (x_, -y_), arrowprops=dict(facecolor='g', shrink=0.05))
        ax.annotate('', xy = (-x0_, -y0_), xytext = (-x_, -y_), arrowprops=dict(facecolor='g', shrink=0.05))
        
    cmap2 = mp.cm.get_cmap('gist_heat_r')
    cmap2.set_under('w', False)
    cmap2.set_bad('w', 1)
    
    ax=fig.add_subplot(234)
    densPhZ /= np.amax(densPhZ)
    surf = ax.imshow(densPhZ, cmap = cmap2, extent=[Xmin, Xmax, Ymin, Ymax], norm=clr.LogNorm(clip='True'), vmin = 1e-2, vmax = 1)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)
    ax.set_xlabel('x, $\mu$m')
    ax.set_ylabel('y, $\mu$m')
    ax.text(-3, 2, '(d)')

    if number <= 6:
        alpha_step = 2 * math.pi/number
        for n_ in range(number):
            alpha = alpha_step * n_ + phi_0
            x_ = - r * math.cos(alpha)
            y_ = r * math.sin(alpha)
            x0_ = - r0 * math.cos(alpha)
            y0_ = r0 * math.sin(alpha)
            ax.annotate('', xy = (x0_, y0_), xytext = (x_, y_), arrowprops=dict(facecolor='b', width = 4 * pulseAmplitudes[n_]**2, headwidth = 8 * pulseAmplitudes[n_]**2, shrink=0.05))
    else:
        alpha_step = 2 * math.pi/(number/2)
        for n_ in range(number/2):
            alpha = alpha_step * n_ + phi_0
            x_ = - r * math.cos(alpha)
            y_ = r * math.sin(alpha)
            x0_ = - r0 * math.cos(alpha)
            y0_ = r0 * math.sin(alpha)
            ax.annotate('', xy = (x0_, y0_), xytext = (x_, y_), arrowprops=dict(facecolor='b', width = 6 * pulseAmplitudes[n_]**2, headwidth = 8 * pulseAmplitudes[n_]**2, shrink=0.05))
            
        for n_ in range(number/2, number):
            alpha = alpha_step * (n_ + 0.5) + phi_0
            x_ = - r * math.cos(alpha)
            y_ = r * math.sin(alpha)
            x0_ = - r0 * math.cos(alpha)
            y0_ = r0 * math.sin(alpha)
            ax.annotate('', xy = (x0_, y0_), xytext = (x_, y_), arrowprops=dict(facecolor='g', width = 4 * pulseAmplitudes[n_]**2, headwidth = 8 * pulseAmplitudes[n_]**2, shrink=0.05))
    
    
    ax=fig.add_subplot(235)
    densPhX /= np.amax(densPhX)
    surf = ax.imshow(densPhX, cmap = cmap2, extent=[Xmin, Xmax, Ymin, Ymax], norm=clr.LogNorm(clip='True'), vmin = 1e-2, vmax = 1)
    ax.set_xlabel('y, $\mu$m')
    ax.set_ylabel('z, $\mu$m')
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)
    ax.text(-3, 2, '(e)')

    if number <= 6:
        x_ = r
        x0_ = r0 
        ax.annotate('', xy = (x0_, 0), xytext = (x_, 0), arrowprops=dict(facecolor='b', shrink=0.05))
        ax.annotate('', xy = (-x0_, 0), xytext = (-x_, 0), arrowprops=dict(facecolor='b', shrink=0.05))
    else:
        alpha = 23.16 * math.pi/180.
        x_ = r 
        y_ = r * math.sin(alpha)
        x0_ = r0 
        y0_ = r0 * math.sin(alpha)
        
        ax.annotate('', xy = (x0_, y0_), xytext = (x_, y_), arrowprops=dict(facecolor='b', shrink=0.05))
        ax.annotate('', xy = (-x0_, y0_), xytext = (-x_, y_), arrowprops=dict(facecolor='b', shrink=0.05))
        ax.annotate('', xy = (x0_, -y0_), xytext = (x_, -y_), arrowprops=dict(facecolor='g', shrink=0.05))
        ax.annotate('', xy = (-x0_, -y0_), xytext = (-x_, -y_), arrowprops=dict(facecolor='g', shrink=0.05))

    ax=fig.add_subplot(236)
    densPhY /= np.amax(densPhY)
    surf = ax.imshow(densPhY, cmap = cmap2, extent=[Xmin, Xmax, Ymin, Ymax], norm=clr.LogNorm(clip='True'), vmin = 1e-2, vmax = 1)
    ax.set_xlabel('x, $\mu$m')
    ax.set_ylabel('z, $\mu$m')
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)
    ax.text(-3, 2, '(f)')

    if number <= 6:
        x_ = r
        x0_ = r0 
        ax.annotate('', xy = (x0_, 0), xytext = (x_, 0), arrowprops=dict(facecolor='b', shrink=0.05))
        ax.annotate('', xy = (-x0_, 0), xytext = (-x_, 0), arrowprops=dict(facecolor='b', shrink=0.05))
    else:
        alpha = 23.16 * math.pi/180.
        x_ = r 
        y_ = r * math.sin(alpha)
        x0_ = r0 
        y0_ = r0 * math.sin(alpha)
        
        ax.annotate('', xy = (x0_, y0_), xytext = (x_, y_), arrowprops=dict(facecolor='b', shrink=0.05))
        ax.annotate('', xy = (-x0_, y0_), xytext = (-x_, y_), arrowprops=dict(facecolor='b', shrink=0.05))
        ax.annotate('', xy = (x0_, -y0_), xytext = (x_, -y_), arrowprops=dict(facecolor='g', shrink=0.05))
        ax.annotate('', xy = (-x0_, -y0_), xytext = (-x_, -y_), arrowprops=dict(facecolor='g', shrink=0.05))
    
    plt.tight_layout()
    figname = os.path.join(picdir,'dens_%d_pw_%d_beams_%s.png' % (int(pwr), number, labels[idx]))
    print(figname)
    fig.savefig(figname ,dpi=128)
    plt.close()
    
    mp.rcParams.update({'font.size': 14})
    fig=plt.figure(figsize=(12, 3))
    ax=fig.add_subplot(121)
    dnEl /= np.amax(dnEl)
    surf = ax.imshow(dnEl, cmap = cmap1, extent=[phi_min, phi_max, theta_min, theta_max])#, norm=clr.LogNorm(clip='True'), vmin = 1e-3, vmax = 1)
    ax.set_xlabel('$\phi$')
    ax.set_ylabel('$\\theta$')
    if number == 4:
        ax.set_xticks([0, math.pi/2, math.pi, 3.*math.pi/2, 2*math.pi])
        ax.set_xticklabels(["0", "$\pi/2$", "$\pi$", "$3\pi/2$", "$2\pi$"])
    else:
        ax.set_xticks([0, math.pi/3, 2*math.pi/3, math.pi, 4*math.pi/3, 5*math.pi/3, 2*math.pi])
        ax.set_xticklabels(["0", "$\pi/3$", "$2\pi/3$", "$\pi$", "$4\pi/3$", "$5\pi/3$", "$2\pi$",])
    ax.set_yticks([0, math.pi/2, math.pi])
    ax.set_yticklabels(["0", "$\pi/2$", "$\pi$"])
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)
    ax.text(-math.pi/4, math.pi, '(a)')

    
    
    ax=fig.add_subplot(122)
    dnPh /= np.amax(dnPh)
    surf = ax.imshow(dnPh, cmap = cmap2, extent=[phi_min, phi_max, theta_min, theta_max])#, norm=clr.LogNorm(clip='True'), vmin = 1e-3, vmax = 1)
    ax.set_xlabel('$\phi$')
    ax.set_ylabel('$\\theta$')
    if number == 4:
        ax.set_xticks([0, math.pi/2, math.pi, 3.*math.pi/2, 2*math.pi])
        ax.set_xticklabels(["0", "$\pi/2$", "$\pi$", "$3\pi/2$", "$2\pi$"])
    else:
        ax.set_xticks([0, math.pi/3, 2*math.pi/3, math.pi, 4*math.pi/3, 5*math.pi/3, 2*math.pi])
        ax.set_xticklabels(["0", "$\pi/3$", "$2\pi/3$", "$\pi$", "$4\pi/3$", "$5\pi/3$", "$2\pi$",])
    ax.set_yticks([0, math.pi/2, math.pi])
    ax.set_yticklabels(["0", "$\pi/2$", "$\pi$"])
    
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)
    ax.text(-math.pi/4, math.pi, '(b)')
    plt.tight_layout()
    figname = os.path.join(picdir,'dn_%d_pw_%d_beams_%s.png' % (int(pwr), number, labels[idx]))
    print(figname)
    fig.savefig(figname ,dpi=128)
    plt.close()
    np.savetxt(os.path.join(sim,"el_dn_theta.txt"), np.sum(dnEl, axis=1))
    np.savetxt(os.path.join(sim,"el_dn_phi.txt"), np.sum(dnEl, axis=0))
    np.savetxt(os.path.join(sim,"ph_dn_theta.txt"), np.sum(dnPh, axis=1))
    np.savetxt(os.path.join(sim,"ph_dn_phi.txt"), np.sum(dnPh, axis=0))
    
    print(sim, "is processed")
    
    
    
    
file=open("Energy1p.txt","w")
file.write("label\t el1%\t pos1%\t ph1%\n")
for typ,el1,pos1,ph1 in zip(labels,En1pEl,En1pPos,En1pPh):
    file.write("%s %g %g %g\n" % (typ,el1,pos1,ph1))
file.close()


    
    
    
    
    
    
    
