#!/bin/bash

#remote host mvs100k
user=evefim
host=mvs100k.jscc.ru
wd_remote=/home4/pstorage1/ipfnn1/evgeny/picador/
#user=alba
#host=mvs100k.jscc.ru
#wd_remote=/home4/pstorage1/ipfnn1/alba/mvs10p/evgeny
#remote host akka
#user=argon
#host=akka.hpc2n.umu.se
#wd_remote=/home/a/argon/pfs/ifdtd/results

ssh_host=${user}@${host}

function create_dir
{
    if [ ! -d $1 ]; 
    then
	mkdir $1
    else
	echo "$1 already exists"
    fi
}

function get_remote_iteration()
{
#    list=`ssh $ssh_host "ls ${wd_remote}/$1/BasicOutput/data"`
    list="Electron2Dx Photon2Dx Electron2Dx_fine Photon2Dx_fine"
    echo "Copy BasicOutput data from $ssh_host:${wd_remote}/$1/BasicOutput/data"
    echo $list
    for dir in $list
    do
	echo "Combining $dir"
	ssh $ssh_host "cd ${wd_remote}/$1/BasicOutput/data/$dir && tar -cjf $dir.tar.bz2 $2.txt"
	create_dir data/$dir
	echo "Copying $dir"
	scp $ssh_host:${wd_remote}/$1/BasicOutput/data/$dir/$dir.tar.bz2 data/$dir
	cd data/$dir
	echo "Unpacking $dir"
	tar -xjf $dir.tar.bz2 && rm $dir.tar.bz2
	cd ../..
	ssh $ssh_host "cd ${wd_remote}/$1/BasicOutput/data/$dir && rm $dir.tar.bz2"
    done
}

function get_remote_statdata()
{
#    list=`ssh $ssh_host "ls ${wd_remote}/$1/statdata | grep ph"`
    echo "Copy statdata from $ssh_host:${wd_remote}/$1/statdata"
    create_dir statdata
    list="ph el"
    for dir in $list
    do

#	list1=`ssh $ssh_host "ls ${wd_remote}/$1/statdata/${dir}"`
	echo "Copy statdata from $ssh_host:${wd_remote}/$1/statdata/${dir}"
	list1="EnAngSp"
	create_dir statdata/$dir
	for dir1 in $list1
	do
	    echo "Combining ${dir}/${dir1}"
	    ssh $ssh_host "cd ${wd_remote}/$1/statdata/${dir}/${dir1} && tar -cjf $dir1.tar.bz2 $2.txt"
	    create_dir statdata/${dir}/${dir1}
	    echo "Copying ${dir}/${dir1}"
	    scp $ssh_host:${wd_remote}/$1/statdata/$dir/$dir1/$dir1.tar.bz2 statdata/$dir/$dir1
	    cd statdata/$dir/$dir1
	    echo "Unpacking $dir/$dir1"
	    tar -xjf $dir1.tar.bz2 && rm $dir1.tar.bz2
	    cd ../../..
	    ssh $ssh_host "cd ${wd_remote}/$1/statdata/$dir/$dir1 && rm $dir1.tar.bz2"
	done
    done
}

function get_remote_config()
{
    echo "Copy config $ssh_host:${wd_remote}/$1/ParsedInput.txt"
    scp $ssh_host:${wd_remote}/$1/ParsedInput.txt .
    scp $ssh_host:${wd_remote}/$1/Input.txt .
}

create_dir $2
cd $2
create_dir data
create_dir pics
get_remote_config $1
get_remote_statdata $1 $3
#get_remote_iteration $1 $3
#../statdata.py
#../series.py
cd ..
