#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import sys
import math
import os, os.path
import utils

def main():

    config = utils.get_config("ParsedInput.txt")

    dt = float(config['TimeStep'])
    ev = float(config['eV'])
    omega = float(config['Omega'])
    duration = float(config['Duration']) #s
    delay = float(config['delay']) #s
    power = float(config['PeakPower'])*1e-7 #W
    Emax = float(config['QEDstatistics.Emax'])
    Emin = float(config['QEDstatistics.Emin'])
    N_E = int(config['QEDstatistics.OutputN_E'])
    N_Phi = int(config['QEDstatistics.OutputN_phi'])
    N_Th = int(config['QEDstatistics.OutputN_theta'])
    outStep = int(config['QEDstatistics.OutputIterStep'])
    tp = duration*1e15
    tdelay = delay*1e15

    path='.'
    elpath='/el/'
    elmaxpath='/el_max/'
    posmaxpath='/pos_max/'
    phmaxpath='/ph_max/'
    elsmaxpath='/elSmax/'
    possmaxpath='/posSmax/'
    phsmaxpath='/phSmax/'
    elnmaxpath='/elNmax/'
    posnmaxpath='/posNmax/'
    phnmaxpath='/phNmax/'
    pospath = '/pos/'
    phpath = '/ph/'
    phangle = '/ph_angle/'
    pht = '/phT/'
    picspath='pics/' 

    nmax = utils.num_files(path + utils.enspsphpath)
    
    print 'Found ' + str(nmax) + ' files'

 
    el_en = np.zeros(nmax)
    ph_en = np.zeros(nmax)
    pos_en = np.zeros(nmax)
    ez_a = np.zeros(nmax)
    delay = float(config['delay'])
    duration = float(config['Duration'])
    T = 2*math.pi/omega*1e15
    nit = int(config['QEDstatistics.OutputIterStep'])
    dt1 = float(config['TimeStep'])*nit
    ax_ = utils.create_axis(nmax, dt1) 
    for i in range(nmax):
        t = i*dt1
        ez_a[i] = math.exp(-2.*math.log(2)*(t - delay)/duration*(t - delay)/duration)
    for i in range(1,nmax):
        sp_ = utils.bo_file_load(path + utils.enspsphpath,i,fmt='%.4f',verbose = 1)
        ph_en[i] += ph_en[i-1] + np.sum(sp_) 
        sp_ = utils.bo_file_load(path + utils.el_enspsphpath,i,fmt='%.4f',verbose = 1)
        el_en[i] += el_en[i-1] + np.sum(sp_)
        sp_ = utils.bo_file_load(path + utils.pos_enspsphpath,i,fmt='%.4f',verbose = 1)
        pos_en[i] += pos_en[i-1] + np.sum(sp_)

    en = max(ph_en)*1e-7
    pulse_energy = 1.05*duration*power
    print en, pulse_energy, en/pulse_energy
    fig, ax1 = plt.subplots()
    pph, = ax1.plot(ax_,ph_en, 'r')
    pel, = ax1.plot(ax_,el_en, 'g')
    ppos, = ax1.plot(ax_,pos_en, 'b')
    
    ax2 = ax1.twinx()
    pu, = ax2.plot(ax_, ez_a, 'k')
    plt.legend([pph, pel, ppos, pu], ['Ph. energy','El. energy','Pos. energy','Pulse'], loc=2)
    ax1.set_xlabel('Time, periods')
    ax1.set_ylabel('Full photon energy, erg')
    plt.figtext(0.2,0.93,str('Pulse energy=%.1f J; Photon energy=%.1lf J' % (pulse_energy, en)) , fontsize = 10)
    plt.figtext(0.6,0.5,str('Efficiency=%.1lf%%' % (en/pulse_energy*100)) , fontsize = 16)
    picname = picspath + "energy.png"
    plt.savefig(picname)
    plt.close()
    f = open('efficiency.txt', 'w')
    f.write("%le %le %le\n"%(ph_en[-1]*1e-7/pulse_energy, el_en[-1]*1e-7/pulse_energy, pos_en[-1]*1e-7/pulse_energy))
    f.close()

if __name__ == '__main__':
    main()
