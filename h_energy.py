#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import shutil
import os
import scipy.signal.signaltools as sigtool
import numpy 

def read_ang(file,nx,ny,mult=1.):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index]*mult)
        field.append(row)
    return field

def read_field(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)/norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1e-9)
        field.append(row)
    return field

def read_file(file, de):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
    if 'tr' in file and not 'fs' in file:
        array[0] = 0
#        print 'removed'
        array, de = adjust_array(array, de)
        
    for i in range(len(array)):
        nph  += array[i]
    for i in range(len(array)):
        array[i] *= (i+0.425)
   
    return array, nph

def read_file_sph(file, de):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
    if 'tr' in file and not 'fs' in file:
        array[0] = 0
#        print 'removed'
        array, de = adjust_array(array, de)
       
    for i in range(len(array)):
        nph  += array[i]/(i+0.425)/de
    for i in range(len(array)):
        array[i] /= de
#        array[i] = array[i]/(i+0.425)/de
   
    return array, nph

def check_length(file):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
       
    return len(array)

def adjust_array(array, de):
    n = len(array)
    adj_size = 10
    m = n/adj_size
    a = [0]*m
    for i in range(m):
        tmp = 0
        for j in range(adj_size):
            tmp += array[i*adj_size + j]
        a[i] = tmp
    return a, de*adj_size

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def find_max_energy_full(array,step):
    n = len(array)
    full = 0
    imax = 0
    summ = 0
    nfull = 0
    av_en = 0
    n_av = 0
    for i in range(n):
        nfull += array[i]/(i+0.425)
        full += array[i]*step
    
    if nfull > 0:
        av_en = full/nfull
        n_av = int(av_en/step)

    for i in range(1,n):
        summ = summ + array[-i]*(n-i+0.425)*step
        if summ > 0.01*full:
            imax = i
            break
    if imax == 0:
        max_en = 0
    else:
        max_en = (n-imax+0.37) * step
#    en1 = max_en*1e-3
#    ratio = en1/step
#    print max_en, av_en
    ratio = 1
    return av_en*1000, max_en, array[n_av], array[-imax]

def read_one_spectrum_sph(path1, nmin):
    config = utils.get_config(path1 + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV']) 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
#    print num, T
    nph_t = []
    nph1_t = []
    ne_t = []
    np_t = []
    step = x0*y0*1e15/T 
    
    dt = y0*float(config['QEDstatistics.OutputIterStep'])*1e15/T

    path = '/statdata/ph/EnSpSph/'
#    nepath = '/data/NeTrap/'
#    nppath = '/data/NposTrap/'
    emin = float(config['QEDstatistics.Emin']) 
    emax = float(config['QEDstatistics.Emax']) 
    ne = int(config['QEDstatistics.OutputN_E'])
    de = (emax - emin)/ne
   
    t = 0
    
    n = check_length(path1 + path + '%.4f.txt'%nmin)
    if 'tr' in path1 and not 'fs' in path1:
        n /= 10
        de *= 10
    axis = create_axis(n, de/ev*1e-9)

    spectrum1 = [0]*n
    nn = 0
    for i in range(nmin,nmin+num):
        name = path1 + path + '%.4f.txt'%i
#        print name
        sp1, nph = read_file_sph(name, de)
        nn += nph
        for k in range(n):
            if nph > 0:
                spectrum1[k] += sp1[k]/nph
    for k in range(n):
        spectrum1[k] /= num
    return spectrum1, axis

def read_one_spectrum(path1, nmin):
    config = utils.get_config(path1 + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV']) 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
#    print num, T
    nph_t = []
    nph1_t = []
    ne_t = []
    np_t = []
    step = x0*y0*1e15/T 
    
    dt = y0*float(config['QEDstatistics.OutputIterStep'])*1e15

    path = '/statdata/ph/EnSp/'
#    nepath = '/data/NeTrap/'
#    nppath = '/data/NposTrap/'
    emin = float(config['QEDstatistics.Emin']) 
    emax = float(config['QEDstatistics.Emax']) 
    ne = int(config['QEDstatistics.OutputN_E'])
    de = (emax - emin)/ne
   
    t = 0
    
    n = check_length(path1 + path + '%.4f.txt'%nmin)
    if 'tr' in path1 and not 'fs' in path1:
        n /= 10
        de *= 10
    axis = create_axis(n, de/ev*1e-9)

    spectrum1 = [0]*n
    nn = 0
    for i in range(nmin,nmin+num):
        name = path1 + path + '%.4f.txt'%i
#        print name
        sp1, nph = read_file(name, de)
        nn += nph
        for k in range(n):
            if nph > 0:
                spectrum1[k] += sp1[k]/nph
    for k in range(n):
        spectrum1[k] /= n
    return spectrum1, axis

def max2d(array):
    return max([max(x) for x in array])

def sum_ang_energy(array):
    nx = len(array)
    ny = len(array[0])
    res = 0
    for i in range(nx):
        for j in range(ny):
            res = res + array[i][j]
    return res

def main():
    picspath = 'pics'
    path = '.'
    enpath = '/statdata/ph/EnSp/'
    angpath = '/statdata/ph/angSpSph/'
    angpath_el = '/statdata/el/angSpSph/'
    angpath_pos = '/statdata/el/angSpSph/'
    ezpath = 'data/E2z'
    nexpath = 'data/Electron2Dx'
    config = utils.get_config(path + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV'])
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    dv = 2*dx*dy*dz 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
    dt = (x0*y0*1e15)/T
    num = len(sys.argv)

    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(ezpath) - 20
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(ezpath) - 20
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'

#    nmin = 0	
#    nmax = utils.num_files(path + enpath)
#    nmin = 340
#    nmax = 400
  
    av_ = []
    max_ = []
    av_s_ = []
    max_s_ = []
    ax_ = []
    ez_t = []
    ne_t = []
    power_t = []
    power_el_t = []
    power_pos_t = []
    t = 0
    omega = float(config['Omega'])
    phi = int(config['QEDstatistics.OutputN_phi'])
    theta = int(config['QEDstatistics.OutputN_theta'])
    nit = int(config['QEDstatistics.OutputIterStep'])
    dt1 = float(config['TimeStep'])*nit
    electronMass = 9.10938e-28
    electronCharge = 4.8032e-10
    ncr = electronMass * omega * omega/(8. * math.pi * electronCharge * electronCharge)
    const_a_p = 7.81441e-9
    a0 = const_a_p * math.sqrt(ppw*1e22)
    print a0
    mp.rcParams.update({'font.size': 16})
    for k in range(nmin,nmax-num):
        t += dt
#        spectrum, axis = read_one_spectrum(path, k)
        spectrum1, axis1 = read_one_spectrum_sph(path, k)
        name = ezpath + '/' + "%06d.txt" % (k,)
        print name
        fieldez_x = read_field(name,nx,ny)
        ez = fieldez_x[nx/2][ny/2]
        name = nexpath + '/' + "%06d.txt" % (k,)
        ne_x = read_field(name,nx,ny)
        ez_t.append(ez/(3.e11*math.sqrt(ppw/10.)))
        ne = max2d(ne_x)/dv/(ncr*a0)
        ne_t.append(ne) 
        ang = read_ang(path + angpath + '%.4f.txt' % (k,), phi, theta)
#        ang_el = read_ang(path + angpath_el + '%.4f.txt' % (k,), phi, theta)
#        ang_pos = read_ang(path + angpath_pos + '%.4f.txt' % (k,), phi, theta) 
        power = sum_ang_energy(ang)/dt1*1e-7*1e-15
#        power_el = sum_ang_energy(ang_el)/dt1*1e-7*1e-15
#        power_pos = sum_ang_energy(ang_el)/dt1*1e-7*1e-15
        power_t.append(power)
#        power_el_t.append(power_el)
#        power_pos_t.append(power_pos)
#        fig = plt.figure(num=None)
#        ax = fig.add_subplot(1,1,1)
#        ms = max(spectrum)
#        ms1 = max(spectrum1)
#        s = [x/ms for x in spectrum]
#        s1 = [x/ms1 for x in spectrum1]
#        ax.plot(axis, s)
#        ax.plot(axis1, s1)
#        ax.set_yscale('log')
#        plt.show()

#        step = axis[1] - axis[0]
        step1 = axis1[1] - axis1[0]  
#        print step, step1
#        av_en, max_en, tmp, tmp1 = find_max_energy_full(spectrum,step)
        av_en_s, max_en_s, tmp_s, tmp1_s = find_max_energy_full(spectrum1,step1)
#        print 'Av = ' + str(av_en) + ' Max = ' + str(max_en)
#        print 'Av_s = ' + str(av_en_s) + ' Max_s = ' + str(max_en_s)
        ax_.append(t)
#        max_.append(max_en)
#        av_.append(av_en)
        max_s_.append(max_en_s)
        av_s_.append(av_en_s)
    fig = plt.figure(num=None)
    mp.rcParams.update({'font.size': 12})

    picname = 'pics/' + "stat1.png"
    ax = fig.add_subplot(2,1,1)
    ez, = ax.plot(ax_, ez_t, 'r')
    ax.set_xlabel('t/T')
#    ax.set_xlim([0,30])
    ax.set_ylabel('$E_z/a_0$')
    
    nx = ax.twinx()
    ne, = nx.plot(ax_, ne_t, 'b')
#    nx.set_xlim([0,30])
    nx.set_ylabel('$N_e/(N_{cr} a_0)$')
#    nx.set_yscale('log')
#    nx.plot(axis_t, np_t, 'k', label = 'Np')
    nx.legend([ez, ne], ['|Ez|', '$N_e$'], loc='upper right', shadow=True)
    ax = fig.add_subplot(2,1,2)
    m, = ax.plot(ax_, max_s_, 'g')
    ax.set_ylabel('Max photon energy, GeV')
    ax.set_xlabel('t/T')
#    ax.set_xlim([0,30])
    ax2 = ax.twinx()
    p, = ax2.plot(ax_, power_t, 'k')
#    ax2.set_xlim([0,30])
#    p_el, = ax2.plot(ax_, power_el_t, 'c')
#    p_pos, = ax2.plot(ax_, power_pos_t, 'y')
    plt.legend([m, p], ['Energy', '$P_{ph}$'], loc='lower left', shadow=True)
    ax2.set_ylabel('Power emitted by photons, PW')
    ax2.set_xlabel('t/T')
    plt.savefig(picname)
    plt.close()
    out = open('h_stat.txt', 'w')
    for i in range(len(ax_)):
        out.write('%lf %lf %le %le %le\n'%(ax_[i], max_s_[i], ez_t[i], ne_t[i], power_t[i]))
    out.close()
if __name__ == '__main__':
    main()
