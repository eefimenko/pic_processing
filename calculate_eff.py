#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.cm as cm
import matplotlib.colors as colors
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def read_trapped(filename):
    f = open(filename, 'r')
    tr = []
    for line in f:
        tmp = line.split()
        tr.append(float(tmp[1]))
    return tr

def main():
    config = utils.get_config('.' + "/ParsedInput.txt")
    pwr = float(config['PeakPowerPW'])
    tr = read_trapped('./trapped_%d.dat'%pwr)
    fig = plt.figure(num=None)
    print max(tr)
    ax = fig.add_subplot(1,1,1)
    ax.plot(tr)
    
    plt.show()
    
if __name__ == '__main__':
    main()
