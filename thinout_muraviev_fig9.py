#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import numpy as np
import os
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

def read_data(filename, norm=100000., t0 = 12800):
    f = open(filename)
    axis = []
    data = []
    idx = 0
    
    for line in f:
        if line.strip() == "":
            continue
        tmp = line.split()
        if float(tmp[1])/norm > 1.1:
            break
        axis.append(int(tmp[0])-12800)
        data.append(float(tmp[1])/norm)
    return np.array(axis), np.array(data)
        

def main():
    mp.rcParams.update({'font.size': 10})
    fontsize = 10
    datapath = '/home/evgeny/Dropbox/pinch_thinout/DataForPics'
    picspath = '/home/evgeny/Dropbox/pinch_thinout/'
    fig9path = 'Fig9'

    fig1 = plt.figure(num=None, figsize = (6,3.5))
    ax1 = fig1.add_subplot(1,1,1)
        
    ax1.set_ylim([0.9, 1.1])
    ax1.set_xlim([0, 6000])
    ax1.set_yticks([0.9, 0.95, 1, 1.05, 1.1])
    lw = 0.7

    ax_simple, data_simple = read_data(os.path.join(datapath, fig9path, 'FieldEnergy_simple.txt'))
    ps, = ax1.plot(ax_simple, data_simple, linewidth = lw, color = 'navy')

    ax_simple, data_simple = read_data(os.path.join(datapath, fig9path, 'FieldEnergy_simple3.txt'))
    ps, = ax1.plot(ax_simple[:-1], data_simple[:-1], linewidth = lw, color = 'navy')

    ax_simple, data_simple = read_data(os.path.join(datapath, fig9path, 'FieldEnergy_simple2.txt'))
    ps, = ax1.plot(ax_simple[:-1], data_simple[:-1], linewidth = lw, color = 'navy')

    ax_simple, data_simple = read_data(os.path.join(datapath, fig9path, 'FieldEnergy_simple2_renamed.txt'))
    ps, = ax1.plot(ax_simple[:-1], data_simple[:-1], linewidth = lw, color = 'navy')

    ax_simple, data_simple = read_data(os.path.join(datapath, fig9path, 'FieldEnergy_simple0.txt'))
    ps, = ax1.plot(ax_simple, data_simple, linewidth = lw, color = 'navy')

    ax_simple, data_simple = read_data(os.path.join(datapath, fig9path, 'FieldEnergy_simple00.txt'))
    ps, = ax1.plot(ax_simple, data_simple, linewidth = lw, color = 'navy')
    
    ax_leveling, data_leveling = read_data(os.path.join(datapath, fig9path, 'FieldEnergy_leveling.txt'))
    pl, = ax1.plot(ax_leveling, data_leveling, linewidth = lw, color = 'g', label = 'leveling')

    ax_conserv, data_conserv = read_data(os.path.join(datapath, fig9path, 'FieldEnergy_conserv.txt'))
    pl, = ax1.plot(ax_conserv, data_conserv, linewidth = lw, color = 'purple', label = 'conserv')
    
    ax_globalLev, data_globalLev = read_data(os.path.join(datapath, fig9path, 'FieldEnergy_globalLev.txt'))
    pg, = ax1.plot(ax_globalLev, data_globalLev, linewidth = lw, color = 'orange', label = 'globalLev')
    
    ax_numberT, data_numberT = read_data(os.path.join(datapath, fig9path, 'FieldEnergy_numberT.txt'))
    pn, = ax1.plot(ax_numberT, data_numberT, linewidth = lw, color = 'r', label = 'numberT')
        
    ax_energyT, data_energyT = read_data(os.path.join(datapath, fig9path, 'FieldEnergy_energyT.txt'))
    pe, = ax1.plot(ax_energyT, data_energyT, linewidth = lw, color = 'c', label = 'energyT')

    ax1.annotate('simple',
                 xy=(700, 1.01), xycoords='data',
                 xytext=(1800, 1.07), textcoords='data',
                 arrowprops=dict(arrowstyle="->", linewidth = 0.5),
                 horizontalalignment='right', verticalalignment='top', fontsize = fontsize-2)
    
    ax1.annotate('simple',
                 xy=(450, 1.06), xycoords='data',
                 xytext=(1800, 1.07), textcoords='data',
                 arrowprops=dict(arrowstyle="->", linewidth = 0.5),
                 horizontalalignment='right', verticalalignment='top', fontsize = fontsize-2)

    ax1.annotate('simple',
                 xy=(350, 1.03), xycoords='data',
                 xytext=(1800, 1.07), textcoords='data',
                 arrowprops=dict(arrowstyle="->", linewidth = 0.5),
                 horizontalalignment='right', verticalalignment='top', fontsize = fontsize-2)
    
    ax1.legend(loc = 'upper right', fontsize = fontsize-2, frameon = False)
   
    #plt.gca().add_artist(legend1)
    ax1.set_xlabel('Iteration')
    ax1.set_ylabel('Field Energy, a.u.')
    ax1.text(-10, 1.45, '(b)')
    
    name1 = 'FieldEnergy_new'
    
    picname1 = picspath + '/' + name1 + ".png"
    
    #plt.legend(loc = 'upper left', fontsize = 10)
    fig1.tight_layout()
    
    fig1.savefig(picname1, dpi = 256)
    
      
#    plt.show()

if __name__ == '__main__':
    main()
