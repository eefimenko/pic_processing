#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import matplotlib.image as mpimg
import matplotlib as mp
import shutil
import os
import scipy.signal.signaltools as sigtool
import numpy as np
import copy
import matplotlib.colors as clr

def main():
    n = len(sys.argv) - 1
    picspath = 'pics'
    enpath = '/statdata/ph/EnSp/'
    angpath = '/statdata/ph/angSpSph/'
    angpath_el = '/statdata/el/angSpSph/'
    angpath_pos = '/statdata/el/angSpSph/'
    ezpath = 'data/E2z'
    nexpath = 'data/Electron2Dx'
    electronMass = 9.10938e-28
    electronCharge = 4.8032e-10
    colors = ['b', 'g', 'k', 'r', 'c', 'm', 'y']
    const_a_p = 7.81441e-9
    bounds = [5, 10, 15, 20, 25, 30, 35, 40, 45]
    axticks = [5, 10, 15, 20, 25, 30, 35, 40, 45]
    axticklabels = ['$%d$'%(x) for x in axticks]
    mp.rcParams.update({'font.size': 26})
    fig1 = plt.figure(num=None, figsize = (10,5))
    ax1 = fig1.add_subplot(1,1,1)
    ax1.set_ylabel('$|E|, \\times 10^{11} statV/cm$ ')
    ax1.set_yticks([0,1,2,3])
    ax1.set_yticklabels(['$0$','$1$','$2$','$3$'])
    ax1.set_xlabel('$t/T$')
    ax1.set_xlim([5,45])
    ax1.set_xticks(axticks)
    ax1.set_xticklabels(axticklabels)
    ax1.text(2, 3.2, '$(c)$')
    fig2 = plt.figure(num=None, figsize = (10,5))
    ax2 = fig2.add_subplot(1,1,1)
    ax2.set_ylabel('$N_e, \\times 10^{10}$')
    ax2.set_yticks([0,2,4,6])
    ax2.set_yticklabels(['$0$','$2$','$4$','$6$'])
    ax2.set_xlabel('$t/T$')
    ax2.set_xlim([5,45])
    ax2.set_xticks(axticks)
    ax2.set_xticklabels(axticklabels)
    fig3 = plt.figure(num=None, figsize = (10,5))
    ax3 = fig3.add_subplot(1,1,1)
    ax3.set_ylabel('$N_p, \\times 10^{10}$')
    ax3.set_yticks([0,2,4,6])
    ax3.set_yticklabels(['$0$','$2$','$4$','$6$'])
    ax3.set_xlabel('$t/T$')
    ax3.set_xlim([5,45])
    ax3.set_xticks(axticks)
    ax3.set_xticklabels(axticklabels)
    ax3.text(2, 5.4, '$(d)$')
    fig4 = plt.figure(num=None, figsize = (10,5))
    ax4 = fig4.add_subplot(1,1,1)
    ax4.set_ylabel('$J_z, MA$')
    ax4.set_yticks([0,2,4,6,8,10])
    ax4.set_yticklabels(['$0$','$2$','$4$','$6$','$8$', '$10$'])
    ax4.set_xlabel('$t/T$')
    ax4.set_xlim([5,45])
    ax4.set_xticks(axticks)
    ax4.set_xticklabels(axticklabels)
    fig5 = plt.figure(num=None, figsize = (10,5))
    ax5 = fig5.add_subplot(1,1,1)
    ax5.set_ylabel('$n_{p}, cm^{-3}$')
    ax5.set_yscale('log')
    ax5.set_xlabel('$t/T$')
    ax5.set_xlim([5,45])
    ax5.set_xticks(axticks)
    ax5.set_xticklabels(axticklabels)
    
   
    for j in range(n):
        path = sys.argv[j+1]
        label = '$\\alpha = \\beta = 0$'
        for n_ in [10,20,30]:
            if str(n_) in path:
                label = '$\\alpha = \\beta = 1/%d$' % (n_)
        
        config = utils.get_config(path + "/ParsedInput.txt")
        x0 = int(config['BOIterationPass'])
        y0 = float(config['TimeStep']) 
        omega = float(config['Omega'])
        ppw = int(config['PeakPowerPW'])
        ev = float(config['eV'])
        dx = float(config['Step_X'])
        dy = float(config['Step_Y'])
        dz = float(config['Step_Z'])
        nx = int(config['MatrixSize_X'])
        ny = int(config['MatrixSize_Y'])
        nz = int(config['MatrixSize_Z'])
        dv = 2*dx*dy*dz 
        T = 2 * math.pi/omega*1e15
        num = int(T/(x0*y0*1e15))
        dt = (x0*y0*1e15)/T
        num = len(sys.argv)
        ncr = electronMass * omega * omega/(8. * math.pi * electronCharge * electronCharge)
        ax5.set_yticks([1e21, 5*ncr, 1e22, 50*ncr, 1e23, 500*ncr,1e24, 5000*ncr, 1e25, 3e25])
        ax5.set_yticklabels(['$10^{21}$','$5n_{c}$','$10^{22}$','$50n_{c}$','$10^{23}$','$500n_{c}$','$10^{24}$','$5000n_{c}$', '$10^{25}$', '$3 \\times 10^{25}$'])
        ax_ = []
        ax1_ = []
        ez_t = []
        ne_t = []
        pow_t = []
        max_t = []
        av_t = []
        br_full_t = []
        br_100mev_t = []
        br_1gev_t = []
        br_full1_t = []
        br_100mev1_t = []
        br_1gev1_t = []
   
        t = 0
        omega = float(config['Omega'])
        delay = float(config['delay'])
        duration = float(config.get('Duration', 0))
        T = 2*math.pi/omega*1e15
        phi = int(config['QEDstatistics.OutputN_phi'])
        theta = int(config['QEDstatistics.OutputN_theta'])
        nit = int(config['QEDstatistics.OutputIterStep'])
        dt1 = float(config['TimeStep'])*nit
        wl = float(config['Wavelength'])
        dx = float(config['Step_X'])
        dy = float(config['Step_Y'])
        dz = float(config['Step_Z'])
        ev = float(config['eV'])
        ne = int(config['QEDstatistics.OutputN_E'])
        nt = int(config['QEDstatistics.OutputN_theta'])
        tmax = float(config.get('QEDstatistics.ThetaMax',0))*180./math.pi
        tmin = float(config.get('QEDstatistics.ThetaMin',0))*180./math.pi
    
        emax = float(config['QEDstatistics.Emax'])/ev*1e-9
        emin = float(config['QEDstatistics.Emin'])/ev*1e-9
        de = (emax - emin)/ne
        dt = (tmax - tmin)/nt
        dee1 = de*1e9*ev # erg
        dtt1 = dt*math.pi/180.
        dv = 2*dx*dy*dz
       
        a0 = const_a_p * math.sqrt(ppw*1e22)
        print a0, 1.18997e+8*a0
        mp.rcParams.update({'font.size': 24})
        S = 2 * math.pi * 0.1 * 0.1 * wl * wl * 100 # square of source mm^2, r = 0.1 lambda 
        ds = 1.5e4 # mrad^2
        f = open(path + '/br_t.dat', 'r')
        i = 0
        for line in f:
            tmp = line.split()
            ax_.append(float(tmp[0])/T)
            ez_t.append(float(tmp[1]))
            pow_t.append(float(tmp[2]))
            max_t.append(float(tmp[3])/(0.511e-3))
            av_t.append(float(tmp[4]))
            ax1_.append((float(tmp[0])-39*dt1*1e15)/T)
            br_full_t.append(float(tmp[5]))
            br_100mev_t.append(float(tmp[6]))
            br_1gev_t.append(float(tmp[7])/1e23)
            br_full1_t.append(float(tmp[5])/S/ds)
            br_100mev1_t.append(float(tmp[6])/S/ds)
            br_1gev1_t.append(float(tmp[7])/S/ds)
  
    
        filename = 'jz_anls_new'
        nmin,nmax = utils.find_min_max_from_directory(path + '/' + utils.jz_zpath)
        jplus = np.zeros(nmax)
        jminus = np.zeros(nmax)
        jfull = np.zeros(nmax)
        jmax = np.zeros(nmax)
        nmin = utils.read_tseries(path,filename, jplus, jminus, jfull, jmax)
        c2 = 3.33e-10*dx*dy*1e-6
        for i in range(len(jfull)):
            jfull[i] = abs(jfull[i])*c2

        np_t = utils.ts(path, utils.npzpath, name='npz',
                        tstype='max', shape = (nx,ny), verbose=True)/dv
        netr_t = utils.ts(path, utils.netpath, name='ne_trapped',
                          tstype='sum', verbose=True)/1e10
        npostr_t = utils.ts(path, utils.npostpath, name='npos_trapped',
                            tstype='sum', verbose=True)/1e10
        print 'Max ', max(npostr_t)
        print de, int(2./de), (2.*1e-3/de)
        fraction = 2.*1e-3/de
        gev2_pos = int(2./de)
        print max(ez_t), max(br_1gev_t), max(br_1gev1_t)
        print br_1gev1_t[gev2_pos]*fraction 
   
        current_dir = os.path.realpath(path)
        field_coeff = 1
        if 'shifted' in current_dir or 'distorted' in current_dir or '12beams' in current_dir:
            field_coeff = 0.9 
    
        ez_a = [0] * len(ez_t)
        for i in range(len(ez_t)):
            t = i*dt1
            ez_a[i] = field_coeff*math.exp(-2.*math.log(2)*(t - delay)/duration*(t - delay)/duration)*3*math.sqrt(ppw/10.)
          
        f1, = ax1.plot(ax_, utils.env(np.array(ez_t)/1e11, ax_), label = label, color = colors[j])
        if j == n-1:
            f2, = ax1.plot(ax_, ez_a, 'b:', label = 'vacuum')
            thr = 3*math.sqrt(7.2/10.)
            f3, = ax1.plot([ax_[0], ax_[-1]], [thr,thr], 'b--', label = 'Threshold')     
        f, = ax2.plot(ax1_, netr_t, label = label, color = colors[j])
        fp, = ax3.plot(ax1_, npostr_t, label = label, color = colors[j])
   
        f4, = ax4.plot(ax1_, utils.env(jfull, ax1_), label = label, color = colors[j])
       
        p1, = ax5.plot(ax1_, np_t, color = colors[j], label = label)
        if j == n-1:
            thrne = 7e24
            n3, = ax5.plot([ax1_[0], ax1_[-1]], [thrne,thrne], label = 'Threshold')
 
    ax1.legend(loc='upper left', fontsize = 19)
    name = "cmppulse_e"
    picname = picspath + '/' + name + ".png"
    fig1.tight_layout()
    fig1.savefig(picname)
    ax2.legend(loc='upper left', fontsize = 20)
    name = "cmppulse_Ne"
    picname = picspath + '/' + name + ".png"
    fig2.tight_layout()
    fig2.savefig(picname)
    ax3.legend(loc='upper left', fontsize = 20)
    name = "cmppulse_Np"
    picname = picspath + '/' + name + ".png"
    fig3.tight_layout()
    fig3.savefig(picname)
    ax4.legend(loc='upper left', fontsize = 19)
    name = "cmppulse_jz"
    picname = picspath + '/' + name + ".png"
    fig4.tight_layout()
    fig4.savefig(picname)
    ax5.legend(loc='upper left', fontsize = 20)
    name = "cmppulse_np"
    picname = picspath + '/' + name + ".png"
    fig5.tight_layout()
    fig5.savefig(picname)

  

if __name__ == '__main__':
    main()
