# -*- coding: utf-8 -*-
"""
Created on Thu Dec 24 14:43:49 2020

@author: Алексей
"""
#import sys
import os
import math
import zipfile
import numpy as np
import re
#import matplotlib.pyplot as plt
import sys

def ReadZipTxt(pathZip,fileInZip,nx,ny=-1):    
    archive = zipfile.ZipFile(pathZip,'r')
    file = archive.read(fileInZip)
    archive.close()
    data=file.decode('utf-8').split()
    return np.array(list(map(float,data))).reshape(ny,nx)


def InputDictionary(pathToInput,dictInput):
    regString=r"""Setting (?:variable|string)\S*\s*(?P<nameVar>\S*)\s*=\s*(?P<valueVar>\S+)\s*"""
    regExpr=re.compile(regString)
    inpFile=open(pathToInput,'r')
    
    for line in inpFile:
        foundVar=regExpr.search(line)
        if foundVar:
            curDict=foundVar.groupdict()
            dictInput[curDict['nameVar']]=curDict['valueVar']
    inpFile.close()
    return 0

def Add1DPlot(data,xax,xrangeFrame,ax,color='k',yrangeFrame=None,linestyle='-',linewidth=1,xscale='linear',yscale='linear',marker=None,fillstyle='full',markersize=4):
    ax.set_xlim(xrangeFrame[0],xrangeFrame[1])
    if (yrangeFrame):
        ax.set_ylim(yrangeFrame[0],yrangeFrame[1])
    else:
        ax.set_ylim(data.min(),data.max())
    ax.set_xscale(xscale)
    ax.set_yscale(yscale)
    
    if (marker):
        lines, =ax.plot(xax,data,color=color,linestyle=linestyle,linewidth=linewidth,marker=marker,fillstyle=fillstyle,markersize=markersize)
    else:
        lines, =ax.plot(xax,data,color=color,linestyle=linestyle,linewidth=linewidth)
    return lines


relfold=".."
numAnalysedPeriods=2
regime = "zip" # "txt"
if __name__ == "__main__":
    try:
        relfold=sys.argv[1]
        numAnalysedPeriods=int(sys.argv[2])
        regime=sys.argv[3]
    
    except:
        print("default parameters")
print("Parameters of work")
print("path=",relfold)
print("numAnalysedPeriods=",numAnalysedPeriods)
print("regime=",regime)
simulations=[]

for name in os.listdir(relfold):
    if (os.path.isdir(relfold+os.path.sep+name) and ('Input' in name)):
        simulations.append(name)
numSimul=len(simulations)
print("Detected %d simulations" % numSimul)
print(simulations)
PGrRt=[]
### calculation of growth rate in each simulation
for sim in simulations:
    print("Analysis of ",sim)
    ###read input file
    path=relfold+os.path.sep+sim+os.path.sep
    inputDict={}
    InputDictionary(path+"ParsedInput.txt",inputDict)
    pwr=float(inputDict["PeakPowerPW"])
    w=float(inputDict["Omega"])
    dt=float(inputDict["TimeStep"])
    iterBetwSaves=int(inputDict["BOIterationPass"])
    iterst=int(inputDict["BOIterationStart"])
    nx=int(inputDict["NeTrap.SetMatrixSize_0"])
    numst=int(iterst/iterBetwSaves)
    period=2*math.pi/w
    numSavesperPeriod=int(period/(dt*iterBetwSaves)+0.5)

    
    ###calculation of number of saved files
    if regime=="zip":
        zipfold=path+"BasicOutput"+os.path.sep+"data.zip"
        zipFile=zipfile.ZipFile(zipfold,"r")
        names=[]
        for name in zipFile.namelist():
            if ("NeTrap" in name):
                names.append(name)
        zipFile.close()
    if regime=="txt":
        names=os.listdir(path+"BasicOutput"+os.path.sep+"data"+os.path.sep+"NeTrap")
        
    
    ###calculation of growth rate
    if regime=="zip":
        finnumFile=len(names)+numst-2
    if regime=="txt":
        finnumFile=len(names)+numst-1
    stnumFile=finnumFile-numAnalysedPeriods*numSavesperPeriod
    if (regime=="zip"):
        finpartnum=ReadZipTxt(zipfold,("data/NeTrap/%.6d.txt"%finnumFile),nx).sum()
        finpartnum+=ReadZipTxt(zipfold,("data/NposTrap/%.6d.txt"%finnumFile),nx).sum()
        stpartnum=ReadZipTxt(zipfold,("data/NeTrap/%.6d.txt"%stnumFile),nx).sum()
        stpartnum+=ReadZipTxt(zipfold,("data/NposTrap/%.6d.txt"%stnumFile),nx).sum()
    if (regime=="txt"):
        pathtxt=path+"BasicOutput"+os.path.sep+"data"+os.path.sep
        finpartnum=np.loadtxt(pathtxt+"NeTrap"+os.path.sep+("%.6d.txt"%finnumFile)).sum()
        finpartnum+=np.loadtxt(pathtxt+"NposTrap"+os.path.sep+("%.6d.txt"%finnumFile)).sum()
        stpartnum=np.loadtxt(pathtxt+"NeTrap"+os.path.sep+("%.6d.txt"%stnumFile)).sum()
        stpartnum+=np.loadtxt(pathtxt+"NposTrap"+os.path.sep+("%.6d.txt"%stnumFile)).sum()
        
    rate=math.log(finpartnum/stpartnum)/numAnalysedPeriods
    PGrRt.append((pwr,rate))
    
    ###making figures to check numAnalysedPeriods
    numdata=finnumFile-numst+1
    Nelpos=np.empty(numdata)
    tar=np.linspace(numst/numSavesperPeriod,finnumFile/numSavesperPeriod,numdata)
    if regime=="zip":
        for i in range(numst,finnumFile+1):
            Nelpos[i-numst]=ReadZipTxt(zipfold,("data/NeTrap/%.6d.txt" % i),nx).sum()
            Nelpos[i-numst]+=ReadZipTxt(zipfold,("data/NposTrap/%.6d.txt" % i),nx).sum()
    if regime=="txt":
        for i in range(numst,finnumFile+1):
            Nelpos[i-numst]=np.loadtxt(pathtxt+"NeTrap"+os.path.sep+("%.6d.txt"%i)).sum()
            Nelpos[i-numst]+=np.loadtxt(pathtxt+"NposTrap"+os.path.sep+("%.6d.txt"%i)).sum()
    Nelposcheck=np.exp(rate*(tar-tar[0]))*Nelpos[-1]*math.exp(rate*(tar[0]-tar[-1]))
    
#    fig=plt.figure()
#    ax=fig.add_subplot(111)
#    ax.set_xlabel("time")
#    ax.set_ylabel("N el+pos")
#    Add1DPlot(Nelpos,tar,(tar.min(),tar.max()),ax,color='r')
#    Add1DPlot(Nelposcheck,tar,(tar.min(),tar.max()),ax,color='k')
#    ax.set_ylim(min(Nelpos.min(),Nelposcheck.min()),max(Nelpos.max(),Nelposcheck.max()))

#    fig.savefig(relfold+os.path.sep+('N_t_%g.png' % pwr),dpi=300)
#    for ax in fig.axes:
#        ax.clear()
#    fig.clf()
#    plt.close()
    print(sim, "is processed")
###calculation of threshold power
PGrRt.sort(key = lambda x: (x[1],x[0]))
file=open(relfold+os.path.sep+"PwrGR.txt","w")
for item in PGrRt:
    print(item)
    file.write("%g %g\n" % item)
file.close()

ThresholdPWR=0
if (PGrRt[0][1]>0):
    print("There are not simulations with negative growth rate")
    ThresholdPWR=PGrRt[0][0]-(PGrRt[1][0]-PGrRt[0][0])/(PGrRt[1][1]-PGrRt[0][1])*PGrRt[0][1]
elif (PGrRt[numSimul-1][1]<0):
    print("There are not simulations with positive growth rate")
    ThresholdPWR=PGrRt[numSimul-2][0]-(PGrRt[numSimul-1][0]-PGrRt[numSimul-2][0])/(PGrRt[numSimul-1][1]-PGrRt[numSimul-2][1])*PGrRt[numSimul-2][1]
else:
    i=0
    while PGrRt[i][1]<0:
        i+=1
    ThresholdPWR=PGrRt[i-1][0]-(PGrRt[i][0]-PGrRt[i-1][0])/(PGrRt[i][1]-PGrRt[i-1][1])*PGrRt[i-1][1]
file=open(relfold+os.path.sep+"ThrPwr.txt","w")
file.write(str(ThresholdPWR))
file.close()

#fig=plt.figure()
#ax=fig.add_subplot(111)
#ax.set_xlabel("P,PW")
#ax.set_ylabel("GR")

#pwrar=np.empty(numSimul)
#grar=np.empty(numSimul)

#for i,item in enumerate(PGrRt):
#    pwrar[i]=item[0]
#    grar[i]=item[1]

#xrange=(min(pwrar.min(),ThresholdPWR),max(pwrar.max(),ThresholdPWR))
#yrange=(min(grar.min(),0),max(grar.max(),0))
#Add1DPlot(grar,pwrar,xrange,ax,color='k',yrangeFrame=yrange,marker='^')
#ax.scatter([ThresholdPWR],[0],marker='o')
#ax.set_xlim(xrange[0],xrange[1])
#ax.set_ylim(yrange[0],yrange[1])

#fig.savefig(relfold+os.path.sep+'Gr_P.png',dpi=300)
#for ax in fig.axes:
#    ax.clear()
#fig.clf()
#plt.close()


    
    
    
    
    
    
    
