#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def main():
    power = [7.5, 7.7, 8, 9, 10, 12, 15, 17, 19]
    gamma = [400, 250, 170, 110, 84, 79, 79, 78, 78]
    gamma1 = [120, 73, 60, 35, 21, 14, 11, 10, 10]
    
    fig, ax1 = plt.subplots()
    for i in range(len(gamma)):
        gamma[i] /= 3.
        gamma1[i] /= 3.
    p, = ax1.plot(power, gamma, 'r-o', label = 'Full')
    p, = ax1.plot(power, gamma1, 'b-v', label = 'Instability')
    ax1.set_xlabel('Power, PW')
    ax1.set_ylabel('Settling time, T')
    ax1.set_yscale('log')
    ax1.set_ylim([0,150])
    ticks = [3,6,12,24,48,100]
    ax1.set_yticks(ticks)
    ax1.set_yticklabels(ticks)
    plt.legend(loc = 'upper right')
    plt.grid()
    plt.show()

if __name__ == '__main__':
    main()
