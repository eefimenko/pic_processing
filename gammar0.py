#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import sys
import math
import utils

def read_diag(file,nx,ny,dv,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            row.append(array[index]/dv/(i+0.5))
        field.append(row)
    return field

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    
    array = field[len(field)/2]
    n = len(array)
    
    array1 = array[n/2::]
   
    return array1

def create_axis(n,step,x0=0):
    axis = []
    for i in range(n):
        axis.append(i*step + x0)
    return axis    

def main():

    config = utils.get_config("ParsedInput.txt")
    BOIterationPass = float(config['BOIterationPass'])
    delta = 1
    ppath = 'data/ElGammaR/'
    epath = 'data/E2z/'
    bpath = 'data/B2z/'
    picspath = 'pics'
    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(ppath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(ppath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax) + ' files'
        
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    nx1 = int(config['ElGammaR.SetMatrixSize_0'])
    ny1 = int(config['ElGammaR.SetMatrixSize_1'])
    rmin = float(config['ElGammaR.SetBounds_0'])*1e4
    rmax = float(config['ElGammaR.SetBounds_1'])*1e4
    Emin = float(config['ElGammaR.SetBounds_2'])
    Emax = float(config['ElGammaR.SetBounds_3'])
    Xmax = float(config['X_Max'])*1e4 #mkm
    Xmin = float(config['X_Min'])*1e4 #mkm
    Ymax = float(config['Y_Max'])*1e4 #mkm
    Ymin = float(config['Y_Min'])*1e4 #mkm
    Zmax = float(config['Z_Max'])*1e4 #mkm
    Zmin = float(config['Z_Min'])*1e4 #mkm 
    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz
    dz = 0.9e-4 
    dr = (rmax - rmin)/nx1 * 1e-4
    dv = 2. * math.pi * dr * dr * dz
    ppw = float(config['PeakPowerPW'])
    emax = 4.5e11 * math.sqrt(ppw/20)
    for i in range(nmin,nmax,delta):
        pname = ppath + '/' + "%06d.txt" % (i,)
        ename = epath + '/' + "%06d.txt" % (i,)
        bname = bpath + '/' + "%06d.txt" % (i,)
#        mname = mpath + '/' + "%06d.txt" % (i,)
        pfield = read_diag(pname,nx1,ny1, dv)
        efield = read_field(ename, nx, ny)
        bfield = read_field(bname, nx, ny)
#        mfield = read_diag(mname,nx1,ny1)
#        p1name = p1path + '/' + "%06d.txt" % (i,)
#        m1name = m1path + '/' + "%06d.txt" % (i,)
#        p1field = read_diag(p1name,nx1,ny1)
#        m1field = read_diag(m1name,nx1,ny1)
       
        fig = plt.figure(num=None)
        ax = fig.add_subplot(1,1,1)
        surf = ax.imshow(pfield, extent = [rmin,rmax, Emin,Emax], aspect='auto', origin='lower', norm=clr.LogNorm())
        plt.colorbar(surf,  orientation  = 'horizontal')
#        ay = fig.add_subplot(2,1,2)
#        ay.imshow(p1field, extent = [zmin,zmax, Emin,Emax], aspect='auto', origin='lower', norm=clr.LogNorm())
#        ax = fig.add_subplot(2,2,2)
#        ax.imshow(p1field, extent = [zmin,zmax, Mmin,Mmax], aspect='auto', origin='lower', norm=clr.LogNorm())
#        ay = fig.add_subplot(2,2,4)
#        ay.imshow(m1field, extent = [zmin,zmax, Mmin,Mmax], aspect='auto', origin='lower', norm=clr.LogNorm())
#        ax1 = fig.add_subplot(2,1,2, sharex = ax)
        ax1 = ax.twinx()
        axis = create_axis(len(efield), dx, 0)
        ax1.plot(axis,efield)
        ax1.plot(axis,bfield)
        ax1.set_ylim([0, emax])
        picname = picspath + '/' + "gammar%06d.png" % (i,)
        print picname
        plt.savefig(picname)
        plt.close()
        #plt.show()

if __name__ == '__main__':
    main()
