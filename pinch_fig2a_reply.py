#!/usr/bin/python
import matplotlib as mp
mp.use('Agg')
import sys
import matplotlib.pyplot as plt
import math
import utils
import shutil
import os
import scipy.signal.signaltools as sigtool
import numpy as np
import copy
from matplotlib import gridspec
import matplotlib.colors as clr
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

def analyze_spectra(sp,de):
    n = len(sp)
    full_energy = sum(sp)
    full_number = 0
    print full_energy
    for i in range(n):
        full_number += sp[i]/(de*(i+0.425))
        
    tail_energy = 0
    
    for i in range(n):
        tail_energy += sp[-1-i]
        if tail_energy/full_energy > 0.01:
            break
    imax = i
    e_1percent = (n-imax)*de
    
    for i in range(n):
        sp[i] /= full_number

    av_energy = full_energy/full_number
            
    return sp, e_1percent, av_energy, n-imax, int(av_energy/de)

def make_patch_spines_invisible(ax):
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)
        
def read_ang(file,nx,ny,mult=1.):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index]*mult)
        field.append(row)
    return field

def norm(a, mult=1):
    m = max(a)
    for i in range(len(a)):
        a[i] *= mult/m
    return a

def read_field(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)/norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1e-9)
        field.append(row)
    return field

def read_file(file, de):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
    if 'tr' in file and not 'fs' in file:
        array[0] = 0
#        print 'removed'
        array, de = adjust_array(array, de)
        
    for i in range(len(array)):
        nph  += array[i]
    for i in range(len(array)):
        array[i] *= (i+0.425)
   
    return array, nph

def read_file_sph(file, de):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
    if 'tr' in file and not 'fs' in file:
        array[0] = 0
#        print 'removed'
        array, de = adjust_array(array, de)
       
    for i in range(len(array)):
        nph  += array[i]/(i+0.425)/de
    for i in range(len(array)):
        array[i] /= de
#        array[i] = array[i]/(i+0.425)/de
   
    return array, nph

def check_length(file):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
       
    return len(array)

def adjust_array(array, de):
    n = len(array)
    adj_size = 10
    m = n/adj_size
    a = [0]*m
    for i in range(m):
        tmp = 0
        for j in range(adj_size):
            tmp += array[i*adj_size + j]
        a[i] = tmp
    return a, de*adj_size

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def find_max_energy_full(array,step):
    n = len(array)
    full = 0
    imax = 0
    summ = 0
    nfull = 0
    av_en = 0
    n_av = 0
    for i in range(n):
        nfull += array[i]/(i+0.425)
        full += array[i]*step
    
    if nfull > 0:
        av_en = full/nfull
        n_av = int(av_en/step)

    for i in range(1,n):
        summ = summ + array[-i]*(n-i+0.425)*step
        if summ > 0.01*full:
            imax = i
            break
    if imax == 0:
        max_en = 0
    else:
        max_en = (n-imax+0.37) * step
#    en1 = max_en*1e-3
#    ratio = en1/step
#    print max_en, av_en
    ratio = 1
    return av_en*1000, max_en, array[n_av], array[-imax]


def max2d(array):
    return max([max(x) for x in array])

def sum_ang_energy(array):
    nx = len(array)
    ny = len(array[0])
    res = 0
    for i in range(nx):
        for j in range(ny):
            res = res + array[i][j]
    return res

def envelope(nt,dt,delay,duration,alpha):
    envelope = np.zeros(nt)
    for i in range(nt):
        t = i*dt
        envelope[i] = 0.25*(1. - math.tanh(-alpha*(t - delay)))*(1. - math.tanh(alpha*(t - delay - duration)))
    return envelope

def cvalue(array):
    s = array.shape
    dx = 2
    dy = 2
    xmin = s[0]/2-dx
    xmax = s[0]/2+dx
    ymin = s[1]/2-dy
    ymax = s[1]/2+dy
    
    return np.amax(array[xmin:xmax,ymin:ymax])

def cvalue1(array):
    s = array.shape
    
    xc = s[0]/2
    yc = s[1]/2
    return np.amax(array[xc-2,yc])

def main():
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
    if (len(sys.argv) > 1):
        label_ = sys.argv[1]
    else:
        label_ = 'a'
    picspath = 'pics'
    path = '.'
    enpath = '/statdata/ph/EnSp/'
    angpath = '/statdata/ph/angSpSph/'
    angpath_el = '/statdata/el/angSpSph/'
    angpath_pos = '/statdata/el/angSpSph/'
    ezpath = 'data/E2z'
    nexpath = 'data/Electron2Dx'
    config = utils.get_config(path + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV'])
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    dv = 2*dx*dy*dz 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
    dt = (x0*y0*1e15)/T
    num = len(sys.argv)

    ax_ = []
    ax1_ = []
    # ax2_ = []
    ez_t = []
    ne_t = []
    pow_t = []
    max_t = []
    av_t = []
    br_full_t = []
    br_100mev_t = []
    br_1gev_t = []
    br_full1_t = []
    br_100mev1_t = []
    br_1gev1_t = []
   
    t = 0
    omega = float(config['Omega'])
    delay = float(config['delay'])
    duration = float(config.get('duration', 0))
    alpha = float(config.get('alpha', 0))
    T = 2*math.pi/omega*1e15
    phi = int(config['QEDstatistics.OutputN_phi'])
    theta = int(config['QEDstatistics.OutputN_theta'])
    nit = int(config['QEDstatistics.OutputIterStep'])
    dt1 = float(config['TimeStep'])*nit
    time_step = float(config['TimeStep'])
    wl = float(config['Wavelength'])
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    ev = float(config['eV'])
    ne = int(config['QEDstatistics.OutputN_E'])
    nt = int(config['QEDstatistics.OutputN_theta'])
    tmax = float(config.get('QEDstatistics.ThetaMax',0))*180./math.pi
    tmin = float(config.get('QEDstatistics.ThetaMin',0))*180./math.pi
    
    emax = float(config['QEDstatistics.Emax'])/ev*1e-9
    emin = float(config['QEDstatistics.Emin'])/ev*1e-9
    de = (emax - emin)/ne
    dt = (tmax - tmin)/nt
    dee1 = de*1e9*ev # erg
    dtt1 = dt*math.pi/180.
    dv = 2*dx*dy*dz
    electronMass = 9.10938e-28
    electronCharge = 4.8032e-10
    ncr = electronMass * omega * omega/(8. * math.pi * electronCharge * electronCharge)
    const_a_p = 7.81441e-9
    a0 = const_a_p * math.sqrt(ppw*1e22)
    print a0, 1.18997e+8*a0
    fontsize = 24
    mp.rcParams.update({'font.size': fontsize})
    S = 2 * math.pi * 0.1 * 0.1 * wl * wl * 100 # square of source mm^2, r = 0.1 lambda 
    ds = 1.5e4 # mrad^2
    f = open('br_t.dat', 'r')
    i = 0
    for line in f:
        tmp = line.split()
        ax_.append(float(tmp[0])/T)
        ez_t.append(float(tmp[1]))
        pow_t.append(float(tmp[2]))
        max_t.append(float(tmp[3])/(0.511e-3))
        av_t.append(float(tmp[4]))
        ax1_.append((float(tmp[0])-39*dt1*1e15)/T)
        br_full_t.append(float(tmp[5]))
        br_100mev_t.append(float(tmp[6]))
        br_1gev_t.append(float(tmp[7])/1e23)
        br_full1_t.append(float(tmp[5])/S/ds)
        br_100mev1_t.append(float(tmp[6])/S/ds)
        br_1gev1_t.append(float(tmp[7])/S/ds)
        
    filename = 'jz_anls_new'
    filename_center = 'jz_center'
    nmin,nmax = utils.find_min_max_from_directory(path + '/' + utils.jz_zpath)
    jplus = np.zeros(nmax)
    jminus = np.zeros(nmax)
    jfull = np.zeros(nmax)
    jmax = np.zeros(nmax)
    jcenter = np.zeros(nmax)
    nmin = utils.read_tseries(path,filename, jplus, jminus, jfull, jmax)
    nmin = utils.read_tseries(path,filename_center, jcenter)
    c2 = 3.33e-10*dx*dy*1e-6
    
    for i in range(len(jfull)):
        jfull[i] = abs(jfull[i])*c2
    
    np_t = utils.ts(path, utils.npzpath, name='npz',
                    tstype='max', shape = (nx,ny), verbose=True)/dv
    ne_t = utils.ts(path, utils.nezpath, name='nez',
                    tstype='max', shape = (nx,ny), verbose=True)/dv
    bz_t = utils.ts(path, utils.bzpath, name='bz',
                    tstype='func', shape = (nx,ny), verbose=True, func = cvalue)/3.*math.sqrt(ppw/10.)*0.653
    netr_t = utils.ts(path, utils.netpath, name='ne_trapped',
                         tstype='sum', verbose=True)/1e10
    npostr_t = utils.ts(path, utils.npostpath, name='npos_trapped',
                         tstype='sum', verbose=True)/1e10
    nt = len(npostr_t)
    env_ = envelope(nt,dt1,delay,duration,alpha)#*3.*math.sqrt(ppw/10.)*0.653
    print 'Max ', max(npostr_t)
    print de, int(2./de), (2.*1e-3/de)
    fraction = 2.*1e-3/de
    gev2_pos = int(2./de)
    print max(ez_t), max(br_1gev_t), max(br_1gev1_t)
    print br_1gev1_t[gev2_pos]*fraction 
    fig = plt.figure(num=None, figsize = (13,5))
    ax = fig.add_subplot(1,1,1)
#    ax1 = fig.add_subplot(2,1,2)
    mp.rcParams.update({'font.size': fontsize})
    current_dir = os.path.realpath(path)
    field_coeff = 1
    if 'shifted' in current_dir or 'distorted' in current_dir:
        field_coeff = 0.9 
    
    ez_a = [0] * len(ez_t)
    for i in range(len(ez_t)):
        t = i*dt1
#        ez_a[i] = field_coeff*math.exp(-2.*math.log(2)*(t - delay)/duration*(t - delay)/duration)*3e11*math.sqrt(ppw/10.)
#    name = path + '/spectra.txt'
#    spectra = np.fromfile(name, sep = ' ')
#    print spectra.shape
#    gs = gridspec.GridSpec(1, 2, width_ratios=[2, 1])

    ncr = utils.ElectronMass * omega * omega/(8. * math.pi * utils.ElectronCharge * utils.ElectronCharge)
    print ncr, 1e23/ncr
    #gs = gridspec.GridSpec(2, 2, width_ratios=[3, 1])
    
    
    fig.subplots_adjust(left=0.2)
    ax2 = ax.twinx()
    ax2.spines["left"].set_position(("axes", -0.15))
    make_patch_spines_invisible(ax2)
    # Second, show the right spine.
    ax2.spines["left"].set_visible(True)
    ax2.yaxis.set_label_position('left')
    ax2.yaxis.set_ticks_position('left')

    ax4 = ax.twinx()
#    ax4.spines["left"].set_position(("axes", -0.1))
#    make_patch_spines_invisible(ax4)
    # Second, show the right spine.
#    ax4.spines["left"].set_visible(True)
#    ax4.yaxis.set_label_position('left')
#    ax4.yaxis.set_ticks_position('left')
#    f, = ax.plot(ax1_, netr_t, 'r')
#    fp, = ax.plot(ax1_, npostr_t, 'k')
    
    print 'Max ', max(npostr_t)
#    f1, = ax2.plot(ax_, utils.env(np.array(ez_t)/1e11, ax_), 'b')
#    f2, = ax2.plot(ax_, utils.env(np.array(bz_t)/1e11, ax_), 'm')
#    f1, = ax.plot(ax_, np.array(ez_t)/1e11, 'b')
    f2, = ax.plot(ax_, np.array(bz_t)/1e11, 'b')
    f3, = ax.plot(ax_, env_, 'b', dashes = [2,2])
    
#    f2, = ax2.plot(ax_, ez_a, 'b:')
    thr = 3*math.sqrt(7.1/10.)
    # f3, = ax2.plot([ax_[0], ax_[-1]], [thr,thr], 'b--')
    ax.set_ylabel('|B/B$_0$| ')
    ax4.set_ylabel('J$_z$ [MA]')
#    ax.set_ylabel('$N_p, \\times 10^{10}$')
    ax.set_xlabel('t/T')
#    ax1.set_xlabel('$t, T$')
    # ax2.set_ylim([0,1])
#    f4, = ax4.plot(ax1_, jfull, 'k')
#    f4, = ax4.plot(ax1_, utils.env(jfull, ax1_), 'r')
    f4, = ax4.plot(ax_, jcenter, 'r', dashes = [6,2])
    print 'Max jz', max(jfull), max(utils.env(jfull, ax1_))
    print len(br_1gev_t)
#    ax1 = ax.twinx()
    thrne = 7e24
    p1, = ax2.plot(ax_, np_t, 'g', dashes = [4,2,1,2])
    ax2.set_yscale('log')
#    p1, = ax1.plot(ax_, np_t, 'g')
    ax2.set_ylabel('n$_{p}$ [cm$^{-3}$]')
    # n3, = ax1.plot([ax1_[0], ax1_[-1]], [thrne,thrne], 'g--')
    t_min = 2
    t_max = 10
#    ax1.set_xlim([t_min,t_max])
    ax.set_xlim([t_min,t_max])
    #ax1.set_ylim([0,20])
#    ax.set_xlim([-2,40])
#    ax.set_ylim([0,25])
#    ax.set_yticks([0,5,10,15,20,25])
#    ax.set_yticklabels(['$0$','$5$','$10$','$15$','$20$','$25$'])
#    ax4.set_yticks([0,5,10,15,20,25,30,35])
#    ax4.set_yticklabels(['$0$','$5$','$10$','$15$','$20$', '$25$','$30$','$35$'])
    ax4.set_yticks([0,4,8,12])
    ax4.set_yticklabels(['$0$','$4$','$8$','$12$'])
    ax.set_xticks([2,3,4,5,6,7,8,9,10])
    ax.set_xticklabels(['$0$','$1$','$2$','$3$','$4$', '$5$','$6$','$7$','$8$'])
#    ax1.set_xticks([6,7,8,9,10,11,12,13,14,15])
#    ax1.set_xticklabels(['$0$','$1$','$2$','$3$','$4$', '$5$','$6$','$7$','$8$','$9$'])
    ax.set_ylim([0,1.6])
    ax.set_yticks([0,0.25,0.5,0.75, 1, 1.25, 1.5])
    ax.set_yticklabels(['$0$','$0.25$','$0.5$','$0.75$','$1$','$1.25$', '$1.5$'])
    
#    ax1.set_ylim([1e21, 3e26])
#    ax1.set_yscale('log')
#    ax1.set_yticks([1e19,1e20,1e21, 1e22, 1e23, 1e24, 1e25, 1e26, 1e27])
#    ax1.set_yticklabels(['$10^{19}$','$10^{20}$','$10^{21}$','$10^{22}$','$10^{23}$','$10^{24}$','$10^{25}$', '$10^{26}$', '$10^{27}$'])
    ax2.set_ylim([1e20, 1e27])
    ax2.set_yticks([1e20,1e21, 1e22, 1e23, 1e24, 1e25, 1e26, 1e27])
    ax2.set_yticklabels(['$10^{20}$','$10^{21}$','$10^{22}$','$10^{23}$','$10^{24}$','$10^{25}$', '$10^{26}$', '$10^{27}$'])
#    axticks = [5, 10, 15, 20, 25, 30, ]
#    axticklabels = ['$%d$' % (x) for x in axticks]
#    ax.set_xticks(axticks)
#    ax.set_xticklabels(axticklabels)
    
#    ax.yaxis.label.set_color(fp.get_color())
#    ax.spines["left"].set_color(fp.get_color())
#    ax.tick_params(axis='y', colors=fp.get_color())
#    ax1.yaxis.label.set_color(p1.get_color())
#    ax1.spines["right"].set_color(p1.get_color())
#    ax1.tick_params(axis='y', colors=p1.get_color())
#    ax2.yaxis.label.set_color(f1.get_color())
#    ax2.spines["left"].set_color(f1.get_color())
#   ax2.tick_params(axis='y', colors=f1.get_color())
#    ax4.yaxis.label.set_color(f4.get_color())
#    ax4.spines["left"].set_color(f4.get_color())
#    ax4.tick_params(axis='y', colors=f4.get_color())
    
#    ax.set_title('Ideal dipole wave')
#    plt.legend([f, fp, f1, f2, p1, n3, f3], ["Electrons", 'Positrons', 'E', 'Vacuum E', '$N_{pos}$', 'Stationary $N_{pos}$', 'Stationary E'], loc='lower center')
    # plt.legend([f1, p1, fp, f4, n3, f3], ['$|E|$', '$n_{p}$','$N_p^{\lambda/2}$', '$J_z$', 'Stationary $n_{p}$', 'Stationary $|E|$', '$J_z$'], loc='lower center')
#    plt.legend([f1, p1, fp, f4], ['$|E|$', '$n_{p}$','$N_p$', '$J_z$', 'Stationary $n_{p}$', 'Stationary $|E|$'], loc='upper left', frameon = False)
#    plt.legend([f1, f2, p1, f4], ['$|E|$', '$|B|$', '$n_{p}$', '$J_z$', 'Stationary $n_{p}$', 'Stationary $|E|$'], loc='upper left', frameon = False)
    plt.legend([f3, f2, p1, f4], ['$f(t)$', '$|B/B_0|$', '$n_{p}$', '$J_z$'], loc='upper left', frameon = False, labelspacing = 0.1)
    ax.text(-0.25, 1.6, '$(%s)$' % (label_))
    name = "pinch_fig2a"
    picname = picspath + '/' + name + ".png"
    plt.tight_layout()
    plt.savefig(picname, dpi = 256)
    
    

   
if __name__ == '__main__':
    main()
