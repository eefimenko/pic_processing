#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils
import numpy as np

def main():
    path = './'
    config = utils.get_config(path + "/ParsedInput.txt")
    picspath = 'pics/'

    iterations = [200, 310, 350, 430, 600, 920]
    labels = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)']
    wl = float(config['Wavelength'])
    xmax = float(config['X_Max'])/wl #mkm
    xmin = float(config['X_Min'])/wl #mkm
    ymax = float(config['Y_Max'])/wl #mkm
    ymin = float(config['Y_Min'])/wl #mkm
    zmax = float(config['Z_Max'])/wl #mkm
    zmin = float(config['Z_Min'])/wl #mkm
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    power = int(config['PeakPowerPW'])
    dx = float(config['Step_X'])/wl
    dy = float(config['Step_Y'])/wl
    dz = float(config['Step_Z'])/wl
    mult = (1/(2.*dx*dy*dz*1e-12))
    step = float(config['TimeStep'])*1e15*float(config['BOIterationPass'])
    omega = float(config['Omega'])
    ncr = utils.ElectronMass * omega * omega/(8. * math.pi * utils.ElectronCharge * utils.ElectronCharge)
    T = 2 * math.pi/omega
    nt = int(T*1e15/step)
    dt  = step/(T*1e15)
    print nt
    print 'Critical density ', ncr
    figures = [utils.bx_zpath, utils.nezpath]
    cmaps = ['bwr', 'Greens']
    titles = ['$B_\\rho$', '$N_e$']
    log = [False, False]
             

#    n0 = [703, 0, 0] # 675
#    n0 = [686, 0, 0] # 680
#    n0 = [1030, 1090]
    
    spx = 2
    spy = 3

    xlim = [-0.5, 0.5]
    ylim = xlim
    verbose = 1
    # nticks = 5
    # ticks = [xlim[0] + i*(xlim[1]-xlim[0])/(nticks-1) for i in range(nticks)]
    # print ticks
    niter = len(iterations)
        
    fig = plt.figure(num=None, figsize=(15., 10))
    mp.rcParams.update({'font.size': 15})
    fontsize = 15
    for i in range(niter):
        fx = utils.bo_file_load(path + utils.bx_zpath,iterations[i],nx,ny,verbose=1)
        fy = utils.bo_file_load(path+utils.by_zpath,iterations[i],nx,ny,verbose=1)
        fx0 = utils.bo_file_load('../10pw_vac/'+utils.bx_zpath,iterations[i],nx,ny,verbose=1)
        fy0 = utils.bo_file_load('../10pw_vac/'+utils.by_zpath,iterations[i],nx,ny,verbose=1)
            
        fr,fphi = utils.convert_xy_to_rphi(fx-fx0,fy-fy0,dx,dy)
        ratio = np.amax(fr)/np.amax(fphi) 
        print ratio, fr.shape, np.amax(fr)
        xticks = [-0.5, 0, 0.5]
        if i%3 != 0:
            ylabel = ''
            yticks = []
        else:
            ylabel = '$y/\lambda$'
            yticks = [-0.5, 0, 0.5]
        clevels = [-0.15, 0.15]
        utils.subplot(fig, iterations[i], path, field = fr,
                      shape = (nx,ny), position = (spx,spy,i+1),
                      extent = [xmin, xmax, ymin, ymax],
                      cmap = 'bwr', title = '', titletype = 'simple',
                      colorbar = False, logarithmic=False, verbose=verbose,
                      xlim = xlim, ylim = ylim,
                      xlabel = '$x/\lambda$', ylabel = ylabel, yticks = yticks, xticks = xticks,
                      maximum = 'local', fontsize = fontsize,
                      contour=True, clevels = clevels)
        
            
        utils.subplot(fig, iterations[i], path+utils.nezpath, 
                      shape = (nx,ny), position = (spx,spy,i+1),
                      extent = [xmin, xmax, ymin, ymax],
                      cmap = 'Greens', title = '$%s %.1f T$'%(labels[i], iterations[i]*dt), titletype = 'simple',
                      colorbar = True, logarithmic=False, verbose=verbose,
                      xlim = xlim, ylim = ylim, yticks = yticks, xticks = xticks,
                      xlabel = '$x/\lambda$', ylabel = ylabel,
                      maximum = 'local', fontsize = fontsize, mult = mult/ncr)
                    
    picname = picspath + '/' + "fig2.png"
    # plt.tight_layout()
    plt.savefig(picname, bbox_inches='tight', dpi=128)
    plt.close()

    
if __name__ == '__main__':
    main()
