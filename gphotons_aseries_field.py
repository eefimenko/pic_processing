#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils
import numpy as np

def main():
  
    numdirs = int(sys.argv[1])
    dirs = []
    configs = []
    for i in range(numdirs):
        dirs.append(sys.argv[2+i] + '/')
        configs.append(utils.get_config(sys.argv[2+i] + "/ParsedInput.txt"))
    
    n = numdirs + 1
    
    picspath = 'pics'
    nmin, nmax, delta = utils.get_min_max_iteration(sys.argv,dirs[0] + '/' + utils.ezpath, n=n)
    print nmin, nmax, delta
    
    xmin = []
    xmax = []
    ymin = []
    ymax = []
    zmin = []
    zmax = []
    mult = []
    nx = []
    ny = []
    nz = []
    power = []
    for k in range(numdirs):
        wl = float(configs[k]['Wavelength'])
        xmax.append(float(configs[k]['X_Max'])/wl) #mkm
        xmin.append(float(configs[k]['X_Min'])/wl) #mkm
        ymax.append(float(configs[k]['Y_Max'])/wl) #mkm
        ymin.append(float(configs[k]['Y_Min'])/wl) #mkm
        zmax.append(float(configs[k]['Z_Max'])/wl) #mkm
        zmin.append(float(configs[k]['Z_Min'])/wl) #mkm
        nx.append(int(configs[k]['MatrixSize_X']))
        ny.append(int(configs[k]['MatrixSize_Y']))
        nz.append(int(configs[k]['MatrixSize_Z']))
        power.append(int(configs[k]['PeakPowerPW']))
        dx = float(configs[k]['Step_X'])/wl
        dy = float(configs[k]['Step_Y'])/wl
        dz = float(configs[k]['Step_Z'])/wl
        mult.append(1/(2.*dx*dy*dz*1e-12))
        step = float(configs[k]['TimeStep'])*1e15*float(configs[k]['BOIterationPass'])
        omega = float(configs[k]['Omega'])
        T = 2 * math.pi/omega
        nt = int(T*1e15/step)
        print nt
    a0 = 3.e11 * math.sqrt(power[0]/10.)
    #figures = [utils.ezpath, utils.bzpath, utils.nezpath, utils.npzpath, utils.nphzpath, utils.nizpath]
    #cmaps = ['Reds', 'Reds', 'Greens','Greens', 'Blues', 'Greens']
    #titles = ['Electric field %d pw', 'Magnetic field %d pw', 'Electrons %d pw','Positrons %d pw', 'Photons %d pw', 'Ions %d pw']
    #log = [False, False, True, True, True, True]
    #mult = [1,1,mult[0],mult[0],mult[0], mult[0]]
    figures = [utils.ezpath, utils.bzpath]
    cmaps = ['Reds', 'Blues']
    titles = ['', '']
    log = [False, False]
    mult = [1./a0, 1./a0]
#    n0 = [703, 0, 0] # 675
#    n0 = [686, 0, 0] # 680
#    n0 = [1030, 1090]
    n0 = [0] * numdirs
    spx = numdirs
    spy = len(figures)
    
    for i in range(nmin,nmax,delta):
        print "\rSaving sc%06d.png, %.0f %% done" % (i,float(i-nmin)/(nmax-nmin)*100),
        sys.stdout.flush()

        fig = plt.figure(num=None, figsize=(10, 2.5), dpi=256)
        mp.rcParams.update({'font.size': 14})
        
        for k in range(spx):
            for j in range(spy):
                figure = figures[j]
                cmap = cmaps[j]
                if j == 0 and k == 0:
                    figure = utils.ezpath
                    cmap = 'Reds'
                    ylabel = '$y/\lambda$'
                    label = '$(a)$'
                elif j == 1 and k == 0:
                    figure = utils.expath
                    cmap = 'Reds'
                    ylabel = '$z/\lambda$'
                    label = '$(b)$'
                elif j == 0 and k == 1:
                    figure = utils.bzpath
                    cmap = 'Blues'
                    ylabel = '$y/\lambda$'
                    label = '$(c)$'
                elif j == 1 and k == 1:
                    figure = utils.bxpath
                    cmap = 'Blues'
                    ylabel = '$z/\lambda$'
                    label = '$(d)$'
                ax = utils.subplot(fig, i+n0[k], dirs[k]+figure,
                                   shape = (nx[k],ny[k]), position = (1,4,j+1+k*spy),
                                   extent = [xmin[k], xmax[k], ymin[k], ymax[k]],
                                   cmap = cmap, title = titles[j], fontsize = 8,
                                   colorbar = True, logarithmic=log[j], verbose=0, transpose=1,
                                   xlim = [-1,1], ylim = [-1,1], ticks = [-1, -0.5,0,0.5, 1], 
                                   xlabel = '$x/\lambda$', ylabel = ylabel, mult = mult[j])
                ax.text(-1.8,1, label, fontsize = 14)      
        picname = picspath + '/' + "ascz%06d.png" % (i,)
        plt.tight_layout()
        plt.savefig(picname, dpi=256)
        plt.close()

    
if __name__ == '__main__':
    main()
