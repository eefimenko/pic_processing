wd=${1}beams
binary=../../pic_new_beams
mkdir ${wd}
cp configs/*.txt ${wd}
cd ${wd}
sed -i -- "s/Number = X/Number = ${1}/g" *.txt
list=`ls *.txt`
echo ${list}
for config in $list
do
    mpirun -np 8 -ppn 1 -maxtime 80 ${binary} -g 2x2x2 ${config}
done
cd .. 