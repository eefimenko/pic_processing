#!/bin/bash

function create_dir
{
    if [ ! -d $1 ]; 
    then
	mkdir $1
    else
	echo "$1 already exists"
    fi
}

function save_host()
{
    echo  "$1" > ./host.txt 
    echo  "${@:2:3}" >> ./host.txt 
}

function save_dir()
{
	echo  "$1" > ./log.txt 
	echo  "$2" >> ./log.txt 
}

function save_iterations()
{
    echo  "$1" > ./iterations.txt 
}

function save_dlist()
{
    echo  "$1" > ./dlist.txt 
}

if [ "$#" -eq 1 ]; then
    dir=$1
    rdir=""
fi

if [ "$#" -eq 2 ]; then
    dir=$2
    rdir=$1
fi

create_dir $dir
cd $dir

# reading saved host and key
if [ -f "host.txt" ]; then
    i=0
    while IFS= read -r var
    do
	if [ $i -eq 0 ]; then
	    ssh_host=$var
	else
	    if [ $i -eq 1 ]; then
		key=$var
	    else
		echo "Something wrong, exiting"
	    fi
	fi
	i=$((i+1))
    done < "host.txt"
    echo "host.txt exists, continue downloading from $ssh_host"
else
    while true; do
    read -p "Choose the cluster 1. MSC(evefim) 2. Kebnekaise(argon) 3. Lomonosov(sbastr) 4. MSC(alba) 5. MSC(argon) 6. restore from cluster 7. Exit :" cl
    case $cl in
        1 ) user=evefim; host=mvs1p1.jscc.ru; wd_remote=/home4/pstorage1/ipfnn1/evgeny/picador/10p; key=' '; break;;
        2 ) user=argon; host=kebnekaise.hpc2n.umu.se; wd_remote=/home/a/argon/pfs/evefim/dw; key=' '; break;;
	3 ) user=sbastr; host=lomonosov.parallel.ru; wd_remote=/mnt/data/users/dm2/vol3/sbastr/_scratch/evefim/dw; key="-i ~/.ssh/mgu"; break;;
	4 ) user=alba; host=mvs1p1.jscc.ru; wd_remote=/home4/pstorage1/ipfnn1/alba/mvs10p/; key=' '; break;;
	5 ) user=argon; host=mvs1p1.jscc.ru; wd_remote=/home4/pstorage1/ipfnn1/Muraviev/; key=' '; break;;
	6 ) user=evgenii; host=cluster; wd_remote=/home/evgenii/results; key=' '; break;;
	7 ) exit;;
        * ) echo "Please choose from the variants 1-6";;
    esac
    done
    
    ssh_host=${user}@${host}
    save_host $ssh_host $key
fi

# reading saved rdir and remote working dir
if [ -f "log.txt" ]; then
    i=0
    while IFS= read -r var
    do
	if [ $i -eq 0 ]; then
	    rdir=$var
	else
	    if [ $i -eq 1 ]; then
		wd_remote=$var
	    else
		echo "Something wrong, exiting"
	    fi
	fi
	i=$((i+1))
    done < "log.txt"
    echo "log.txt exists, continue downloading from $rdir"
else
    if [ -z "$rdir" ]; then
	echo "Unknown remote directory, exiting"
	exit -1
    fi
    save_dir $rdir $wd_remote
fi

# reading saved iterations
if [ -f "iterations.txt" ]; then
    read -r iterations < "iterations.txt"
    echo "iterations.txt exists, continue downloading $iterations"
else
    while true; do
    read -p "Input which iterations to download: 1. All 2. Selected 3. exit " cl
    case $cl in
        1 ) iterations='all'; break;;
        2 ) read -p "Input regular expression which iterations to download: " iterations; break;;
	3 ) exit;;
        * ) echo "Please choose from the variants 1-3";;
    esac
    done
    save_iterations $iterations
fi

# reading saved download list
if [ -f "dlist.txt" ]; then
    read -r dlist < "dlist.txt"
    echo "list.txt exists, continue downloading $dlist"
else
    while true; do
    read -p "Input which directories to download: 1. All 2. Selected 3. None 4. exit " cl
    case $cl in
        1 ) dlist='all'; break;;
        2 ) read -p "Input directory names: " dlist; break;;
	3 ) dlist=' '; break;;
	4 ) exit;;
        * ) echo "Please choose from the variants 1-3";;
    esac
    done
    save_dlist "$dlist"
fi

echo "Ssh host: $ssh_host"
echo "Ssh key: $key"
echo "Save path: $dir"
echo "Download path: $rdir"
echo "Working directory: $wd_remote"

# get list of config files
list=`ssh $key $ssh_host "cd ${wd_remote}/${rdir}/ && ls *.txt"`

for nn in ${list}
do
    name=${nn%.*}
    res_dir=`ssh $key $ssh_host "cd ${wd_remote}/${rdir}/ && ls . | grep ${name}_"`
    echo $name $res_dir
    if [ "$res_dir" = "" ]; then
	echo "No directory for" $name "yet"
    else
    if [ ! -d $name ]; then
       mkdir $name
       cp host.txt $name
       cp iterations.txt $name
       cp dlist.txt $name
       cd $name
       save_dir $rdir/$res_dir $wd_remote
       cd ..
    fi
    get_results.sh $name
    fi
    
done
cd ..


