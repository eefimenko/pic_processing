import math

def R(x,y,z):
    R = math.sqrt(x*x + y*y + z*z)
    return R

def exf(k,x,y,z):
    r = R(x,y,z)
    if r < 0.0001:
        r = 0.0001
    return x*z/(r*r) * f2(k,r)*2.

def eyf(k,x,y,z):
    r = R(x,y,z)
    if r < 0.0001:
        r = 0.0001
    return y*z/(r*r) * f2(k,r)*2.

def ezf(k,x,y,z):
    r = R(x,y,z)
    if r < 0.0001 :
        r = 0.0001
    return 2. * (f3(k,r) + z*z/(r*r)*f2(k,r))
  
def bxf(k,x,y,z):
    r = R(x,y,z)
    if r < 0.0001 :
        r = 0.0001
    return y/r * 2. * f1(k,r)

def byf(k,x,y,z):
    r = R(x,y,z)
    if r < 0.0001 :
        r = 0.0001
    return -x/r * 2. * f1(k,r)
    
def f1(k,r):
    return -k*k/r * math.cos(k*r) + k/(r*r) * math.sin(k*r)

def f2(k,r):
    return (-k*k/r + 3./(r*r*r))*math.sin(k*r) - 3.*k/(r*r)*math.cos(k*r)

def f3(k,r):
    return (k*k/r - 1./(r*r*r))*math.sin(k*r) + k/(r*r)*math.cos(k*r)

def Sin(r,t,k):
    return math.sin(k*r + Omega*t)

def Cos(r,t,k):
    return math.cos(k*r + Omega*t)

def Ex(x,y,z,R,t,k):
    return z * x / (R*R*R) * ( Sin(R,t,k)*(-1 + 3./(k*k*R*R)) - Cos(R,t,k)*3./(k*R))

def Ey(x,y,z,R,t,k):
    return z * y / (R*R*R) * ( Sin(R,t,k)*(-1 + 3./(k*k*R*R)) - Cos(R,t,k)*3./(k*R))

def Ez(x,y,z,R,t,k):
    return -( (x*x + y*y)/(R*R*R) * Sin(R,t,k) + 1./(k*k*R*R*R) * (-1. + 3.*z*z/(R*R))*Sin(R,t,k) + 1./(k*R*R) * (1. - 3.*z*z/(R*R)) * Cos(R,t,k) )

def Bx(x,y,z,R,t,k):
    return - y /(R*R) * (Sin(R,t,k) + 1./(k*R)*Cos(R,t,k))

def By(x,y,z,R,t,k):
    return - x /(R*R) * (Sin(R,t,k) + 1./(k*R)*Cos(R,t,k))
