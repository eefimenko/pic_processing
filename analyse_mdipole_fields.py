#!/usr/bin/python
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.cm as cmx
import numpy as np
import utils
import sys
import os
from tqdm import *
from pylab import *

def read_one_file(filename):
    f = open(filename)
    data = f.readline().split()
    return data

def read_concentration(filename):
    f = open(filename)
    ax = []
    conc = []
    for line in f:
        tmp = line.split()
        ax.append(float(tmp[0]))
        conc.append(float(tmp[1]))
    f.close()
    conc = np.array(conc)
    s = 0.
    for j in range(len(conc)):
        s += conc[j] * (j+0.5)
        
    return ax, conc/s*1000

def main():
    filename = 'gamma_ne_fields.txt'
    path = './'
    picspath = '/home/evgeny/Dropbox/tmp_pics'

    power = []
    g = []
    ne = []
    ne_max = []
    b = []
    e = []
    nl_p = []
    p_ph = []
    en_max_ph = []
    en_av_ph = []
    flux_ph_full = []
    flux_ph_100mev = []
    flux_ph_1gev = []
    p_el = []
    en_max_el = []
    en_av_el = []
    flux_el_full = []
    flux_el_100mev = []
    flux_el_1gev = []

    lin_labels = []
    nonlin_labels = []
    lin_e_conc = []
    lin_b_conc = []
    nonlin_e_conc = []
    nonlin_b_conc = []

    nonlin_power = []
    max_el_lin = []
    max_ph_lin = []
    max_el_nonlin = []
    max_ph_nonlin = []
    av_el_lin = []
    av_ph_lin = []
    av_el_nonlin = []
    av_ph_nonlin = []
    max_el_lin_r = []
    max_ph_lin_r = []
    max_el_nonlin_r = []
    max_ph_nonlin_r = []
    av_el_lin_r = []
    av_ph_lin_r = []
    av_el_nonlin_r = []
    av_ph_nonlin_r = []
        
    for d in sys.argv[1:]:
        config = utils.get_config(d + "/ParsedInput.txt")
        ppw = int(config['PeakPowerPW'])
        data = read_one_file(d + '/' + filename)
        power.append(ppw)
        nonlin_power.append(ppw)
        g.append(float(data[0]))
        ne.append(float(data[1]))
        ne_max.append(float(data[2]))
        b.append(float(data[3]))
        e.append(float(data[4]))
        nl_p.append(float(data[5]))
        p_ph.append(float(data[6]))
        en_max_ph.append(float(data[7]))
        en_av_ph.append(float(data[8]))
        flux_ph_full.append(float(data[9]))
        flux_ph_100mev.append(float(data[10]))
        flux_ph_1gev.append(float(data[11]))
        p_el.append(float(data[12]))
        en_max_el.append(float(data[13]))
        en_av_el.append(float(data[14]))
        flux_el_full.append(float(data[15]))
        flux_el_100mev.append(float(data[16]))
        flux_el_1gev.append(float(data[17]))
        
        lin_labels.append('%d PW' % ppw)
        axis1, conc = read_concentration(d + '/' + 'concentration_linear_max_E.txt')
        lin_e_conc.append(conc)
        axis2, conc = read_concentration(d + '/' + 'concentration_linear_max_B.txt')
        lin_b_conc.append(conc)
        if ppw != 35:
            nonlin_labels.append('%d PW' % ppw)
            axis1, conc = read_concentration(d + '/' + 'concentration_nonlinear_max_E.txt')
            nonlin_e_conc.append(conc)
            axis2, conc = read_concentration(d + '/' + 'concentration_nonlinear_max_B.txt')
            nonlin_b_conc.append(conc)
        else:
            nonlin_labels.append('%d PW regime 1' % ppw)
            axis1, conc = read_concentration(d + '/' + 'concentration_nonlinear1_max_E.txt')
            nonlin_e_conc.append(conc)
            axis2, conc = read_concentration(d + '/' + 'concentration_nonlinear1_max_B.txt')
            nonlin_b_conc.append(conc)
            nonlin_labels.append('%d PW regime 2' % ppw)
            axis1, conc = read_concentration(d + '/' + 'concentration_nonlinear2_max_E.txt')
            nonlin_e_conc.append(conc)
            axis2, conc = read_concentration(d + '/' + 'concentration_nonlinear2_max_B.txt')
            nonlin_b_conc.append(conc)

        data = read_one_file(d + '/linear_ph.txt')
        av_ph_lin_r.append(float(data[0]))
        max_ph_lin_r.append(float(data[1]))
        av_ph_lin.append(float(data[2]))
        max_ph_lin.append(float(data[3]))
        data = read_one_file(d + '/linear_el.txt')
        av_el_lin_r.append(float(data[0]))
        max_el_lin_r.append(float(data[1]))
        av_el_lin.append(float(data[2]))
        max_el_lin.append(float(data[3]))
        if ppw != 35:
            data = read_one_file(d + '/nonlinear_ph.txt')
            av_ph_nonlin_r.append(float(data[0]))
            max_ph_nonlin_r.append(float(data[1]))
            av_ph_nonlin.append(float(data[2]))
            max_ph_nonlin.append(float(data[3]))
            data = read_one_file(d + '/nonlinear_el.txt')
            av_el_nonlin_r.append(float(data[0]))
            max_el_nonlin_r.append(float(data[1]))
            av_el_nonlin.append(float(data[2]))
            max_el_nonlin.append(float(data[3]))
        else:
            data = read_one_file(d + '/nonlinear1_ph.txt')
            av_ph_nonlin_r.append(float(data[0]))
            max_ph_nonlin_r.append(float(data[1]))
            av_ph_nonlin.append(float(data[2]))
            max_ph_nonlin.append(float(data[3]))
            data = read_one_file(d + '/nonlinear1_el.txt')
            av_el_nonlin_r.append(float(data[0]))
            max_el_nonlin_r.append(float(data[1]))
            av_el_nonlin.append(float(data[2]))
            max_el_nonlin.append(float(data[3]))
            nonlin_power.append(ppw)
            data = read_one_file(d + '/nonlinear2_ph.txt')
            av_ph_nonlin_r.append(float(data[0]))
            max_ph_nonlin_r.append(float(data[1]))
            av_ph_nonlin.append(float(data[2]))
            max_ph_nonlin.append(float(data[3]))
            data = read_one_file(d + '/nonlinear2_el.txt')
            av_el_nonlin_r.append(float(data[0]))
            max_el_nonlin_r.append(float(data[1]))
            av_el_nonlin.append(float(data[2]))
            max_el_nonlin.append(float(data[3]))

    fig = plt.figure(num=None, figsize = (18,12))
    mp.rcParams.update({'font.size': 18})
    picname = picspath + '/' + "mdipole_dn.png"
    ax1 = fig.add_subplot(2,2,1)
    ax1.set_title('(a) Maximum energy (linear)')
    print len(power), len(max_ph_lin)
    ax1.plot(power, max_ph_lin, 'r', label = 'Ph full')
    ax1.plot(power, max_ph_lin_r, 'r', dashes = [3,3], label = 'Ph rad')
    ax1.plot(power, max_el_lin, 'b', label = 'El full')
    ax1.plot(power, max_el_lin_r, 'b', dashes = [3,3], label = 'El rad')
    ax1.set_xlabel('Power, PW')
    ax1.set_ylabel('Energy, GeV')
    plt.legend(loc = 'lower left', frameon = False)
    ax1 = fig.add_subplot(2,2,2)
    ax1.set_title('(b) Average energy (linear)')
    ax1.plot(power, av_ph_lin, 'r', label = 'Ph full')
    ax1.plot(power, av_ph_lin_r, 'r', dashes = [3,3], label = 'Ph rad')
    ax1.plot(power, av_el_lin, 'b', label = 'El full')
    ax1.plot(power, av_el_lin_r, 'b', dashes = [3,3], label = 'El rad')
    ax1.set_xlabel('Power, PW')
    ax1.set_ylabel('Energy, GeV')
    plt.legend(loc = 'lower left', frameon = False)
    ax1 = fig.add_subplot(2,2,3)
    ax1.set_title('(c) Maximum energy (nonlinear)')
    ax1.plot(nonlin_power, max_ph_nonlin, 'r', label = 'Ph full')
    ax1.plot(nonlin_power, max_ph_nonlin_r, 'r', dashes = [3,3], label = 'Ph rad')
    ax1.plot(nonlin_power, max_el_nonlin, 'b', label = 'El full')
    ax1.plot(nonlin_power, max_el_nonlin_r, 'b', dashes = [3,3], label = 'El rad')
    ax1.set_xlabel('Power, PW')
    ax1.set_ylabel('Energy, GeV')
    plt.legend(loc = 'lower left', frameon = False)
    ax1 = fig.add_subplot(2,2,4)
    ax1.set_title('(d) Average energy (linear)')
    ax1.plot(nonlin_power, av_ph_nonlin, 'r', label = 'Ph full')
    ax1.plot(nonlin_power, av_ph_nonlin_r, 'r', dashes = [3,3], label = 'Ph rad')
    ax1.plot(nonlin_power, av_el_nonlin, 'b', label = 'El full')
    ax1.plot(nonlin_power, av_el_nonlin_r, 'b', dashes = [3,3], label = 'El rad')
    ax1.set_xlabel('Power, PW')
    ax1.set_ylabel('Energy, GeV')
    plt.legend(loc = 'lower left', frameon = False)
    print picname
    plt.tight_layout()
    plt.savefig(picname)
    plt.close()
        
        
    jet = cm = plt.get_cmap('jet')
    values = range(len(nonlin_e_conc)) 
    cNorm  = clr.Normalize(vmin=0, vmax=values[-1])
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)
    print scalarMap.get_clim()
    config = utils.get_config(sys.argv[1] + "/ParsedInput.txt")
    Xmax = float(config['X_Max']) #mkm
    Xmin = float(config['X_Min']) #mkm
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    dx = (Xmax-Xmin)/nx
    read, ez = utils.bo_file_load(sys.argv[1] + '/' + utils.ezpath,277,nx,ny)
    read, bz = utils.bo_file_load(sys.argv[1] + '/' + utils.bzpath,270,nx,ny)
    ezp = ez[nx/2]
    bzp = bz[nx/2]
    ezp = ezp/np.amax(ezp)*0.2
    bzp = bzp/np.amax(bzp)*0.2*1.53
   
    e_axis = utils.create_axis(nx, dx*1e4, Xmin*1e4)
    
    fig = plt.figure(num=None, figsize = (18,12))
    mp.rcParams.update({'font.size': 18})
    picname = picspath + '/' + "mdipole_concentration.png"
    ax1 = fig.add_subplot(2,2,1)
    ax1.set_title('(a) Maximum electric field (linear)')
    ax1.set_xlabel('r, mkm')
    ax1.set_ylabel('N, a.u.')
    for i in range(len(lin_e_conc)):
        colorVal = scalarMap.to_rgba(values[i])
        ax1.plot(axis1, lin_e_conc[i], color = colorVal, label = lin_labels[i])
        plt.legend(loc = 'upper right')
        ax1.set_xlim([0,1.5])
    ax11 = ax1.twinx()
    ax11.plot(e_axis, ezp, color = 'g', dashes = [2,2])
    ax11.plot(e_axis, bzp, color = 'b', dashes = [2,2])
    ax11.set_ylim([0,1])    
    ax2 = fig.add_subplot(2,2,2)
    ax2.set_title('(b) Maximum magnetic field (linear)')
    ax2.set_xlabel('r, mkm')
    ax2.set_ylabel('N, a.u.')
    for i in range(len(lin_b_conc)):
        colorVal = scalarMap.to_rgba(values[i])
        ax2.plot(axis1, lin_b_conc[i], color = colorVal, label = lin_labels[i])
        plt.legend(loc = 'upper right')
        ax2.set_xlim([0,1.5])
    ax11 = ax2.twinx()
    ax11.plot(e_axis, ezp, color = 'g', dashes = [2,2])
    ax11.plot(e_axis, bzp, color = 'b', dashes = [2,2])
    ax11.set_ylim([0,1])
    ax3 = fig.add_subplot(2,2,3)
    ax3.set_title('(c) Maximum electric field (nonlinear)')
    ax3.set_xlabel('r, mkm')
    ax3.set_ylabel('N, a.u.')
    for i in range(len(nonlin_e_conc)):
        colorVal = scalarMap.to_rgba(values[i])
        ax3.plot(axis1, nonlin_e_conc[i], color = colorVal, label = nonlin_labels[i])
        plt.legend(loc = 'upper right')
        ax3.set_xlim([0,2])
    ax11 = ax3.twinx()
    ax11.plot(e_axis, ezp, color = 'g', dashes = [2,2])
    ax11.plot(e_axis, bzp, color = 'b', dashes = [2,2])
    ax11.set_ylim([0,1])
    ax4 = fig.add_subplot(2,2,4)
    ax4.set_title('(d) Maximum magnetic field (nonlinear)')
    ax4.set_xlabel('r, mkm')
    ax4.set_ylabel('N, a.u.')
    for i in range(len(nonlin_b_conc)):
        colorVal = scalarMap.to_rgba(values[i])
        ax4.plot(axis1, nonlin_b_conc[i], color = colorVal, label = nonlin_labels[i])
        plt.legend(loc = 'upper right')
        ax4.set_xlim([0,2])
    ax11 = ax4.twinx()
    ax11.plot(e_axis, ezp, color = 'g', dashes = [2,2])
    ax11.plot(e_axis, bzp, color = 'b', dashes = [2,2])
    ax11.set_ylim([0,1])
    print picname
    plt.tight_layout()
    plt.savefig(picname)
    plt.close()

    
    p_ = np.linspace(10, 70, 50)
    e_ = np.sqrt(p_/10)*3e11/1.53
    b_ = np.sqrt(p_/10)*3e11
        
    fig = plt.figure(num=None, figsize = (21,15))
    mp.rcParams.update({'font.size': 18})
    picname = picspath + '/' + "mdipole_params.png"
    ax = fig.add_subplot(3,3,1)
    ax.plot(power, ne)
    ax.set_title('(a) Average density')
    ax.set_xlabel('Power, PW')
    ax.set_ylabel('Average pair density, cm-3')
    ax.axvline(x = 30, color = 'g')
    ax.axvline(x = 40, color = 'g')
    
    ax = fig.add_subplot(3,3,2)
    ax.plot(power, ne_max)
    ax.set_title('(b) Maximum pair density')
    ax.set_xlabel('Power, PW')
    ax.set_ylabel('Max pair density, cm-3')
    ax.axvline(x = 30, color = 'g')
    ax.axvline(x = 40, color = 'g')
    
    ax = fig.add_subplot(3,3,3)
    ax.set_title('(c) E,B fields')
    ax.plot(power, b, 'b', label = 'B')
    ax.plot(power, e, 'r', label = 'E')
    ax.plot(p_, b_, 'b', label = 'B vac', dashes = [2,2])
    ax.plot(p_, e_, 'r', label = 'E vac', dashes = [2,2])
    plt.legend(loc = 'upper left', frameon = False)
    ax.set_ylabel('E,B, CGS')
    ax.set_xlabel('Power, PW')
    ax.axvline(x = 30, color = 'g')
    ax.axvline(x = 40, color = 'g')
    
    ax = fig.add_subplot(3,3,4)
    ax.plot(power, nl_p, 'b', label = 'field')
    ax.plot(power, p_ph, 'r', label = 'ph')
    ax.plot(power, np.array(p_el)*10, 'g', label = 'el x10')
    ax.set_title('(d) Radiated power')
    ax.set_ylabel('P, PW')
    ax.set_xlabel('Power, PW')
    plt.legend(loc = 'upper left', frameon = False)
    ax.axvline(x = 30, color = 'g')
    ax.axvline(x = 40, color = 'g')
    
    ax = fig.add_subplot(3,3,5)
    ax.plot(power, en_max_ph, 'r', label = 'ph')
    ax.plot(power, en_max_el, 'g', label = 'el')
    ax.set_title('(e) Maximum particle energy')
    ax.set_xlabel('Power, PW')
    ax.set_ylabel('W, GeV')
    plt.legend(loc = 'upper left', frameon = False)
    ax.axvline(x = 30, color = 'g')
    ax.axvline(x = 40, color = 'g')
    
    ax = fig.add_subplot(3,3,6)
    ax.plot(power, en_av_ph, 'r', label = 'ph')
    ax.plot(power, en_av_el, 'g', label = 'el')
    ax.set_title('(f) Average particle energy')
    ax.set_xlabel('Power, PW')
    ax.set_ylabel('W, GeV')
    plt.legend(loc = 'upper left', frameon = False)
    ax.axvline(x = 30, color = 'g')
    ax.axvline(x = 40, color = 'g')
    
    ax = fig.add_subplot(3,3,7)
    ax.plot(power, flux_ph_full, 'r', label = 'ph')
    ax.plot(power, np.array(flux_el_full)*50, 'g', label = 'el x50')
    ax.set_title('(g) Full flux')
    ax.set_xlabel('Power, PW')
    ax.set_ylabel('Flux, s-1')
    plt.legend(loc = 'upper left', frameon = False)
    ax.axvline(x = 30, color = 'g')
    ax.axvline(x = 40, color = 'g')
    
    ax = fig.add_subplot(3,3,8)
    ax.plot(power, flux_ph_100mev, 'r', label = 'ph')
    ax.plot(power, np.array(flux_el_100mev)*10, 'g', label = 'el x10')
    ax.set_title('(h) Flux > 100 MeV')
    ax.set_xlabel('Power, PW')
    ax.set_ylabel('Flux, s-1')
    plt.legend(loc = 'upper left', frameon = False)
    ax.axvline(x = 30, color = 'g')
    ax.axvline(x = 40, color = 'g')
    
    ax = fig.add_subplot(3,3,9)
    ax.plot(power, flux_ph_1gev, 'r', label = 'ph')
    ax.plot(power, np.array(flux_el_1gev)*10., 'g', label = 'el x10')
    ax.set_title('(i) Flux > 1 Gev')
    ax.set_xlabel('Power, PW')
    ax.set_ylabel('Flux, s-1')
    plt.legend(loc = 'upper left', frameon = False)
    ax.axvline(x = 30, color = 'g')
    ax.axvline(x = 40, color = 'g')
    print picname
    plt.tight_layout()
    plt.savefig(picname)
    plt.close()

    f = open('gamma_m.dat', 'w')
    plist = [9, 10, 12, 15, 20, 30, 40, 50, 60, 70, 80, 100, 200]
    for i, p in enumerate(power):
        if p in plist:
            f.write('%lf %lf\n' %(power[i], g[i]))
    f.close()

if __name__ == '__main__':
    main()
