#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mpl
import numpy as np
import utils
import sys
from multiprocessing.dummy import Pool as ThreadPool 
from pylab import *

ppath = ''
ne = 0
nt = 0

def sum2d(a):
    s = 0
    print len(a)
    for i in range(len(a)):
        s += sum(a[i])/(i+0.5)
    return s
 
def write_field2d(a, file):
    print file
    f = open(file, 'w')
    for j in range(len(a[0])):
        for i in range(len(a)):
            f.write('%f ' % a[i][j])
    f.close()

def read_field2d(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    array = np.fromfile(f, sep=' ')
    f.close()
    print file, len(array)
    res = np.reshape(array, (nx,ny), order = 'F')
    num = 0
    for i in range(len(res)):
        num += sum(res[i])/(i+0.425)

#    if num != 0:
#        for i in range(len(res)):
#            for j in range(len(res[0])):
#                res[i][j] /= num

    return res

def read_saved(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    array = np.fromfile(f, sep=' ')
    f.close()
#    print file, len(array)
    res = np.reshape(array, (nx,ny), order = 'F')
    return res

def create_axis(n,step, x0):
    axis = []
#    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def sp_theta(a,level=0,de=1.):
    res = [0]*len(a[0])
    res1 = [0]*len(a[0])
#    print a[0]
#    print de, level
    for i in range(len(a)):
#        if i*de > level:
        for j in range(len(a[0])):
#                if a[i][j] != 0:
#                    print 'here'
            res[j] += a[i][j]
            res1[j] += a[i][j]/(i+0.5)
#    m = max(res)
#    print res
#    print m
#    if m != 0:
#        for i in range(len(res)):
#            res[i] /= m
    m = sum(res1)
    for j in range(len(res)):
        res[j] /= m
        res1[j] /= m
    return res, res1

def br_theta(a,level,dth,dph):
    res = [0]*len(a[0])
    s = [0]*len(a[0])
    for i in range(len(a)):
        for j in range(len(a[0])):
            for j1 in range(j):
                res[j] += a[i][j1]
                s[j] += math.sin((j1+0.5)*dth)*dth*dph*1e6
    for j in range(len(res)):
        if s[j] > 0:
            res[j] /= s[j]
    return res

def norm(a):
    m = max2d(a)
    if m != 0:
        for i in range(len(a)):
            for j in range(len(a[0])):
                a[i][j] /= m
                a[i][j] += 1e-10
    return a

def norm1(a, m = 1.):
    if m != 0:
        for i in range(len(a)):
            for j in range(len(a[0])):
                a[i][j] /= m
    return a

def sp_energy(a):
    res = [0]*len(a)
    for i in range(len(a)):
        for j in range(len(a[0])):
            res[i] += a[i][j]
#    m = max(res)
#    if m != 0:
#        for i in range(len(res)):
#            res[i] /= m
    return res

def find_max_nz(a):
    n = len(a)
    for i in range(1,len(a)):
        if a[-i] > 1e-8:
            n = len(a)-i
            break
    return n

def smooth(a,factor):
    n = len(a)
    n1 = n/factor
    if n1%factor != 0:
        print 'Error'
    res = [0] * n1
    for i in range(n1):
        for j in range(factor):
            res[i] += a[i*factor + j]/factor
    return res

def find_angle_max(a,dt):
    res = [0]*len(a)
    for i in range(len(a)):
        maxj = 0
        maxn = 0.
        for j in range(len(a[0])):
            if a[i][j] > maxn:
                maxj = j
                maxn = a[i][j]
#                print 'here', a[i][j], maxn, maxj, j
        res[i] = maxj*dt
#        print i, maxj, res[j]
#    print res
    return res

def smooth_2d(a,factor_ne,factor_nt):
    ne = len(a)
    nt = len(a[0])
    nt1 = nt/factor_nt
    ne1 = ne/factor_ne
    print ne, nt, ne1, nt1
    res = np.zeros((ne1,nt1))

    for j in range(ne1):
        for i in range(nt1):
            for k in range(factor_nt):
                for m in range(factor_ne):
                    res[j][i] += a[j*factor_ne+m][i*factor_nt+k]/factor_ne/factor_nt + 1e-15
    
    return res

def add_sp(res,a):
    num = 0
    for i in range(len(a)):
        for j in range(len(a[0])):
            num += a[i][j]/(i+0.5)
    for i in range(len(a)):
        for j in range(len(a[0])):
            res[i][j] += a[i][j]/num
    return res

def max2d(array):
    return max([max(x) for x in array])

def process(n):
    phname = ppath + '%.4f.txt' % (n)
    print 'Processing' + phname
    res = read_field2d(phname,ne,nt)
    return res

def find_max_energy(array,step):
    n = len(array)
    full = 0
    summ = 0
    imax = 0
    max_en = 0
    for i in range(n):
        full += array[i]*step

    for i in range(1,n):
        summ = summ + array[-i]*step
        if summ > 0.01*full:
            imax = i
            break
    if imax == 0:
        max_en = 0
    else:
        max_en = (n-imax) * step

    return max_en

def main():
    path = '.'
    
    picspath = 'pics'
    elpath = '/statdata/el/EnAngSp/'
    phpath = '/statdata/ph/EnAngSp/'
    pospath = '/statdata/pos/EnAngSp/'

    num = len(sys.argv) - 1
    regime = sys.argv[1]
    wtype = 1
    paths = ['analitycal_g', 'analitycal_ng', 'numerical_g', 'numerical_ng']
    path = paths[0]
    if regime == 'r':
        print 'read file'
    elif regime == 'rs':
        print 'read smoothed file'
    elif regime == 'c':
        print 'calculate file'
    elif regime == 'cs':
        print 'calculate smoothed file'
    else:
        print 'Wrong type, correct r - read or c - calculate, rs - read smoothed file, cs - calculate smoothed file'
        sys.exit(0)

    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(path + phpath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[2] 
        nmin = int(sys.argv[2])
        nmax = utils.num_files(path + elpath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[2] + ' to ' + sys.argv[3]
        nmin = int(sys.argv[2])
        nmax = int(sys.argv[3])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[2] + ' to ' + sys.argv[3] + ' with delta ' + sys.argv[4]
        nmin = int(sys.argv[2])
        nmax = int(sys.argv[3])
        delta = int(sys.argv[4])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax-nmin) + ' files'
   
    config = utils.get_config(path + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
#    omega = float(config['Omega'])
#    T = 2 * math.pi/omega*1e15
#    step = x0*y0*1e15/T
#    nT = int(1./step)
#    print nT
    ev = float(config['eV'])
    global ne,nt
    ne = int(config['QEDstatistics.OutputN_E'])
    nt = int(config['QEDstatistics.OutputN_theta'])
    tmax = float(config['QEDstatistics.ThetaMax'])*180./3.14159
    tmin = float(config['QEDstatistics.ThetaMin'])*180./3.14159
    emax = float(config['QEDstatistics.Emax'])/ev*1e-9
    emin = float(config['QEDstatistics.Emin'])/ev*1e-9
    
#    peakpower = float(config['PeakPower'])*1e-7*1e-15
#    const_a_p = 7.81441e-9
#    a0 = const_a_p * math.sqrt(peakpower*1e22)
    de = (emax - emin)/ne
    dt = (tmax - tmin)/nt
    dth = dt * 3.14159/180. 
    dph = 2*3.14159
    factor_nt = 1 #for mom
    factor_ne = 1 #for mom
#    factor_nt = 3 #for angular
#    factor_ne = 2 #for angular
    nt1 = nt/factor_nt
    ne1 = ne/factor_ne
    axe = create_axis(ne, de, 0)
    axt = create_axis(nt, dt, tmin)
    axe_f = create_axis(ne/factor_ne, de*factor_ne, 0)
    axe_f2 = create_axis(ne/factor_ne, de*factor_ne, 0)

    axt_f = create_axis(nt/factor_nt, dt*factor_nt, tmin)
#    omega = float(config['Omega'])
#    T = 2 * math.pi/omega*1e15
#    num = int(T/(x0*y0*1e15))
#    num = 120
#    print num, T
    
    nmins = [3, 3, 13, 13]
    nmaxs = [11, 11, 21, 21] 
    summs = []
    global ppath
    for j in range(len(paths)):
        ppath = paths[j] + phpath
#    pool = ThreadPool(8)
#    results = pool.map(process, range(nmin, nmax))
#    print len(results)
        summ = np.zeros((ne,nt)) 
        sphname = paths[j] + phpath + 'saved%d.txt' %(nmax-nmin)
        if regime == 'c':
            for i in range(nmins[j], nmaxs[j]):
                res = process(i)
                summ = add_sp(summ, res)
#                write_field2d(summ,sphname)
        elif regime == 'r':
            summ = read_saved(sphname, ne,nt)
        else:
            print 'Unknown regime'
            exit(-1)

        config = utils.get_config(paths[j] + "/ParsedInput.txt")
        pf = float(config['ParticlesFactor'])
#        summ = norm1(summ, pf)
        summs.append(summ)

    fig = plt.figure(num=None, figsize=(10, 10))
#    summ = norm(summ)
    factor_ne=1
    factor_nt=1
    axe = create_axis(ne/factor_ne, de*factor_ne, 0)
    axt = create_axis(nt/factor_nt, dt*factor_nt, tmin)
    axt_r = []
    for i in range(len(axt)):
        axt_r.append(axt[i] * math.pi/180.*1e3)
    mes = []   
    ax1 = fig.add_subplot(2,2,1)
    ax2 = fig.add_subplot(2,2,2)
    ax3 = fig.add_subplot(2,2,3)
    ax4 = fig.add_subplot(2,2,4)
   
    dtheta = dt*math.pi/180.
    
    for j in range(len(paths)):
        print 'Summ ', j, sum2d(summs[j])
        s_e = sp_energy(summs[j])
        
        ax1.plot(axe, s_e, label = paths[j])
        
        me = find_max_nz(s_e)*de*factor_ne
        mes.append(me)
        s_t, s_n = sp_theta(summs[j])
        s_n_sin = []
        for i in range(len(s_n)):
            s_n_sin.append(s_n[i]/math.sin((i+0.5)*dtheta))
        ax2.plot(axt_r, s_n, label = paths[j])
        ax3.plot(axt, s_n, label = paths[j])
        ax4.plot(axt_r, s_n_sin, label = paths[j])
   
    ax1.set_xlim([0, max(mes)])
#    ax1.set_ylim([1e-4, 1])
    ax1.set_yscale('log')
    ax1.set_xlabel('GeV')
#    print 'Here'
    plt.legend(loc = 'upper right')
    ax2.set_xlim([0, 5])
    ax4.set_xlim([0, 5])
    ax2.set_xlabel('mrad')
#    ax3.set_xlim([0, 1.])
    ax3.set_xlabel('mrad')
#    print 'Here'
#    s_t = sp_theta(summ)
#    s_t1 = []
#    for j in range(len(s_t)):
#        s_t1.append(s_t[j]/math.sin((j+0.5)*dth))
#    ax2.plot(axt, s_t, 'b')
#    ax2 = ax1.twinx()
#    ax2.plot(axt, s_t1, 'r')
#    ax2.set_yscale('log')
#        ax1.plot(axt, ph_t1, label = '> 1GeV')
#        ax1.plot(axt, ph_t2, label = '> W(1%)')
#    print s_t
#    te = find_max_nz(s_t)*dt*factor_nt
    
    picname = picspath + '/dncmp.png'
  
    plt.savefig(picname)
    plt.close()

if __name__ == '__main__':
    main()

'''    
for n in range(nmin,nmax):
        picname = picspath + '/' + "ldn%06d.png" % (n,)
        picname_ph = picspath + '/' + "dnp%06d.png" % (n,)

        if 'c' in regime:
            el = np.zeros((ne,nt))
            ph = np.zeros((ne,nt))
            el1 = np.zeros((ne,nt))
            ph1 = np.zeros((ne,nt))

            for j in range(num):
#                elname = path + elpath + '%.4f.txt' % (n+j)
                phname = path + phpath + '%.4f.txt' % (n+j)
           
#                el_,el1_ = read_field2d(elname,ne,nt,dth)
#                el = add_sp(el,el_)
#                el1 = add_sp(el1,el1_)
                ph_,ph1_ = read_field2d(phname,ne,nt,dth)
                ph = add_sp(ph,ph_)
                ph1 = add_sp(ph1,ph1_)

#            selname = path + elpath + 'c%.4f.txt' % (n)
            sphname = path + phpath + 'c_%.4f.txt' % (n)
            sph1name = path + phpath + 'c1_%.4f.txt' % (n)
            if 's' in regime:
#                el = smooth_2d(el,factor_ne,factor_nt)
                ph = smooth_2d(ph, factor_ne, factor_nt)
                ph1 = smooth_2d(ph1, factor_ne, factor_nt)
#            write_field2d(el, selname)
            write_field2d(ph, sphname)
            write_field2d(ph1, sph1name)

        if 'r' in regime:
#            elname = path + elpath + 'c%.4f.txt' % (n)
            phname = path + phpath + 'c_%.4f.txt' % (n)
            ph1name = path + phpath + 'c1_%.4f.txt' % (n)
            if 's' in regime:
#                el,el1 = read_field2d(elname,ne/factor_ne,nt/factor_nt,dth)
                ph = read_field_1_2d(phname,ne/factor_ne,nt/factor_nt,dth)
                ph1 = read_field_1_2d(ph1name,ne/factor_ne,nt/factor_nt,dth)
            else:
#                el,el1 = read_field2d(elname,ne,nt,dth)
                ph = read_field_1_2d(phname,ne,nt,dth)
                ph1 = read_field_1_2d(ph1name,ne,nt,dth)       
    el = ph1'''

'''        axph = fig.add_subplot(2,5,6)
        if not 's' in regime:
            ph = smooth_2d(ph, factor_ne, factor_nt)
            ph = norm(ph)
            axph.imshow(ph, aspect = 'auto',  extent = [tmin, tmax, emin, emax], origin = 'lower', norm=clr.LogNorm(ph.max()*1e-3, ph.max()))
            axph.set_ylabel('GeV')
            axph.set_xlabel('grad')
            axph.set_xlim([0,1])
            el_e = sp_energy(el)
            ph_e = sp_energy(ph)
            onep_energy = find_max_energy(ph_e,de*factor_ne)
            onep_energy_el = find_max_energy(el_e,de*factor_ne)
            print onep_energy
            el_t = sp_theta(el)
            el_t1 = sp_theta(el, 1, de*factor_ne)
            el_t2 = sp_theta(el, onep_energy_el, de*factor_ne)
            ph_t = sp_theta(ph)
            ph_t1 = sp_theta(ph, 1, de*factor_ne)
            ph_t2 = sp_theta(ph, onep_energy, de*factor_ne)
            axe = create_axis(ne/factor_ne, de*factor_ne, 0)
            axt = create_axis(nt/factor_nt, dt*factor_nt, tmin)
            me = find_max_nz(el_e)*de*factor_ne
            mp = find_max_nz(ph_e)*de*factor_ne
            te = find_max_nz(el_t)*dt*factor_nt
            tp = find_max_nz(ph_t)*dt*factor_nt 
            axel.set_ylim([0,me])
            axph.set_ylim([0,mp])
#        axph.set_xlim([0,20])
#        axel.set_xlim([0,20])
#        fig = plt.figure(num=None)
#        plt.rc('text', usetex=True)
#        mpl.rcParams.update({'font.size': 36})
#        axph = fig.add_subplot(1,1,1)
#        if not 's' in regime:
#            ph = smooth_2d(ph, factor_ne, factor_nt)
#        ph = norm(ph)
#        axph.imshow(ph, aspect = 'auto',  extent = [tmin, tmax, emin, emax], origin = 'lower', norm=clr.LogNorm(ph.max()*1e-3, ph.max()))
#        axph.set_ylabel('$\epsilon_{\gamma}$')
#        axph.set_xlabel('$\\theta$')
#        axph.set_ylim([0,mp])
#        plt.show()

        ax1 = fig.add_subplot(2,5,3)
        ax1.plot(axe, el_e)
        ax1.set_xlim([0, me])
        ax1.set_ylim([1e-4, 1])
        ax1.set_yscale('log')
        ax1.set_xlabel('GeV')
        onep_energy = find_max_energy(ph_e,de*factor_ne)
        ax1 = fig.add_subplot(2,5,4)
        ax1.plot(axt, el_t, label = 'All')
#        ax1.plot(axt, el_t1, label = '> 1GeV')
#        ax1.plot(axt, el_t2, label = '> W(1%)')
        ax1.set_xlim([0, te])
        ax1.set_xlabel('grad')
        plt.legend(loc = 'upper right')
        ax1 = fig.add_subplot(2,5,5)
        ax1.plot(axt, el_t, label = 'All')
#        ax1.plot(axt, el_t1, label = '> 1GeV')
#        ax1.plot(axt, el_t2, label = '> W(1%)')
        ax1.set_xlim([0, 1])
        ax1.set_xlabel('grad')
        plt.legend(loc = 'upper right')

        ax1 = fig.add_subplot(2,5,8)
        ax1.plot(axe, ph_e)
        ax1.set_xlim([0, me])
        ax1.set_ylim([1e-4, 1])
        ax1.set_xlabel('GeV')
        ax1.set_yscale('log')
        onep_energy = find_max_energy(ph_e,de*factor_ne)
        print onep_energy
        ax1 = fig.add_subplot(2,5,9)
        ax1.plot(axt, ph_t, label = 'All')
#        ax1.plot(axt, ph_t1, label = '> 1GeV')
#        ax1.plot(axt, ph_t2, label = '> W(1%)')
        ax1.set_xlim([0, te])
        ax1.set_xlabel('grad')
        plt.legend(loc = 'upper right')
        ax1 = fig.add_subplot(2,5,10)
        ax1.plot(axt, ph_t, label = 'All')
#        ax1.plot(axt, ph_t1, label = '> 1GeV')
#        ax1.plot(axt, ph_t2, label = '> W(1%)')
        ax1.set_xlim([0, 1])
        ax1.set_xlabel('grad')
        plt.legend(loc = 'upper right')

#        axx = fig.add_subplot(2,5,2, polar=True)
#        azimuths_p = np.radians(np.linspace(tmin, tmax, nt/factor_nt))
#        azimuths_n = -np.radians(np.linspace(tmin, tmax, nt/factor_nt))
#        zeniths = np.arange(emin, emax, de*factor_ne)
        
#        r_p,thetas_p = np.meshgrid(zeniths, azimuths_p)
#       r_n,thetas_n = np.meshgrid(zeniths, azimuths_n)
#        x = np.array(el).transpose()
        
#        axx.pcolormesh(thetas_p, r_p, x, norm=clr.LogNorm(x.max()*1e-3, x.max()))
#        axx.pcolormesh(thetas_n, r_n, x, norm=clr.LogNorm(x.max()*1e-3, x.max()))
        
#        axx.set_rscale('log')
#        axx.set_ylim([0.1,me])
#        print max2d(el), max2d(ph), de*factor_ne

#        axx = fig.add_subplot(2,5,7, polar=True)
#        x = np.array(ph).transpose()
       
#        axx.pcolormesh(thetas_p, r_p, x, norm=clr.LogNorm(x.max()*1e-3, x.max()))
#        axx.pcolormesh(thetas_n, r_n, x, norm=clr.LogNorm(x.max()*1e-3, x.max()))
#        axx.set_rscale('log')
#        axx.set_ylim([0.1,mp])'''
