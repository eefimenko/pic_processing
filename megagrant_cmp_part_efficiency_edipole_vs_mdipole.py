#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import sys
import math
import utils
import pickle
import os

class Data(object):
    def __init__(self, pickle_file):
        if os.path.exists(pickle_file):
            with open(pickle_file) as f:
                self.ne = pickle.load(f)
                self.number_tgt = pickle.load(f)
                self.efficiency_ph = pickle.load(f)
                self.efficiency_1gev_ph = pickle.load(f)
                self.efficiency_el = pickle.load(f)
                self.efficiency_1gev_el = pickle.load(f)
                self.efficiency_pos = pickle.load(f)
                self.efficiency_1gev_pos = pickle.load(f)
                self.number_ph = pickle.load(f)
                self.number_1gev_ph = pickle.load(f)
                self.number_el = pickle.load(f)
                self.number_1gev_el = pickle.load(f)
                self.number_pos = pickle.load(f)
                self.number_1gev_pos = pickle.load(f)


def main():
    savepath = '/home/evgeny/Dropbox/megagrant2021/'
    if len(sys.argv) == 2:
        savepath = sys.argv[1]
    fontsize = 18
    mp.rcParams.update({'font.size': fontsize})
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})

    edipole = Data('dump_12beams_edipole_2mkm.pkl')
    mdipole = Data('dump_12beams_mdipole_2mkm.pkl')
    
    fig = plt.figure(figsize = (15,6))
    ax1 = fig.add_subplot(1,2,1)
    ax2 = fig.add_subplot(1,2,2)

    ax1.plot(edipole.ne, edipole.number_pos, 'r', label = 'E-dipole')
    ax1.plot(mdipole.ne, mdipole.number_pos, 'b', label = 'M-dipole')
    ax1.plot(edipole.ne, edipole.number_1gev_pos, 'r--', label = 'E-dipole (> 1GeV)')
    ax1.plot(mdipole.ne, mdipole.number_1gev_pos, 'b--', label = 'M-dipole (> 1GeV)')
    ax1.plot(mdipole.ne, mdipole.number_tgt, 'g', dashes = [2,2], label = '$N_0$')
    
    ax2.plot(edipole.ne, edipole.efficiency_ph, 'r', label = 'E-dipole')
    ax2.plot(mdipole.ne, mdipole.efficiency_ph, 'b', label = 'M-dipole')
    ax2.plot(edipole.ne, edipole.efficiency_1gev_ph, 'r--', label = 'E-dipole (> 1GeV)')
    ax2.plot(mdipole.ne, mdipole.efficiency_1gev_ph, 'b--', label = 'M-dipole (> 1GeV)')

    ax1.set_xscale('log')
    ax1.set_yscale('log')
#    ax1.set_ylim([5e-3, 1]) 
    ax1.set_xlabel(u'$n_0/n_{c}$')
    ax1.set_ylabel(u'$N_{e^{+}}$')
    ax2.set_xscale('log')
    ax2.set_yscale('log')
#    ax2.set_ylim([1e-4, 2e-1]) 
    ax2.set_xlabel(u'$n_0/n_{c}$')
    ax2.set_ylabel(u'$W_{\hbar\omega}/W_l$')
    
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, 1.175), frameon = False, fontsize = fontsize-3, ncol = 3, columnspacing = 1)
    ax2.legend(loc='upper center', bbox_to_anchor=(0.5, 1.175), frameon = False, fontsize = fontsize-3, ncol = 2, columnspacing = 1)
    
    plt.tight_layout()
    ax1.text(0.00003, ax1.get_ylim()[1], '(a)')
    ax2.text(0.00003, ax2.get_ylim()[1], '(b)')
    
    picname = savepath + '/12beams_edipole_vs_mdipole.png'
    print(picname)
    plt.savefig(picname)

 

if __name__ == '__main__':
    main()

    
