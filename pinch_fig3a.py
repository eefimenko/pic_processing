#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils
import numpy as np

def main():
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
    dirs = ['27pw', '27pw_x4', '27pw_x16r']
    iterations = [398, 1524, 6070]
    configs = []
    numdirs = len(dirs)
    
    for i in range(numdirs):
        configs.append(utils.get_config(dirs[i] + "/ParsedInput.txt"))
    
    n = numdirs + 1
    
    picspath = 'pics'
        
    xmin = []
    xmax = []
    ymin = []
    ymax = []
    zmin = []
    zmax = []
    mult = []
    nx = []
    ny = []
    nz = []
    power = []
    for k in range(numdirs):
        if 'resize' in configs[k].keys():
            factor = int(configs[k]['resize'])
        else:
            factor = 1
        if 'rescale' in configs[k].keys():
            rescale = int(configs[k]['rescale'])
        else:
            rescale = 1
        print rescale
        wl = float(configs[k]['Wavelength'])
        Xmax = float(configs[k]['X_Max'])/wl/rescale
        Xmin = float(configs[k]['X_Min'])/wl/rescale
        Ymax = float(configs[k]['Y_Max'])/wl/rescale
        Ymin = float(configs[k]['Y_Min'])/wl/rescale
        Zmax = float(configs[k]['Z_Max'])/wl/rescale
        Zmin = float(configs[k]['Z_Min'])/wl/rescale
        xmax.append(Xmax) #mkm
        xmin.append(Xmin) #mkm
        ymax.append(Ymax) #mkm
        ymin.append(Ymin) #mkm
        zmax.append(Zmax) #mkm
        zmin.append(Zmin) #mkm
        nx.append(int(configs[k]['MatrixSize_X']))
        ny.append(int(configs[k]['MatrixSize_Y']))
        nz.append(int(configs[k]['MatrixSize_Z']))
        power.append(int(configs[k]['PeakPowerPW']))
        dx = (Xmax-Xmin)/int(configs[k]['MatrixSize_X'])
        dy = (Ymax-Ymin)/int(configs[k]['MatrixSize_Y'])
        dz = (Zmax-Zmin)/int(configs[k]['MatrixSize_Z'])
        print dx,dy,dz
        if 'r' not in dirs[k]:
            dv = 2.*dx*dy*dz*wl*wl*wl*factor
        else:
            dv = 2.*dx*dy*dz*wl*wl*wl*rescale
        mult.append(1.e-11)  
        mult.append(1./dv)
        step = float(configs[k]['TimeStep'])*1e15*float(configs[k]['BOIterationPass'])
        omega = float(configs[k]['Omega'])
        T = 2 * math.pi/omega
        nt = int(T*1e15/step)
        print nt

    #figures = [utils.ezpath, utils.bzpath, utils.nezpath, utils.npzpath, utils.nphzpath, utils.nizpath]
    #cmaps = ['Reds', 'Reds', 'Greens','Greens', 'Blues', 'Greens']
    #titles = ['Electric field %d pw', 'Magnetic field %d pw', 'Electrons %d pw','Positrons %d pw', 'Photons %d pw', 'Ions %d pw']
    #log = [False, False, True, True, True, True]
    #mult = [1,1,mult[0],mult[0],mult[0], mult[0]]
    figures = [utils.bypath, utils.neypath]
    cmaps = ['Blues', 'hot_r']
    titles = ['$n_\lambda=115$', '$n_\lambda=460$', '$n_\lambda=1840$']
    log = [False, True]
#    mult = [1, mult[0]]
    labels = ['$(a)$', '$(b)$', '$(c)$', '$(d)$', '$(e)$', '$(f)$', '$(g)$', '$(h)$']
   
#    values  = [1, 2.71/3.0, 3.04/(3.0*math.sqrt(2.)), 3.54/6., 1, 1, 1, 1]
#    for i in range(4):
#        print 7.2/(values[i]*values[i])
#    n0 = [703, 0, 0] # 675
#    n0 = [686, 0, 0] # 680
#    n0 = [1030, 1090]
    n0 = [0] * numdirs
    spx = numdirs
    spy = len(figures)
    nmin = 201
    nmax = 202
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
    fontsize = 20
    fig = plt.figure(num=None, figsize=(3.5*float(spy), 3.*float(spx)), dpi=256)
    mp.rcParams.update({'font.size': fontsize})
    
    for j in range(spy):
        for k in range(spx):
            zoom = False
            figure = figures[j]
            cmap = cmaps[j]
            if j == 0:
                ylabel = 'z/$\lambda$'
                yticklabels = ['$-0.2$', '$-0.1$', '$0$', '$0.1$', '$0.2$']
            else:
                ylabel = ''
                yticklabels = []
                
            if k == spx-1:
                zoom = True
                xlabel = 'y/$\lambda$'
                xticklabels = ['$-0.2$', '$-0.1$', '$0$', '$0.1$', '$0.2$']
            else:
                xlabel = ''
                xticklabels = []
                
            if figures[j] == utils.neypath or figures[j] == utils.nphypath:
                ncbarticks = None
            else:
                ncbarticks = 5
            zoom_xlim = [-0.02, 0.02]
            zoom_ylim = [-0.05, 0.05]
            zoom_val = 3
            ax = utils.subplot(fig, iterations[k], dirs[k]+'/'+figure,
                               shape = (nx[k],ny[k]), position = (spx,spy,j+1+k*spy),
                               extent = [xmin[k], xmax[k], ymin[k], ymax[k]], ratio = 1e-3,
                               cmap = cmap,  fontsize = fontsize, normalize = False, #title = titles[k+j*spx],
                               vmax = 1,
                               colorbar = True, logarithmic=log[j], verbose=0, transpose=1,
                               xlim = [-0.2,0.2], ylim = [-0.2,0.2], xticks = [-0.2, -0.1, 0, 0.1, 0.2], yticks = [-0.2, -0.1, 0, 0.1, 0.2],  xticklabels = xticklabels, yticklabels = yticklabels,
                               xlabel = xlabel, ylabel = ylabel, mult = mult[j + k*spy], zoom = zoom,
                               zoom_xlim=zoom_xlim, zoom_ylim=zoom_ylim, zoom_val = zoom_val, ncbarticks = ncbarticks)
            if j == 0:
                ax.text(-1.*0.2,0.7*0.2, labels[j+k*spy], fontsize = 22)
            else:
                ax.text(-1.*0.2,0.7*0.2, labels[j+k*spy], fontsize = 22)
            picname = picspath + '/' + "pinch_fig3a.png"
    plt.tight_layout()
    plt.savefig(picname, dpi=256)
    plt.close()

    
if __name__ == '__main__':
    main()
