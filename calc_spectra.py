#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import shutil
import os

def read_file_sp(file, de):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
    if 'tr' in file and not 'fs' in file:
        print 'removed'
        array, de = adjust_array(array, de)
        array[0] = 0
    for i in range(len(array)):
        nph  += array[i]/(i+0.425)/de
    print de
    for i in range(len(array)):
        array[i] /= de
#        array[i] = array[i]/(i+0.425)/de/de
   
    return array, nph

def read_file(file, de):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
    if 'tr' in file and not 'fs' in file:
        print 'removed'
        array, de = adjust_array(array, de)
        array[0] = 0
    for i in range(len(array)):
        nph  += array[i]
    for i in range(len(array)):
        array[i] *= (i+0.425)
#        array[i] /= de
           
    return array, nph

def check_length(file):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
       
    return len(array)

def adjust_array(array, de):
    n = len(array)
    adj_size = 10
    m = n/adj_size
    a = [0]*m
    for i in range(m):
        tmp = 0
        for j in range(adj_size):
            tmp += array[i*adj_size + j]
        a[i] = tmp
    return a, de*adj_size

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def read_one_spectrum(path1, nmin):
    config = utils.get_config(path1 + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV']) 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
    print num, T
    nph_t = []
    nph1_t = []
    ne_t = []
    np_t = []
    step = x0*y0*1e15/T 
    
    dt = y0*float(config['QEDstatistics.OutputIterStep'])*1e15

    path = '/statdata/ph/EnSp/'
    nepath = '/data/NeTrap/'
    nppath = '/data/NposTrap/'
    emin = float(config['QEDstatistics.Emin']) 
    emax = float(config['QEDstatistics.Emax']) 
    ne = int(config['QEDstatistics.OutputN_E'])
    de = (emax - emin)/ne
   
    t = 0
    
    n = check_length(path1 + path + '%.4f.txt'%nmin)
    if 'tr' in path1 and not 'fs' in path1:
        n /= 10
    axis = create_axis(n, de/ev*1e-9)

    spectrum1 = [0]*n
    
    for i in range(nmin,nmin+num):
        name = path1 + path + '%.4f.txt'%i
        print name
        sp1, nph = read_file(name, de)
       
        for k in range(n):
            if nph > 0:
                spectrum1[k] += sp1[k]/nph
    for k in range(n):
        spectrum1[k] /= num
    if 'tr' in path1 and not 'fs' in path1:
        de *= 10
    axis = create_axis(n, de/ev*1e-9)
    return spectrum1, axis

def read_one_spectrum_sp(path1, nmin):
    config = utils.get_config(path1 + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV']) 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
    print num, T
    nph_t = []
    nph1_t = []
    ne_t = []
    np_t = []
    step = x0*y0*1e15/T 
    
    dt = y0*float(config['QEDstatistics.OutputIterStep'])*1e15

    path = '/statdata/ph/EnSpSph/'
    nepath = '/data/NeTrap/'
    nppath = '/data/NposTrap/'
    emin = float(config['QEDstatistics.Emin']) 
    emax = float(config['QEDstatistics.Emax']) 
    ne = int(config['QEDstatistics.OutputN_E'])
    de = (emax - emin)/ne
   
    t = 0
    
    n = check_length(path1 + path + '%.4f.txt'%nmin)
    if 'tr' in path1 and not 'fs' in path1:
        n /= 10
  
    spectrum1 = [0]*n
   
    for i in range(nmin,nmin+num):
        name = path1 + path + '%.4f.txt'%i
        print name
        sp1, nph = read_file_sp(name, de)
       
        for k in range(n):
            if nph > 0:
                spectrum1[k] += sp1[k]/nph
    for k in range(n):
        spectrum1[k] /= num
    if 'tr' in path1 and not 'fs' in path1:
        de *= 10
    axis = create_axis(n, de/ev*1e-9)
    return spectrum1, axis



def main():
    picspath = 'pics'
    num = len(sys.argv) - 1
    if num < 2:
        print 'Wrong number of arguments'
        exit(-1)
    iteration = int(sys.argv[-1])
    
    for i in range(num-1):
        inpath = sys.argv[i+1]
        config = utils.get_config(inpath + "/ParsedInput.txt")
        ppw = int(config['PeakPowerPW'])
        sp_path = 'spectra_new_%d' % (ppw)
    
        if not os.path.exists(sp_path):
            os.makedirs(sp_path)
    
        spectrum, axis = read_one_spectrum(inpath, iteration)
        spectrum_sp, axis_sp = read_one_spectrum_sp(inpath, iteration)
        lbl = '%d PW'%(ppw)
        mm = 6
        fig = plt.figure(num=None, figsize=(20, 10))
        ax1 = fig.add_subplot(2,1,1)
        ax1.plot(axis, spectrum, label = lbl + ' full')
        ax1.set_xlim([1e-2, mm])
        #    ax1.set_xscale('log')
        ax1.set_yscale('log')
        ax1.legend(loc='upper right', shadow=True)
        ax1 = fig.add_subplot(2,1,2)
        ax1.plot(axis_sp, spectrum_sp, label = lbl + ' sphere')
        
        ax1.set_yscale('log')
        #    ax1.set_xscale('log')
        ax1.set_xlim([1e-2, mm])
        ax1.legend(loc='upper right', shadow=True)
        plt.grid()
        ax1.legend(loc='upper right', shadow=True)
            
        plt.savefig(sp_path + '/' + 'sp_%d_%d.png'%(ppw,int(iteration)))
        plt.close()
        out = open(sp_path + '/' + 'sp_full_%d.dat'%ppw, 'w')
        for i in range(len(spectrum)):
            out.write('%lf %le\n'%(axis[i], spectrum[i]))
        out.close()
        out = open(sp_path + '/' + 'sp_sphere_%d.dat'%ppw, 'w')
        for i in range(len(spectrum_sp)):
            out.write('%lf %le\n'%(axis_sp[i], spectrum_sp[i]))
        out.close()
        shutil.copy(inpath+'/Input.txt', sp_path)
        shutil.make_archive(sp_path, 'zip', sp_path)
    
if __name__ == '__main__':
    main()
