#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import shutil
import os

def read_file(file, de):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
    if 'tr' in file and not 'fs' in file:
        array[0] = 0
#        print 'removed'
        array, de = adjust_array(array, de)
    for i in range(len(array)):
        nph  += array[i]/(i+0.425)/de
    for i in range(len(array)):
        array[i] /= de
   
    return array, nph

def check_length(file):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
       
    return len(array)

def adjust_array(array, de):
    n = len(array)
    adj_size = 10
    m = n/adj_size
    a = [0]*m
    for i in range(m):
        tmp = 0
        for j in range(adj_size):
            tmp += array[i*adj_size + j]
        a[i] = tmp
    return a, de*adj_size

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def read_one_spectrum(path1, nmin):
    config = utils.get_config(path1 + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV']) 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
#    print num, T
    nph_t = []
    nph1_t = []
    ne_t = []
    np_t = []
    step = x0*y0*1e15/T 
    
    dt = y0*float(config['QEDstatistics.OutputIterStep'])*1e15

    path = '/statdata/ph/EnSpSph/'
    nepath = '/data/NeTrap/'
    nppath = '/data/NposTrap/'
    emin = float(config['QEDstatistics.Emin']) 
    emax = float(config['QEDstatistics.Emax']) 
    ne = int(config['QEDstatistics.OutputN_E'])
    de = (emax - emin)/ne
   
    t = 0
    
    n = check_length(path1 + path + '%.4f.txt'%nmin)
    if 'tr' in path1 and not 'fs' in path1:
        n /= 10
        de *= 10
    axis = create_axis(n, de/ev*1e-9)

    spectrum1 = [0]*n
    spectrum2 = [0]*n
    nn = 0
    for i in range(nmin,nmin+num):
        name = path1 + path + '%.4f.txt'%i
#        print name
        sp1, nph = read_file(name, de)
        nn += nph
        for k in range(n):
            spectrum1[k] += sp1[k]
            if nph > 0:
                spectrum2[k] += sp1[k]/nph
    for k in range(n):
        if nn > 0:
            spectrum1[k] /= nn
        spectrum2[k] /= num
    return nph, spectrum1, spectrum2, axis

def main():
    path = sys.argv[1]
    picspath = path+'/pics'
    if not os.path.exists(picspath):
        os.makedirs(picspath)
    num = len(sys.argv)-1
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(path+'/statdata/ph/EnSpSph/')
    elif num == 2:
        print 'Printing from pic ' + sys.argv[2] 
        nmin = int(sys.argv[2])
        nmax = utils.num_files(path+'/statdata/ph/EnSpSph/')
    elif num == 3:
        print 'Printing pics from ' + sys.argv[2] + ' to ' + sys.argv[3]
        nmin = int(sys.argv[2])
        nmax = int(sys.argv[3])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[2] + ' to ' + sys.argv[3] + ' with delta ' + sys.argv[4]
        nmin = int(sys.argv[2])
        nmax = int(sys.argv[3])
        delta = int(sys.argv[4])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax) + ' files'

    for k in range(nmin,nmax):
        nph, sp1, sp2, axis = read_one_spectrum(path, k)
        if nph == 0:
            print 'Skipped %.4f.txt, no particles'%k
            continue
        else:
            print 'Processed %.4f.txt, %le particles'%(k,nph)
        mm = 6
        fig = plt.figure(num=None, figsize=(20, 10))
        ax1 = fig.add_subplot(1,1,1)
        ax1.plot(axis, sp1, label = 'sum')
        ax1.plot(axis, sp2, label = 'each')
        ax1.set_xlim([0, mm])
        ax1.set_yscale('log')
        plt.grid()
        ax1.legend(loc='upper right', shadow=True)
        plt.savefig(picspath + '/' + 'cmpsp_%d.png'%k)
        plt.close()
        
if __name__ == '__main__':
    main()
