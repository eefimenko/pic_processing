#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import math
import utils
import sys
#import scipy.ndimage as ndimage

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            row.append(array[index]+ 1e-12)
        field.append(row)
    return field

def find_max(file, mult = 1.):
    max = 0
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]
    for p in array:
        if p > max:
            max = p
    return max

def find_max_from_all_fields(expath, eypath, ezpath, bxpath, bypath, bzpath, i):
    max = 0
    name = expath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = eypath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = ezpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    return max

def find_max_from_all_particles(xpath, ypath, zpath, i, mult = 1.):
    max = 0
    name = xpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    name = ypath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    name = zpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    return max
    
def max2d(array):
    return max([max(x) for x in array])    

def create_subplot(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, text, text1, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
    field = utils.bo_file_load(path,i,nx,ny)*mult
    field = np.transpose(field)
    maxe = np.max(field)
    mine = np.min(field)
    profx = field[:][nx/2]
    profy = field[ny/2][:]
    cmap1 = mp.cm.get_cmap(color)
    cmap1.set_under('w', False)
    print path, maxe, mine
    ratio = 1e-4
    if maxe == mine:
        v_min_ = maxe+1
        v_max_ = 1e9*(maxe+1)
    else:
        v_max_ = maxe
        v_min_ = maxe*ratio
   
    ticks = [-2,-1,0,1,2]
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = cmap1, vmin = v_min_, vmax = v_max_, norm=clr.LogNorm())
#    ax.text(-2.5, 2.5, text)
    ax.set_title(text) 
#    ax.text(1.6, 1.6, text1, fontsize=30)
    ax.set_xlim([-2,2])
    ax.set_ylim([-2,2])
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
    plt.xticks(ticks)
    plt.yticks(ticks)
#    plt.colorbar(surf,  orientation  = 'vertical', ticks = [v_min, v_max])
    return surf, profx,profy

def create_subplot_f(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, text, text1, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
    field = utils.bo_file_load(path,i,nx,ny)
    field = np.transpose(field)
    maxe = np.max(field)
    mine = np.min(field)
    profx = field[:][nx/2]
    profy = field[ny/2][:]
    cmap1 = mp.cm.get_cmap(color)
    cmap1.set_under('w', False)
    ratio = 1e-3
    ticks = [-2,-1,0,1,2]
#    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = v_max_*ratio, vmax = v_max_)
    if i < 240:
        amax = 1.
    elif i > 250:
#        amax = 0.5
        amax = 1.
    else:
        amax = 1.
        #amax = 1 - 0.5 * (i - 240)/10.
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = 0.0000001, vmax = amax)
    ax.set_title(text)
#    ax.text(1.6, 1.6, text1, fontsize=30)
    ax.set_xlim([-2,2])
    ax.set_ylim([-2,2])
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
    plt.xticks(ticks)
    plt.yticks(ticks)
#    plt.colorbar(surf,  orientation  = 'vertical', ticks = [v_min, v_max])
    return surf, profx,profy


def pulse_value(i,step,amp,tp,delay):
    ans = []
    tau = tp/1.1437
#    delay = 23.1099984185576458468467817
    t = i*step-delay
    if t > 0 and t < math.pi * tau:
        tmp = math.sin(t/tau)
        f = amp*tmp*tmp
    else:
        f = 0.
    return f

def main():
    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nixpath = 'data/Proton2Dx'
    niypath = 'data/Proton2Dy'
    nizpath = 'data/Proton2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nphxpath = 'data/Photon2Dx'
    nphypath = 'data/Photon2Dy'
    nphzpath = 'data/Photon2Dz'
    picspath = 'series'

    config = utils.get_config("ParsedInput.txt")

    BOIterationPass = float(config['BOIterationPass'])
    dt = float(config['TimeStep'])*1e15*BOIterationPass #fs
    maxpower = float(config['PeakPower'])*1e-7*1e-15 #PW
#    duration = float(config['Duration'])*1e15 #fs
#    delay = float(config['delay'])*1e15 #fs
    nx1 = int(config['ElPhase.SetMatrixSize_0'])
    ny1 = int(config['ElPhase.SetMatrixSize_1'])
    Emin = float(config['ElPhase.SetBounds_2'])
    Emax = float(config['ElPhase.SetBounds_3'])
    dg = (Emax-Emin)/ny1
    omega = float(config['Omega'])
    dt = float(config['TimeStep'])*BOIterationPass
    T = 2 * math.pi/omega
    dtt = dt/T
    print "Power = " + str(maxpower) + 'PW'
    print "dt = " + str(dt) + 'fs'
    wl = float(config['Wavelength'])
#    print "duration = " + str(duration)
#    print "delay = " + str(delay)
    
    Xmax = float(config['X_Max'])/wl #mkm
    Xmin = float(config['X_Min'])/wl #mkm
    Ymax = float(config['Y_Max'])/wl #mkm
    Ymin = float(config['Y_Min'])/wl #mkm
    Zmax = float(config['Z_Max'])/wl #mkm
    Zmin = float(config['Z_Min'])/wl #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz

    step = (Xmax-Xmin)/nx
    const_a_p = 7.81441e-9
    ppw = int(config['PeakPower'])*1e-22
    a0 = const_a_p * math.sqrt(ppw*1e22)
    a00 = 3.e11 * math.sqrt(ppw/10.)
    print a0, a00
    mult1 = 1/(2.*dx*dy*dz*1e-12)

    delta = 1
    axis = utils.create_axis(nx,dx,Xmin)
    axis1 = utils.create_axis(ny1,dg)
    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(expath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(expath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax) + ' files'

    maxf = 0
    maxe = 0
    maxi = 0
    maxp = 0
    maxph = 0
    print 'Reading files'

    for i in range(0,0):
        print "\r%.0f %% done" % (float(i)/nmax*100),
        sys.stdout.flush()        
        tmpf = find_max_from_all_fields(expath, eypath, ezpath, bxpath, bypath, bzpath, i)
        if tmpf > maxf:
            maxf = tmpf
        tmpe = find_max_from_all_particles(nexpath, neypath, nezpath, i, mult)
        if tmpe > maxe:
            maxe = tmpe
        tmpi = find_max_from_all_particles(nixpath, niypath, nizpath, i, mult)
        if tmpi > maxi:
            maxi = tmpi
        tmpp = find_max_from_all_particles(npxpath, npypath, npzpath, i, mult)
        if tmpp > maxp:
            maxp = tmpp
        tmpph = find_max_from_all_particles(nphxpath, nphypath, nphzpath, i, mult)
        if tmpph > maxph:
            maxph = tmpph
#    print maxf, maxe, maxi, maxp, maxph
    if maxi > maxe:
        maxe = maxi

    if maxp > maxe:
        maxe = maxp
    maxph += 1
#    maxf = 0
    fmin = maxf*1e-3
    fmax = maxf
#    cmin = maxe*1e-5
    cmax = maxe
    cmin = maxe*1e-3
    cimax = maxi
    cimin = maxi*1e-3
#    cmax = 4.5e25
    phmin = maxph*1e-3
    phmax = maxph
    print 'Here', nmin, nmax, delta
    nr = 2
    nc = 5
    for i in range(nmin,nmax,delta):
        print "\rSaving s%06d.png, %.0f %% done" % (i,float(i-nmin)/(nmax-nmin)*100),
        sys.stdout.flush()
        t = i * dtt
        
        fig = plt.figure(num=None, figsize=(10, 5), dpi=256)
        mp.rcParams.update({'font.size': 10})
        s,e1,e2 = create_subplot_f(fig,i,eypath,nx,ny,nr,nc,1, Xmin, Xmax, Ymin, Ymax, fmin, fmax, '', '$z/\lambda$', 'Reds', u'Электрическое\n поле', 'a', mult = 1./a00)
        s,ez1,ez2 = create_subplot_f(fig,i,ezpath,nx,ny,nr,nc,nc+1, Xmin, Xmax, Ymin, Ymax, fmin, fmax, '$y/\lambda$', '$x/\lambda$', 'Reds', '', 'e', mult = 1./a00)
        s,b1,b2 = create_subplot_f(fig,i,bypath,nx,ny,nr,nc,2, Xmin, Xmax, Ymin, Ymax, fmin, fmax, '', '','Reds', u'Магнитное\n поле', 'b', mult = 1./a00)
        s,bz1,bz2 = create_subplot_f(fig,i,bzpath,nx,ny,nr,nc,nc+2, Xmin, Xmax, Ymin, Ymax, fmin, fmax, '$y/\lambda$', '', 'Reds', '', 'f', mult = 1./a00)
        s,ne1,ne2 = create_subplot(fig,i,neypath,nx,ny,nr,nc,3, Xmin, Xmax, Ymin, Ymax, cmin, cmax, '', '','Greens', u'Распределение\n электронов', 'c', mult1)
        s,nez1,nez2 = create_subplot(fig,i,nezpath,nx,ny,nr,nc,nc+3, Xmin, Xmax, Ymin, Ymax, cmin, cmax, '$y/\lambda$', '', 'Greens', '', 'g', mult1)
        s,np1,np2 = create_subplot(fig,i,npypath,nx,ny,nr,nc,4, Xmin, Xmax, Ymin, Ymax, phmin, phmax, '', '', 'Blues', u'Распределение\n фотонов', 'd', mult1)
        s,npz1,npz2 = create_subplot(fig,i,npzpath,nx,ny,nr,nc,nc+4, Xmin, Xmax, Ymin, Ymax, phmin, phmax, '$y/\lambda$', '', 'Blues', '', 'h', mult1)
        ax = fig.add_subplot(nr,nc,5)
        ax.plot(axis,e1, 'r')
        ax.plot(axis,b1, 'g')
        ax1 = ax.twinx()
        ax1.plot(axis,ne1, 'b')
        ax1.plot(axis,np1, 'k')
        ax1.set_xlim([-2,2])
        f = open('profile_data_%d.dat'%i, 'w')
        for idx_ in xrange(nx/2):
            idx = nx/2+idx_
            f.write('%lf %le %le %le\n' %(axis[idx], e1[idx], b1[idx], ne1[idx]))
        f.close()
        ax = fig.add_subplot(nr,nc,nc+5)
       
        pfield = utils.bo_file_load(utils.ppath,i,nx1,ny1)
        res = np.sum(pfield, axis=0)
        ax.plot(axis1,res,'r')
        f = open('gamma_data_%d.dat'%i, 'w')
        idxmax = ny1
        for idx in xrange(ny1):
            if res[-1-idx] != 0:
                idxmax = ny1-idx
                break
        for idx in xrange(idxmax):
            f.write('%lf %le\n' %(axis1[idx], res[idx]))
        f.close()
        ax.set_xlim([0,1000])
#        ax.plot(b2, 'g')
#        ax1 = ax.twinx()
#        ax1.plot(ne2, 'b')
#        ax1.plot(np2, 'k')
        picname = picspath + '/' + "ufn%06d.png" % (i,)
        plt.tight_layout()
        plt.savefig(picname)
        plt.close()

if __name__ == '__main__':
    main()
