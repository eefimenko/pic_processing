#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np
import itertools
import os

def read_gamma_file(filename):
    f = open(filename)
    power = []
    gamma = []
    for line in f:
        tmp = line.split()
        power.append(float(tmp[0]))
        gamma.append(float(tmp[1]))
    return power, gamma
        

def read_ntrapped(path,ne,np = None):
    f = open(path + '/ntrapped.txt', 'r')
    i = 0
    for line in f:
        values = line.split()
        i = int(values[0])
        ne[i] = float(values[1])
        if np != None:
            np[i] = float(values[2])
    return i

def save_ntrapped(path,ne,npos = None,nmin = 0,nmax = 0):
    f = open(path + '/ntrapped.txt', 'a')
    if nmax == 0:
        nmax = len(ne)
    for i in range(nmin,nmax):
        if npos != None:
            f.write('%d %le %le\n'%(i,ne[i],npos[i]))
        else:
            f.write('%d %le\n'%(i,ne[i]))
    f.close()
    
def get_immediate_subdirectories(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]
def main():
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
#    plt.rc('text', usetex = True)
    num = len(sys.argv)
#    picspath = 'pics'
    picspath = '/home/evgeny/Dropbox/tmp_pics/'
    wd = os.getcwd()

    wtypes = ['edipole', 'mdipole']

    fig = plt.figure(num=None, figsize=(9, 5))
    
    ax1 = fig.add_subplot(1,1,1)
    ax1.set_ylabel(u'Показатель роста, ГТ')
    ax1.set_xlabel(u'Мощность, ПВт')
    
    
    
    i0 = 89
    i1 = 121
    print 'here'
    for wtype in wtypes:
        path = wtype + '/'
        if wtype == 'edipole':
            label = 'E-type'
        else:
            label = 'B-type'
        print path
        d = '0beams'
        power_dirs = get_immediate_subdirectories(wtype + '/' +d)
        growth_rate = {}
        for p in power_dirs:
            power = int(filter(str.isdigit, p))
            nmin, nmax = utils.find_min_max_from_directory(path + '/' + d + '/' + p + '/' + utils.netpath)
            config = utils.get_config(path + '/' + d + '/' + p + '/' + "/ParsedInput.txt")
            step = float(config['TimeStep'])*float(config['BOIterationPass'])
            omega = float(config['Omega'])
            T = 2.*math.pi/omega
            n = int(T/step) # period!
            num = (nmax - nmin)
            ne_ = np.zeros(num)
#            npos_ = np.zeros(num)
#            print 'Found ' + str(num) + ' files'
#            print str(n) + ' steps per period'
            if os.path.exists(path + p + '/ntrapped.txt'):
                nmin = read_ntrapped(path + '/' + d + '/' + p + '/',ne_) + 1

            for i in range(nmin,nmax):
#                print i
                ne_[i] = utils.ntrapped(wtype + '/' + d + '/' + p, i, particles = 'electrons', ftype = 'bin')
            save_ntrapped(path + '/' + d + '/' + p + '/', ne_, None, nmin, nmax)
#                npos_[i] = utils.ntrapped(d + '/' + p, i, particles = 'positrons', ftype = 'bin')

            length = num - n
            ge = np.zeros(length)
#            gp = np.zeros(length)
            for i in range(length):
                if ne_[i+n] > 0 and ne_[i] > 0:
                    ge[i] = math.log(ne_[i+n]/ne_[i])
                else:
                    ge[i] = 0.
#                if npos_[i+n] > 0 and npos_[i] > 0:
#                    gp[i] = math.log(npos_[i+n]/npos_[i])
#                else:
#                    gp[i] = 0.
            if wtype == '_m':
                if power < 40:
                    i0 = 10*n-1
                    i1 = 20*n-1
                else:
                    i0 = 9*n
                    i1 = 10*n
            else:
                if power < 10:
                    i0 = 6*n
                    i1 = 7*n
                else:
                    i0 = 9*n
                    i1 = 10*n
            growth_rate_ = np.sum(ge[i0:i1])/(i1-i0)
            print power, ' PW: ', growth_rate_
#            if nbeams in [3,4,5,6]:
#                fig = plt.figure(num=None, figsize=(10, 5))
#                ax11 = fig.add_subplot(1,1,1)
#                axis = utils.create_axis(length, step/T)
#                pe, = ax11.plot(axis, ge, 'r', label = 'Electrons')
#                ax11.axhline(y=0)
#                plt.show()
#            ax12 = fig.add_subplot(2,1,2)
            
#           axis_n = utils.create_axis(num, step/T)
#            pe, = ax11.plot(axis, ge, 'r', label = 'Electrons')
#            pp, = ax11.plot(axis, gp, 'b', label = 'Positrons')
#            pe, = ax12.plot(axis_n, ne_, 'r', label = 'Electrons')
#            pp, = ax12.plot(axis_n, 8*npos_, 'b', label = 'Positrons')
#            ax12.set_yscale('log')
#            ax11.axhline(y=0)
#            ax12.axhline(y=0)
#            plt.show()
#            plt.close()
#            exit(-1)
#            if power == 100 or power == 50:
#                i0 = 35
#                i1 = 51
#            elif power <= 30:
#                if d != '2beams':
#                    i0 = 121
#                    i1 = 129
#
#            else:
#                if d != '2beams':
#                    i0 = 63
#                    i1 = 79
#                else:
#                    i0 = 55
#                    i1 = 63
#            N_T = (i1-i0)/8.
            
            #config = utils.get_config(path + "/ParsedInput.txt")
#            n0 = utils.ntrapped(d + '/' + p, i0, particles = 'positrons', ftype = 'bin')
#            n1 = utils.ntrapped(d + '/' + p, i1, particles = 'positrons', ftype = 'bin')
            #print power, n0, n1, math.log(n1/n0)/N_T
#            print power, i0, i1, n1, n0, N_T, math.log(n1/n0)/N_T 
            growth_rate[power] = growth_rate_
        axis = []
        gamma = []
        for power_ in sorted(growth_rate.iterkeys()):
            axis.append(power_)
            gamma.append(growth_rate[power_])
        for i in range(len(gamma) - 1):
            threshold = 0
            if gamma[i] < 0 and gamma[i+1] > 0:
                g_ = abs(gamma[i]/gamma[i+1])
                threshold = (axis[i] + g_*axis[i+1])/(1 + g_)
                break
        print 'Threshold = ', threshold

        ax1.plot(axis, gamma, label = label)
        f = open('gamma_' + wtype + '.dat', 'w')
        for i_ in range(len(axis)):
            f.write('%lf %lf\n'%(axis[i_], gamma[i_]))
        f.close()
    ax1.axhline(y=0, color = 'k')
    power__, gamma__ = read_gamma_file('gamma.dat')
    ax1.plot(power__, gamma__, 'b', dashes = [2,2], label = 'E-type, 2 series')
    power__, gamma__ = read_gamma_file('gamma_m.dat')
    ax1.plot(power__, gamma__, 'g', dashes = [2,2], label = 'B-type, 2 series')
    plt.legend(loc = 'upper left', frameon = False)
    #plt.show()
    savename = picspath + '/gamma_cmp.png'
    print savename
    plt.tight_layout()
    plt.savefig(savename)
    plt.close()
    
if __name__ == '__main__':
    main()
