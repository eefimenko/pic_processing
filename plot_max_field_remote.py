#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def read_file(file):
    axis = []
    ez_t = []
    bx_t = []
    ne_t = []
    np_t = []
    f = open(file, 'r')
    for line in f:
        a = line.split()
        axis.append(a[0])
        ez_t.append(a[1])
        bx_t.append(a[2])
        ne_t.append(a[3])
        np_t.append(a[4])
    return axis, ez_t, bx_t, ne_t, np_t

def main():
    num = len(sys.argv)-1
    picspath = 'pics'
    fig = plt.figure()
    ezx = fig.add_subplot(2,2,1)
    bxx = fig.add_subplot(2,2,2)
    nex = fig.add_subplot(2,2,3)
    npx = fig.add_subplot(2,2,4)

    for i in range(num):
        path = sys.argv[i+1]
        axis, ez_t, bx_t, ne_t, np_t = read_file(path+'/res.dat')
        ezx.plot(axis, ez_t, utils.get_color(i), label = str(i))
        bxx.plot(axis, bx_t, utils.get_color(i), label = str(i))
        nex.plot(axis, ne_t, utils.get_color(i), label = str(i))
        npx.plot(axis, np_t, utils.get_color(i), label = str(i))

    ezx.legend(loc='upper right', shadow=True)
    bxx.legend(loc='upper right', shadow=True)
    nex.legend(loc='upper right', shadow=True)
    npx.legend(loc='upper right', shadow=True)
    plt.savefig(picspath + '/' + "ez_compare.png")
    plt.close()
if __name__ == '__main__':
    main()
