#!/usr/bin/python
import matplotlib as mp
#mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import sys
import math
import os, os.path
import utils
from tqdm import tqdm
import pickle

def find_max_photon_energy_1percent(array, step):
    summ = 0
    for j, e in enumerate(array):
        summ += j*e
    part_sum = 0
    N = len(array)
    for i in range(N):
        if array[N-i-1] > 0:
            part_sum += array[N-i-1]*(N - i - 1)
            
            if part_sum > 0.01 * summ:
                break
    
    return (N-i-1)*step

def main():
    picdir = '/home/evgeny/Dropbox/artXCELS/'
    fontsize = 16
            
    mp.rcParams.update({'font.size': fontsize})
    mp.rcParams['mathtext.fontset'] = 'custom'
    mp.rcParams['mathtext.it'] = 'Arial:italic'
    mp.rcParams['mathtext.rm'] = 'Arial'
        
    
    
    nbeams = [12, 6, 4]
    angles = [0.4, 0.3, 0.2]
    fnum = [1./(2. * math.tan(x)) for x in angles]
    maxs = [0.002, 0.004, 0.002]
    path = 'ideal_36PW'
    config = utils.get_config(os.path.join(path, "ParsedInput.txt"))
    dumpfile = os.path.join(path, 'dump.pkl')
    
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    dt = float(config['TimeStep'])
    ev = float(config['eV'])
    omega = float(config['Omega'])
    duration = float(config['PulseDuration'])*float(config['period'])  # s
    delay = float(config.get('delay',2e-14))  # s
    power = float(config['PeakPower'])*1e-7  # W
    powerPW = float(config['PeakPowerPW'])
    Emax = float(config['QEDstatistics.Emax'])
    Emin = float(config['QEDstatistics.Emin'])
    Th_max = float(config['QEDstatistics.ThetaMax'])
    Th_min = float(config['QEDstatistics.ThetaMin'])
    Phi_max = 2. * math.pi #float(config['QEDstatistics.PhiMax'])
    Phi_min = 0 #float(config['QEDstatistics.PhiMin'])
    N_E = int(config['QEDstatistics.OutputN_E'])
    N_Phi = int(config['QEDstatistics.OutputN_phi'])
    N_Th = int(config['QEDstatistics.OutputN_theta'])
    density = float(config['Ne'])
    print('Density ', density)
    radius = float(config['R'])
    de = (Emax - Emin)/N_E/1.6e-12/1e9
    de_ = (Emax - Emin)/N_E
    dth = (Th_max - Th_min)/N_Th  # erg -> eV -> GeV
    dphi = (Phi_max - Phi_min)/N_Phi
    ax_e = utils.create_axis(N_E, de)
    ax_th = utils.create_axis(N_Th, dth, dth/2.)
    ax_phi = utils.create_axis(N_Phi, dphi, 0)

    outStep = int(config['QEDstatistics.OutputIterStep'])
    tp = duration*1e15
    tdelay = delay*1e15
    n_tgt = density * 4./3. * math.pi * radius**3
    delay = float(config.get('delay', 2.2e-14))
    duration = float(config['PulseDuration'])*float(config['period'])
    period = float(config['period'])
    T = 2*math.pi/omega*1e15
    nit = int(config['QEDstatistics.OutputIterStep'])
    dt1 = float(config['TimeStep'])*nit
   
    omega = float(config['Omega'])
    alpha = float(config['alpha'])
    relField = float(config['RelativisticField'])
    verbose = 0

    if os.path.exists(dumpfile):
        with open(dumpfile, 'rb') as f:
            sp_ph_sum_0 = pickle.load(f)
            sp_el_sum_ = pickle.load(f)
            sp_pos_sum_ = pickle.load(f)
            ang_ph_sum_0 = pickle.load(f)
            ang_el_sum_ = pickle.load(f)
            ang_pos_sum_ = pickle.load(f)
            ph_en = pickle.load(f)
            ph_en_1gev = pickle.load(f)
            el_en = pickle.load(f)
            el_en_1gev = pickle.load(f)
            pos_en = pickle.load(f)
            pos_en_1gev = pickle.load(f)
            ph_flux = pickle.load(f)
            el_flux = pickle.load(f)
            pos_flux = pickle.load(f)
            ph_flux_1gev = pickle.load(f)
            el_flux_1gev = pickle.load(f)
            pos_flux_1gev = pickle.load(f)
            ang_sp_sph = pickle.load(f)
            el_ang_sp_sph0 = pickle.load(f)
            pos_ang_sp_sph0 = pickle.load(f)
    pulse_energy = powerPW*1e22*3*period*(alpha-2*np.sin(2*np.pi*alpha)/((alpha**2-1)*(alpha**2-4)*np.pi))/(16)*1e-7
    
    
    fig = plt.figure(num=None, figsize=(10, 4), dpi=80)
    ax1 = fig.add_subplot(1,2,1)
    ax2 = fig.add_subplot(1,2,2)
    for n in nbeams:
        flux = []
        flux_1gev = []
        efficiency = []
        efficiency_1gev = []
        for angle in angles:
            path = '%dbeams_%g_36PW' % (n, angle)
            config = utils.get_config(os.path.join(path, "ParsedInput.txt"))
            dumpfile = os.path.join(path, 'dump.pkl')
            if os.path.exists(dumpfile):
                with open(dumpfile, 'rb') as f:
                    sp_ph_sum_ = pickle.load(f)
                    sp_el_sum_ = pickle.load(f)
                    sp_pos_sum_ = pickle.load(f)
                    ang_ph_sum_ = pickle.load(f)
                    ang_el_sum_ = pickle.load(f)
                    ang_pos_sum_ = pickle.load(f)
                    ph_en = pickle.load(f)
                    ph_en_1gev = pickle.load(f)
                    el_en = pickle.load(f)
                    el_en_1gev = pickle.load(f)
                    pos_en = pickle.load(f)
                    pos_en_1gev = pickle.load(f)
                    ph_flux = pickle.load(f)
                    el_flux = pickle.load(f)
                    pos_flux = pickle.load(f)
                    ph_flux_1gev = pickle.load(f)
                    el_flux_1gev = pickle.load(f)
                    pos_flux_1gev = pickle.load(f)
                    ang_sp_sph = pickle.load(f)
                    el_ang_sp_sph = pickle.load(f)
                    pos_ang_sp_sph = pickle.load(f)
            flux.append(max(ph_flux))
            flux_1gev.append(max(ph_flux_1gev))
            en = max(ph_en)*1e-7
            print(en, pulse_energy, en/pulse_energy)
            en_1gev = max(ph_en_1gev)*1e-7
            efficiency.append(en/pulse_energy)
            efficiency_1gev.append(en_1gev/pulse_energy)
            
        #ax2.set_yscale('log')
        ax2.plot(fnum, flux, label='%d beams' % n)
        ax2.plot(fnum, flux_1gev, label='%d beams (> 1GeV)' % n)
        ax1.plot(fnum, efficiency)
        ax1.plot(fnum, efficiency_1gev)
        print(n, efficiency_1gev)
        print(n, efficiency)
    fig.legend(frameon=False, loc='upper center', ncol=3, fontsize=fontsize-2, bbox_to_anchor = (0, 0., 1, 1))
    ax1.set_ylabel('Efficiency')
    ax1.set_xlabel('F#')
    ax2.set_ylabel('Flux, ph s$^{-1}$')
    ax2.set_xlabel('F#')
    #ax1.set_xlim([0, 4])
    ax2.set_yscale('log')
    ax1.set_yscale('log')
    #ax1.set_ylim([1e-11, 1e-5])
        #ax1.set_yticks([0, 0.5, 1])
        #ax2.set_ylabel('$\\frac{1}{sin\\theta} \\frac{\partial W}{\partial \\theta}$, a.u.')
    ax1.text(ax1.get_xlim()[0] + (ax1.get_xlim()[0]-ax1.get_xlim()[1])/4, ax1.get_ylim()[1] * 0.8, '(a)')
    ax2.text(ax2.get_xlim()[0] + (ax2.get_xlim()[0]-ax2.get_xlim()[1])/4, ax2.get_ylim()[1] * 0.8, '(b)')
    
        #ax1.set_xticks([0, math.pi/2, math.pi, 3*math.pi/2, 2*math.pi])
        #ax1.set_xticklabels(["0", "$\pi/2$", "$\pi$", "$3\pi/2$", "$2\pi$"])
        #ax2.set_xticks([0, math.pi/2, math.pi])
        #ax2.set_xticklabels(["0", "$\pi/2$", "$\pi$"])
        #ax2.set_xlim([0, math.pi])
        #ax2.set_xlabel('$\\theta$')
        #ax2.legend(loc='upper left', frameon=False, ncol=2, fontsize=fontsize-2)
    plt.tight_layout()
    plt.subplots_adjust(top=0.8)
    figname = os.path.join(picdir, "flux_fine.eps")
    print(figname)
    plt.savefig(figname, dpi=1200, format='eps')
    plt.show()


if __name__ == '__main__':
    main()
