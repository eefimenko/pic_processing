#!/usr/bin/python
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import sys
import math
import os, os.path
import utils
from tqdm import *

def main():

    config = utils.get_config("ParsedInput.txt")

    dt = float(config['TimeStep'])
    ev = float(config['eV'])
    omega = float(config['Omega'])
    duration = float(config.get('Duration', 0)) #s
    delay = float(config['delay']) #s
    power = float(config['PeakPower'])*1e-7 #W
    Emax = float(config['QEDstatistics.Emax'])
    Emin = float(config['QEDstatistics.Emin'])
    Th_max = math.pi
    Th_min = 0.
    N_E = int(config['QEDstatistics.OutputN_E'])
    N_Phi = int(config['QEDstatistics.OutputN_phi'])
    N_Th = int(config['QEDstatistics.OutputN_theta'])
    de = (Emax - Emin)/N_E/1.6e-12/1e9
    dth = (Th_max - Th_min)/N_Th # erg -> eV -> GeV
    ax_e = utils.create_axis(N_E,de)
    ax_th = utils.create_axis(N_Th,dth)
   
    outStep = int(config['QEDstatistics.OutputIterStep'])
    tp = duration*1e15
    tdelay = delay*1e15

    path='.'
    elpath='/el/'
    elmaxpath='/el_max/'
    posmaxpath='/pos_max/'
    phmaxpath='/ph_max/'
    elsmaxpath='/elSmax/'
    possmaxpath='/posSmax/'
    phsmaxpath='/phSmax/'
    elnmaxpath='/elNmax/'
    posnmaxpath='/posNmax/'
    phnmaxpath='/phNmax/'
    pospath = '/pos/'
    phpath = '/ph/'
    phangle = '/ph_angle/'
    pht = '/phT/'
    picspath='pics/' 

    nmax = utils.num_files(path + utils.enspsphpath)
    
    print 'Found ' + str(nmax) + ' files'

    el_en = np.zeros(nmax)
    ph_en = np.zeros(nmax)
    pos_en = np.zeros(nmax)
    el_en_1gev = np.zeros(nmax)
    ph_en_1gev = np.zeros(nmax)
    pos_en_1gev = np.zeros(nmax)
    ez_a = np.zeros(nmax)
    delay = float(config['delay'])
    
    T = 2*math.pi/omega*1e15
    nit = int(config['QEDstatistics.OutputIterStep'])
    dt1 = float(config['TimeStep'])*nit
    ax_ = utils.create_axis(nmax, dt1) 

    verbose = 0
        
    read, sp_ph_sum_ = utils.bo_file_load(path + utils.ensppath,0,fmt='%.4f', verbose = verbose)
    read, sp_el_sum_ = utils.bo_file_load(path + utils.el_ensppath,0,fmt='%.4f', verbose = verbose)
    read, sp_pos_sum_ = utils.bo_file_load(path + utils.pos_ensppath,0,fmt='%.4f', verbose = verbose)
    
    ang_ph_sum_ = np.sum(utils.bo_file_load(path + utils.enangsphpath,0,nx=N_E,ny=N_Th,fmt='%.4f',verbose = verbose)[1], axis = 0)
    ang_el_sum_ = np.sum(utils.bo_file_load(path + utils.el_enangsphpath,0,nx=N_E,ny=N_Th,fmt='%.4f',verbose = verbose)[1], axis = 0)
    ang_pos_sum_ = np.sum(utils.bo_file_load(path + utils.pos_enangsphpath,0,nx=N_E,ny=N_Th,fmt='%.4f',verbose = verbose)[1], axis = 0)
   
    print len(ang_ph_sum_)
    
    i_1gev = int(1./de)
    linear = False
    nmin = 180
    nmax = 260
    sp_filename = 'qe_spectra.txt'
    
    if not linear:
        nmin = 300
        nmax = 340
        sp_filename = 'qe_spectra_nl.txt'
        
    for i in tqdm(range(nmin, nmax)):
        read, sp_ = utils.bo_file_load(path + utils.ensppath,i,fmt='%.4f',verbose = verbose)
        #n = utils.full_number(sp_,de,N_E)
        n = np.sum(sp_)
        sp_ph_sum_ += sp_/n
        ph_en[i] += ph_en[i-1] + np.sum(sp_)
        ph_en_1gev[i] += ph_en_1gev[i-1] + np.sum(sp_[i_1gev:])
        
        read, sp_ = utils.bo_file_load(path + utils.el_ensppath,i,fmt='%.4f',verbose = verbose)
        #n = utils.full_number(sp_,de,N_E)
        n = np.sum(sp_)
        sp_el_sum_ += sp_/n
        el_en[i] += el_en[i-1] + np.sum(sp_)
        el_en_1gev[i] += el_en_1gev[i-1] + np.sum(sp_[i_1gev:])
        
        read, sp_ = utils.bo_file_load(path + utils.pos_ensppath,i,fmt='%.4f',verbose = verbose)
        #n = utils.full_number(sp_,de,N_E)
        n = np.sum(sp_)
        sp_pos_sum_ += sp_/n
        pos_en[i] += pos_en[i-1] + np.sum(sp_)
        pos_en_1gev[i] += pos_en_1gev[i-1] + np.sum(sp_[i_1gev:])
        
        ang_ph_sum_ += np.sum(utils.bo_file_load(path + utils.enangsphpath,i,nx=N_E,ny=N_Th,fmt='%.4f', verbose = verbose)[1], axis = 0)
        ang_el_sum_ += np.sum(utils.bo_file_load(path + utils.el_enangsphpath,i,nx=N_E,ny=N_Th,fmt='%.4f', verbose = verbose)[1], axis = 0)
        ang_pos_sum_ += np.sum(utils.bo_file_load(path + utils.pos_enangsphpath,i,nx=N_E,ny=N_Th,fmt='%.4f', verbose = verbose)[1], axis = 0)
    sp_ph_sum_ /= (nmax - nmin)
    sp_el_sum_ /= (nmax - nmin)
    sp_ph_sum_ = utils.number_spectra_to_energy(sp_ph_sum_,de,N_E)
    sp_el_sum_ = utils.number_spectra_to_energy(sp_el_sum_,de,N_E)
    sp_pos_sum_ = utils.number_spectra_to_energy(sp_pos_sum_,de,N_E)

    en = max(ph_en)*1e-7
    pulse_energy = 1.05*duration*power
    print en, pulse_energy, en/pulse_energy
    fig, ax1 = plt.subplots()
    pph, = ax1.plot(ax_,ph_en, 'r')
    pel, = ax1.plot(ax_,el_en, 'g')
    ppos, = ax1.plot(ax_,pos_en, 'b')
    pph_1gev, = ax1.plot(ax_,ph_en_1gev, 'r:')
    pel_1gev, = ax1.plot(ax_,el_en_1gev, 'g:')
    ppos_1gev, = ax1.plot(ax_,pos_en_1gev, 'b:')
    
    plt.legend([pph, pel, ppos], ['Ph. energy','El. energy','Pos. energy'], loc=2)
    ax1.set_xlabel('Time, periods')
    ax1.set_ylabel('Full photon energy, erg')
    plt.figtext(0.2,0.93,str('Pulse energy=%.1f J; Photon energy=%.1lf J' % (pulse_energy, en)) , fontsize = 10)
    plt.figtext(0.6,0.5,str('Efficiency=%.1lf%%' % (en/pulse_energy*100)) , fontsize = 16)
    picname = picspath + "energy.png"
    plt.savefig(picname)
    plt.close()
    
    fig, ax1 = plt.subplots()
    ax1.set_yscale('log')
    n = utils.full_number(sp_ph_sum_,de,N_E)
    pph, = ax1.plot(ax_e,sp_ph_sum_/n, 'r')
    n = utils.full_number(sp_el_sum_,de,N_E)
    pel, = ax1.plot(ax_e,sp_el_sum_/n, 'g')
    n = utils.full_number(sp_pos_sum_,de,N_E)
    ppos, = ax1.plot(ax_e,sp_pos_sum_/n, 'b')
    picname = picspath + "spectra.png"
    plt.savefig(picname)
    plt.close()
    f = open(sp_filename, 'w')
    for i in range(len(ax_e)):
        f.write("%lf %le %le %le\n"%(ax_e[i], sp_ph_sum_[i], sp_el_sum_[i], sp_pos_sum_[i]))
    f.close()
    
    fig, ax1 = plt.subplots()
    ax1.set_yscale('log')
    n = np.sum(ang_ph_sum_)
    if n < 1e-30:
        n = 1.
    pph, = ax1.plot(ax_th,ang_ph_sum_/n, 'r')
    n = np.sum(ang_el_sum_)
    if n < 1e-30:
        n = 1.
    pel, = ax1.plot(ax_th,ang_el_sum_/n, 'b')
    n = np.sum(ang_pos_sum_)
    if n < 1e-30:
        n = 1.
    print n
    pel, = ax1.plot(ax_th,(ang_pos_sum_+1e-12)/n, 'g')
    picname = picspath + "dn.png"
    plt.savefig(picname)
    plt.close()
    f = open('qe_dn.txt', 'w')
    for i in range(len(ax_th)):
        f.write("%lf %le %le %le\n"%(ax_th[i], ang_ph_sum_[i], ang_el_sum_[i], ang_pos_sum_[i]))
    f.close()
 
if __name__ == '__main__':
    main()
