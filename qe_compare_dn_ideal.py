#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib as mp
mp.use("Agg")
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np

def read_spectra(filename):
    f = open(filename, 'r')
    ax_ = []
    ph_ = []
    el_ = []
    pos_ = []
    for line in f:
        tmp = line.split()
        ax_.append(float(tmp[0]))
        ph_.append(float(tmp[1]))
        el_.append(float(tmp[2]))
        pos_.append(float(tmp[3]))
    f.close()
    return ax_, ph_, el_, pos_

def main():
    savepath = './pics'
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
    if (len(sys.argv)) == 2:
        savepath = sys.argv[1]
    fontsize = 14
    mp.rcParams.update({'font.size': fontsize})
    el = []
    ph = []
    pos = []
    ne = []
    types = ['edipole']
    dirs = ['nc_0.001', 'nc_0.01', 'nc_0.1', 'nc_1', 'nc_10','nc_30', 'nc_50', 'nc_100', 'nc_1000']
    colors = ['k', 'r', 'g', 'b', 'm', 'gray', 'c', 'lime', 'orange']
    updirs = ['1mkm','2mkm','3mkm']
    dashes = [[2,2], [6,1]]
    window = 9
    order = 3
    for type_ in types:
        print type_
        for ud in updirs:
            print ud
            fig = plt.figure(figsize = (6,10))
            ax3 = fig.add_subplot(3,1,3)
            ax2 = fig.add_subplot(3,1,2)
            ax1 = fig.add_subplot(3,1,1)
#            ax1.set_yscale('log')
#            ax2.set_yscale('log')
#            ax3.set_yscale('log')
#            ax1.set_ylim([1e-8, 3e-2])
#            ax2.set_ylim([1e-8, 3e-2])
#            ax3.set_ylim([1e-8, 3e-2])
#            ax1.set_xlim([0,4])
#            ax2.set_xlim([0,4])
#            ax3.set_xlim([0,4])
            xtext = -0.2
            ytext = 1
            ax1.set_xlabel(u'$\\theta$')
            ax2.set_xlabel(u'$\\theta$')
            ax3.set_xlabel(u'$\\theta$')
            ax1.set_ylabel(u'$\partial W/\partial\\theta$')
            ax2.set_ylabel(u'$\partial W/\partial\\theta$')
            ax3.set_ylabel(u'$\partial W/\partial\\theta$')
            ax1.set_title(u'Фотоны')
            ax2.set_title(u'Электроны')
            ax3.set_title(u'Позитроны')
            for i_,d in enumerate(dirs):
                print d
                ax_, ph_, el_, pos_ = read_spectra(type_ + '/' + ud + '/' + d + '/qe_dn.txt')
                config = utils.get_config(type_ + '/' + ud + '/' + d + '/ParsedInput.txt')
                n_cr = float(config['n_cr'])
                ne_ = float(config['Ne'])
                ne_ = ne_/n_cr
                if ne_ >= 1.:
                    ne_ = int(ne_)
                    
                Emax = float(config['QEDstatistics.Emax'])
                Emin = float(config['QEDstatistics.Emin'])
                N_E = int(config['QEDstatistics.OutputN_E'])
                de = (Emax - Emin)/N_E/1.6e-12/1e9 # erg -> eV -> GeV
                n = sum(ph_)
                ax1.plot(ax_, np.array(ph_)/n, colors[i_], label = ne_)
                n = sum(el_)
                ax2.plot(ax_, np.array(el_)/n, colors[i_], label = ne_)
                n = sum(pos_)
                ax3.plot(ax_, np.array(pos_)/n, colors[i_], label = ne_)
                if type_ == 'edipole':
                    ax3.text(xtext,ytext,'$(f)$',transform = ax3.transAxes)
                    ax2.text(xtext,ytext,'$(e)$',transform = ax2.transAxes)
                    ax1.text(xtext,ytext,'$(d)$',transform = ax1.transAxes)
                else:
                    ax3.text(xtext,ytext,'$(f)$',transform = ax3.transAxes)
                    ax2.text(xtext,ytext,'$(e)$',transform = ax2.transAxes)
                    ax1.text(xtext,ytext,'$(d)$',transform = ax1.transAxes)
                plt.legend(loc = 'upper right', frameon = False, ncol = 3, labelspacing=0.05)
            picname = savepath +'/dn_' + ud + '_' + type_ + '_ideal.png'
            print picname
            plt.tight_layout()
            plt.savefig(picname)
            plt.close()


if __name__ == '__main__':
    main()
