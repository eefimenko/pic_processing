#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import sys
import math
import utils
import os
import numpy as np
import matplotlib as mp
from matplotlib.font_manager import FontProperties

def main():
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
    path = './'
    picspath = '/home/evgeny/Dropbox/tmp_pics/'
    fontsize = 18
    nperiods = 10
    steps_per_period = 8
    nbeams = ['0beams', '12beams', '10beams', '8beams', '6beams', '5beams', '4beams', '3beams', '2beams']
    wd = os.getcwd()
    axtype = 'x'
    if 'edipole' in wd:
        wtype = 'E'
        bpowers = [8, 10, 15, 15, 15, 20, 30, 30, 30]
    else:
        wtype = 'B'
        bpowers = [10, 15, 19, 23, 25, 25, 25, 25, 30]
        
    if len(sys.argv) == 1:
        layer = 'all'
#        titles = [u'Идеальная волна', u'12 пучков', u'10 пучков', u'8 пучков', u'2 пучков', u'6 пучков', u'5 пучков', u'4 пучка', u'3 пучка', u'2 пучка']
        titles = [u'Ideal', u'12 beams', u'10 beams', u'8 beams', u'2 beams', u'6 beams', u'5 beams', u'4 beams', u'3 beams', u'2 beams']
    elif len(sys.argv) == 2:
        layer = sys.argv[1]
        if layer != '1layer' and layer != '2layer':
            print 'Wrong parameter: ', layer, 'Correct values: 1layer, 2layer'
            exit(-1)
        elif layer == '1layer':
            nbeams = nbeams[4:]
            bpowers = bpowers[4:]
            titles = [u'6 beams', u'5 beams', u'4 beams', u'3 beams', u'2 beams']
        else:
            nbeams = nbeams[:4]
            bpowers = bpowers[:4]
            titles = [u'Ideal', u'12 beams', u'10 beams', u'8 beams', u'2 beams']
    else:
        print 'Wrong number of arguments'
        exit(-1)
        
    config = utils.get_config(path + '/' + nbeams[0] + '/Dipole' + wtype + '_' + str(bpowers[0]) + 'PW' +  "/ParsedInput.txt")
    wl = float(config['Wavelength'])
    xmax = float(config['X_Max'])/wl #mkm
    xmin = float(config['X_Min'])/wl #mkm
    ymax = float(config['Y_Max'])/wl #mkm
    ymin = float(config['Y_Min'])/wl #mkm
    zmax = float(config['Z_Max'])/wl #mkm
    zmin = float(config['Z_Min'])/wl #mkm
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    power = int(config['PeakPowerPW'])
    dx = float(config['Step_X'])/wl
    dy = float(config['Step_Y'])/wl
    dz = float(config['Step_Z'])/wl
    mult = 1/(2.*dx*dy*dz*wl*wl*wl)
    step = float(config['TimeStep'])*1e15*float(config['BOIterationPass'])
    omega = float(config['Omega'])
    T = 2 * math.pi/omega
    nt = int(T*1e15/step)
    print nt
    iteration = 159
    
    if axtype == 'z':
        figures = [utils.ezpath, utils.bzpath, utils.nezpath, utils.nphzpath]
    else:
        figures = [utils.eypath, utils.bypath, utils.neypath, utils.nphypath]
    cmaps = ['Reds', 'Blues', 'Greens', 'RdPu']
#    titles = ['Electric field', 'Magnetic field', 'Electrons', 'Photons']
    log = [False, False, False, False]
    mult = [1e-11,1e-11, mult, mult]
    normalize = [False, False, True, True]
   

    spx_ = len(nbeams)
    spy_ = len(figures)
    labels = [u'a', u'b', u'c', u'd', u'e', u'f', u'g', u'h', u'i', u'j', u'k', u'l', u'm', u'n', u'o', u'p', u'q', u'r', u's', u't']
    
    verbose = 1
#    plt.rcParams.update({'mathtext.default':  'regular' })
    fig = plt.figure(num=None, figsize=(4.*spy_, 3.4*spx_))
    for k in range(spx_):
#        i = iterations[k]
        pathdir = path + '/' + nbeams[k] + '/Dipole' + wtype + '_' + str(bpowers[k]) + 'PW'
        for j in range(spy_):
           
            mp.rcParams.update({'font.size': fontsize})
            yticks = [-1., -0.5, 0, 0.5, 1]
            xticks = [-1., -0.5, 0, 0.5, 1]
            axlim = [-1.5, 1.5]
            
            if k == spx_ -1:
                xlabel = '$x/\lambda$'
                xticklabels = ['$-1$', '$-0.5$','$0$','$0.5$', '$1$']
            else:
                xlabel = ''
                xticklabels = []
                
            if j == 0:
                if axtype == 'z':
                    ylabel = '$y/\lambda$'
                else:
                    ylabel = '$z/\lambda$'
                yticklabels = ['$-1$', '$-0.5$','$0$','$0.5$', '$1$']
            else:
                ylabel = ''
                yticklabels = []
                
            if figures[j] == utils.nezpath or figures[j] == utils.nphzpath or figures[j] == utils.neypath or figures[j] == utils.nphypath:
                print figures[j]
                ncbarticks = None
                read, field = utils.bo_file_load(pathdir + '/' + figures[j], iteration, nx, ny, ftype = 'bin', transpose = 1, verbose = 1)
                mmax = np.amax(field)
                field /= mmax
                for itp in range(1,nperiods):
                    read, field_ = utils.bo_file_load(pathdir + '/' + figures[j], iteration - itp*steps_per_period, nx, ny, ftype = 'bin', transpose = 1)
                    mmax = np.amax(field_)
                    field_ /= mmax
                    field += field_
            else:
                ncbarticks = 5
                field = None
            ncbarticks = 5 
            if j == 0:
                
                    # fx = utils.bo_file_load(dirs[k]+utils.ezpath,
                    #                         i+n0[k],nx[k],ny[k],verbose=1)
                    # fy = utils.bo_file_load(dirs[k]+utils.bzpath,
                    #                         i+n0[k],nx[k],ny[k],verbose=1)
                    # sf = np.square(fx) + np.square(fy)
                                
                ax = utils.subplot(fig, iteration, pathdir + '/' + figures[j],
                                   shape = (nx,ny), position = (spx_,spy_,j+spy_*k+1),
                                   extent = [xmin, xmax, ymin, ymax],
                                   cmap = cmaps[j], title = '', #titles[j] % (power),
                                   colorbar = True, logarithmic=log[j], verbose=verbose, normalize = normalize[j], ratio = 1e-3, field = field,
                                   xlim = axlim, ylim = axlim, xticks = xticks, yticks = yticks,yticklabels = yticklabels, xticklabels = xticklabels,
                                   xlabel = xlabel, ylabel = ylabel, fontsize=fontsize, mult = mult[j], transpose = 1, ncbarticks = ncbarticks, ftype = 'bin')
            else:
                ax = utils.subplot(fig, iteration, pathdir + '/' + figures[j],
                                   shape = (nx,ny), position = (spx_,spy_,j+spy_*k+1),
                                   extent = [xmin, xmax, ymin, ymax],
                                   cmap = cmaps[j], title = '', titletype = 'simple',
                                   colorbar = True, logarithmic=log[j], verbose=verbose, normalize = normalize[j], ratio = 1e-3, field = field,
                                   xlim = axlim, ylim = axlim, xticks = xticks, yticks = yticks,yticklabels = yticklabels, xticklabels = xticklabels,
                                   xlabel = xlabel, ylabel = ylabel, #vmax = 0.5, vmin=0.5,
                                   maximum = 'local', fontsize = fontsize, mult = mult[j], transpose = 1, ncbarticks = ncbarticks, ftype = 'bin')
                
                                  
            if j == 0:
                ax.set_title(titles[k] +': ' + str(bpowers[k]) + u' ПВт')
            if j == 0:
                ax.text(-2.0*1.5, 0.9*1.5, '$(%s)$'%(labels[k*spy_ + j]), fontsize = 24)
            else:
                ax.text(-1.45*1.5, 0.9*1.5, '$(%s)$'%(labels[k*spy_ + j]), fontsize = 24)
    picname = picspath + '/' + "lphys_beams_distribution" + wtype + '_' + axtype + '_' + layer + ".png"
    print picname
    plt.tight_layout()
    plt.savefig(picname, bbox_inches='tight', dpi=128)
    plt.close()

    
    # picname = picspath + '/' + "fig4_full.png"
    # # plt.tight_layout()
    # plt.savefig(picname, bbox_inches='tight', dpi=128)
    # plt.close()
    # plt.show()
    
if __name__ == '__main__':
    main()
