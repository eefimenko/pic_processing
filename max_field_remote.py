#!/usr/bin/python
import math
import utils
import sys

def read_field(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)/norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])

def main():
    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
    ex_xpath = 'data/Ex_x'
    ex_ypath = 'data/Ex_y'
    ex_zpath = 'data/Ex_z'
    ey_xpath = 'data/Ey_x'
    ey_ypath = 'data/Ey_y'
    ey_zpath = 'data/Ey_z'
    ez_xpath = 'data/Ez_x'
    ez_ypath = 'data/Ez_y'
    ez_zpath = 'data/Ez_z'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    bx_xpath = 'data/Bx_x'
    bx_ypath = 'data/Bx_y'
    bx_zpath = 'data/Bx_z'
    by_xpath = 'data/By_x'
    by_ypath = 'data/By_y'
    by_zpath = 'data/By_z'
    bz_xpath = 'data/Bz_x'
    bz_ypath = 'data/Bz_y'
    bz_zpath = 'data/Bz_z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nixpath = 'data/Proton2Dx'
    niypath = 'data/Proton2Dy'
    nizpath = 'data/Proton2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nphxpath = 'data/Photon2Dx'
    nphypath = 'data/Photon2Dy'
    nphzpath = 'data/Photon2Dz'
    picspath = 'pics'
    config = utils.get_config("ParsedInput.txt")
    wl = float(config['Wavelength'])
    
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])

    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
    print num, T
    step = x0*y0*1e15/T

    Xmax = float(config['X_Max'])/wl #mkm to wavelength
    Xmin = float(config['X_Min'])/wl #mkm to wavelength
    Ymax = float(config['Y_Max'])/wl #mkm to wavelength
    Ymin = float(config['Y_Min'])/wl #mkm to wavelength

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    delta = 1

    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(expath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(expath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'

    dv = 2*dx*dy*dz
#    dmy = int(0.35*wl/dy)
    dmy = 4
    print int(dmy)
    f = open('max_field_ne.dat', 'w')
    for i in range(nmin, nmax, delta):
        if i%10 == 0:
            print i
#        name = expath + '/' + "%06d.txt" % (i,)
#        fielde2x = read_field(name,nx,ny)
#        m = max2d(fielde2x)

#        name = ex_xpath + '/' + "%06d.txt" % (i,)
#        fieldex_x = read_field(name,nx,ny)
        
#        name = ey_xpath + '/' + "%06d.txt" % (i,)
#        fieldey_x = read_field(name,nx,ny)
        
        name = ez_xpath + '/' + "%06d.txt" % (i,)
        fieldez_x = read_field(name,nx,ny)
        
        ez = fieldez_x[nx/2][ny/2]
#        ez_t.append(ez)
        name = nexpath + '/' + "%06d.txt" % (i,)
        ne_x = read_field(name,nx,ny)
        ne = ne_x[nx/2][ny/2]/dv
#        ne_t.append(ne)
#        name = bxpath + '/' + "%06d.txt" % (i,)
#        fielde2x = read_field(name,nx,ny)
#        m = max2d(fielde2x)
        
        name = bx_xpath + '/' + "%06d.txt" % (i,)
        fieldbx_x = read_field(name,nx,ny)
        bx = fieldbx_x[nx/2-dmy][ny/2]
#       bx_t.append(bx)
        name = npxpath + '/' + "%06d.txt" % (i,)
        np_x = read_field(name,nx,ny)
        np = np_x[nx/2][ny/2]/dv
#        np_t.append(np)
#        name = by_xpath + '/' + "%06d.txt" % (i,)
#        fieldby_x = read_field(name,nx,ny)
        
#        name = bz_xpath + '/' + "%06d.txt" % (i,)
#        fieldbz_x = read_field(name,nx,ny)
        f.write("%lf %le %le %le %le\n" % (i*step, ez, bx, ne, np))
    
#        plt.show()
    f.close()

if __name__ == '__main__':
    main()
