#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import utils
import sys
from pylab import *

def read_field(file,nx,ny,mult=1.):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index]*mult)
        field.append(row)
    return field

def average_energy(up, down, energy):
    ans = [[0 for x in range(len(up[0]))] for x in range(len(up))]
    for i in range(len(up)):
        for j in range(len(up[0])):
            summ = up[i][j]+down[i][j]
            if summ > 0:
                ans[i][j] = energy[i][j]/summ
            else:
                ans[i][j] = 0
    return ans
    
def main():
    picspath = 'pics'
    uppath = '/statdata/ph/NPlaneUp/'
    downpath = 'statdata/ph/NPlaneDown/'
    enpath = 'statdata/ph/EnSpPlane/'
    
    lx = 0.8e-4
    ly = 0.8e-4
    nx = 52
    ny = 52
    ds = lx/nx*ly/ny

    xmin = -0.5
    xmax = 0.5
    ymin = -0.5
    ymax = 0.5
    path = sys.argv[1]
    config = utils.get_config(path + "/ParsedInput.txt")
    ev = float(config['eV'])
    n = int(sys.argv[2])
    print path + uppath + '%.4f.txt' % (n,)
    up = read_field(path + uppath + '%.4f.txt' % (n,), 52, 52)
    down = read_field(path + downpath + '%.4f.txt' % (n,), 52, 52)
    energy = read_field(path + enpath + '%.4f.txt' % (n,), 52, 52, 1.e-9/ev)
    av_energy = average_energy(up, down, energy) 
    fig = plt.figure(num=None)
    mp.rcParams.update({'font.size': 8})
    ax = fig.add_subplot(1,3,1)
    surf = ax.imshow(up, extent=[xmin, xmax, ymin, ymax])
    plt.colorbar(surf, orientation  = 'vertical')
    ax = fig.add_subplot(1,3,2)
    surf = ax.imshow(down, extent=[xmin, xmax, ymin, ymax])
    plt.colorbar(surf, orientation  = 'vertical')
    ax = fig.add_subplot(1,3,3)
    surf = ax.imshow(av_energy, extent=[xmin, xmax, ymin, ymax])
    plt.colorbar(surf, orientation  = 'vertical')
    
    plt.show()
if __name__ == '__main__':
    main()
