#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import sys
import math
import utils
import numpy as np

def read_diag(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def read_conc(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    return tmp[0]

def profile(array, de):
    p1 = []
    p10 = []
    n = len(array)
    m = len(array[0])
    av_num = int(m/8.88)
    av_num1 = int(m/44.4)
    for i in range(n):
        s = 0
        s1 = 0
        for j in range(av_num):
            s += array[i][j]
        s /= av_num
        for j in range(av_num1):
            s1 += array[i][j]
        s1 /= av_num1
        p1.append(s)
        p10.append(s1)
    s = sum(p1)
    s1 = sum(p10)
    av = 0
    av1 = 0
    imax = 0
    max_g = 0. 
    for i in range(len(p1)):
        if p1[i] > max_g:
            max_g = p1[i]
            imax = i
        p1[i] /= s
        p10[i] /= s1
        av += (i+0.5)*de*p1[i]
        av1 += (i+0.5)*de*p10[i]
    gmax = (imax+0.5)*de
    print av, av1
    factor = 10
    n1 = n/factor
    p11 = [0]*n1
    p101 = [0]*n1
    for i in range(n1):
        for j in range(factor):
            p11[i] += p1[i*factor + j]
            p101[i] += p10[i*factor + j]
           
    return p11, p101, av, av1, gmax

def create_axis(n,step,x0=0):
    axis = []
    for i in range(n):
        axis.append(i*step + x0)
    return axis

def summarray(array):
    n = len(array)
    m = len(array[0])
    s = 0
    for i in range(n):
        for j in range(m):
            if j > m/4 and j < m*3/4: 
                s += array[i][j]
    return s

def average(p, de):
    s = 0
    n = 0
    for i in range(len(p)):
        s += (i+0.5)*de*p[i]
        n += p[i]
    s /= n
    return s, n

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def width(array, de):
    n = len(array)
    m = 0.5 * max(array)
    for i in range(5, n-1):
        if array[i] > m and array[i+1]<m:
            return i*de
        
    return 0

def read_field2d(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def calc_ez(amp,delay,alpha,k,r,t):
    c = 29979245800.0
    env = 0.5*(1. - math.tanh(-alpha*(t + r/c - delay)))
    return amp * env * abs(math.cos(k *(c*t + r - delay)))

def max2d(array):
    m = 0
    for i in range(len(array)):
        for j in range(len(array[0])):
            if array[i][j] > m:
                m = array[i][j]
    return m, max([max(row) for row in array])

def main():

    config = utils.get_config("ParsedInput.txt")
    denspath = 'data/Electron2Dx'
    ezpath = 'data/E2z'
    picspath = 'pics'
   
    delta = 1
    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(ezpath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(ezpath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax) + ' files'
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    wl = float(config['Wavelength'])
#    nx1 = int(config['ElGammaR.SetMatrixSize_0'])
#    ny1 = int(config['ElGammaR.SetMatrixSize_1'])
#    zmin = float(config['ElGammaR.SetBounds_0'])*1e4
#    zmax = float(config['ElGammaR.SetBounds_1'])*1e4
#    Emin = float(config['ElGammaR.SetBounds_2'])
#    Emax = float(config['ElGammaR.SetBounds_3'])
        
#    de = (Emax - Emin)/ny1
#    factor = 10
#    gaxis = create_axis(ny1/factor,de*factor,Emin)
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])

    T = 2 * math.pi/omega*1e15
    
    step = x0*y0*1e15/T
    nT = int(1./step)
    n0 = 0
    nf = 0
    nf1 = 0
    nf2 = 0
    i0 = 0
    i_f = nmax
    i_f1 = nmax
    i_f2 = nmax
    dmy = int(0.35*wl/dy)
    coeff = 10./(3.3e11*3.3e11)*0.97399*(wl/0.8e-4)*(wl/0.8e-4)
    ppw = float(config['PeakPowerPW'])
    ppp = float(config['PeakPower'])
    amp = 1.
    delay = float(config['delay'])
    alpha = float(config['alpha'])
    k = float(config['k'])
    print k
    
    print dmy
    print 'nt' + str(nT)
    const_a_p=7.81441e-9
    p1 = []
    m1 = []
    av_t = []
    av10_t = []
    gmax_t = []
    ez_t = []
    ez1_t = []
    av_th = 0
    av10_th = 0
    nesize = float(config['ne.SetBounds_1'])
    r = int(config['PML.Size_X'])*dx
    nenum = int(config['ne.SetMatrixSize_0'])
    step1 = nesize/nenum
    v = 2. * math.pi * step1 * step1 * wl
    a0 = const_a_p*math.sqrt(ppp) 
    maxe = 0
    g_max = 0.
    g_min = 1.e9
    eprev = 0
    conc = 0
    conc1 = 0
    conc2 = 0 
    dv = 2*dx*dy*dz
    max_dens10 = 0
    max_dens20 = 0
    max_dens30 = 0
    max_dens40 = 0
    max_dens50 = 0
#    ff = open('fg.dat', 'w')
    a0 = 3.e11*math.sqrt(ppw/10.)
    ratio = 0.05
    max_dens = 0
    for i in range(nmin,nmax,delta):
        pname = "%06d.txt" % (i,)
        print pname
#        pfield = read_diag(pname,nx1,ny1)
        t = x0*y0*i
        ezname = ezpath + '/' + "%06d.txt" % (i,)
#        bzname = bzpath + '/' + "%06d.txt" % (i,)
        ez = read_field2d(ezname,nx,ny)
#        bz = read_field2d(bzname,nx,ny)
        ezv = ez[nx/2][ny/2]
#        bzv = bz[nx/2][ny/2-dmy]*1.53
#        tmp = (ezv*ezv + bzv*bzv)*coeff
        ezv1 = calc_ez(a0,delay,alpha,k,0,t)
#        prof, prof10, av, av10, gmax = profile(pfield,de)
#        p1.append(prof)
#        av_t.append(av)
#        av10_t.append(av10)
#        gmax_t.append(gmax)
        ez_t.append(ezv)
        ez1_t.append(ezv1)
        densname = denspath + '/' + "%06d.txt" % (i,)
        dens = read_field2d(densname,nx,ny, 1./dv)
        m, dens = max2d(dens)
        if dens > 1e27:
            break
        if dens > max_dens:
            max_dens = dens
        if ezv1/a0 > 0.1:
            print ezv/ezv1
        if ezv < 0.9*ezv1 and ezv1/a0 > ratio and max_dens10 == 0:
            densname = denspath + '/' + "%06d.txt" % (i,)
            dens = read_field2d(densname,nx,ny, 1./dv)
            m, max_dens10 = max2d(dens)
        if ezv < 0.8*ezv1 and ezv1/a0 > ratio and max_dens20 == 0:
            densname = denspath + '/' + "%06d.txt" % (i,)
            dens = read_field2d(densname,nx,ny, 1./dv)
            m, max_dens20 = max2d(dens)
        if ezv < 0.7*ezv1 and ezv1/a0 > ratio and max_dens30 == 0:
            densname = denspath + '/' + "%06d.txt" % (i,)
            dens = read_field2d(densname,nx,ny, 1./dv)
            m, max_dens30 = max2d(dens)
        if ezv < 0.6*ezv1 and ezv1/a0 > ratio and max_dens40 == 0:
            densname = denspath + '/' + "%06d.txt" % (i,)
            dens = read_field2d(densname,nx,ny, 1./dv)
            m, max_dens40 = max2d(dens)
        if ezv < 0.5*ezv1 and ezv1/a0 > ratio and max_dens50 == 0:
            densname = denspath + '/' + "%06d.txt" % (i,)
            dens = read_field2d(densname,nx,ny, 1./dv)
            m, max_dens50 = max2d(dens)
       
#            break
#        ff.write('%d %lf %lf\n'%(i, ezv, av10))

    print max_dens10, max_dens20, max_dens30, max_dens40, max_dens50, max_dens
    f = open('max_dens.txt', 'w')
    f.write('%le %le %le %le %le %le\n' % (max_dens10, max_dens20, max_dens30, max_dens40, max_dens50, max_dens))
    f.close()

#    axis = create_axis(nmax-nmin,step,nmin*step)

#    fig = plt.figure(num=None)
#    plt.rc('text', usetex=True)
#    ax = fig.add_subplot(1,1,1)
#    ax.plot(axis, ez_t)
#    ax1 = ax.twinx()
#    ax1.plot(axis, ez1_t, 'r')
#    plt.show()

if __name__ == '__main__':
    main()
