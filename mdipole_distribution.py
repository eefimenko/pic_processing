#!/usr/bin/python
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import utils
import sys
import os
from tqdm import *
from pylab import *

def process_distribution(path, picspath, iterations, filenames, nt, nx, ny, nT, extent, ne_axis, e_axis, power):
    read, ez = utils.bo_file_load(path + '/' + utils.ezpath,277,nx,ny)
    read, bz = utils.bo_file_load(path + '/' + utils.bzpath,270,nx,ny)
    ezp = ez[nx/2]
    bzp = bz[nx/2]
    ezp = -ezp/np.amax(ezp)*0.5
    bzp = -bzp/np.amax(bzp)*0.5*1.53
    for i_ in range(len(iterations)):
        i = iterations[i_]
        filename = filenames[i_]
        res_z = np.zeros((nx,ny))
        res_y = np.zeros((nx,ny))
        e_z = np.zeros((nx,ny))
        e_y = np.zeros((nx,ny))
        b_z = np.zeros((nx,ny))
        b_y = np.zeros((nx,ny))
        res_ne = np.zeros(nx)
        for j in range(nt):
            read, tmp_z = utils.bo_file_load(path + '/' + utils.nezpath,j*nT/2 + i,nx,ny, transpose = 1)
            read, tmp_y = utils.bo_file_load(path + '/' + utils.neypath,j*nT/2 + i,nx,ny, transpose = 1)
            read, tmp_e_z = utils.bo_file_load(path + '/' + utils.ezpath,j*nT/2 + i,nx,ny, transpose = 1)
            read, tmp_e_y = utils.bo_file_load(path + '/' + utils.eypath,j*nT/2 + i,nx,ny, transpose = 1)
            read, tmp_b_z = utils.bo_file_load(path + '/' + utils.bzpath,j*nT/2 + i,nx,ny, transpose = 1)
            read, tmp_b_y = utils.bo_file_load(path + '/' + utils.bypath,j*nT/2 + i,nx,ny, transpose = 1)
            read, tmp_ne = utils.bo_file_load(path + '/' + utils.nepath,j*nT/2 + i,nx,)
            for k in range(len(tmp_ne)):
                tmp_ne[k] /= (k+0.5)
            res_z += tmp_z/np.amax(tmp_z)
            res_y += tmp_y/np.amax(tmp_y)
            e_z += tmp_e_z
            e_y += tmp_e_y
            b_z += tmp_b_z
            b_y += tmp_b_y
            res_ne += tmp_ne
            
        fig = plt.figure(num=None, figsize = (15,9))
        mp.rcParams.update({'font.size': 16})
        ax = fig.add_subplot(2,3,1)
        ax.set_title('(a) Pair density x-z')
        ax.imshow(res_y, cmap = 'jet', origin = 'lower',
                  extent = extent, interpolation = 'bicubic')
        ax.set_xlim([-1,1])
        ax.set_ylim([-1,1])
        ax.set_xlabel('x, mkm')
        ax.set_ylabel('z, mkm')
        ax.plot(ne_axis, res_ne/np.amax(res_ne), 'r')
        ax.plot(-ne_axis, res_ne/np.amax(res_ne), 'r')

        ax = fig.add_subplot(2,3,2)
        ax.set_title('(b) Electric field x-z')
        ax.imshow(e_y/nt, cmap = 'jet', origin = 'lower',
                  extent = extent, interpolation = 'bicubic')
        ax.set_xlim([-1,1])
        ax.set_ylim([-1,1])
        ax.set_xlabel('x, mkm')
        ax.set_ylabel('z, mkm')

        ax = fig.add_subplot(2,3,3)
        ax.set_title('(c) Magnetic field x-z')
        ax.imshow(b_y/nt, cmap = 'jet', origin = 'lower',
                  extent = extent, interpolation = 'bicubic')
        ax.set_xlim([-1,1])
        ax.set_ylim([-1,1])
        ax.set_xlabel('x, mkm')
        ax.set_ylabel('z, mkm')
        
        ax1 = fig.add_subplot(2,3,4)
        ax1.set_title('(d) Pair density x-y')
        ax1.imshow(res_z, cmap = 'jet', origin = 'lower',
                   extent = extent, interpolation = 'bicubic')
        ax1.plot(ne_axis, res_ne/np.amax(res_ne), 'r')
        ax1.plot(-ne_axis, res_ne/np.amax(res_ne), 'r')
        ax1.plot(e_axis, ezp, 'g')
        ax1.plot(e_axis, bzp, 'gray')
        ax1.axhline(y=0, color = 'w')
        ax1.set_xlim([-1,1])
        ax1.set_ylim([-1,1])
        ax1.set_xlabel('x, mkm')
        ax1.set_ylabel('y, mkm')

        ax1 = fig.add_subplot(2,3,5)
        ax1.set_title('(e) Electric field x-y')
        ax1.imshow(e_z/nt, cmap = 'jet', origin = 'lower',
                   extent = extent, interpolation = 'bicubic')
        ax1.set_xlim([-1,1])
        ax1.set_ylim([-1,1])
        ax1.set_xlabel('x, mkm')
        ax1.set_ylabel('y, mkm')

        ax1 = fig.add_subplot(2,3,6)
        ax1.set_title('(f) Magnetic field x-y')
        ax1.imshow(b_z/nt, cmap = 'jet', origin = 'lower',
                   extent = extent, interpolation = 'bicubic')
        ax1.set_xlim([-1,1])
        ax1.set_ylim([-1,1])
        ax1.set_xlabel('x, mkm')
        ax1.set_ylabel('y, mkm')
        plt.tight_layout()
        plt.savefig(picspath + '/' + filename + '_%d.png'%power)
        plt.close()

        f = open(path + '/' + 'concentration_' + filename + '.txt', 'w')
        for i in range(len(res_ne)):
            f.write('%lf %lf\n'%(ne_axis[i], res_ne[i]))
        f.close()


def main():
   
    path = './'
    picspath = '/home/evgeny/Dropbox/tmp_pics'

    config = utils.get_config(path + "/ParsedInput.txt")
    ev = float(config['eV'])
    wl = float(config['Wavelength'])

    nit = int(config['BOIterationPass'])
    dt = float(config['TimeStep'])*nit
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    duration = float(config.get('Duration',15))
    Xmax = float(config['X_Max']) #mkm
    Xmin = float(config['X_Min']) #mkm
    Ymax = float(config['Y_Max']) #mkm
    Ymin = float(config['Y_Min']) #mkm
    Zmax = float(config['Z_Max']) #mkm
    Zmin = float(config['Z_Min']) #mkm

    ne_xmax = float(config['ne.SetBounds_1'])
    ne_nx = int(config['ne.SetMatrixSize_0'])
    ne_dx = ne_xmax/ne_nx*1e4
    ne_axis = utils.create_axis(ne_nx, ne_dx)
    
    
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)
    T = 2 * math.pi/omega
    step = float(config['TimeStep'])*float(config['BOIterationPass'])
    nT = int(T/step)
    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz
    dmy = int(0.35*wl/dy)
    dv = 2.*dx*dy*dz
    e_axis = utils.create_axis(nx, dx*1e4, Xmin*1e4)
    
    nx1 = int(config['MatrixSize_X'])
    ny1 = int(config['MatrixSize_Y'])
    T = 2 * math.pi/omega
    nnn = int(T/dt)

    ftype = config['BODataFormat']
    print 'Read electric field in the center vs time'
    ez_c_t = utils.ts(path, utils.ezpath, name='ec', ftype = ftype,
                    tstype='center', shape = (nx1,ny1), verbose=True)
    print 'Read max electric field vs time'
    ez_m_t = utils.ts(path, utils.eypath, name='em', ftype = ftype,
                    tstype='max', shape = (nx1,ny1))
    print 'Read electric field in max in dipole wave vs time'
    ez_md_t = utils.ts(path, utils.ezpath, name='em_d', ftype = ftype,
                      tstype='func', func = lambda x: x[nx1/2, ny1/2-dmy],
                      shape = (nx1,ny1), verbose=True)
    print 'Read magnetic field in the center vs time'
    bz_c_t = utils.ts(path, utils.bzpath, name='bc', ftype = ftype,
                    tstype='center', shape = (nx1,ny1), verbose=True)
    print 'Read max magnetic field vs time'
    bz_m_t = utils.ts(path, utils.bypath, name='bm', ftype = ftype,
                    tstype='max', shape = (nx1,ny1), verbose=True)
    print 'Read Ne in the center vs time'
    nez_c_t = utils.ts(path, utils.nezpath, name='nec', ftype = ftype,
                    tstype='center', shape = (nx1,ny1), verbose=True)
    print 'Read max Ne vs time'
    nez_m_t = utils.ts(path, utils.nezpath, name='nem', ftype = ftype,
                    tstype='max', shape = (nx1,ny1), verbose=True)
    xmin = 0
    xmax = -1
    axis_t = utils.create_axis(ez_c_t.shape[0], 1./nT)
    p = 0
    p_prev = 0
    i0 = 0
    i1 = 0
    threshold = 0.97
    nl_power = np.zeros(len(ez_m_t))
    
    for i in range(len(ez_m_t)):
        p_prev = p
        p = ((bz_m_t[i]/3e11)**2 + (ez_m_t[i]/0.653/3e11)**2)*10.
        
        if i0 == 0 and p > threshold * ppw and p_prev < threshold * ppw:
            i0 = i
        if i1 == 0 and p < threshold * ppw and p_prev > threshold * ppw:
            i1 = i
        nl_power[i] = ((bz_c_t[i]/3e11)**2 + (ez_md_t[i]/0.653/3e11)**2)*10.
    nt = (i1 - i0)/(nT/2)
    nt2 = nT/2
    print i0, i1, nt, nT/2
    extent = [Xmin*1e4, Xmax*1e4, Ymin*1e4, Ymax*1e4]
    
    
    iterations = [270, 277]
    filenames = ['linear_max_B', 'linear_max_E']
       
    process_distribution(path, picspath, iterations, filenames, nt, nx, ny, nT, extent, ne_axis, e_axis, ppw)
    if ppw > 15:        
        i2 = i1 + 120
    else:
        i2 = i1 + 200
    i3 = len(ez_m_t)
    nt = (i3 - i2)/(nT/2)
    tmp = (i2+nt2)/nt2
    i22 = tmp*nt2
    print i3, i2, nt, nT/2, (i2+nt2)/nt2
    if ppw != 35:
        iterations = [i22,i22-7]
        filenames = ['nonlinear_max_B', 'nonlinear_max_E']
       
        process_distribution(path, picspath, iterations, filenames, nt, nx, ny, nT, extent, ne_axis, e_axis, ppw)
    else:
        iterations = [360,367]
        nt = 10
        filenames = ['nonlinear1_max_B', 'nonlinear1_max_E']
       
        process_distribution(path, picspath, iterations, filenames, nt, nx, ny, nT, extent, ne_axis, e_axis, ppw)

        iterations = [600,607]
        nt = (i3 - 600)/(nT/2)
        filenames = ['nonlinear2_max_B', 'nonlinear2_max_E']
       
        process_distribution(path, picspath, iterations, filenames, nt, nx, ny, nT, extent, ne_axis, e_axis, ppw)
    
            
if __name__ == '__main__':
    main()
