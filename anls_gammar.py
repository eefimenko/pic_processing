#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import sys
import math
import utils
import numpy as np

def read_diag(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def read_conc(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    return tmp[0]

def profile(array, de):
    p1 = []
    p10 = []
    n = len(array)
    m = len(array[0])
    av_num = int(m/8.88)
    av_num1 = int(m/44.4)
    for i in range(n):
        s = 0
        s1 = 0
        for j in range(av_num):
            s += array[i][j]
        s /= av_num
        for j in range(av_num1):
            s1 += array[i][j]
        s1 /= av_num1
        p1.append(s)
        p10.append(s1)
    s = sum(p1)
    s1 = sum(p10)
    av = 0
    av1 = 0
    imax = 0
    max_g = 0. 
    for i in range(len(p1)):
        if p1[i] > max_g:
            max_g = p1[i]
            imax = i
        p1[i] /= s
        p10[i] /= s1
        av += (i+0.5)*de*p1[i]
        av1 += (i+0.5)*de*p10[i]
    gmax = (imax+0.5)*de
    print av, av1
    factor = 10
    n1 = n/factor
    p11 = [0]*n1
    p101 = [0]*n1
    for i in range(n1):
        for j in range(factor):
            p11[i] += p1[i*factor + j]
            p101[i] += p10[i*factor + j]
           
    return p11, p101, av, av1, gmax

def create_axis(n,step,x0=0):
    axis = []
    for i in range(n):
        axis.append(i*step + x0)
    return axis

def summarray(array):
    n = len(array)
    m = len(array[0])
    s = 0
    for i in range(n):
        for j in range(m):
            if j > m/4 and j < m*3/4: 
                s += array[i][j]
    return s

def average(p, de):
    s = 0
    n = 0
    for i in range(len(p)):
        s += (i+0.5)*de*p[i]
        n += p[i]
    s /= n
    return s, n

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def width(array, de):
    n = len(array)
    m = 0.5 * max(array)
    for i in range(5, n-1):
        if array[i] > m and array[i+1]<m:
            return i*de
        
    return 0

def read_field2d(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def main():

    config = utils.get_config("ParsedInput.txt")
    ppath = 'data/ElGammaR/'
    ezpath = 'data/E2z'
    bzpath = 'data/B2z'
    necpath = 'data/ne'
    picspath = 'pics'
   
    delta = 1
    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(ppath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(ppath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax) + ' files'
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])

    wl = float(config['Wavelength'])
    nx1 = int(config['ElGammaR.SetMatrixSize_0'])
    ny1 = int(config['ElGammaR.SetMatrixSize_1'])
    zmin = float(config['ElGammaR.SetBounds_0'])*1e4
    zmax = float(config['ElGammaR.SetBounds_1'])*1e4
    Emin = float(config['ElGammaR.SetBounds_2'])
    Emax = float(config['ElGammaR.SetBounds_3'])
        
    de = (Emax - Emin)/ny1
    factor = 10
    gaxis = create_axis(ny1/factor,de*factor,Emin)
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])

    T = 2 * math.pi/omega*1e15
    
    step = x0*y0*1e15/T
    nT = int(1./step)
    n0 = 0
    nf = 0
    nf1 = 0
    nf2 = 0
    i0 = 0
    i_f = nmax
    i_f1 = nmax
    i_f2 = nmax
    dmy = int(0.35*wl/dy)
    coeff = 10./(3.3e11*3.3e11)*0.97399*(wl/0.8e-4)*(wl/0.8e-4)
    peakpower = float(config['PeakPower'])*1e-7*1e-15
    ppp = float(config['PeakPower'])
    print dmy
    print 'nt' + str(nT)
    const_a_p=7.81441e-9
    p1 = []
    m1 = []
    av_t = []
    av10_t = []
    gmax_t = []
    ez_t = []
    av_th = 0
    av10_th = 0
    nesize = float(config['ne.SetBounds_1'])
    nenum = int(config['ne.SetMatrixSize_0'])
    step1 = nesize/nenum
    v = 2. * math.pi * step1 * step1 * wl
    a0 = const_a_p*math.sqrt(ppp) 
    maxe = 0
    g_max = 0.
    g_min = 1.e9
    eprev = 0
    conc = 0
    conc1 = 0
    conc2 = 0
#    ff = open('fg.dat', 'w')
    for i in range(nmin,nmax,delta):
        pname = ppath + '/' + "%06d.txt" % (i,)
        print pname
        pfield = read_diag(pname,nx1,ny1)
        ezname = ezpath + '/' + "%06d.txt" % (i,)
        bzname = bzpath + '/' + "%06d.txt" % (i,)
        ez = read_field2d(ezname,nx,ny)
        bz = read_field2d(bzname,nx,ny)
        ezv = ez[nx/2][ny/2]
        bzv = bz[nx/2][ny/2-dmy]*1.53
        tmp = (ezv*ezv + bzv*bzv)*coeff
        prof, prof10, av, av10, gmax = profile(pfield,de)
        p1.append(prof)
        av_t.append(av)
        av10_t.append(av10)
        gmax_t.append(gmax)
        ez_t.append(ezv)
        
#        ff.write('%d %lf %lf\n'%(i, ezv, av10))
        if maxe < ezv:
            maxe = ezv
        if n0 == 0 and tmp > 0.993*peakpower:
            n0 = 1.
            i_0 = i

        if n0 != 0 and nf == 0:
            if av10 > g_max:
                g_max = av10
            if av10 < g_min:
                g_min = av10 
     
        if n0 != 0 and nf == 0 and tmp < 0.993*peakpower:
            nf = 1.
            i_f = i
            av_th = av
            av10_th = av10
            nename = necpath + '/' + "%06d.txt" % (i,)
            e = read_conc(nename)
            for j in range(len(e)):
                e[j] = e[j]/((j+0.5)*v)
            conc = max(e)
            phase = math.asin(ezv/maxe)
            if ezv < eprev:
                phase = math.pi - phase
            print ' iter = ' + str(i_f)
            print ' conc = ' + str(conc)
            print ' phase = ' + str(phase/math.pi)
            print ' maxe = ' + str(maxe) + ' ezv = ' + str(ezv)
        if n0 != 0 and nf1 == 0 and tmp < 0.905*peakpower:
            nf1 = 1.
            i_f1 = i
            nename = necpath + '/' + "%06d.txt" % (i,)
            e = read_conc(nename)
            for j in range(len(e)):
                e[j] = e[j]/((j+0.5)*v)
            conc1 = max(e)
            print ' conc1 = ' + str(conc1)
        if n0 != 0 and nf2 == 0 and tmp < 0.75*peakpower:
            nf2 = 1.
            i_f2 = i
            nename = necpath + '/' + "%06d.txt" % (i,)
            e = read_conc(nename)
            for j in range(len(e)):
                e[j] = e[j]/((j+0.5)*v)
            conc2 = max(e)
            print ' conc2 = ' + str(conc2)
        eprev = ezv
    axis = create_axis(nmax-nmin,step,nmin*step)
#    ff.close()
    value = [i_f*step]*(ny1/factor)
    fig = plt.figure(num=None)
    plt.rc('text', usetex=True)
    ax = fig.add_subplot(2,1,1)
    
    ax.imshow(p1, extent=[Emin, Emax, nmin*step, (nmax-1)*step], aspect = 'auto', norm=clr.LogNorm(), origin = 'lower')
#    a1 = np.array(gaxis)
#    a2 = np.array(axis)
#    surf = np.array(p1)
#    ax.pcolor(a1,a2,surf)
    ax.autoscale(False)
    ax.plot(av_t, axis)
    ax.plot(av10_t, axis)
    ax.plot(gaxis, value, 'r')
    ax.set_ylabel('t, T')
    ax.set_xlabel('$\gamma$')
#    ax.set_xscale('log')
#    ax.set_xlim([Emin,Emax])
#    ax.set_ylim([nmin*step, (nmax-1)*step])
    ax = fig.add_subplot(2,1,2)
    ax.plot(axis, av_t, label = '$\lambda/2$')
    ax.plot(axis, av10_t, label = '$\lambda/10$')
    ax.plot(value, gaxis, 'r')
    ax.set_xlabel('t, T')
    ax.set_ylabel('$\gamma$')
    ax.set_ylim([0, max([max(av_t), max(av10_t)])])
    ax.legend(loc='lower left')
    ax1 = ax.twinx()
    ax1.plot(axis, ez_t, 'k', label = 'Field')
    ax1.set_ylabel('El field')
    ax1.legend(loc='upper left')
    f = open('gamma_av.dat', 'w')
    f.write('%lf %lf %lf %le %le %le %lf %lf %lf' % (peakpower, av_th, av10_th, conc, conc1, conc2, phase, g_min, g_max))
    f.close()
#    plt.close()
    plt.show()

if __name__ == '__main__':
    main()
