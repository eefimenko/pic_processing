#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_stat(filename):
    f = open(filename, 'r')
    power = []
    ax = []
    i = 0
    shift = 26
    for line in f:
        tmp = line.split()
        i += 1
        if i < shift:
            continue
        
        ax.append(float(tmp[0]))
        power.append(float(tmp[4]))
    f.close()
    return ax, power

def read_trapped(filename):
    f = open(filename, 'r')
    ntr = []
    ax = []
    for line in f:
        tmp = line.split()
        ntr.append(float(tmp[1]))
        ax.append(float(tmp[0]))
    f.close()
    return ax, ntr

def main():
    path = '.'
    config = utils.get_config(path + "/ParsedInput.txt")
    pwr = float(config['PeakPowerPW'])
    a, power = read_stat(path + '/h_stat.txt')
    a1, ntr = read_trapped(path + '/trapped_%d.dat'%pwr)
    
    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(2,1,1)
    p, = ax1.plot(power, 'r', label = 'power')
    plt.legend(loc = 'lower right')
    ax2 = ax1.twinx()
    n, = ax2.plot(ntr, 'g', label = 'trapped')
    plt.legend(loc = 'upper left')
    ax1.set_yscale('log')
    ax1.set_ylim([1e-13, 1e2])
      
    ax2.set_yscale('log')
    ax2.set_ylabel('Ntrapped')
    ax1.set_ylabel('Power, PW')
    ax2.set_xlabel('Time, T')
    ax3 = fig.add_subplot(2,1,2)
    m1 = len(power)
    m2 = len(ntr)
    m = min([m1,m2])
    rate = []
    av = []
    n = 15
    for i in range(m):
        rate.append(power[i]/ntr[i])
    for i in range(m-n):
        s = 0
        for k in range(n):
            s += rate[i+k]
        av.append(s/n)
    ax3.plot(rate, label = 'ratio')
    ax3.plot(av, label = 'av ratio')
    ax3.set_ylabel('Power/Ntrapped, PW/s/particle')
    ax3.set_xlabel('Time, iteration')
    plt.legend(loc = 'lower right')
    f = open('eff.txt', 'w')
    for i in range(len(av)):
        f.write('%le\n'%av[i])
    f.close()
    plt.show()

if __name__ == '__main__':
    main()
