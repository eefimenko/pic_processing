#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.cm as cm
import matplotlib.colors as colors
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def main():
    mp.rcParams.update({'font.size': 16})
    power = [8., 9., 10., 12., 15., 20.]
    energy = [2533., 2645., 2690., 2730., 2745., 2703.]
    ntrapped = [1.e9, 1.2e9, 1.3e9, 1.6e9, 2.73e9, 2.78e9]
    eff = []
    for i in range(len(power)):
        tmp = 2*ntrapped[i]*energy[i]*0.511*1e6*1.6e-12*1e-7/(power[i]*1e15*3e-15)
        eff.append(tmp)
    fig = plt.figure(num=None)
    ax = fig.add_subplot(1,1,1)
    e, = ax.plot(power, energy, 'b')
    ay = ax.twinx()
    print eff
    f, = ay.plot(power,eff, 'r')
    ay.set_ylim([0, 0.03])
    plt.legend([e,f], ['Average energy', 'Efficiency'], loc = 'lower right')
    ax.set_xlabel('Power, PW')
    ax.set_ylabel('Average radiated energy/$mc^2$')
    plt.show()
    
if __name__ == '__main__':
    main()
