#!/usr/bin/python
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import utils
import sys
import os
from tqdm import *
from pylab import *

def main():
   
    path = './'
    picspath = './pics'

    config = utils.get_config(path + "/ParsedInput.txt")
    ev = float(config['eV'])
#    phi = int(config['QEDstatistics.OutputN_phi'])
#    theta = int(config['QEDstatistics.OutputN_theta'])
#    dphi = 2*math.pi/phi
#    dtheta = math.pi/theta
    wl = float(config['Wavelength'])

#    S = 2 * math.pi * 0.1 * 0.1 * wl * wl * 100 # square of source mm^2, r = 0.1 lambda 
#    ds = 1e6
#    ds1 = 2 * math.pi * (1 - math.cos(0.05))*1e6
#    ds2 = 2 * math.pi * (1 - math.cos(0.23))*1e6
#    ds3 = 2 * math.pi * (1 - math.cos(0.42))*1e6
#    emin = float(config['QEDstatistics.Emin'])
#    emax = float(config['QEDstatistics.Emax'])
#    ne = int(config['QEDstatistics.OutputN_E'])
    nit = int(config['BOIterationPass'])
    dt = float(config['TimeStep'])*nit
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    duration = float(config.get('Duration',15))
    Xmax = float(config['X_Max']) #mkm
    Xmin = float(config['X_Min']) #mkm
    Ymax = float(config['Y_Max']) #mkm
    Ymin = float(config['Y_Min']) #mkm
    Zmax = float(config['Z_Max']) #mkm
    Zmin = float(config['Z_Min']) #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)
    T = 2 * math.pi/omega
    step = float(config['TimeStep'])*float(config['BOIterationPass'])
    nT = T/step
    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz
    dmy = int(0.35*wl/dy)
    dv = 2.*dx*dy*dz

    nx1 = int(config['MatrixSize_X'])
    ny1 = int(config['MatrixSize_Y'])
    T = 2 * math.pi/omega
    nnn = int(T/dt)
#    de = emax*1e-9/ev/ne #GeV
#    print 'de =', de
    ftype = config['BODataFormat']
    curr_dir = os.getcwd()
    
    print curr_dir
    print 'Read electric field in the center vs time'
    ez_c_t = utils.ts(path, utils.ezpath, name='ec', ftype = ftype,
                    tstype='center', shape = (nx1,ny1), verbose=True)
    print 'Read max electric field vs time'
    ez_m_t = utils.ts(path, utils.eypath, name='em', ftype = ftype,
                    tstype='max', shape = (nx1,ny1))
    print 'Read electric field in max in dipole wave vs time'
    ez_md_t = utils.ts(path, utils.ezpath, name='em_d', ftype = ftype,
                      tstype='func', func = lambda x: x[nx1/2, ny1/2-dmy],
                      shape = (nx1,ny1), verbose=True)
    print 'Read magnetic field in the center vs time'
    bz_c_t = utils.ts(path, utils.bzpath, name='bc', ftype = ftype,
                    tstype='center', shape = (nx1,ny1), verbose=True)
    print 'Read max magnetic field vs time'
    bz_m_t = utils.ts(path, utils.bypath, name='bm', ftype = ftype,
                    tstype='max', shape = (nx1,ny1), verbose=True)
    print 'Read Ne in the center vs time'
    nez_c_t = utils.ts(path, utils.nezpath, name='nec', ftype = ftype,
                    tstype='center', shape = (nx1,ny1), verbose=True)
    print 'Read max Ne vs time'
    nez_m_t = utils.ts(path, utils.nezpath, name='nem', ftype = ftype,
                    tstype='max', shape = (nx1,ny1), verbose=True)
    xmin = 0
    xmax = -1
    axis_t = utils.create_axis(ez_c_t.shape[0], 1./nT)
    axis_n_t = utils.create_axis(nez_c_t.shape[0], 1./nT)
    p = 0
    p_prev = 0
    i0 = 0
    i1 = 0
    threshold = 0.97
    nl_power = np.zeros(len(ez_m_t))
    
    for i in range(len(ez_m_t)):
        p_prev = p
        p = ((bz_m_t[i]/3e11)**2 + (ez_m_t[i]/0.653/3e11)**2)*10.
        
        if i0 == 0 and p > threshold * ppw and p_prev < threshold * ppw:
            i0 = i
        if i1 == 0 and p < threshold * ppw and p_prev > threshold * ppw:
            i1 = i
        nl_power[i] = ((bz_c_t[i]/3e11)**2 + (ez_md_t[i]/0.653/3e11)**2)*10.
    print i0, i1
    i2 = i1 + 120
    
    gamma = math.log(nez_m_t[i1]/nez_m_t[i0])/(i1 - i0)*nT

    print gamma

    ax_ = []
    ax1_ = []
    pow_t = []
    pow1_t = []
    max_t = []
    av_t = []
    br_full_t = []
    br_100mev_t = []
    br_1gev_t = []
        
    f = open('br_t.dat', 'r')
    i = 0
    T1 = 2*math.pi/omega*1e15
    for line in f:
        tmp = line.split()
        ax_.append(float(tmp[0])/T/1e15)
        pow_t.append(ppw-float(tmp[2]))
        pow1_t.append(float(tmp[2]))
        max_t.append(float(tmp[3]))
        av_t.append(float(tmp[4]))
        ax1_.append(float(tmp[0])/T/1e15 - 2.)
        br_full_t.append(float(tmp[5]))
        br_100mev_t.append(float(tmp[6]))
        br_1gev_t.append(float(tmp[7]))

    el_ax_ = []
    el_ax1_ = []
    el_pow_t = []
    el_pow1_t = []
    el_max_t = []
    el_av_t = []
    el_br_full_t = []
    el_br_100mev_t = []
    el_br_1gev_t = []
        
    f = open('el_br_t.dat', 'r')
    i = 0
    T1 = 2*math.pi/omega*1e15
    for line in f:
        tmp = line.split()
        el_ax_.append(float(tmp[0])/T/1e15)
        el_pow_t.append(ppw-float(tmp[2]))
        el_pow1_t.append(float(tmp[2]))
        el_max_t.append(float(tmp[3]))
        el_av_t.append(float(tmp[4]))
        el_ax1_.append(float(tmp[0])/T/1e15 - 2.)
        el_br_full_t.append(float(tmp[5]))
        el_br_100mev_t.append(float(tmp[6]))
        el_br_1gev_t.append(float(tmp[7]))

    for i in range(len(el_pow1_t)):
        el_pow_t[i] = ppw - 2.*el_pow1_t[i] - pow1_t[i]
    

    bound = 30    
    fig = plt.figure(num=None, figsize = (10, 5))
    mp.rcParams.update({'font.size': 20})
    ax = fig.add_subplot(1,1,1)
#    ax.set_title('(a) Electric field')
    bzm, = ax.plot(axis_t, utils.env(bz_c_t, axis_t)/1e11, 'b')
    ax.plot([0,30], [3.51, 3.51], 'b--')
    ax1 = ax.twinx()
    nem, = ax1.plot(axis_n_t, nez_m_t/dv/1e24, 'r')
#    ax2 = ax.twinx()
#    pwm, = ax2.plot(ax1_, pow1_t, 'g')
#    ax2.spines["left"].set_position(("axes", -0.18))
#    utils.make_patch_spines_invisible(ax2)
#    ax2.spines["left"].set_visible(True)
#    ax2.yaxis.set_label_position('left')
#    ax2.yaxis.set_ticks_position('left')
    ax.set_xlim([0, bound])
    ax.set_ylim([0, 3.6])
    ax.set_xlabel('$t, T$')
    ax.set_ylabel('$|B|, \\times 10^{11}G$')
    ax.axvline(x=9, color='k', linewidth = '0.2')
    ax.axvline(x=14, color='k', linewidth = '0.2')
    ax.axvline(x=22, color='k', linewidth = '0.2')
    ax.fill_between([9,14], 0, 3.6, facecolor='green', alpha=0.2)
    ax.text(10, 0.25, 'Linear\ncascade', fontsize = 16)
    ax.fill_between([14,22], 0, 3.6, facecolor='red', alpha=0.2)
    ax.text(15, 0.25, 'Instability/\nRing\ncompression', fontsize = 16)
    ax.fill_between([22,30], 0, 3.6, facecolor='blue', alpha=0.2)
    ax.text(24, 0.25, 'Nonlinear\ncascade', fontsize = 16)
    ax1.set_xlim([0, bound])
    ax1.set_xlabel('$t, T$')
    ax1.set_ylabel('$N_p, \\times 10^{24} cm^{-3}$')
#    ax2.set_xlim([0, bound])
#    ax2.set_ylim([0,7])
    ax1.set_ylim([0.0001,10])
    ax1.set_yscale('log')
#    ax2.set_xlabel('$t, T$')
#    ax2.set_ylabel('$P, PW$')
#    ax.text(-10, max(bz_c_t/1e11), '(b)')
    plt.legend([bzm, nem], ['$B_c$', '$N_{p}$'], loc = 'upper left', frameon = False)
#    plt.legend([bzm, nem, pwm], ['$B_c$', '$N_{p}$', '$P_{ph}$'], loc = 'upper left', frameon = False)
    plt.tight_layout()
    
    picname = picspath + '/' + "mdipole_article_%dPW.png" %(ppw)
    print picname
    plt.savefig(picname, dpi=256)
    plt.close()
 
if __name__ == '__main__':
    main()
