#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
from matplotlib.pyplot import cm
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def read_diag(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            row.append(array[index]+ 1e-10)
        field.append(row)
    return field

def read_field(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index]+1e-10)
        field.append(row)
    return field

def read_maxfield(file):
    axis = []
    ez_t = []
    bx_t = []
    ne_t = []
    np_t = []
    f = open(file, 'r')
    for line in f:
        a = line.split()
        axis.append(float(a[0]))
        ez_t.append(float(a[1]))
        bx_t.append(float(a[2]))
        ne_t.append(float(a[3]))
        np_t.append(float(a[4]))
    return axis, ez_t, bx_t, ne_t, np_t

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])

def create_subplot(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, text, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
    name = path + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny, mult)
    maxe = max([max(row) for row in field])
    mine = min([min(row) for row in field])
    v_max_ = max2d(field)
    ratio = 1e-3
    if maxe == mine:
        v_min_ = maxe
        v_max_ = 1e9*maxe
    else:
        v_max_ = maxe
        v_min_ = maxe*ratio
   
    ticks = [-2,-1,0,1,2]
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = v_min_, vmax = v_max_, norm=clr.LogNorm())
    ax.text(-1.5, 2.1, text)
    ax.set_xlim([-1,1])
    ax.set_ylim([-1,1])
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
#    plt.xticks(ticks)
#    plt.yticks(ticks)
#    plt.colorbar(surf,  orientation  = 'vertical', ticks = [v_min, v_max])
    return ax

def create_subplot_f(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, text, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
    name = path + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny, mult)
    maxe = max([max(row) for row in field])
    v_max_ = max2d(field)
    print 'here', v_max_
    ratio = 1e-3
    ticks = [-2,-1,0,1,2]
    amax = 1.
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = 0.0000001, vmax = amax)
    ax.text(-1, 2.1, text)
    ax.set_xlim([-1,1])
    ax.set_ylim([-1,1])
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
#    plt.xticks(ticks)
#    plt.yticks(ticks)
#    plt.colorbar(surf,  orientation  = 'vertical', ticks = [v_min, v_max])
    return ax

def create_subplot_cur(fig,i,pathx,pathy,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, text, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
    axisx = create_axis(nx, (Xmax-Xmin)/nx, Xmin)
    axisy = create_axis(ny, (Ymax-Ymin)/ny, Ymin)
    Y, X = np.mgrid[Xmin:Xmax:nx*1j, Ymin:Ymax:ny*1j]
    field = [[0.  for j in range(ny)] for k in range(nx)]
    xmi = -2
    xma = 2
    namex= pathx+ '/' + "%06d.txt" % (i,)
    namey= pathy+ '/' + "%06d.txt" % (i,)
    print namex, namey
    jx= read_field(namex,nx,ny, mult)
    jy= read_field(namey,nx,ny, mult)
    thr = 1e-6
    for k in range(nx):
        for j in range(ny):
            field[k][j] = math.sqrt(jx[k][j]*jx[k][j] + jy[k][j]*jy[k][j])
    q = max2d(field)
    for k in range(nx):
        for j in range(ny):
            if abs(jx[k][j]) < thr*q:
                jx[k][j] = 0.
            if abs(jy[k][j]) < thr*q:
                jy[k][j] = 0.
            if field[k][j] != 0:
                jx[k][j] /= field[k][j]
                jy[k][j] /= field[k][j]
           
       
    maxe = max([max(row) for row in field])
    v_max_ = max2d(field)
    print "Max", v_max_
    ratio = 1e-3
    ticks = [-2,-1,0,1,2]
    amax = 1.
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = maxe*1e-3, vmax = maxe)
    ax.streamplot(X, Y, np.array(jy), np.array(jx), 
                    density=3,
                    color='DarkRed', 
                    arrowstyle='->',     # arrow style
                    arrowsize=1.5)
    ax.text(-1, 2.1, text)
    ax.set_xlim([-1,1])
    ax.set_ylim([-1,1])
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
#    plt.xticks(ticks)
#    plt.yticks(ticks)
#    plt.colorbar(surf,  orientation  = 'vertical', ticks = [v_min, v_max])
    return ax

def main():
    jxpath = 'data/Jy_x'
    jypath = 'data/Jz_x'
    elzpath = 'data/Electron2Dx'
    phzpath = 'data/Photon2Dx'
    ezpath = 'data/E2x'
    bzpath = 'data/B2x'  
    picspath = 'pics'
    
    config = utils.get_config("ParsedInput.txt")
    wl = float(config['Wavelength'])
    Xmax = float(config['X_Max'])/wl #mkm to wavelength
    Xmin = float(config['X_Min'])/wl #mkm to wavelength
    Ymax = float(config['Y_Max'])/wl #mkm to wavelength
    Ymin = float(config['Y_Min'])/wl #mkm to wavelength

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    ppw = int(config['PeakPowerPW'])
    a00 = 3.e11 * math.sqrt(ppw/10.)
    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    mult1 = 1/(2.*dx*dy*dz*1e-12)
    delta = 1

    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(jxpath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(jxpath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'

    dv = dx*dy*dz
    axisx = create_axis(nx, (Xmax-Xmin)/nx, Xmin)
    axisy = create_axis(ny, (Ymax-Ymin)/ny, Ymin)
    Y, X = np.mgrid[Xmin:Xmax:nx*1j, Ymin:Ymax:ny*1j]
    absv = [[0.  for j in range(ny)] for i in range(nx)]
    xmi = -2
    xma = 2
    for i in range(nmin, nmax, delta): 
        picname = picspath + '/' + "fcm%06d.png" % (i,)
        print picname
        fig = plt.figure(num=None, figsize=(25, 5), dpi=256)

        pe = create_subplot_f(fig,i,ezpath,nx,ny,1,5,1, Xmin, Xmax, Ymin, Ymax, 0, 0, '$y/\lambda$', '$x/\lambda$', 'Reds', 'Electric field', mult = 1./a00)
        pb = create_subplot_f(fig,i,bzpath,nx,ny,1,5,2, Xmin, Xmax, Ymin, Ymax, 0, 0, '$y/\lambda$', '', 'Reds', 'Magnetic field', mult = 1./a00)

        pel = create_subplot(fig,i,elzpath,nx,ny,1,5,3, Xmin, Xmax, Ymin, Ymax, 0, 0, '$y/\lambda$', '','Greens', 'Electron distribution', mult1)

        pph = create_subplot(fig,i,phzpath,nx,ny,1,5,5, Xmin, Xmax, Ymin, Ymax, 0, 0, '$y/\lambda$', '','Blues', 'Photon distribution', mult1)
        pph = create_subplot_cur(fig,i,jxpath,jypath,nx,ny,1,5,4, Xmin, Xmax, Ymin, Ymax, 0, 0, '$y/\lambda$', '','Greens', 'Current distribution', mult1)
#        name = jxpath + '/' + "%06d.txt" % (i,)
#        jx = read_field(name,nx,ny)
#        name = jypath + '/' + "%06d.txt" % (i,)
#        jy = read_field(name,nx,ny)
#        name = elzpath + '/' + "%06d.txt" % (i,)
#        elz = read_field(name,nx,ny)
#        name = phzpath + '/' + "%06d.txt" % (i,)
#        phz = read_field(name,nx,ny)
#        name = ezpath + '/' + "%06d.txt" % (i,)
#        ez = read_field(name,nx,ny)
#        name = bzpath + '/' + "%06d.txt" % (i,)
#        bz = read_field(name,nx,ny)
      
#        pcur = fig.add_subplot(1,5,4)
#        pe = fig.add_subplot(1,5,1)
#        pb = fig.add_subplot(1,5,2)
#        pel = fig.add_subplot(1,5,3)
#        pph = fig.add_subplot(1,5,5)
        
#        surf = pe.imshow(ez, extent=[xmi, xma, xmi, xma], cmap='Reds')
#        surf = pb.imshow(bz, extent=[xmi, xma, xmi, xma], cmap='Reds')
#        surf = pel.imshow(elz, extent=[xmi, xma, xmi, xma], cmap='Greens')
#        surf = pph.imshow(phz, extent=[xmi, xma, xmi, xma], cmap='Blues')
#        for k in range(nx):
#            for j in range(ny):
#                absv[k][j] = math.sqrt(jx[k][j]*jx[k][j] + jy[k][j]*jy[k][j])
#        for k in range(nx):
#            for j in range(ny):
#                if absv[k][j] != 0:
#                    jx[k][j] /= absv[k][j]
#                    jy[k][j] /= absv[k][j] 
       
#        a = np.array(absv)
#        lw = 5*a/a.max()
#        pjq.quiver(axisx, axisy, jx,jy, absv, cmap=cm.cool)
#        print X, Y
#        surf = pcur.imshow(absv, extent=[xmi, xma, xmi, xma], cmap='Greens')
#        pcur.streamplot(X, Y, np.array(jy), np.array(jx), 
#                       density=1,
#                       color='DarkRed', 
                       #linewidth=lw,
#                       arrowstyle='->',     # arrow style
#                       arrowsize=1.5)
#        plt.colorbar()
#        s.colorbar()
#        pcur.set_xlim([-2,2])
#        pcur.set_ylim([-2,2])
#        plt.show()
        plt.savefig(picname)
        plt.close()
    
if __name__ == '__main__':
    main()
