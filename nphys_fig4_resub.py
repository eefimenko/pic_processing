#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np
import matplotlib as mp
from matplotlib.font_manager import FontProperties

def main():
    power_p = []
    ezx_p = []
    bzx_p = []
    nex_p = []
    nemaxx_p = []
    ncrx_p = []
    jx_min_p = []
    jx_max_p = []
    powx_p = []
    pow1x_p = []
    maxx_p = []
    avx_p = []
    el_powx_p = []
    el_pow1x_p = []
    el_maxx_p = []
    el_avx_p = []
    picspath = 'pics/'
    
    f = open('stat_summary_el.txt', 'r')
    for line in f:
        tmp = line.split()
        power_p.append(float(tmp[0]))
        ezx_p.append(float(tmp[1])/1e11)
        bzx_p.append(float(tmp[2])/1e11)
        maxx_p.append(float(tmp[3]))
        avx_p.append(float(tmp[4]))
        ncrx_p.append(float(tmp[5]))
        nex_p.append(float(tmp[6])/1e25)
        powx_p.append(float(tmp[7]))
        pow1x_p.append(float(tmp[8]))
        jx_min_p.append(float(tmp[9]))
        jx_max_p.append(float(tmp[10]))
        el_maxx_p.append(float(tmp[11]))
        el_avx_p.append(float(tmp[12]))
        el_powx_p.append(float(tmp[13]))
        el_pow1x_p.append(float(tmp[14]))
        nemaxx_p.append(float(tmp[15])/1e25)
    f.close()
    thr_power = 7.2
    nmax = 100
    dp = (power_p[-1]-thr_power)/nmax
    p = np.zeros(nmax)
    e = np.zeros(nmax)
    b = np.zeros(nmax)
    for i in range(nmax):
        p[i] = thr_power + dp*i
        e[i] = 3*math.sqrt(p[i]/10)
        b[i] = 0.653*3*math.sqrt(p[i]/10)

    
    ep = 3*math.sqrt(thr_power/10)
    bp = 0.653*3*math.sqrt(thr_power/10)

    xticks = [8,10,12,14]
    xticklabels = ['$%d$'%(x) for x in xticks]
    # fontsize = 14
    # fig = plt.figure(figsize = (15, 12))
    # mp.rcParams.update({'font.size': fontsize})
    # fig.subplots_adjust(bottom=-0.08)
    # spx = 4
    # spy = 3
    
    msize = 6
    mp.rcParams.update({'font.size': 10})
    mp.rcParams.update({'font.family': 'sans-serif'})
    
    legfontsize = 15
    nx = 4
    ny = 4
    fig = plt.figure(num=None, figsize=(16, 12), dpi=256)
   
    # ax = fig.add_subplot(spx,spy,1)
    ax = fig.add_subplot(nx,ny,1)
    plt.minorticks_on()
    ax.plot(power_p, ezx_p, 'r-o', label='E', fillstyle = 'none', markersize = msize)
    ax.plot(p, e, 'r-', label = 'E', dashes = [2,2])
    ax.plot([thr_power,power_p[-1]], [ep,ep], 'r-' , label = 'E', dashes = [6,1])
    # ax.plot([thr_power,power_p[-1]], [ep,ep], 'r--')
    ax.plot(power_p, bzx_p, 'b-v', label='B - Stationary', fillstyle = 'none', markersize = msize)
    ax.plot(p, b, 'b-', label = 'B - Vacuum', dashes = [2,2])
    ax.plot([thr_power,power_p[-1]], [bp,bp], 'b-', label = 'B - Threshold', dashes = [6,1])
    # ax.plot([thr_power,power_p[-1]], [bp,bp], 'b--')
    #ax.set_xlim([7,power_p[-1]+0.5])
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    plt.legend(loc = 'lower right', fontsize = 13, ncol=2, frameon=False)
    yticks = [0,1,2,3,4]
    ax.set_yticks(yticks)
    ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ax.set_xlabel('$P, PW$')
    ax.set_ylabel('$E,B, \\times 10^{11} statV/cm$')
    ax.text(5.8, 4, '$(a)$')
#    picname = picspath + '/' + "fig4a.png"
#    plt.savefig(picname, dpi=256, bbox_inches='tight')
#    plt.close()
    
    rpow_p = []
    rpow1_p = []
    eff_p = []
    eff1_p = []
    
    for i in range(len(power_p)):
        el_powx_p[i] = power_p[i] - el_powx_p[i]
        powx_p[i] = power_p[i] - powx_p[i]
        rpow_p.append(power_p[i] - powx_p[i] - 2*el_powx_p[i])
        print power_p[i], powx_p[i], el_powx_p[i], power_p[i] - powx_p[i] - 2*el_powx_p[i]
        eff_p.append((powx_p[i] + 2*el_powx_p[i])/power_p[i])
        eff1_p.append(powx_p[i]/power_p[i])

    # power1_p = [0, thr_power] + power_p
    # rpow1_p = [0, thr_power] + rpow_p
    # powx1_p = [0, 0] + powx_p
    # el_powx1_p = [0, 0] + el_powx_p
    power1_p = power_p
    rpow1_p = rpow_p
    powx1_p = powx_p
    el_powx1_p = el_powx_p
#    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    ax = fig.add_subplot(nx,ny,2)
    plt.minorticks_on()
    # ax = fig.add_subplot(spx,spy,2)
    print power1_p
    ax.plot(power1_p, rpow1_p, 'b-o', label = 'Reflected', fillstyle = 'none', markersize = msize)
    ax.plot(power1_p, powx1_p, 'g-v', label = 'Photons', fillstyle = 'none', markersize = msize)
    ax.plot(power1_p, el_powx1_p, 'r-s', label = 'Positrons', fillstyle = 'none', markersize = msize)
    # ax.plot([thr_power, thr_power], [0, 8], 'b--', label = 'Threshold')
    plt.minorticks_on()
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    yticks = [2,4,6,8]
    ax.set_yticks(yticks)
    ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ax.set_xlabel('$P, PW$')
    ax.set_ylabel('Radiated power, $PW$')
    ax.set_xlim([7,15])
    # ax.set_xticks([0,5,thr_power,10,15])
    plt.legend(bbox_to_anchor=(1., 0.5), fontsize = 13, frameon=False)
    ax.text(5.8, 8, '$(b)$')
#    picname = picspath + '/' + "fig4b.png"
#    plt.savefig(picname, dpi=256, bbox_inches='tight')
#    plt.close()
    
#    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    ax = fig.add_subplot(nx,ny,3)
    plt.minorticks_on()
    ax.plot([thr_power]+ power_p, [0] + eff_p, 'b-v', label = 'Absorption', fillstyle = 'none', markersize = msize)
    ax.plot([thr_power] + power_p, [0] + eff1_p, 'g-o', label = 'Conversion', fillstyle = 'none', markersize = msize)
    ax.set_xlabel('$P, PW$')
    ax.set_ylabel('Efficiency')
    yticks = [0.2,0.4,0.6]
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    ax.set_yticks(yticks)
    ax.set_yticklabels(['$%.1f$'%(x) for x in yticks])
    plt.legend(loc = 'lower right', frameon = False, fontsize = 13)
    ax.set_xlim([7,15])
    ax.text(5.5, 0.6, '$(c)$')
#    picname = picspath + '/' + "fig4c.png"
#    plt.savefig(picname, dpi=256, bbox_inches='tight')
#    plt.close()
    
#    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    ax = fig.add_subplot(nx,ny,4)
    plt.minorticks_on()
    # ax = fig.add_subplot(spx,spy,4)
    ax.plot(power_p, nemaxx_p, 'r-v', label = 'Max', fillstyle = 'none', markersize = msize)
    ax.plot(power_p, nex_p, 'b-o', label = 'Stationary', fillstyle = 'none', markersize = msize)
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    yticks = [0,1,2,3,4,5]
    ax.set_yticks(yticks)
    ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ax.set_xlabel('$P, PW$')
    ax.set_ylabel('$N_e, \\times 10^{25} cm^{-3}$')
    plt.legend(loc = 'upper left', fontsize = 13, frameon = False)
    ax.set_xlim([7,15])
    ax.text(5.8, 5, '$(d)$')
#    picname = picspath + '/' + "fig4d.png"
#    plt.savefig(picname, dpi=256, bbox_inches='tight')
#    plt.close()
        
#    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    maxx = fig.add_subplot(nx,ny,5)
    plt.minorticks_on()
    # maxx = fig.add_subplot(spx,spy,3)
    mel, = maxx.plot(power_p, el_maxx_p, 'r-^', label = 'Positrons', fillstyle = 'none', markersize = msize)
    m, = maxx.plot(power_p, maxx_p, 'g-v', label = 'Photons', fillstyle = 'none', markersize = msize)
    maxx.set_xlim([7,15])
    maxx.set_ylim([0, 1.5])
    maxx.set_xlabel('$P, PW$')
    maxx.set_ylabel('Maximal energy, GeV')
    # maxx.yaxis.label.set_color(m.get_color())
    # maxx.spines["left"].set_color(m.get_color())
    # maxx.tick_params(axis='y', colors=m.get_color())
    yticks = [0.5,0.9,1.3]
    maxx.set_yticks(yticks)
    maxx.set_yticklabels(['$%.1f$'%(x) for x in yticks])
    maxx.set_xticks(xticks)
    maxx.set_xticklabels(xticklabels)
    maxx.legend(bbox_to_anchor=(1, 0.5), fontsize = 13, frameon = False)
    maxx.text(5.6, 1.5, '$(e)$')
#    picname = picspath + '/' + "fig4e.png"
#    plt.savefig(picname, dpi=256, bbox_inches='tight')
#    plt.close()
    
#    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    avx = fig.add_subplot(nx,ny,6)
    plt.minorticks_on()
    ael, = avx.plot(power_p, el_avx_p, 'r-^', label = 'Positrons', fillstyle = 'none', markersize = msize)
    a, = avx.plot(power_p, avx_p, 'g-v', label = 'Photons', fillstyle = 'none', markersize = msize)
    avx.set_xlim([7,15])
    avx.set_ylim([0, 1.1*max(el_avx_p)])
    avx.set_ylabel('Average energy, GeV')
    avx.set_xlabel('$P, PW$')
    # avx.yaxis.label.set_color(a.get_color())
    # avx.spines["right"].set_color(a.get_color())
    # avx.spines["left"].set_color(m.get_color())
    # avx.tick_params(axis='y', colors=a.get_color())
    avx.legend(bbox_to_anchor=(1, 0.5), fontsize = 13, frameon = False)
    yticks = [0.03,0.22,0.27,0.32]
    avx.set_yticks(yticks)
    avx.set_yticklabels(['$%.2f$'%(x) for x in yticks])
    avx.set_xticks(xticks)
    avx.set_xticklabels(xticklabels)
    avx.text(5.6, 0.35, '$(f)$')
#    picname = picspath + '/' + "fig4f.png"
#    plt.savefig(picname, dpi=256, bbox_inches='tight')
#    plt.close()

    f = open('charge.txt', 'r')
    power_p = []
    charge = []
    el_flux = []
    ph_flux = []
    for line in f:
        tmp = line.split()
        power_p.append(float(tmp[0]))
        charge.append(float(tmp[1]))
        el_flux.append(float(tmp[2]))
        ph_flux.append(float(tmp[3]))
    f.close()
#    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    fl_ax = fig.add_subplot(nx,ny,8)
#    plt.minorticks_on()
    fl_ax.set_xlim([7,15])
    # fl_ax = fig.add_subplot(spx,spy,6)
    fl_ax.plot(power_p, el_flux, 'r-^', label = 'Positrons', fillstyle = 'none', markersize = msize)
    fl_ax.plot(power_p, ph_flux, 'g-v', label = 'Photons', fillstyle = 'none', markersize = msize)
    fl_ax.set_ylabel('Flux,$\\times 10^{23} s^{-1}$')
    fl_ax.set_xlabel('$P, PW$')
#    yticks = [0,2,4,6,8]
#    fl_ax.set_yticks(yticks)
#    fl_ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    
    plt.legend(loc = 'upper left', fontsize = 13, frameon = False)
    ch_ax = fl_ax.twinx()
    ch_ax.plot(power_p, charge, 'b-o', label = 'Charge', fillstyle = 'none', markersize = msize)
    ch_ax.set_xticks(xticks)
    ch_ax.set_xticklabels(xticklabels)
#    yticks = [10,20,30,40,50]
#    ch_ax.set_yticks(yticks)
#    ch_ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ch_ax.set_xlabel('$P, PW$')
    ch_ax.set_ylabel('Charge, pC')
    plt.legend(loc = 'lower right', fontsize = 13, frameon = False)
    fl_ax.text(5.8, 9, '$(h)$')
#    picname = picspath + '/' + "fig4h.png"
#    plt.savefig(picname, dpi=256, bbox_inches='tight')
#    plt.close()
    
    f = open('jfull.txt', 'r')
    power_p = []
    jp = []
    jn = []
    jf = []
    jfm = []
    for line in f:
        tmp = line.split()
        power_p.append(float(tmp[0]))
        jp.append(float(tmp[1]))
        jn.append(float(tmp[2]))
        jf.append(float(tmp[3]))
        jfm.append(float(tmp[4]))
    f.close()
#    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    ax = fig.add_subplot(nx,ny,7)
    plt.minorticks_on()
    ax.set_xlim([7,15])
    # ax = fig.add_subplot(spx,spy,5)
    ax.plot(power_p, jfm, 'k-^', label='full max', fillstyle = 'none', markersize = msize)
    ax.plot(power_p, jp, 'r-o', label='$r < 0.44 \lambda$', fillstyle = 'none', markersize = msize)
    # ax.plot(power_p, jn, 'b-o', label='negative')
    ax.plot(power_p, jf, 'g-v', label='full', fillstyle = 'none', markersize = msize)
    
    ax.set_ylim([0, 17])
    ax.set_ylabel('$J_z, MA$')
    ax.set_xlabel('$P, PW$')
    yticks = [0,5,10,15]
    ax.set_yticks(yticks)
    ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    ax.text(5.8, 17, '$(g)$')
    plt.legend(loc = 'upper left', fontsize = 13, frameon = False)
#    picname = picspath + '/' + "fig4g.png"
#    plt.savefig(picname, dpi=256, bbox_inches='tight')
#    plt.close()
    
    dirs = ['8pw/', '15pw/']
    xmin = []
    xmax = []
    ymin = []
    ymax = []
    zmin = []
    zmax = []
    mult = []
    nx = []
    ny = []
    nz = []
    power = []
    numdirs = len(dirs)
    configs = []
    for i in range(numdirs):
        configs.append(utils.get_config(dirs[i] + "/ParsedInput.txt"))
    for k in range(numdirs):
        wl = float(configs[k]['Wavelength'])
        xmax.append(float(configs[k]['X_Max'])/wl) #mkm
        xmin.append(float(configs[k]['X_Min'])/wl) #mkm
        ymax.append(float(configs[k]['Y_Max'])/wl) #mkm
        ymin.append(float(configs[k]['Y_Min'])/wl) #mkm
        zmax.append(float(configs[k]['Z_Max'])/wl) #mkm
        zmin.append(float(configs[k]['Z_Min'])/wl) #mkm
        nx.append(int(configs[k]['MatrixSize_X']))
        ny.append(int(configs[k]['MatrixSize_Y']))
        nz.append(int(configs[k]['MatrixSize_Z']))
        power.append(int(configs[k]['PeakPowerPW']))
        dx = float(configs[k]['Step_X'])/wl
        dy = float(configs[k]['Step_Y'])/wl
        dz = float(configs[k]['Step_Z'])/wl
        mult.append(1/(2.*dx*dy*dz*wl*wl*wl))
        step = float(configs[k]['TimeStep'])*1e15*float(configs[k]['BOIterationPass'])
        omega = float(configs[k]['Omega'])
        T = 2 * math.pi/omega
        nt = int(T*1e15/step)
        print nt
    
    figures = [utils.bzpath, utils.nezpath, utils.jz_zpath, utils.nphzpath]
    cmaps = ['hot_r', 'YlGn', 'bwr', 'RdPu']
    titles = ['Electric field %d pw', 'Magnetic field %d pw', 'Positrons %d pw', 'J_z %d pw']
    log = [False, True, False, True]
    mult = [1e-11,mult[0],3.33564e-10*1e-16, mult[0]]
   
    offset = 6
    n0 = [1300, 800]
    spx_ = numdirs
#    n0 = [0] * numdirs 
    spy_ = len(figures)
    labels = ['i', 'j', 'k', 'l', 'm', 'n', 'o', 'p']
    i = 215
    verbose = 1
    for k in range(spx_):
        for j in range(spy_):
#            fig = plt.figure(num=None, figsize=(4., 4.))
            mp.rcParams.update({'font.size': 14})
            fontsize = 14
            ylabel = '$y/\lambda$'
            yticks = [-1, -0.5,0,0.5, 1]
            print 'Position', 9+j*spx_+k
            if j == 0:
                
                    # fx = utils.bo_file_load(dirs[k]+utils.ezpath,
                    #                         i+n0[k],nx[k],ny[k],verbose=1)
                    # fy = utils.bo_file_load(dirs[k]+utils.bzpath,
                    #                         i+n0[k],nx[k],ny[k],verbose=1)
                    # sf = np.square(fx) + np.square(fy)
                ax = utils.subplot(fig, i+n0[k], dirs[k]+figures[j],
                                   shape = (nx[k],ny[k]), position = (4,4,9+k*spy_+j),
                                   extent = [xmin[k], xmax[k], ymin[k], ymax[k]],
                                   cmap = cmaps[j], title = '', #titles[j] % (power[k]),
                                   colorbar = True, logarithmic=log[j], verbose=1,
                                   xlim = [-1.,1.], ylim = [-1.,1.], xticks = [-1, -0.5,0,0.5, 1], yticks = yticks,
                                   xlabel = '$x/\lambda$', ylabel = ylabel, fontsize=14, mult = mult[j])
            else:
                ax = utils.subplot(fig, i+n0[k], dirs[k]+figures[j],
                                   shape = (nx[k],ny[k]), position = (4,4,9+k*spy_+j),
                                   extent = [xmin[k], xmax[k], ymin[k], ymax[k]],
                                   cmap = cmaps[j], title = '', titletype = 'simple',
                                   colorbar = True, logarithmic=log[j], verbose=verbose,
                                   xlim = [-1.,1.], ylim = [-1.,1.], xticks = [-1, -0.5,0,0.5, 1], yticks = yticks,
                                   xlabel = '$x/\lambda$', ylabel = ylabel, vmax = 0.5, vmin=0.5,
                                   maximum = 'local', fontsize = fontsize, mult = mult[j])
                
                                   # shape = (nx[k],ny[k]), position = (spx,spy,j+1+k*spy_+offset),
                                   # extent = [xmin[k], xmax[k], ymin[k], ymax[k]],
                                   # cmap = cmaps[j], title = '', #titles[j] % (power[k]),
                                   # colorbar = True, logarithmic=log[j], verbose=1,
                                   # xlim = [-1.,1.], ylim = [-1.,1.], xticks = [-1, -0.5,0,0.5, 1], yticks = yticks,
                                   # xlabel = '$x/\lambda$', ylabel = ylabel, fontsize=14, mult = mult[j],)
                circ = plt.Circle((0, 0), radius=0.44, color='k', fill=False, linewidth = 0.5)
                ax.add_patch(circ)
            ax.text(-1.6, 1.1, '$(%s)$'%(labels[k*spy_ + j]))
    picname = picspath + '/' + "fig4_resub.png"
    plt.tight_layout()
    plt.savefig(picname, bbox_inches='tight', dpi=256)
    plt.close()

    
    # picname = picspath + '/' + "fig4_full.png"
    # # plt.tight_layout()
    # plt.savefig(picname, bbox_inches='tight', dpi=128)
    # plt.close()
    # plt.show()
    
if __name__ == '__main__':
    main()
