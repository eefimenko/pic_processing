#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils

def read_field2d(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def read_diagph(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def read_conc(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    return tmp[0]

def read_field(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def create_axis(n,step,x0=0):
    axis = []
    for i in range(n):
        axis.append(i*step + x0)
    return axis

def create_pulse(n,step,amp, tp, delay):
    dt = step
    ans = []
#    tp = 30
    tau = tp/1.1437
#    delay = 23.1099984185576458468467817
    for i in range(n):
        t = i*dt-delay
        if t > 0 and t < math.pi * tau:
            tmp = math.sin(t/tau)
            f = amp*tmp*tmp
        else:
            f = 0.
        ans.append(f)
    return ans

def get_color(j):
    color = ['r', 'g', 'b', 'k', 'c', 'y']
    if (j >= len(color)):
        j = j - len(color)
    return color[j]

def get_dirs_configs(array):
    cfgs = []
    dirs = []
    for j in range(1, len(array)):
        config = utils.get_config(array[j] + "/ParsedInput.txt")
        cfgs.append(config)
        dirs.append(array[j])
    return dirs, cfgs

def get_max_diagPh(dirs):
    n = []
    for path in dirs:
        n.append(utils.num_files(path + '/data/diagPh/'))
    return min(n)

def check_dt(configs):
    x0 = -1
    y0 = -1
    ans = 0
    for config in configs:
        if x0 == -1:
            x0 = float(config['BOIterationPass'])
            y0 = float(config['TimeStep'])
        else:
            x = float(config['BOIterationPass'])
            y = float(config['TimeStep'])
            if x != x0:
                print "BOIterationPass differs", x0, x
                ans = 1
            if y != y0:
                print "step differs", y0, y
                ans = 1
    return ans

def get_axis_and_field(path, config):
    pi = 3.14159
    ne = []
    nect = []
    power = []

    BOIterationPass = float(config['BOIterationPass'])
    step = float(config['TimeStep'])*1e15*float(config['BOIterationPass'])
    diag = float(config['npos.SetBounds_1'])
    ndiag = int(config['npos.SetMatrixSize_0'])
    step1 = diag/ndiag
    wl = float(config['Wavelength'])
    nepath = path + '/data/NeTrap'
    necpath = path + '/data/ne'
    ezpath = path +'/data/E2z'
    bzpath = path + '/data/B2z'
    peakpower = float(config['PeakPower'])*1e-7*1e-15
    v = 2. * math.pi * step1 * step1 * wl
    nmax = utils.num_files(nepath)
#    nmax = 20
    axis = create_axis(nmax, step)
    coeff = 10./(3.3e11*3.3e11)*0.97399
    dv = pi*wl*wl*0.25*wl*0.5
    nx = 512
    ny = 512
    print path, nmax
    for i in range(nmax):
        nename = nepath + '/' + "%06d.txt" % (i,)
        e = read_field(nename)
        ne.append(e)
        nename = necpath + '/' + "%06d.txt" % (i,)
        e = read_conc(nename) 
        for j in range(len(e)):
            e[j] = e[j]/((j+0.5)*v)
        nect.append(max(e))
        ezname = ezpath + '/' + "%06d.txt" % (i,)
        bzname = bzpath + '/' + "%06d.txt" % (i,)
        ez = read_field2d(ezname,nx,ny)
        bz = read_field2d(bzname,nx,ny)
        ezv = ez[nx/2][ny/2]
        bzv = bz[nx/2][ny/2-17]*1.53
        tmp = (ezv*ezv + bzv*bzv)*coeff
        if tmp < 1.1*peakpower:
            power.append(tmp)
        else:
            power.append(0)
    return axis, ne, nect, power

def sum_array(array):
    res = [0] * len(array[0])
    for i in range(len(array[0])):
        for j in range(len(array)):
            res[i] += array[j][i]
    return res

def sum_all(array):
    res = 0
    for i in range(len(array[0])):
        for j in range(len(array)):
            res += array[j][i]
    return res

def sum_arrayj(array, axis):
    res = [0] * len(array)
    for i in range(len(array[0])):
        for j in range(len(array)):
            res[j] += array[j][i]*axis[i]
    return res

def norm1(array, a=1.):
    maximum = max(array)
    for i,e in enumerate(array):
        array[i] = e/maximum*a
    return array

def get_diagph_spectrum(dirs, configs, i):
    picspath = 'cmp_pics'
    expath = '/data/diagPh/'
    fig = plt.figure(num=None)
    mp.rcParams.update({'font.size': 8})
#    ax1 = fig.add_subplot(1,2,1)
#    ax1.set_xlabel('Energy, GeV')
#    ax1.set_ylabel('Number of photons')
#    ax1.set_yscale('log') 
#    plt.text(0.1,0.9 * max(field[0]),'Photon spectra for theta = 0')
#    plt.ylim([2e-4, 1])
#    plt.xlim([0, 5])
#    ax2 = fig.add_subplot(1,2,2)
#    ax2.set_xlabel('Energy, GeV')
#    ax2.set_ylabel('Number of photons')
#    ax2.set_yscale('log') 
#    plt.text(0.1,0.6,'Photon spectra for theta = 90')
#    plt.ylim([2e-4, 1])
#    plt.xlim([0, 1.5])
    ax3 = fig.add_subplot(2,1,1)
    ax3.set_title('Photon spectra for all theta')
    ax3.set_xlabel('Energy, GeV')
    ax3.set_ylabel('Number of photons')
    ax3.set_yscale('log') 
#    plt.ylim([2e-4, 1e3])
    plt.xlim([0, 5])
    ax4 = fig.add_subplot(2,1,2)
    ax4.set_title('Radiation pattern')
    ax4.set_xlabel('Angle')
    ax4.set_ylabel('Photon energy')
    ax4.set_yscale('log') 
#    plt.ylim([2e-4, 1e3])
    plt.xlim([0, 90])
    nph = []
    for j in range(len(dirs)):
        config = configs[j]
        path = dirs[j] + '/' 
        ev = float(config['eV'])
        ang_max = float(config['diagPh.SetBounds_3'])/math.pi*180
        ang_min = float(config['diagPh.SetBounds_2'])
        en_max = float(config['diagPh.SetBounds_1'])/ev*1e-9
        en_min = float(config['diagPh.SetBounds_0'])/ev*1e-9
        en_num = int(config['diagPh.SetMatrixSize_0'])
        ang_num = int(config['diagPh.SetMatrixSize_1'])
        ang_step = (ang_max - ang_min)/ang_num
        en_step = (en_max - en_min)/en_num
    
        en_axis = create_axis(en_num, en_step, en_min)
        ang_axis = create_axis(ang_num, ang_step, ang_min)
    
        name = path + expath + "%06d.txt" % (i,)
        field = read_diagph(name,en_num,ang_num,1./en_step)
        if sum_all(field) == 0:
            print 'Skipped ' + name
            plt.close()
            return
        sum1 = sum_array(field)
        sumj1 = norm1(sum_arrayj(field, en_axis))
        nph.append(sum_all(field))      
#        p1, = ax1.plot(en_axis, field[0], get_color(j), label = path)
#        p1, = ax2.plot(en_axis, field[ang_num/2], get_color(j), label = path)
        p1, = ax3.plot(en_axis, sum1, get_color(j), label = path)
        p1, = ax4.plot(ang_axis, sumj1, get_color(j), label = path)
#    ax1.legend(loc='upper right', shadow=True)
#    ax2.legend(loc='upper right', shadow=True)
    ax3.legend(loc='upper right', shadow=True)
    ax4.legend(loc='upper right', shadow=True)
    picname = picspath + '/diagph_compare%d.png' % (i,)
    plt.savefig(picname)
    plt.close()
    print nph
    return nph

def main():
    leg = []
    axis = []
    ne = []
    nect = []
    power = []
    picspath = 'cmp_pics'
    dirs, configs = get_dirs_configs(sys.argv)
    equal = check_dt(configs)
        
#    if equal == 0:
#        for i in range(0,get_max_diagPh(dirs)):
#            print i
#            get_diagph_spectrum(dirs, configs, i)
#    else:
#        print "Config is different, no diagPh comparison"

    for j in range(len(dirs)):
        axis_tmp, ne_tmp, nect_tmp, power_tmp = get_axis_and_field(dirs[j], configs[j])
        axis.append(axis_tmp)
        ne.append(ne_tmp)
        nect.append(nect_tmp)
        power.append(power_tmp)
        leg.append(sys.argv[j])
       
    m = 0

    for j in range(len(ne)):
        if axis[j][-1] > m:
            m = axis[j][-1]
    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(3,1,1)
    
    ax1.set_title('Number of trapped electrons')
    for j in range(len(ne)):
        pe, = ax1.plot(axis[j], ne[j], get_color(j), label = dirs[j])

    ax1.legend(loc='lower right', shadow=True)
    ax1.set_yscale('log')
    ax1.set_xlim(0, m)
    
    ax2 = fig.add_subplot(3,1,2)
    
    ax2.set_title('Maximum concentration')
    for j in range(len(ne)):
        pe, = ax2.plot(axis[j], nect[j], get_color(j), label = dirs[j])

    ax2.legend(loc='lower right', shadow=True)
    ax2.set_yscale('log')
    ax2.set_xlim(0, m)
    plt.savefig(picspath + '/concentration_compare.png')

    ax3 = fig.add_subplot(3,1,3)
    
    ax3.set_title('Power')
    for j in range(len(ne)):
        pe, = ax3.plot(axis[j], power[j], get_color(j), label = dirs[j])

#    ax3.legend(loc='lower right', shadow=True)
#    ax3.set_yscale('log')
    ax3.set_xlim(0, m)
    plt.savefig(picspath + '/concentration_compare.png')
    plt.close()
    
    
if __name__ == '__main__':
    main()
