#!/usr/bin/python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import sys
import math
import utils
import matplotlib as mp

def read_power(filename):
    f = open(filename, 'r')
    power = []
    rest_power = []
    radiated_power = []
    normalized_power = []
    for line in f:
        tmp = line.split()
        power.append(float(tmp[0]))
        rest_power.append(float(tmp[0]) - float(tmp[1]))
        radiated_power.append(float(tmp[1]))
        normalized_power.append(float(tmp[1])/float(tmp[0]))
    return power, rest_power, radiated_power, normalized_power

def main():
    power, rest_power, radiated_power, normalized_power = read_power('power.dat')
#    fig = plt.figure(num=None, figsize=(20, 10), dpi=256)
    fig = plt.figure(figsize = (16,8))
    mp.rcParams.update({'font.size': 20})
    ax = fig.add_subplot(1,2,1)
#    print power
#    print rest_power
    int_rad = np.polyfit(power, radiated_power, 3)
    p_ = np.poly1d(int_rad)
    xp = np.linspace(power[0], power[-1], 100)
    p, = ax.plot(power, radiated_power, 'o', label = 'Radiated power')
    p, = ax.plot(xp, p_(xp), '-')
#    p, = ax.plot(power, rest_power, 'x', label = 'Field power')
#    plt.legend(loc = 'upper left')
    ax1 = fig.add_subplot(1,2,2)
    int_rad = np.polyfit(power, normalized_power, 4)
    p_ = np.poly1d(int_rad)
    p, = ax1.plot(power, normalized_power, 'o')
    p, = ax1.plot(xp, p_(xp), '-')
    ax.set_xlabel(u'Мощность, ПВт')
    ax.set_ylabel(u'Излучение, ПВт')
    ax1.set_xlabel(u'Мощность, ПВт')
    ax1.set_ylabel(u'Доля излучаемой\nэнергии')
    plt.tight_layout()
    plt.show()

if __name__ == '__main__':
    main()
