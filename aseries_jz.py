#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils
import numpy as np

def main():
  
    numdirs = int(sys.argv[1])
    dirs = []
    configs = []
    for i in range(numdirs):
        dirs.append(sys.argv[2+i] + '/')
        configs.append(utils.get_config(sys.argv[2+i] + "/ParsedInput.txt"))
    #delta = 1
    n = numdirs + 1
    #num = len(sys.argv) - n
    picspath = 'pics'
    
    '''if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = find_nmax(dirs, expath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1 + n] 
        nmin = int(sys.argv[1 + n])
        nmax = find_nmax(dirs, expath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1 + n] + ' to ' + sys.argv[2 + n]
        nmin = int(sys.argv[1 + n])
        nmax = int(sys.argv[2 + n])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1 + n] + ' to ' + sys.argv[2 + n] + ' with delta ' + sys.argv[3 + n]
        print sys.argv[1 + n]
        nmin = int(sys.argv[1 + n])
        nmax = int(sys.argv[2 + n])
        delta = int(sys.argv[3 + n])
    else:
        nmin = 0
        nmax = 0
        print 'Too much parameters'''
    nmin, nmax, delta = utils.get_min_max_iteration(sys.argv,dirs[0] + '/' + utils.ezpath, n=n)
    nmin = 400
    nmax = 500
    print nmin, nmax, delta
    
    xmin = []
    xmax = []
    ymin = []
    ymax = []
    zmin = []
    zmax = []
    mult = []
    nx = []
    ny = []
    nz = []
    power = []
    for k in range(numdirs):
        wl = float(configs[k]['Wavelength'])
        xmax.append(float(configs[k]['X_Max'])/wl) #mkm
        xmin.append(float(configs[k]['X_Min'])/wl) #mkm
        ymax.append(float(configs[k]['Y_Max'])/wl) #mkm
        ymin.append(float(configs[k]['Y_Min'])/wl) #mkm
        zmax.append(float(configs[k]['Z_Max'])/wl) #mkm
        zmin.append(float(configs[k]['Z_Min'])/wl) #mkm
        nx.append(int(configs[k]['MatrixSize_X']))
        ny.append(int(configs[k]['MatrixSize_Y']))
        nz.append(int(configs[k]['MatrixSize_Z']))
        power.append(int(configs[k]['PeakPowerPW']))
        dx = float(configs[k]['Step_X'])/wl
        dy = float(configs[k]['Step_Y'])/wl
        dz = float(configs[k]['Step_Z'])/wl
        mult.append(1/(2.*dx*dy*dz*1e-12))
        step = float(configs[k]['TimeStep'])*1e15*float(configs[k]['BOIterationPass'])
        omega = float(configs[k]['Omega'])
        T = 2 * math.pi/omega
        nt = int(T*1e15/step)
        print nt

    figures = [utils.ezpath, utils.bzpath, utils.nezpath, utils.jz_zpath]
    cmaps = ['Reds', 'Reds', 'Greens', 'bwr']
    titles = ['Electric field %d pw', 'Magnetic field %d pw', 'Electrons %d pw', 'J_z %d pw']
    log = [False, False, True, False]
    mult = [1,1,mult[0],3.33564e-10]

#    n0 = [703, 0, 0] # 675
#    n0 = [686, 0, 0] # 680
#    n0 = [1030, 1090]
    n0 = [0, 1300, 800]
    spx = numdirs
#    n0 = [0] * numdirs 
    spy = len(figures)
    
    for i in range(nmin,nmax,delta):
        print "\rSaving sc%06d.png, %.0f %% done" % (i,float(i-nmin)/(nmax-nmin)*100),
        sys.stdout.flush()

        fig = plt.figure(num=None, figsize=(20., 20.*spx/spy), dpi=256)
        mp.rcParams.update({'font.size': 16})
        
        for k in range(spx):
            for j in range(spy):
                ylabel = ''
                yticks = []
                if j == 0:
                    ylabel = '$y/\lambda$'
                    yticks = [-0.5,-0.25,0,0.25,0.5]
                utils.subplot(fig, i+n0[k], dirs[k]+figures[j],
                              shape = (nx[k],ny[k]), position = (spx,spy,j+1+k*spy),
                              extent = [xmin[k], xmax[k], ymin[k], ymax[k]],
                              cmap = cmaps[j], title = '', #titles[j] % (power[k]),
                              colorbar = True, logarithmic=log[j], verbose=0,
                              xlim = [-0.5,0.5], ylim = [-0.5,0.5], xticks = [-0.5,-0.25,0,0.25,0.5], yticks = yticks,
                              xlabel = '$x/\lambda$', ylabel = ylabel, fontsize=16, mult = mult[j])
                      
        picname = picspath + '/' + "jscz%06d.png" % (i,)
#        plt.tight_layout()
        plt.savefig(picname)
        plt.close()

    
if __name__ == '__main__':
    main()
