#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import math
import utils
import sys
import scipy.ndimage as ndimage
k = 0
LightVelocity = 2.99792e+10
Omega = 1

def R(x,y,z):
    R = math.sqrt(x*x + y*y + z*z)
    return R

def exf(x,y,z):
    r = R(x,y,z)
    if r < 0.0001:
        r = 0.0001
    return x*z/(r*r) * f2(r)*2.

def eyf(x,y,z):
    r = R(x,y,z)
    if r < 0.0001:
        r = 0.0001
    return y*z/(r*r) * f2(r)*2.

def ezf(x,y,z):
    r = R(x,y,z)
    if r < 0.0001 :
        r = 0.0001
    return 2. * (f3(r) + z*z/(r*r)*f2(r))
  
def bxf(x,y,z):
    r = R(x,y,z)
    if r < 0.0001 :
        r = 0.0001
    return y/r * 2. * f1(r)

def byf(x,y,z):
    r = R(x,y,z)
    if r < 0.0001 :
        r = 0.0001
    return -x/r * 2. * f1(r)
    
def f1(r):
    return -k*k/r * math.cos(k*r) + k/(r*r) * math.sin(k*r)

def f2(r):
    return (-k*k/r + 3./(r*r*r))*math.sin(k*r) - 3.*k/(r*r)*math.cos(k*r)

def f3(r):
    return (k*k/r - 1./(r*r*r))*math.sin(k*r) + k/(r*r)*math.cos(k*r)

def Sin(r,t,k):
    return math.sin(k*r + Omega*t)

def Cos(r,t,k):
    return math.cos(k*r + Omega*t)

def Ex(x,y,z,R,t,k):
    return z * x / (R*R*R) * ( Sin(R,t,k)*(-1 + 3./(k*k*R*R)) - Cos(R,t,k)*3./(k*R))

def Ey(x,y,z,R,t,k):
    return z * y / (R*R*R) * ( Sin(R,t,k)*(-1 + 3./(k*k*R*R)) - Cos(R,t,k)*3./(k*R))

def Ez(x,y,z,R,t,k):
    return -( (x*x + y*y)/(R*R*R) * Sin(R,t,k) + 1./(k*k*R*R*R) * (-1. + 3.*z*z/(R*R))*Sin(R,t,k) + 1./(k*R*R) * (1. - 3.*z*z/(R*R)) * Cos(R,t,k) )

def Bx(x,y,z,R,t,k):
    return - y /(R*R) * (Sin(R,t,k) + 1./(k*R)*Cos(R,t,k))

def By(x,y,z,R,t,k):
    return - x /(R*R) * (Sin(R,t,k) + 1./(k*R)*Cos(R,t,k))

def create_analitycal_yz(nx, ny, xmin, dx, ymin, dy):
    n = 12
    field = []
    for j in range(ny):
        row = []
        z = xmin + j*dx
        for i in range(nx):
            y = ymin + i*dy
            ex = exf(0.,y,z)
            ey = eyf(0.,y,z)
            ez = ezf(0.,y,z)
            if i < n or i > nx-n or j < n or j > ny-n:
                row.append(0)
            else:
                row.append(math.sqrt(ex*ex+ey*ey+ez*ez))
        field.append(row)
    return norm(field)

def create_analitycal_xy(nx, ny, xmin, dx, ymin, dy, phase, a0=1.):
    n = 12
    fielde = []
    fieldb = []
    s = abs(math.sin(phase))
    c = abs(math.cos(phase))
    for j in xrange(ny):
        rowe = []
        rowb = []
        x = xmin + j*dx
        for i in xrange(nx):
            y = ymin + i*dy
            ex = exf(x,y,0)
            ey = eyf(x,y,0)
            ez = ezf(x,y,0)
            bx = bxf(x,y,0)
            by = byf(x,y,0)
            
            if i < n or i > nx-n or j < n or j > ny-n:
                rowe.append(0)
                rowb.append(0)
            else:
                rowe.append(math.sqrt(ex*ex+ey*ey+ez*ez))
                rowb.append(math.sqrt(bx*bx+by*by))
        fielde.append(rowe)
        fieldb.append(rowb)
    print a0
    return norm2d(fielde, a0*s), norm2d(fieldb, a0*c/1.533)

def create_analitycal_x(nx, xmin, dx, phase, a0=1.):
    n = 12
    fielde = np.zeros(nx)
    fieldb = np.zeros(nx)
    s = abs(math.sin(phase))
    c = abs(math.cos(phase))
    c1 = a0*s
    c2 = a0*c/1.533
    for i in xrange(nx):
        x = xmin + i*dx
        
        ex = exf(x,0,0)
        ey = eyf(x,0,0)
        ez = ezf(x,0,0)
        bx = bxf(x,0,0)
        by = byf(x,0,0)
            
        if i >= n and i < nx-n:
            fielde[i] = math.sqrt(ex*ex+ey*ey+ez*ez)
            fieldb[i] = math.sqrt(bx*bx+by*by)
    
    return norm(fielde, c1), norm(fieldb, c2)

def norm2d(a, a0 = 1.):
    m = max2d(a)
    if m != 0:
        for i in range(len(a)):
            for j in range(len(a[0])):
                a[i][j] *= a0/m
    return a

def norm(a, a0 = 1.):
    m = max(a)
    if m != 0:
        for i in range(len(a)):
            a[i] *= a0/m
    return a

    
def max2d(array):
    return max([max(x) for x in array])    

def average_ne(array):
    n = 10
    m = len(array)/2
    prof = np.zeros(len(array[0]))
    for i in range(len(array[0])):
        a = 0.
        for j in range(n):
            a += array[m+j-n/2][i]
        prof[i] = a/n
    return prof

def process_one_image(i, nx, ny, mult, Omega, dt, t0, ppw, T, a00, Xmin, dx, axis, v_1, axis1, picspath):
    phase = Omega*i*dt + t0
    nex = len(axis1)
    fig = plt.figure(num=None, figsize=(20, 10), dpi=256)
    ax = fig.add_subplot(1,1,1)
    ax.set_title('P= %.1lf PW, t = %.2lf T'%(ppw, i*dt/T))
    ax1 = ax.twinx()
#    print nx,ny
    fieldex = utils.bo_file_load(utils.ezpath,i,nx,ny)
    fieldbx = utils.bo_file_load(utils.bzpath,i,nx,ny)
    ne_z = utils.bo_file_load(utils.nezpath,i,nx,ny,mult=mult)
#    els = utils.bo_file_load(utils.nepath,i,nex)*v_1
#    for j in range(nex):
#        els[j] /= (j+0.5)
    fex_a, fbx_a  = create_analitycal_x(nx, Xmin, dx, phase, a00)
    e, = ax.plot(axis, fieldex[nx/2], 'r')
    
    m1 = max([max(fieldbx[nx/2]), max(fbx_a), max(fieldex[nx/2]), max(fex_a), a00])
    es, = ax.plot(axis, fex_a, 'r--')
    b, = ax.plot(axis, fieldbx[nx/2], 'b')
    bs, = ax.plot(axis, fbx_a, 'b--')
    ax.set_xlim([-1,1])
    ax.set_ylim([0,1.1*m1])
    ax.set_xlabel('y/lambda')
    ax.set_ylabel('E,B, CGS')
    ax1.set_ylabel('Electrons density, cm-3')
    prof = average_ne(ne_z)
    ne, = ax1.plot(axis,prof, 'g')
#    ne1, = ax1.plot(axis1,els, 'g--')
#    ne1, = ax1.plot(-np.array(axis1),els, 'g--')
    ax1.set_xlim([-1,1])
#    plt.legend([e,es,b,bs,ne,ne1], ['E', 'standing wave E', 'B', 'standing wave B', 'Electrons', 'Concentration'])
    plt.legend([e,es,b,bs,ne], ['E', 'standing wave E', 'B', 'standing wave B', 'Electrons'])
    picname = picspath + '/' + "field_cmp_%d.png" % i
    print picname
    plt.savefig(picname)
    plt.close()



def main():
    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    picspath = 'pics'

    config = utils.get_config("ParsedInput.txt")

    BOIterationPass = float(config['BOIterationPass'])
    dt = float(config['TimeStep'])*1e15*BOIterationPass #fs
    maxpower = float(config['PeakPower'])*1e-7*1e-15 #PW
    
    omega = float(config['Omega'])
    dt = float(config['TimeStep'])*BOIterationPass
    T = 2 * math.pi/omega
    print "Power = " + str(maxpower) + 'PW'
    print "dt = " + str(dt) + 'fs'
    wl = float(config['Wavelength'])*1e4
    global k
    global Omega
    Omega = float(config['Omega'])
    k = 2*math.pi/wl
    ppw = int(config['PeakPower'])*1e-22
    a00 = 3.e11 * math.sqrt(ppw/10.)
    print a00
    Xmax = float(config['X_Max'])*1e4 #mkm
    Xmin = float(config['X_Min'])*1e4 #mkm
    Ymax = float(config['Y_Max'])*1e4 #mkm
    Ymin = float(config['Y_Min'])*1e4 #mkm
    Zmax = float(config['Z_Max'])*1e4 #mkm
    Zmin = float(config['Z_Min'])*1e4 #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    num = len(sys.argv)
    delta = 1

    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(utils.ezpath) 
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(utils.ezpath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'

    if nx == 256:
        t0 = 1.5609
    else:
        t0 = 1.54
    print t0
    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz
    dv = dx*dy*dz*1e-12
    mult = 1./(2.*dv)
    axis = utils.create_axis(nx,dx/wl, Xmin/wl)
#    diag = float(config['ne.SetBounds_1'])*1e4
#    ndiag = int(config['ne.SetMatrixSize_0'])
    
#    step1 = diag/ndiag
#    v_1 = 1./(2. * math.pi * step1 * step1 * 0.1*wl* 1e-12) 
#    axis1 = utils.create_axis(ndiag, step1/wl,0)
    
    for i in range(nmin,nmax, delta):
        process_one_image(i, nx, ny, mult, Omega, dt, t0, ppw, T, a00, Xmin, dx, axis, 1, axis, picspath)
'''        fig = plt.figure(num=None, figsize=(20, 10), dpi=256)
        ax = fig.add_subplot(1,1,1)
        ax.set_title('P= %.1lf PW, t = %.2lf T'%(ppw, i*dt/T))
        ax1 = ax.twinx()
        fieldex = utils.bo_file_load(ezpath,i,nx,ny)
        fieldbx = utils.bo_file_load(bzpath,i,nx,ny)

        ne_z = utils.bo_file_load(utils.nezpath,i,nx,ny,mult=mult)
        phase = Omega*i*dt + t0
        fex_a, fbx_a  = create_analitycal_x(nx, Xmin, dx, phase, a00)
        e, = ax.plot(axis, fieldex[nx/2], 'r')

        m1 = max([max(fieldbx[nx/2]), max(fbx_a), max(fieldex[nx/2]), max(fex_a), a00])
        es, = ax.plot(axis, fex_a, 'r--')

        b, = ax.plot(axis, fieldbx[nx/2], 'b')
        bs, = ax.plot(axis, fbx_a, 'b--')
        ax.set_xlim([-1,1])
        ax.set_ylim([0,1.1*m1])
        ax.set_xlabel('y/lambda')
        ax.set_ylabel('E,B, CGS')
        ax1.set_ylabel('Electrons density, cm-3')
        prof = average_ne(ne_z)
        ne, = ax1.plot(axis,prof, 'g')
        ax1.set_xlim([-1,1])
        plt.legend([e,es,b,bs,ne], ['E', 'standing wave E', 'B', 'standing wave B', 'Electrons'])
        picname = picspath + '/' + "field_cmp_%d.png" % i
        print picname
        plt.savefig(picname)
        plt.close()'''

if __name__ == '__main__':
    main()
