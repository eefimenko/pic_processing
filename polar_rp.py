#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import mpl_toolkits
import numpy as np
import utils
import sys
from mpl_toolkits.mplot3d import Axes3D
from pylab import *

def read_field(file,nx,ny,mult=1.):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index]*mult)
        field.append(row)
    return np.array(field)

def average_energy(up, down, energy):
    ans = [[0 for x in range(len(up[0]))] for x in range(len(up))]
    for i in range(len(up)):
        for j in range(len(up[0])):
            summ = up[i][j]+down[i][j]
            if summ > 0:
                ans[i][j] = energy[i][j]/summ
            else:
                ans[i][j] = 0
    return ans
 
def sum_phi(array):
    nx = len(array)
    ny = len(array[0])
    res = [0] * nx
    for i in range(nx):
        for j in range(ny):
            res[i] = res[i] + array[i][j]
    m = max(res)
    if m > 0:
        for i in range(nx):
            res[i] = res[i]/m
    return res

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append((i+0.5)*step - math.pi*0.5)
    return axis

def main():
    path = sys.argv[1]
    nmin = int(sys.argv[2])
    nmax = int(sys.argv[3])
    picspath = 'pics'
    angpath = '/statdata/ph/angSpSph/'
    config = utils.get_config(path + "/ParsedInput.txt")
    ev = float(config['eV'])
    phi = int(config['QEDstatistics.OutputN_phi'])
    theta = int(config['QEDstatistics.OutputN_theta'])
    print path + angpath + '%.4f.txt' % (nmin,)

    array = read_field(path + angpath + '%.4f.txt' % (nmin,), phi, theta)
    for j in range(nmin+1,nmax):
        array += read_field(path + angpath + '%.4f.txt' % (j,), phi, theta)
    summ_en = sum_phi(array)
    th_ax = create_axis(theta,2*math.pi/theta)
    fig = plt.figure(num=None)
    mp.rcParams.update({'font.size': 8})
    ax = fig.add_subplot(1,2,1)
    surf = ax.imshow(array, extent=[0, 1, 0, 2])
    plt.colorbar(surf, orientation  = 'vertical')
    ax = fig.add_subplot(1,2,2, polar=True)
    ax.plot(th_ax, summ_en)
    
    plt.show()
if __name__ == '__main__':
    main()
