#!/usr/bin/python
from matplotlib.colors import colorConverter
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
from mathgl import *

Xmax = 4
Xmin = -4
Ymax = 4
Ymin = -4
nx = 512
ny = 512
step = 8./nx
spx = 2
spy = 3

def read_field(file,nx,ny):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def find_max(file):
    max = 0
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    array = tmp[0]
    for p in array:
        if p > max:
            max = p
    return max

def find_max_from_all_fields(expath, eypath, ezpath, bxpath, bypath, bzpath, i):
    max = 0
    name = expath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = eypath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = ezpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    return max

def find_max_from_all_particles(nexpath, neypath, nezpath, npxpath, npypath, npzpath, i):
    max = 0
    name = nexpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = neypath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = nezpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = npxpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = npypath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = npzpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    return max

def create_combined_subplot(fig, path1, path2, i, spi, v1_min, v1_max, v2_min, v2_max, xlabel, ylabel):
    global Xmin, Xmax, Ymin, Ymax, spx, spy, nx, ny
    v = step*step*step*1e-12*2
    ax = fig.add_subplot(spx,spy,spi)
    name = path1 + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny)
#    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap='hot_r', vmin = v1_min, vmax = v1_max, norm=clr.LogNorm())
    fig.hold(True)
    name = path2 + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny)
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Greens', alpha = 1, vmin = v2_min, vmax = v2_max, norm=clr.LogNorm())
    maxv = max([max(row) for row in field])/v
    plt.text(-3.5,3,str('%.2e' % maxv) , fontsize = 10)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    return surf

def main():
    global Xmin, Xmax, Ymin, Ymax, spx, spy, nx, ny, step
    edipole = '35PW/'
    mdipole = 'corrected/40pwt_e/'
    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
#    expath = 'data/Proton2Dx'
#    eypath = 'data/Proton2Dy'
#    ezpath = 'data/Proton2Dz'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    picspath = 'pics'

    maxn = 16612.7
    maxf = 0
#    for i in range(nmax):
#    i = 20
    nmin = 0
    nmax = 80
    for i in range(nmin,nmax):
        print "%06d.txt" % (i,)
        
        e_expath = edipole + expath
        e_eypath = edipole + eypath
        e_ezpath = edipole + ezpath
        e_bxpath = edipole + bxpath
        e_bypath = edipole + bypath
        e_bzpath = edipole + bzpath
        tmpf = find_max_from_all_fields(e_expath, e_eypath, e_ezpath, e_bxpath, e_bypath, e_bzpath, i)
        if tmpf > maxf:
            maxf = tmpf
        m_expath = mdipole + expath
        m_eypath = mdipole + eypath
        m_ezpath = mdipole + ezpath
        m_bxpath = mdipole + bxpath
        m_bypath = mdipole + bypath
        m_bzpath = mdipole + bzpath
        tmpf = find_max_from_all_fields(m_expath, m_eypath, m_ezpath, m_bxpath, m_bypath, m_bzpath, i)
        if tmpf > maxf:
            maxf = tmpf
        e_nexpath = edipole + nexpath
        e_neypath = edipole + neypath
        e_nezpath = edipole + nezpath
        e_npxpath = edipole + npxpath
        e_npypath = edipole + npypath
        e_npzpath = edipole + npzpath
        tmpn = find_max_from_all_particles(e_nexpath, e_neypath, e_nezpath, e_npxpath, e_npypath, e_npzpath, i)
        if tmpn > maxn:
            maxn = tmpn
        m_nexpath = edipole + nexpath
        m_neypath = edipole + neypath
        m_nezpath = edipole + nezpath
        m_npxpath = edipole + npxpath
        m_npypath = edipole + npypath
        m_npzpath = edipole + npzpath
        tmpn = find_max_from_all_particles(m_nexpath, m_neypath, m_nezpath, m_npxpath, m_npypath, m_npzpath, i)
        if tmpn > maxn:
            maxn = tmpn
    
    v1_min = maxf*1e-3
    v1_max = maxf
    v2_min = 16612.7
    v2_max = maxn
    nmin = 0
    nmax = 80
    for i in range(nmin,nmax):
        fig = plt.figure(num=None, figsize=(30, 20), dpi=256)
        mp.rcParams.update({'font.size': 12})
        e_expath = edipole + expath
        e_nexpath = edipole + nexpath
        create_combined_subplot(fig, e_expath, e_nexpath, i, 1, v1_min, v1_max, v2_min, v2_max, 'z', 'y')
#        plt.colorbar(s, orientation  = 'vertical')
        e_eypath = edipole + eypath
        e_neypath = edipole + neypath
        create_combined_subplot(fig, e_eypath, e_neypath, i, 2, v1_min, v1_max, v2_min, v2_max, 'z', 'x')
        e_ezpath = edipole + ezpath
        e_nezpath = edipole + nezpath
        create_combined_subplot(fig, e_ezpath, e_nezpath, i, 3, v1_min, v1_max, v2_min, v2_max, 'y', 'x')
        m_expath = mdipole + expath
        m_nexpath = mdipole + nexpath
        create_combined_subplot(fig, m_expath, m_nexpath, i, 4, v1_min, v1_max, v2_min, v2_max, 'z', 'y')
        m_eypath = mdipole + eypath
        m_neypath = mdipole + neypath
        create_combined_subplot(fig, m_eypath, m_neypath, i, 5, v1_min, v1_max, v2_min, v2_max, 'z', 'x')
        m_ezpath = mdipole + ezpath
        m_nezpath = mdipole + nezpath
        create_combined_subplot(fig, m_ezpath, m_nezpath, i, 6, v1_min, v1_max, v2_min, v2_max, 'y', 'x')
        picname = picspath + '/' + "cmp_el_%06d.png" % (i,)
        print picname
        plt.savefig(picname)
        plt.close()
#    for i in range(nmax):
#    i = 20
    nmin = 0
    nmax = 0
    maxf = 0
    maxn = 0
    for i in range(nmin,nmax):
        print "%06d.txt" % (i,)
        maxf = 0
        maxn = 0
        tmpf = find_max_from_all_fields(expath, eypath, ezpath, bxpath, bypath, bzpath, i)
        if tmpf > maxf:
            maxf = tmpf
        tmpn = find_max_from_all_particles(nexpath, neypath, nezpath, npxpath, npypath, npzpath, i)
        if tmpn > maxn:
            maxn = tmpn
    
    for i in range(nmax):
        print "pic%06d.png" % (i,)
        fig = plt.figure(num=None, figsize=(40, 40), dpi=256)
        mp.rcParams.update({'font.size': 16})
        
        s = create_subplot(fig,i,expath,nx,ny,3,3,1, Xmin, Xmax, Ymin, Ymax, tmpf/1e4, tmpf)
        plt.colorbar(s, orientation  = 'vertical')
        
        s = create_subplot(fig,i,eypath,nx,ny,3,3,4, Xmin, Xmax, Ymin, Ymax, tmpf/1e4, tmpf)
        plt.colorbar(s, orientation  = 'vertical')
        
        s = create_subplot(fig,i,ezpath,nx,ny,3,3,7, Xmin, Xmax, Ymin, Ymax, tmpf/1e4, tmpf)
        plt.colorbar(s, orientation  = 'vertical')
        
        s = create_subplot(fig,i,nexpath,nx,ny,3,3,2, Xmin, Xmax, Ymin, Ymax, tmpn/1e4, tmpn)
        plt.colorbar(s, orientation  = 'vertical')
        
        s = create_subplot(fig,i,neypath,nx,ny,3,3,5, Xmin, Xmax, Ymin, Ymax, tmpn/1e4, tmpn)
        plt.colorbar(s, orientation  = 'vertical')
        
        s = create_subplot(fig,i,nezpath,nx,ny,3,3,8, Xmin, Xmax, Ymin, Ymax, tmpn/1e4, tmpn)
        plt.colorbar(s, orientation  = 'vertical')
        
        s = create_subplot(fig,i,npxpath,nx,ny,3,3,3, Xmin, Xmax, Ymin, Ymax, tmpn/1e4, tmpn)
        plt.colorbar(s, orientation  = 'vertical')
        
        s = create_subplot(fig,i,npypath,nx,ny,3,3,6, Xmin, Xmax, Ymin, Ymax, tmpn/1e4, tmpn)
        plt.colorbar(s, orientation  = 'vertical')
        
        s = create_subplot(fig,i,npzpath,nx,ny,3,3,9, Xmin, Xmax, Ymin, Ymax, tmpn/1e4, tmpn)
        plt.colorbar(s, orientation  = 'vertical')
        picname = picspath + '/' + "pic%06d.png" % (i,)
        plt.savefig(picname)
        plt.close()
#        plt.show()
if __name__ == '__main__':
    main()
