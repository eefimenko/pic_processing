#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import shutil
import os
import scipy.signal.signaltools as sigtool
import numpy 

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    return sum(tmp1)

def read_file_sph(file, de, de1, emin=0.):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
        
    for i in range(len(array)):
        if (i+0.425)*de > emin:
            nph  += array[i]/((i+0.425)*de)
   
    return nph

def read_one_spectrum_sph(path1, e=0., nmin=0):
    config = utils.get_config(path1 + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV'])
    n0 = float(config['Ne']) 
    T = 2 * math.pi/omega*1e15
    num = utils.num_files(path1 + utils.enspsphpath)
    print 'Found ' + str(num) + ' files'
    nph_t = []
    ntr_t = []
    axis = []
    axis1 = []
    step = x0*y0*1e15 
    emin = float(config['QEDstatistics.Emin']) 
    emax = float(config['QEDstatistics.Emax']) 
    ne = int(config['QEDstatistics.OutputN_E'])
    de = (emax - emin)/ne/ev/1e9
    de1 = (emax - emin)/ne
    print ev*1e9, 1/(ev*1e9)
    shift = -15
    nn = 0
    print nmin, nmin+num, e
    for i in range(nmin,nmin+num):
        name = path1 + utils.enspsphpath + '%.4f.txt'%i
        print name
        ntrapped = read_trap(path1 + utils.nepath + '%06d.txt' % (i,))
        nph = read_file_sph(name, de, de1, e)
        nn += nph
        nph_t.append(nn)
        ntr_t.append(ntrapped)
        axis.append(i*step)
        axis1.append((i-shift)*step)
    pp = 'photons_new'
    if 'pulse' in path1:
        f = open(pp + '/pulse_%dpw_%.0le_%dgev.dat'%(ppw, n0, int(e)), 'w')
        label = 'pulse_%d_n0_%.0le'%(ppw, n0)
    else:
        f = open(pp + '/semiinf_%dpw_%dgev.dat'%(ppw, int(e)), 'w')
        label = 'semiinf_%d_n0_%.0le'%(ppw, n0)
    for i in range(len(nph_t)):
        f.write("%lf %le %le\n"%(axis[i],nph_t[i], ntr_t[i]))
    f.close()
    return axis, nph_t, axis1, ntr_t, label

def main():
    picspath = 'pics'
    
    mp.rcParams.update({'font.size': 16})
    fig = plt.figure(num=None, figsize=(20, 10))
    ax1 = fig.add_subplot(2,1,1)
    ax2 = fig.add_subplot(2,1,2)
    for k in range(1,len(sys.argv)):
        
        path = sys.argv[k]
        axis, nph_t, axis1, ntr_t, label = read_one_spectrum_sph(path, 0.)
        ax1.plot(axis, nph_t, label = label)
        ax1.set_yscale('log')
        ax1.set_xlim([0, 120])
        ax1.set_ylim([1e-10, 1e12])
        ax3 = ax1.twinx()
        ax3.plot(axis1, ntr_t, 'r', label = label)
        ax3.set_yscale('log')
        ax3.set_ylim([1e-10, 1e12])
        ax3.set_xlim([0, 120])
        axis, nph_t, axis1, ntr_t, label = read_one_spectrum_sph(path, 1.)
        ax2.plot(axis, nph_t, label = label)
        ax2.set_yscale('log')
        ax2.set_xlim([0, 120])
        axis, nph_t, axis1, ntr_t, label = read_one_spectrum_sph(path, 2.)
        ax2.plot(axis, nph_t, label = label)
        ax2.set_yscale('log')
        ax2.set_xlim([0, 120])
        axis, nph_t, axis1, ntr_t, label = read_one_spectrum_sph(path, 3.)
        ax2.plot(axis, nph_t, label = label)
        ax2.set_yscale('log')
        ax2.set_xlim([0, 120])

    plt.legend()
    plt.savefig('photons/' + label + '.png')

   
if __name__ == '__main__':
    main()
