#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils

def read_file(file, de):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
    if 'tr' in file and not 'fs' in file:
        array[0] = 0
        print 'removed'
    for i in range(len(array)):
        nph  += array[i]/(i+0.5)/de
    for i in range(len(array)):
#        array[i] /= (i+0.5)
#        array[i] /= de*(i+0.5)*de
        array[i] /= de
   
    return array, nph

def check_length(file):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
       
    return len(array)

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def read_one_spectrum(path1, nmin):
    config = utils.get_config(path1 + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV']) 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
    print num, T
    nph_t = []
    nph1_t = []
    ne_t = []
    np_t = []
    step = x0*y0*1e15/T 
    
    dt = y0*float(config['QEDstatistics.OutputIterStep'])*1e15

    path = '/statdata/ph/EnSpSph/'
    nepath = '/data/NeTrap/'
    nppath = '/data/NposTrap/'
    emin = float(config['QEDstatistics.Emin']) 
    emax = float(config['QEDstatistics.Emax']) 
    ne = int(config['QEDstatistics.OutputN_E'])
    de = (emax - emin)/ne
   
    t = 0
    n = check_length(path1 + path + '%.4f.txt'%nmin)
    axis = create_axis(n, de/ev*1e-9)

    spectrum1 = [0]*n
    nn = 0
    for i in range(nmin,nmin+num):
        name = path1 + path + '%.4f.txt'%i
        print name
        sp1, nph = read_file(name, de)
        nn += nph
        for k in range(n):
            spectrum1[k] += sp1[k]
    for k in range(n):
            spectrum1[k] /= nn
    return spectrum1, axis

def main():
    picspath = 'pics'
    num = len(sys.argv) - 1
    n = num/2
    sp = []
    ax = []
    for k in range(n):
        spectrum, axis = read_one_spectrum(sys.argv[2*k+1], int(sys.argv[2*k+2]))
        sp.append(spectrum)
        ax.append(axis)

    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(1,1,1)
    maxn = []
    for k in range(n):
        maxn.append(max(sp[k]))
    m = max(maxn)
    for k in range(n):
#        for n in range(len(sp[k])):
#            sp[k][n] /= m
        de = ax[k][1] - ax[k][0]
        ax1.plot(ax[k], sp[k], label = sys.argv[2*k+1])
        print sys.argv[2*k+1], sum(sp[k])*de, sp[k][0]
#    ax1.plot(axis, spectrum2)
#    ax1.set_yscale('log')
    ax1.set_xlim([0, 2])
#    ax1.set_ylim([1e2, 1e5])
    ax1.legend(loc='upper right', shadow=True)
#    plt.close()
    plt.show()
    
if __name__ == '__main__':
    main()
