#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils
import numpy as np
import glob
import pickle
import os

def read_weights(path, iteration, ptype = 'el', prefix = 'weights', bins = 100, logbins = False, logfactor = 1.1, step = None, processes = None, nprocesses = None):
    picklename = '%s/weights/%s/%s_%d.pkl' % (path,ptype,prefix,iteration)
    wmin = 1e9
    wmax = 0.
    
    if os.path.exists(picklename):
        with open(picklename) as f:
            axis = pickle.load(f)
            weights = pickle.load(f)
    else:
        if processes == None:
            filelist = glob.glob('%s/weights/%s/*_%d.txt' % (path, ptype, iteration))
        else:
            filelist = []
            for i in processes:
                for j in processes:
                    for k in processes:
                        num = i + j*8 + k*8*8
                        filelist.append('%s/weights/%s/%d_%d.txt' % (path, ptype, num, iteration))
            
        for filename in filelist:
            print filename
            with open(filename) as f:
                content = f.readline().split()
                for item in content:
                    val = float(item)
                    if val > wmax:
                        wmax = val
                    if val < wmin:
                        wmin = val

        if logbins:
            nbins = int(math.log(wmax/wmin)/math.log(logfactor))+1
            axis = np.zeros(nbins)
            step = np.zeros(nbins)
            factor = wmin
            for i in range(nbins):
                axis[i] = factor
                step[i] = (logfactor - 1.) * factor
                factor *= logfactor
        else:
            if step is None:
                nbins = bins
            else:
                nbins = int(wmax/step) + 1
            
            axis = np.zeros(nbins)
            dw = wmax/(nbins - 1)
            for i in range(nbins):
                axis[i] = dw * i
        
        weights = np.zeros(nbins)
        
        count = 0
        if logbins:
            for filename in filelist:
                print filename
                with open(filename) as f:
                    content = f.readline().split()
                    for item in content:
                        val = float(item)
                        idx = int(math.log(val/wmin)/math.log(logfactor))
                        weights[idx] += 1
                        count += 1
        else:
            for filename in filelist:
                print filename
                with open(filename) as f:
                    content = f.readline().split()
                    for item in content:
                        val = float(item)
                        idx = int(val/dw)
                        weights[idx] += 1
                        count += 1
                    
        #weights /= float(count)
        
        #if logbins:
        #    weights /= step
                       
        with open(picklename, 'w') as f:
            pickle.dump(axis, f)
            pickle.dump(weights, f)
            
    return axis, weights


def smooth_tail(array, boundary = 25, window = 15):
    for i in range(boundary, len(array)):
        array[i] = np.sum(array[i-window/2:i+window/2])/window
    return array

def main():
    linear = True
    picspath = '/home/evgeny/Dropbox/pinch_thinout'
    ptype = 'ph'
    
    if linear:
        dirs = ['27pw_default', '27pw_conservation', '27pw_merge', '27pw_energy', '27pw_number', '27pw_leveling', '27pw_simple']
        #dirs = ['27pw_merge']
        iterations = [2600]
        ratios = [1e-3]
        titles = ['globalLev', 'conserve', 'merge', 'energy', 'number', 'leveling', 'simple']
        filename = "cmp_pinch_thinout_weight_%s.png" % ptype
    else:
        dirs = ['27pw_default', '27pw_conservation', '27pw_merge', '27pw_energy', '27pw_number', '27pw_leveling']
        iterations = [3200]
        ratios = [1e-3]
        titles = ['globalLev', 'conserve', 'merge', 'energy', 'number', 'leveling']
        filename = "cmp_pinch_thinout_weight_%s_nl.png" % ptype
    configs = []
    numdirs = len(dirs)
    
    mp.rcParams.update({'font.size': 12})
    fig1 = plt.figure(num=None, figsize = (5,3.5))
    ax1 = fig1.add_subplot(1,1,1)
    max_factor = 0
    lw = 1
    
    for i in range(numdirs):
        config = utils.get_config(dirs[i] + "/ParsedInput.txt")
        for iteration in iterations:
            axis, weights = read_weights(dirs[i], iteration, prefix = 'weights_even', postfix = titles[i], ptype = ptype, step = 50.)
            if max_factor == 0:
               max_factor = np.sum(axis * weights)/np.sum(weights)
            #print(max_factor)
            
            axis /= max_factor
            av = np.sum(axis * weights)/np.sum(weights)
            print(av)
            #if 'simple' not in dirs[i]:
            #    weights = smooth_tail(weights)
            p = ax1.plot(axis, weights, label = titles[i], linewidth = lw)
            if 'default' in dirs[i]:
                ax1.plot([axis[-1], axis[-1]], [1e-8, weights[-1]], color = p[-1].get_color(), linewidth = lw)
            ax1.plot([av, av], [1e-8, 1], color = p[-1].get_color(), dashes = [2,1])
        
        print len(weights)
    ax1.set_xscale('log')
    ax1.set_yscale('log')
    ax1.set_xlabel('f')
    ax1.set_ylabel('N$_f$/N$_\Sigma$')
    ax1.set_ylim([5e-8, 1])
    ax1.set_xlim([5e-2, 5e5])
    plt.legend(loc = 'upper right', frameon = False)
    #plt.show()
    picname = picspath + '/' + filename
    plt.tight_layout()
    print(picname)
    plt.savefig(picname, dpi=512)
    plt.close()

    
if __name__ == '__main__':
    main()
