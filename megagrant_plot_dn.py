#!/usr/bin/python
import matplotlib as mp
mp.use('Agg')
import os
import math
import zipfile
import numpy as np
import re
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import sys
import utils
from mpl_toolkits.axes_grid1 import make_axes_locatable

def main():
    fontsize = 14
    mp.rcParams.update({'font.size': fontsize})
    picdir = "/home/evgeny/Dropbox/reportMegagrant"
    labels=["Ideal", "Amp",  "Shift", "Delay"]
    #labels=["Ideal", "Amp 1.5", "Amp 1.25",  "Shift", "Delay"]
    simulations=[
    #    "InputUnified_4_30_2020-12-29_23-49-05_Ideal4",
    #             "DifA/InputUnified_4_30_2020-12-23_15-12-56",
    #             "DifShift/InputUnified_4_30_2020-12-25_00-34-31",
    #             "DifDelay/InputUnified_4_30_2020-12-29_15-55-44"
    #    "equal_6beams/6beams_21pw",
    #    "amplitude_6beams/6beams_21pw",
    #    "shift_6beams/6beams_21pw",
    #    "delay_6beams/6beams_21pw",
        "equal_4beams/4beams_30pw",
        "amplitude_4beams/4beams_30pw",
        "shift_4/4beams_30pw",
        "delay_4beams/4beams_30pw",
    #    "equal/12beams_13pw",
    #    "amplitude/12beams_13pw",
     #   "amplitude_1.25/12beams_13pw",
     #   "shift/12beams_13pw",
     #   "delay/12beams_13pw",
    ]
    fig = plt.figure(figsize=(12, 8))
    ax1 = fig.add_subplot(2, 2, 1)
    ax2 = fig.add_subplot(2, 2, 2)
    ax3 = fig.add_subplot(2, 2, 3)
    ax4 = fig.add_subplot(2, 2, 4)
    m1 = 0
    m2 = 0
    m3 = 0
    m4 = 0
    for idx, path_ in enumerate(simulations):
        print(path_)
                
        el_dn_theta = np.loadtxt(os.path.join(path_, "el_dn_theta.txt"))
        el_dn_phi = np.loadtxt(os.path.join(path_, "el_dn_phi.txt"))
        ph_dn_theta = np.loadtxt(os.path.join(path_, "ph_dn_theta.txt"))
        ph_dn_phi = np.loadtxt(os.path.join(path_, "ph_dn_phi.txt"))
        el_dn_theta /= np.sum(el_dn_theta)
        el_dn_phi /= np.sum(el_dn_phi)
        ph_dn_theta /= np.sum(ph_dn_theta)
        ph_dn_phi /= np.sum(ph_dn_phi)
        m1 = max(m1, np.amax(el_dn_theta))
        m2 = max(m2, np.amax(el_dn_phi))
        m3 = max(m3, np.amax(ph_dn_theta))
        m4 = max(m4, np.amax(ph_dn_phi))
        
    for idx, path_ in enumerate(simulations):
        print(path_)
        cfg = utils.get_config(os.path.join(path_, "ParsedInput.txt"))
        number = int(cfg["FieldGenerator.Number"])
        
        el_dn_theta = np.loadtxt(os.path.join(path_, "el_dn_theta.txt"))
        el_dn_phi = np.loadtxt(os.path.join(path_, "el_dn_phi.txt"))
        ph_dn_theta = np.loadtxt(os.path.join(path_, "ph_dn_theta.txt"))
        ph_dn_phi = np.loadtxt(os.path.join(path_, "ph_dn_phi.txt"))
        el_dn_theta /= np.sum(el_dn_theta) * m1
        el_dn_phi /= np.sum(el_dn_phi) * m2
        ph_dn_theta /= np.sum(ph_dn_theta) * m3
        ph_dn_phi /= np.sum(ph_dn_phi) * m4
        ax1.plot(el_dn_theta, label = labels[idx])
        ax2.plot(el_dn_phi, label = labels[idx])
        ax3.plot(ph_dn_theta, label = labels[idx])
        ax4.plot(ph_dn_phi, label = labels[idx])
    ax1.legend(loc="upper center", frameon = False)
    ax2.legend(loc="upper right", frameon = False)
    ax3.legend(loc="upper center", frameon = False)
    ax4.legend(loc="upper right", frameon = False)
    ax1.set_xlim([0,180])
    ax3.set_xlim([0,180])
    ax2.set_xlim([0,360])
    ax4.set_xlim([0,360])
    ax1.set_xlabel('$\\theta$')
    ax2.set_xlabel('$\phi$')
    ax3.set_xlabel('$\\theta$')
    ax4.set_xlabel('$\phi$')
    ax1.set_ylabel('$\partial N/\partial \\theta$, a.u.')
    ax2.set_ylabel('$\partial N/\partial \phi$, a.u.')
    ax1.set_ylabel('$\partial N/\partial \\theta$, a.u.')
    ax2.set_ylabel('$\partial N/\partial \phi$, a.u.')
    ax1.text(-25, 1.0, '(a)')
    ax2.text(-50, 1.0, '(b)')
    ax3.text(-25, 1.0, '(c)')
    ax4.text(-50, 1.0, '(d)')
    ax2.set_xticks([0, 90, 180, 270, 360])
    ax2.set_xticklabels(["0", "$\pi/2$", "$\pi$", "$3\pi/2$", "$2\pi$"])
    ax4.set_xticks([0, 90, 180, 270, 360])
    ax4.set_xticklabels(["0", "$\pi/2$", "$\pi$", "$3\pi/2$", "$2\pi$"])
    ax1.set_xticks([0, 90, 180])
    ax1.set_xticklabels(["0", "$\pi/2$", "$\pi$"])
    ax3.set_xticks([0, 90, 180])
    ax3.set_xticklabels(["0", "$\pi/2$", "$\pi$"])
    plt.tight_layout()
    figname = os.path.join(picdir, "dn_%d.png" % number)
    print(figname)
    plt.savefig(figname, dpi=128)
    plt.close()
    
if __name__ == '__main__':
    main()
