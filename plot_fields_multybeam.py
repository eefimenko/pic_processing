#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import os
import numpy as np

def read_fields(path):
    config = utils.get_config(path+"/ParsedInput.txt")
    filename = os.path.join(path, 'ParticleTracking/Dummy1/1.txt')
    BOIterationPass = float(config['BOIterationPass'])
    step = float(config['TimeStep'])*1e15*BOIterationPass

    print(filename)

    ex = []
    ey = []
    ez = []
    bx = []
    by = []
    bz = []
    with open(filename, 'r') as f:
        for line in f:
            values = line.split()
            ex.append(float(values[1]))
            ey.append(float(values[2]))
            ez.append(float(values[3]))
            bx.append(float(values[4]))
            by.append(float(values[5]))
            bz.append(float(values[6]))

    ax = np.zeros_like(ex)
    
    for i in range(len(ax)):
        ax[i] = i * step

    if '0.' in path:
        ex, bx = bx, ex
        ey, by = by, ey
        ez, bz = bz, ez
        
    return ax, np.array(ex), np.array(ey), np.array(ez), np.array(bx), np.array(by), np.array(bz)

def main():
    dirs = ['mb_6_pulse_dw_2022-07-12_17-29-38',
            'mb_6_pulse_0.52_2022-07-12_19-18-35',
            'mb_6_pulse_0.4_2022-07-12_21-23-20',
            'mb_6_pulse_dw_2022-07-12_16-14-30',
            'mb_12_pulse_0.4_2022-07-19_16-39-36',
            'mb_12_pulse_0.52_2022-07-19_20-01-51'
    ]

    labels = ['dw', '6b 0.52', '6b 0.4', 'dw 6b', '12b 0.4', '12b 0.52']

    n = len(dirs)

    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(3,2,1)
    ax1.set_title('E_$x$')
    ax2 = fig.add_subplot(3,2,3)
    ax2.set_title('E_$y$')
    ax3 = fig.add_subplot(3,2,5)
    ax3.set_title('E_$z$')
    ax4 = fig.add_subplot(3,2,2)
    ax4.set_title('B_$x$')
    ax5 = fig.add_subplot(3,2,4)
    ax5.set_title('B_$y$')
    ax6 = fig.add_subplot(3,2,6)
    ax6.set_title('B_$z$')
    
    for i in range(n):
        path = dirs[i]
        label = labels[i]
        axis, ex, ey, ez, bx, by, bz = read_fields(path)
        ax1.plot(axis, np.abs(ex), label = label)
        ax2.plot(axis, np.abs(ey), label = label)
        ax3.plot(axis, np.abs(ez), label = label)
        ax4.plot(axis, np.abs(bx), label = label)
        ax5.plot(axis, np.abs(by), label = label)
        ax6.plot(axis, np.abs(bz), label = label)

    ax1.legend()
    ax2.legend()
    ax3.legend()
    ax4.legend()
    ax5.legend()
    ax6.legend()
    
    plt.show()

if __name__ == '__main__':
    main()
