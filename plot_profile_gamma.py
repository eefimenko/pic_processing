#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_gamma(filename):
    f = open(filename, 'r')
    tmp = f.readline().split()
    return float(tmp[0])

def main():
    n = len(sys.argv) - 2
    power = []
    gamma = []
    name = sys.argv[-1]
    gf = open(name, 'w')
    for i in range(n):
        path = sys.argv[i+1]
        config = utils.get_config(path + "/ParsedInput.txt")
        pwr = float(config['PeakPowerPW'])
        gmm = read_gamma(path + '/' + name)
        gamma.append(gmm)
        power.append(pwr)
        gf.write('%lf %le\n' % (pwr, gmm))
    gf.close()
    fig, ax1 = plt.subplots()
    p, = ax1.plot(power, gamma, 'r')
#    ax1.set_yscale('log')
#    ax1.set_xscale('log')
    plt.show()

if __name__ == '__main__':
    main()
