#!/usr/bin/python
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import sys
import math
import utils
import numpy as np

def get_first_harmonic(x):
    fft = np.fft.rfft(x)
    field = np.sum(x, axis = 0)
#    fig = plt.figure(num=None, figsize=(8., 6.))
#    ax = fig.add_subplot(1,1,1)
#    ax.plot(field)
#    plt.show()
    return field[176] # magic number

def main():
  
    numdirs = int(sys.argv[1])
    dirs = []
    configs = []
    for i in range(numdirs):
        dirs.append(sys.argv[2+i] + '/')
        configs.append(utils.get_config(sys.argv[2+i] + "/ParsedInput.txt"))
    #delta = 1
    n = numdirs + 1
    num = len(sys.argv) - n
    picspath = '/home/evgeny/Dropbox/Thinout/new/pics'
    
    if num == 1:
        print 'Plotting all pics'
        nmin, nmax, delta = utils.get_min_max_iteration(sys.argv, dirs[0] + '/' + 'data/Exxy', n=n)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1 + n]
        nmin, nmax, delta = utils.get_min_max_iteration(sys.argv, dirs[0] + '/' + 'data/Exxy', n=n)
        nmin = int(sys.argv[1 + n])
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1 + n] + ' to ' + sys.argv[2 + n]
        nmin = int(sys.argv[1 + n])
        nmax = int(sys.argv[2 + n])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1 + n] + ' to ' + sys.argv[2 + n] + ' with delta ' + sys.argv[3 + n]
        print sys.argv[1 + n]
        nmin = int(sys.argv[1 + n])
        nmax = int(sys.argv[2 + n])
        delta = int(sys.argv[3 + n])
    else:
        nmin = 0
        nmax = 0
        print 'Too much parameters'
    
    nmin, nmax, delta = utils.get_min_max_iteration(sys.argv, dirs[0] + '/' + 'data/Exxy', n=n)
    print nmin, nmax, delta
    mkm = 1e-4
    xmin = []
    xmax = []
    ymin = []
    ymax = []
    zmin = []
    zmax = []
    mult = []
    nx = []
    ny = []
    nz = []
    power = []
    cmaps = ['bwr', 'bwr', 'Reds', 'Greens', 'Blues']
    log = [False, False, False, False, False]
    normalize = False
    fft = False
    save_timeseries = True
    if fft:
        cmaps = ['hot', 'hot', 'Reds', 'Greens', 'Blues']
        log = [True, True, True, True, True]
        normalize = True
    
        
    for k in range(numdirs):
        if fft:
            nx.append(int(configs[k]['MatrixSize_X']))
            ny.append(int(configs[k]['MatrixSize_Y']))
            nz.append(int(configs[k]['MatrixSize_Z']))
            xmax.append(float(configs[k]['X_Max'])/mkm) #mkm
            xmin.append(float(configs[k]['X_Min'])/mkm) #mkm
            ymax.append(float(configs[k]['Y_Max'])/mkm) #mkm
            ymin.append(float(configs[k]['Y_Min'])/mkm) #mkm
            zmax.append(float(configs[k]['Z_Max'])/mkm) #mkm
            zmin.append(float(configs[k]['Z_Min'])/mkm) #mkm
        else:
            xmax.append(float(configs[k]['X_Max'])/mkm) #mkm
            xmin.append(float(configs[k]['X_Min'])/mkm) #mkm
            ymax.append(float(configs[k]['Y_Max'])/mkm) #mkm
            ymin.append(float(configs[k]['Y_Min'])/mkm) #mkm
            zmax.append(float(configs[k]['Z_Max'])/mkm) #mkm
            zmin.append(float(configs[k]['Z_Min'])/mkm) #mkm
            nx.append(int(configs[k]['MatrixSize_X']))
            ny.append(int(configs[k]['MatrixSize_Y']))
            nz.append(int(configs[k]['MatrixSize_Z']))
       
        dx = float(configs[k]['Step_X'])/mkm
        dy = float(configs[k]['Step_Y'])/mkm
        dz = float(configs[k]['Step_Z'])/mkm
        mult.append(1/(2.*dx*dy*dz*1e-12))
        step = float(configs[k]['TimeStep'])*1e15*float(configs[k]['BOIterationPass'])
        

    figures = ['data/Exxy', 'data/Bzxy', 'data/Electronxy', 'data/Positronxy', 'data/Photonxy']
    
    titles = ['E, CGSE', 'B, CGSE', 'Electrons', 'Positrons', 'Photons']
    titles1 = ['Initial', 'No_thinout/No_cascade', 'No_thinout/Cascade', 'No_thinout/No_cascade_20', 'No_thinout/Cascade_20', 'Energy', 'Merge', 'MergeAverage']
    
    mult = [1,1,1,1,1]
    fontsize = 10
    spx = numdirs
#    n0 = [0] * numdirs 
    spy = len(figures)
    labels = ['(g)', '(h)', '(i)', '(j)', '(k)', '(l)']
    if fft:
        xlabel = '$k_y$'
    else:
        xlabel = '$y, \mu m$'
    ratio = 1e-2
    ne_max = np.zeros((spx,nmax))
    ne_sum = np.zeros((spx,nmax))
    ne_std = np.zeros((spx,nmax))
    ne_1st = np.zeros((spx,nmax))
    for i in range(nmin,nmax,delta):
        print "\rSaving sc%06d.png, %.0f %% done" % (i,float(i-nmin)/(nmax-nmin)*100),
        sys.stdout.flush()

        fig = plt.figure(num=None, figsize=(6.*float(spx)/float(spy), 8.))
        mp.rcParams.update({'font.size': 10})
        
        for k in range(spx):
            if k == 0:
                i_ = 0
            else:
                i_ = i
            for j in range(spy):
                ylabel = ''
                yticks = []
                if j == 0:
                    if fft:
                        ylabel = '$k_x$'
                    else:
                        ylabel = '$x, \mu m$'
                    yticks = [-1, -0.5,0,0.5, 1]
                    
                    # fx = utils.bo_file_load(dirs[k]+utils.ezpath,
                    #                         i+n0[k],nx[k],ny[k],verbose=1)
                    # fy = utils.bo_file_load(dirs[k]+utils.bzpath,
                    #                         i+n0[k],nx[k],ny[k],verbose=1)
                    # sf = np.square(fx) + np.square(fy)
                    ax = utils.subplot(fig, i_, dirs[k]+figures[j],ftype = 'bin',
                                  shape = (nx[k],ny[k]), position = (spy,spx,k+1+j*spx), #(spx,spy,j+1+k*spy),
                                  extent = [xmin[k], xmax[k], ymin[k], ymax[k]],
                                  cmap = cmaps[j], title = titles1[k] + '\n' + titles[j],
                                       colorbar = True, logarithmic=log[j], verbose=1, fft = fft, ratio=1e-4,
                                  #xlim = [-1.,1.], ylim = [-1.,1.], xticks = [-1, -0.5,0,0.5, 1], yticks = yticks,
                                       xlabel = xlabel, ylabel = ylabel, fontsize=fontsize, mult = mult[j])
                else:
                    if fft:
                        ylabel = '$k_x$'
                        ax, (harm_) = utils.subplot(fig, i_, dirs[k]+figures[j],ftype = 'bin',
                                                                     shape = (nx[k],ny[k]), position = (spy,spx,k+1+j*spx), #(spx,spy,j+1+k*spy),
                                                                     extent = [xmin[k], xmax[k], ymin[k], ymax[k]],
                                                                    cmap = cmaps[j], title = titles[j],
                            colorbar = True, logarithmic=log[j], verbose=1, fft = fft, ratio=1e-4,
                                                                     #xlim = [-1.,1.], ylim = [-1.,1.], xticks = [-1, -0.5,0,0.5, 1], yticks = yticks,
                                                                     xlabel = xlabel, ylabel = ylabel, fontsize=fontsize, mult = mult[j],
                                                                     func = lambda x: get_first_harmonic(x))
                    else:
                        ylabel = '$x, \mu m$'
                        ax, (min_, max_, sum_, std_) = utils.subplot(fig, i_, dirs[k]+figures[j],ftype = 'bin',
                            shape = (nx[k],ny[k]), position = (spy,spx,k+1+j*spx), #(spx,spy,j+1+k*spy),
                            extent = [xmin[k], xmax[k], ymin[k], ymax[k]],
                            cmap = cmaps[j], title = titles[j],
                            colorbar = True, logarithmic=log[j], verbose=1, fft = fft, ratio=1e-4,
                            #xlim = [-1.,1.], ylim = [-1.,1.], xticks = [-1, -0.5,0,0.5, 1], yticks = yticks,
                            xlabel = xlabel, ylabel = ylabel, fontsize=fontsize, mult = mult[j],
                            func = lambda x: (np.amin(x), np.amax(x), np.sum(x), np.std(x,dtype=np.float64)))
                    if j == 2:
                        if not fft: 
                            print 'Minimum = %le Maximum = %le ' % (min_, max_)
                            ne_max[k,i] = max_
                            ne_sum[k,i] = sum_/(nx[k]*ny[k])
                            ne_std[k,i] = std_
                            print 'Std = %le'% (ne_std[k,i])
                        else:
                            ne_1st[k,i] = harm_
                            #                    circ = plt.Circle((0, 0), radius=0.44, color='gray', fill=False)
#                    ax.add_patch(circ)
#                ax.text(-1.2, 1.2, labels[k*spy + j])
        if fft:
            picname = picspath + '/' + "fft_comb%06d.png" % (i,)
        else:
            picname = picspath + '/' + "comb%06d.png" % (i,)
        plt.tight_layout()
        plt.savefig(picname, dpi=128, bbox_inches='tight')
        plt.close()
    if not fft and save_timeseries:
        fig = plt.figure(num=None, figsize=(8., 6.))
        ax = fig.add_subplot(1,1,1)
        for k in range(spx):
            ax.plot(ne_max[k,:], label = titles1[k])
        ax.legend(loc = 'upper left', frameon = False)
        ax.set_yscale('log')
        picname = picspath + '/' + "compare_max_density.png"
        plt.tight_layout()
        plt.savefig(picname, dpi=128, bbox_inches='tight')
        plt.close()
        
        f = open('max_density.txt', 'w')
        for k in range(spx):
            f.write('%s '%titles1[k])
        f.write('\n')
        for i in range(nmax):
            f.write('%d '%i)
            for k in range(spx):
                f.write('%le '%ne_max[k, i])
            f.write('\n')
        f.close()
        
        fig = plt.figure(num=None, figsize=(8., 6.))
        ax = fig.add_subplot(1,1,1)
        for k in range(spx):
            ax.plot(ne_sum[k,:], label = titles1[k])
        ax.legend(loc = 'upper left', frameon = False)
        ax.set_yscale('log')
        picname = picspath + '/' + "compare_sum_density.png"
        plt.tight_layout()
        plt.savefig(picname, dpi=128, bbox_inches='tight')
        plt.close()

        f = open('sum_density.txt', 'w')
        for k in range(spx):
            f.write('%s '%titles1[k])
        f.write('\n')
        for i in range(nmax):
            f.write('%d '%i)
            for k in range(spx):
                f.write('%le '%ne_sum[k, i])
            f.write('\n')
        f.close()

        fig = plt.figure(num=None, figsize=(8., 6.))
        ax = fig.add_subplot(1,1,1)
        for k in range(spx):
            ax.plot(ne_std[k,:], label = titles1[k])
        ax.legend(loc = 'upper left', frameon = False)
        ax.set_yscale('log')
        picname = picspath + '/' + "compare_std_density.png"
        plt.tight_layout()
        plt.savefig(picname, dpi=128, bbox_inches='tight')
        plt.close()

        f = open('std_density.txt', 'w')
        for k in range(spx):
            f.write('%s '%titles1[k])
        f.write('\n')
        for i in range(nmax):
            f.write('%d '%i)
            for k in range(spx):
                f.write('%le '%ne_std[k, i])
            f.write('\n')
        f.close()
        
    if fft and save_timeseries:
        fig = plt.figure(num=None, figsize=(8., 6.))
        ax = fig.add_subplot(1,1,1)
        for k in range(spx):
            ax.plot(ne_1st[k,:], label = titles1[k])
        ax.legend(loc = 'upper left', frameon = False)
        ax.set_yscale('log')
        picname = picspath + '/' + "compare_harm_density.png"
        plt.tight_layout()
        plt.savefig(picname, dpi=128, bbox_inches='tight')
        plt.close()
        
        f = open('harm_density.txt', 'w')
        for k in range(spx):
            f.write('%s '%titles1[k])
        f.write('\n')
        for i in range(nmax):
            f.write('%d '%i)
            for k in range(spx):
                f.write('%le '%ne_1st[k, i])
            f.write('\n')
        f.close()
    
if __name__ == '__main__':
    main()
