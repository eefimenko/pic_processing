#!/usr/bin/python
import matplotlib as mp
mp.use("Agg")
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np
import os
from tqdm import *

def create_axis(n,step):
    axis = np.zeros(n)
    for i in range(n):
        axis[i] = i*step
    return axis

def get_config(file):
    searchfile = open(file, "r")
    myvars = {}
    for line in searchfile:
        if "Setting" in line and ("variable" in line or "string" in line) and '=' in line:
            tmp, var = line.partition("=")[::2]
            rem, name = tmp.partition(":")[::2]
            myvars[name.strip()] = var.strip()
    searchfile.close()
    return myvars

def num_files(dirname):
    num = len([name for name in os.listdir(dirname) if os.path.isfile(dirname + '/' +name)])
    return num

def main():
    n = len(sys.argv) - 1
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    bpath = 'data/B2z'
    nepath = 'data/NeTrap'
    picspath = '/home/evgeny/Dropbox/tmp_pics'
    power = []
    n_pairs = []
    n_pairs_max = []
    window = 200
    for i_ in range(n):
    	path = sys.argv[i_+1]
        label = path
        print path 
        config = get_config(path + '/' + "ParsedInput.txt")
        BOIterationPass = float(config['BOIterationPass'])
        step = float(config['TimeStep'])*1e15*BOIterationPass
        nmax = num_files(path + '/' + nepath)
        ppw = float(config['PeakPowerPW'])
        print nmax
        Ne = np.zeros(nmax)
        Bz = np.zeros(nmax)
        
        for i in tqdm(range(150, min(1000,nmax))):
            nename = path + '/' + nepath + '/' + "%06d.txt" % (i,)
            nbzname = path + '/' + bpath + '/' + "%06d.txt" % (i,)
            read, e_ = utils.bo_file_load(path + '/' + nepath, i)
            
            e = np.sum(e_)

            #p = np.amax(utils.fromfile(nbzname))
            Ne[i] = e
            #Bz[i] = p
        axis = create_axis(nmax, step)
        pn, = ax1.plot(axis, Ne, label = label)
        power.append(ppw)
        n_pairs.append(np.amax(Ne[-window:]))
        n_pairs_max.append(np.amax(Ne))
        #pb, = ax2.plot(axis, Bz, label = label, dashes = [2,2])

    ax1.legend(loc='lower right', frameon = 'False')
    ax1.set_yscale('log')
    ax1.set_xlim(0., 100.)
#    ax2.set_ylim(0., 1e12)
    ax1.set_ylabel('$N_e$')
#    ax2.set_ylabel('$B$, CGSE')
    plt.savefig(picspath+ '/trapped_cmp.png')
    plt.close()
    print power, n_pairs
    fig, ax1 = plt.subplots()
    ax1.plot(power, n_pairs, label = 'stationary')
    ax1.plot(power, n_pairs_max, label = 'full')
    ax1.legend(loc='lower right', frameon = 'False')
    ax1.set_yscale('log')
    ax1.set_ylabel('Number of pairs')
    ax1.set_xlabel('Power, PW')
    print(picspath+ '/num_pairs.png')
    plt.savefig(picspath+ '/num_pairs.png')
    plt.close()
    f = open("number_trapped.txt", 'w')
    for i in range(len(power)):
        f.write('%lf %le %le\n'%(power[i], n_pairs[i], n_pairs_max[i]))
    f.close()
if __name__ == '__main__':
    main()
