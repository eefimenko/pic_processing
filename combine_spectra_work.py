#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np
import matplotlib.colors as clr

def analyze_spectra(sp,de):
    n = len(sp)
    full_energy = sum(sp)
    full_number = 0
    print full_energy
    for i in range(n):
        full_number += sp[i]/(de*(i+0.425))
        
    tail_energy = 0
    
    for i in range(n):
        tail_energy += sp[-1-i]
        if tail_energy/full_energy > 0.01:
            break
    imax = i
    e_1percent = (n-imax)*de
    
    for i in range(n):
        sp[i] /= full_number

    av_energy = full_energy/full_number
            
    return sp, e_1percent, av_energy, n-imax, int(av_energy/de)

def main():
    path = './'
    picspath = 'pics/'
    config = utils.get_config(path + "/ParsedInput.txt")
    ev = float(config['eV'])
    ne = int(config['QEDstatistics.OutputN_E'])
    nt = int(config['QEDstatistics.OutputN_theta'])
    if 'QEDstatistics.ThetaMax' in config:
        tmax = float(config['QEDstatistics.ThetaMax'])*180./math.pi
    else:
        tmax = 0.
    if 'QEDstatistics.ThetaMin' in config:
        tmin = float(config['QEDstatistics.ThetaMin'])*180./math.pi
    else:
        tmin = 0.
    
    emax = float(config['QEDstatistics.Emax'])/ev*1e-9
    emin = float(config['QEDstatistics.Emin'])/ev*1e-9
    
    de = (emax - emin)/ne
    dt = (tmax - tmin)/nt
    de0 = de*1e9*ev 
    nmin,nmax,delta = utils.get_min_max_iteration(sys.argv,path + utils.enspsphpath)
    nmin = nmax - 500
    print nmin, nmax, delta
    eax = utils.create_axis(ne, de, emin)
    res = utils.bo_file_load(path + utils.enspsphpath,nmin,fmt='%.4f',verbose = 1)
    res1 = utils.bo_file_load(path + utils.el_enspsphpath,nmin,fmt='%.4f',verbose = 1)
    for i in range(nmin+1,nmax,delta):
        res += utils.bo_file_load(path + utils.enspsphpath,i,fmt='%.4f',verbose = 1)
        res1 += utils.bo_file_load(path + utils.el_enspsphpath,i,fmt='%.4f',verbose = 1)
    name = path + '/spectra.txt'
    res.tofile(name,' ', '%.6e')
    fig = plt.figure(num=None, figsize=(8, 6))
    ax1 = fig.add_subplot(1,1,1)
    res, ph_max, ph_av, ph_imax, ph_iav = analyze_spectra(res,de0)
    res1, el_max, el_av, el_imax, el_iav = analyze_spectra(res1,de0)
    ph_max *= 1e-9/ev
    el_max *= 1e-9/ev
    ph_av *= 1e-9/ev
    el_av *= 1e-9/ev
    mm = max(max(res), max(res1))
    print 'Ph ', ph_max, ph_av, ph_imax
    print 'El ', el_max, el_av, el_imax
    for i in range(len(res)):
        res[i] /= mm
        res1[i] /= mm
    ax1.plot(eax,res,'r', label = 'Photon')
    ax1.plot([ph_max, ph_max], [0, res[ph_imax]], 'r--', label = '$W_{ph}^{1\%}$')
    ax1.plot([ph_av, ph_av], [0, res[ph_iav]], 'r:', label = '$W_{ph}^{av}$')
    ax1.plot(eax,res1, 'b', label = 'Electron')
    ax1.plot([el_max, el_max], [0, res1[el_imax]], 'b--', label = '$W_{el}^{1\%}$')
    ax1.plot([el_av, el_av], [0, res1[el_iav]], 'b:', label = '$W_{el}^{av}$')
    ax1.set_yscale('log')
    ax1.set_xlim([0,2.2])
    ax1.set_ylim([1.e-6,4])
    xticks = [ph_av, el_av, 0.5, ph_max, 1., el_max, 1.5]
    xticklabels = ['%.2f'%(x) for x in xticks]
    ax1.set_xticks(xticks)
    ax1.set_xticklabels(xticklabels)
    ax1.set_xlabel('$\hbar \omega, GeV$')
    ax1.set_ylabel('$dW/d\hbar \omega, a.u.$')
    ax1.legend(loc = 'upper right')
    # axx = fig.add_subplot(1,4,2, polar=True)
    # axy = fig.add_subplot(1,4,3)
    # axz = fig.add_subplot(1,4,4)
    # azimuths_p = np.radians(np.linspace(tmin, tmax, nt))
    # azimuths_p1 = np.radians(np.linspace(90+tmax, 90+tmin, nt))
    # azimuths_n = -np.radians(np.linspace(tmin, tmax, nt))
    # azimuths_n1 = -np.radians(np.linspace(90+tmax, 90+tmin, nt))
    # zeniths = np.arange(emin, emax, de)
        
    # r_p,thetas_p = np.meshgrid(zeniths, azimuths_p)
    # r_p1,thetas_p1 = np.meshgrid(zeniths, azimuths_p1)
    # r_n,thetas_n = np.meshgrid(zeniths, azimuths_n)
    # r_n1,thetas_n1 = np.meshgrid(zeniths, azimuths_n1)
    # x = np.array(el).transpose()
       
    # axx.pcolormesh(thetas_p, r_p, x, norm=clr.LogNorm(x.max()*1e-3, x.max()),shading='gouraud')
    # axx.pcolormesh(thetas_p1, r_p1, x, norm=clr.LogNorm(x.max()*1e-3, x.max()),shading='gouraud')
    # axx.pcolormesh(thetas_n, r_n, x, norm=clr.LogNorm(x.max()*1e-3, x.max()),shading='gouraud')
    # axx.pcolormesh(thetas_n1, r_n1, x, norm=clr.LogNorm(x.max()*1e-3, x.max()),shading='gouraud')
    # axy.pcolormesh(thetas_p, r_p, x)
    # axy.pcolormesh(thetas_p1, r_p1, x)
    # axy.pcolormesh(thetas_n, r_n, x)
    # axy.pcolormesh(thetas_n1, r_n1, x)
    # axx.grid()
    

#    axx.set_rscale('log')
    # axx.set_ylim([0,emax])
    
    plt.savefig(path + picspath + 'spectra.png')
    
if __name__ == '__main__':
    main()
