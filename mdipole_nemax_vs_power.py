#!/usr/bin/python
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import utils
import sys
import os
from tqdm import *
from pylab import *

def main():
   
    path = './'
    picspath = '/home/evgeny/Dropbox/mdipole_pics'

    dirs = ['pdw_12pw', 'pdw_13pw', 'pdw_17pw_1', 'pdw_20pw', 'pdw_25pw', 'pdw_27pw', 'pdw_30pw', 'pdw_35pw', 'pdw_40pw', 'pdw_50pw', 'pdw_60pw']

    power = []
    nex_max = []
    ney_max = []
    nez_max = []
    
    for path in dirs:
        config = utils.get_config(path + "/ParsedInput.txt")
        ev = float(config['eV'])
        wl = float(config['Wavelength'])

        nit = int(config['BOIterationPass'])
        dt = float(config['TimeStep'])*nit
        omega = float(config['Omega'])
        ppw = int(config['PeakPowerPW'])
        duration = float(config.get('Duration',15))
        Xmax = float(config['X_Max']) #mkm
        Xmin = float(config['X_Min']) #mkm
        Ymax = float(config['Y_Max']) #mkm
        Ymin = float(config['Y_Min']) #mkm
        Zmax = float(config['Z_Max']) #mkm
        Zmin = float(config['Z_Min']) #mkm

        nx = int(config['MatrixSize_X'])
        ny = int(config['MatrixSize_Y'])
        nz = int(config['MatrixSize_Z'])

        print('Nx = ' + str(nx)) 
        print('Ny = ' + str(ny))
        print('Nz = ' + str(nz))
        T = 2 * math.pi/omega
        step = float(config['TimeStep'])*float(config['BOIterationPass'])
        nT = T/step
        dx = (Xmax-Xmin)/nx
        dy = (Ymax-Ymin)/ny
        dz = (Zmax-Zmin)/nz
        dmy = int(0.35*wl/dy)
        dv = 2.*dx*dy*dz

        nx1 = int(config['MatrixSize_X'])
        ny1 = int(config['MatrixSize_Y'])
        T = 2 * math.pi/omega
        nnn = int(T/dt)

        ftype = config['BODataFormat']
        curr_dir = os.getcwd()
        print(ppw)
        print(curr_dir)
        print('Read max Ne vs time')
        
        nez_m_t = utils.ts(path, utils.nezpath, name='nem', ftype = ftype,
                           tstype='max', shape = (nx1,ny1), verbose=True)
        ney_m_t = utils.ts(path, utils.neypath, name='nemy', ftype = ftype,
                           tstype='max', shape = (nx1,ny1), verbose=True)
        nex_m_t = utils.ts(path, utils.nexpath, name='nemx', ftype = ftype,
                           tstype='max', shape = (nx1,ny1), verbose=True)
        power.append(ppw)
        if ppw > 20:
            nex_max.append(np.amax(nex_m_t[:450])/dv)
            ney_max.append(np.amax(ney_m_t[:450])/dv)
            nez_max.append(np.amax(nez_m_t[:450])/dv)
        else:
            nex_max.append(np.amax(nex_m_t)/dv)
            ney_max.append(np.amax(ney_m_t)/dv)
            nez_max.append(np.amax(nez_m_t)/dv)
            
    fig = plt.figure(num=None, figsize = (10, 5))
    mp.rcParams.update({'font.size': 20})
    ax = fig.add_subplot(1,1,1)
    ax.plot(power, nex_max, label = 'y-z')
    ax.plot(power, ney_max, label = 'x-z')
    ax.plot(power, nez_max, label = 'x-y')
    plt.legend(loc = 'upper left', frameon = False)
    #plt.show()
    f = open('ne_max.txt', 'w')
    for i in range(len(power)):
        f.write('%f %e %e %e\n' % (power[i], nex_max[i], ney_max[i], nez_max[i]))
    f.close()
    ax.set_ylabel('N$_m$, cm$^{-3}$')
    ax.set_xlabel('Power, PW')
    picname = 'ne_max.png'
    print(picname)
    plt.tight_layout()
    plt.savefig(picname, dpi=256)
    plt.close()
 
if __name__ == '__main__':
    main()
