#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import ticker
import sys
import math
import utils
import numpy as np
import itertools
import os
from mpl_toolkits.axes_grid1 import make_axes_locatable

def Modificator(Rx, Ry, Rz, Cx, Cy, Cz, n):
    rnorm = math.sqrt(Rx*Rx + Ry*Ry + Rz*Rz)
    if rnorm < 1e-8:
        return 0
    cosy = Rz/rnorm
    cos_max = -2
    i_m = 0
    for i in range(n):
        cos_temp = Rx*Cx[i] + Ry*Cy[i] + Rz*Cz[i] 
        if  cos_temp > cos_max:
            cos_max = cos_temp
            i_m = i
    fi = math.acos(cos_max/rnorm)
    F = 1.
    if fi > 0.42:
        F = math.cos(math.pi*0.5*(fi - 0.42)/0.05)**2
    if fi > 0.47:
        F = 0
    
    return F

def create_array(n, ntheta, nphi):
    array = np.zeros((ntheta,nphi))
    if n == 0:
        for i in range(nphi):
            for j in range(ntheta):
                array[j,i] = math.sin(math.pi/ntheta*j)
    else:
        Cx = np.zeros(n)
        Cy = np.zeros(n)
        Cz = np.zeros(n)
        n2 = n/2
        
        if (n > 7):
            cosa = math.sqrt(4./(3. + math.sqrt(3.0)));
	    sina = math.sqrt(1. - cosa*cosa);
		
	    for i in range(n2):
	        Cx[i] = cosa*math.cos(2*math.pi*i/n2)
                Cy[i] = cosa*math.sin(2*math.pi*i/n2)
                Cz[i] = -sina
            for i in range(n2,n):
	        Cx[i] = cosa*math.cos(2*math.pi*(i-n2+0.5)/n2)
                Cy[i] = cosa*math.sin(2*math.pi*(i-n2+0.5)/n2)
                Cz[i] = sina
        else:
            cosa = 1
	    sina = 0
		
	    for i in range(n):
	        Cx[i] = cosa*math.cos(2*math.pi*i/n)
                Cy[i] = cosa*math.sin(2*math.pi*i/n)
                Cz[i] = -sina
        np_ = 100
	nt_ = 100
	dp = 2*math.pi/np_
	dt = math.pi/nt_
	s0 = 0.
	s1 = 0.
	t_ = 0.
	
	for i_ in range(nt_-1):
            theta = dt*(i_+0.5)
	    st = math.sin(theta)
	    ct = math.cos(theta)
	    theta_n = dt*(i_+1)
	    theta_p = dt*i_
	    st_n = math.sin(theta_n)
	    st_p = math.sin(theta_p)
	    ds = 0.5*(st_n + st_p)
	    for j_ in range(np_-1):
	        phi = dp*(j_+0.5)
		sp = math.sin(phi)
		cp = math.cos(phi)
		Rx = st*cp
                Ry = st*sp
                Rz = ct
		
		s0 += st*st*ds
		s1 += Modificator(Rx, Ry, Rz, Cx, Cy, Cz, n)*Modificator(Rx, Ry, Rz, Cx, Cy, Cz, n)*ds
	       
	amplCorrection = math.sqrt(s0/s1)
        for i_ in range(ntheta):
            theta = math.pi/ntheta*i_
	    st = math.sin(theta)
	    ct = math.cos(theta)
	    for j_ in range(nphi):
	        phi = 2.*math.pi/nphi*j_
		sp = math.sin(phi)
		cp = math.cos(phi)
		Rx = st*cp
                Ry = st*sp
                Rz = ct
                array[i_,j_] = amplCorrection*Modificator(Rx, Ry, Rz, Cx, Cy, Cz, n)
    return array
        

def main():
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
#    plt.rc('text', usetex = True)
    num = len(sys.argv)
#    picspath = 'pics'
    picspath = '/home/evgeny/Dropbox/tmp_pics/'
    wd = os.getcwd()
    
    fig = plt.figure(num=None, figsize=(10, 5))
    
    ax1 = fig.add_subplot(3,3,1)
    ax2 = fig.add_subplot(3,3,2)
    ax3 = fig.add_subplot(3,3,3)
    ax4 = fig.add_subplot(3,3,4)
    ax5 = fig.add_subplot(3,3,5)
    ax6 = fig.add_subplot(3,3,6)
    ax7 = fig.add_subplot(3,3,7)
    ax8 = fig.add_subplot(3,3,8)
    ax9 = fig.add_subplot(3,3,9)
    
    ax1.set_xlabel('$\phi$')
    ax1.set_ylabel('$\\theta$')
    ax2.set_xlabel('$\phi$')
    ax2.set_ylabel('$\\theta$')
    ax3.set_xlabel('$\phi$')
    ax3.set_ylabel('$\\theta$')
    ax4.set_xlabel('$\phi$')
    ax4.set_ylabel('$\\theta$')
    ax5.set_xlabel('$\phi$')
    ax5.set_ylabel('$\\theta$')
    ax6.set_xlabel('$\phi$')
    ax6.set_ylabel('$\\theta$')
    ax7.set_xlabel('$\phi$')
    ax7.set_ylabel('$\\theta$')
    ax8.set_xlabel('$\phi$')
    ax8.set_ylabel('$\\theta$')
    ax9.set_xlabel('$\phi$')
    ax9.set_ylabel('$\\theta$')
    axes = []
    axes.append(ax1)
    axes.append(ax2)
    axes.append(ax3)
    axes.append(ax4)
    axes.append(ax5)
    axes.append(ax6)
    axes.append(ax7)
    axes.append(ax8)
    axes.append(ax9)

    nbeams = [0,12,10,8,6,5,4,3,2]
    labels = [u'(a)', u'(b)', u'(c)', u'(d)', u'(e)', u'(f)', u'(g)', u'(h)', u'(i)', u'(j)'] 
    ntheta = 100
    nphi = 200
    for i in range(len(nbeams)):
        array = create_array(nbeams[i], ntheta, nphi)
        c = axes[i].imshow(array, origin = 'lower')
        axes[i].set_yticks([0, ntheta/2, ntheta])
        axes[i].set_yticklabels(['$0$', '$\pi/2$', '$\pi$'])
        axes[i].set_xticks([0, nphi/2, nphi])
        axes[i].set_xticklabels(['$0$', '$\pi$', '$2\pi$'])
        axes[i].text(-50, ntheta, labels[i])
        divider = make_axes_locatable(axes[i])
        cax = divider.append_axes("right", size="5%", pad=0.05)
        cbar = plt.colorbar(c,  orientation  = 'vertical', cax=cax)
        tick_locator = ticker.LinearLocator(numticks=3)
        cbar.locator = tick_locator
#        cbar.ax.yaxis.set_major_locator(ticker.AutoLocator())
        cbar.update_ticks()
#        plt.colorbar(c, orientation = 'vertical')
      
    savename = picspath + '/beams_dn.png'
    print savename
    plt.tight_layout()
    plt.savefig(savename)
    plt.close()
        
if __name__ == '__main__':
    main()
