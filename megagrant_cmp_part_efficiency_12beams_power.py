#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import sys
import math
import utils
import pickle
import os

class Data(object):
    def __init__(self, pickle_file):
        if os.path.exists(pickle_file):
            with open(pickle_file) as f:
                self.ne = pickle.load(f)
                self.number_tgt = pickle.load(f)
                self.efficiency_ph = pickle.load(f)
                self.efficiency_1gev_ph = pickle.load(f)
                self.efficiency_el = pickle.load(f)
                self.efficiency_1gev_el = pickle.load(f)
                self.efficiency_pos = pickle.load(f)
                self.efficiency_1gev_pos = pickle.load(f)
                self.number_ph = pickle.load(f)
                self.number_1gev_ph = pickle.load(f)
                self.number_el = pickle.load(f)
                self.number_1gev_el = pickle.load(f)
                self.number_pos = pickle.load(f)
                self.number_1gev_pos = pickle.load(f)


def main():
    savepath = '/home/evgeny/Dropbox/megagrant2021/'
    if len(sys.argv) == 2:
        savepath = sys.argv[1]
    fontsize = 18
    mp.rcParams.update({'font.size': fontsize})
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})

    edipole1 = Data('dump_12beams_edipole_2mkm.pkl')
    edipole2 = Data('dump_12beams_30fs_edipole_2mkm.pkl')
    edipole3 = Data('dump_12beams_30fs_15pw_edipole_2mkm.pkl')
    
    fig = plt.figure(figsize = (15,6))
    ax1 = fig.add_subplot(1,2,1)
    ax2 = fig.add_subplot(1,2,2)

    ax1.plot(edipole1.ne, edipole1.number_pos, 'k', label = '30 PW 15 fs')
    ax1.plot(edipole2.ne, edipole2.number_pos, 'r', label = '30 PW 30 fs')
    ax1.plot(edipole3.ne, edipole3.number_pos, 'b', label = '15 PW 30 fs')
    ax1.plot(edipole1.ne, edipole1.number_1gev_pos, 'k--', label = '30 PW 15 fs (>1GeV)')
    ax1.plot(edipole2.ne, edipole2.number_1gev_pos, 'r--', label = '30 PW 30 fs (> 1GeV)')
    ax1.plot(edipole3.ne, edipole3.number_1gev_pos, 'b--', label = '15 PW 30 fs (> 1GeV)')
    ax1.plot(edipole1.ne, edipole1.number_tgt, 'g', dashes = [2,2])
    
    ax2.plot(edipole1.ne, edipole1.efficiency_ph, 'k', label = '30 PW 15 fs')
    ax2.plot(edipole2.ne, edipole2.efficiency_ph, 'r', label = '30 PW 30 fs')
    ax2.plot(edipole3.ne, edipole3.efficiency_ph, 'b', label = '15 PW 30 fs')
    ax2.plot(edipole1.ne, edipole1.efficiency_1gev_ph, 'k--', label = '30 PW 15 fs (>1GeV)')
    ax2.plot(edipole2.ne, edipole2.efficiency_1gev_ph, 'r--', label = '30 PW 30 fs (> 1GeV)')
    ax2.plot(edipole3.ne, edipole3.efficiency_1gev_ph, 'b--', label = '15 PW 30 fs (> 1GeV)')
    
    ax1.text(100, 1e13, "$N_0$")
    ax1.set_xscale('log')
    ax1.set_yscale('log')
#    ax1.set_ylim([5e-3, 1]) 
    ax1.set_xlabel(u'$n_0/n_{c}$')
    ax1.set_ylabel(u'$N_{e^{+}}$')
    ax2.set_xscale('log')
    ax2.set_yscale('log')
#    ax2.set_ylim([1e-4, 2e-1]) 
    ax2.set_xlabel(u'$n_0/n_{c}$')
    ax2.set_ylabel(u'$W_{\hbar\omega}/W_l$')
    
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, 1.25), frameon = False, fontsize = fontsize-3, ncol = 2, columnspacing = 1, labelspacing = 0.2)
    ax2.legend(loc='upper center', bbox_to_anchor=(0.5, 1.25), frameon = False, fontsize = fontsize-3, ncol = 2, columnspacing = 1, labelspacing = 0.2)
    
    plt.tight_layout()
    ax1.text(0.00003, ax1.get_ylim()[1], '(a)')
    ax2.text(0.00003, ax2.get_ylim()[1], '(b)')
    
    picname = savepath + '/edipole_12beams_power.png'
    print(picname)
    plt.savefig(picname)

 

if __name__ == '__main__':
    main()

    
