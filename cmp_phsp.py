#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import shutil
import os

def read_file(file, de):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    a_e = [0] * len(array)
    a_n = [0] * len(array)
    f.close()
    nph = 0
    if 'tr' in file and not 'fs' in file:
        array[0] = 0
        print 'removed'
        array, de = adjust_array(array, de)
    for i in range(len(array)):
        nph  += array[i]/(i+0.425)/de
    for i in range(len(array)):
        a_e[i] = array[i]/de
        array[i] /= de*(i+0.425)*de
#        array[i] /= de
    
    return array, a_e, nph # array - dn/de, a_e - dE/de

def check_length(file):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
       
    return len(array)

def adjust_array(array, de):
    n = len(array)
    adj_size = 10
    m = n/adj_size
    a = [0]*m
    for i in range(m):
        tmp = 0
        for j in range(adj_size):
            tmp += array[i*adj_size + j]
        a[i] = tmp
    return a, de*adj_size

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def read_one_spectrum(path1, nmin):
    config = utils.get_config(path1 + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV']) 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
    print num, T
    nph_t = []
    nph1_t = []
    ne_t = []
    np_t = []
    step = x0*y0*1e15/T 
    
    dt = y0*float(config['QEDstatistics.OutputIterStep'])*1e15

    path = '/statdata/ph/EnSpSph/'
    nepath = '/data/NeTrap/'
    nppath = '/data/NposTrap/'
    emin = float(config['QEDstatistics.Emin']) 
    emax = float(config['QEDstatistics.Emax']) 
    ne = int(config['QEDstatistics.OutputN_E'])
    de = (emax - emin)/ne
   
    t = 0
    
    n = check_length(path1 + path + '%.4f.txt'%nmin)
    if 'tr' in path1 and not 'fs' in path1:
        n /= 10
 
    spectrum1 = [0]*n
    spectrum1_e = [0]*n
    
    for i in range(nmin,nmin+num):
        name = path1 + path + '%.4f.txt'%i
        print name
        sp1, sp1_e, nph = read_file(name, de)
       
        for k in range(n):
            spectrum1[k] += sp1[k]/nph
            spectrum1_e[k] += sp1_e[k]/nph
    for k in range(n):
        spectrum1[k] /= num
        spectrum1_e[k] /= num

    if 'tr' in path1 and not 'fs' in path1:
        de *= 10
    axis = create_axis(n, de/ev*1e-9)
    return spectrum1, spectrum1_e, axis

def main():
    picspath = 'pics'
    num = len(sys.argv) - 1
    n = num/2
    sp = []
    sp_e = []
    ax = []
    for k in range(n):
        spectrum, spectrum_e, axis = read_one_spectrum(sys.argv[2*k+1], int(sys.argv[2*k+2]))
        sp.append(spectrum)
        sp_e.append(spectrum_e)
        ax.append(axis)
        
    config = utils.get_config(sys.argv[1] + "/ParsedInput.txt")
#    x0 = int(config['BOIterationPass'])
#    y0 = float(config['TimeStep']) 
#    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    sp_path = 'spectra_%d' % (ppw)
    if not os.path.exists(sp_path):
        os.makedirs(sp_path)
#    ev = float(config['eV']) 
#    T = 2 * math.pi/omega*1e15
#    num = int(T/(x0*y0*1e15))
#    print num, T
#    nph_t = []
#    nph1_t = []
#    ne_t = []
#    np_t = []
#    step = x0*y0*1e15/T 
    
#    nmin = int(sys.argv[2])
#    path = '/statdata/ph/EnSpSph/'
#    nepath = '/data/NeTrap/'
#    nppath = '/data/NposTrap/'
#    emin = float(config['QEDstatistics.Emin']) 
#    emax = float(config['QEDstatistics.Emax']) 
#    ne = int(config['QEDstatistics.OutputN_E'])
#    de = (emax - emin)/ne
#    nmax = utils.num_files(sys.argv[1] + path)
   
#    t = 0
#    n = check_length(sys.argv[1] + path + '%.4f.txt'%nmin)
#    axis = create_axis(n, de/ev*1e-9)

#    spectrum1 = [0]*n
#    for i in range(nmin,nmin+num):
#        name = sys.argv[1] + path + '%.4f.txt'%i
#        print name
#        sp1, nph = read_file(name, de)
#        for k in range(n):
#            spectrum1[k] += sp1[k]/num

#    spectrum2 = [0]*n
#    for i in range(nmin+100,nmin+num+100):
#        name = sys.argv[1] + path + '%.4f.txt'%i
#        print name
#        sp1, nph = read_file(name, de)
#        for k in range(n):
#            spectrum2[k] += sp1[k]/num
 
    mm = 6
    fig = plt.figure(num=None, figsize=(20, 20))
    ax1 = fig.add_subplot(3,2,1)
    ax11 = fig.add_subplot(3,2,2)
    for k in range(n):
        if 'tr' in sys.argv[2*k+1]:
            lbl = '(1)no pair production'
        else:
            lbl = '(2)pair production'
        ax1.plot(ax[k], sp[k], label = lbl)
        ax11.plot(ax[k], sp_e[k], label = lbl)
        print sys.argv[2*k+1], sum(sp[k]), sp[k][0]
#    ax1.plot(axis, spectrum2)
#    ax1.set_yscale('log')
    ax1.set_xlim([1e-2, mm])
    ax1.legend(loc='upper right', shadow=True)
    ax11.set_xlim([1e-2, mm])
    ax11.legend(loc='upper right', shadow=True)
    ax1.set_xscale('log')
    ax11.set_xscale('log')
    ax1 = fig.add_subplot(3,2,3)
    ax11 = fig.add_subplot(3,2,4)
    for k in range(n):
        if 'tr' in sys.argv[2*k+1]:
            lbl = '(1)no pair production'
        else:
            lbl = '(2)pair production'
        ax1.plot(ax[k], sp[k], label = lbl)
        ax11.plot(ax[k], sp_e[k], label = lbl)
        print sys.argv[2*k+1], sum(sp[k]), sp[k][0]
#    ax1.plot(axis, spectrum2)
    ax1.set_yscale('log')
    ax1.set_xlim([1e-2, mm])
    ax1.legend(loc='upper right', shadow=True)
    ax11.set_yscale('log')
    ax11.set_xlim([1e-2, mm])
    ax11.legend(loc='upper right', shadow=True)  
    ax1.set_xscale('log')
    ax11.set_xscale('log')
    ax1 = fig.add_subplot(3,2,5)
    ax11 = fig.add_subplot(3,2,6)
    diff = [0] * len(sp[0])
    diff_e = [0] * len(sp[0])
    for i in range(len(sp[0])):
        diff[i] = sp[0][i] - sp[1][i]
        diff_e[i] = sp_e[0][i] - sp_e[1][i]

    ax1.plot(ax[0], diff, label = 'diff (2) - (1)')
    ax11.plot(ax[0], diff_e, label = 'diff (2) - (1)')
#        print sys.argv[2*k+1], sum(sp[k]), sp[k][0]
#    ax1.plot(axis, spectrum2)
#    ax1.set_yscale('log')
    ax1.set_xlim([1e-2, mm])
    ax11.set_xlim([1e-2, mm])
    ax1.set_xscale('log')
    ax11.set_xscale('log')
    plt.grid()
    ax1.legend(loc='upper right', shadow=True)
    plt.savefig(sp_path + '/' + 'sp_%d.png'%ppw)
    plt.close()
    out = open(sp_path + '/' + 'sp_n_%d.dat'%ppw, 'w')
    for i in range(len(sp[0])):
        out.write('%lf %le %le\n'%(ax[0][i], sp[0][i], sp[1][i]))
    out.close()
    out = open(sp_path + '/' + 'sp_e_%d.dat'%ppw, 'w')
    for i in range(len(sp[0])):
        out.write('%lf %le %le\n'%(ax[0][i], sp_e[0][i], sp_e[1][i]))
    out.close()
    shutil.copy(sys.argv[1]+'/Input.txt', sp_path)
    shutil.make_archive(sp_path, 'zip', sp_path)
#    plt.show()
    
if __name__ == '__main__':
    main()
