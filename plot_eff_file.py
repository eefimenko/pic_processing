#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_file(filename):
    f = open(filename, 'r')
    av = []
    for line in f:
        tmp = line.split()
        av.append(float(tmp[0])*1e15*3e-15*1e7/(1.6e-12*0.5e6)*0.5)
    f.close()
    return av

def main():
    n = len(sys.argv) - 1
    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(1,2,1)
    p = [8., 9., 10., 12., 15., 20., 30., 35.,]
    r = [2533., 2647., 2693., 2732., 2739., 2709., 2655., 2632.]
    for i in range(n):
        path = sys.argv[i+1]
        config = utils.get_config(path + "/ParsedInput.txt")
        pwr = float(config['PeakPowerPW'])
        av = read_file(path + '/eff.txt')
        ax1.plot(av, label = '%d PW'%pwr)
    ax1.set_xlim([0,1000])
    ax1.set_xlabel('Iteration')
    ax1.set_ylabel('Average energy/mc^2')
    plt.legend(loc = 'lower right')
    ax2 = fig.add_subplot(1,2,2)
    ax2.set_xlabel('Power, PW')
    ax2.set_ylabel('Average energy/mc^2')
    ax2.plot(p,r)
    plt.show()

if __name__ == '__main__':
    main()
