#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils
import numpy as np
import glob
import pickle
import os

def read_weights(path, iteration, ptype = 'el', prefix = 'weights', bins = 100, logbins = False, logfactor = 1.1, step = None, processes = None, nprocesses = None):
    picklename = '%s/weights/%s/%s_%d.pkl' % (path,ptype,prefix,iteration)
    wmin = 1e9
    wmax = 0.
    
    if os.path.exists(picklename):
        with open(picklename) as f:
            axis = pickle.load(f)
            weights = pickle.load(f)
    else:
        if processes == None:
            filelist = glob.glob('%s/weights/%s/*_%d.txt' % (path, ptype, iteration))
        else:
            filelist = []
            for i in processes:
                for j in processes:
                    for k in processes:
                        num = i + j*8 + k*8*8
                        filelist.append('%s/weights/%s/%d_%d.txt' % (path, ptype, num, iteration))
            
        for filename in filelist:
            print filename
            with open(filename) as f:
                content = f.readline().split()
                for item in content:
                    val = float(item)
                    if val > wmax:
                        wmax = val
                    if val < wmin:
                        wmin = val

        if logbins:
            nbins = int(math.log(wmax/wmin)/math.log(logfactor))+1
            axis = np.zeros(nbins)
            step = np.zeros(nbins)
            factor = wmin
            for i in range(nbins):
                axis[i] = factor
                step[i] = (logfactor - 1.) * factor
                factor *= logfactor
        else:
            if step is None:
                nbins = bins
            else:
                nbins = int(wmax/step) + 1
            
            axis = np.zeros(nbins)
            dw = wmax/(nbins - 1)
            for i in range(nbins):
                axis[i] = dw * i
        
        weights = np.zeros(nbins)
        
        count = 0
        if logbins:
            for filename in filelist:
                print filename
                with open(filename) as f:
                    content = f.readline().split()
                    for item in content:
                        val = float(item)
                        idx = int(math.log(val/wmin)/math.log(logfactor))
                        weights[idx] += 1
                        count += 1
        else:
            for filename in filelist:
                print filename
                with open(filename) as f:
                    content = f.readline().split()
                    for item in content:
                        val = float(item)
                        idx = int(val/dw)
                        weights[idx] += 1
                        count += 1
                    
        #weights /= float(count)
        
        #if logbins:
        #    weights /= step
                       
        with open(picklename, 'w') as f:
            pickle.dump(axis, f)
            pickle.dump(weights, f)
            
    return axis, weights

def smooth_tail(array, boundary = 500, window = 15):
    for i in range(boundary, len(array)):
        array[i] = np.sum(array[i-window/2:i+window/2])/window
    return array

def main():
    linear = True
    picspath = '/home/evgeny/Dropbox/pinch_thinout'
    ptype = 'el'
    
    if linear:
        dirs = ['27pw_simple', '27pw_default', '27pw_leveling', '27pw_conservation', '27pw_number', '27pw_merge', '27pw_energy' ]
        #dirs = ['27pw_merge']
        iterations = [2600]
        ratios = [1e-3]
        titles = ['simple', 'globalLev', 'leveling', 'conserve', 'number', 'merge', 'energy' ]
        filename = "cmp_pinch_thinout_weight_%s.png" % ptype
    else:
        dirs = ['27pw_default', '27pw_conservation', '27pw_merge', '27pw_energy', '27pw_number', '27pw_leveling']
        iterations = [3200]
        ratios = [1e-3]
        titles = ['globalLev', 'conserve', 'merge', 'energy', 'number', 'leveling']
        filename = "cmp_pinch_thinout_weight_%s_nl.png" % ptype
    configs = []
    numdirs = len(dirs)
    
    mp.rcParams.update({'font.size': 12})
    max_factor = 269
    lw = 0.8
    logfactor = 1.1
    initial = 4.76837e-06
    f = open('factors_%s.dat' % ptype, 'w')
    f.write("%10s\tFactor*\tAverage\tSigma\tMax factor\n" % "Method")
    for i in range(numdirs):
        config = utils.get_config(dirs[i] + "/ParsedInput.txt")
        nx = int(config['MatrixSize_X'])
        ny = int(config['MatrixSize_Y'])
        axis, weights = read_weights(dirs[i], iterations[0], prefix = 'logbins_%g_weights' % logfactor, ptype = ptype, step = 50., logbins = True)
        axis_ph, weights_ph = read_weights(dirs[i], iterations[0], prefix = 'logbins_%g_weights' % logfactor, ptype = 'ph', step = 50., logbins = True)
        #if max_factor == 0:
        #    max_factor = np.sum(axis * weights)/np.sum(weights)
            #f.write('%g\n' % max_factor)
        #print max_factor
        
        

        #print 'Sum', np.sum(weights)
        axis /= max_factor
        axis_ph /= max_factor

        weights /= 0.1*axis
        weights_ph /= 0.1*axis_ph
        
        av_factor = np.sum(axis * weights)/np.sum(weights)
        sigma_factor = np.sqrt(np.sum((axis - av_factor)**2 * weights))
        
        av_factor_ph = np.sum(axis_ph * weights_ph)/np.sum(weights_ph)
        sigma_factor_ph = np.sqrt(np.sum((axis_ph - av_factor_ph)**2 * weights_ph))
       
        read, ne_z = utils.bo_file_load(dirs[i] + '/' + utils.nezpath,iterations[0]/10,nx,ny, verbose=0)
        factor = np.amax(ne_z/2)
        print "\\textit{%s} &" % titles[i], "\\begin{tabular}{@{}c@{}} %.4g \\\\ %.4g \\end{tabular}" % (axis[0]*max_factor/initial, axis[-1]*max_factor/initial), " & ", "\\begin{tabular}{@{}c@{}} %.4g \\\\ %.4g \\end{tabular}" % (axis_ph[0]*max_factor/initial, axis_ph[-1]*max_factor/initial), " & ", "%.4g" % av_factor, " & ", "%.4g" % av_factor_ph, " & ", "%.4g" % sigma_factor, " & ", "%.4g" % sigma_factor_ph, " \\\\"        
        #print factor, av_factor, sigma_factor
        f.write('%10s\t%.4g\t%.4g\t%.4g\t%.4g\n' % (titles[i], factor/max_factor, av_factor, sigma_factor, axis[-1]))
        #f.write('%10s\t%.4g\t%.4g\t%.4g\t%.4g\n' % (titles[i], factor/max_factor, av_factor, sigma_factor, axis[-1]))
    f.close()

    
if __name__ == '__main__':
    main()
