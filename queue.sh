#!/bin/bash
clear
#remote host mvs100k
user=evefim
host=mvs100k.jscc.ru
echo -e "\n100k - evefim"
echo "-------------"
ssh ${user}@${host} "source .bashrc; mqinfo | grep evefim" 2> /dev/null
echo -e "\n100k - alba"
echo "-------------" 
ssh ${user}@${host} "source .bashrc; mqinfo | grep alba" 2> /dev/null

#remote host mvs10p
user=evefim
host=mvs1p1.jscc.ru
echo -e "\n10p - evefim"
echo "-------------"
ssh ${user}@${host} "source /nethome/evefim/.bashrc; mqinfo | grep evefim" 2> /dev/null
echo -e "\n10p - alba"
echo "-------------"
ssh ${user}@${host} "source /nethome/evefim/.bashrc; mqinfo | grep alba" 2> /dev/null

#remote host akka
user=argon
host=abisko.hpc2n.umu.se
echo -e "\nabisko - argon"
echo "-------------"
ssh ${user}@${host} "squeue | grep -n argon"

