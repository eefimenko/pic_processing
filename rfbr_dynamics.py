#!/usr/bin/python
from matplotlib import gridspec
import matplotlib as mp
#mp.use('Agg')
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import utils
import sys
import os
from tqdm import *
from pylab import *

def make_patch_spines_invisible(ax):
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)
        
def average(field):
    field = (field + field[:, ::-1] + field[::-1, :] + field[::-1, ::-1])/4.
    return field

def plot_zx(ax, ax1, b_field, el_field, ph_field, cmap_el, cmap_ph, nx, ny, xmin, xmax, ymin, ymax, bound, fontsize, x_label ='$x/\lambda$', y_label = '$z/\lambda$', pos_label = '(a)', circle_r = None):
    ax.set_xlim([-bound,bound])
    ax.set_ylim([-bound,bound])
    ax.set_xticks([-1, -0.5, 0, 0.5, 1])
    ax.set_yticks([-1, -0.5, 0, 0.5, 1])
    ax.set_xticklabels([])
    ax.set_yticklabels(['-1', '',  '0', '', '1'])
    ax1.set_xlim([-bound,bound])
    ax1.set_ylim([-bound,bound])
    ax1.set_xticks([-1, -0.5, 0, 0.5, 1])
    ax1.set_yticks([-1, -0.5, 0, 0.5, 1])
    ax1.set_xticklabels([])
    ax1.set_yticklabels(['-1', '',  '0', '', '1'])
    
    max_ = np.amax(b_field)
    min_ = np.amin(b_field)
    mf = max(max_, abs(min_))
    
    ax.imshow(b_field, origin = 'lower', cmap='bwr', extent=[xmin,xmax,ymin,ymax], vmin=-mf, vmax = mf, alpha = 0.3)

    m_el = np.amax(el_field)
    m_ph = np.amax(ph_field)
    print(m_el)
    ph_field = average(ph_field)
    
    surf_ph = ax1.imshow(ph_field, origin = 'lower', cmap=cmap_ph, vmin = 1e-4*m_ph, vmax = m_ph, norm=clr.LogNorm(), extent=[xmin,xmax,ymin,ymax])
    surf_el = ax.imshow(el_field, origin = 'lower', cmap=cmap_el, vmin = 1e-4*m_el, vmax = m_el, norm=clr.LogNorm(), extent=[xmin,xmax,ymin,ymax])

    #ax.axvline(x=0, color = 'k', dashes = [2,2])
    if circle_r is None:
        ax.axvline(x=-0.25, color = 'k')
        ax.axvline(x=0.25, color = 'k')
    else:
        circ = plt.Circle((0, 0), radius=circle_r, color='k', fill=False, linewidth = 1)
        ax.add_patch(circ)
        circ = plt.Circle((0, 0), radius=circle_r, color='k', fill=False, linewidth = 1)
        ax1.add_patch(circ)
        
    ax.text(-bound, bound-0.25, pos_label)
    #ax.text(-bound + 0.05, -bound + 0.05, '$e^{-}$', fontsize = fontsize - 4)
    #ax1.text(bound-0.5, -bound+0.05, '$\hbar\omega$', fontsize = fontsize - 4)
    ax.set_ylabel(y_label)
    ax1.set_ylabel(y_label)
        
    if x_label is not None:
        ax.set_xlabel(x_label)
        ax.set_xticklabels(['-1', '',  '0', '', '1'])
        ax1.set_xlabel(x_label)
        ax1.set_xticklabels(['-1', '',  '0', '', '1'])
    
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf_el,  orientation  = 'vertical', cax=cax)
    cbar.ax.tick_params(labelsize=fontsize-4)
    
    
    cbar.set_ticks([0.01, 0.1, 1, 10, 100])
    cbar.set_ticklabels(['0.1', '1','10','100', '1000']) # it is correct!
    cbar.ax.text(0.5,1.03, '$n_{e^-}/n_c$', fontsize = fontsize-4)
    
    divider = make_axes_locatable(ax1)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf_ph,  orientation  = 'vertical', cax=cax)
    cbar.ax.tick_params(labelsize=fontsize-4)
    cbar.ax.text(1.5,1.03, '$n_{\hbar\omega}$', fontsize = fontsize-4)
    

def plot_time(axis_2, axis_4, ax, ax2, ne, npos, nph, bz, lin_bound, fontsize, pos_label = '(c)', x_axis = False):
    
    axis_2.set_yscale('linear')
    #axis_2.set_ylim((lin_bound, 850))
    #axis_2.set_yticks([100, 200, 300, 400, 500, 600, 700])
    #axis_2.set_yticklabels(['100', '', '300', '', '', '600', ''])
    axis_2.set_ylabel('$\\frac{n_{e^{-,+}}}{n_c}$', fontsize = fontsize)
    axis_2.spines['bottom'].set_visible(False)
    axis_2.yaxis.set_ticks_position('left')
    axis_2.yaxis.set_label_position('left')
    #axis_2.xaxis.set_visible(False)
    ne3_, = axis_2.plot(ax2, ne, 'g')
    np3_, = axis_2.plot(ax2, npos, 'b')
    axis_2.set_yscale('log')
    axis_2.set_xlabel('Iteration')
    
    axis = axis_2.twinx()
    axis.yaxis.set_ticks_position('right')
    axis.yaxis.set_label_position('right')
    axis.yaxis.tick_right()
    axis.set_yticks([])
    
    bz3_, = axis.plot(ax2, np.abs(bz)/np.amax(bz), 'grey', alpha=0.5)
    plt.legend([ne3_, np3_, bz3_],
               ['$n_{e^-}$', '$n_{e^+}$', '|$B_z$|'],
               loc = 'upper left',
               frameon = False, ncol=2, columnspacing=0.5,
               labelspacing=0.1,
               fontsize = fontsize-2, #bbox_to_anchor=(0.25,0.68),
               handlelength=1.5, handletextpad=0.1)
    #divider = make_axes_locatable(axis_2)
    #axis_3 = divider.append_axes("bottom", size=.9, pad=0, sharex=axis_2)
    #axis_3.set_yscale('log')
    #axis_3.set_ylim((0.05, lin_bound))
    #axis_3.set_yticks([0.1, 1, 10])
    #axis_3.set_yticklabels(['0.1', '1', '10'])
    #axis_3.spines['top'].set_visible(False)
    
    #axis_3.yaxis.set_ticks_position('left')
    #axis_3.yaxis.set_label_position('left')
    
    #ne3__, = axis_3.plot(ax, ne, 'g')
    #np3__, = axis_3.plot(ax, npos, 'b')
    
    #axis_3.set_xlim([0, 35])

    #axis_4 = axis_2.twinx()
    #axis_4.spines["right"].set_position(("axes", 1.))
    #make_patch_spines_invisible(axis_4)
    # Second, show the right spine.
    #axis_4.spines["right"].set_visible(True)
    #axis_4.yaxis.set_label_position('right')
    #axis_4.yaxis.set_ticks_position('right')
    nph3_, = axis_4.plot(ax, nph, color = 'orange')
    axis_4.set_yscale('log')
    #axis_4.set_ylim([0, 1.35])
    axis_4.set_ylabel('$n_{\hbar\omega}$, $\\times 10^{23} cm^{-3}$', fontsize = fontsize-4)
    axis_4.set_xlabel('$t/T$')
    
    axis = axis_4.twinx()
    axis.yaxis.set_ticks_position('right')
    axis.yaxis.set_label_position('right')
    axis.yaxis.tick_right()
    
    bz3_, = axis.plot(ax, np.abs(bz)/np.amax(bz), 'grey', alpha=0.5)

    #axis.set_xticks([0,5,10,15,20,25,30])
    #axis.set_xticklabels([])
    #axis_4.set_xlabel('$t/T$')
    #if x_axis:
    #    axis_3.text(30, 0.007, '$t/T$')
    #    axis.set_xticklabels(['','5', '', '15', '', '25', ''])
        
    axis.set_yticks([])
    #axis.set_ylim([0, 1.35])
    #axis.set_xlim([0, 35])
    
    #axis.text(0, 1.22, pos_label)
    plt.legend([nph3_, bz3_],
               ['$n_{\hbar\omega}$','|$B_z$|'],
               loc = 'upper left',
               frameon = False, ncol=2, columnspacing=0.5,
               labelspacing=0.1,
               fontsize = fontsize-2, #bbox_to_anchor=(0.25,0.68),
               handlelength=1.5, handletextpad=0.1)

def create_cmap(name):
    # make the colormaps
    cmap1 = mp.cm.get_cmap(name)
   
    cmap1._init() # create the _lut array, with rgba values
    alphas = np.ones(cmap1.N+3)
    for i in range(10):
        alphas[i] = 0.
    
    cmap1._lut[:,-1] = alphas
    cmap1.set_under('w', 0)
    cmap1.set_bad('w', False)
    
    return cmap1
    
def main():
    iter_compression = 600 # !!!!!
    if len(sys.argv) == 2:
        iter_compression = int(sys.argv[1])
    
    fontsize = 20
    picpath = '.'
    mp.rcParams.update({'font.size': fontsize})
        
    path = '.' 
    
    config = utils.get_config(path + "/ParsedInput.txt")
    
    archive = os.path.join(path,  'data.zip')
        
    wl = float(config['Wavelength'])
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    dt = float(config['TimeStep'])/float(config['Period'])*int(config['BOIterationPass'])
    xmax = float(config['X_Max'])/wl 
    xmin = float(config['X_Min'])/wl 
    ymax = float(config['Y_Max'])/wl
    ymin = float(config['Y_Min'])/wl
    zmax = float(config['Z_Max'])/wl
    zmin = float(config['Z_Min'])/wl

    prefix = ''
    ez_z_path = os.path.join(prefix, 'data/Ez_z')
    bz_z_path = os.path.join(prefix, 'data/Bz_z')
    bz_y_path = os.path.join(prefix, 'data/Bz_y')
    bz_x_path = os.path.join(prefix, 'data/Bz_x')
    ex_z_path = os.path.join(prefix, 'data/Ex_z')
    ex_y_path = os.path.join(prefix, 'data/Ex_y')
    el_z_path = os.path.join(prefix, 'data/Electron_z')
    pos_z_path = os.path.join(prefix, 'data/Positron_z')
    ph_z_path = os.path.join(prefix, 'data/Photon_z')
    ion_z_path = os.path.join(prefix, 'data/Ion_z')
    el_y_path = os.path.join(prefix, 'data/Electron_y')
    ph_y_path = os.path.join(prefix, 'data/Photon_y')
    pos_y_path = os.path.join(prefix, 'data/Positron_y')
    ion_y_path = os.path.join(prefix, 'data/Ion_y')
    el_x_path = os.path.join(prefix, 'data/Electron_x')
    ph_x_path = os.path.join(prefix, 'data/Photon_x')
    pos_x_path = os.path.join(prefix, 'data/Positron_x')
    ion_x_path = os.path.join(prefix, 'data/Ion_x')

    verbose = False
    window = 30
    bound = 1.5
    ratio = 1
     
    
    print('Read magnetic field in the center vs time')
    bz_5pw_t = utils.ts(path, bz_z_path, name='bz_arc', ftype = 'bin', 
                        tstype='center', shape = (nx,ny), verbose=verbose,
                        archive = archive)
    ne_5pw_t = utils.ts(path, el_z_path, name='el_arc', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose,
                        archive = archive)
    np_5pw_t = utils.ts(path, pos_z_path, name='pos_arc', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose,
                        archive = archive)
    nph_5pw_t = utils.ts(path, ph_z_path, name='ph_arc', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose,
                        archive = archive)/1e23
    
    print(max(bz_5pw_t))
    
    ax_5pw = utils.create_axis(len(bz_5pw_t), dt)
    ax_5pw_2 = utils.create_axis(len(bz_5pw_t), 1)
    cmap1 = create_cmap('Greens')
    cmap2 = create_cmap('YlOrBr')
    
    
    
    read, b_field_c_5pw = utils.bo_file_load(os.path.join(bz_y_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive)
    read, el_field_c_5pw = utils.bo_file_load(os.path.join(el_y_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive)
    read, ph_field_c_5pw = utils.bo_file_load(os.path.join(ph_y_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive)

    read, b_field_c_5pw_z = utils.bo_file_load(os.path.join(bz_z_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive)
    read, el_field_c_5pw_z = utils.bo_file_load(os.path.join(el_z_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive)
    read, ph_field_c_5pw_z = utils.bo_file_load(os.path.join(ph_z_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive)
        
    fig = plt.figure(figsize = (20, 10))
    
    ax3 = plt.subplot2grid((2,6),(0,0), colspan=2)
    ax4 = plt.subplot2grid((2,6),(1,0), colspan=2)
    plot_zx(ax3, ax4, b_field_c_5pw, el_field_c_5pw, ph_field_c_5pw, cmap1, cmap2, nx, ny, xmin, xmax, ymin, ymax, bound, fontsize, pos_label = '', circle_r = 1)
    
    ax3 = plt.subplot2grid((2,6),(0,2), colspan=2)
    ax4 = plt.subplot2grid((2,6),(1,2), colspan=2)
    plot_zx(ax3, ax4, b_field_c_5pw_z, el_field_c_5pw_z, ph_field_c_5pw_z, cmap1, cmap2, nx, ny, xmin, xmax, ymin, ymax, bound, fontsize,  pos_label = '', y_label = '$y/\lambda$', circle_r = 1)

    lin_bound = 10
    
    axis_2 = plt.subplot2grid((2,6),(0,4), colspan=2)
    axis_4 = plt.subplot2grid((2,6),(1,4), colspan=2)
    plot_time(axis_2, axis_4, ax_5pw, ax_5pw_2, ne_5pw_t, np_5pw_t, nph_5pw_t, bz_5pw_t, lin_bound, fontsize, pos_label = '')
        
    plt.tight_layout()
    plt.subplots_adjust(hspace=0.17, wspace=1.5, right=0.92, left = 0.05)
    
    picname = os.path.join(picpath, 'rfbr_dynamics_%d.png' % iter_compression)
    print(picname)
    plt.savefig(picname, dpi=128)

if __name__ == '__main__':
    main()
