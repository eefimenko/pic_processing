#include <stdio.h>
#include <zlib.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <stdio.h>
#include <unistd.h>

void swap(float* buf)
{
    char* item = (char*)buf;
    float tmp = *buf;
    char* tmparray = (char*)&tmp;
    item[0] = tmparray[3];
    item[1] = tmparray[2];
    item[2] = tmparray[0];
    item[3] = tmparray[1];
}

int main(int argc, char* argv[])
{
    FILE* in, *out;
    char str[1024];
    int N = 10;
    
    if (NULL == (in = fopen(argv[1], "r")))
    {
	fprintf(stderr, "Unknown file %s\n", argv[1]);
	exit(-1);
    }
    sprintf(str, "fixed_%s", argv[1]);
    if (NULL == (out = fopen(str, "w")))
    {
	fprintf(stderr, "Unknown file %s\n", str);
	exit(-1);
    }
    for (int i=0; i<N; i++)
    {
	fgets(str, 1024, in);
	if (i == N-2)
	{
	    fprintf(out, "SCALARS density float 1\n");
	}
	else
	{
	    fprintf(out, "%s", str);
	}
    }
    
    float val;
    while (!feof(in))
    {
	fread(&val, sizeof(float), 1, in);
	swap(&val);
	fwrite(&val, sizeof(float), 1, out);
    }
}
