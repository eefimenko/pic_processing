#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_gamma(filename):
    f = open(filename, 'r')
    tmp = f.readline().split()
    return float(tmp[0]), float(tmp[1]), float(tmp[2]), float(tmp[3]), float(tmp[4]), float(tmp[5]), float(tmp[6]), float(tmp[7]), float(tmp[8])

def read_ne(filename):
    f = open(filename, 'r')
    tmp = f.readline().split()
    return float(tmp[0])

def read_ne(filename):
    f = open(filename, 'r')
    tmp = f.readline().split()
    return float(tmp[0])

def main():
    n = len(sys.argv) - 1
    power = []
    avgamma = []
    av10gamma = []
    maxne = []
    conc = []
    conc1 = []
    conc2 = []
    g_min = []
    g_max = []
    n_cr = 2.15e21 
    gf = open('gamma_av.dat', 'w')
    for i in range(n):
        path = sys.argv[i+1]
        
#        pwr, gmm, gmm10, cn, cn1, cn2,  phase, gmin, gmax = read_gamma(path + '/gamma_av.dat')
#        ne = read_ne(path + '/max_ne.dat')
#        maxne.append(ne)
#        avgamma.append(gmm)
#        av10gamma.append(gmm10)
#        power.append(pwr)
#        conc.append(cn)
#        conc1.append(cn1)
#        conc2.append(cn2)
#        g_min.append(gmin)
#        g_max.append(gmax)
#        gf.write('%lf %lf %lf %le %le %le %le %lf %lf\n' % (pwr, gmm, gmm10, ne, cn, cn1, cn2, gmin, gmax))
    gf.close()
    
    fig = plt.figure(num=None)
    ax2 = fig.add_subplot(2,1,1)
#    p, = ax1.plot(power, avgamma, 'r')
#    p1, = ax1.plot(power, av10gamma, 'b')
#    p5, = ax1.plot(power, g_min, 'y')
#    p6, = ax1.plot(power, g_max, 'c')
#    ax2 = ax1.twinx()
    p3, = ax2.plot(power, maxne, 'g')
    p4, = ax2.plot(power, conc, 'k')
    p5, = ax2.plot(power, conc1, 'r')
    p6, = ax2.plot(power, conc2, 'b')
    ax2.set_yscale('log')
    ax2.set_xscale('log')
    ax2.set_xlim([9,100])
    plt.legend([p3, p4, p5, p6], ['Ne', 'conc', 'conc1', 'conc2'], loc=2)
    ax2.set_xlabel('Power, PW')
    ax2.set_ylabel('Gamma')
    ax2.set_ylabel('Ne, cm-3')
    nn = [0]*len(power)
    for i in range(len(power)):
        nn[i] = maxne[i]/n_cr/math.pow(av10gamma[i], 1.)
    ax3 = fig.add_subplot(2,1,2)
    ax3.plot(power, nn, 'r')
    plt.show()

if __name__ == '__main__':
    main()
