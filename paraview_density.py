# state file generated using paraview version 5.0.1

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

#### import the simple module from the paraview
from paraview.simple import *
from subprocess import call
import os

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

iterations = range(5500,6000,10)
print iterations

for i in iterations:
    # Create a new 'Render View'
    renderView1 = CreateView('RenderView')
    renderView1.ViewSize = [910, 518]
    renderView1.AxesGrid = 'GridAxes3DActor'
    renderView1.CenterOfRotation = [4.830368652619654e-05, 5.9088500165671576e-05, 6.306605428108014e-05]
    renderView1.StereoType = 0
    renderView1.CameraPosition = [0.00019403893495286887, -0.00012090849213220324, 0.0002878084002149203]
    renderView1.CameraFocalPoint = [4.830368652619638e-05, 5.9088500165671596e-05, 6.306605428108016e-05]
    renderView1.CameraViewUp = [-0.40781871979144335, -0.823251585329652, -0.3948933008293291]
    renderView1.CameraParallelScale = 3.896527256678499e-05
    renderView1.Background = [0.32, 0.34, 0.43]
    os.chdir("VTKOutput")
    call(["gunzip", "Electron_density_%d.vtk.gz"%(i)])
    call(["/home/evgeny/work/picador/pic_processing/fixVtk/fixVtk", "Electron_density_%d.vtk"%(i)])
    os.chdir("..")
    infile = 'VTKOutput/fixed_Electron_density_%d.vtk'%(i)
    # create a new 'Legacy VTK Reader'
    density = LegacyVTKReader(FileNames=infile)
    
    amin, amax = density.PointData.GetArray('density').GetRange()
    print amax
    # exit(-1)
    # create a new 'Contour'
    
    contour1 = Contour(Input=density)
    contour1.ContourBy = ['POINTS', 'density']
    contour1.Isosurfaces = [0.1*amax]
    contour1.PointMergeMethod = 'Uniform Binning'
    
    # ----------------------------------------------------------------
    # setup the visualization in view 'renderView1'
    # ----------------------------------------------------------------
    
    # show data from fixed_Electron_density_6000vtk
    #fixed_Electron_density_6000vtkDisplay = Show(density, renderView1)
    # trace defaults for the display properties.
    #fixed_Electron_density_6000vtkDisplay.Representation = 'Outline'
    #fixed_Electron_density_6000vtkDisplay.ColorArrayName = ['POINTS', '']
    #fixed_Electron_density_6000vtkDisplay.GlyphType = 'Arrow'
    #fixed_Electron_density_6000vtkDisplay.ScalarOpacityUnitDistance = 1.7320508075688778e-06
    #fixed_Electron_density_6000vtkDisplay.Slice = 255
    
    # show data from contour1
    contour1Display = Show(contour1, renderView1)
    # trace defaults for the display properties.
    contour1Display.ColorArrayName = [None, '']
    contour1Display.GlyphType = 'Arrow'
    WriteImage("test_%d.png"%(i))
    Delete(contour1)
    Delete(renderView1)
    Delete(density)
    
