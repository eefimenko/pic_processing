#!/usr/bin/python
import matplotlib as mp
mp.use("Agg")
import matplotlib.pyplot as plt
import sys
import math
import utils
from tqdm import *
import numpy as np

def read_conc(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    return tmp[0]

def read_field(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def create_pulse(n,step,amp, tp, delay):
    dt = step
    ans = []
#    tp = 30
    tau = tp/1.1437
#    delay = 23.1099984185576458468467817
    for i in range(n):
        t = i*dt-delay
        if t > 0 and t < math.pi * tau:
            tmp = math.sin(t/tau)
            f = amp*tmp*tmp
        else:
            f = 0.
        ans.append(f)
    return ans

def main():

    config = utils.get_config("ParsedInput.txt")
#    duration = float(config['Duration']) #s
    delay = float(config['delay']) #s
    BOIterationPass = float(config['BOIterationPass'])
#    tp = duration*1e15
    tdelay = delay*1e15

    Xmax = float(config['X_Max'])*1e4 #mkm
    Xmin = float(config['X_Min'])*1e4 #mkm
    Ymax = float(config['Y_Max'])*1e4 #mkm
    Ymin = float(config['Y_Min'])*1e4 #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
   
    row = -1
    step = float(config['TimeStep'])*1e15*BOIterationPass
    print step
    epath = 'data/E2z'
    bpath = 'data/B2z'
    nepath = 'data/NeTrap'
    nppath = 'data/NposTrap'
    picspath = 'pics'
    necpath = 'data/ne'
    npcpath = 'data/npos'
    maxf = 0.
    maxn = 0.
  
    step1 = 5.656854249492380319955242562457442546/nx
    v = 2. * math.pi * step1 * step1 * 0.8e-4 * 1e-8
    axis1 = create_axis(nx, step/0.8)

    Ne = []
    Npos = []
    nect = []
    npct = []
    nmin,nmax = utils.find_min_max_from_directory(nepath)
    print nmax
    for i in tqdm(range(nmin,nmax)):
#        print "%06d.txt" % (i,)
#        nename = nepath + '/' + "%06d.txt" % (i,)
#        npname = nppath + '/' + "%06d.txt" % (i,)
        e = utils.get(nepath, i, ftype = 'txt', func = lambda x: np.sum(x))
        p = utils.get(nppath, i, ftype = 'txt', func = lambda x: np.sum(x))
        Ne.append(e)
        Npos.append(p)

#        nename = necpath + '/' + "%06d.txt" % (i,)
#        npname = npcpath + '/' + "%06d.txt" % (i,)
#        e = read_conc(nename)
#        p = read_conc(npname)
#        for j in range(len(e)):
#            e[j] = e[j]/((j+0.5)*v)
#            p[j] = p[j]/((j+0.5)*v)
#        nect.append(max(e))

#        fig, ax1 = plt.subplots()
#        pe, = ax1.plot(axis1, e, 'g')
#        pp, = ax1.plot(axis1, p, 'b')
#        ax1.set_xlim(0., 1.)
#        ax1.legend([pe, pp], ["El", "Pos"], loc=1)
#        picname = picspath + '/' + "n%06d.png" % (i,)
#        plt.savefig(picname)
#        plt.close()

 #   pulse = create_pulse(nmax, step, 1, tp, tdelay)
    fig, ax1 = plt.subplots()
    axis = create_axis(nmax-nmin, step)
    pe, = ax1.plot(axis, Ne, 'g')
#    pe, = ax1.plot(axis, Ne, 'g')
    pb, = ax1.plot(axis, Npos, 'b')
#    ax2 = ax1.twinx()
#    pu, = ax2.plot(axis, pulse, 'r-')
    ax1.legend([pe], ["El"], loc=2)
    ax1.set_yscale('log')
    ax1.set_xlim(0., 160.)
    plt.savefig('pics/trapped.png')
    plt.close()
if __name__ == '__main__':
    main()
