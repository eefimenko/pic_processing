#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import matplotlib as mp
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from matplotlib import cm

def read_file(filename):
    a = []
    f = open(filename, 'r')
    for line in f:
        a.append(float(line))
    return a

def main():
    n = len(sys.argv) - 1
    mp.rcParams.update({'font.size': 16})
  

    fig1 = plt.figure(num=None)
    ax11 = fig1.add_subplot(1,2,1)
    ax11.set_yscale('log')
    ax11.set_ylim([1e-6, 3e-3])
    ax11.set_xlim([0, 4000])
    ax11.set_xlabel('$\gamma$')
    ax11.set_ylabel('$dN/d\gamma$')
    ax21 = fig1.add_subplot(1,2,2)
    ax21.set_yscale('log')
    ax21.set_ylim([1e-6, 3e-3])
    ax21.set_xlim([0, 4000])
    ax21.set_xlabel('$\gamma$')
    ax21.set_ylabel('$dN/d\gamma$')

    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(1,2,1)
    ax1.set_xlim([0, 2])
    ax1.set_xlabel('$r/\lambda$')
    ax1.set_ylabel('$dN/dr$')
    ax2 = fig.add_subplot(1,2,2)
    ax2.set_yscale('log')
    ax2.set_ylim([1e-6, 3e-3])
    ax2.set_xlim([0, 4000])
    ax2.set_xlabel('$\gamma$')
    ax2.set_ylabel('$dN/d\gamma$')
    power = np.zeros(n)
  
    nmin = []
    nmax = []

    rsurf = []
    gsurf = []
    for i in range(n):
        path = sys.argv[i+1]
        config = utils.get_config(path + "/ParsedInput.txt")
        pwr = float(config['PeakPowerPW'])
        power[i] = pwr
        rmax = float(config['ElGammaR.SetBounds_1'])
        nr = int(config['ElGammaR.SetMatrixSize_0'])
        wl = float(config['Wavelength'])
        dr = rmax/nr/wl
        axr = utils.create_axis(nr, dr)
        gmax = float(config['ElGammaR.SetBounds_3'])
        ng = int(config['ElGammaR.SetMatrixSize_1'])
        dg = gmax/ng
        axg = utils.create_axis(ng, dg)
        el_r = read_file(path + "/el_r.txt")
        
        el_g = read_file(path + "/el_g.txt")
       
        el_g_min = read_file(path + "/el_g_min.txt")
        el_g_max = read_file(path + "/el_g_max.txt")
        n = sum(el_g)
        for k in range(len(el_g)):
            el_g[k] /= n*dg
        n = sum(el_g_min)
        nmin.append(n)
        for k in range(len(el_g_min)):
            el_g_min[k] /= n*dg
        n = sum(el_g_max)
        nmax.append(n)
        for k in range(len(el_g_max)):
            el_g_max[k] /= n*dg
        n = sum(el_r)
        for k in range(len(el_r)):
            el_r[k] /= n*dr
        rsurf.append(el_r)
        gsurf.append(el_g)
        print len(axr), len(el_r)
        ax1.plot(axr, el_r, label = str(pwr) + ' PW')
        ax2.plot(axg, el_g, label = str(pwr) + ' PW')
        ax11.plot(axg, el_g_min, label = str(pwr) + ' PW')
        ax21.plot(axg, el_g_max, label = str(pwr) + ' PW')
      
    ax1.legend()
    ax2.legend()
    ax11.legend()
    ax21.legend()
    plt.show()
   

if __name__ == '__main__':
    main()
