#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import sys
import math
import utils
import shutil
import os
import numpy as np

def read_field2d(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def read_bd(file):
    f = open(file, 'r')
    line = f.readline()
    tmp = [float(x) for x in line.split()]
    s = sum(tmp)
    if s > 0:
        for i in range(len(tmp)):
            tmp[i] /= s
    return tmp

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def main():

    config = utils.get_config("ParsedInput.txt")
    bpath = 'statdata/el/ElChiPh/'
    dpath = 'statdata/ph/PhChiEl/' 
    ezpath = 'data/E2z'
    bzpath = 'data/B2z'
    picspath = 'pics'
   
    delta = 1
    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(bpath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(bpath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax) + ' files'

    nx = int(config['QEDstatistics.nChi'])
    xmax = float(config['QEDstatistics.ChiPhMax'])
    nx1 = int(config['MatrixSize_X'])
    ny1 = int(config['MatrixSize_Y'])
    dx1 = float(config['Step_X'])
    dy1 = float(config['Step_Y'])
    wl = float(config['Wavelength'])
    peakpowerpw = int(config['PeakPowerPW'])

    dmy = int(0.35*wl/dy1)
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])

    T = 2 * math.pi/omega*1e15
    
    step = x0*y0*1e15/T
    nT = int(1./step)

    birth = []
    death = []
    ez_t = []
    bz_t = []

    sp_path = 'chi_gamma_%d' % (peakpowerpw)
    if not os.path.exists(sp_path):
        os.makedirs(sp_path)
    shutil.copy("ParsedInput.txt", sp_path)
    shutil.copy("Input.txt", sp_path)

    for i in range(nmin,nmax,delta):
        bname = bpath + '/' + "%.4f.txt" % (i,)
        print bname
        b = read_bd(bname)
        birth.append(b)
        dname = dpath + '/' + "%.4f.txt" % (i,)
        print dname
        d = read_bd(dname)
        death.append(d)
        ezname = ezpath + '/' + "%06d.txt" % (i,)
        bzname = bzpath + '/' + "%06d.txt" % (i,)
        ez = read_field2d(ezname,nx1,ny1)
        bz = read_field2d(bzname,nx1,ny1)
        ezv = ez[nx1/2][ny1/2]
        bzv = bz[nx1/2][ny1/2-dmy]
        ez_t.append(ezv)
        bz_t.append(bzv)
    axis = create_axis(nmax-nmin,step,nmin*step)
    a = 0.1
    ezm = a*max(ez_t)
    bzm = a*max(bz_t)
    for i in range(len(ez_t)):
        ez_t[i] /= ezm
        bz_t[i] /= ezm
    fig = plt.figure(num=None)
    plt.rc('text', usetex=True)
    ax = fig.add_subplot(2,1,1)
    
    surf = ax.imshow(birth, extent=[0, xmax, nmin*step, (nmax-1)*step], aspect = 'auto',  origin = 'lower')
    ax.plot(ez_t,axis)
    ax.plot(bz_t, axis)
    ax.autoscale(False)
    ax.set_ylabel('t, T')
    ax.set_xlabel('$\chi$')
    ax.set_ylim([nmin*step, (nmax-1)*step])
    ax.set_xlim([0, 20])
#    plt.colorbar(surf,  orientation  = 'vertical')
    ax = fig.add_subplot(2,1,2)
    surf = ax.imshow(death, extent=[0, xmax, nmin*step, (nmax-1)*step], aspect = 'auto',  origin = 'lower')
#    ax.set_xlim([0, 20])
    ax.plot(ez_t,axis)
    ax.plot(bz_t, axis)
    ax.autoscale(False)
    ax.set_ylabel('t, T')
    ax.set_xlabel('$\chi$')
    ax.set_ylim([nmin*step, (nmax-1)*step]) 
    ax.set_xlim([0, 20])
#    plt.colorbar(surf,  orientation  = 'vertical')
    plt.savefig(sp_path + '/' + 'chi_%d.png'%peakpowerpw)
    plt.close()

    fb = open(sp_path + '/' + 'chi_birth_%d.dat'%peakpowerpw, 'w')
    fd = open(sp_path + '/' + 'chi_death_%d.dat'%peakpowerpw, 'w')
    for i in range(len(birth)):
        for j in range(len(birth[0])):
            fb.write('%e ' % birth[i][j])
            fd.write('%e ' % death[i][j])
        fb.write('\n')
        fd.write('\n')
    fb.close()
    fd.close()
    
    

if __name__ == '__main__':
    main()
