#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils
import os
#import numpy as np

def main():
    picspath = 'pics'
    path = './'
     
    config = utils.get_config(path + "ParsedInput.txt")
    nmin, nmax, delta = utils.get_min_max_iteration(sys.argv,path+utils.expath)

    xmax = float(config['X_Max'])*1e4 #mkm
    xmin = float(config['X_Min'])*1e4 #mkm
    ymax = float(config['Y_Max'])*1e4 #mkm
    ymin = float(config['Y_Min'])*1e4 #mkm
    zmax = float(config['Z_Max'])*1e4 #mkm
    zmin = float(config['Z_Min'])*1e4 #mkm
    extent = [xmin, xmax, ymin, ymax]
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    power = int(config['PeakPowerPW'])
    dx = float(config['Step_X'])*1e4
    dy = float(config['Step_Y'])*1e4
    dz = float(config['Step_Z'])*1e4
    mult = 1./(2.*dx*dy*dz*1e-12)
    spx = 2
    spy = 3
    for i in range(nmin,nmax,delta):
        print "\rSaving sc%06d.png, %.0f %% done" % (i,float(i-nmin)/(nmax-nmin)*100),
        sys.stdout.flush()
        picname = picspath + '/' + "series_jz%06d.png" % (i,)
        if os.path.exists(picname):
            continue
        fig = plt.figure(num=None, figsize=(20., 20.*spx/spy), dpi=256)
        mp.rcParams.update({'font.size': 14})
        
        utils.subplot(fig, i, path+utils.ezpath,
                      shape = (nx,ny), position = (spx,spy,1), extent = extent,
                      cmap = 'Reds', title = 'Electric field %d pw'%(power))
        utils.subplot(fig, i, path+utils.bzpath,
                      shape = (nx,ny), position = (spx,spy,2), extent = extent,
                      cmap = 'Reds', title = 'Magnetic field %d pw'%(power))
        utils.subplot(fig, i, path+utils.jz_zpath,
                      shape = (nx,ny), position = (spx,spy,3), extent = extent,
                      cmap = 'bwr', title = 'Jz %d pw'%(power))
        utils.subplot(fig, i, path+utils.nezpath,
                      shape = (nx,ny), position = (spx,spy,4), extent = extent,
                      cmap = 'Greens', title = 'Electrons %d pw'%(power), logarithmic = True, mult = mult)
        utils.subplot(fig, i, path+utils.npzpath,
                      shape = (nx,ny), position = (spx,spy,5), extent = extent,
                      cmap = 'Greens', title = 'Positrons %d pw'%(power), logarithmic = True, mult = mult)
        utils.subplot(fig, i, path+utils.npzpath,
                      shape = (nx,ny), position = (spx,spy,6), extent = extent,
                      cmap = 'Reds', title = 'Photons %d pw'%(power), logarithmic = True, mult = mult)
                  
        
        plt.tight_layout()
        plt.savefig(picname)
        plt.close()

    
if __name__ == '__main__':
    main()
