#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import sys
import math
import utils
import os
import pickle
import numpy as np

def read_gamma(filename):
    f = open(filename, 'r')
    tmp = f.readline().split()
    f.close()
    return float(tmp[0]), float(tmp[1]), float(tmp[2])

def max_and_ne(array, ne, n_cr):
    l_np = np.asarray(array)
    i = l_np.argmax()
    return array[i], ne[i]


def boundary(n_pos, n_tgt, ne):
    n_pos_norm = []
    
    for i in range(len(n_pos)):
        n_pos_norm.append(n_pos[i]/n_tgt[i])

    print(n_pos_norm)
    
    for i in range(0, len(n_pos)-1):
        if n_pos_norm[i] > 1. and n_pos_norm[i+1] < 1.:
            x = math.exp(math.log(ne[i]) * (1 - n_pos_norm[i+1])/(n_pos_norm[i] - n_pos_norm[i+1])
                         + math.log(ne[i+1]) * (n_pos_norm[i] - 1)/(n_pos_norm[i] - n_pos_norm[i+1]))
            print('Max boundary: ', x)
        if n_pos_norm[i] < 1. and n_pos_norm[i+1] > 1.:
            x = math.exp(math.log(ne[i]) * (n_pos_norm[i+1] - 1)/(n_pos_norm[i+1] - n_pos_norm[i])
                         + math.log(ne[i+1]) * (1 - n_pos_norm[i])/(n_pos_norm[i+1] - n_pos_norm[i]))
            print('Min boundary: ', x)
       
def main():
    path = sys.argv[1]
    structure = sys.argv[2]
    wave_type = sys.argv[3]
    size = sys.argv[4]
    save_path = sys.argv[5]

    dirs = ['nc_0.001', 'nc_0.01', 'nc_0.1', 'nc_1', 'nc_10','nc_30', 'nc_50', 'nc_100', 'nc_1000']
    ne = []
    efficiency_ph = []
    efficiency_el = []
    efficiency_pos = []
    efficiency_1gev_ph = []
    efficiency_1gev_el = []
    efficiency_1gev_pos = []
    number_ph = []
    number_el = []
    number_pos = []
    number_1gev_ph = []
    number_1gev_el = []
    number_1gev_pos = []
    number_tgt = []

    for d in dirs:
        ph_, el_, pos_ = read_gamma(os.path.join(path, wave_type, size, d, 'efficiency.txt'))
        efficiency_el.append(el_)
        efficiency_ph.append(ph_)
        efficiency_pos.append(pos_)
        ph_, el_, pos_ = read_gamma(os.path.join(path, wave_type, size, d, 'efficiency_1gev.txt'))
        efficiency_1gev_el.append(el_)
        efficiency_1gev_ph.append(ph_)
        efficiency_1gev_pos.append(pos_)
        ph_, el_, pos_ = read_gamma(os.path.join(path, wave_type, size, d, 'number.txt'))
        number_el.append(el_)
        number_ph.append(ph_)
        number_pos.append(pos_)
        ph_, el_, pos_ = read_gamma(os.path.join(path, wave_type, size, d, 'number_1gev.txt'))
        number_1gev_el.append(el_)
        number_1gev_ph.append(ph_)
        number_1gev_pos.append(pos_)
        config = utils.get_config(os.path.join(path, wave_type, size, d, 'ParsedInput.txt'))
        n_cr = float(config['n_cr'])
        ne_ = float(config['Ne'])
        r = float(config['R'])
        ne.append(ne_/n_cr)
        number_tgt.append(4./3.* math.pi * r**3 * ne_)
        
    with open(os.path.join(save_path, 'efficiency_ph_%s_%s_%s.dat' % (structure, wave_type, size)), 'w') as f:
        for i in range(len(ne)):
            f.write('%e %e %e\n' % (ne[i], efficiency_ph[i], efficiency_1gev_ph[i]))
    with open(os.path.join(save_path, 'efficiency_el_%s_%s_%s.dat' % (structure, wave_type, size)), 'w') as f:
        for i in range(len(ne)):
            f.write('%e %e %e\n' % (ne[i], efficiency_el[i], efficiency_1gev_el[i]))
    with open(os.path.join(save_path, 'efficiency_pos_%s_%s_%s.dat' % (structure, wave_type, size)), 'w') as f:
        for i in range(len(ne)):
            f.write('%e %e %e\n' % (ne[i], efficiency_pos[i], efficiency_1gev_pos[i]))
            
    with open(os.path.join(save_path, 'number_ph_%s_%s_%s.dat' % (structure, wave_type, size)), 'w') as f:
        for i in range(len(ne)):
            f.write('%e %e %e\n' % (ne[i], number_ph[i], number_1gev_ph[i]))
    with open(os.path.join(save_path, 'number_el_%s_%s_%s.dat' % (structure, wave_type, size)), 'w') as f:
        for i in range(len(ne)):
            f.write('%e %e %e\n' % (ne[i], number_el[i], number_1gev_el[i]))
    with open(os.path.join(save_path, 'number_pos_%s_%s_%s.dat' % (structure, wave_type, size)), 'w') as f:
        for i in range(len(ne)):
            f.write('%e %e %e\n' % (ne[i], number_pos[i], number_1gev_pos[i]))

    print('Efficiency', max_and_ne(efficiency_ph, ne, n_cr))
    print('Number', max_and_ne(number_pos, ne, n_cr)[0]/1e12, max_and_ne(number_pos, ne, n_cr)[1])
    print('Efficiency 1 Gev', max_and_ne(efficiency_1gev_ph, ne, n_cr))
    print('Number 1 GeV', max_and_ne(number_1gev_pos, ne, n_cr)[0]/1e9, max_and_ne(number_pos, ne, n_cr)[1])
    boundary(number_pos, number_tgt, ne)
    
    with open(os.path.join(save_path, 'dump_%s_%s_%s.pkl' % (structure, wave_type, size)), 'w') as f:
        pickle.dump(ne, f)
        pickle.dump(number_tgt, f)
        pickle.dump(efficiency_ph, f)
        pickle.dump(efficiency_1gev_ph, f)
        pickle.dump(efficiency_el, f)
        pickle.dump(efficiency_1gev_el, f)
        pickle.dump(efficiency_pos, f)
        pickle.dump(efficiency_1gev_pos, f)
        pickle.dump(number_ph, f)
        pickle.dump(number_1gev_ph, f)
        pickle.dump(number_el, f)
        pickle.dump(number_1gev_el, f)
        pickle.dump(number_pos, f)
        pickle.dump(number_1gev_pos, f)
            
    
            
        
        

if __name__ == '__main__':
    main()
