#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import os
import numpy as np

def read_profiles(path,i):
    xy2d_path = 'BasicOutput/data/XZ2D'
    config = utils.get_config(path+"/ParsedInput.txt")
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    wl = float(config['Wavelength'])
    dx = float(config['GridStep'])
    dy = float(config['GridStep'])
    dz = float(config['GridStep'])
    Xmax = float(config['X_Max'])/wl #mkm to wavelength
    Xmin = float(config['X_Min'])/wl #mkm to wavelength
    
    path_ = os.path.join(path, xy2d_path)
    print(path_)
    read, fieldn = utils.bo_file_load(path_,i,nx,ny)
    nx_max, ny_max = np.unravel_index(np.argmax(fieldn, axis=None), fieldn.shape)
    print(np.amax(fieldn)/1.19e8, nx_max, ny_max)
    prof1 = fieldn[:,ny//2]
    prof2 = fieldn[nx_max,:]
    ax = utils.create_axis(nx, (Xmax-Xmin)/nx, Xmin)
    return ax, prof1, prof2

def main():
    dirs = ['shift_1_2022-06-15_18-48-53',
            'shift_2_2022-06-15_19-11-37',
            'shift_3_2022-06-15_19-30-27',
            'focus_0_0_0_shift_0.41_dist_2.82_x2',
            'shift_4_2022-06-15_19-48-05',
            'shift_5.6_2022-06-15_17-14-21']
    iterations = [40, 50, 50, 50, 57, 63]
    n = len(dirs)

    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(1,2,1)
    ax2 = fig.add_subplot(1,2,2)
    
    for i in range(n):
        path = dirs[i]
        iteration = iterations[i]
        axis, p1, p2 = read_profiles(path, iteration)
        ax1.plot(axis, p1, label = path)
        ax2.plot(axis, p2/np.amax(p2), label = path)
        print(np.argmax(p1), np.max(p1)/1.19e8, (len(p1)/2 - np.argmax(p1))*(axis[1]-axis[0]))
        #ax1.imshow(f)
    ax1.legend()
    plt.show()

if __name__ == '__main__':
    main()
