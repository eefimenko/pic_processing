#!/usr/bin/python
from matplotlib import gridspec
import matplotlib as mp
#mp.use('Agg')
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import utils
import sys
import os
from tqdm import *
from pylab import *

def main():
    fontsize = 32
    picpath = '/home/evgeny/Dropbox/m-dipole'
    mp.rcParams.update({'font.size': fontsize})
    
    path_15pw = 'DipoleB_Wire_1_2021-05-21_21-15-52'
    path_10pw = 'DipoleB_Wire__2021-05-21_17-24-09'
    path_3pw = 'DipoleB_Wire__2021-04-22_23-51-45' 
    config_15pw = utils.get_config(path_15pw + "/ParsedInput.txt")
    config_10pw = utils.get_config(path_15pw + "/ParsedInput.txt")
    config_3pw = utils.get_config(path_15pw + "/ParsedInput.txt")
    
    wl = float(config_10pw['Wavelength'])
    nx = int(config_10pw['MatrixSize_X'])
    ny = int(config_10pw['MatrixSize_Y'])
    dt = float(config_10pw['TimeStep'])/float(config_10pw['Period'])*int(config_10pw['BOIterationPass'])
    xmax = float(config_10pw['X_Max'])/wl 
    xmin = float(config_10pw['X_Min'])/wl 
    ymax = float(config_10pw['Y_Max'])/wl
    ymin = float(config_10pw['Y_Min'])/wl
    zmax = float(config_10pw['Z_Max'])/wl
    zmin = float(config_10pw['Z_Min'])/wl
    
    bz_z_path = 'BasicOutput/data/Bz_z'
    bz_y_path = 'BasicOutput/data/Bz_y'
    ex_z_path = 'BasicOutput/data/Ex_z'
    ex_y_path = 'BasicOutput/data/Ex_y'
    el_z_path = 'BasicOutput/data/Electron_z'
    pos_z_path = 'BasicOutput/data/Positron_z'
    ion_z_path = 'BasicOutput/data/Ion_z'
    el_y_path = 'BasicOutput/data/Electron_y'
    pos_y_path = 'BasicOutput/data/Positron_y'
    ion_y_path = 'BasicOutput/data/Ion_y'
    verbose = False
    window = 30
    bound = 1.5
    ratio = 1
    
    print 'Read magnetic field in the center vs time!'
    bz_15pw_t = utils.ts(path_15pw, bz_z_path, name='bz', ftype = 'bin', 
                         tstype='center', shape = (nx,ny), verbose=verbose)
    bz_10pw_t = utils.ts(path_10pw, bz_z_path, name='bz', ftype = 'bin', 
                         tstype='center', shape = (nx,ny), verbose=verbose)
    bz_3pw_t = utils.ts(path_3pw, bz_z_path, name='bz', ftype = 'bin', 
                         tstype='center', shape = (nx,ny), verbose=verbose)
    ex_15pw_t = utils.ts(path_15pw, ex_z_path, name='ex', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose)
    ne_15pw_t = utils.ts(path_15pw, el_z_path, name='el', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose)
    np_15pw_t = utils.ts(path_15pw, pos_z_path, name='pos', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose)
    ne_10pw_t = utils.ts(path_10pw, el_z_path, name='el', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose)
    np_10pw_t = utils.ts(path_10pw, pos_z_path, name='pos', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose)
    ne_3pw_t = utils.ts(path_3pw, el_z_path, name='el', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose)
    np_3pw_t = utils.ts(path_3pw, pos_z_path, name='pos', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose)
    
    ax_15pw = utils.create_axis(len(bz_15pw_t), dt)
    ax_10pw = utils.create_axis(len(bz_10pw_t), dt)
    ax_3pw = utils.create_axis(len(bz_3pw_t), dt)
    bz_15pw_env_t = utils.env(bz_15pw_t,ax_15pw)
    bz_10pw_env_t = utils.env(bz_10pw_t,ax_10pw)

    # make the colormaps
    cmap1 = mp.cm.get_cmap('Greens')
    cmap1.set_under('w', 0)
    cmap1.set_bad('w', False)
    #cmap1 = mpl.colors.LinearSegmentedColormap.from_list('my_cmap',['green','blue'],256)
    #cmap2 = mpl.colors.LinearSegmentedColormap.from_list('my_cmap2',[color1,color2],256)

    cmap1._init() # create the _lut array, with rgba values

    # create your alpha array and fill the colormap with them.
    # here it is progressive, but you can create whathever you want
    alphas = np.ones(cmap1.N+3)
    for i in range(10):
        alphas[i] = 0.
    
    cmap1._lut[:,-1] = alphas
    cmap1.set_under('w', 0)
    cmap1.set_bad('w', False)
    
    iter_rings = 640
    iter_compression = 180
    
    print(iter_rings * dt)
    read, b_field = utils.bo_file_load(path_15pw + '/' + bz_y_path,iter_rings,nx,ny, ftype='bin', verbose=1, transpose=1)
    read, b_field_c = utils.bo_file_load(path_15pw + '/' + bz_y_path,iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1)
    read, el_field = utils.bo_file_load(path_15pw + '/' + el_y_path,iter_rings,nx,ny, ftype='bin', verbose=1, transpose=1)
    read, el_field_c = utils.bo_file_load(path_15pw + '/' + el_y_path,iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1)
    read, b_field_10pw = utils.bo_file_load(path_10pw + '/' + bz_y_path,iter_rings,nx,ny, ftype='bin', verbose=1, transpose=1)
    read, b_field_c_10pw = utils.bo_file_load(path_10pw + '/' + bz_y_path,iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1)
    read, el_field_10pw = utils.bo_file_load(path_10pw + '/' + el_y_path,iter_rings,nx,ny, ftype='bin', verbose=1, transpose=1)
    read, el_field_c_10pw = utils.bo_file_load(path_10pw + '/' + el_y_path,iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1)
    read, b_field_3pw = utils.bo_file_load(path_3pw + '/' + bz_y_path,iter_rings,nx,ny, ftype='bin', verbose=1, transpose=1)
    read, el_field_3pw = utils.bo_file_load(path_3pw + '/' + el_y_path,iter_rings,nx,ny, ftype='bin', verbose=1, transpose=1)
    read, b_field_c_3pw = utils.bo_file_load(path_3pw + '/' + bz_y_path,iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1)
    read, el_field_c_3pw = utils.bo_file_load(path_3pw + '/' + el_y_path,iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1)

    for i in range(iter_rings-window/2, iter_rings+window/2 + 1):
        if i == iter_rings:
            continue
        read, el_field_ = utils.bo_file_load(path_15pw + '/' + el_y_path,i,nx,ny, ftype='bin', verbose=1, transpose=1)
        read, el_field_10pw_ = utils.bo_file_load(path_10pw + '/' + el_y_path,i,nx,ny, ftype='bin', verbose=1, transpose=1)
        read, el_field_3pw_ = utils.bo_file_load(path_3pw + '/' + el_y_path,i,nx,ny, ftype='bin', verbose=1, transpose=1)
        el_field += el_field_
        el_field_10pw += el_field_10pw_
        el_field_3pw += el_field_3pw_

    el_field /= (window + 1)
    el_field_10pw /= (window + 1)
    el_field_3pw /= (window + 1)

    
    fig = plt.figure(figsize = (14, 15))
    ax2 = plt.subplot2grid((3,4),(0,0), colspan=2)

    ax2.set_xlim([-bound,bound])
    ax2.set_ylim([-bound,bound])
    ax2.set_yticks([-1, 0, 1])
    ax2.set_xticks([-1, -0.5, 0, 0.5, 1])
    ax2.set_xticklabels([])
    max_ = np.amax(b_field_3pw)
    min_ = np.amin(b_field_3pw)
    mf = max(max_, abs(min_))
    
    ax2.imshow(b_field_3pw[:,nx/2:-1], origin = 'lower', cmap='bwr', extent=[0,xmax,ymin,ymax], vmin=-mf, vmax = mf, alpha = 0.5)
    m = np.amax(el_field_3pw)
    #print(m)
    el_field_3pw = (el_field_3pw + el_field_3pw[:, ::-1] + el_field_3pw[::-1, :] + el_field_3pw[::-1, ::-1])/4.
    surf = ax2.imshow(el_field_3pw[:,nx/2:-1], origin = 'lower', cmap=cmap1, vmin = 1e-4*m, vmax = m*ratio, norm=clr.LogNorm(), extent=[0,xmax,ymin,ymax])
    max_ = np.amax(b_field_c_3pw)
    min_ = np.amin(b_field_c_3pw)
    mf = max(max_, abs(min_))
    ax2.imshow(b_field_c_3pw[:,:nx/2], origin = 'lower', cmap='bwr', extent=[xmin,0,ymin,ymax], vmin=-mf, vmax = mf, alpha = 0.5)
    surf = ax2.imshow(el_field_c_3pw[:,:nx/2], origin = 'lower', cmap=cmap1, vmin = 1e-4*m, vmax = m*ratio, norm=clr.LogNorm(), extent=[xmin,0,ymin,ymax])

    ax2.axvline(x=0, color = 'k', dashes = [2,2])
    ax2.axvline(x=-0.25, color = 'grey', dashes = [6,2,2,2])
    
    divider = make_axes_locatable(ax2)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)
    cbar.ax.tick_params(labelsize=fontsize-2)
    #cbar.set_ticks([0.01, 0.1,1,10, 100])
    #cbar.set_ticklabels(['0.01', '0.1','1','10', '100'])
    #ax2.set_ylabel('$z/\lambda$')
    #ax2.text(bound-0.9, -bound+0.05, '15 PW')
    #ax2.text(-bound, -bound+0.05, '10 PW')
    ax2.text(-bound, bound-0.25, '(a)')
    ax2.text(bound-0.8, bound-0.25, '3 PW')
    ax2.text(-bound + 0.05, -bound + 0.05, '$t=6T$', fontsize = fontsize - 4)
    ax2.text(bound-1., -bound+0.05, '$t=21T$', fontsize = fontsize - 4)
    ax2.text(-bound-0.5, bound-0.25, '$z/\lambda$')
    #ax2.set_xlabel('$x/\lambda$')
    #ax2.axvline(x=0, color='k', linewidth = 1., dashes = [3,3])
    #ax2.axvline(x=0.015, color='k', linewidth = 0.5)
    #ax2.axvline(x=-0.015, color='k', linewidth = 0.5)
    
    ax3 = plt.subplot2grid((3,4),(1,0), colspan=2, sharex = ax2)

    ax3.set_xlim([-bound,bound])
    ax3.set_ylim([-bound,bound])
    ax3.set_yticks([-1, 0, 1])
    ax3.set_xticks([-1, -0.5, 0, 0.5, 1])
    ax3.set_xticklabels([])
    max_ = np.amax(b_field_10pw)
    min_ = np.amin(b_field_10pw)
    mf = max(max_, abs(min_))
    
    ax3.imshow(b_field_10pw[:,nx/2:-1], origin = 'lower', cmap='bwr', extent=[0,xmax,ymin,ymax], vmin=-mf, vmax = mf, alpha = 0.5)
    m = np.amax(el_field)
    #print(m)
    el_field_10pw = (el_field_10pw + el_field_10pw[:, ::-1] + el_field_10pw[::-1, :] + el_field_10pw[::-1, ::-1])/4.
    surf = ax3.imshow(el_field_10pw[:,nx/2:-1], origin = 'lower', cmap=cmap1, vmin = 1e-4*m, vmax = m*ratio, norm=clr.LogNorm(), extent=[0,xmax,ymin,ymax])
    max_ = np.amax(b_field_c_10pw)
    min_ = np.amin(b_field_c_10pw)
    mf = max(max_, abs(min_))
    ax3.imshow(b_field_c_10pw[:,:nx/2], origin = 'lower', cmap='bwr', extent=[xmin,0,ymin,ymax], vmin=-mf, vmax = mf, alpha = 0.5)
    surf = ax3.imshow(el_field_c_10pw[:,:nx/2], origin = 'lower', cmap=cmap1, vmin = 1e-4*m, vmax = m*ratio, norm=clr.LogNorm(), extent=[xmin,0,ymin,ymax])
    ax3.axvline(x=0, color = 'k', dashes = [2,2])
    ax3.axvline(x=-0.25, color = 'grey', dashes = [6,2,2,2])
    divider = make_axes_locatable(ax3)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)
    cbar.ax.tick_params(labelsize=fontsize-2)
    #cbar.set_ticks([0.01, 0.1,1,10, 100])
    #cbar.set_ticklabels(['0.01', '0.1','1','10', '100'])
    #ax3.set_ylabel('$z/\lambda$')
    #ax2.text(bound-0.9, -bound+0.05, '15 PW')
    #ax2.text(-bound, -bound+0.05, '10 PW')
    ax3.text(-bound, bound-0.25, '(c)')
    ax3.text(bound-1, bound-0.25, '10 PW')
    ax3.text(-bound + 0.05, -bound + 0.05, '$t=6T$', fontsize = fontsize - 4)
    ax3.text(bound-1., -bound+0.05, '$t=21T$', fontsize = fontsize - 4)
    ax3.text(-bound-0.5, bound-0.25, '$z/\lambda$')
    #ax3.set_xlabel('$x/\lambda$')

    ax4 = plt.subplot2grid((3,4),(2,0), colspan=2)

    ax4.set_xlim([-bound,bound])
    ax4.set_ylim([-bound,bound])
    ax4.set_yticks([-1, 0, 1])
    ax4.set_xticks([-1, -0.5, 0, 0.5, 1])
    ax4.set_xticklabels(['-1', '', '0', '', '1'])
    max_ = np.amax(b_field)
    min_ = np.amin(b_field)
    mf = max(max_, abs(min_))
    
    ax4.imshow(b_field[:,nx/2:-1], origin = 'lower', cmap='bwr', extent=[0,xmax,ymin,ymax], vmin=-mf, vmax = mf, alpha = 0.5)
    m = np.amax(el_field)
    #print(m)
    surf = ax4.imshow(el_field[:,nx/2:-1], origin = 'lower', cmap=cmap1, vmin = 1e-4*m, vmax = m*ratio, norm=clr.LogNorm(), extent=[0,xmax,ymin,ymax])
    max_ = np.amax(b_field_c)
    min_ = np.amin(b_field_c)
    mf = max(max_, abs(min_))
    ax4.imshow(b_field_c[:,:nx/2], origin = 'lower', cmap='bwr', extent=[xmin,0,ymin,ymax], vmin=-mf, vmax = mf, alpha = 0.5)
    surf = ax4.imshow(el_field_c[:,:nx/2], origin = 'lower', cmap=cmap1, vmin = 1e-4*m, vmax = m*ratio, norm=clr.LogNorm(), extent=[xmin,0,ymin,ymax])
    ax4.axvline(x=0, color = 'k', dashes = [2,2])
    ax4.axvline(x=-0.25, color = 'grey', dashes = [6,2,2,2])
    
    divider = make_axes_locatable(ax4)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf,  orientation  = 'vertical', cax=cax)
    cbar.ax.tick_params(labelsize=fontsize-2)
    #cbar.set_ticks([0.01, 0.1,1,10, 100])
    #cbar.set_ticklabels(['0.01', '0.1','1','10', '100'])
    #ax4.set_ylabel('$z/\lambda$')
    #ax2.text(bound-0.9, -bound+0.05, '15 PW')
    #ax2.text(-bound, -bound+0.05, '10 PW')
    ax4.text(-bound, bound-0.25, '(e)')
    ax4.text(bound-1, bound-0.25, '15 PW')
    ax4.text(-bound + 0.05, -bound + 0.05, '$t=6T$', fontsize = fontsize - 4)
    ax4.text(bound-1., -bound+0.05, '$t=21T$', fontsize = fontsize - 4)
    ax4.text(-bound-0.5, bound-0.25, '$z/\lambda$')
    ax4.text(bound-0.25, -bound-0.3, '$x/\lambda$')
    #ax4.set_xlabel('$x/\lambda$')
    
    lin_bound = 10
    
    axis_2 = plt.subplot2grid((3,4),(0,2), colspan=2)
    axis_2.set_yscale('linear')
    axis_2.set_ylim((lin_bound, 850))
    axis_2.set_yticks([100, 200, 300, 400, 500, 600, 700])
    axis_2.set_yticklabels(['100', '', '300', '', '', '600', ''])
    axis_2.text(-8, 800, '$n/n_c$')
    axis_2.spines['bottom'].set_visible(False)
    axis_2.yaxis.set_ticks_position('left')
    axis_2.yaxis.set_label_position('left')
    axis_2.xaxis.set_visible(False)
    ne3_, = axis_2.plot(ax_3pw, ne_3pw_t, 'r')
    np3_, = axis_2.plot(ax_3pw, np_3pw_t, 'b')
    #axis_2.set_ylabel('$n_{e^-}/n_c$, $n_{e^+}/n_c$')
    
    divider = make_axes_locatable(axis_2)
    axis_3 = divider.append_axes("bottom", size=.9, pad=0, sharex=axis_2)
    axis_3.set_yscale('log')
    axis_3.set_ylim((0.05, lin_bound))
    axis_3.set_yticks([0.1, 1, 10])
    axis_3.set_yticklabels(['0.1', '1', '10'])
    axis_3.spines['top'].set_visible(False)
    
    axis_3.yaxis.set_ticks_position('left')
    axis_3.yaxis.set_label_position('left')
    
    #axis_3.set_xlabel('$t/T$') 
    ne3__, = axis_3.plot(ax_3pw, ne_3pw_t, 'r')
    np3__, = axis_3.plot(ax_3pw, np_3pw_t, 'b')
    axis_3.set_xlim([0, 35])
        
    axis = axis_2.twinx()
    axis.yaxis.set_ticks_position('right')
    axis.yaxis.set_label_position('right')
    axis.yaxis.tick_right()
    bz3_, = axis.plot(ax_3pw, np.abs(bz_3pw_t)/np.amax(bz_3pw_t), 'grey', alpha=0.5)
    #axis.set_ylabel('$|B_z|/A_0$')
    #axis.set_xlabel('$t/T$')
    axis.set_xticklabels([])
    axis.set_xticks([0,5,10,15,20,25,30])
    axis.set_yticks([0, 0.5,1.0])
    axis.set_yticklabels(['0', '0.5', '1'])
    axis.set_ylim([0, 1.35])
    axis.set_xlim([0, 35])
    axis.text(35.3, 1.22, '$|B_z|/A_0$')
    axis.text(0, 1.22, '(b)')
    plt.legend([ne3_, np3_, bz3_],
               ['$n_{e^-}$', '$n_{e^+}$', '|$B_z$|'],
               loc = 'lower left',
               frameon = False, ncol=2, columnspacing=0.5,
               labelspacing=0.1,
               fontsize = fontsize-2, bbox_to_anchor=(0.25,0.68),
               handlelength=1.5, handletextpad=0.1)

    axis_2 = plt.subplot2grid((3,4),(1,2), colspan=2)
    axis_2.set_yscale('linear')
    axis_2.set_ylim((lin_bound, 850))
    axis_2.set_yticks([100, 200, 300, 400, 500, 600, 700])
    axis_2.set_yticklabels(['100', '', '300', '', '', '600', ''])
    axis_2.text(-8, 800, '$n/n_c$')
    axis_2.spines['bottom'].set_visible(False)
    axis_2.yaxis.set_ticks_position('left')
    axis_2.yaxis.set_label_position('left')
    axis_2.xaxis.set_visible(False)
    ne10_, = axis_2.plot(ax_10pw, ne_10pw_t, 'r')
    np10_, = axis_2.plot(ax_10pw, np_10pw_t, 'b')
    #axis_2.set_ylabel('$n_{e^-}/n_c$, $n_{e^+}/n_c$')
    
    divider = make_axes_locatable(axis_2)
    axis_3 = divider.append_axes("bottom", size=1.0, pad=0, sharex=axis_2)
    axis_3.set_yscale('log')
    axis_3.set_ylim((0.05, lin_bound))
    axis_3.set_yticks([0.1, 1, 10])
    axis_3.set_yticklabels(['0.1', '1', '10'])
    axis_3.spines['top'].set_visible(False)
    
    axis_3.yaxis.set_ticks_position('left')
    axis_3.yaxis.set_label_position('left')
    
    #axis_3.set_xlabel('$t/T$') 
    ne10__, = axis_3.plot(ax_10pw, ne_10pw_t, 'r')
    np10__, = axis_3.plot(ax_10pw, np_10pw_t, 'b')
    axis_3.set_xlim([0, 35])
        
    axis = axis_2.twinx()
    axis.yaxis.set_ticks_position('right')
    axis.yaxis.set_label_position('right')
    axis.yaxis.tick_right()
    bz10_, = axis.plot(ax_10pw, np.abs(bz_10pw_t)/np.amax(bz_10pw_t), 'grey', alpha=0.5)
    #axis.set_ylabel('$|B_z|/A_0$')
    #axis.set_xlabel('$t/T$')
    axis.set_xticklabels([])
    axis.set_xticks([0,5,10,15,20,25,30])
    axis.set_yticks([0, 0.5,1.0])
    axis.set_yticklabels(['0', '0.5', '1'])
    
    axis.set_ylim([0, 1.35])
    axis.set_xlim([0, 35])
    axis.text(35.3, 1.22, '$|B_z|/A_0$')
    axis.text(0, 1.22, '(d)')
    plt.legend([ne3_, np3_, bz3_],
               ['$n_{e^-}$', '$n_{e^+}$', '|$B_z$|'],
               loc = 'lower left',
               frameon = False, ncol=2, columnspacing=0.5,
               labelspacing=0.1,
               fontsize = fontsize-2, bbox_to_anchor=(0.25,0.68),
               handlelength=1.5, handletextpad=0.1)
    
    axis_2 = plt.subplot2grid((3,4),(2,2), colspan=2)
    axis_2.set_yscale('linear')
    axis_2.set_ylim((lin_bound, 850))
    axis_2.set_yticks([100, 200, 300, 400, 500, 600, 700])
    axis_2.set_yticklabels(['100', '', '300', '', '', '600', ''])
    axis_2.text(-8, 800, '$n/n_c$')
    axis_2.spines['bottom'].set_visible(False)
    axis_2.yaxis.set_ticks_position('left')
    axis_2.yaxis.set_label_position('left')
    axis_2.xaxis.set_visible(False)
    ne15_, = axis_2.plot(ax_15pw, ne_15pw_t, 'r')
    np15_, = axis_2.plot(ax_15pw, np_15pw_t, 'b')
    #axis_2.set_ylabel('$n_{e^-}/n_c$, $n_{e^+}/n_c$')
    
    divider = make_axes_locatable(axis_2)
    axis_3 = divider.append_axes("bottom", size=1.0, pad=0, sharex=axis_2)
    axis_3.set_yscale('log')
    axis_3.set_ylim((0.05, lin_bound))
    axis_3.set_yticks([0.1, 1, 10])
    axis_3.set_yticklabels(['0.1', '1', '10'])
    axis_3.spines['top'].set_visible(False)
    
    axis_3.yaxis.set_ticks_position('left')
    axis_3.yaxis.set_label_position('left')
    
    #axis_3.set_xlabel('$t/T$') 
    ne15__, = axis_3.plot(ax_15pw, ne_15pw_t, 'r')
    np15__, = axis_3.plot(ax_15pw, np_15pw_t, 'b')
    axis_3.set_xlim([0, 35])
        
    axis = axis_2.twinx()
    axis.yaxis.set_ticks_position('right')
    axis.yaxis.set_label_position('right')
    axis.yaxis.tick_right()
    bz15_, = axis.plot(ax_15pw, np.abs(bz_15pw_t)/np.amax(bz_15pw_t), 'grey', alpha=0.5)
    #axis.set_ylabel('$|B_z|/A_0$')
    #axis.set_xlabel('$t/T$')
    axis_3.text(30, 0.007, '$t/T$')
    
    axis.set_ylim([0, 1.35])
    axis.set_xlim([0, 35])
    axis.set_xticks([0,5,10,15,20,25,30])
    axis.set_xticklabels(['','5', '', '15', '', '25', ''])
    axis.set_yticks([0, 0.5,1.0])
    axis.set_yticklabels(['0', '0.5', '1'])
    axis.text(35.3, 1.22, '$|B_z|/A_0$')
    axis.text(0, 1.22, '(f)')
    plt.legend([ne3_, np3_, bz3_],
               ['$n_{e^-}$', '$n_{e^+}$', '|$B_z$|'],
               loc = 'lower left',
               frameon = False, ncol=2, columnspacing=0.5,
               labelspacing=0.1,
               fontsize = fontsize-2, bbox_to_anchor=(0.25,0.68),
               handlelength=1.5, handletextpad=0.1)
    
    
    plt.tight_layout()
    plt.subplots_adjust(hspace=0.075, wspace=1.5, right=0.89)
    
    picname = os.path.join(picpath, 'fig_target_v2_cc.png')
    print(picname)
    plt.savefig(picname, dpi=128)
    #plt.show()
   
#    plt.show()
if __name__ == '__main__':
    main()
