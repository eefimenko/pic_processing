#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np
import matplotlib as mp

def main():
    power_p = []
    ezx_p = []
    bzx_p = []
    nex_p = []
    ncrx_p = []
    jx_min_p = []
    jx_max_p = []
    powx_p = []
    pow1x_p = []
    maxx_p = []
    avx_p = []
    
    f = open('stat_summary.txt', 'r')
    for line in f:
        tmp = line.split()
        power_p.append(float(tmp[0]))
        ezx_p.append(float(tmp[1]))
        bzx_p.append(float(tmp[2]))
        maxx_p.append(float(tmp[3]))
        avx_p.append(float(tmp[4]))
        ncrx_p.append(float(tmp[5]))
        nex_p.append(float(tmp[6]))
        powx_p.append(float(tmp[7]))
        pow1x_p.append(float(tmp[8]))
        jx_min_p.append(float(tmp[9]))
        jx_max_p.append(float(tmp[10]))
       
    f.close()
    nmax = 100
    dp = 17./nmax
    p = np.zeros(nmax)
    e = np.zeros(nmax)
    b = np.zeros(nmax)
    for i in range(nmax):
        p[i] = dp*i
        e[i] = 3e11*math.sqrt(p[i]/10)
        b[i] = 0.653*3e11*math.sqrt(p[i]/10)
    ep = 3e11*math.sqrt(7.2/10)
    bp = 0.653*3e11*math.sqrt(7.2/10)

   
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.plot(power_p, ezx_p, 'r', label='Stationary E')
    ax.plot(p, e, 'r:', label = 'Vacuum E')
    ax.plot([7.2,17], [ep,ep], 'r--', label = 'Threshold E (7.2 PW)')
    ax.plot(power_p, bzx_p, 'b', label='Stationary B')
    ax.plot(p, b, 'b:', label = 'Vacuum B')
    ax.plot([7.2,17], [bp,bp], 'b--', label = 'Threshold B (7.2 PW)')
    ax.set_xlim([7,18])
    plt.legend(loc = 'lower right')
    ax.set_xlabel('Power, PW')
    ax.set_ylabel('E,B, CGS')
    plt.show()
    plt.close()

    rpow_p = []
    
    for i in range(len(power_p)):
        rpow_p.append(power_p[i] - powx_p[i])
        
    
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.plot(power_p, powx_p, label = 'Remaining power')
    ax.plot(power_p, rpow_p, label = 'Radiated power')
    ax.plot([7, 18], [7.2, 7.2], 'r:', label = '7.2 PW')
    ax.set_xlabel('Power, PW')
    ax.set_ylabel('Radiated power, PW')
    plt.legend(loc = 'lower right')
    plt.show()
    plt.close()

    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.plot(power_p, pow1x_p)
    
    ax.set_xlabel('Power, PW')
    ax.set_ylabel('Efficiency')
    plt.legend(loc = 'lower right')
    plt.show()
    plt.close()

    fig = plt.figure()
    nex = fig.add_subplot(1,1,1)
    nex.set_title('Ne')
    nex.plot(power_p, nex_p)
    nex.set_xlabel('Power, PW')
    nex.set_ylabel('Electron density, $cm^{-3}$')
    plt.show()
    plt.close()

    fig = plt.figure()
    maxx = fig.add_subplot(1,1,1)
    m, = maxx.plot(power_p, maxx_p, 'b')
    maxx.set_ylim([0, 1.1*max(maxx_p)])
    maxx.set_title('Max energy')
    maxx.set_xlabel('Power, PW')
    maxx.set_ylabel('Maximal photon energy, GeV')
    
    avx = maxx.twinx()
    a, = avx.plot(power_p, avx_p, 'r')
    avx.set_ylim([0, 1.1*max(avx_p)])
    avx.set_ylabel('Average photon energy, GeV')
    avx.legend([m,a], ['Maximal', 'Average'], loc = 'lower right')
    plt.show()
    plt.close()

    fig = plt.figure()
    maxx = fig.add_subplot(1,1,1)
    m, = maxx.plot(power_p, maxx_p, 'b')
    maxx.set_ylim([0, 1.1*max(maxx_p)])
    maxx.set_title('Max energy')
    maxx.set_xlabel('Power, PW')
    maxx.set_ylabel('Maximal photon energy, GeV')
    
    ezx = maxx.twinx()
    a, = ezx.plot(power_p, ezx_p, 'r')
    ezx.set_ylim([0, 1.1*max(ezx_p)])
    ezx.set_ylabel('Stationary E field')
    plt.legend([m,a], ['Max energy', 'Electric field'], loc = 'lower right')
    plt.show()
    plt.close()
    
    power_p1 = []
    nmin = []
    nmax = []
    f = open('nmin_nmax.txt')
    for line in f:
        tmp = line.split()
        power_p1.append(float(tmp[0]))
        nmin.append(float(tmp[1]))
        nmax.append(float(tmp[2]))
    f.close()

    power_p2 = []
    jneg = []
    jpos = []
    f = open('jpos_jneg.txt')
    for line in f:
        tmp = line.split()
        power_p2.append(float(tmp[0]))
        jpos.append(float(tmp[1]))
        jneg.append(float(tmp[2]))
    f.close()
    print jpos, jneg
    mp.rcParams.update({'font.size': 16})
    fig = plt.figure()
    maxx = fig.add_subplot(1,2,1)
    m1, = maxx.plot(power_p1, nmin, 'r')
    maxx.set_ylim([0, 1.05*max(nmin)])
   
    maxx.set_xlabel('Power, PW')
    maxx.set_ylabel('Number of particles')

    jx = maxx.twinx()
    j1, = jx.plot(power_p2, jpos, 'r:')
    jx.set_ylim([0, 1.1*max(jpos)])
    jx.set_xlabel('Power, PW')
    jx.set_ylabel('$+J_z, MA$')

    maxx = fig.add_subplot(1,2,2)
    m2, = maxx.plot(power_p1, nmax, 'b')
    maxx.set_ylim([0, 1.1*max(nmax)])
    
    maxx.set_xlabel('Power, PW')
    # maxx.set_ylabel('Number of particles')

    jx = maxx.twinx()
    j2, = jx.plot(power_p2, jneg, 'b:')
    jx.set_ylim([0, 1.1*max(jpos)])
    jx.set_xlabel('Power, PW')
    jx.set_ylabel('$-J_z, MA$')
    
    plt.legend([m1,m2,j1,j2], ['$r<0.4 \lambda$', '$r>0.4 \lambda$', '$+J_z$', '$-J_z$'], loc = 'upper left')
    
    plt.show()
    plt.close()
    
    fig = plt.figure()
    ezx = fig.add_subplot(4,2,1)
    ezx.set_title('Electric field')
    ezx.plot(power_p, ezx_p)
    ezx.set_ylim([0, max(ezx_p)])
    bzx = fig.add_subplot(4,2,2)
    bzx.set_title('Magnetic field')
    bzx.plot(power_p, bzx_p)
    bzx.set_ylim([0, max(bzx_p)])
    ncrx = fig.add_subplot(4,2,4)
    ncrx.set_title('Ne/Ncr')
    ncrx.plot(power_p, ncrx_p)
    ncrx.set_ylim([0, max(ncrx_p)])
    nex = fig.add_subplot(4,2,3)
    nex.set_title('Ne')
    nex.plot(power_p, nex_p)
    nex.set_ylim([0, max(nex_p)])
    powx = fig.add_subplot(4,2,6)
    powx.set_title('Power')
    powx.plot(power_p, powx_p)
    powx.set_ylim([0, max(powx_p)])
    pow1x = powx.twinx()
    pow1x.plot(power_p, pow1x_p, 'g')
    pow1x.set_ylim([0, max(pow1x_p)])
    maxx = fig.add_subplot(4,2,7)
    maxx.set_title('Max energy')
    maxx.plot(power_p, maxx_p)
    maxx.set_ylim([0, max(maxx_p)])
    avx = fig.add_subplot(4,2,8)
    avx.plot(power_p, avx_p)
    avx.set_ylim([0, max(avx_p)])
    avx.set_title('Av energy')
    jx = fig.add_subplot(4,2,5)
    jx.set_title('Number of particles')
    jx.plot(power_p, jx_min_p)
    jx.plot(power_p, jx_max_p)
    jx.set_ylim([0, max(jx_max_p)])

    plt.show()
    
if __name__ == '__main__':
    main()
