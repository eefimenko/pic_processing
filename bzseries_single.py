#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils
import numpy as np

def main():
  
    numdirs = int(sys.argv[1])
    dirs = []
    configs = []
    for i in range(numdirs):
        dirs.append(sys.argv[2+i] + '/')
        configs.append(utils.get_config(sys.argv[2+i] + "/ParsedInput.txt"))
    delta = 1
    n = numdirs + 1
    num = len(sys.argv) - n
    picspath = 'pics_single'
    
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = find_nmax(dirs, expath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1 + n] 
        nmin = int(sys.argv[1 + n])
        nmax = find_nmax(dirs, expath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1 + n] + ' to ' + sys.argv[2 + n]
        nmin = int(sys.argv[1 + n])
        nmax = int(sys.argv[2 + n])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1 + n] + ' to ' + sys.argv[2 + n] + ' with delta ' + sys.argv[3 + n]
        print sys.argv[1 + n]
        nmin = int(sys.argv[1 + n])
        nmax = int(sys.argv[2 + n])
        delta = int(sys.argv[3 + n])
    else:
        nmin = 0
        nmax = 0
        print 'Too much parameters'

    print nmin, nmax, delta
    
    xmin = []
    xmax = []
    ymin = []
    ymax = []
    zmin = []
    zmax = []
    mult = []
    nx = []
    ny = []
    nz = []
    power = []
    dt = 0
    ncr = 1
    for k in range(numdirs):
        wl = float(configs[k]['Wavelength'])
        xmax.append(float(configs[k]['X_Max'])/wl) #mkm
        xmin.append(float(configs[k]['X_Min'])/wl) #mkm
        ymax.append(float(configs[k]['Y_Max'])/wl) #mkm
        ymin.append(float(configs[k]['Y_Min'])/wl) #mkm
        zmax.append(float(configs[k]['Z_Max'])/wl) #mkm
        zmin.append(float(configs[k]['Z_Min'])/wl) #mkm
        nx.append(int(configs[k]['MatrixSize_X']))
        ny.append(int(configs[k]['MatrixSize_Y']))
        nz.append(int(configs[k]['MatrixSize_Z']))
        power.append(int(configs[k]['PeakPowerPW']))
        dx = float(configs[k]['Step_X'])/wl
        dy = float(configs[k]['Step_Y'])/wl
        dz = float(configs[k]['Step_Z'])/wl
        mult.append(1/(2.*dx*dy*dz*1e-12))
        step = float(configs[k]['TimeStep'])*1e15*float(configs[k]['BOIterationPass'])
        omega = float(configs[k]['Omega'])
        ncr = utils.ElectronMass * omega * omega/(8. * math.pi * utils.ElectronCharge * utils.ElectronCharge)
        T = 2 * math.pi/omega
        nt = int(T*1e15/step)
        dt  = step/(T*1e15)
        print nt
    print 'Critical density ', ncr
    figures = [utils.bx_zpath, utils.nezpath]
    cmaps = ['bwr', 'Greens']
    titles = ['$B_\\rho$', '$N_e$']
    log = [False, False]
    

#    n0 = [703, 0, 0] # 675
#    n0 = [686, 0, 0] # 680
#    n0 = [1030, 1090]
    n0 = [0] * numdirs
    spx = numdirs
    spy = len(figures)

    xlim = [-0.5, 0.5]
    ylim = xlim
    verbose = 1
    # nticks = 5
    # ticks = [xlim[0] + i*(xlim[1]-xlim[0])/(nticks-1) for i in range(nticks)]
    # print ticks
    fontsize = 16
    for i in range(nmin,nmax,delta):
        print "\rSaving sc%06d.png, %.0f %% done" % (i,float(i-nmin)/(nmax-nmin)*100),
        sys.stdout.flush()

        fig = plt.figure(num=None, figsize=(10., 10.), dpi=256)
        mp.rcParams.update({'font.size': fontsize})
        
        for k in range(spx):
            fx = utils.bo_file_load(dirs[k]+utils.bx_zpath,
                                  i+n0[k],nx[k],ny[k],verbose=1)
            fy = utils.bo_file_load(dirs[k]+utils.by_zpath,
                                  i+n0[k],nx[k],ny[k],verbose=1)
            fx0 = utils.bo_file_load('10pw_vac/'+utils.bx_zpath,
                                     i+n0[k],nx[k],ny[k],verbose=1)
            fy0 = utils.bo_file_load('10pw_vac/'+utils.by_zpath,
                                     i+n0[k],nx[k],ny[k],verbose=1)
            fr,fphi = utils.convert_xy_to_rphi(fx-fx0,fy-fy0,dx,dy)
            ratio = np.amax(fr)/np.amax(fphi) 
            print ratio
            for j in range(spy):
                if figures[j] == utils.bx_zpath:
                    if i > 250:
                        utils.subplot(fig, i+n0[k], dirs[k]+figures[j], field = fr,
                                      shape = (nx[k],ny[k]), position = (1,1,1),
                                      extent = [xmin[k], xmax[k], ymin[k], ymax[k]],
                                      cmap = cmaps[j], title = '', titletype = 'simple',
                                      colorbar = False, logarithmic=log[j], verbose=verbose,
                                      xlim = xlim, ylim = ylim,
                                      xlabel = '$x/\lambda$', ylabel = '$y/\lambda$',
                                      maximum = 'local', fontsize = fontsize,
                                      contour=True, clevels = [-0.15, 0.15])
                else:
                    utils.subplot(fig, i+n0[k], dirs[k]+figures[j], 
                                  shape = (nx[k],ny[k]), position = (1,1,1),
                                  extent = [xmin[k], xmax[k], ymin[k], ymax[k]],
                                  cmap = cmaps[j], title = '%.1f T'%(i*dt), titletype = 'simple',
                                  colorbar = True, logarithmic=log[j], verbose=verbose,
                                  xlim = xlim, ylim = ylim, 
                                  xlabel = '$x/\lambda$', ylabel = '$y/\lambda$',
                                  maximum = 'local', fontsize = fontsize, mult = mult[k]/ncr)
                    
        picname = picspath + '/' + "bzs%06d.png" % (i,)
        plt.tight_layout()
        plt.savefig(picname)
        plt.close()

    
if __name__ == '__main__':
    main()
