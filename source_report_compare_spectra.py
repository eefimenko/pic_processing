#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import numpy as np
import os
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

def analyze_spectra(sp,de):
    n = len(sp)
    full_energy = sum(sp)
    full_number = 0
   
    for i in range(n):
        full_number += sp[i]/(de*(i+0.425))
        
    tail_energy = 0
    
    for i in range(n):
        tail_energy += sp[-1-i]
        if tail_energy/full_energy > 0.01:
            break
    imax = i
    e_1percent = (n-imax)*de
    
    for i in range(n):
        sp[i] /= full_number

    av_energy = full_energy/full_number
            
    return sp, e_1percent, av_energy, n-imax, int(av_energy/de)

def read_spectra(filename):
    f = open(filename, 'r')
    ax_ = []
    ph_ = []
    el_ = []
    pos_ = []
    for line in f:
        tmp = line.split()
        ax_.append(float(tmp[0]))
        ph_.append(float(tmp[1]))
        el_.append(float(tmp[2]))
        pos_.append(float(tmp[3]))
    f.close()
    return ax_, np.array(ph_), np.array(el_), np.array(pos_)

def main():
    mp.rcParams.update({'font.size': 12})
    
    picspath = '.'
    dirs = ['DipoleB_Wire__2021-04-22_23-51-45', 'DipoleB_Wire__2021-05-14_03-53-22', 'DipoleB_Wire__2021-05-21_17-24-09']#, '27pw_leveling', '27pw_conservation']
    #dirs = ['sph_target_3pw', 'sph_target_5pw', 'sph_target_10pw']
    legends = ['3 PW', '5 PW', '10 PW'] #, 'leveling', 'conserve']
    fontsize = 12
    
    fig1 = plt.figure(num=None, figsize = (5,3.5))
    ax1 = fig1.add_subplot(1,1,1)
    fig2 = plt.figure(num=None, figsize = (5,3.5))
    ax2 = fig2.add_subplot(1,1,1)
    
    ax1.set_yscale('log')
    ax2.set_yscale('log')
    
    ax1.set_ylim([1e-8, 2e-3])
    ax2.set_ylim([1e-8, 2e-3])
    
    ax1.set_xlim([0, 1.2])
    ax2.set_xlim([0, 1.2])
    window = 11
    order = 7

    #axins = zoomed_inset_axes(ax1, 2.5, loc= 'upper center')  # zoom = 6
    #axins.set_yscale('log')
    #axins.set_xlim([0.75, 1.5])
    #axins.set_ylim([3e-5, 3e-4])
    
    #mark_inset(ax1, axins, loc1=2, loc2=3, fc="none", ec="0.5")

    #axins1 = zoomed_inset_axes(ax2, 2.5, loc= 'upper right')  # zoom = 6
    #axins1.set_yscale('log')
    #axins1.set_xlim([0.75, 1.5])
    #axins1.set_ylim([8e-4, 9e-3])
    
    #mark_inset(ax2, axins1, loc1=1, loc2=3, fc="none", ec="0.5")
    i = 0
    c = ['r', 'g', 'b']
    for d, label in zip(dirs, legends):
        ax_, ph_, el_, pos_ = read_spectra(d + '/qe_spectra.txt')
        config = utils.get_config(d + '/ParsedInput.txt')
        n_cr = float(config['n_cr'])
        #ne_ = float(config['Ne'])
        Emax = float(config['QEDstatistics.Emax'])
        Emin = float(config['QEDstatistics.Emin'])
        N_E = int(config['QEDstatistics.OutputN_E'])
        de = (Emax - Emin)/N_E/1.6e-12/1e9 # erg -> eV -> GeV
        n = utils.full_number(ph_,de,N_E)
        #ax1.plot(ax_, utils.savitzky_golay(np.array(ph_)/n, window, order), linewidth = 0.9, label = label)
        ax1.plot(ax_, np.array(ph_)/n, linewidth = 0.7, label = label, color = c[i])
        print(label + ' photon:', analyze_spectra(ph_,de)[1:])
        w_1pe = analyze_spectra(ph_,de)[1]
        ax1.axvline(x = w_1pe, color = c[i], dashes = [2,2])
        #axins.plot(ax_, np.array(ph_)/n, linewidth = 0.7, label = label)
        n = utils.full_number(el_,de,N_E)
        #ax2.plot(ax_, utils.savitzky_golay(np.array(el_)/n, window, order), linewidth = 0.9, label = label)
        ax2.plot(ax_, np.array(el_)/n, linewidth = 0.7, label = label, color = c[i])
        w_1pe = analyze_spectra(el_,de)[1]
        ax2.axvline(x = w_1pe, color = c[i], dashes = [2,2])
        print(label + ' electron:', analyze_spectra(el_,de)[1:])
        i += 1
        #axins1.plot(ax_, np.array(el_)/n, linewidth = 0.7, label = label)
        #n = utils.full_number(pos_,de,N_E)
        #ax3.plot(ax_, utils.savitzky_golay(np.array(pos_)/n, window, order), linewidth = 0.9, label = label)
        #ax3.plot(ax_, np.array(pos_)/n, linewidth = 0.9, label = label)

    ax1.legend(loc = 'upper right', fontsize = fontsize-2, frameon = False)
    ax2.legend(loc = 'upper right', fontsize = fontsize-2, frameon = False)
    ax1.set_xlabel('$\hbar\omega$, GeV')
    ax2.set_xlabel('$\hbar\omega$, GeV')
    ax1.set_ylabel('S[$\omega$], a.u.')
    ax2.set_ylabel('S[$\omega$], a.u.')
    ax1.text(-0.25, 2e-3, '(a)')
    ax2.text(-0.25, 2e-3, '(b)')
    #axins.set_xticks([])
    #axins.set_yticks([])
    #axins.set_yticklabels([])
    #axins1.set_xticks([])
    #axins1.set_yticks([])
    #axins1.set_yticklabels([])
    
    name1 = 'ph_spectra'
    name2 = 'el_spectra'
    picname1 = picspath + '/' + name1 + ".png"
    picname2 = picspath + '/' + name2 + ".png"
    #plt.legend(loc = 'upper left', fontsize = 10)
    fig1.tight_layout()
    fig2.tight_layout()
    fig1.savefig(picname1, dpi = 256)
    fig2.savefig(picname2, dpi = 256)
      
#    plt.show()

if __name__ == '__main__':
    main()
