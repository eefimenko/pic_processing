#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_gamma(filename):
    f = open(filename, 'r')
    tmp = f.readline().split()
    f.close()
    return float(tmp[0]), float(tmp[1]), float(tmp[2])

def main():
    savepath = './pics'
    if len(sys.argv) == 2:
        savepath = sys.argv[1]
    fontsize = 21
    mp.rcParams.update({'font.size': fontsize})
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
    el = []
    ph = []
    pos = []
    ne = []
    types = ['edipole']
    types_labels = [u'е-волна']
    dirs = ['nc_0.001', 'nc_0.01', 'nc_0.1', 'nc_1', 'nc_10','nc_30', 'nc_50', 'nc_100', 'nc_1000']
    updirs = ['1mkm', '2mkm', '3mkm']
    volumes = [4./3. * math.pi * 1e-4**3, 4./3. * math.pi * 2e-4**3, 4./3. * math.pi * 3e-4**3]
    updir_labels = [u'2 мкм', u'4 мкм', u'6 мкм']
    color = ['r', 'g', 'b']
    dashes = [[3,2], [], [5,2,1,2]]
    fig = plt.figure(figsize = (18,5))
    ax1 = fig.add_subplot(1,3,1)
    ax2 = fig.add_subplot(1,3,2)
    ax3 = fig.add_subplot(1,3,3)
#    fig2 =  plt.figure(figsize = (10,5))
#    ax2 = fig2.add_subplot(1,1,1)
#    ax3 = fig.add_subplot(2,2,3)
#    ax4 = fig.add_subplot(2,2,4)
    m = 0
    for type_ in types:
        for i,ud in enumerate(updirs):
            el = []
            ph = []
            pos = []
            el_1gev = []
            ph_1gev = []
            pos_1gev = []
            ne = []
            number = []
            for d in dirs:
                ph_, el_, pos_ = read_gamma(type_ + '/' + ud + '/' + d + '/efficiency.txt')
                el.append(el_)
                ph.append(ph_)
                pos.append(pos_)
                ph_, el_, pos_ = read_gamma(type_ + '/' + ud + '/' + d + '/efficiency_1gev.txt')
                el_1gev.append(el_)
                ph_1gev.append(ph_)
                pos_1gev.append(pos_)
                config = utils.get_config(type_ + '/' + ud + '/' + d + '/ParsedInput.txt')
                n_cr = float(config['n_cr'])
                ne_ = float(config['Ne'])
                r = float(config['R'])
                ne.append(ne_/n_cr)
                number.append(4./3.* math.pi * r**3 * ne_)
            ax1.plot(ne, ph, color[i], label = updir_labels[i])
            ax2.plot(ne, el, color[i], label = updir_labels[i])
            ax3.plot(ne, pos, color[i], label = updir_labels[i])
#            ax2.plot(number, ph, 'r', dashes = dashes[m])
#            ax2.plot(number, el, 'g', dashes = dashes[m])
#            ax2.plot(number, pos, 'b', dashes = dashes[m])
#            ax2.plot(ne, ph_1gev, 'r', dashes = dashes[m])
#            ax2.plot(ne, el_1gev, 'g', dashes = dashes[m])
#            ax2.plot(ne, pos_1gev, 'b', dashes = dashes[m])
#            ax4.plot(number, ph_1gev, 'r', dashes = dashes[m])
#            ax4.plot(number, el_1gev, 'g', dashes = dashes[m])
#            ax4.plot(number, pos_1gev, 'b', dashes = dashes[m])
        m += 1
#    ax1.set_yscale('log')
    ax1.set_xscale('log')
    ax1.set_yscale('log')
    ax1.set_ylim([5e-3, 1]) 
    ax1.set_xlabel(u'$n_0/n_{c}$')
    ax1.set_ylabel(u'$W_{\hbar\omega}/W_l$')
    ax2.set_xscale('log')
    ax2.set_yscale('log')
    ax2.set_ylim([1e-4, 2e-1]) 
    ax2.set_xlabel(u'$n_0/n_{c}$')
    ax2.set_ylabel(u'$W_{e^{-}}/W_l$')
    ax3.set_xscale('log')
    ax3.set_yscale('log')
    ax3.set_ylim([1e-4, 2e-1]) 
    ax3.set_xlabel(u'$n_0/n_{c}$')
    ax3.set_ylabel(u'$W_{e^{+}}/W_l$')
    ax1.legend(loc = 'lower left', frameon = False, fontsize = fontsize-3)
    ax2.legend(loc = 'lower left', frameon = False, fontsize = fontsize-3)
    ax3.legend(loc = 'lower left', frameon = False, fontsize = fontsize-3)
    plt.tight_layout()
    ax1.text(0.00001, ax1.get_ylim()[1], '(a)')
    ax2.text(0.00001, ax2.get_ylim()[1], '(b)')
    ax3.text(0.00001, ax3.get_ylim()[1], '(c)')
    picname = savepath + '/coversion_ideal.png'
    print(picname)
    plt.savefig(picname)

    fig = plt.figure(figsize = (18,5))
    ax1 = fig.add_subplot(1,3,1)
    ax2 = fig.add_subplot(1,3,2)
    ax3 = fig.add_subplot(1,3,3)
#    fig2 =  plt.figure(figsize = (10,5))
#    ax2 = fig2.add_subplot(1,1,1)
#    ax3 = fig.add_subplot(2,2,3)
#    ax4 = fig.add_subplot(2,2,4)
    m = 0
    for type_ in types:
        for i,ud in enumerate(updirs):
            el = []
            ph = []
            pos = []
            el_1gev = []
            ph_1gev = []
            pos_1gev = []
            ne = []
            number = []
            for d in dirs:
                ph_, el_, pos_ = read_gamma(type_ + '/' + ud + '/' + d + '/efficiency_1gev.txt')
                el.append(el_)
                ph.append(ph_)
                pos.append(pos_)
                
                config = utils.get_config(type_ + '/' + ud + '/' + d + '/ParsedInput.txt')
                n_cr = float(config['n_cr'])
                ne_ = float(config['Ne'])
                r = float(config['R'])
                ne.append(ne_/n_cr)
                number.append(4./3.* math.pi * r**3 * ne_)
            ax1.plot(ne, ph, color[i], label = updir_labels[i])
            ax2.plot(ne, el, color[i], label = updir_labels[i])
            ax3.plot(ne, pos, color[i], label = updir_labels[i])
#            ax2.plot(number, ph, 'r', dashes = dashes[m])
#            ax2.plot(number, el, 'g', dashes = dashes[m])
#            ax2.plot(number, pos, 'b', dashes = dashes[m])
#            ax2.plot(ne, ph_1gev, 'r', dashes = dashes[m])
#            ax2.plot(ne, el_1gev, 'g', dashes = dashes[m])
#            ax2.plot(ne, pos_1gev, 'b', dashes = dashes[m])
#            ax4.plot(number, ph_1gev, 'r', dashes = dashes[m])
#            ax4.plot(number, el_1gev, 'g', dashes = dashes[m])
#            ax4.plot(number, pos_1gev, 'b', dashes = dashes[m])
        m += 1
#    ax1.set_yscale('log')
    ax1.set_xscale('log')
    ax1.set_yscale('log')
    ax1.set_ylim([1e-6, 1e-2]) 
    ax1.set_xlabel(u'$n_0/n_{c}$')
    ax1.set_ylabel(u'$W_{\hbar\omega}/W_l$')
    ax2.set_xscale('log')
    ax2.set_yscale('log')
    ax2.set_ylim([1e-6, 1e-2]) 
    ax2.set_xlabel(u'$n_0/n_{c}$')
    ax2.set_ylabel(u'$W_{e^{-}}/W_l$')
    ax3.set_xscale('log')
    ax3.set_yscale('log')
    ax3.set_ylim([1e-6, 1e-2]) 
    ax3.set_xlabel(u'$n_0/n_{c}$')
    ax3.set_ylabel(u'$W_{e^{+}}/W_l$')
    ax1.legend(loc = 'lower left', frameon = False, fontsize = fontsize-3)
    ax2.legend(loc = 'lower left', frameon = False, fontsize = fontsize-3)
    ax3.legend(loc = 'lower left', frameon = False, fontsize = fontsize-3)
    plt.tight_layout()
    ax1.text(0.00001, ax1.get_ylim()[1], '(a)')
    ax2.text(0.00001, ax2.get_ylim()[1], '(b)')
    ax3.text(0.00001, ax3.get_ylim()[1], '(c)')
    picname = savepath + '/coversion_1gev_ideal.png'
    print(picname)
    plt.savefig(picname)

    fig = plt.figure(figsize = (18,5))
    ax1 = fig.add_subplot(1,3,1)
    ax2 = fig.add_subplot(1,3,2)
    ax3 = fig.add_subplot(1,3,3)
#    fig2 =  plt.figure(figsize = (10,5))
#    ax2 = fig2.add_subplot(1,1,1)
#    ax3 = fig.add_subplot(2,2,3)
#    ax4 = fig.add_subplot(2,2,4)
    m = 0
    for type_ in types:
        for i,ud in enumerate(updirs):
            el = []
            ph = []
            pos = []
            el_t = []
            ph_t = []
            pos_t = []
            el_1gev = []
            ph_1gev = []
            pos_1gev = []
            ne = []
            number = []
            for d in dirs:
                ph_, el_, pos_ = read_gamma(type_ + '/' + ud + '/' + d + '/number.txt')
                el.append(el_)
                ph.append(ph_)
                pos.append(pos_)

                
                #ph_, el_, pos_ = read_number(type_ + '/' + ud + '/' + d + '/efficiency_1gev.txt')
                #el_1gev.append(el_)
                #ph_1gev.append(ph_)
                #pos_1gev.append(pos_)
                config = utils.get_config(type_ + '/' + ud + '/' + d + '/ParsedInput.txt')
                n_cr = float(config['n_cr'])
                ne_ = float(config['Ne'])
                r = float(config['R'])
                ne.append(ne_/n_cr)
                number.append(4./3.* math.pi * r**3 * ne_)
                el_t.append(2.*4./3.* math.pi * r**3 * ne_)
                pos_t.append(4./3.* math.pi * r**3 * ne_)
            ax1.plot(ne, ph, color[i], label = updir_labels[i])
            ax2.plot(ne, el, color[i], label = updir_labels[i])
            ax2.plot(ne, el_t, color[i], dashes = [3,3], label = '2N$_{%s}$' % updir_labels[i])
            ax3.plot(ne, pos, color[i], label = updir_labels[i])
            ax3.plot(ne, pos_t, color[i], dashes = [3,3], label = 'N$_{%s}$' % updir_labels[i])
           
        m += 1
#    ax1.set_yscale('log')
    ax1.set_xscale('log')
    ax1.set_yscale('log')
    #ax1.set_ylim([5e-3, 1]) 
    ax1.set_xlabel(u'$n_0/n_{c}$')
    ax1.set_ylabel(u'N$_{\hbar\omega}$')
    ax2.set_xscale('log')
    ax2.set_yscale('log')
    #ax2.set_ylim([1e-4, 2e-1]) 
    ax2.set_xlabel(u'$n_0/n_{c}$')
    ax2.set_ylabel(u'N$_{e^-}$')
    ax3.set_xscale('log')
    ax3.set_yscale('log')
    #ax3.set_ylim([1e-4, 2e-1]) 
    ax3.set_xlabel(u'$n_0/n_{c}$')
    ax3.set_ylabel(u'N$_{e^+}$')
    ax1.legend(loc = 'upper left', frameon = False, fontsize = fontsize-3, labelspacing = 0.1)
    handles, lbls = ax2.get_legend_handles_labels()
    handles_ = [handles[0], handles[2], handles[4], handles[1], handles[3], handles[5]]
    labels_ = [lbls[0], lbls[2], lbls[4], lbls[1], lbls[3], lbls[5]]
    ax2.legend(handles_, labels_, loc = 'upper left',ncol=2,
               columnspacing=0.2, labelspacing = 0.1, handletextpad= 0.1,
               frameon = False, fontsize = fontsize-3)
    
    handles, lbls = ax3.get_legend_handles_labels()
    handles_ = [handles[0], handles[2], handles[4]]
    labels_ = [lbls[0], lbls[2], lbls[4]]
    legend1 = plt.legend(handles_, labels_, loc = 'upper left',
                         frameon = False, fontsize = fontsize-3, labelspacing = 0.1)
    handles_ = [handles[1], handles[3], handles[5]]
    labels_ = [lbls[1], lbls[3], lbls[5]]
    legend2 = plt.legend(handles_, labels_, loc = 'lower center',
                         frameon = False, fontsize = fontsize-3, labelspacing = 0.1)
    ax3.add_artist(legend1)
    ax3.add_artist(legend2)
    ax1.text(0.00001, ax1.get_ylim()[1], '(a)')
    ax2.text(0.00001, ax2.get_ylim()[1], '(b)')
    ax3.text(0.00001, ax3.get_ylim()[1], '(c)')
   
    plt.tight_layout()
    picname = savepath + '/number_ideal.png'
    print (picname)
    plt.savefig(picname)

    fig = plt.figure(figsize = (18,5))
    ax1 = fig.add_subplot(1,3,1)
    ax2 = fig.add_subplot(1,3,2)
    ax3 = fig.add_subplot(1,3,3)
#    fig2 =  plt.figure(figsize = (10,5))
#    ax2 = fig2.add_subplot(1,1,1)
#    ax3 = fig.add_subplot(2,2,3)
#    ax4 = fig.add_subplot(2,2,4)
    m = 0
    for type_ in types:
        for i,ud in enumerate(updirs):
            el = []
            ph = []
            pos = []
            el_t = []
            ph_t = []
            pos_t = []
            el_1gev = []
            ph_1gev = []
            pos_1gev = []
            ne = []
            number = []
            for d in dirs:
                ph_, el_, pos_ = read_gamma(type_ + '/' + ud + '/' + d + '/number_1gev.txt')
                el.append(el_)
                ph.append(ph_)
                pos.append(pos_)

                
                #ph_, el_, pos_ = read_number(type_ + '/' + ud + '/' + d + '/efficiency_1gev.txt')
                #el_1gev.append(el_)
                #ph_1gev.append(ph_)
                #pos_1gev.append(pos_)
                config = utils.get_config(type_ + '/' + ud + '/' + d + '/ParsedInput.txt')
                n_cr = float(config['n_cr'])
                ne_ = float(config['Ne'])
                r = float(config['R'])
                ne.append(ne_/n_cr)
                number.append(4./3.* math.pi * r**3 * ne_)
                el_t.append(2.*4./3.* math.pi * r**3 * ne_)
                pos_t.append(4./3.* math.pi * r**3 * ne_)
            ax1.plot(ne, ph, color[i], label = updir_labels[i])
            ax2.plot(ne, el, color[i], label = updir_labels[i])
            #ax2.plot(ne, el_t, color[i], dashes = [3,3], label = 'N$_{%s}$' % updir_labels[i])
            ax3.plot(ne, pos, color[i], label = updir_labels[i])
            #ax3.plot(ne, pos_t, color[i], dashes = [3,3], label = 'N$_{%s}$' % updir_labels[i])
           
        m += 1
#    ax1.set_yscale('log')
    ax1.set_xscale('log')
    ax1.set_yscale('log')
    #ax1.set_ylim([5e-3, 1]) 
    ax1.set_xlabel(u'$n_0/n_{c}$')
    ax1.set_ylabel(u'N$_{\hbar\omega}$')
    ax2.set_xscale('log')
    ax2.set_yscale('log')
    #ax2.set_ylim([1e-4, 2e-1]) 
    ax2.set_xlabel(u'$n_0/n_{c}$')
    ax2.set_ylabel(u'N$_{e^-}$')
    ax3.set_xscale('log')
    ax3.set_yscale('log')
    #ax3.set_ylim([1e-4, 2e-1]) 
    ax3.set_xlabel(u'$n_0/n_{c}$')
    ax3.set_ylabel(u'N$_{e^+}$')
    ax1.legend(loc = 'lower left', frameon = False, fontsize = fontsize-3)
    #handles, lbls = ax2.get_legend_handles_labels()
    #handles_ = [handles[0], handles[2], handles[4], handles[1], handles[3], handles[5]]
    #labels_ = [lbls[0], lbls[2], lbls[4], lbls[1], lbls[3], lbls[5]]
    ax2.legend(loc = 'lower left', frameon = False, fontsize = fontsize-3)
    #handles, lbls = ax3.get_legend_handles_labels()
    #handles_ = [handles[0], handles[2], handles[4], handles[1], handles[3], handles[5]]
    #labels_ = [lbls[0], lbls[2], lbls[4], lbls[1], lbls[3], lbls[5]]
    ax3.legend(loc = 'lower left', frameon = False, fontsize = fontsize-3)
    
    ax1.text(0.00001, ax1.get_ylim()[1], '(a)')
    ax2.text(0.00001, ax2.get_ylim()[1], '(b)')
    ax3.text(0.00001, ax3.get_ylim()[1], '(c)')
    plt.tight_layout()

    plt.tight_layout()
    picname = savepath + '/number_1gev_ideal.png'
    print (picname)
    plt.savefig(picname)
 

if __name__ == '__main__':
    main()

    
