#!/usr/bin/python
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import utils
import sys
import os
from tqdm import *
from pylab import *

def main():
   
    path = './'
    picspath = '/home/evgeny/Dropbox/tmp_pics'

    config = utils.get_config(path + "/ParsedInput.txt")
    ev = float(config['eV'])
#    phi = int(config['QEDstatistics.OutputN_phi'])
#    theta = int(config['QEDstatistics.OutputN_theta'])
#    dphi = 2*math.pi/phi
#    dtheta = math.pi/theta
    wl = float(config['Wavelength'])

#    S = 2 * math.pi * 0.1 * 0.1 * wl * wl * 100 # square of source mm^2, r = 0.1 lambda 
#    ds = 1e6
#    ds1 = 2 * math.pi * (1 - math.cos(0.05))*1e6
#    ds2 = 2 * math.pi * (1 - math.cos(0.23))*1e6
#    ds3 = 2 * math.pi * (1 - math.cos(0.42))*1e6
#    emin = float(config['QEDstatistics.Emin'])
#    emax = float(config['QEDstatistics.Emax'])
#    ne = int(config['QEDstatistics.OutputN_E'])
    nit = int(config['BOIterationPass'])
    dt = float(config['TimeStep'])*nit
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    duration = float(config.get('Duration',15))
    Xmax = float(config['X_Max']) #mkm
    Xmin = float(config['X_Min']) #mkm
    Ymax = float(config['Y_Max']) #mkm
    Ymin = float(config['Y_Min']) #mkm
    Zmax = float(config['Z_Max']) #mkm
    Zmin = float(config['Z_Min']) #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)
    T = 2 * math.pi/omega
    step = float(config['TimeStep'])*float(config['BOIterationPass'])
    nT = T/step
    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz
    dmy = int(0.35*wl/dy)
    dv = 2.*dx*dy*dz

    nx1 = int(config['MatrixSize_X'])
    ny1 = int(config['MatrixSize_Y'])
    nbeams = int(config['FieldGenerator.Number'])
    
    T = 2 * math.pi/omega
    nnn = int(T/dt)
#    de = emax*1e-9/ev/ne #GeV
#    print 'de =', de
    ftype = config['BODataFormat']
    curr_dir = os.getcwd()
    picname = picspath + '/' + "fields_t_flux_nbeams_%d_%dPW.png" %(nbeams, ppw)
    print curr_dir, picname
    print 'Read electric field in the center vs time'
    ez_c_t = utils.ts(path, utils.ezpath, name='ec', ftype = ftype,
                    tstype='center', shape = (nx1,ny1), verbose=True)
    print 'Read max electric field vs time'
    ez_m_t = utils.ts(path, utils.eypath, name='em', ftype = ftype,
                    tstype='max', shape = (nx1,ny1))
    print 'Read electric field in max in dipole wave vs time'
    ez_md_t = utils.ts(path, utils.ezpath, name='em_d', ftype = ftype,
                      tstype='func', func = lambda x: x[nx1/2, ny1/2-dmy],
                      shape = (nx1,ny1), verbose=True)
    print 'Read magnetic field in the center vs time'
    bz_c_t = utils.ts(path, utils.bzpath, name='bc', ftype = ftype,
                    tstype='center', shape = (nx1,ny1), verbose=True)
    print 'Read max magnetic field vs time'
    bz_m_t = utils.ts(path, utils.bypath, name='bm', ftype = ftype,
                    tstype='max', shape = (nx1,ny1), verbose=True)
    print 'Read Ne in the center vs time'
    nez_c_t = utils.ts(path, utils.nezpath, name='nec', ftype = ftype,
                    tstype='center', shape = (nx1,ny1), verbose=True)
    print 'Read max Ne vs time'
    nez_m_t = utils.ts(path, utils.nezpath, name='nem', ftype = ftype,
                    tstype='max', shape = (nx1,ny1), verbose=True)
    xmin = 0
    xmax = -1
    axis_t = utils.create_axis(ez_c_t.shape[0], 1./nT)
    axis_n_t = utils.create_axis(nez_c_t.shape[0], 1./nT)
    p = 0
    p_prev = 0
    i0 = 0
    i1 = 0
    threshold = 0.97
    nl_power = np.zeros(len(ez_m_t))
    
    for i in range(len(ez_m_t)):
        p_prev = p
        p = ((bz_m_t[i]/3e11)**2 + (ez_m_t[i]/0.653/3e11)**2)*10.
        
        if i0 == 0 and p > threshold * ppw and p_prev < threshold * ppw:
            i0 = i
        if i1 == 0 and p < threshold * ppw and p_prev > threshold * ppw:
            i1 = i
        nl_power[i] = ((bz_c_t[i]/3e11)**2 + (ez_md_t[i]/0.653/3e11)**2)*10.
    print i0, i1
    i2 = i1 + 120
    if i1 != i0:
        gamma = math.log(nez_m_t[i1]/nez_m_t[i0])/(i1 - i0)*nT
    else:
	gamma = 0
    print gamma

    '''    ax_ = []
    ax1_ = []
    pow_t = []
    pow1_t = []
    max_t = []
    av_t = []
    br_full_t = []
    br_100mev_t = []
    br_1gev_t = []
        
    f = open('br_t.dat', 'r')
    i = 0
    T1 = 2*math.pi/omega*1e15
    for line in f:
        tmp = line.split()
        ax_.append(float(tmp[0])/T/1e15)
        pow_t.append(ppw-float(tmp[2]))
        pow1_t.append(float(tmp[2]))
        max_t.append(float(tmp[3]))
        av_t.append(float(tmp[4]))
        ax1_.append(float(tmp[0])/T/1e15 - 2.)
        br_full_t.append(float(tmp[5]))
        br_100mev_t.append(float(tmp[6]))
        br_1gev_t.append(float(tmp[7]))

    el_ax_ = []
    el_ax1_ = []
    el_pow_t = []
    el_pow1_t = []
    el_max_t = []
    el_av_t = []
    el_br_full_t = []
    el_br_100mev_t = []
    el_br_1gev_t = []
        
    f = open('el_br_t.dat', 'r')
    i = 0
    T1 = 2*math.pi/omega*1e15
    for line in f:
        tmp = line.split()
        el_ax_.append(float(tmp[0])/T/1e15)
        el_pow_t.append(ppw-float(tmp[2]))
        el_pow1_t.append(float(tmp[2]))
        el_max_t.append(float(tmp[3]))
        el_av_t.append(float(tmp[4]))
        el_ax1_.append(float(tmp[0])/T/1e15 - 2.)
        el_br_full_t.append(float(tmp[5]))
        el_br_100mev_t.append(float(tmp[6]))
        el_br_1gev_t.append(float(tmp[7]))

    for i in range(len(el_pow1_t)):
        el_pow_t[i] = ppw - 2.*el_pow1_t[i] - pow1_t[i]
    '''
        
    fig = plt.figure(num=None, figsize = (18, 15))
    mp.rcParams.update({'font.size': 16})
    ax = fig.add_subplot(4,2,1)
    ax.set_title('(a) Electric field')
    fc, = ax.plot(axis_t, ez_c_t, 'b', label = 'Center')
    fm, = ax.plot(axis_t, ez_m_t, 'r', label = 'Max')
    fm, = ax.plot(axis_t, ez_md_t, 'g', label = 'Max dipole')
    ax.axvline(i1/nT, color = 'gray')
    ax.axvline(i0/nT, color = 'gray')
    ax.axvline(i2/nT, color = 'gray')
    ax.set_xlim([0, axis_t[-1]])
    ax.set_xlabel('t, T')
    ax.set_ylabel('E, CGS')
    plt.legend(loc = 'upper left', frameon = False)
#    ax.set_xlim([xmin, xmax])
    ax = fig.add_subplot(4,2,2)
    ax.set_title('(b) Magnetic field')
    fc, = ax.plot(axis_t, bz_c_t, 'b', label = 'Center')
    fm, = ax.plot(axis_t, bz_m_t, 'r', label = 'Max')
    ax.set_xlim([0, axis_t[-1]])
    ax.set_xlabel('t, T')
    ax.set_ylabel('B, CGS')
    plt.legend(loc = 'upper left', frameon = False)
    ax = fig.add_subplot(4,2,3)
    ax.set_title('(c) Electron density')
#    fc, = ax.plot(axis_t, nez_c_t/dv, 'b', label = 'Ne Center')
    print len(axis_t), len(nez_m_t) 
    fm, = ax.plot(axis_n_t, nez_m_t/dv, 'r', label = 'Max')
    ax.set_xlabel('t, T')
    ax.set_ylabel('Ne, cm-3')
    ax = ax.twinx()
    fm, = ax.plot(axis_n_t, nez_m_t/dv, 'b', label = 'Max')
    ax.set_yscale('log')
    ax.set_xlim([0, axis_t[-1]])
    plt.legend(loc = 'upper left', frameon = False)
    ax.set_xlabel('t, T')
    ax.set_ylabel('Ne, cm-3')
    ax = fig.add_subplot(4,2,4)
    ax.set_title('(d) Radiated power')
    fc, = ax.plot(axis_t, nl_power, 'b', label = 'field')
#    fc, = ax.plot(ax1_, pow1_t, 'r', label = 'ph')
#    fc, = ax.plot(el_ax1_, el_pow1_t, 'g', label = 'el')
#    fc, = ax.plot(el_ax1_, el_pow_t, 'k', label = 'calc')
    ax.set_xlim([0, axis_t[-1]])
    ax.set_xlabel('t, T')
    ax.set_ylabel('P, PW')
    plt.legend(loc = 'upper left', frameon = False)
    ax = fig.add_subplot(4,2,5)
    ax.set_title('(e) Max energy')
#    fc, = ax.plot(ax1_, max_t, 'b', label = 'ph')
#    fc, = ax.plot(ax1_, el_max_t, 'r', label = 'el')
    ax.set_xlim([0, axis_t[-1]])
    ax.set_xlabel('t, T')
    ax.set_ylabel('Max energy, GeV')
    plt.legend(loc = 'upper left', frameon = False)
    ax = fig.add_subplot(4,2,6)
    ax.set_title('(f) Av energy')
#    fc, = ax.plot(ax1_, av_t, 'b', label = 'ph')
#    fc, = ax.plot(ax1_, el_av_t, 'r', label = 'el')
    ax.set_xlim([0, axis_t[-1]])
    ax.set_xlabel('t, T')
    ax.set_ylabel('Av energy, GeV')
#    ax.set_xlim([xmin, xmax])
    plt.legend(loc = 'upper left', frameon = False)
    ax = fig.add_subplot(4,2,7)
    ax.set_title('(g) Flux > 100 MeV')
#    fc, = ax.plot(ax1_, br_100mev_t, 'b', label = 'ph')
#    fc, = ax.plot(ax1_, np.array(el_br_100mev_t)*10, 'r', label = 'el x10')
    ax.set_xlim([0, axis_t[-1]])
    ax.set_xlabel('t, T')
    ax.set_ylabel('Max energy, GeV')
    plt.legend(loc = 'upper left', frameon = False)
    ax = fig.add_subplot(4,2,8)
    ax.set_title('(h) Flux > 1 GeV')
#    fc, = ax.plot(ax1_, br_1gev_t, 'b', label = 'ph')
#    fc, = ax.plot(ax1_, np.array(el_br_1gev_t)*10, 'r', label = 'el x10')
    ax.set_xlim([0, axis_t[-1]])
    ax.set_xlabel('t, T')
    ax.set_ylabel('Av energy, GeV')
#    ax.set_xlim([xmin, xmax])
    plt.legend(loc = 'upper left', frameon = False)
    plt.tight_layout()
    plt.savefig(picname)
    plt.close()

        
    '''length = len(nez_m_t) - i2
    ne_av = np.sum(nez_m_t[i2:])/length/dv
    ne_max = np.amax(nez_m_t)/dv
    b_av = np.sum(bz_m_t[i2:])/length
    e_av = np.sum(ez_m_t[i2:])/length
    power_av = np.sum(nl_power[i2:])/length
    p_ph = np.sum(pow1_t[i2:])/length
    en_max_ph = np.amax(max_t[i2:])
    en_av_ph = np.sum(av_t[i2:])/length
    flux_ph_full = np.sum(br_full_t[i2:])/length
    flux_ph_100mev = np.sum(br_100mev_t[i2:])/length
    flux_ph_1gev = np.sum(br_1gev_t[i2:])/length
    p_el = np.sum(el_pow1_t[i2:])/length
    en_max_el = np.amax(el_max_t[i2:])
    en_av_el = np.sum(el_av_t[i2:])/length
    flux_el_full = np.sum(el_br_full_t[i2:])/length
    flux_el_100mev = np.sum(el_br_100mev_t[i2:])/length
    flux_el_1gev = np.sum(el_br_1gev_t[i2:])/length
    
    print length, ne_av, ne_max, b_av, e_av, power_av, p_ph, en_max_ph, en_av_ph, flux_ph_full, flux_ph_100mev, flux_ph_1gev, p_el, en_max_el, en_av_el, flux_el_full, flux_el_100mev, flux_el_1gev 
    f = open('gamma_ne_fields.txt', 'w')
    f.write('%lf %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le\n' % (gamma,                                                                             ne_av, ne_max, b_av, e_av, power_av, p_ph, en_max_ph, en_av_ph, flux_ph_full, flux_ph_100mev, flux_ph_1gev, p_el, en_max_el, en_av_el, flux_el_full, flux_el_100mev, flux_el_1gev))
    f.close()
    '''
    
#    plt.show()
if __name__ == '__main__':
    main()
