#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_file(filename, norm = 0):
    ax = []
    res = []
    f = open(filename, 'r')
    for line in f:
        tmp = line.split()
        ax.append(float(tmp[0]))
        res.append(float(tmp[1]))
    m = max(res)
    if m != 0 and norm != 0:
        for i in range(len(res)):
            res[i] /= m
    return ax,res
        

def main():
    n = len(sys.argv) - 2
    num = int(sys.argv[-1])  
    fig = plt.figure(num=None)
    axel = fig.add_subplot(2,2,1)
    axph = fig.add_subplot(2,2,3)
    axmel = fig.add_subplot(2,2,2)
    axmph = fig.add_subplot(2,2,4)
    
    axph.set_xlabel('Angle, grad')
    axel.set_title('Electron radiation pattern')
    axph.set_title('Photon radiation pattern')
    axmel.set_title('Angle of electron maximum radiation')
    axmph.set_title('Angle of photon maximum radiation')
    for i in range(n):
        path = sys.argv[i+1]
        config = utils.get_config(path + "/ParsedInput.txt")
        pwr = float(config['PeakPowerPW'])
        ax, dn_el = read_file(path + '/' + 'sp_t_el%d.dat'%num, 1)
        ax, dn_ph = read_file(path + '/' + 'sp_t_%d.dat'%num, 1)
        axel.plot(ax, dn_el, label = str(pwr))
        axph.plot(ax, dn_ph, label = str(pwr))
        ax, m_el = read_file(path + '/' + 'max_t_el%d.dat'%num)
        ax, m_ph = read_file(path + '/' + 'max_t_%d.dat'%num)
        axmel.plot(ax, m_el, label = str(pwr))
        axmph.plot(ax, m_ph, label = str(pwr))

    plt.legend(loc = 'upper right')
    plt.show()

if __name__ == '__main__':
    main()
