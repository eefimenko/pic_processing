#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import math
from utils import *
import sys
import os

def summ_over_z(array):
    res = [0]*len(array)
    for j in range(len(array[0])):
        for i in range(len(array)):
            res[i] += array[i][j]
    return res

def average_gamma(array, dg):
    n = sum(array)
#    print 'Av gamma', len(array), n
    s = 0
    for i in range(len(array)):
        s += array[i]*(i+0.5)*dg
    if n != 0:
        return s/n
    else:
        return 0
def distr_width(array, xmin, dx):
    n = len(array)
    m = 0
    imax = 0
    x = [i*dx + xmin for i in range(n)]
    
    for i in range(n):
        if array[i] > m:
            m = array[i]
            imax = i

    for j in range(imax):
        if array[imax - j] < 0.5*m:
            x0 = x[imax-j]
            break
    for j in range(n-imax):
        if array[imax + j] < 0.5*m:
            x1 = x[imax+j]
            break
            
    s2 = x1 - x0
    return s2

 


def distr_width_b(array):
    n = len(array)
    i1 = np.argmax(array[:n/2])
    i2 = np.argmax(array[n/2:]) + n/2
    return i1,i2,i2-i1

def main():
    picspath = 'pics'
    config = get_config("ParsedInput.txt")
    wl = float(config['Wavelength'])
    Xmax = float(config['X_Max']) #mkm to wavelength
    Xmin = float(config['X_Min']) #mkm to wavelength
    Ymax = float(config['Y_Max']) #mkm to wavelength
    Ymin = float(config['Y_Min']) #mkm to wavelength
    Zmax = float(config['Z_Max']) #mkm to wavelength
    Zmin = float(config['Z_Min']) #mkm to wavelength

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)
    if 'resize' in config.keys():
        factor = int(config['resize'])
    else:
        factor = 1
    if 'rescale' in config.keys():
        rescale = int(config['rescale'])
    else:
        rescale = 1
    print factor
    path = os.getcwd()
    Xmax /= rescale
    Xmin /= rescale
    Ymax /= rescale
    Ymin /= rescale
    Zmax /= rescale
    Zmin /= rescale
        
    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz

    if os.path.exists(elgammarpath):
        rmin = float(config['ElGammaR.SetBounds_0'])*1e4
        rmax = float(config['ElGammaR.SetBounds_1'])*1e4
        gmin = float(config['ElGammaR.SetBounds_2'])
        gmax = float(config['ElGammaR.SetBounds_3']) 
        nr = int(config['ElGammaR.SetMatrixSize_0'])
        ng = int(config['ElGammaR.SetMatrixSize_1'])
        dr = (rmax - rmin)/nr
        dg = (gmax - gmin)/ng
    
#    Mmin = float(config['ElMomentum.SetBounds_2'])
#    Mmax = float(config['ElMomentum.SetBounds_3'])
    BOIterationPass = float(config['BOIterationPass'])
    ftype = str(config['BODataFormat'])
    timestep = float(config['TimeStep'])
    dt = float(config['TimeStep'])*1e15*BOIterationPass
    

    delta = 1

    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = num_files(eypath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = num_files(eypath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    xmin = 0.04
    dirname = os.path.split(os.getcwd())[1]
    if 'r' not in dirname:
        dv = 2.*dx*dy*dz*factor
    else:
        dv = 2.*dx*dy*dz*rescale

    axis = create_axis(nmax-nmin, dt, 0)

    e2max = []
    b2max = []
    nemax = []
    gammaav = []
    wp = []
    ndtwp = []
    ndxne = []
    dxne = []
    print ftype
    for i in range(nmin, nmax, delta):
        print i
        fielde2x = bo_file_load(eypath,i,ny,nz,ftype=ftype)
        e2max.append(np.amax(fielde2x))
        fieldb2x = bo_file_load(bypath,i,ny,nz,ftype=ftype)
        b2max.append(np.amax(fieldb2x))
        nefield = bo_file_load(nezpath,i,ny,nz,ftype=ftype)/dv
        nemax_ = np.amax(nefield)
        nemax.append(nemax_)
        nz_ = int(0.125e-4/dz)
        neshape = nefield.shape
        nz0 = neshape[1]/2
        print neshape, nz0, nz_
        res = np.sum(fieldb2x[:,nz0-nz_:nz0+nz_], axis = 1)
#        fig = plt.figure()
#        ax = fig.add_subplot(1,1,1)
#        e, = ax.plot(res, marker = 'o')
#        nx = len(res)/2
#        ax.set_xlim([nx - 10, nx + 10])
#        i1,i2,di = distr_width_b(res)
#        m = max(res)
#        ax.plot([i1, i1],[0, m])
#        ax.plot([i2, i2],[0, m])
#        plt.show()
        i1,i2,di = distr_width_b(res)
        if di > 100:
            di = 0
        w = dx*di
        print w/dx
        ndxne.append(w/dx)
        dxne.append(w)
        if os.path.exists(elgammarpath):
            pfield = bo_file_load(elgammarpath,i,nr,ng,ftype=ftype)
            print i, pfield.shape, int(0.5/dr), pfield[:128, :].shape
            nr_ = int(0.5/dr) 
            res = np.sum(pfield[:nr_, :], axis = 0)          

            print 'Max = ', max(res), np.amax(pfield)
            gammaav.append(average_gamma(res, dg))
            wp_ = math.sqrt(4*math.pi*ElectronCharge*ElectronCharge*nemax_/ElectronMass)
            wp.append(wp_)
            if wp_ > 0:
                ndtwp.append(2*math.pi/wp_/timestep)
            else:
                ndtwp.append(0)
        else:
            gammaav.append(0)
            wp.append(0)
            ndtwp.append(0)

    fig = plt.figure()
    ax = fig.add_subplot(3,1,1)
    e, = ax.plot(axis, e2max)
    
    ax.set_yscale('log')
    ay = ax.twinx()
    ne, = ay.plot(axis, nemax, 'r')
    ay.set_yscale('log')
    plt.legend([e,ne],['|E|','Ne'], loc = 'upper left')
    ax = fig.add_subplot(3,1,2)
    g, = ax.plot(axis, gammaav)
    ay = ax.twinx()
    wp_, = ay.plot(axis, ndtwp, 'r')
    plt.legend([g,wp_],['av gamma','Tp/dt'], loc = 'upper left')
    ax = fig.add_subplot(3,1,3)
    b, = ax.plot(axis, b2max)
    ay = ax.twinx()
    n_, = ay.plot(axis, ndxne, 'r')
    plt.legend([b, n_],['|B|', 'N dx/ne'], loc = 'upper left')
    plt.tight_layout()
    plt.show()
    f = open('analyse_pinch.dat', 'w')
    for i in range(len(nemax)):
        f.write('%le %le %le %le %le %le %le %le %le\n' % (axis[i], e2max[i], b2max[i], nemax[i], gammaav[i], wp[i], ndtwp[i], ndxne[i], dxne[i]))
    f.close()
    
if __name__ == '__main__':
    main()
