#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import math
import utils
import sys
import numpy as np
print(np.__version__)
import os
from tqdm import *
print(np.__version__)
import pickle

def calculate_volume(field, dx, dy):
    volume = 0.
    volume1 = 0.
    ny = field.shape[1]
    nx = field.shape[0]
    ilow = nx//2
    ihigh = nx//2
    print(field.shape)
    
    for j in range(field.shape[1]):
        for i_ in range(nx//2):
                if field[i_,j] > 0:
                    ilow = i_
                    break
        for i_ in range(nx//2):
                if field[i_,j] > 0:
                    ihigh = nx - i_ - 1
                    break
        for i in range(nx):
            if field[i,j] > 0:
                volume1 += dy*dx* math.pi*dx * abs(j-field.shape[1]//2)
        for i in range(ilow,ihigh):
            volume += dy*dx* math.pi*dx * abs(j-field.shape[1]//2)
    return volume, volume1

def main():
    import numpy as np
    print(np.__version__)
    picspath = '/home/evgeny/Dropbox/MG'
    path = '.'
    config = utils.get_config("ParsedInput.txt")
    
    nit = float(config['BOIterationPass'])
    dt = float(config['TimeStep'])*1e15*nit #fs
    power = int(config['PeakPowerPW'])
    ppw = int(config['PeakPower'])*1e-22 #PW
    omega = float(config['Omega'])
    dt = float(config['TimeStep'])
    T = 2 * math.pi/omega
    dtt = dt/T
    print("Power = " + str(ppw) + 'PW')
    print("dt = " + str(dt) + 'fs')
    wl = float(config['Wavelength'])
    print(np.__version__) 
    Xmax = float(config['X_Max'])/1e-4 #mkm
    Xmin = float(config['X_Min'])/1e-4 #mkm
    Ymax = float(config['Y_Max'])/1e-4 #mkm
    Ymin = float(config['Y_Min'])/1e-4 #mkm
    Zmax = float(config['Z_Max'])/1e-4 #mkm
    Zmin = float(config['Z_Min'])/1e-4 #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print('Nx = ' + str(nx)) 
    print('Ny = ' + str(ny))
    print('Nz = ' + str(nz))

    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz
    mult_dv = 1/(2.*dx*dy*dz*1e-12)
    step = (Xmax-Xmin)/nx
    const_a_p = 7.81441e-9
    nmin, nmax, delta = utils.get_min_max_iteration(sys.argv,
                                                    os.path.join('BasicOutput', utils.ezpath),
                                                    os.path.join('BasicOutput', utils.bzpath),
                                                    os.path.join('BasicOutput', utils.nezpath))
    print('Here', nmin, nmax, delta)
    print(nx, dx, ny,dy)
    
    print(np.__version__)

    nmin = 350
    nmax = min(nmax, 900)
    
    filename = 'number.pkl'
    if os.path.exists(filename):
        with open(filename, 'rb') as f:
            ne_t = pickle.load(f)
            np_t = pickle.load(f)
    else:
        ne_t = np.zeros((nmax))
        np_t = np.zeros((nmax))
        for it in tqdm(range(nmin, nmax)):
            read, ne_ = utils.bo_file_load(os.path.join('BasicOutput', utils.nezpath), it, nx=nx,ny=ny, ftype='txt',transpose = 1, verbose = 0, archive = None)
            read, np_ = utils.bo_file_load(os.path.join('BasicOutput', utils.npzpath), it, nx=nx,ny=ny, ftype='txt',transpose = 1, verbose = 0, archive = None)
            r = 0.9
            for i_ in range(nx):
                x_ = (i_ - nx//2)*dx
                for j in range(ny):
                    y_ = (j - ny//2)*dy
                    if (x_*x_ + y_*y_) < r*r:
                        ne_t[it] += ne_[i_,j]
                        np_t[it] += np_[i_,j]
        with open(filename, 'wb') as f:
            pickle.dump(ne_t, f)
            pickle.dump(np_t, f)
            
    np_t *= mult_dv

    filename = 'number_pos_full.pkl'
    if os.path.exists(filename):
        with open(filename, 'rb') as f:
            #ne_t = pickle.load(f)
            np_full_t = pickle.load(f)
    else:
        #ne_t = np.zeros((nmax))
        np_full_t = np.zeros((nmax))
        for it in tqdm(range(nmin, nmax)):
            #read, ne = utils.bo_file_load(os.path.join('BasicOutput', utils.nezpath), i, nx=nx,ny=ny, ftype='txt',transpose = 1, verbose = 0, archive = None)
            read, np_ = utils.bo_file_load(os.path.join('BasicOutput', utils.npzpath), it, nx=nx,ny=ny, ftype='txt',transpose = 1, verbose = 0, archive = None)
            r = 0.9
            for i_ in range(nx):
                for j in range(ny):
                    np_full_t[it] += np_[i_,j]
        with open(filename, 'wb') as f:
            pickle.dump(np_full_t, f)
    np_full_t *= mult_dv

    print('Read electric field in the center vs time')
    ez_t = utils.ts(path, os.path.join('BasicOutput', utils.ezpath), name='ez',
                    tstype='center', shape = (nx,ny), verbose=0)
    
    
    filename = 'number_el_full.pkl'
    if os.path.exists(filename):
        with open(filename, 'rb') as f:
            #ne_t = pickle.load(f)
            ne_full_t = pickle.load(f)
    else:
        #ne_t = np.zeros((nmax))
        ne_full_t = np.zeros((nmax))
        for it in tqdm(range(nmax)):
            #read, ne = utils.bo_file_load(os.path.join('BasicOutput', utils.nezpath), i, nx=nx,ny=ny, ftype='txt',transpose = 1, verbose = 0, archive = None)
            read, ne_ = utils.bo_file_load(os.path.join('BasicOutput', utils.nezpath), it, nx=nx,ny=ny, ftype='txt',transpose = 1, verbose = 0, archive = None)
            for i_ in range(nx):
                x_ = (i_ - nx//2)*dx
                for j in range(ny):
                    y_ = (j - ny//2)*dy
                    if abs(x_) < 20 and abs(y_) < 20:
                        ne_full_t[it] += ne_[i_,j]
        with open(filename, 'wb') as f:
            pickle.dump(ne_full_t, f)
        
    print(ne_t.max(), np_t.max(), np_full_t.max())

    axis = utils.create_axis(len(ez_t), dt*nit/T)

    relf = omega * 3e10 * 9.1e-28/4.8e-10
    print(relf)
    ez_t /= relf*1000

    fontsize = 18
    mp.rcParams.update({'font.size': fontsize})
    
    axis = utils.create_axis(len(ez_t), dt*nit/T)
    axis1 = utils.create_axis(len(ne_t), dt*nit/T)
    fig = plt.figure(figsize=(7, 5))
    ax1 = fig.add_subplot(1, 1, 1)
    lf, = ax1.plot(axis, ez_t, 'r', dashes = [], label = '|E|')
    ax2 = ax1.twinx()
    le, = ax2.plot(axis1, ne_full_t/ne_full_t[:400].max(), 'b', dashes = [2,2], label = 'N$_{e^-}$')
    lp, = ax2.plot(axis1, np_full_t/ne_full_t[:400].max(), 'g', dashes = [6,1], label = 'N$_{e^+}$')
    ax2.axhline(y=1, color='grey', alpha=0.3)
    plt.legend([lf, le, lp], ['|E|', 'N$_{\mathrm{e}^-}$', 'N$_{\mathrm{e}^+}$'], frameon=False, loc='center left')
    ax2.set_yscale('log')
    ax1.set_ylim([0, ez_t.max()*1.05])
    ax1.set_xlim([45, 80])
    ax1.set_xlabel('t/T')
    ax1.set_ylabel('|E|/a$_0$, $\\times 10^3$')
    ax2.set_ylabel('N$_{\mathrm{e}^-}$/N$_0$, N$_{\mathrm{e}^+}$/N$_0$')
    if ppw == 8:
        label = '(a)'
    elif ppw == 12:
        label = '(b)'
    else:
        label = '(c)'
    ax1.text(40, ez_t.max()*1.05, label)
    
    ax1.text(72.5, 0.1 * ez_t.max()/2., '%d PW' % ppw,
             bbox = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9))
    picname = os.path.join(picspath, 'number_2d_%dpw_norm.png' % ppw)
    plt.tight_layout()
    plt.savefig(picname, dpi=256)

    
    fig = plt.figure(figsize=(7, 5))
    ax1 = fig.add_subplot(1, 1, 1)
    ax1.plot(ne_t, 'r', label='El')
    ax1.plot(np_t, 'b', label='Pos')
    ax1.plot(np_full_t, 'g', label='Pos full')
    ax1.plot(ne_full_t, 'm', label='El full')
    
    plt.legend(frameon=False, loc='upper left')
    picname = os.path.join(picspath, 'number_%dpw.png'%power)
    plt.tight_layout()
    plt.savefig(picname, dpi=256)            
    iteration = nmax-1
    
    archive = None#os.path.join(path, 'BasicOutput', 'data.zip')
    fontsize = 14
    mp.rcParams.update({'font.size': fontsize})
<<<<<<< HEAD
    fig = plt.figure(figsize=(10, 5))
    ax1 = fig.add_subplot(1, 1, 1)
    #ax2 = fig.add_subplot(2, 1, 2)
    read, field = utils.bo_file_load('statdata/el/InitialPos', iteration,nx=nx,ny=ny,ftype='txt',fmt='%d.0000',transpose = 1, verbose = 1, archive = None)
    print(field.shape)
    m = field.max()
    print(m)
=======
    fig = plt.figure(figsize=(7, 7))
    ax1 = fig.add_subplot(1, 1, 1)
    field = bo_file_load('statdata/el/InitialPos', iteration,nx=nx,ny=ny,ftype='txt',fmt='%d.0000',transpose = 1, verbose = 1, archive = None)
    ax1.imshow(field, origin='lower', cmap = 'Greens', extent=[XMin, XMax, YMin, Ymax])
>>>>>>> 295b4ea8b5d302ae60f4da86ed850d73ecb3d9eb
    
    ax1.imshow(field, origin='lower', cmap = 'Greens', extent=[Xmin, Xmax, Ymin, Ymax], norm=clr.LogNorm(clip='True', vmin = 1e-4*m, vmax = 1e-1*m))
    #ax2.plot(field[nx//2,:])
    #ax2.set_yscale('log')
    #ax2.plot(field)
    #ax1.set_xlim([-20, 20])
    #ax1.set_ylim([-20, 20])
    plt.tight_layout()
    picname = os.path.join(picspath, 'initial_pos_%dpw.png'% power)
    plt.savefig(picname, dpi=256)
    plt.close()

    volume = 0.
    filename = 'volume.txt'
    if os.path.exists(filename):
        with open(filename, 'r') as f:
            line = f.readline()
            volume = float(line.split()[0])
            volume1 = float(line.split()[1])
    else:
        volume, volume1 = calculate_volume(field, dx, dy)
        with open(filename, 'w') as f:
            f.write('%f %f' % (volume, volume1))
    
    print('Volume', volume, volume1)
    
if __name__ == '__main__':
    main()
