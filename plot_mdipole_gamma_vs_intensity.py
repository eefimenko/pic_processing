#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils

def read_gamma(filename):
    f = open(filename, 'r')
    field = []
    gamma = []
    power = []
    for line in f:
        tmp = line.split()
        if float(tmp[0]) < 101:
            field.append(3*math.sqrt(float(tmp[0])/10.))
            gamma.append(float(tmp[1]))
            power.append(float(tmp[0]))
    f.close()
    return power, field, gamma

def main():
    picspath = 'pics'
    power, field, gamma = read_gamma("gamma.dat")
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
    power_ = [25, 50, 70, 100]
    gamma_ = [0.866, 5.12, 6.749, 8.504]
    field_ = [3*math.sqrt(float(x)/10.)/1.53 for x in power_]
    fig = plt.figure(num=None, figsize=(7, 5.))
    mp.rcParams.update({'font.size': 14})
    ax1 = fig.add_subplot(1,1,1)
    p, = ax1.plot(field, gamma, 'r', label = u'e-тип', marker = 'o')
    p1, = ax1.plot(field_, gamma_, 'g', label = u'm-тип', marker = 'x')
    ax1.set_ylabel('$\Gamma T$')
    ax1.set_xlabel(u'Электрическое поле, $\\times 10^{11}$ статВ/см')
    ax1.set_xlim([2.5, 6.5])
    ax1.set_ylim([0, 10])
    plt.legend(frameon=False, loc = 'upper left', numpoints=1)
    plt.tight_layout()
    picname = picspath + '/' + "gamma_mdipole_vs_edipole.png"
    ax1.text(2., 10, u'(б)', fontsize=20)
    plt.savefig(picname, dpi=512)
    plt.close()
    fig = plt.figure(num=None, figsize=(7, 5.))
    mp.rcParams.update({'font.size': 14})
    ax1 = fig.add_subplot(1,1,1)
    p, = ax1.plot(power, gamma, 'r', label = u'e-тип', marker = 'o')
    p1, = ax1.plot(power_, gamma_, 'g', label = u'm-тип', marker = 'x')
    ax1.set_ylabel('$\Gamma T$')
    ax1.set_xlabel(u'Мощность, ПВт')
    ax1.set_xlim([0, 100])
    ax1.set_ylim([0, 10])
    plt.legend(frameon=False, loc = 'upper left', numpoints=1)
    plt.tight_layout()
    picname = picspath + '/' + "gamma_mdipole_vs_edipole_power.png"
    ax1.text(-12, 10, u'(а)', fontsize=20)
    plt.savefig(picname, dpi=512)
    plt.close()
    

if __name__ == '__main__':
    main()
