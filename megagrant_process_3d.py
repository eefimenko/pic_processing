#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import math
import utils
import sys
import numpy as np
print(np.__version__)
import os
from tqdm import *
print(np.__version__)
import pickle

def main():
    fontsize = 18
    mp.rcParams.update({'font.size': fontsize})
    
    picspath = '/home/evgeny/Dropbox/MG'
    path = '.'
    config = utils.get_config(path + "/ParsedInput.txt")
    wl = float(config['Wavelength'])

    dt = float(config['TimeStep'])
    nit = int(config['BOIterationPass'])
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    
    nx1 = int(config['MatrixSize_X'])
    ny1 = int(config['MatrixSize_Y'])
    T = 2 * math.pi/omega
    nnn = int(T/dt)
    
        
    print('Read electric field in the center vs time')
    ez_t = utils.ts(path, utils.ezpath, name='ez',
                    tstype='center', shape = (nx1,ny1), verbose=0)
    ne_t = utils.ts(path, 'data/NeTrap', name='ne',
                    tstype='sum', shape = (2,2), verbose=0)
    np_t = utils.ts(path, 'data/NposTrap', name='np',
                    tstype='sum', shape = (2,2), verbose=0)

    relf = omega * 3e10 * 9.1e-28/4.8e-10
    print(relf)
    ez_t /= relf*1000
    
    axis = utils.create_axis(len(ez_t), dt*nit/T)
    fig = plt.figure(figsize=(7, 5))
    ax1 = fig.add_subplot(1, 1, 1)
    lf, = ax1.plot(axis, ez_t, 'r', dashes = [], label = '|E|')
    ax2 = ax1.twinx()
    le, = ax2.plot(axis, ne_t/ne_t[0], 'b', dashes = [2,2], label = 'N$_{e^-}$')
    lp, = ax2.plot(axis, np_t/ne_t[0], 'g', dashes = [6,1], label = 'N$_{e^+}$')
    ax2.axhline(y=1, color='grey', alpha=0.3)
    plt.legend([lf, le, lp], ['|E|', 'N$_{\mathrm{e}^-}$', 'N$_{\mathrm{e}^+}$'], frameon=False, loc='center left')
    ax2.set_yscale('log')
    ax1.set_ylim([0, ez_t.max()*1.05])
    ax1.set_xlim([35, axis[-1]])
    ax1.set_xlabel('t/T')
    ax1.set_ylabel('|E|/a$_0$, $\\times 10^3$')
    ax2.set_ylabel('N$_{\mathrm{e}^-}$/N$_0$, N$_{\mathrm{e}^+}$/N$_0$')
    if ppw == 10:
        label = '(a)'
    elif ppw == 15:
        label = '(b)'
    else:
        label = '(c)'
    ax1.text(29, ez_t.max()*1.05, label)
    
    ax1.text(59.5, 0.1 * ez_t.max()/2., '%d PW' % ppw,
             bbox = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9))
    picname = os.path.join(picspath, 'number_3d_%dpw_norm.png' %ppw)
    plt.tight_layout()
    plt.savefig(picname, dpi=256)
    with open('ratio.txt', 'w') as f:
        f.write('%lf' % (np_t[-1]/ne_t[0]))
    #picname = os.path.join(picspath, 'analyse_3d_%d.png' % (ppw))
    #plt.tight_layout()
    #plt.savefig(picname, dpi=256)            

    
if __name__ == '__main__':
    main()
