#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils
import numpy as np

def main():
    path = './'
    config = utils.get_config(path + "/ParsedInput.txt")
    picspath = 'pics/'

    iterations = [200, 350, 430, 920]
    labels = ['a', 'b', 'c', 'd', 'e', 'f']
    wl = float(config['Wavelength'])
    xmax = float(config['X_Max'])/wl #mkm
    xmin = float(config['X_Min'])/wl #mkm
    ymax = float(config['Y_Max'])/wl #mkm
    ymin = float(config['Y_Min'])/wl #mkm
    zmax = float(config['Z_Max'])/wl #mkm
    zmin = float(config['Z_Min'])/wl #mkm
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    power = int(config['PeakPowerPW'])
    dx = float(config['Step_X'])/wl
    dy = float(config['Step_Y'])/wl
    dz = float(config['Step_Z'])/wl
    mult = (1/(2.*dx*dy*dz*wl*wl*wl))
    step = float(config['TimeStep'])*1e15*float(config['BOIterationPass'])
    omega = float(config['Omega'])
    ncr = utils.ElectronMass * omega * omega/(8. * math.pi * utils.ElectronCharge * utils.ElectronCharge)
    T = 2 * math.pi/omega
    nt = int(T*1e15/step)
    dt  = step/(T*1e15)
    print nt
    print 'Critical density ', ncr
    figures = [utils.bx_zpath, utils.nezpath]
    cmaps = ['bwr', 'Greens']
    titles = ['$B_\\rho$', '$N_e$']
    log = [False, False]
             

#    n0 = [703, 0, 0] # 675
#    n0 = [686, 0, 0] # 680
#    n0 = [1030, 1090]
    
    spx = 1
    spy = 4

    xlim = [-0.4, 0.4]
    ylim = xlim
    verbose = 1
    # nticks = 5
    # ticks = [xlim[0] + i*(xlim[1]-xlim[0])/(nticks-1) for i in range(nticks)]
    # print ticks
    niter = len(iterations)
        
    # fig = plt.figure(num=None, figsize=(16., 4.))
    # mp.rcParams.update({'font.size': 6})
    # fontsize = 10
    lw = 0.4
    for i in range(niter):
        fig = plt.figure(num=None, figsize=(4., 4.))
        mp.rcParams.update({'font.size': 14})
        fontsize = 14
        fx = utils.bo_file_load(path + utils.bx_zpath,iterations[i],nx,ny,verbose=1)
        fy = utils.bo_file_load(path+utils.by_zpath,iterations[i],nx,ny,verbose=1)
        fx0 = utils.bo_file_load('../10pw_vac/'+utils.bx_zpath,iterations[i],nx,ny,verbose=1)
        fy0 = utils.bo_file_load('../10pw_vac/'+utils.by_zpath,iterations[i],nx,ny,verbose=1)
            
        fr,fphi = utils.convert_xy_to_rphi(fx-fx0,fy-fy0,dx,dy)
        ratio = np.amax(fr)/np.amax(fphi) 
        print ratio, fr.shape, np.amax(fr)
        xticks = [-0.4, 0, 0.4]
        if i%4 != 0:
            ylabel = ''
            yticks = []
        else:
            ylabel = '$y/\lambda$'
            yticks = [-0.4, 0, 0.4]
        clevels = [-0.15, 0.15]
        ax = utils.subplot(fig, iterations[i], path, field = fr,
                           shape = (nx,ny), position = (1,1,1),
                           extent = [xmin, xmax, ymin, ymax],
                           cmap = 'bwr', title = '', titletype = 'simple',
                           colorbar = False, logarithmic=False, verbose=verbose,
                           xlim = xlim, ylim = ylim,
                           xlabel = '$x/\lambda$', ylabel = ylabel, yticks = yticks, xticks = xticks,
                           maximum = 'local', fontsize = fontsize,
                           contour=True, clevels = clevels, clinewidth = lw)
        for i__ in ax.spines.itervalues():
            i__.set_linewidth(lw)                     
            
        ax = utils.subplot(fig, iterations[i], path+utils.nezpath,
                           shape = (nx,ny), position = (1,1,1),
                           extent = [xmin, xmax, ymin, ymax],
                           cmap = 'YlGn', title = '', titletype = 'simple',
                           colorbar = True, cbartitle = '$n_e/n_c$', ncbarticks = 2, logarithmic=False, verbose=verbose,
                           xlim = xlim, ylim = ylim, yticks = yticks, xticks = xticks,
                           xlabel = '$x/\lambda$', ylabel = ylabel, vmax = 0.5,
                           maximum = 'local', fontsize = fontsize, mult = mult/ncr)
        ax.text(-0.63, 0.4, '$(%s)$'%(labels[i]), fontsize = 18)
        for i__ in ax.spines.itervalues():
            i__.set_linewidth(lw) 
                     
                    
        picname = picspath + '/' + "fig2%s.png"%(labels[i])
        # plt.tight_layout()
        plt.savefig(picname, bbox_inches='tight', dpi=256)
        plt.close()

    
if __name__ == '__main__':
    main()
