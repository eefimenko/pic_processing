#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def find_max(file, mult = 1.):
    max = 0
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]
    for p in array:
        if p > max:
            max = p
    return max

def find_max_from_all_fields(expath, eypath, ezpath, bxpath, bypath, bzpath, i):
    max = 0
    name = expath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = eypath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = ezpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    return max

def find_max_from_all_particles(xpath, ypath, zpath, i, mult = 1.):
    max = 0
    name = xpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    name = ypath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    name = zpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    return max
    
    

def create_subplot(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, ftype = 'txt', mult=1, log = 1):
    ax = fig.add_subplot(spx,spy,spi)
    name = path + '/' + "%06d.%s" % (i,ftype)
    field = utils.fromfile(name,nx,ny,ftype,mult)
    
    maxe = max([max(row) for row in field])
    mine = min([min(row) for row in field])
   
    ratio = 1e-3
    if maxe == mine:
        v_min_ = maxe
        v_max_ = 1e9*maxe
    else:
        v_max_ = maxe
        v_min_ = maxe*ratio
    if log == 1:
        surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = v_min_, vmax = v_max_, norm=clr.LogNorm())
    else:
        surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = v_min_, vmax = v_max_)
    ax.text(0.3, 1.1, 'min=%.1le max=%.1le' % (v_min, maxe), style='italic', horizontalalignment='center', verticalalignment='center', transform = ax.transAxes)
    ax.set_xlim([-0.03, 0.03])
    ax.set_ylim([-0.03, 0.03])
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
    plt.colorbar(surf,  orientation  = 'vertical', ticks = [v_min, v_max])
    return surf

def create_subplot_f(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
    name = path + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny, mult)
    maxe = max([max(row) for row in field])
    mine = min([min(row) for row in field])
    v_max_ = max2d(field)
    ratio = 1e-3
    if maxe == mine:
        v_min_ = maxe
        v_max_ = 1e9*maxe
    else:
        v_max_ = maxe
        v_min_ = maxe*ratio
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Reds', vmin = v_min_, vmax = v_max_, norm=clr.LogNorm())
    ax.text(-5, 4.5, '%.1le' % maxe, style='italic')
    plt.colorbar(surf, orientation  = 'vertical', ticks = [v_min, v_max])
    return surf

def pulse_value(i,step,amp,tp,delay):
    ans = []
    tau = tp/1.1437
#    delay = 23.1099984185576458468467817
    t = i*step-delay
    if t > 0 and t < math.pi * tau:
        tmp = math.sin(t/tau)
        f = amp*tmp*tmp
    else:
        f = 0.
    return f

def main():
    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nixpath = 'data/Proton2Dx'
    niypath = 'data/Proton2Dy'
    nizpath = 'data/Proton2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nphxpath = 'data/Photon2Dx'
    nphypath = 'data/Photon2Dy'
    nphzpath = 'data/Photon2Dz'
    picspath = 'pics'

    config = utils.get_config("ParsedInput.txt")
   
    BOIterationPass = float(config['BOIterationPass'])
    ftype = str(config['BODataFormat'])
    print ftype
    dt = float(config['TimeStep'])*1e15*BOIterationPass #fs
#    duration = float(config['Duration'])*1e15 #fs
#    delay = float(config['delay'])*1e15 #fs

    print "dt = " + str(dt) + 'fs'
#    print "duration = " + str(duration)
#    print "delay = " + str(delay)
    
    Xmax = float(config['X_Max'])*1e4 #mkm
    Xmin = float(config['X_Min'])*1e4 #mkm
    Ymax = float(config['Y_Max'])*1e4 #mkm
    Ymin = float(config['Y_Min'])*1e4 #mkm
    Zmax = float(config['Z_Max'])*1e4 #mkm
    Zmin = float(config['Z_Min'])*1e4 #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz

    step = (Xmax-Xmin)/nx

    mult = 1/(2.*dx*dy*dz*1e-12)

    delta = 1

    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(expath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(expath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax) + ' files'

    maxf = 0
    maxe = 0
    maxi = 0
    maxp = 0
    maxph = 0
    print 'Reading files'
    fmin = 0
    fmax = 0
    cmin = 0
    cmax = 0
    cimin = 0
    cimax = 0
    phmin = 0
    phmax = 0
    print 'Type', ftype
    for i in range(nmin,nmax,delta):
        print "\rSaving s%06d.png, %.0f %% done" % (i,float(i-nmin)/(nmax-nmin)*100),
        sys.stdout.flush()

        fig = plt.figure(num=None, figsize=(20, 10), dpi=256)
        mp.rcParams.update({'font.size': 8})
        
        s = create_subplot(fig,i,expath,nx,ny,3,6,1, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'z', 'y', 'Reds', ftype = ftype, log = 0)
        s = create_subplot(fig,i,eypath,nx,ny,3,6,7, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'z', 'x', 'Reds', ftype = ftype,  log = 0)
        s = create_subplot(fig,i,ezpath,nx,ny,3,6,13, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'y', 'x', 'Reds', ftype = ftype, log = 0)
        
        s = create_subplot(fig,i,bxpath,nx,ny,3,6,2, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'z', 'y','Reds', ftype = ftype, log = 0)
        s = create_subplot(fig,i,bypath,nx,ny,3,6,8, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'z', 'x', 'Reds', ftype = ftype, log = 0)
        s = create_subplot(fig,i,bzpath,nx,ny,3,6,14, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'y', 'x', 'Reds', ftype = ftype, log = 0)
        
        s = create_subplot(fig,i,nexpath,nx,ny,3,6,3, Xmin, Xmax, Ymin, Ymax, cmin, cmax, 'z', 'y','Greens', mult=mult, ftype = ftype)
        s = create_subplot(fig,i,neypath,nx,ny,3,6,9, Xmin, Xmax, Ymin, Ymax, cmin, cmax, 'z', 'x', 'Greens', mult=mult, ftype = ftype)
        s = create_subplot(fig,i,nezpath,nx,ny,3,6,15, Xmin, Xmax, Ymin, Ymax, cmin, cmax, 'y', 'x', 'Greens', mult=mult, ftype = ftype)
        
        s = create_subplot(fig,i,nixpath,nx,ny,3,6,4, Xmin, Xmax, Ymin, Ymax, cimin, cimax, 'z', 'y','Greens', mult=mult, ftype = ftype)
        s = create_subplot(fig,i,niypath,nx,ny,3,6,10, Xmin, Xmax, Ymin, Ymax, cimin, cimax, 'z', 'x', 'Greens', mult=mult, ftype = ftype)
        s = create_subplot(fig,i,nizpath,nx,ny,3,6,16, Xmin, Xmax, Ymin, Ymax, cimin, cimax, 'y', 'x', 'Greens', mult=mult, ftype = ftype)
        
#        s = create_subplot(fig,i,npxpath,nx,ny,3,6,5, Xmin, Xmax, Ymin, Ymax, cmin, cmax, 'z', 'y','Greens', mult=mult, ftype = ftype)
#        s = create_subplot(fig,i,npypath,nx,ny,3,6,11, Xmin, Xmax, Ymin, Ymax, cmin, cmax, 'z', 'x', 'Greens', mult=mult, ftype = ftype)
#        s = create_subplot(fig,i,npzpath,nx,ny,3,6,17, Xmin, Xmax, Ymin, Ymax, cmin, cmax, 'y', 'x', 'Greens', mult=mult, ftype = ftype)
            
        s = create_subplot(fig,i,nphxpath,nx,ny,3,6,6, Xmin, Xmax, Ymin, Ymax, phmin, phmax, 'z', 'y', 'Blues', mult=mult, ftype = ftype)
        s = create_subplot(fig,i,nphypath,nx,ny,3,6,12, Xmin, Xmax, Ymin, Ymax, phmin, phmax, 'z', 'x', 'Blues', mult=mult, ftype = ftype)
        s = create_subplot(fig,i,nphzpath,nx,ny,3,6,18, Xmin, Xmax, Ymin, Ymax, phmin, phmax, 'y', 'x', 'Blues', mult=mult, ftype = ftype)
        
#        f = pulse_value(i, dt, 1., duration, delay)
#        power = f*f*maxpower
        
#        plt.figtext(0.2,0.93,str('Power in linear centre =%.3e PW' % (power)) , fontsize = 10)
        picname = picspath + '/' + "s%06d.png" % (i,)
        
        plt.savefig(picname)
        plt.close()

if __name__ == '__main__':
    main()
