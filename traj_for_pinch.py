#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.cm as cm
import matplotlib.colors as colors
import matplotlib as mp
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import math
import utils
import sys

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def read_traj(file):
    f = open(file, 'r')
    x_ = []
    y_ = []
    z_ = []
    e_ = []
    r_ = []
    mx_ = []
    my_ = []
    mz_ = []
    efield = []
    bfield = []
    ev = 1.6e-12
    c = 2.99792e+10
    for line in f:
        tmp = line.split()
        num_ = len(tmp)
        x = float(tmp[1])
        y = float(tmp[2])
        z = float(tmp[3])
        px = float(tmp[4])
        py = float(tmp[5])
        pz = float(tmp[6])
        if num_ == 13:
            ex_ = float(tmp[7])
            ey_ = float(tmp[8])
            ez_ = float(tmp[9])
            bx_ = float(tmp[10])
            by_ = float(tmp[11])
            bz_ = float(tmp[12])
            efield.append(math.sqrt(ex_*ex_ + ey_*ey_ + ez_*ez_))
            bfield.append(math.sqrt(bx_*bx_ + by_*by_ + bz_*bz_))
        r = math.sqrt(x*x + y*y)
        pr = math.sqrt(px*px + py*py)
        e = math.sqrt(px*px + py*py + pz*pz)*c/ev/0.511e6
        mx = y*pz - py*z
        my = -(x*pz - px*z)
        mz = x*py - px*y
        x_.append(x)
        y_.append(y)
        z_.append(z)
        mx_.append(mx)
        my_.append(my)
        mz_.append(mz)
        r_.append(r)
        e_.append(e)
    return np.array(x_), np.array(y_), np.array(z_), np.array(r_), np.array(e_), np.array(mx_), np.array(my_), np.array(mz_), np.array(efield), np.array(bfield)

def normalize(a, n = 1.):
    if n != 0:
        for i in range(len(a)):
            a[i] /= n
    return a
  
def main():
    expath = 'data/E2x'
    partpath = 'ParticleTracking/Electrons'
#    partpath = 'ParticleTracking' 
    picspath = 'pics'
#    numbers = [1412, 9347, 3820,  9359, 1225, 745, 1130, 1432, 1503, 1507, 2047, 2476, 3755, 3820, 4377, 6589, 8291, 8335, 8798, 8831, 9269, 9672, 9728, 9788]
    numbers = [334, 5764, 2015, 5143] # 19pw
    num = 2#len(numbers)

    config = utils.get_config("ParsedInput.txt")
    wl = float(config['Wavelength'])
    k = 2.*math.pi/wl
    BOIterationPass = float(config['BOIterationPass'])
    dt = float(config['TimeStep'])*1e15*BOIterationPass #fs
    maxpower = float(config['PeakPower'])*1e-7*1e-15 #PW
    peakpower = float(config['PeakPower'])*1e-7*1e-15
    const_a_p = 7.81441e-9
    a0 = const_a_p * math.sqrt(peakpower*1e22)
    print a0
    print "Power = " + str(maxpower) + 'PW'
    print "dt = " + str(dt) + 'fs'
#    print "duration = " + str(duration)
#    print "delay = " + str(delay)
    
    Xmax = float(config['X_Max']) #mkm
    Xmin = float(config['X_Min']) #mkm
    Ymax = float(config['Y_Max']) #mkm
    Ymin = float(config['Y_Min']) #mkm
    Zmax = float(config['Z_Max']) #mkm
    Zmin = float(config['Z_Min']) #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz
    wl = float(config['Wavelength']) 
    step = (Xmax-Xmin)/nx
    x_ = [Xmin + i_*dx for i_ in range(nx)]
    axis_ = [x__/wl for x__ in x_]
    Esw = np.array([math.sqrt(utils.exf(x__,0,0,k)**2 + utils.eyf(x__,0,0,k)**2 + utils.ezf(x__,0,0,k)**2) for x__ in x_])
    Bsw = np.array([math.sqrt(utils.bxf(x__,0,0,k)**2 + utils.byf(x__,0,0,k)**2) for x__ in x_])
    mult = 1/(2.*dx*dy*dz)

    delta = 1
    jet = plt.get_cmap('jet') 
    cNorm  = colors.Normalize(vmin=0.5, vmax=a0*1e-3*0.5)
    scalarMap = cm.ScalarMappable(norm=cNorm, cmap=jet)
    print scalarMap.get_clim()
   
    
    n = 0
    ii = 0
    zout = 0.4
    start = 0
    end=500
    bound=0.1
    boundz=0.25
    xmax = 0.3
    zmax = 0.4
    fig = plt.figure(num=None, figsize=(5, 5))
    ax = fig.add_subplot(1,1,1)
    fig1 = plt.figure(num=None, figsize=(10, 5))
    ax1 = fig1.add_subplot(1,1,1)
#    ay = fig.add_subplot(2,2,2)
#    axy = fig.add_subplot(2,2,3)
#    az = fig.add_subplot(2,2,4)
    Esw /= 5.*np.amax(Esw)
    Bsw /= 10.*np.amax(Bsw)
    ax.plot(axis_, Esw, 'k')
    ax.plot(axis_, Bsw, 'r')
    print Esw
    for i in range(0,num):
        print i
       
        i_ = numbers[i]
        x,y,z,r,e,mx,my,mz,efield,bfield = read_traj('./' + partpath + '/%d.txt' % (i_))
        x /= wl
        y /= wl
        z /= wl
        tr = 1
#        if len(r) < 1000:
#            tr = 0
#            continue
#        for i in range(len(r)):
#            if abs(r[i]) > 2.:
#                tr = 0
#                break
#        if tr == 0:
#            continue
#        n += 1
        if len(e) == 0:
            continue
#        if e[0] < 0.5:
#            continue
#        e = normalize(e, a0*1e-3)
#        print e[0]
       
#        for j_ in range(len(z[start:end])):
#            if abs(z[j_]) > zout:
#                tr = 0
#                break
        if tr == 0:
            continue
        ii += 1
      
#        az = fig.add_subplot(2,2,4,projection='3d')     
        cmap = plt.cm.get_cmap('plasma')
        ax.plot(x[start:end], z[start:end])
        ax.plot(x[start], z[start], marker='o', color='b')
        
#        ax.plot(x[start+150], z[start+150], marker='o', color='g')
#        ax.plot(x[start+300], z[start+300], marker='o', color='g')
#        ax.plot(x[start+450], z[start+450], marker='o', color='g')
#        ax.plot(x[start+600], z[start+600], marker='o', color='g')
        ax.set_xlim([-xmax,xmax])
        ax.set_ylim([-zmax,zmax])
        ax.axvline(x=0, color='gray')
        ax.axhline(y=0, color='gray')
#        ax.axvline(x=dx/wl, color='g')
#        ax.axvline(x=-dx/wl, color='g')
        ax.set_xlabel('$x/\lambda$')
        ax.set_ylabel('$z/\lambda$')
        if len(bfield) > 0:
          
            ax1.plot(x[start:end], bfield[start:end])
            ax1.plot(x[start], bfield[start], marker='o', color='b')
            ax1.set_xlim([-xmax,xmax])
            ax1.set_xlabel('$x/\lambda$')
            ax1.set_ylabel('$B$')
#        ax.set_xlim([-bound, bound])
#        ax.set_ylim([-boundz, boundz])
#        ay.plot(y[start:end], z[start:end])
#        ay.plot(y[start], z[start], marker='o')
#        ay.plot(y[start+150], z[start+150], marker='o', color='g')
#        ay.plot(y[start+300], z[start+300], marker='o', color='g')
#        ay.plot(y[start+450], z[start+450], marker='o', color='g')
#        ay.plot(y[start+600], z[start+600], marker='o', color='g')
#        ay.set_xlim([-xmax,xmax])
#        ay.set_ylim([-zmax,zmax])
#        ay.axvline(x=0, color='k')
#        ay.axvline(x=dx/wl, color='g')
#        ay.axvline(x=-dx/wl, color='g')
#        ay.set_xlabel('$y/\lambda$')
#        ay.set_ylabel('$z/\lambda$')
#        axy.plot(x[start:end], y[start:end])
#        axy.plot(x[start], y[start], marker='o')
#        axy.plot(x[start+150], y[start+150], marker='o', color='g')
#        axy.plot(x[start+300], y[start+300], marker='o', color='g')
#        axy.plot(x[start+450], y[start+450], marker='o', color='g')
#        axy.plot(x[start+600], y[start+600], marker='o', color='g')
#        axy.set_xlim([-xmax,xmax])
#        axy.set_ylim([-xmax,xmax])
#        axy.axhline(y=0, color='k')
#        axy.axvline(x=0, color='k')
#        axy.axhline(y=dx/wl, color='g')
#        axy.axvline(x=dx/wl, color='g')
#        axy.axhline(y=-dx/wl, color='g')
#        axy.axvline(x=-dx/wl, color='g')
#        axy.set_xlabel('$x/\lambda$')
#        axy.set_ylabel('$y/\lambda$')
#        az.plot(mx[start:end], label = 'mx')
#        az.plot(my[start:end], label = 'my')
#        az.plot(mz[start:end], label = 'mz')
#        az.legend()
#        ay.set_xlim([-bound, bound])
#        ay.set_ylim([-boundz, boundz])
        
#        sc = az.plot(x[start:end], y[start:end], z[start:end], c=np.abs(e[start:end]), cmap=cmap, edgecolor='none', s=5)
#        plt.colorbar(sc)
#        az.plot(x[start], y[start], z[start], marker='o')
#        az.set_xlim([-xmax,xmax])
#        az.set_ylim([-xmax,xmax])
#        az.set_zlim([-zmax,zmax])
#        az.set_xlabel('$x/\lambda$')
#        az.set_ylabel('$y/\lambda$')
#        az.set_zlabel('$z/\lambda$')

#        az.set_xlim([0, bound])
#        az.set_ylim([-boundz, boundz])

        
        print float(n)/num
    picname = picspath + '/' + "trajectories_for_pinch.png"
    print picname
    fig.tight_layout()
    fig.savefig(picname, dpi=256)
#    fig.close()
    
    if len(bfield) > 0:
        picname = picspath + '/' + "trajectories_for_pinch_b.png"
        print picname
        fig1.tight_layout()
        fig1.savefig(picname, dpi=256)
#        fig1.close()
if __name__ == '__main__':
    main()
