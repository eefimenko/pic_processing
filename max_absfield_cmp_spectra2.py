#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys
import os

def read_field(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)/norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1e-9)
        field.append(row)
    return field

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])

def calc_current(array,nx,ny,dx,dy,r):
    num = 0.
    for i in range(nx):
        x = (i - nx/2)*dx
        for j in range(ny):
            y = (j-ny/2)*dy
            if x*x + y*y < r*r:
                num += array[i][j]
    return num

def main():
    picspath = 'pics'

    n = len(sys.argv)
    fig = plt.figure()
#    ezx = fig.add_subplot(3,1,1)
#    ezx.set_title('Electric field')
    bzx = fig.add_subplot(3,1,2)
    bzx.set_title('Magnetic field')
    nex = fig.add_subplot(3,1,1)
    nex.set_title('Ne')
    powx = fig.add_subplot(3,1,3)
    powx.set_title('Power')
#    maxx = fig.add_subplot(5,1,5)
#    maxx.set_title('Max energy')
      
    electronMass = 9.10938e-28
    electronCharge = 4.8032e-10
    const_a_p = 7.81441e-9
    filename = 'ez_b_ne_ncr_j'
    power_p = []
    ezx_p = []
    bzx_p = []
    nex_p = []
    ncrx_p = []
    jx_min_p = []
    jx_max_p = []
    powx_p = []
    pow1x_p = []
    maxx_p = []
    avx_p = []
      
    for i in range(n-1):
        path = sys.argv[i+1] + '/'
        config = utils.get_config(path + "/ParsedInput.txt")
        dx = float(config['Step_X'])
        dy = float(config['Step_Y'])
        dz = float(config['Step_Z'])
        nx = int(config['MatrixSize_X'])
        ny = int(config['MatrixSize_Y'])
        BOIterationPass = int(config['BOIterationPass'])
        TimeStep = float(config['TimeStep'])
        dt = BOIterationPass*TimeStep*1e15
        step = float(config['TimeStep'])*float(config['BOIterationPass'])
        omega = float(config['Omega'])
       
        T = 2*math.pi/omega
        nper = int(T/step)/2
        dv = 2*dx*dy*dz
        coeffj = 2. * 1.6e-19 * 3e8 /(2*dz*1e-2)*1e-6
        wl = float(config['Wavelength'])
        print 'J = ', coeffj
        ppw = float(config['PeakPowerPW'])
        ncr = electronMass * omega * omega/(8. * math.pi * electronCharge * electronCharge)
        a0 = const_a_p * math.sqrt(ppw*1e22)
        ax_ = []
        ax1_ = []
        ez_t = []
        pow_t = []
        pow1_t = []
        max_t = []
        av_t = []
        br_full_t = []
        br_100mev_t = []
        br_1gev_t = []
        br_full1_t = []
        br_100mev1_t = []
        br_1gev1_t = []
        S = 2 * math.pi * 0.1 * 0.1 * wl * wl * 100 # square of source mm^2, r = 0.1 lambda 
        ds = 1.5e4 # mrad^2
        f = open(path + 'br_t.dat', 'r')
        i = 0
        T1 = 2*math.pi/omega*1e15
        for line in f:
            tmp = line.split()
            ax_.append(float(tmp[0])/T1)
            ez_t.append(float(tmp[1])/1.18997e+8/a0)

            pow_t.append(float(tmp[2]))
            pow1_t.append(float(tmp[2])/ppw)
            max_t.append(float(tmp[3]))
            av_t.append(float(tmp[4]))
            ax1_.append(float(tmp[0])/T1 - 2.)
            br_full_t.append(float(tmp[5]))
            br_100mev_t.append(float(tmp[6]))
            br_1gev_t.append(float(tmp[7])/1e23)
            br_full1_t.append(float(tmp[5])/S/ds)
            br_100mev1_t.append(float(tmp[6])/S/ds)
            br_1gev1_t.append(float(tmp[7])/S/ds)
        #npow = len(pow_t)
        num_ = 30
        pow_av = utils.avg(pow_t, num_)
        pow1_av = utils.avg(pow1_t, num_)
        max_av = utils.avg(max_t, num_)
        av_av = utils.avg(av_t, num_)
      
        print a0
        ncr *= a0
        
        nmin,nmax = utils.find_min_max_from_directory(path + utils.ezpath, path + utils.bzpath, path + utils.nezpath)
#        num = nmax - nmin
        ez_t = np.zeros(nmax)
        bz_t = np.zeros(nmax)
        ne_t = np.zeros(nmax)
        ncr_t = np.zeros(nmax)
        j_t = np.zeros(nmax)
        
        print 'min =', nmin, ' max =', nmax
        axis_t = utils.create_axis(nmax, dt/T1)
        
        nmin = utils.read_tseries(path,filename, ez_t, bz_t, ne_t, ncr_t, j_t)
        ntr_t = utils.ts(path, utils.netpath, name='ne_trapped',
                         tstype='sum', verbose=True)
        axisn_t = utils.create_axis(len(ntr_t), dt/T1)
        for i in range(nmin, nmax):
            print i
            fieldez_x = utils.bo_file_load(path + utils.ezpath,i,nx)
            ez_t[i] = np.amax(fieldez_x)
            fieldbz_x = utils.bo_file_load(path + utils.bzpath,i,nx)
            bz_t[i] = np.amax(fieldbz_x)

            ne_x = utils.bo_file_load(path + utils.nezpath,i,nx,ny)
            curr = calc_current(ne_x,nx,ny,dx,dy,1)
            ne_t[i] = np.amax(ne_x)/dv
            ncr_t[i] = ne_t[i]/ncr
            j_t[i] = curr
        utils.save_tseries(path,filename, nmin, nmax, ez_t, bz_t, ne_t, ncr_t, j_t)
        ez_e = utils.env(ez_t, axis_t)
        bz_e = utils.env(bz_t, axis_t)
        ne_e = utils.avg(ne_t, 30)
        tbounds = [0, 65]    
#        ezx.plot(axis_t, ez_e, label = path)
        bzx.plot(axis_t, bz_e, label = ppw)
        nex.plot(axis_t, ne_e, label = ppw)
#        maxx.plot(ax1_, max_av, label = path)
        powx.plot(ax1_, pow_av, label = ppw)
#        pow1x = powx.twinx()
#        pow1x.plot(ax1_, pow1_av, label = path)
#        ezx.set_xlim(tbounds)
        bzx.set_xlim(tbounds)
        nex.set_xlim(tbounds)
        powx.set_xlim(tbounds)
#        maxx.set_xlim(tbounds)
        
      
    plt.legend(loc = 'lower left')
#    plt.savefig(picspath + '/' + "ez_t.png")
    plt.tight_layout()
    plt.show()
    plt.close()
  
    f.close()
      
if __name__ == '__main__':
    main()
