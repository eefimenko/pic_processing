#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib as mp
mp.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import math
import utils
import sys
import numpy as np
import os
import copy

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            row.append(array[index]+ 1e-12)
        field.append(row)
    return field

def find_max(file, mult = 1.):
    max = 0
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]
    for p in array:
        if p > max:
            max = p
    return max

def find_max_from_all_fields(expath, eypath, ezpath, bxpath, bypath, bzpath, i):
    max = 0
    name = expath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = eypath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    name = ezpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name)
    if tmp > max:
        max = tmp
    return max

def find_max_from_all_particles(xpath, ypath, zpath, i, mult = 1.):
    max = 0
    name = xpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    name = ypath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    name = zpath + '/' + "%06d.txt" % (i,)
    tmp = find_max(name, mult)
    if tmp > max:
        max = tmp
    return max
    
def max2d(array):
    return max([max(x) for x in array])    

def create_subplot(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, text, text1, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
#    name = path + '/' + "%06d.txt" % (i,)
    read, field = utils.bo_file_load(path,i,nx,ny, verbose=1, transpose=1)
    if read == False:
        return
#    maxe = max([max(row) for row in field])
#    mine = min([min(row) for row in field])
    maxe = np.amax(field)
    mine = np.amin(field)
    print path, maxe, mine
    ratio = 1e-3
    if maxe == 0:
        maxe = 1
    if maxe == mine:
        v_min_ = maxe
        v_max_ = 1e9*maxe
    else:
        v_max_ = maxe
        v_min_ = maxe*ratio

#    ticks = [-1,-1,0,1,2]
    my_cmap = copy.copy(mp.cm.get_cmap(color)) # copy the default cmap
    my_cmap.set_bad('w')
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = my_cmap, vmin = v_min_, vmax = v_max_/1.5)#, norm=clr.LogNorm())
#    ax.text(-2.5, 2.5, text)
    ax.set_title(text) 
#    ax.text(1.6, 1.6, text1, fontsize=30)
    ax.set_xlim([-1.,1.])
    ax.set_ylim([-1.,1.])
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
#    plt.xticks(ticks)
#    plt.yticks(ticks)
#    plt.colorbar(surf,  orientation  = 'vertical', ticks = [v_min, v_max])
    return surf
def create_subplot_f(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max,  xl, yl, color, text, text1, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
#    name = path + '/' + "%06d.txt" % (i,)
    read, field = utils.bo_file_load(path,i,nx,ny, verbose=1, transpose=1)
    if read == False:
        return
#    maxe = max([max(row) for row in field])
#    v_max_ = max2d(field)
#    print v_max_
    ratio = 1e-3
#    ticks = [-2,-1,0,1,2]
#    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, vmin = v_max_*ratio, vmax = v_max_)
    maxe = np.amax(field)
    mine = np.amin(field)
    print path, maxe, mine, np.amax(field[nx/4:3*nx/4, ny/4:3*ny/4])
    ratio = 1e-6
    if maxe == 0:
        maxe = 1
    if maxe == mine:
        v_min_ = maxe
        v_max_ = 1e9*maxe
    else:
        v_max_ = maxe
        v_min_ = maxe*ratio
    my_cmap = copy.copy(mp.cm.get_cmap(color)) # copy the default cmap
    my_cmap.set_bad('w')
    if 'E' in path: 
        vmax__ = 3e11*math.sqrt(2.5)*0.653
    else:
        vmax__ = 3e11*math.sqrt(2.5)
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = my_cmap, vmin = 0, vmax = vmax__)
    ax.set_title(text)
#    ax.text(1.6, 1.6, text1, fontsize=30)
    ax.set_xlim([-1.,1.])
    ax.set_ylim([-1.,1.])
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
#    plt.xticks(ticks)
#    plt.yticks(ticks)
#    plt.colorbar(surf,  orientation  = 'vertical', ticks = [v_min, v_max])
    return surf


def pulse_value(i,step,amp,tp,delay):
    ans = []
    tau = tp/1.1437
#    delay = 23.1099984185576458468467817
    t = i*step-delay
    if t > 0 and t < math.pi * tau:
        tmp = math.sin(t/tau)
        f = amp*tmp*tmp
    else:
        f = 0.
    return f

def main():
    picspath = 'gif2'

    config = utils.get_config("ParsedInput.txt")

    BOIterationPass = float(config['BOIterationPass'])
    dt = float(config['TimeStep'])*1e15*BOIterationPass #fs
    
    ppw = int(config['PeakPower'])*1e-22 #PW
    omega = float(config['Omega'])
    dt = float(config['TimeStep'])*BOIterationPass
    T = 2 * math.pi/omega
    dtt = dt/T
    print "Power = " + str(ppw) + 'PW'
    print "dt = " + str(dt) + 'fs'
    wl = float(config['Wavelength'])
    
    Xmax = float(config['X_Max'])/wl #mkm
    Xmin = float(config['X_Min'])/wl #mkm
    Ymax = float(config['Y_Max'])/wl #mkm
    Ymin = float(config['Y_Min'])/wl #mkm
    Zmax = float(config['Z_Max'])/wl #mkm
    Zmin = float(config['Z_Min'])/wl #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz

    step = (Xmax-Xmin)/nx
    const_a_p = 7.81441e-9
    
    a0 = const_a_p * math.sqrt(ppw*1e22)
    a00 = 3.e11 * math.sqrt(ppw/10.)
    print a0, a00
    mult1 = 1/(2.*dx*dy*dz*1e-12)

    nmin, nmax, delta = utils.get_min_max_iteration(sys.argv,
                                                    utils.eypath, utils.bypath, utils.neypath, utils.nphypath,
                                                    utils.ezpath, utils.bzpath, utils.nezpath, utils.nphzpath)
        
    print 'Plot from ' + str(nmin) + ' to ' + str(nmax) + ' pics'

    maxf = 0
    maxe = 0
    maxi = 0
    maxp = 0
    maxph = 0
    print 'Reading files'

    for i in range(0,0):
        print "\r%.0f %% done" % (float(i)/nmax*100),
        sys.stdout.flush()        
        tmpf = find_max_from_all_fields(eypath, eypath, ezpath, bxpath, bypath, bzpath, i)
        if tmpf > maxf:
            maxf = tmpf
        tmpe = find_max_from_all_particles(neypath, neypath, nezpath, i, mult)
        if tmpe > maxe:
            maxe = tmpe
        tmpi = find_max_from_all_particles(niypath, niypath, nizpath, i, mult)
        if tmpi > maxi:
            maxi = tmpi
        tmpp = find_max_from_all_particles(npypath, npypath, npzpath, i, mult)
        if tmpp > maxp:
            maxp = tmpp
        tmpph = find_max_from_all_particles(nphypath, nphypath, nphzpath, i, mult)
        if tmpph > maxph:
            maxph = tmpph
#    print maxf, maxe, maxi, maxp, maxph
    if maxi > maxe:
        maxe = maxi

    if maxp > maxe:
        maxe = maxp
    maxph += 1
#    maxf = 0
    fmin = maxf*1e-3
    fmax = maxf
#    cmin = maxe*1e-5
    cmax = maxe
    cmin = maxe*1e-3
    cimax = maxi
    cimin = maxi*1e-3
#    cmax = 4.5e25
    phmin = maxph*1e-3
    phmax = maxph
    nmin=360
    nmax=450
    print 'Here', nmin, nmax, delta
    
    if not os.path.exists(picspath):
        print picspath, ' does not exist, creating'
        os.makedirs(picspath)
    
    for i in range(nmin,nmax,delta):
        picname = picspath + '/' + "ufn%06d.png" % (i,)
        if os.path.exists(picname):
            continue
        print "\rSaving %s, %.0f %% done" % (picname,float(i-nmin)/(nmax-nmin)*100),
        sys.stdout.flush()
        t = i * dtt
        
        fig = plt.figure(num=None, figsize=(15, 7.5))
        mp.rcParams.update({'font.size': 12})

        s = create_subplot_f(fig,i,utils.eypath,nx,ny,2,4,1, Xmin, Xmax, Ymin, Ymax, fmin, fmax, '', '$z/\lambda$', 'Reds', u'Electric field', 'a', mult = 1./a00)
#        s = create_subplot(fig,i,eypath,nx,ny,3,6,7, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'z', 'x', 'Reds')
        s = create_subplot_f(fig,i,utils.ezpath,nx,ny,2,4,5, Xmin, Xmax, Ymin, Ymax, fmin, fmax, '$y/\lambda$', '$x/\lambda$', 'Reds', '', 'e', mult = 1./a00)
        
        s = create_subplot_f(fig,i,utils.bypath,nx,ny,2,4,2, Xmin, Xmax, Ymin, Ymax, fmin, fmax, '', '','Blues', u'Magnetic field', 'b', mult = 1./a00)
#        s = create_subplot(fig,i,bypath,nx,ny,3,6,8, Xmin, Xmax, Ymin, Ymax, fmin, fmax, 'z', 'x', 'Reds')
        s = create_subplot_f(fig,i,utils.bzpath,nx,ny,2,4,6, Xmin, Xmax, Ymin, Ymax, fmin, fmax, '$y/\lambda$', '', 'Blues', '', 'f', mult = 1./a00)
        
        s = create_subplot(fig,i,utils.neypath,nx,ny,2,4,3, Xmin, Xmax, Ymin, Ymax, cmin, cmax, '', '','Greens', u'Electrons', 'c', mult1)
#        s = create_subplot(fig,i,neypath,nx,ny,3,6,9, Xmin, Xmax, Ymin, Ymax, cmin, cmax, 'z', 'x', 'Greens', mult)
        s = create_subplot(fig,i,utils.nezpath,nx,ny,2,4,7, Xmin, Xmax, Ymin, Ymax, cmin, cmax, '$y/\lambda$', '', 'Greens', '', 'g', mult1)
        
#        s = create_subplot(fig,i,npypath,nx,ny,2,4,4, Xmin, Xmax, Ymin, Ymax, cimin, cimax, '$y/\lambda$', 'y','Greens', 'Positron distribution', mult1)
#       s = create_subplot(fig,i,niypath,nx,ny,3,6,10, Xmin, Xmax, Ymin, Ymax, cimin, cimax, 'z', 'x', 'Greens', mult)
#        s = create_subplot(fig,i,npzpath,nx,ny,2,4,8, Xmin, Xmax, Ymin, Ymax, cimin, cimax, '$y/\lambda$', '', 'Greens', '', mult1)
        
#        s = create_subplot(fig,i,npxpath,nx,ny,3,6,5, Xmin, Xmax, Ymin, Ymax, cmin, cmax, 'z', 'y','Greens', mult)
#        s = create_subplot(fig,i,npypath,nx,ny,3,6,11, Xmin, Xmax, Ymin, Ymax, cmin, cmax, 'z', 'x', 'Greens', mult)
#        s = create_subplot(fig,i,npzpath,nx,ny,3,6,17, Xmin, Xmax, Ymin, Ymax, cmin, cmax, 'y', 'x', 'Greens', mult)
            
        s = create_subplot(fig,i,utils.nphypath,nx,ny,2,4,4, Xmin, Xmax, Ymin, Ymax, phmin, phmax, '', '', 'Purples', u'Photons', 'd', mult1)
#        s = create_subplot(fig,i,nphypath,nx,ny,3,6,12, Xmin, Xmax, Ymin, Ymax, phmin, phmax, 'z', 'x', 'Blues', mult)
        s = create_subplot(fig,i,utils.nphzpath,nx,ny,2,4,8, Xmin, Xmax, Ymin, Ymax, phmin, phmax, '$y/\lambda$', '', 'Purples', '', 'h', mult1)
        
      
        plt.tight_layout()
        print picname
        plt.savefig(picname, dpi=64)
        plt.close()

if __name__ == '__main__':
    main()
