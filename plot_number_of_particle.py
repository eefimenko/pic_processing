#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import os
import numpy as np

def main():
    old = [9999848, 2499332, 2499343, 2499446, 2500162, 2504588, 2529777, 2597681, 2691540, 2811537, 2927187, 3014821, 3078124]
    number = [9999848, 3936487, 3936487, 3936487, 3936500, 3937332, 3947705, 2749201, 1597928, 1746550, 1882294, 1998268, 2085057]
    energy = [9999848, 3936487, 3936487, 3936487, 3936489, 3936512, 3936604, 3936927]
    level = []

    old_p = [0, 508586, 1505787, 2826343, 3009304, 2510696, 2299487, 2510724, 1947676, 3302180, 2285871, 2100384, 2191206]
    number_p = [0, 431665, 1156703, 2266159, 2113664, 1266850, 2808742, 2126310, 2483196, 2379011, 2224767, 3306962, 1269297]
    energy_p = [0, 243291, 610881, 1013403, 1461374, 1926222, 2392074, 2870860]
    fig = plt.figure()
    ax1 = fig.add_subplot(2,1,1)
    ax2 = fig.add_subplot(2,1,2)
    ax1.set_xlabel('x100 iterations')
    ax2.set_xlabel('x100 iterations')
    ax1.set_ylabel('# of electrons (macro) x10^6')
    ax2.set_ylabel('# of photons (macro) x10^6')
    p_old, = ax1.plot(np.array(old)*1e-6, 'r', label = 'old')
    p_number, = ax1.plot(np.array(number)*1e-6,'g', label = 'number')
    p_energy, = ax1.plot(np.array(energy)*1e-6, 'b', label = 'energy')
    p_old, = ax2.plot(np.array(old_p)*1e-6, 'r', label = 'old')
    p_number, = ax2.plot(np.array(number_p)*1e-6,'g', label = 'number')
    p_energy, = ax2.plot(np.array(energy_p)*1e-6, 'b', label = 'energy')
    ax2.axhline(3.2, color = 'gray', dashes = [2,2], label = 'limit')
    ax1.axhline(4, color = 'gray', dashes = [2,2], label = 'limit')
#    ax1.set_yscale('log')
#    ax1.set_xscale('log')
    ax1.legend(loc = 'upper right')
    plt.show()

if __name__ == '__main__':
    main()
