#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def read_diag(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def read_field(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)/norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def read_maxfield(file):
    axis = []
    ez_t = []
    bx_t = []
    ne_t = []
    np_t = []
    f = open(file, 'r')
    for line in f:
        a = line.split()
        axis.append(float(a[0]))
        ez_t.append(float(a[1]))
        bx_t.append(float(a[2]))
        ne_t.append(float(a[3]))
        np_t.append(float(a[4]))
    return axis, ez_t, bx_t, ne_t, np_t

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])

def main():
    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
    ex_xpath = 'data/Ex_x'
    ex_ypath = 'data/Ex_y'
    ex_zpath = 'data/Ex_z'
    ey_xpath = 'data/Ey_x'
    ey_ypath = 'data/Ey_y'
    ey_zpath = 'data/Ey_z'
    ez_xpath = 'data/Ez_x'
    ez_ypath = 'data/Ez_y'
    ez_zpath = 'data/Ez_z'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    bx_xpath = 'data/Bx_x'
    bx_ypath = 'data/Bx_y'
    bx_zpath = 'data/Bx_z'
    by_xpath = 'data/By_x'
    by_ypath = 'data/By_y'
    by_zpath = 'data/By_z'
    bz_xpath = 'data/Bz_x'
    bz_ypath = 'data/Bz_y'
    bz_zpath = 'data/Bz_z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nixpath = 'data/Proton2Dx'
    niypath = 'data/Proton2Dy'
    nizpath = 'data/Proton2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nphxpath = 'data/Photon2Dx'
    nphypath = 'data/Photon2Dy'
    nphzpath = 'data/Photon2Dz'
    picspath = 'pics'
    ppath = 'data/ElPhase/'
    mpath = 'data/PosPhase/'
    p1path = 'data/ElMomentum/'
    m1path = 'data/PosMomentum/'
   

    config = utils.get_config("ParsedInput.txt")
    wl = float(config['Wavelength'])
    Xmax = float(config['X_Max'])/wl #mkm to wavelength
    Xmin = float(config['X_Min'])/wl #mkm to wavelength
    Ymax = float(config['Y_Max'])/wl #mkm to wavelength
    Ymin = float(config['Y_Min'])/wl #mkm to wavelength

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])

#    nx1 = int(config['ElPhase.SetMatrixSize_0'])
#    ny1 = int(config['ElPhase.SetMatrixSize_1'])
#    zmin = float(config['ElPhase.SetBounds_0'])*1e4
#    zmax = float(config['ElPhase.SetBounds_1'])*1e4
#    Emin = float(config['ElPhase.SetBounds_2'])
#    Emax = float(config['ElPhase.SetBounds_3'])
#    Mmin = float(config['ElMomentum.SetBounds_2'])
#    Mmax = float(config['ElMomentum.SetBounds_3'])

    delta = 1

    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(expath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = nmin + 1
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'

    dv = dx*dy*dz
    axis1 = create_axis(nx, (Xmax-Xmin)/nx, Xmin)
    mult = 1./(2.*dv)
    for i in range(nmin, nmax, delta):
#        fig = plt.figure(num=None, figsize=(32, 12), dpi=128)
        fig, axes = plt.subplots(nrows=4, sharex=True, figsize=(24,15))
#        a2x = fig.add_subplot(3,4,1)
#        ax_x = fig.add_subplot(3,4,2)
#        ay_x = fig.add_subplot(3,4,3)
#        az_x = fig.add_subplot(2,1,1)
#        prof_x = fig.add_subplot(2,1,2)
#        a2x.set_xlim([-1, 1])
#        a2x.set_ylim([-0.5, 0.5])
#        ax_x.set_xlim([-1, 1])
#        ax_x.set_ylim([-0.5, 0.5])
#        ay_x.set_xlim([-1, 1])
#        ay_x.set_ylim([-0.5, 0.5])
        axes[0].set_xlim([-1, 1])
        axes[0].set_ylim([-0.5, 0.5])
        axes[1].set_ylim([-0.5, 0.5])
#        b2x = fig.add_subplot(3,4,5)
#        bx_x = fig.add_subplot(3,4,6)
#        by_x = fig.add_subplot(3,4,7)
#        bz_x = fig.add_subplot(3,4,8)
#        b2x.set_xlim([-1, 1])
#        b2x.set_ylim([-0.5, 0.5])
#        bx_x.set_xlim([-1, 1])
#        bx_x.set_ylim([-0.5, 0.5])
#        by_x.set_xlim([-1, 1])
#        by_x.set_ylim([-0.5, 0.5])
#        bz_x.set_xlim([-1, 1])
#        bz_x.set_ylim([-0.5, 0.5])
#        ezx = fig.add_subplot(3,4,12)
#        el_x = fig.add_subplot(3,4,9)
#        pos_x = fig.add_subplot(3,4,10)
#        gamma = fig.add_subplot(3,4,10)
#        momentum = fig.add_subplot(3,4,11)
    
#        fig = plt.figure(num=None, figsize=(32, 20), dpi=128)
        
        picname = picspath + '/' + "fcm%06d.png" % (i,)
        print picname
#        a2x = fig.add_subplot(3,4,1)
#        ax_x = fig.add_subplot(3,4,2)
#        ay_x = fig.add_subplot(3,4,3)
#        az_x = fig.add_subplot(3,4,4)
#        name = expath + '/' + "%06d.txt" % (i,)
#        fielde2x = read_field(name,nx,ny)
#        m = max2d(fielde2x)

#        surf = a2x.imshow(fielde2x, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Reds')
        name = ex_xpath + '/' + "%06d.txt" % (i,)
        fieldex_x = read_field(name,nx,ny)
#        surf = ax_x.imshow(fieldex_x, extent=[Xmin, Xmax, Ymin, Ymax], vmin = -m, vmax = m, cmap='Spectral')
        name = ey_xpath + '/' + "%06d.txt" % (i,)
        fieldey_x = read_field(name,nx,ny)
#        surf = ay_x.imshow(fieldey_x, extent=[Xmin, Xmax, Ymin, Ymax], vmin = -m, vmax = m, cmap='Spectral')
        name = expath + '/' + "%06d.txt" % (i,)
        fieldex = read_field(name,nx,ny)
        
        name = ez_xpath + '/' + "%06d.txt" % (i,)
        fieldez_x = read_field(name,nx,ny)
        m = max2d(fieldez_x)
        surf = axes[0].imshow(fieldez_x, extent=[Xmin, Xmax, Ymin, Ymax], vmin = -m, vmax = m, cmap='bwr')
        name = nexpath + '/' + "%06d.txt" % (i,)
        nex = read_field(name,nx,ny,mult)
        name = npxpath + '/' + "%06d.txt" % (i,)
        npx = read_field(name,nx,ny,mult)
        m = max2d(nex)
        print m
        surf = axes[1].imshow(nex, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Greens', vmin = m*1e-4, vmax = m, norm=clr.LogNorm())
        axes[2].plot(axis1,nex[nx/2], 'g', label = 'El')
        axes[2].plot(axis1,npx[nx/2], 'b', label = 'Pos')
        axes[3].plot(axis1,fieldez_x[nx/2], 'r',label = 'Ez')
        axes[3].plot(axis1,fieldex[nx/2], 'k',label = '|Ez|')
        axes[3].plot(axis1,fieldey_x[nx/2], 'b',label = 'Ey')
        axes[3].plot(axis1,fieldex_x[nx/2], 'g',label = 'Ex')
        axes[3].legend(loc='upper right', shadow=True)
        plt.grid()
        
#        axes[1].plot(axis1,npx[nx/2], 'k', label = 'Pos')
        
        
#        name = bxpath + '/' + "%06d.txt" % (i,)
#        fielde2x = read_field(name,nx,ny)
#        m = max2d(fielde2x)
#        surf = b2x.imshow(fielde2x, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Blues')
#        name = bx_xpath + '/' + "%06d.txt" % (i,)
#        fieldex_x = read_field(name,nx,ny)
#        surf = bx_x.imshow(fieldex_x, extent=[Xmin, Xmax, Ymin, Ymax], vmin = -m, vmax = m, cmap='Spectral')
#        name = by_xpath + '/' + "%06d.txt" % (i,)
#        fieldey_x = read_field(name,nx,ny)
#        surf = by_x.imshow(fieldey_x, extent=[Xmin, Xmax, Ymin, Ymax], vmin = -m, vmax = m, cmap='Spectral')
#        name = bz_xpath + '/' + "%06d.txt" % (i,)
#        fieldez_x = read_field(name,nx,ny)
#        surf = bz_x.imshow(fieldez_x, extent=[Xmin, Xmax, Ymin, Ymax], vmin = -m, vmax = m, cmap='Spectral')
        
      
#        name = nexpath + '/' + "%06d.txt" % (i,)
#        fieldel_x = read_field(name,nx,ny)
#        m = max2d(fieldel_x)
#        surf = el_x.imshow(fieldel_x, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Greens', vmin = m*1e-3, vmax = m, norm=clr.LogNorm())
#        el_x.set_xlim([-1, 1])
#        el_x.set_ylim([-0.5, 0.5])
#        name = npxpath + '/' + "%06d.txt" % (i,)
#        fieldpos_x = read_field(name,nx,ny)
#        m = max2d(fieldpos_x)
#        surf = pos_x.imshow(fieldpos_x, extent=[Xmin, Xmax, Ymin, Ymax], cmap='bone_r', vmin = m*1e-3, vmax = m, norm=clr.LogNorm())
#        pos_x.set_xlim([-1, 1])
#        pos_x.set_ylim([-0.5, 0.5])

#        pname = ppath + '/' + "%06d.txt" % (i,)
#        p1name = p1path + '/' + "%06d.txt" % (i,)
#        pfield = read_diag(pname,nx1,ny1)
#        p1field = read_diag(p1name,nx1,ny1)
#        m = max2d(pfield)
#        gamma.imshow(pfield, extent = [zmin,zmax, Emin,Emax], aspect='auto', cmap='Greens', origin='lower', vmin = m*1e-2, vmax = m, norm=clr.LogNorm())
#        m = max2d(p1field)
#        momentum.imshow(p1field, extent = [zmin,zmax, Mmin,Mmax], aspect='auto', cmap='Greens', origin='lower', vmin = m*1e-2, vmax = m, norm=clr.LogNorm())
                
#        axis, ez_t, bx_t, ne_t, np_t = read_maxfield('max_field_ne.dat')
#        ezx.plot(axis, ez_t, 'r', label = 'ez')
#       ezx.plot(axis, bx_t, 'b', label = 'bx')
#        ezx.legend(loc='lower left', shadow=True)
#        el = ezx.twinx()
#        el.plot(axis, ne_t, 'g', label = 'el')
#        el.plot(axis, np_t, 'k', label = 'pos')
#        step = axis[1] - axis[0]
#        axis1 = [i*step, i*step]
#        data1 = [min(ne_t), max(ne_t)]
#        axis2 = [axis[0], axis[-1]]
#        data2 = [0, 0]
#        ezx.plot(axis2, data2, 'k')
#        el.plot(axis1, data1, 'K', linewidth=2.0)
#        el.legend(loc='upper left', shadow=True)
#        el.set_yscale('log')
#        plt.subplots_adjust(left=0.0, right=1.0, bottom=0.0, top=1.0)
        plt.savefig(picname)
        plt.close()
    
#        plt.show()
    
if __name__ == '__main__':
    main()
