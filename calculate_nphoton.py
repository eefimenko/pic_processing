#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def create_array(nx,ny):
    return [[0. for i in range(nx)] for j in range(ny)]

def read_field(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)/norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def create_axis(n,step, x0):
    axis = []
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])

def calc_npart(a, dr, dz):
    n = 0 
    for k in range(len(a[0])):
        for m in range(len(a)):
            n += a[m][k] * 2. * math.pi * (m + 0.5) * dr * dr *dz
    return n

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def calc_n(path,i,nx,ny,length):
    name = path + '/' + "%06d.txt" % (i,)
    array = read_field(name,nx,ny)
    summ = 0
    for k in range(2*length):
        for m in range(length):
            summ += array[nx/2+m][ny/2-length+k] * 2. * math.pi * (m + 0.5)/2. # dv = dr * dr * dz
    return summ

def main():
    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
    ex_xpath = 'data/Ex_x'
    ex_ypath = 'data/Ex_y'
    ex_zpath = 'data/Ex_z'
    ey_xpath = 'data/Ey_x'
    ey_ypath = 'data/Ey_y'
    ey_zpath = 'data/Ey_z'
    ez_xpath = 'data/Ez_x'
    ez_ypath = 'data/Ez_y'
    ez_zpath = 'data/Ez_z'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    bx_xpath = 'data/Bx_x'
    bx_ypath = 'data/Bx_y'
    bx_zpath = 'data/Bx_z'
    by_xpath = 'data/By_x'
    by_ypath = 'data/By_y'
    by_zpath = 'data/By_z'
    bz_xpath = 'data/Bz_x'
    bz_ypath = 'data/Bz_y'
    bz_zpath = 'data/Bz_z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nixpath = 'data/Proton2Dx'
    niypath = 'data/Proton2Dy'
    nizpath = 'data/Proton2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nphxpath = 'data/Photon2Dx'
    nphypath = 'data/Photon2Dy'
    nphzpath = 'data/Photon2Dz'
    nepath = 'data/NeTrap/'
    picspath = 'pics'
    config = utils.get_config("ParsedInput.txt")
    wl = float(config['Wavelength'])
    Xmax = float(config['X_Max'])/wl #mkm to wavelength
    Xmin = float(config['X_Min'])/wl #mkm to wavelength
    Ymax = float(config['Y_Max'])/wl #mkm to wavelength
    Ymin = float(config['Y_Min'])/wl #mkm to wavelength

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)
    
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    delta = 1

    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])

    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15)) 
    wl = float(config['Wavelength'])
    peakpower = float(config['PeakPower'])*1e-7*1e-15
    coeff = 10./(3.3e11*3.3e11)*0.97399*(wl/0.8e-4)*(wl/0.8e-4)
    step = x0*y0*1e15/T

    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(expath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(expath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'

    dv = 2*dx*dy*dz
    axis1 = create_axis(nx, (Xmax-Xmin)/nx, Xmin)
    ez_t = []
    bx_t = []
    ne_t = []
    np_t = []
    f = open('max_field_ne.dat', 'w')
    length = int(wl/2/dy)
    tmp = create_array(2*length, length)
    print length, len(tmp), len(tmp[0])
    n0 = 0
    nf = 0
    i_0 = 0
    i_f = nmax
    av = 0 
    dmy = int(0.35*wl/dy)
    for i in range(nmin, nmax, delta):
        if i%10 == 0:
            print i 
        ezname = ezpath + '/' + "%06d.txt" % (i,)
        bzname = bzpath + '/' + "%06d.txt" % (i,)
        ez = read_field(ezname,nx,ny)
        bz = read_field(bzname,nx,ny)
        ezv = ez[nx/2][ny/2]
        bzv = bz[nx/2][ny/2-dmy]*1.53
        tmp = (ezv*ezv + bzv*bzv)*coeff
        
        n = calc_n(nexpath,i,nx,ny,length)
        p = calc_n(npxpath,i,nx,ny,length)
        ph = calc_n(nphxpath,i,nx,ny,length) 
#        name = nexpath + '/' + "%06d.txt" % (i,)
#        ne_x = read_field(name,nx,ny)
#        e = read_trap(nepath + '%06d.txt' % (i,))
#        for k in range(2*length):
#            for m in range(length):
#                tmp[m][k] = ne_x[nx/2+m][ny/2-length+k]/dv

#        n = calc_npart(tmp, dy, dz)
        np_t.append(ph/(n+p))
#        ne_t.append(e)
        if n0 == 0 and tmp > 0.993*peakpower:
            n0 = 1
            i_0 = i
            print 'Start: ', n0, i_0

        if n0 != 0 and nf == 0 and tmp < 0.993*peakpower:
            nf = 1
            i_f = i
            print 'Stop: ', n0, i_f
            break 
        if i_0 > 0:
            av += ph/(n+p)
        
    if i_0 > 0 and i_f > 0 and (i_f - i_0) > 0:
        av = av/(i_f - i_0) 
    print av
    f = open('avn.dat', 'w')
    f.write('%lf\n'%(av))
    f.close()
    fig = plt.figure(num=None)
    mp.rcParams.update({'font.size': 8})
    ax = fig.add_subplot(1,1,1)
    ax.plot(ne_t, 'r', label = 'NeTrap')
    ax.plot(np_t, 'g', label = 'calc')
    ax.legend(loc='upper left', shadow=True)
#    ax.set_yscale('log')
    plt.show()
    plt.close()

if __name__ == '__main__':
    main()
