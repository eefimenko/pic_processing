#ifndef COMPLEX_H_INCLUDED
#define COMPLEX_H_INCLUDED

# include <gsl/gsl_rng.h>
# include <gsl/gsl_randist.h>
# include <fstream>
# include <iostream>
# include <math.h>
# include <stdlib.h>
# include <string.h>

//0 - Windows
//1 - Linux
const int sys = 0;

using namespace std;
const double Pi = 3.1415926535897932346;
inline double DCutter(double x)
{
    if(x < 0.0001)return 0;
    return x;
};
inline string DoubleToString(double x)
{
	char str[32];
	sprintf(str, "%g", x);
	return str;
};
inline string DoubleToString_(double x)
{
	char str[32];
	sprintf(str, "%g", DCutter(x));
	return str;
};
class complex
{
public: double Re;
		double Im;
public:
	inline complex(double re, double im){
		Re = re;
		Im = im;
	}
	inline complex(double re){
		Re = re;
		Im = 0;
	}
	inline complex(int re){
		Re = re;
		Im = 0;
	}
	inline complex(){
		Re = 0;
		Im = 0;
	}
	~complex(){
	}
	double abs_(double x){
		if(x > 0) return x; else return -x;
	}
	string strprint_double(double x)
	{
		if(((abs_(x) < 1e-4)||(abs_(x) >= 1e+14))&&(x != 0)){
			return "(" + DoubleToString(x) + ")";
		} else return DoubleToString(x);
	}
	string strprint()
	{
	    string Res = "";
	    if((Re!=0)||(Im==0))Res = strprint_double(Re);
		if(Im!=0)
		{
			if(Im>=0)
			{
				if((Re!=0)||(Im==0))Res += "+";
			}else Res += "-";
			Res += strprint_double(abs_(Im));
			Res += "*i";
		}
		return Res;
	}
	void print_double(double x){
		if(((abs_(x) < 1e-4)||(abs_(x) >= 1e+14))&&(x != 0)){
			cout << "(" << x << ")";
		} else cout << x;
	}
	void print(){
		if((Re!=0)||(Im==0))print_double(Re);
		if(Im!=0)
		{
			if(Im>=0)
			{
				if((Re!=0)||(Im==0))cout << "+";
			}else cout << "-";
			print_double(abs_(Im));
			cout << "*i";
		}
	}
	void printf_double(double x, fstream &f){
		if(((abs_(x) < 1e-4)||(abs_(x) >= 1e+14))&&(x != 0)){
			f << "(" << x << ")";
		} else f << x;
	}
	void printf(fstream &f){
		if((Re!=0)||(Im==0))printf_double(Re, f);
		if(Im!=0)
		{
			if(Im>=0)
			{
				if((Re!=0)||(Im==0))f << "+";
			}else f << "-";
			printf_double(Im, f);
			f << "*i";
		}
	}
/*	inline void operator=(complex &a){
		Re = a.Re;
		Im = a.Im;
	}*/
	inline complex operator=(double a){
		Re = a;
		Im = 0;
		return *this;
	}
	inline bool operator==(complex a){
		return ((Re == a.Re)&&(Im == a.Im));
	}
	inline bool operator!=(complex a){
		return !((Re == a.Re)&&(Im == a.Im));
	}
	inline complex operator+(){
		//return *this;
		complex ret(Re, Im);
		return ret;
	}
	inline complex operator-(){
	    complex ret(-Re, -Im);
		return ret;
	}
	inline friend double abs(complex a){
		return sqrt(a.Re*a.Re + a.Im*a.Im);
	}
	inline double abs_2(){
		return Re*Re + Im*Im;
	}
	inline friend complex sqr(complex a){
        return a*a;
	}
	inline friend complex sqrt(complex a){
		double r=abs(a);
		if(r>0)
		{
			double c = sqrt((1 + a.Re/r)/2);
			double s = sqrt((1 - a.Re/r)/2);
			if(a.Im < 0)s *= -1;
			r = sqrt(r);
			complex ret(r*c, r*s);
			return ret;
		}else{
			complex ret(0, 0);
			return ret;
		}
	}
	inline complex operator+=(complex a){
		Re += a.Re;
		Im += a.Im;
		return *this;
	}
	inline complex operator+=(double a){
		Re += a;
		return *this;
	}
	inline complex operator-=(complex a){
		Re -= a.Re;
		Im -= a.Im;
		return *this;
	}
	inline complex operator-=(double a){
		Re -= a;
		return *this;
	}
	inline complex operator*=(complex a){
		double r = Re;
		Re = Re*a.Re - Im*a.Im;
		Im = r*a.Im + Im*a.Re;
		return *this;
	}
	inline complex operator*=(double a){
		Re *= a;
		Im *= a;
		return *this;
	}
	inline complex operator/=(complex a){
		double r = Re;
		Re = (Re*a.Re + Im*a.Im)/(a.Re*a.Re + a.Im*a.Im);
		Im = (Im*a.Re - r*a.Im)/(a.Re*a.Re + a.Im*a.Im);
		return *this;
	}
	inline complex operator/=(double a){
		Re /= a;
		Im /= a;
		return *this;
	}
	inline friend complex conj(complex a){
		complex ret(a.Re, -a.Im);
		return ret;
	}
	inline friend complex operator+(complex a,complex b){
		return complex(a.Re + b.Re, a.Im + b.Im);
	}
	inline friend complex operator+(double a,complex b){
		complex ret(a + b.Re, b.Im);
		return ret;
	}
	inline friend complex operator+(complex a,double b){
		complex ret(a.Re + b, a.Im);
		return ret;
	}
	inline friend complex operator-(complex a,complex b){
		complex ret(a.Re - b.Re, a.Im - b.Im);
		return ret;
	}
	inline friend complex operator-(double a,complex &b){
		complex ret(a - b.Re, -b.Im);
		return ret;
	}
	inline friend complex operator-(complex a,double b){
		complex ret(a.Re - b,a.Im);
		return ret;
	}
	inline friend complex operator*(complex a,complex b){
		complex ret(b.Re*a.Re - b.Im*a.Im, b.Re*a.Im + b.Im*a.Re);
		return ret;
	}
	inline friend complex operator*(double a,complex b){
		complex ret(a*b.Re, a*b.Im);
		return ret;
	}
	inline friend complex operator*(complex a,double b){
		complex ret(b*a.Re, b*a.Im);
		return ret;
	}
	inline friend complex operator/(complex a,complex b){
		complex ret((a.Re*b.Re+a.Im*b.Im)/(b.Re*b.Re+b.Im*b.Im), (a.Im*b.Re-a.Re*b.Im)/(b.Re*b.Re+b.Im*b.Im));
		return ret;
	}
	inline friend complex operator/(double a,complex &b){
		complex ret((a*b.Re)/(b.Re*b.Re + b.Im*b.Im), (-a*b.Im)/(b.Re*b.Re + b.Im*b.Im));
		return ret;
	}
	inline friend complex operator/(complex a,double b){
		complex ret(a.Re/b, a.Im/b);
		return ret;
	}
	inline friend complex exp(complex a){
		double r = exp(a.Re);
		double t = cos(a.Im);
		if((int(a.Im/Pi)%2) == 0){
			if(a.Im >= 0){
				complex ret(r*t, r*sqrt(1 - t*t));
				return ret;
			}else{
				complex ret(r*t, -r*sqrt(1 - t*t));
				return ret;
			}
		}else{
			if(a.Im >= 0){
				complex ret(r*t, -r*sqrt(1 - t*t));
				return ret;
			}else{
				complex ret(r*t, r*sqrt(1 - t*t));
				return ret;
			}
		}
	}
	inline friend complex exp_im(double x){
		double t = cos(x);
		if(x <= Pi){
			if(x>=0){
				complex ret(t, sqrt(1 - t*t));
				return ret;
			}else{
				complex ret(t, -sqrt(1 - t*t));
				return ret;
			}
		}else{
			if(x>=0){
				complex ret(t, -sqrt(1 - t*t));
				return ret;
			}else{
				complex ret(t, sqrt(1 - t*t));
				return ret;
			}
		}
	}
	inline friend void spec_1(complex &s,complex &A, complex&B, double x, int k){
		double r = cos(x);
		double m = sqrt(1 - r*r);
		if(x <= Pi){
			if(x < 0)m *= -1;
		}else{
			if(x >= 0)m *= -1;
		}
		s.Re = A.Re + k*(r*B.Re - m*B.Im);
		s.Im = A.Im + k*(r*B.Im + m*B.Re);
	}
	inline friend void spec_2(complex &s, complex &A, complex&B, double x, int k){
		double r = cos(x);
		double m = sqrt(1 - r*r);
		if(x <= Pi){
			if(x < 0)m *= -1;
		}else{
			if(x >= 0)m *= -1;
		}
		double a = A.Re + k*(r*B.Re - m*B.Im);
		s.Im = A.Im + k*(r*B.Im + m*B.Re);
		s.Re = a;
	}
	inline friend complex cos(complex a){
		double r=exp(a.Im);
		double _r=exp(-a.Im);
		double c=cos(a.Re);
		double s=sin(a.Re);
		complex ret(0.5*c*(_r + r), 0.5*s*(_r - r));
		return ret;
	}
	inline friend complex sin(complex a){
		double r = exp(a.Im);
		double _r = exp(-a.Im);
		double c = cos(a.Re);
		double s = sin(a.Re);
		complex ret(0.5*s*(_r + r), 0.5*c*(r - _r));
		return ret;
	}
	inline friend complex ln(complex a){
		double r = abs(a);
		double tet = 0;
		if(a.Re != 0){
			if(a.Re > 0)tet = atan(a.Im/a.Re);else{
				if(a.Im >= 0)tet = Pi - atan(a.Im/(-a.Re));
				else tet = -Pi + atan(a.Im/a.Re);
			}
		}else{
			if(a.Im >= 0)tet = Pi/2;else tet = -Pi/2;
		}
		complex ret(log(r), tet);
		return ret;
	}
};

#endif // COMPLEX_H_INCLUDED
