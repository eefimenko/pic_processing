#ifndef VECTORS_H
#define VECTORS_H

#include <cmath>
#include "Switchers.h"

// Type for real quantities
#ifdef SINGLE_PRECISION
    typedef float FP;
#else
    typedef double FP;
#endif


struct Int3
{
    int x, y, z;

    Int3()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    Int3(int _x, int _y, int _z)
    {
        x = _x;
        y = _y;
        z = _z;
    }

    inline int operator[](int idx) const
    {
        return *((int*)this + idx);
    }

    inline int& operator[](int idx)
    {
        return *((int*)this + idx);
    }
    inline bool operator==(Int3 i)
    {
        return ((x == i.x)&&(y == i.y)&&(z == i.z));
    }
};

// This function is deprecated and left for compatibility,
// will be removed later.
inline Int3 makeInt3(int x, int y, int z)
{
    return Int3(x, y, z);
}

inline Int3 operator + (const Int3 & v1, const Int3 & v2)
{
    return Int3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}

inline Int3 operator - (const Int3 & v1, const Int3 & v2)
{
    return Int3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}

inline Int3 operator * (const Int3 & v1, const Int3 & v2)
{
    return Int3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
}

inline Int3 operator / (const Int3 & v1, const Int3 & v2)
{
    return Int3(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);
}

inline bool operator < (const Int3 & v1, const Int3 & v2)
{
    return v1.x < v2.x && v1.y < v2.y && v1.z < v2.z;
}

inline bool operator > (const Int3 & v1, const Int3 & v2)
{
    return v1.x > v2.x && v1.y > v2.y && v1.z > v2.z;
}

inline bool operator <= (const Int3 & v1, const Int3 & v2)
{
    return v1.x <= v2.x && v1.y <= v2.y && v1.z <= v2.z;
}

inline bool operator >= (const Int3 & v1, const Int3 & v2)
{
    return v1.x >= v2.x && v1.y >= v2.y && v1.z >= v2.z;
}


// Vector of 3 FPs. Can be accessed by either .x, .y, .z or
// .p[0], .p[1], .p[2].
struct FP3
{
    FP x, y, z;

    FP3()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    FP3(FP _x, FP _y, FP _z)
    {
        x = _x;
        y = _y;
        z = _z;
    }

    inline FP operator[](int idx) const
    {
        return *((FP*)this + idx);
    }

    inline FP& operator[](int idx)
    {
        return *((FP*)this + idx);
    }

    FP3 operator / (FP s) const
    {
        return FP3(x / s, y / s, z / s);
    }
    inline FP norm() const
    {
        return sqrt(x*x + y*y + z*z);
    }
    inline FP norm2() const
    {
        return x*x + y*y + z*z;
    }
    inline FP volume() const
    {
        return x * y * z;
    }
    void normalize()
    {
        FP n = norm();
        x /= n;
        y /= n;
        z /= n;
    }
};

// This function is deprecated and left for compatibility,
// will be removed later.
inline FP3 makeFP3(FP x, FP y, FP z)
{
    return FP3(x, y, z);
}

inline FP3 & operator += (FP3 & v1, const FP3 & v2)
{
    v1.x += v2.x;
    v1.y += v2.y;
    v1.z += v2.z;
    return v1;
}

inline FP3 operator + (const FP3 & v1, const FP3 & v2)
{
    return FP3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}

inline FP3 operator - (const FP3 & v1, const FP3 & v2)
{
   return FP3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}

inline FP3 operator * (const FP3 & v1, const FP3 & v2)
{
    return FP3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
}

inline FP3 operator * (const FP3 & v1, const Int3 & v2)
{
   return FP3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
}

inline FP3 operator * (const FP3 & v, FP a)
{
    return FP3(v.x * a, v.y * a, v.z * a);
}

inline FP3 operator * (FP a, const FP3 & v)
{
    return FP3(v.x * a, v.y * a, v.z * a);
}

inline FP3 operator / (const FP3 & v1, const FP3 & v2)
{
    return FP3(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);
}

inline FP3 operator / (const FP3 & v1, const Int3 & v2)
{
    return FP3(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);
}

inline bool operator <= (const FP3 & v1, const FP3 & v2)
{
    return v1.x <= v2.x && v1.y <= v2.y && v1.z <= v2.z;
}

inline bool operator < (const FP3 & v1, const FP3 & v2)
{
    return v1.x < v2.x && v1.y < v2.y && v1.z < v2.z;
}

inline FP3 VP(const FP3 &a, const FP3 &b)
{
    return FP3(a.y*b.z-a.z*b.y, a.z*b.x-a.x*b.z, a.x*b.y-a.y*b.x);
}

inline FP SP(const FP3 &a, const FP3 &b)
{
    return a.x*b.x + a.y*b.y + a.z*b.z;
}

inline FP sqr(const FP3 &a)
{
    return a.x*a.x + a.y*a.y + a.z*a.z;
}


#endif
