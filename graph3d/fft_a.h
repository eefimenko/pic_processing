#ifndef FFT_A_H_INCLUDED
#define FFT_A_H_INCLUDED

#include "complex.h"
#include <ctime>
#include <fftw3.h>
#include <sstream>

#include <cstring>
#include <cstdarg>
#include <iostream>
#include <sstream>
#include <fstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "Vectors.h"
#include <vector>



inline string dir_divider()
{
    if(sys == 0) return "\\";
    if(sys == 1) return "/";
}
inline int cyclic(int x, int N) // returns cyclically redefined x within [0, N-1] interval
{
    return x%N + ((x%N) < 0)*N;
}
// when creating directory put "/" at the end of it name
inline void create_dir_fft_a(const char *main_dir, const char *dir, ...)
{
	char command[512] = "rm -r -f ";

	va_list var; va_start(var, dir);
	char temp[256];
	vsprintf(temp, dir, var);
    if(sys == 1){
	strcat(command, main_dir);
	strcat(command, temp);
	system(command);
    }
	sprintf(command, "mkdir ");
	if(sys == 0)sprintf(command, "md ");
	strcat(command, main_dir);
	strcat(command, temp);

	//cout << command << endl;

	system(command);

	va_end(var);
}

inline string create_dir_fft_a(string path)
{
	//cout << "Creating " + path + " ... ";
	struct stat buf;
	int ret = stat(path.c_str(), &buf);
	if (ret == 0)return path;
    create_dir_fft_a("", path.c_str(), "");
    return path;
}

inline void itoa(int number, std::string &val, int n)
{
    std::ostringstream sin;
    sin << number;
    val = sin.str();
}
inline string itos(int n)
{
	stringstream ss;
	ss.width(6); ss.fill('0');
	ss << right << n;
	return ss.str();
}

inline string itos_(int n)
{
	stringstream ss;
	ss << right << n;
	return ss.str();
}

inline string ftos(double a)
{
	stringstream ss;
	ss.width(15);
	ss << right << scientific << a;
	return ss.str();
}
typedef unsigned char byte;
#define CANNOTOPENFILE  -1
#define RECORDNOTFOUND  -2
inline double sqr(double x)
{
	return x*x;
};
inline double Max3(double v0, double v1, double v2)
{
    if((v0 >= v1)&&(v0 >= v2))return v0;
    if((v1 >= v0)&&(v1 >= v2))return v1;
    return v2;
}
inline int Round(double x)
{
	return int(0.5+x);
};
inline string IntToString(int x)
{
	string str;
	itoa(x, str, 10);
	return str;
};
class TChronometer
{
public:
	double time_mark;
public:
	TChronometer()
	{
	};
	virtual ~TChronometer()
	{
	};
	void Start()
	{
		time_mark = clock()/double(CLOCKS_PER_SEC);
	};
	double GetTime()
	{
		return clock()/double(CLOCKS_PER_SEC) - time_mark;
	};
	double ReStartTime()
	{
		double time_mark_ = time_mark;
		time_mark = clock()/double(CLOCKS_PER_SEC);
		return clock()/double(CLOCKS_PER_SEC) - time_mark_;
	};
};
class frame
{
public:
	int Nx;
	int Ny;
	byte *v;
	int palette_type;
	frame(int nx = 2,int ny = 2)
	{
		Nx=nx;
		Ny=ny;
		v = new byte[Nx*Ny];
		palette_type=0;
	}
	frame(frame &a)
	{
		Nx = a.Nx;
		Ny = a.Ny;
		palette_type = a.palette_type;
		v = new byte[Nx*Ny];
		int i;
		for(i = 0; i != Nx*Ny; i++)v[i] = a.v[i];
	}
	~frame()
	{
		delete []v;
	}
	void operator=(frame &a)
	{
		delete []v;
		Nx = a.Nx;
		Ny = a.Ny;
		palette_type = a.palette_type;
		v = new byte[Nx*Ny];
		int i;
		for(i = 0; i != Nx*Ny; i++)v[i] = a.v[i];
	}
	byte& pixel(int x,int y)
	{
		return v[Nx*y+x];
	}
	//function for continious transformation of frame sizes
	void TransformTo(frame &a)
	{
		int x=0,y=0;
		int Lx=a.Nx;
		int Ly=a.Ny;
		double *S;
		S = new double[Lx*Ly];

		for(x=0;x!=Lx;x++)for(y=0;y!=Ly;y++)S[Lx*y+x]=0;
		double dx=Lx/double(Nx);
		double dy=Ly/double(Ny);
		double x1=0,y1=0,x2=0,y2=0,xmin=0,xmax=0,ymin=0,ymax=0;
		int xx=0,yy=0;
		for(x=0;x!=Nx;x++)for(y=0;y!=Ny;y++)
		{
			x1=x*dx;x2=(x+1)*dx;
			y1=y*dy;y2=(y+1)*dy;
			for(xx=int(x1);((xx<=int(x2))&&(xx<Lx));xx++)
			for(yy=int(y1);((yy<=int(y2))&&(yy<Ly));yy++)
			{
				xmin=xx;xmax=xx+1;
				ymin=yy;ymax=yy+1;
				if(xmin<x1)xmin=x1;
				if(xmax>x2)xmax=x2;
				if(ymin<y1)ymin=y1;
				if(ymax>y2)ymax=y2;
				if(S[Lx*yy+xx]!=-1)S[Lx*yy+xx]+=(xmax-xmin)*(ymax-ymin)*int(v[Nx*y+x]);
				if(int(v[Nx*y+x])==0)S[Lx*yy+xx]=-1;
			}
		}
		int col=0,b=0,g=0,r=0;
		a.palette_type=palette_type;
		for(y=0;y!=Ly;y++)for(x=0;x!=Lx;x++)
		{
			col=Round(S[Lx*y+x]);
			b=0;g=0;r=0;
			if((col>=0)&&(col<=255))
			{
				if(palette_type==1)
				{
					a.pixel(x,y)=col;
					if(S[Lx*y+x]==-1){a.pixel(x,y)=0;}
				}
				if(palette_type==2)
				{
					a.pixel(x,y)=col;
					if(S[Lx*y+x]==-1){a.pixel(x,y)=0;}
				}
			}
		}
		delete []S;
	}
	//function for frame saving to the end of a file
	int AppendToFile(char *FileName){
		FILE *f;
		f = fopen(FileName,"ab+");
		if (f == NULL) return CANNOTOPENFILE;
		fwrite(&Nx,sizeof(int),1,f);
		fwrite(&Ny,sizeof(int),1,f);
		fwrite(&palette_type,sizeof(int),1,f);
		fwrite(v,sizeof(byte)*Nx*Ny,1,f);
		fclose(f);
		return 0;
	}
	//function for frame saving
	int save(char *FileName){
		InitFile(FileName);
		return AppendToFile(FileName);
	}
	void Ind(char *G,int P,int n)
	{
		int p=P;
		int m=n;
		int k=1;
		for(;m!=0;)
		{
			k=m-10*int(m/10);
			m=int(m/10);
			if(k==0)G[p]=char(48);
			if(k==1)G[p]=char(49);
			if(k==2)G[p]=char(50);
			if(k==3)G[p]=char(51);
			if(k==4)G[p]=char(52);
			if(k==5)G[p]=char(53);
			if(k==6)G[p]=char(54);
			if(k==7)G[p]=char(55);
			if(k==8)G[p]=char(56);
			if(k==9)G[p]=char(57);
			p--;
		}
	}
	//function for frame saving in file "0Mf0000f.m43"
	int save(int M,int f)
	{
		const char *F = "00f00000.m43";
		char *G;
		G = new char[13];
		int i;
		for(i = 0; i <= 12; i++)G[i] = F[i];
		Ind(G, 1, M);
		Ind(G, 7, f);
		int ret = 0;
		ret = save(G);
		return ret;
	}
/*	int save(char *File_, int M,int f)
	{
        char *F = "00f00000.m43";
        char *G;
        G = new char[13];
        int i;
        for(i = 0; i <= 12; i++)G[i] = F[i];
        Ind(G, 1, M);
        Ind(G, 7, f);
        PStr temp = PStr(File_) + PStr(G);
        int ret=0;
        //printf("save   M=%i f=%i\n",M,f);
        ret = save(temp.PChar());
        delete []G;
        return ret;
    }
*/
	//function for frame loading in file "0Mf0000f.m43"
	int load(int M,int f){
		const char *F="00f00000.m43";
		char *G;G = new char[13];
		for(int i=0;i<=12;i++)G[i]=F[i];
		Ind(G,1,M);
		Ind(G,7,f);
		int ret=0;
		ret=load(G);
		return ret;
	}
	//function for frame loading from all parts files
	bool LoadAndCombine(int f)
	{
		int N=0;
		frame F0;
		if(F0.load(0,f)==0){
			N=F0.Nx/F0.Ny;
			frame *F;F = new frame[N];
			F[0]=F0;
			int i;
			for(i=1;F[i].load(i,f)==0;i++)
			N=i+1;
			delete []v;
			Nx=F0.Nx;
			Ny=F0.Ny*N;
			palette_type=F0.palette_type;
			v = new byte[Nx*Ny];
			for(i=0;i!=N;i++){
				for(int x=0;x!=F[i].Nx;x++)for(int y=0;y!=F[i].Ny;y++){
					pixel(x,i*F0.Ny+y)=F[i].pixel(x,y);
				}
			}
			delete []F;
			return true;
		}else return false;
	}
	//function for next frame loading from a file
	void LoadNext(FILE *f){
		delete []v;
		fread(&Nx,sizeof(int),1,f);
		fread(&Ny,sizeof(int),1,f);
		fread(&palette_type,sizeof(int),1,f);
		v = new byte[Nx*Ny];
		fread(v,sizeof(byte)*Nx*Ny,1,f);
	}
	//function for loading a frame number rec from a file
	int LoadFromFile(char *FileName,int rec){
		FILE *f;
		f = fopen(FileName,"rb+");
		if (f == NULL) return CANNOTOPENFILE;
		while (true){
			LoadNext(f);
			if (rec == 1){
				fclose(f);
				return 0;
			}
			rec--;
		}
		fclose(f);
		return RECORDNOTFOUND;
	}
	//function for frame loading
	int load(char *FileName){
		return LoadFromFile(FileName,1);
	}
	//function for creation an empty file
	int InitFile(char *FileName){
		FILE *f;
		f = fopen(FileName,"w");
        fclose(f);
        return 0;
	}
	int SaveBMP_(char *FileName, int Width, int Height, byte *BMP){
		FILE *f;
		f = fopen(FileName,"wb+");
		if (f == NULL) return CANNOTOPENFILE;
		short t2;
		int t4;
		t2 = 19778;fwrite(&t2,sizeof(short),1,f);//MB
		t4 = Width*Height*3 + 54;fwrite(&t4,sizeof(int),1,f);//size of file
		t2 = 0;fwrite(&t2,sizeof(short),1,f);//Reserved 1
		t2 = 0;fwrite(&t2,sizeof(short),1,f);//Reserved 2
		t4 = 54;fwrite(&t4,sizeof(int),1,f);//OffsetBits
		t4 = 40;fwrite(&t4,sizeof(int),1,f);//Size of header
		fwrite(&Width,sizeof(int),1,f);//Width
		fwrite(&Height,sizeof(int),1,f);//Height
		t2 = 1;fwrite(&t2,sizeof(short),1,f);//Planes
		t2 = 24;fwrite(&t2,sizeof(short),1,f);//BitCount
		t4 = 0;fwrite(&t4,sizeof(int),1,f);//Compression
		t4 = Width*Height*4;fwrite(&t4,sizeof(int),1,f);//SizeImage
		t4 = 0;fwrite(&t4,sizeof(int),1,f);//XpelsPerMeter
		t4 = 0;fwrite(&t4,sizeof(int),1,f);//YpelsPerMeter
		t4 = 0;fwrite(&t4,sizeof(int),1,f);//ColorsUsed
		t4 = 0;fwrite(&t4,sizeof(int),1,f);//ColorsImportant
		fwrite(BMP,3*Width*Height,1,f);
		fclose(f);
		return 0;
	}
	void palette_line(double z,int &r,int &g,int &b)
	{
		if(palette_type == 0)//black&white
		{
			r = Round((1 - z)*255);
			if(r < 0)r = 0;
			if(r > 255)r = 255;
			g = r;
			b = r;
		}
		if(palette_type == 1)//my_old
		{
			double f=2*3.1415927*z*0.9999;
			r=255;g=255;b=255;
			if((z>1)||(z<0)||(f<0)||(f>2*3.1415927)){r=200;g=0;b=0;}
			int F=int(256*2*f/(2*3.1415927));
			if((F>=0)&&(F<=255)){r=F;g=r;b=255-F;}
			if((F>=256)&&(F<=511)){r=255;g=511-F;b=0;}
			r=int(r);g=int(g);b=int(b);
		}
		if(palette_type == 2)//classic_matlab
		{
			r = 255;
			g = 255;
			b = 255;
			if((z >= 0)&&(z < 0.125)){
				r = 0;
				g = 0;
				b = 127 + int(128*z/0.125);
			}
			if((z >= 0.125)&&(z < 0.375)){
				r = 0;
				g = int(255*(z - 0.125)/0.25);
				b = 255;
			}
			if((z >= 0.375)&&(z < 0.625)){
				r = int(255*(z - 0.375)/0.25);
				g = 255;
				b = int(255*(0.625 - z)/0.25);
			}
			if((z >= 0.625)&&(z < 0.875)){
				r = 255;
				g = int(255*(0.875 - z)/0.25);
				b = 0;
			}
			if((z >= 0.875)&&(z <= 1)){
				r = 255 - int(128*(z - 0.875)/0.125);
				g = 0;
				b = 0;
			}
		}
	}
	int SaveBMP(char *FileName){
		int width = Nx;
		int height = Ny;
		byte *bmp;
		bmp = new byte[3*Nx*Ny+1];
		int i, x, y;
		i = 0;
		for(y = Ny-1; y != -1; y--)for(x = Nx-1; x != -1; x--){
			int r, g, b;
			palette_line(int(v[Nx*y + (Nx - x - 1)])/double(255),r,g,b);
			bmp[3*i  ] = b;
			bmp[3*i+1] = g;
			bmp[3*i+2] = r;
			i++;
		}
		return SaveBMP_(FileName, width, height, bmp);
	}
	int SaveBMP(string Name)
	{
        char CH[32];
        strcpy(CH, Name.c_str());
        return SaveBMP(CH);
	}
    void Makefilm(char *Dir)
	{
	    char dir[256];
	    char tmp[256];
	    create_dir_fft_a(dir, "", Dir);
	    bool ex = true;
	    int j;
	    for(j = 0;ex;j++)
	    {
	        ex = LoadAndCombine(j);
            if(ex)
            {
                const char *F = "00000000.bmp";
                char *G;
                G = new char[13];
                int i;
                for(i = 0; i <= 12; i++)G[i] = F[i];
                Ind(G, 7, j);
                sprintf(tmp, dir);
                if(sys == 0)strcat(tmp,"\\");
                if(sys == 1)strcat(tmp,"/");
                strcat(tmp,G);
                SaveBMP(tmp);
                cout << "frame_" << j << endl;
            }
	    }
	}
};
class point_f
{
public:	    complex *xp;
			fftw_complex *fxp;
			fftw_plan fp, bp;
			int n;
public:
    void Null()
    {
        memset(xp, char(0), n*sizeof(complex));
    }
	inline point_f(int k = 2)
	{
		n = k;
		fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
		xp = (complex*)fxp;
		double x = log2(n);
		if(x - int(x) == 0)
		{
            fp = fftw_plan_dft_1d(n, fxp, fxp, FFTW_FORWARD, FFTW_PATIENT);
            bp = fftw_plan_dft_1d(n, fxp, fxp, FFTW_BACKWARD, FFTW_PATIENT);
		}
	}
	inline ~point_f()
	{
	    double x = log2(n);
		if(x - int(x) == 0)
		{
            fftw_destroy_plan(fp);
            fftw_destroy_plan(bp);
		}
		fftw_free(fxp);
	}
	void constructor(int k = 2)
	{
	    fftw_destroy_plan(fp);
		fftw_destroy_plan(bp);
		fftw_free(fxp);
        n = k;
		fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
		xp = (complex*)fxp;
		double x = log2(n);
		if(x - int(x) == 0)
		{
            fp = fftw_plan_dft_1d(n, fxp, fxp, FFTW_FORWARD, FFTW_PATIENT);
            bp = fftw_plan_dft_1d(n, fxp, fxp, FFTW_BACKWARD, FFTW_PATIENT);
		}
	}
	void nofftw(int k = 2)
	{
	    fftw_destroy_plan(fp);
		fftw_destroy_plan(bp);
		fftw_free(fxp);
        n = k;
		fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
		xp = (complex*)fxp;
	}
	inline void operator=(point_f &a)
	{
		fftw_free(fxp);
		n = a.n;
		fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
		int i;
		for(i = 0; i != n; i++)
		{
			fxp[i][0] = a.fxp[i][0];
			fxp[i][1] = a.fxp[i][1];
		}
	}
	inline complex& value(int i)
	{
		if(i < 0)return xp[0];
		if(i >= n)return xp[n-1];
		return xp[i];
	}
	inline complex get_value(double x)
	{
		complex z;
		z = value(int(x)+1)*(x-int(x)) + value(int(x))*(int(x)+1-x);
		return z;
	}
	inline complex& operator[](int i)
	{
		return value(i);
	}
private:inline complex bracket_(int i)
	{
		if((i>=0)&&(i<n)){return xp[i];}else return xp[n];
	}
public:
    inline friend point_f operator*(complex x, point_f a);
	inline friend point_f operator*(point_f a,complex x);
	inline friend complex operator*(point_f &a,point_f &b);
	inline friend point_f operator+(complex x,point_f &a);
	inline friend point_f operator+(point_f &a,complex x);
	inline friend point_f operator+(point_f &a,point_f &b);
	inline point_f operator*=(complex x)
	{
		for(int i=0;i!=n;i++)xp[i]*=x;
		return *this;
	}
	inline point_f operator+=(point_f &a)
	{
		for(int i=0;i!=n;i++)xp[i]+=a.xp[i];
		return *this;
	}
	inline point_f operator+=(complex x)
	{
		for(int i=0;i!=n;i++)xp[i]+=x;
		return *this;
	}
	inline friend point_f operator-(point_f &a, point_f &b);
	inline point_f operator-=(point_f &a)
	{
		for(int i=0;i!=n;i++)xp[i]-=a.xp[i];
		return *this;
	}
	void print()
	{
		complex c;
		for(int i=0;i!=n;i++)
		{
			c=bracket_(i);
			cout << "[" << i << "]:";c.print();cout << endl;
		}
	}
	void printf(fstream &f)
	{
		complex c;
		for(int i=0;i!=n;i++)
		{
			c=bracket_(i);
			f << "[" << i << "]:";c.printf(f);f << endl;
		}
	}
	void FFT_d()
	{
		fftw_execute(fp);
	}
	void FFT_b()
	{
		fftw_execute(bp);
		int i;
		for(i = 0;i != n; i++)
		{
			fxp[i][0] /= double(n);
			fxp[i][1] /= double(n);
		}
	}
	void FFT(int direction)
	{
		if(direction == 1) FFT_d();
		if(direction == -1) FFT_b();
	}
	void normalize()
	{
		double S = 0;
		int i;
		for(i = 0; i != n; i++)S += xp[i].abs_2()/n;
		for(i = 0; i != n; i++)xp[i] /= sqrt(S);
	}
};
point_f operator*(complex x, point_f a)
{
    int i;
    for(i = 0; i != a.n; i++)a.xp[i] *= x;
    return a;
};
point_f operator*(point_f a, complex x)
{
    int i;
    for(i = 0; i != a.n; i++)a.xp[i] *= x;
    return a;
};
complex operator*(point_f &a, point_f &b)
{
    complex S;
    int i;
    for(i = 0; i != a.n; i++)S = S + a.xp[i] * b.xp[i];
    return S;
};
point_f operator+(complex x,point_f &a)
{
    int i;
    for(i = 0; i != a.n; i++)a.xp[i] += x;
    return a;
};
point_f operator+(point_f &a,complex x)
{
    int i;
    for(i = 0; i != a.n; i++)a.xp[i] += x;
    return a;
};
point_f operator+(point_f &a,point_f &b)
{
    int i;
    for(i = 0; i != a.n; i++)a.xp[i] += b.xp[i];
    return a;
};
point_f operator-(point_f &a, point_f &b)
{
    int i;
    for(i = 0; i != a.n; i++)a.xp[i] -= b.xp[i];
    return a;
};
class point_fd
{
public: complex *xp;
		int n;
		int dim;
		int *d;
		fftw_complex *fxp;
		fftw_plan fp, bp;
public: void Null()
        {
            memset(xp, char(0), n*sizeof(complex));
        }
        point_fd(int dimension, int d1, int d2)
		{
			dim = dimension;
			if(dim != 2)
			{
				printf("Error: point_fd constructor: incorrect parameter \n");
				exit(0);
			}
			d = new int[dim + 1];
			d[1] = d1;
			n = d1;
			d[2] = d2;
			n *= d[2];
			fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
			xp = (complex*)fxp;
			fp = fftw_plan_dft_2d(d2, d1, fxp, fxp, FFTW_FORWARD, FFTW_PATIENT);
			bp = fftw_plan_dft_2d(d2, d1, fxp, fxp, FFTW_BACKWARD, FFTW_PATIENT);
		}
		point_fd()
		{
			point_fd(2,1,1);
		}
		point_fd(int dimension, int d1, int d2, int d3)
		{
			dim = dimension;
			if(dim != 3)
			{
				printf("Error: point_fd constructor: incorrect parameter \n");
				exit(0);
			}
			d = new int[dim + 1];
			d[1] = d1;
			n = d1;
			d[2] = d2;
			n *= d[2];
			d[3] = d3;
			n *= d[3];
			fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
			xp = (complex*)fxp;
			fp = fftw_plan_dft_3d(d3, d2, d1, fxp, fxp, FFTW_FORWARD, FFTW_PATIENT);
			bp = fftw_plan_dft_3d(d3, d2, d1, fxp, fxp, FFTW_BACKWARD, FFTW_PATIENT);
		}
		inline ~point_fd()
		{
			fftw_destroy_plan(fp);
			fftw_destroy_plan(bp);
			fftw_free(fxp);
			delete []d;
		}
		//function returns true only if the (x, y)/(x, y, z) is available point of the matrix
		bool RightIndex(int x, int y)
		{
			if((x < 0)||(x > d[1] - 1)) return false;
			if((y < 0)||(y > d[2] - 1)) return false;
			return true;
		}
		bool RightIndex(int x, int y, int z)
		{
			if((x < 0)||(x > d[1] - 1)) return false;
			if((y < 0)||(y > d[2] - 1)) return false;
			if((z < 0)||(z > d[2] - 1)) return false;
			return true;
		}
		complex& operator()(int n1, int n2)
		{
			int N = n1;
			int K = d[1];
			N += n2 * K;
			return xp[N];
		}
		complex& operator()(int n1, int n2, int n3)
		{
			int N = n1;
			int K = d[1];
			N += n2 * K;
			K *= d[2];
			N += n3 * K;
			return xp[N];
		}
		complex& br(int *n)
		{
			int N=n[1];
			int K=d[1];
			for(int i=2;i<=dim;i++)
			{
				N+=n[i]*K;
				K*=d[i];
			}
			return xp[N];
		}
		// function for matrix saving to the end of a file
		int	AppendToFile(char *FileName)
		{
			FILE *f;
			f = fopen(FileName,"ab+");
			if (f == NULL) return CANNOTOPENFILE;
			fwrite(&n,sizeof(int),1,f);
			fwrite(&dim,sizeof(int),1,f);
			fwrite(d,sizeof(int)*(dim+1),1,f);
			fwrite(xp,sizeof(complex)*n,1,f);
			fclose(f);
			return 0;
		}
		//function for next matrix loading from a file
		void LoadNext(FILE *f)
		{
			fread(&n,sizeof(int),1,f);
			fread(&dim,sizeof(int),1,f);
			fftw_destroy_plan(fp);
			fftw_destroy_plan(bp);
			fftw_free(fxp);
			delete []d;
			d  = new int[dim+1];
			fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
			xp = (complex*)fxp;
			fread(d,sizeof(int)*(dim+1),1,f);
			fread(xp,sizeof(complex)*n,1,f);
			if(dim == 2){
				fp = fftw_plan_dft_2d(d[2], d[1], fxp, fxp, FFTW_FORWARD, FFTW_PATIENT);
				bp = fftw_plan_dft_2d(d[2], d[1], fxp, fxp, FFTW_BACKWARD, FFTW_PATIENT);
			}
			if(dim == 3){
				fp = fftw_plan_dft_3d(d[3], d[2], d[1], fxp, fxp, FFTW_FORWARD, FFTW_PATIENT);
				bp = fftw_plan_dft_3d(d[3], d[2], d[1], fxp, fxp, FFTW_BACKWARD, FFTW_PATIENT);
			}
		}
		//function for loading a rec-th matrix from a file
		int LoadFromFile_(char *FileName,int rec)
		{
			FILE *f;
			f = fopen(FileName,"rb+");
			if (f == NULL) return CANNOTOPENFILE;
			while (true)
			{
				LoadNext(f);
				if (rec == 1)  return 0;
			  rec--;
			}
			fclose(f);
			return RECORDNOTFOUND;
		}
        int LoadFromFile(char *FileName,int rec)
        {
    	    //printf("-Load file\n");
	    int lf;
	    complex *xf;
            FILE *f;
            //printf("-Load file-1\n");
            int t;//int Res, t;
            //if (d != NULL) { delete[] d;  d  = NULL; }
            //if (xp != NULL){ delete[] xp; xp = NULL; }
            //if (xf != NULL){ delete[] xf; xf = NULL; }
	    //printf("Load file l0\n");
            f = fopen(FileName,"rb+");
            if (f == NULL) return CANNOTOPENFILE;
            //printf("ss\n");
            while (true){
            //printf("Load file l1\n");

                fread(&n,sizeof(int),1,f);
                fread(&dim,sizeof(int),1,f);
                fread(&lf,sizeof(int),1,f);
                t = sizeof(int)*(dim+1) + sizeof(complex)*n +
                sizeof(complex)*lf;

            //if (d != NULL) { delete[] d;  d  = NULL; }
            //if (xp != NULL){ delete[] xp; xp = NULL; }
            //if (xf != NULL){ delete[] xf; xf = NULL; }
		//printf("Load fil l2e\n");

                d  = new int[dim+1];
                xp = new complex[n];
                xf = new complex[lf];
                fread(d,sizeof(int)*(dim+1),1,f);
                fread(xp,sizeof(complex)*n,1,f);
                fread(xf,sizeof(complex)*lf,1,f);
                delete []xf;
		//printf("Load file l3\n");

                if (rec == 1) { fclose(f); return 0;}
                rec--;
            }
            return RECORDNOTFOUND;
        }
		int InitFile(char *FileName)
		{
			FILE *f;
			f = fopen(FileName,"w");
			fclose(f);
			return 0;
		}
		void FillRandom()
		{
			int i;
			for (i=0;i<n;i++) {
				xp[i].Re = double(rand());
				xp[i].Im = double(rand());
			}
		}
		void operator=(point_fd &a)
		{
			fftw_destroy_plan(fp);
			fftw_destroy_plan(bp);
			fftw_free(fxp);
			delete []d;
			dim = a.dim;
			d = new int[dim + 1];
			int i;
			for(i = 1; i <= dim ; i++)d[i] = a.d[i];
			n = a.n;
			fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
			xp = (complex*)fxp;
			for(i = 0; i < n; i++)xp[i] = a.xp[i];
			if(dim == 2){
				fp = fftw_plan_dft_2d(d[2], d[1], fxp, fxp, FFTW_FORWARD, FFTW_PATIENT);
				bp = fftw_plan_dft_2d(d[2], d[1], fxp, fxp, FFTW_BACKWARD, FFTW_PATIENT);
			}
			if(dim == 3){
				fp = fftw_plan_dft_3d(d[3], d[2], d[1], fxp, fxp, FFTW_FORWARD, FFTW_PATIENT);
				bp = fftw_plan_dft_3d(d[3], d[2], d[1], fxp, fxp, FFTW_BACKWARD, FFTW_PATIENT);
			}
		}
		point_fd(point_fd &a)
		{
			dim = a.dim;
			d = new int[dim + 1];
			int i;
			for(i = 1; i <= dim ; i++)d[i] = a.d[i];
			n = a.n;
			fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
			xp = (complex*)fxp;
			for(i = 0; i < n; i++)xp[i] = a.xp[i];
			if(dim == 2){
				fp = fftw_plan_dft_2d(d[2], d[1], fxp, fxp, FFTW_FORWARD, FFTW_PATIENT);
				bp = fftw_plan_dft_2d(d[2], d[1], fxp, fxp, FFTW_BACKWARD, FFTW_PATIENT);
			}
			if(dim == 3){
				fp = fftw_plan_dft_3d(d[3], d[2], d[1], fxp, fxp, FFTW_FORWARD, FFTW_PATIENT);
				bp = fftw_plan_dft_3d(d[3], d[2], d[1], fxp, fxp, FFTW_BACKWARD, FFTW_PATIENT);
			}
		}
		point_f reduction(int k,int *n)
		{
			point_f tmp(d[k]);
			int i;
			for(i = 0; i < d[k]; i++)
			{
				n[k] = i;
				tmp[i] = br(n);
			}
			return tmp;
		}
		void FFT(int direction)
		{
			if(direction == 1) fftw_execute(fp);
			if(direction == -1)
			{
				fftw_execute(bp);
				int i;
				for(i=0;i!=n;i++)
				{
					fxp[i][0] /= double(n);
					fxp[i][1] /= double(n);
				}
			}
		}
		void FFT_d()
		{
			FFT(1);
		}
		void FFT_b()
		{
			FFT(-1);
		}
		friend point_fd operator*(complex x, point_fd &a);
		void normalize()
		{
			double S = 0;
			int i;
			for(i = 0;i != n; i++)S += xp[i].abs_2()/double(n);
			for(i = 0;i != n; i++)xp[i] /= sqrt(S);
		}
		void normalize(double V)
		{
			double S = 0;
			int i;
			for(i = 0; i != n; i++)S += xp[i].abs_2()*(V/double(n));
			for(i = 0; i != n; i++)xp[i] /= sqrt(S);
		}
/*		double maximum()
		{
			double M = 0;
			int i;
			for(i = 0; i != n; i++) if(M*M < xp[i].abs_2()) M = abs(xp[i]);
			return M;
		}
*/		inline point_fd operator-=(point_fd &a)
		{
			int i;
			if(n == a.n)for(i = 0; i != n; i++)xp[i] -= a.xp[i];
			return *this;
		}
		//type  :
		//0 -> p.Re;
		//1 -> |p|^2;
		//style : paletter_type
		void CreateFrame(frame &F, double min, double max, int type, int style, int dx, int dy, int *n, double Rate)
		{
			frame FF(d[dx],d[dy]);
			F = FF;

			F.palette_type = style;
			complex ct;
			int x, y, col = 0;
			for(x=0;x!=d[dx];x++)for(y=0;y!=d[dy];y++)
			{
				n[dx] = x;
				n[dy] = y;
				ct = br(n);
				double z = 0;
				if(type == 0)z = ct.Re;
				if(type == 1)z = ct.abs_2();
				double X = (z - min)/(max - min);
				if((X >= 0)&&(X <= 1)){
					if(Rate == 0)col = Round(255 * X);
					if(Rate > 0)col = Round(255 * (1 - pow(1 - pow(X, 1 + Rate), 1/(1 + Rate))));
					if(Rate < 0)col = Round(255 * pow(1 - pow(1 - X, 1 - Rate), 1/(1 - Rate)));
					F.pixel(x,y) = col;
				} else F.pixel(x,y)=0;
			}
		}
		//type  :
		//0 -> p.Re;
		//1 -> |p|^2;
		//style : paletter_type
		void CreateFrame(frame &F, double min, double max, int type, int style, double Rate = 0)
		{
			int *u;u = new int[dim+1];
			for(int i=1;i<=dim;i++)u[i]=(d[i]>>1);
			CreateFrame(F,min,max,type,style,1,2,u, Rate);
			delete []u;
		}
};
inline point_fd operator*(complex x, point_fd &a)
{
    point_fd ret(a);
    int i;
    for(i = 0; i != a.n; i++)ret.xp[i] = x*a.xp[i];
    return ret;
};
class point_fdu
{
public: complex *xp;
		int n;
		int dim;
		int *d;
		int  *mind;
		int *maxd;
		fftw_complex *fxp;
		fftw_plan fp, bp;
		bool fp_created;
public:
    void Null()
    {
        memset(xp, char(0), n*sizeof(complex));
    }
	bool full()
	{
		if(dim == 2)
		{
			return ((mind[1] == 0)&&(maxd[1] == d[1] - 1)&&(mind[2] == 0)&&(maxd[2] == d[2] - 1));
		}
		if(dim == 3)
		{
			return ((mind[1] == 0)&&(maxd[1] == d[1] - 1)&&(mind[2] == 0)&&(maxd[2] == d[2] - 1)&&(mind[3] == 0)&&(maxd[3] == d[3] - 1));
		}
		return false;
	}
	virtual void Create_fftw_plans()
	{
		fp_created = false;
		if(full())
		{
			if(dim == 2)
			{
				fp = fftw_plan_dft_2d(d[2], d[1], fxp, fxp, FFTW_FORWARD, FFTW_PATIENT);
				bp = fftw_plan_dft_2d(d[2], d[1], fxp, fxp, FFTW_BACKWARD, FFTW_PATIENT);
				fp_created = true;
			}
			if(dim == 3)
			{
				fp = fftw_plan_dft_3d(d[3], d[2], d[1], fxp, fxp, FFTW_FORWARD, FFTW_PATIENT);
				bp = fftw_plan_dft_3d(d[3], d[2], d[1], fxp, fxp, FFTW_BACKWARD, FFTW_PATIENT);
				fp_created = true;
			}
		}
	}
	point_fdu(int dimension, int d1, int mind1, int maxd1, int d2, int mind2, int maxd2)
	{
		dim = dimension;
		if(dim != 2)
		{
			printf("Error: point_fdu constructor: incorrect parameter \n");
			exit(0);
		}
		d = new int[dim+1];
		mind = new int[dim+1];
		maxd = new int[dim+1];
		d[1] = d1;
		mind[1] = mind1;
		maxd[1] = maxd1;
		n = maxd1 - mind1 + 1;
		d[2] = d2;
		mind[2] = mind2;
		maxd[2] = maxd2;
		n *= maxd[2] - mind[2] + 1;
		fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
		xp = (complex*)fxp;
		Create_fftw_plans();
	}
	point_fdu(int dimension, int d1, int mind1, int maxd1, int d2, int mind2, int maxd2, int d3, int mind3, int maxd3)
	{
		dim = dimension;
		if(dim != 3)
		{
			printf("Error: point_fdu constructor: incorrect parameter \n");
			exit(0);
		}
		d = new int[dim+1];
		mind = new int[dim+1];
		maxd = new int[dim+1];
		d[1] = d1;
		mind[1] = mind1;
		maxd[1] = maxd1;
		n = maxd1 - mind1 + 1;
		d[2] = d2;
		mind[2] = mind2;
		maxd[2] = maxd2;
		n *= maxd[2] - mind[2] + 1;
		d[3] = d3;
		mind[3] = mind3;
		maxd[3] = maxd3;
		n *= maxd[3] - mind[3] + 1;
		fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
		xp = (complex*)fxp;
		Create_fftw_plans();
	}
	point_fdu()
	{
		dim = 2;
		d = new int[dim+1];
		mind = new int[dim+1];
		maxd = new int[dim+1];
		d[1] = 2;
		mind[1] = 0;
		maxd[1] = 1;
		n = maxd[1] - mind[1] + 1;
		d[2] = 2;
		mind[2] = 0;
		maxd[2] = 0;
		n *= maxd[2] - mind[2] + 1;
		fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
		xp = (complex*)fxp;
		Create_fftw_plans();
	}
	void destructor()
	{
		if(fp_created)
		{
			fftw_destroy_plan(fp);
			fftw_destroy_plan(bp);
		}
		delete []d;
		delete []mind;
		delete []maxd;
		fftw_free(fxp);
	}
	virtual ~point_fdu()
	{
		destructor();
	}
	void constructor(int dimension, int *d1, int *mind1, int *maxd1)
	{
		destructor();
		dim = dimension;
		if(dim == 2)
		{
			d = new int[dim+1];
			mind = new int[dim+1];
			maxd = new int[dim+1];
			d[1] = d1[1];
			mind[1] = mind1[1];
			maxd[1] = maxd1[1];
			n = maxd1[1] - mind1[1] + 1;
			d[2] = d1[2];
			mind[2] = mind1[2];
			maxd[2] = maxd1[2];
			n *= maxd1[2] - mind1[2] + 1;
			fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
			xp = (complex*)fxp;
			Create_fftw_plans();
		}
		if(dim == 3)
		{
			d = new int[dim+1];
			mind = new int[dim+1];
			maxd = new int[dim+1];
			d[1] = d1[1];
			mind[1] = mind1[1];
			maxd[1] = maxd1[1];
			n = maxd1[1] - mind1[1] + 1;
			d[2] = d1[2];
			mind[2] = mind1[2];
			maxd[2] = maxd1[2];
			n *= maxd1[2] - mind1[2] + 1;
			d[3] = d1[3];
			mind[3] = mind1[3];
			maxd[3] = maxd1[3];
			n *= maxd1[3] - mind1[3] + 1;
			fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
			xp = (complex*)fxp;
			Create_fftw_plans();
		}
	}
    void constructor(int dimension, int d1, int mind1, int maxd1, int d2, int mind2, int maxd2, int d3, int mind3, int maxd3)
	{
		destructor();
        dim = dimension;
		if(dim != 3)
		{
			printf("Error: point_fdu constructor: incorrect parameter \n");
			exit(0);
		}
		d = new int[dim+1];
		mind = new int[dim+1];
		maxd = new int[dim+1];
		d[1] = d1;
		mind[1] = mind1;
		maxd[1] = maxd1;
		n = maxd1 - mind1 + 1;
		d[2] = d2;
		mind[2] = mind2;
		maxd[2] = maxd2;
		n *= maxd[2] - mind[2] + 1;
		d[3] = d3;
		mind[3] = mind3;
		maxd[3] = maxd3;
		n *= maxd[3] - mind[3] + 1;
		fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
		xp = (complex*)fxp;
		Create_fftw_plans();
	}
    void constructor(int dimension, int d1, int mind1, int maxd1, int d2, int mind2, int maxd2)
	{
		destructor();
        dim = dimension;
		if(dim != 2)
		{
			printf("Error: point_fdu constructor: incorrect parameter \n");
			exit(0);
		}
		d = new int[dim+1];
		mind = new int[dim+1];
		maxd = new int[dim+1];
		d[1] = d1;
		mind[1] = mind1;
		maxd[1] = maxd1;
		n = maxd1 - mind1 + 1;
		d[2] = d2;
		mind[2] = mind2;
		maxd[2] = maxd2;
		n *= maxd[2] - mind[2] + 1;
		fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
		xp = (complex*)fxp;
		Create_fftw_plans();
	}
	void constructor_nofft(int dimension, int d1, int mind1, int maxd1, int d2, int mind2, int maxd2, int d3, int mind3, int maxd3)
	{
		destructor();
        dim = dimension;
		if(dim != 3)
		{
			printf("Error: point_fdu constructor: incorrect parameter \n");
			exit(0);
		}
		d = new int[dim+1];
		mind = new int[dim+1];
		maxd = new int[dim+1];
		d[1] = d1;
		mind[1] = mind1;
		maxd[1] = maxd1;
		n = maxd1 - mind1 + 1;
		d[2] = d2;
		mind[2] = mind2;
		maxd[2] = maxd2;
		n *= maxd[2] - mind[2] + 1;
		d[3] = d3;
		mind[3] = mind3;
		maxd[3] = maxd3;
		n *= maxd[3] - mind[3] + 1;
		fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
		xp = (complex*)fxp;
		//Create_fftw_plans();
	}
	//function returns true only if a point (x, y)/(x, y, z) is available for the matrix
	bool RightIndex(int x, int y)
	{
		if((x < mind[1])||(x > maxd[1])) return false;
		if((y < mind[2])||(y > maxd[2])) return false;
		return true;
	}
	bool RightIndex(int x, int y, int z)
	{
		if((x < mind[1])||(x > maxd[1])) return false;
		if((y < mind[2])||(y > maxd[2])) return false;
		if((z < mind[3])||(y > maxd[3])) return false;
		return true;
	}
	//function for matrix saving to the end of a file
	int	AppendToFile(char *FileName)
	{
		FILE *f;
		f = fopen(FileName,"ab+");
		if (f == NULL) return CANNOTOPENFILE;
		fwrite(&n,sizeof(int),1,f);
		fwrite(&dim,sizeof(int),1,f);
		fwrite(d,sizeof(int)*(dim+1),1,f);
		fwrite(mind,sizeof(int)*(dim+1),1,f);
		fwrite(maxd,sizeof(int)*(dim+1),1,f);
		fwrite(xp,sizeof(complex)*n,1,f);
		fclose(f);
		return 0;
	}
	int LoadNext(FILE *f)
	{
		if (f == NULL) return CANNOTOPENFILE;
		fread(&n,sizeof(int),1,f);
		fread(&dim,sizeof(int),1,f);
		destructor();
		d  = new int[dim+1];
		mind  = new int[dim+1];
		maxd  = new int[dim+1];
		fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
		xp = (complex*)fxp;
		fread(d,sizeof(int)*(dim+1),1,f);
		fread(mind,sizeof(int)*(dim+1),1,f);
		fread(maxd,sizeof(int)*(dim+1),1,f);
		fread(xp,sizeof(complex)*n,1,f);
		Create_fftw_plans();
		return RECORDNOTFOUND;
	}
	//function for loading a rec-th matrix from a file
	int LoadFromFile_(char *FileName,int rec)
	{
		FILE *f;
		f = fopen(FileName,"rb+");
		if (f == NULL) return CANNOTOPENFILE;
		while (true)
		{
			LoadNext(f);
			if (rec == 1)  return 0;
			rec--;
		}
		fclose(f);
		return RECORDNOTFOUND;
	}
    int     LoadFromFile(char *FileName,int rec)
    {
        FILE *f;
        int lf;
        complex *xf;
        int t;//int Res, t;
        if (d != NULL) { delete[] d;  d  = NULL; }
        if (mind != NULL) { delete[] mind;  mind  = NULL; }
        if (maxd != NULL) { delete[] maxd;  maxd  = NULL; }
        if (xp != NULL){ delete[] xp; xp = NULL; }
        if (xf != NULL){ delete[] xf; xf = NULL; }

        f = fopen(FileName,"rb+");
        if (f == NULL) return CANNOTOPENFILE;
        while (true){
            fread(&n,sizeof(int),1,f);
            fread(&dim,sizeof(int),1,f);
            fread(&lf,sizeof(int),1,f);
            t = sizeof(int)*(dim+1) + sizeof(complex)*n +
            sizeof(complex)*lf;

        if (d != NULL) { delete[] d;  d  = NULL; }
        if (mind != NULL) { delete[] mind;  mind  = NULL; }
        if (maxd != NULL) { delete[] maxd;  maxd  = NULL; }
        if (xp != NULL){ delete[] xp; xp = NULL; }
        if (xf != NULL){ delete[] xf; xf = NULL; }

            d  = new int[dim+1];
            mind  = new int[dim+1];
            maxd  = new int[dim+1];
            xp = new complex[n];
            xf = new complex[lf];
            fread(d,sizeof(int)*(dim+1),1,f);
            fread(mind,sizeof(int)*(dim+1),1,f);
            fread(maxd,sizeof(int)*(dim+1),1,f);
            fread(xp,sizeof(complex)*n,1,f);
            fread(xf,sizeof(complex)*lf,1,f);
            delete []xf;
            if (rec == 1)  return 0;
            rec--;
        }
        return RECORDNOTFOUND;
    }
	int InitFile(char *FileName)
	{
		FILE *f;
		f = fopen(FileName,"w");
		fclose(f);
		return 0;
	}
	void FillRandom()
	{
		int i;
		for (i = 0; i < n; i++)
		{
			xp[i].Re = double(rand());
			xp[i].Im = double(rand());
		}
	}
	point_fdu(point_fdu &a)
	{
		dim = a.dim;
		d = new int[dim+1];
		mind = new int[dim+1];
		maxd = new int[dim+1];
		int i;
		for(i = 1; i <= dim; i++)
		{
			d[i] = a.d[i];
			mind[i] = a.mind[i];
			maxd[i] = a.maxd[i];
		}
		n = a.n;
		fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
		xp = (complex*)fxp;
		for(i = 0; i < n; i++)xp[i] = a.xp[i];
		Create_fftw_plans();
	}
	void operator=(point_fdu &a)
	{
		destructor();
		dim = a.dim;
		d = new int[dim+1];
		mind = new int[dim+1];
		maxd = new int[dim+1];
		int i;
		for(i = 1; i <= dim; i++)
		{
			d[i] = a.d[i];
			mind[i] = a.mind[i];
			maxd[i] = a.maxd[i];
		}
		n = a.n;
		fxp = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * n);
		xp = (complex*)fxp;
		for(i = 0; i < n; i++)xp[i] = a.xp[i];
		Create_fftw_plans();
	}
	complex& operator()(int x, int y)
	{
		return xp[x - mind[1] + (y - mind[2])*(maxd[1] - mind[1] + 1)];
	}
	virtual complex& operator()(int x, int y, int z)
	{
		return xp[x - mind[1] + (y - mind[2])*(maxd[1] - mind[1] + 1) + (z - mind[3])*(maxd[1] - mind[1] + 1)*(maxd[2] - mind[2] + 1)];
	}
	complex& bracket(int x, int y)
	{
		return xp[x - mind[1] + (y - mind[2])*(maxd[1] - mind[1] + 1)];
	}
	virtual complex& bracket(int x, int y, int z)
	{
		return xp[x - mind[1] + (y - mind[2])*(maxd[1] - mind[1] + 1) + (z - mind[3])*(maxd[1] - mind[1] + 1)*(maxd[2] - mind[2] + 1)];
	}
	complex get_value(double x)
	{
	    if(double(int(x)) == x) return bracket(int(x), 0);
	    return  bracket(int(x)+1, 0)*(x-int(x)) +
				bracket(int(x), 0)*(int(x)+1-x);
	}
	complex get_value(double x, double y)
	{
	    if((double(int(x)) == x)&&(double(int(y)) == y)) return bracket(int(x),int(y));
	    if(double(int(x)) == x)
	    return  bracket(int(x),int(y)+1)*(int(x)+1-x)*(y-int(y)) +
				bracket(int(x),int(y))*(int(x)+1-x)*(int(y)+1-y);
		if(double(int(y)) == y)
		return  bracket(int(x)+1,int(y))*(x-int(x))*(int(y)+1-y) +
				bracket(int(x),int(y))*(int(x)+1-x)*(int(y)+1-y);

	    return  bracket(int(x)+1,int(y)+1)*(x-int(x))*(y-int(y)) +
				bracket(int(x)+1,int(y))*(x-int(x))*(int(y)+1-y) +
				bracket(int(x),int(y)+1)*(int(x)+1-x)*(y-int(y)) +
				bracket(int(x),int(y))*(int(x)+1-x)*(int(y)+1-y);
	}
	complex get_value(double x, double y, double z)
	{
	    if((double(int(x)) == x)&&(double(int(y)) == y)&&(double(int(z)) == z)) return bracket(int(x), int(y), int(z));
		if((double(int(x)) == x)&&(double(int(y)) == y))
		return  bracket(int(x),int(y),int(z)+1)*(int(x)+1-x)*(int(y)+1-y)*(z-int(z)) +
                bracket(int(x),int(y),int(z))*(int(x)+1-x)*(int(y)+1-y)*(int(z)+1-z);
        if((double(int(x)) == x)&&(double(int(z)) == z))
        return  bracket(int(x),int(y)+1,int(z))*(int(x)+1-x)*(y-int(y))*(int(z)+1-z) +
				bracket(int(x),int(y),int(z))*(int(x)+1-x)*(int(y)+1-y)*(int(z)+1-z);
        if((double(int(y)) == y)&&(double(int(z)) == z))
				bracket(int(x)+1,int(y),int(z))*(x-int(x))*(int(y)+1-y)*(int(z)+1-z) +
				bracket(int(x),int(y),int(z))*(int(x)+1-x)*(int(y)+1-y)*(int(z)+1-z);
        if(double(int(x)) == x)
        return  bracket(int(x),int(y)+1,int(z)+1)*(int(x)+1-x)*(y-int(y))*(z-int(z)) +
				bracket(int(x),int(y),int(z)+1)*(int(x)+1-x)*(int(y)+1-y)*(z-int(z)) +
				bracket(int(x),int(y)+1,int(z))*(int(x)+1-x)*(y-int(y))*(int(z)+1-z) +
				bracket(int(x),int(y),int(z))*(int(x)+1-x)*(int(y)+1-y)*(int(z)+1-z);
        if(double(int(y)) == y)
        return  bracket(int(x)+1,int(y),int(z)+1)*(x-int(x))*(int(y)+1-y)*(z-int(z)) +
				bracket(int(x),int(y),int(z)+1)*(int(x)+1-x)*(int(y)+1-y)*(z-int(z)) +
				bracket(int(x)+1,int(y),int(z))*(x-int(x))*(int(y)+1-y)*(int(z)+1-z) +
				bracket(int(x),int(y),int(z))*(int(x)+1-x)*(int(y)+1-y)*(int(z)+1-z);
        if(double(int(z)) == z)
        return  bracket(int(x)+1,int(y)+1,int(z))*(x-int(x))*(y-int(y))*(int(z)+1-z) +
				bracket(int(x)+1,int(y),int(z))*(x-int(x))*(int(y)+1-y)*(int(z)+1-z) +
				bracket(int(x),int(y)+1,int(z))*(int(x)+1-x)*(y-int(y))*(int(z)+1-z) +
				bracket(int(x),int(y),int(z))*(int(x)+1-x)*(int(y)+1-y)*(int(z)+1-z);

		return  bracket(int(x)+1,int(y)+1,int(z)+1)*(x-int(x))*(y-int(y))*(z-int(z)) +
				bracket(int(x)+1,int(y),int(z)+1)*(x-int(x))*(int(y)+1-y)*(z-int(z)) +
				bracket(int(x),int(y)+1,int(z)+1)*(int(x)+1-x)*(y-int(y))*(z-int(z)) +
				bracket(int(x),int(y),int(z)+1)*(int(x)+1-x)*(int(y)+1-y)*(z-int(z)) +
				bracket(int(x)+1,int(y)+1,int(z))*(x-int(x))*(y-int(y))*(int(z)+1-z) +
				bracket(int(x)+1,int(y),int(z))*(x-int(x))*(int(y)+1-y)*(int(z)+1-z) +
				bracket(int(x),int(y)+1,int(z))*(int(x)+1-x)*(y-int(y))*(int(z)+1-z) +
				bracket(int(x),int(y),int(z))*(int(x)+1-x)*(int(y)+1-y)*(int(z)+1-z);
	}
	virtual complex& br(int *n)
	{
		if(dim == 2)
		{
			return xp[n[1] - mind[1] + (n[2] - mind[2])*(maxd[1] - mind[1] + 1)];
		}
		if(dim == 3)
		{
			return xp[n[1] - mind[1] + (n[2] - mind[2])*(maxd[1] - mind[1] + 1) + (n[3] - mind[3])*(maxd[1] - mind[1] + 1)*(maxd[2] - mind[2] + 1)];
		}
		printf("Error: incorrect index(& br(int *n))\n");
		return xp[0];
	}
	point_f reduction(int k,int *n)
	{
		point_f tmp(maxd[k] - mind[k] + 1);
		int i;
		for(i = mind[k]; i < maxd[k] + 1; i++)
		{
			n[k] = i;
			tmp[i] = br(n);
		}
		return tmp;
	}
	void FFT(int direction)
	{
		if(full())
		{
			if(direction == 1)fftw_execute(fp);
			if(direction == -1)
			{
				fftw_execute(bp);
				int i;
				double n_1 = 1/double(n);
				for(i = 0; i != n; i++)
				{
					xp[i].Re *= n_1;
					xp[i].Im *= n_1;
				}
			}
		} else printf("Error: point_fdu: can't call FFT() for not full matrix.\n");
	}
	void FFT_d()
	{
		FFT(1);
	}
	void FFT_b()
	{
		FFT(-1);
	}
	void Nulling(int from,int *ind)
    {
        ind[0] = 0;
        int i;
        for(i = from; i <= dim; i++)ind[i] = mind[i];
    }
    void Next(int from,int *ind)
    {
        if(ind[0] == 2)
        {
            ind[0] = 1;
        } else
        {
            int p = 1;
            int J;
            for(J = dim; ((J >= from)&&(p == 1)); J--)
            {
                ind[J]++;
                p = 0;
                if(ind[J] > maxd[J])
                {
                    ind[J] = mind[J];
                    p = 1;
                }
            }
            bool an = true;
            int i;
            for(i = from; ((i <= dim)&&(an)); i++)if(ind[i] < maxd[i])an = false;
            if(an)ind[0] = 2;
        }
    }
	friend point_fd operator*(complex x,point_fd a);
	void normalize()
	{
		double S = 0;
		int i;
		for(i = 0; i != n; i++)S += xp[i].abs_2()/double(n);
		for(i = 0; i != n; i++)xp[i] /= sqrt(S);
	}
	void normalize(double V)
	{
		double S = 0;
		int i;
		for(i = 0; i != n; i++)S += xp[i].abs_2()*(V/double(n));
		for(i = 0; i != n; i++)xp[i] /= sqrt(S);
	}
	//type  :
	//0 -> p.Re;
	//1 -> |p|^2;
	//style : paletter_type
	void CreateFrame(frame &F, double min, double max, int type, int style, int dx, int dy, int *n, double Rate)
	{
		frame FF(maxd[dx] - mind[dx] + 1, maxd[dy] - mind[dy] + 1);
		F = FF;
		F.palette_type = style;
		complex ct;
		int x, y, col = 0;
		for(x = mind[dx];x != maxd[dx] + 1; x++)for(y = mind[dy]; y != maxd[dy] + 1; y++)
		{
			n[dx] = x;
			n[dy] = y;
			ct = br(n);
			double z = 0;
			if(type == 0)z = ct.Re;
			if(type == 1)z = ct.abs_2();
			double X = (z - min)/(max - min);
			if((X >= 0)&&(X <= 1)){
				if(Rate == 0)col = Round(254 * X);
				if(Rate > 0)col = Round(254 * (1 - pow(1 - pow(X, 1 + Rate), 1/(1 + Rate))));
				if(Rate < 0)col = Round(254 * pow(1 - pow(1 - X, 1 - Rate), 1/(1 - Rate)));
				F.pixel(x - mind[dx],y - mind[dy]) = col + 1;
			} else F.pixel(x - mind[dx], y - mind[dy]) = 1;
		}
	}
	//type  :
	//0 -> p.Re;
	//1 -> |p|^2;
	//style : paletter_type
	void CreateFrame(frame &F, double min, double max, int type, int style, double Rate = 0)
	{
		int *u;
		u = new int[dim + 1];
		int i;
		for(i = 1; i <= dim; i++)u[i] = (d[i]>>1);
		CreateFrame(F, min, max, type, style, 1, 2, u, Rate);
		delete []u;
	}
	void CreateFrame_(frame &F, double min, double max, int type, int style, double Rate = 0)
	{
		int *u;
		u = new int[dim + 1];
		int i;
		for(i = 1; i <= dim; i++)u[i] = (d[i]>>1);
		CreateFrame(F, min, max, type, style, 1, 2, u, Rate);
		delete []u;
	}
    void CopyFrom(point_fdu &a)
    {
        int *n;
        n = new int[dim+1];
        for(int i=1;i<=dim;i++)n[i]=mind[i];
        n[0]=0;
        int k=0,p=0;
        mind[0]=0;maxd[0]=1;
        for(;n[0]==0;){
            bool can=true;
            for(int s=1;s<=dim;s++)if((n[s]<a.mind[s])||(n[s]>a.maxd[s]))can=false;
            if(can)br(n)=a.br(n);
            p=1;
            for(k=dim;k>=0;k--){
            if(p==1){
            n[k]++;
            p=0;
            if(n[k]>maxd[k]){
            n[k]=mind[k];
            p=1;
            }
            }
            }
        }
        delete []n;
    }
    void FillCrossPart(point_fdu &a)
    {
        int *ind;
        ind = new int[dim+1];
        for(Nulling(1,ind);ind[0]!=1;Next(1,ind)){
            bool can=true;
            for(int s=1;s<=dim;s++)if((ind[s]<a.mind[s])||(ind[s]>a.maxd[s]))can=false;
            if(can)br(ind)=a.br(ind);
        }
        delete []ind;
    }
    void PrepareIndex(int *index,int x)
    {
        index[0]=2;index[1]=x;
        for(int i=2;i<=dim;i++)index[i]=0;
    }
};
inline point_fd operator*(complex x,point_fd a)
{
    int i;
    for(i = 0; i != a.n; i++) a.xp[i] *= x;
    return a;
}
inline void s_mp_t(complex *xp,long int N,int d)
		{
			int nd2 = (N>>1);
			int logn = 0;
			int tmpn = N;
			while (tmpn > 1) {logn++; tmpn = (tmpn>>1); }
			int i, j, k, le, le2, ip;
			double sr, si, ur, ui, tr, ti;
			complex tc;
			for (i = 1, j = nd2; i < N-1; i++)
			{
				if (i < j)	{ tc = xp[j]; xp[j] = xp[i]; xp[i] = tc;}
				k = nd2;
				while (k <= j) { j -= k; k = (k >> 1); }
				j += k;
			}
			for (int l = 1; l <= logn; l++)
			{
				le = 1 << l; le2 = le >> 1;
				ur = 1.0; ui = 0.0;
				sr = cos(M_PI/le2); si = -d*sin (M_PI/le2);
				for (j = 1; j <= le2; j++)
				{
					for (i = j-1; i < N; i+= le)
					{
						ip = i + le2;
						tr = (xp[ip]).Re * ur - (xp[ip]).Im * ui;
						ti = (xp[ip]).Re * ui + (xp[ip]).Im * ur;
						(xp[ip]).Re = double((xp[i]).Re - tr);
						(xp[ip]).Im = double((xp[i]).Im - ti);
						(xp[i]).Re += double(tr);
						(xp[i]).Im += double(ti);
					}
					tr = ur;
					ur = tr * sr - ui * si;
					ui = tr * si + ui * sr;
				}
			}
		};
class point_fdux : public point_fdu{
public:
	fftw_plan fpX;
	fftw_plan bpX;
public:
	void Create_fftw_plans()
	{
	    if((mind[1] != 0)||(maxd[1] != d[1] - 1))
	    {
            //printf("point_fdux_ constructor incorrect parameter\n");
	    } else {
            int fft_number = (maxd[2] - mind[2] + 1)*(maxd[3] - mind[3] + 1);
            fpX = fftw_plan_many_dft(1, d + 1, fft_number,
                                fxp, d + 1, fft_number, 1,
                                fxp, d + 1, fft_number, 1,
                                FFTW_FORWARD, FFTW_PATIENT);
            bpX = fftw_plan_many_dft(1, d + 1, fft_number,
                                fxp, d + 1, fft_number, 1,
                                fxp, d + 1, fft_number, 1,
                                FFTW_BACKWARD, FFTW_PATIENT);
	    }
	}
	void destructor_fdux()
	{
		fftw_destroy_plan(fpX);
		fftw_destroy_plan(bpX);
	}
	~point_fdux()
	{
		destructor_fdux();
	}
	point_fdux(int dimension = 3, int d1 = 2, int mind1 = 0, int maxd1 = 1, int d2 = 2, int mind2 = 0, int maxd2 = 1, int d3 = 2, int mind3 = 0, int maxd3 = 1)
	{
		int d[4];
		int mind[4];
		int maxd[4];
		d[1] = d1;
		d[2] = d2;
		d[3] = d3;
		mind[1] = mind1;
		mind[2] = mind2;
		mind[3] = mind3;
		maxd[1] = maxd1;
		maxd[2] = maxd2;
		maxd[3] = maxd3;
		constructor(dimension, d, mind, maxd);
	}
	inline int xp_index(int x, int y, int z)
	{
	    return z - mind[3] + (y - mind[2])*(maxd[3] - mind[3] + 1) + (x - mind[1])*(maxd[3] - mind[3] + 1)*(maxd[2] - mind[2] + 1);
	}
	complex& operator()(int x, int y, int z)
	{
		return xp[z - mind[3] + (y - mind[2])*(maxd[3] - mind[3] + 1) + (x - mind[1])*(maxd[3] - mind[3] + 1)*(maxd[2] - mind[2] + 1)];
	}
	complex& bracket(int x, int y, int z)
	{
		return xp[z - mind[3] + (y - mind[2])*(maxd[3] - mind[3] + 1) + (x - mind[1])*(maxd[3] - mind[3] + 1)*(maxd[2] - mind[2] + 1)];
	}
	complex& br(int *n)
	{
		if(dim == 3)
		{
			return xp[n[3] - mind[3] + (n[2] - mind[2])*(maxd[3] - mind[3] + 1) + (n[1] - mind[1])*(maxd[3] - mind[3] + 1)*(maxd[2] - mind[2] + 1)];
		}
		printf("Error: point_fduz: incorrect index (& br(int *n))\n");
		return xp[0];
	}
    void FFTx(int direction)
	{
		if(direction == 1) fftw_execute(fpX);
		if(direction == -1)
		{
			fftw_execute(bpX);
			int i;
			for(i = 0; i != n; i++)
			{
				fxp[i][0] /= double(d[1]);
				fxp[i][1] /= double(d[1]);
			}
		}
	}
};
class point_fduz : public point_fdu{
public:
	fftw_plan fpZ;
	fftw_plan bpZ;
	fftw_plan fpY;
	fftw_plan bpY;
	int Count;
	int FFTy_K_counter;
public:
	void Create_fftw_plans()
	{
		Count = d[1]/(maxd[1] - mind[1] + 1);
		int fft_numberZ = d[2]/Count;
		int fft_numberY = d[3]/Count;
		fpZ = fftw_plan_many_dft(1, d + 3, fft_numberZ,
							fxp, d + 3, 1, d[3],
							fxp, d + 3, 1, d[3],
							FFTW_FORWARD, FFTW_PATIENT);
		bpZ = fftw_plan_many_dft(1, d + 3, fft_numberZ,
							fxp, d + 3, 1, d[3],
							fxp, d + 3, 1, d[3],
							FFTW_BACKWARD, FFTW_PATIENT);
		fpY = fftw_plan_many_dft(1, d + 2, fft_numberY,
							fxp, d + 2, d[3], 1,
							fxp, d + 2, d[3], 1,
							FFTW_FORWARD, FFTW_PATIENT);
		bpY = fftw_plan_many_dft(1, d + 2, fft_numberY,
							fxp, d + 2, d[3], 1,
							fxp, d + 2, d[3], 1,
							FFTW_BACKWARD, FFTW_PATIENT);

	}
	void destructor_fduz()
	{
		fftw_destroy_plan(fpY);
		fftw_destroy_plan(bpY);
		fftw_destroy_plan(fpZ);
		fftw_destroy_plan(bpZ);
	}
	~point_fduz()
	{
		destructor_fduz();
	}
	point_fduz(int dimension = 3, int d1 = 2, int mind1 = 0, int maxd1 = 1, int d2 = 2, int mind2 = 0, int maxd2 = 1, int d3 = 2, int mind3 = 0, int maxd3 = 1)
	{
		int d[4];
		int mind[4];
		int maxd[4];
		d[1] = d1;
		d[2] = d2;
		d[3] = d3;
		mind[1] = mind1;
		mind[2] = mind2;
		mind[3] = mind3;
		maxd[1] = maxd1;
		maxd[2] = maxd2;
		maxd[3] = maxd3;
		constructor(dimension, d, mind, maxd);
		FFTy_K_counter = 0;
	}
	complex& operator()(int x, int y, int z)
	{
		return xp[z - mind[3] + (y - mind[2])*(maxd[3] - mind[3] + 1) + (x - mind[1])*(maxd[3] - mind[3] + 1)*(maxd[2] - mind[2] + 1)];
	}
	complex& bracket(int x, int y, int z)
	{
		return xp[z - mind[3] + (y - mind[2])*(maxd[3] - mind[3] + 1) + (x - mind[1])*(maxd[3] - mind[3] + 1)*(maxd[2] - mind[2] + 1)];
	}
	complex& br(int *n)
	{
		if(dim == 3)
		{
			return xp[n[3] - mind[3] + (n[2] - mind[2])*(maxd[3] - mind[3] + 1) + (n[1] - mind[1])*(maxd[3] - mind[3] + 1)*(maxd[2] - mind[2] + 1)];
		}
		printf("Error: point_fduz: incorrect index (& br(int *n))\n");
		return xp[0];
	}
	int FFTy_K(int *n, int aP, int direction)
	{
		if(aP != 2 * Count)
		{
			printf("Error: point_fduz: incorrect aP value.\n");
			return 1;
		}
		if(FFTy_K_counter < Count){
			if(direction == 1){
				fftw_execute_dft(fpZ, fxp + d[2]*d[3]*(n[1] - mind[1]) + FFTy_K_counter*d[3]*d[2]/Count,
									  fxp + d[2]*d[3]*(n[1] - mind[1]) + FFTy_K_counter*d[3]*d[2]/Count);
			}
			if(direction == -1)
			{
				fftw_execute_dft(bpZ, fxp + d[2]*d[3]*(n[1] - mind[1]) + FFTy_K_counter*d[3]*d[2]/Count,
									  fxp + d[2]*d[3]*(n[1] - mind[1]) + FFTy_K_counter*d[3]*d[2]/Count);
				int i;
				for(i = 0; i != d[3]*d[2]/Count; i++)
				{
					fxp[d[2]*d[3]*(n[1] - mind[1]) + FFTy_K_counter*d[3]*d[2]/Count + i][0] /= d[3];
					fxp[d[2]*d[3]*(n[1] - mind[1]) + FFTy_K_counter*d[3]*d[2]/Count + i][1] /= d[3];
				}
			}
		} else {
			if(direction == 1){
				fftw_execute_dft(fpY, fxp + d[2]*d[3]*(n[1] - mind[1]) + (FFTy_K_counter - Count)*d[3]/Count,
									  fxp + d[2]*d[3]*(n[1] - mind[1]) + (FFTy_K_counter - Count)*d[3]/Count);
			}
			if(direction == -1)
			{
				fftw_execute_dft(bpY, fxp + d[2]*d[3]*(n[1] - mind[1]) + (FFTy_K_counter - Count)*d[3]/Count,
									  fxp + d[2]*d[3]*(n[1] - mind[1]) + (FFTy_K_counter - Count)*d[3]/Count);
				int y;
				int z;
				for(y = 0; y != d[2]; y++)
					for(z = 0; z != d[3]/Count; z++)
					{
						bracket(n[1], y, z + (FFTy_K_counter - Count)*d[3]/Count) /= d[2];
					}
			}
		}
		FFTy_K_counter++;
		if(FFTy_K_counter == 2 * Count) FFTy_K_counter = 0;
		return 0;
	}
};
class image
{
public:
	int Nx;
	int Ny;
	byte *v;
	int Xmin, Xmax, Ymin, Ymax;
public:
    image(int nx = 4, int ny = 4)
	{
	    if((nx%4 != 0)||(ny%4 != 0))printf("Class 'image' warning: The image sizes must be numbers divisible by 4.\n");
		Nx = nx;
		Ny = ny;
		v = new byte[Nx*Ny*3];
		Xmin = 0;
		Ymin = 0;
		Xmax = Nx;
		Ymax = Ny;
	}
	void transpose()
	{
	    byte *t;
	    t = new byte[Nx*Ny*3];
	    memcpy(t, v, Nx*Ny*3);
	    int nx = Nx;
	    int ny = Ny;
	    Nx = ny;
	    Ny = nx;
	    int x, y;
	    for(y = 0; y < Ny; y++)for(x = 0; x < Nx; x++)
	    {
            v[3*(Nx*(Ny - y - 1) + x)] = t[3*(nx*(ny - x - 1) + y)];
            v[3*(Nx*(Ny - y - 1) + x) + 1] = t[3*(nx*(ny - x - 1) + y) + 1];
            v[3*(Nx*(Ny - y - 1) + x) + 2] = t[3*(nx*(ny - x - 1) + y) + 2];
	    }
	    delete []t;
	    int Xmin_, Xmax_, Ymin_, Ymax_;
	    Xmin_ = Xmin;
	    Xmax_ = Xmax;
	    Ymin_ = Ymin;
	    Ymax_ = Ymax;

	    Xmin = Ymin_;
	    Xmax = Ymax_;
	    Ymin = Xmin_;
	    Ymax = Xmax_;
	}
	void reflectY()
	{
	    byte *t;
	    t = new byte[Nx*Ny*3];
	    memcpy(t, v, Nx*Ny*3);
	    int x, y;
	    for(y = 0; y < Ny; y++)for(x = 0; x < Nx; x++)
	    {
            v[3*(Nx*(Ny - y - 1) + x)] = t[3*(Nx*(y) + x)];
            v[3*(Nx*(Ny - y - 1) + x) + 1] = t[3*(Nx*(y) + x) + 1];
            v[3*(Nx*(Ny - y - 1) + x) + 2] = t[3*(Nx*(y) + x) + 2];
	    }
	    delete []t;
	}
	void constructor(int nx, int ny)
	{
	    delete []v;
	    if((nx%4 != 0)||(ny%4 != 0))printf("Class 'image' warning: The image sizes must be numbers divisible by 4.\n");
		Nx = nx;
		Ny = ny;
		v = new byte[Nx*Ny*3];
		Xmin = 0;
		Ymin = 0;
		Xmax = Nx;
		Ymax = Ny;
	}
    ~image()
	{
		delete []v;
	}
	bool SetFrame(int xmin, int ymin, int xmax, int ymax)
	{
	    if((xmin < 0)||(xmin > xmax)||(xmax > Nx)) return false;
	    if((ymin < 0)||(ymin > ymax)||(ymax > Ny)) return false;
	    Xmin = xmin;
	    Ymin = ymin;
	    Xmax = xmax;
	    Ymax = ymax;
	    return true;
	}
	bool SetFrame_(int xD, int yD, int nD, int NX = -1, int NY = -1)
	{
	    int Nx_ = NX;if(Nx_ == -1)Nx_ = Nx;
	    int Ny_ = NY;if(Ny_ == -1)Ny_ = Ny;
	    int x = nD%xD;
	    int y = (nD - x)/xD;
	    if(y >= yD) return false;
	    SetFrame(int(x*Nx_/double(xD)), int(y*Ny_/double(yD)), int((x + 1)*Nx_/double(xD)), int((y + 1)*Ny_/double(yD)));
	}
	int print_info()
	{
	    printf("image [%i x %i], current frame: (%i, %i, %i, %i)\n", Nx, Ny, Xmin, Ymin, Xmax, Ymax);
	    return 0;
	}
	image(image &a)
	{
		Nx = a.Nx;
		Ny = a.Ny;
		v = new byte[Nx*Ny*3];
		memcpy(v, a.v, Nx*Ny*3);
		Xmin = a.Xmin;
		Ymin = a.Ymin;
		Xmax = a.Xmax;
		Ymax = a.Ymax;
	}
	void operator=(image &a)
	{
		delete []v;
		Nx = a.Nx;
		Ny = a.Ny;
		v = new byte[Nx*Ny*3];
		memcpy(v, a.v, Nx*Ny*3);
		Xmin = a.Xmin;
		Ymin = a.Ymin;
		Xmax = a.Xmax;
		Ymax = a.Ymax;
	}
	int PutPixel(int x, int y, int r, int g, int b)
	{
	    if((x >= Xmin)&&(x < Xmax)&&(y >= Ymin)&&(y < Ymax))
	    {
            v[3*(Nx*(Ny - y - 1) + x)]     = b;
			v[3*(Nx*(Ny - y - 1) + x) + 1] = g;
			v[3*(Nx*(Ny - y - 1) + x) + 2] = r;
	        return 0;
	    } else return 1;
	}
	int GetPixel(int x, int y, int &r, int &g, int &b)
	{
	    if((x >= Xmin)&&(x < Xmax)&&(y >= Ymin)&&(y < Ymax))
	    {
            b = v[3*(Nx*(Ny - y - 1) + x)];
			g = v[3*(Nx*(Ny - y - 1) + x) + 1];
			r = v[3*(Nx*(Ny - y - 1) + x) + 2];
	        return 0;
	    } else return 1;
	}
	int Spot(int x, int y, int r, int g, int b, double intensity)
	{
	    if((x >= Xmin)&&(x < Xmax)&&(y >= Ymin)&&(y < Ymax))
	    {
	        double Intensity = 1 - exp(-intensity);
	        if(Intensity > 1)Intensity = 1;
	        if(Intensity < 0)Intensity = 0;
	        int i = 3*(Nx*(Ny - y - 1) + x);
            v[i]     = int(v[i] + (b - v[i])*Intensity);
			v[i + 1] = int(v[i + 1] + (g - v[i + 1])*Intensity);
			v[i + 2] = int(v[i + 2] + (r - v[i + 2])*Intensity);
	        return 0;
	    } else return 1;
	}
	int SpotW(int x, int y, int r, int g, int b)
	{
	    if((x >= Xmin)&&(x < Xmax)&&(y >= Ymin)&&(y < Ymax))
	    {
	        int i = 3*(Nx*(Ny - y - 1) + x);
            v[i]     = int(double(v[i])*double(b)/255.0);
			v[i + 1] = int(double(v[i + 1])*double(g)/255.0);
			v[i + 2] = int(double(v[i + 2])*double(r)/255.0);
	        return 0;
	    } else return 1;
	}
	int Spot(double x, double y, int r, int g, int b, double intensity)
	{
	    Spot(int(x), int(y), r, g, b, intensity*(1 - x + int(x))*(1 - y + int(y)));
	    Spot(int(x) + 1, int(y), r, g, b, intensity*(x - int(x))*(1 - y + int(y)));
	    Spot(int(x), int(y) + 1, r, g, b, intensity*(1 - x + int(x))*(y - int(y)));
	    Spot(int(x) + 1, int(y) + 1, r, g, b, intensity*(x - int(x))*(y - int(y)));
	    return 0;
	}
	void triangle(double *x, double *y, int r, int g, int b)
	{
	    double X0, Y0, X1, Y1, X2, Y2;
	    Y0 = y[0];
	    Y2 = y[2];
	    int i, i_min, i_max, i_mid;
	    i_min = 0;
	    i_max = 2;
	    for(i = 0; i != 3; i++)
	    {
	        if(y[i] < Y0)
	        {
	            i_min = i;
	            Y0 = y[i];
	        }
	        if(y[i] > Y2)
	        {
	            i_max = i;
	            Y2 = y[i];
	        }
	    }
	    if((i_min != 0)&&(i_max != 0)) i_mid = 0;
	    if((i_min != 1)&&(i_max != 1)) i_mid = 1;
	    if((i_min != 2)&&(i_max != 2)) i_mid = 2;

	    X0 = x[i_min]; Y0 = y[i_min];
	    X1 = x[i_mid]; Y1 = y[i_mid];
	    X2 = x[i_max]; Y2 = y[i_max];

	    if(Y2 == Y0) Y2 += 0.001;
	    int k;
	    int shift;
	    int beg, end;
	    if((X0*Y2 - X0*Y1 + X2*Y1 - X2*Y0)/(Y2 - Y0) < X1) shift = 1; else shift = -1;
	    for(i = int(Y0); i <= int(Y2); i++)
	    {
	        beg = int((X0*Y2 - X0*i + X2*i - X2*Y0)/(Y2 - Y0));
            if(i <= Y1)
            {
                for(k = beg; ((X0*Y1 - X0*i + X1*i - X1*Y0)/(Y1 - Y0) - k)*shift > 0; k += shift)
                    PutPixel(k, i, r, g, b);
            }
            if(i >= Y1)
            {
                for(k = beg; ((X1*Y2 - X1*i + X2*i - X2*Y1)/(Y2 - Y1) - k)*shift > 0; k += shift)
                    PutPixel(k, i, r, g, b);
            }
	    }
	}
	void triangle(double x0, double y0, double x1, double y1, double x2, double y2, int r, int g, int b)
	{
	    double x[3];
	    double y[3];
	    x[0] = x0; y[0] = y0;
	    x[1] = x1; y[1] = y1;
	    x[2] = x2; y[2] = y2;
	    triangle(x, y, r, g, b);
	}
    int SaveBMP(char *FileName)
    {
        frame tmp;
		return tmp.SaveBMP_(FileName, Nx, Ny, v);
	}
    int SaveBMP(string Name)
	{
        char CH[256];
        strcpy(CH, Name.c_str());
        return SaveBMP(CH);
	}
	int white()
	{
	    memset(v, 255, Nx*Ny*3);
	    return 0;
	}
	int Color(int r, int g, int b)
	{
	    int x, y;
	    for(y = Ny - 1; y != -1; y --)for(x = 0; x != Nx; x++)PutPixel(x, y, r, g, b);
	    return 0;
	}
	int Line(double x1, double y1, double x2, double y2, int r, int g, int b, double intensity)
	{
	    //cout << "@<";
	    double tmin = 0, tmax = 1;
	    if(x2 > x1)
	    {
	        if(((Xmin - x1)/(x2 - x1)) > tmin) tmin = ((Xmin - x1)/(x2 - x1));
	        if(((Xmax - x1)/(x2 - x1)) < tmax) tmax = ((Xmax - x1)/(x2 - x1));
	    }
	    if(x2 < x1){
	        if(((Xmin - x1)/(x2 - x1)) < tmax) tmax = ((Xmin - x1)/(x2 - x1));
	        if(((Xmax - x1)/(x2 - x1)) > tmin) tmin = ((Xmax - x1)/(x2 - x1));
	    }
	    if((x2 == x1)&&((x2 > Xmax)||(x2 < Xmin))) return 1;
	    if(y2 > y1)
	    {
	        if(((Ymin - y1)/(y2 - y1)) > tmin) tmin = ((Ymin - y1)/(y2 - y1));
	        if(((Ymax - y1)/(y2 - y1)) < tmax) tmax = ((Ymax - y1)/(y2 - y1));
	    }
	    if(y2 < y1)
	    {
	        if(((Ymin - y1)/(y2 - y1)) < tmax) tmax = ((Ymin - y1)/(y2 - y1));
	        if(((Ymax - y1)/(y2 - y1)) > tmin) tmin = ((Ymax - y1)/(y2 - y1));
	    }
	    if((y2 == y1)&&((y2 > Ymax)||(y2 < Ymin))) return 1;
	    if(tmax <= tmin) return 1;
	    double t;
	    int i, NS;
	    NS = int(2*(tmax - tmin)*sqrt(sqr(x2 - x1) + sqr(y2 - y1))) + 1;
	    //cout << NS << endl;
	    double Intensity = intensity*sqrt(sqr(x2 - x1) + sqr(y2 - y1))/double(NS);
	    for(i = 1; i != NS; i++)
	    {
	        t = tmin + (tmax - tmin)*i/double(NS);
	        Spot(x1 + t*(x2 - x1), y1 + t*(y2 - y1), r, g, b, Intensity);
	    }
	    Spot(x1 + tmin*(x2 - x1), y1 + tmin*(y2 - y1), r, g, b, Intensity/2);
	    Spot(x1 + tmax*(x2 - x1), y1 + tmax*(y2 - y1), r, g, b, Intensity/2);
	    //cout << ">";
	    return 0;
	}
    void Vector(double x1, double y1, double x2, double y2, int r, int g, int b, double intensity, double size = 0.3)
	{
	    Line(x1, y1, x2, y2, r, g, b, intensity);
	    Line(x2, y2, x1 + (x2 - x1)*(1 - size) + (y2 - y1)*size/2, y1 + (y2 - y1)*(1 - size) - (x2 - x1)*size/2, r, g, b, intensity);
	    Line(x2, y2, x1 + (x2 - x1)*(1 - size) - (y2 - y1)*size/2, y1 + (y2 - y1)*(1 - size) + (x2 - x1)*size/2, r, g, b, intensity);
	}
	int FrameSizeX()
	{
	    return Xmax - Xmin;
	}
	int FrameSizeY()
	{
	    return Ymax - Ymin;
	}
    void PlotC(point_f &R, point_f &Col, FP3 Min, FP3 Max, string palette, double intensity, int nn = -1);
	void Plot(point_f &D, int type, double min, double max, double r, double g, double b, double intensity = 1)
	{
	    int x;
	    double Xp, X, dX;
	    X = Xmin;
	    double Y, Yp;
	    dX = (Xmax - Xmin)/double(D.n);
	    for(x = 0; x != D.n; x++)
	    {
	        if(type == 0)Y = Ymin + (Ymax - Ymin - 1)*(1 - (D[x].Re - min)/(max - min));
	        if(type == 1)Y = Ymin + (Ymax - Ymin - 1)*(1 - (D[x].Im - min)/(max - min));
	        if(type == 2)Y = Ymin + (Ymax - Ymin - 1)*(1 - (D[x].abs_2() - min)/(max - min));
	        if(x > 0)Line(Xp, Yp, X, Y, r, g, b, intensity);
	        Yp = Y;
	        Xp = X;
            X += dX;
	    }
	}
    void FilledPlot(point_f &D, int type, double min, double max, double center, double r, double g, double b, double intensity = 1, double intensity_f = 0.3)
	{
	    int x;
	    double Xp, X, dX;
	    X = Xmin;
	    double Y, Yp;
	    dX = FrameSizeX()/double(D.n);
	    double Ycenter = Ymin + (FrameSizeY() - 1)*(1 - (center - min)/(max - min));
	    for(x = 0; x != D.n; x++)
	    {
	        if(type == 0)Y = Ymin + (FrameSizeY() - 1)*(1 - (D[x].Re - min)/(max - min));
	        if(type == 1)Y = Ymin + (FrameSizeY() - 1)*(1 - (D[x].Im - min)/(max - min));
	        if(type == 2)Y = Ymin + (FrameSizeY() - 1)*(1 - (D[x].abs_2() - min)/(max - min));
	        if(x > 0)
	        {
	            Line(Xp, Yp, X, Y, r, g, b, intensity);
	            Line(X, Ycenter, X, Y, r, g, b, intensity_f);
	        }
	        Yp = Y;
	        Xp = X;
            X += dX;
	    }
	}
};
class image3D;
class Object3D
{
public:
    virtual void plot(image3D *X){}
    //virtual save(){fstream &f}
    //virtual load(){fstream &f}
};
class Plot2D;
class image3D: public image
{
public:
    FP3 viewSide;
    FP3 lightPoint;
    double Z0_x, Z0_y;
    double X0_x, X0_y;
    double Y0_x, Y0_y;
    float *height;
    bool height_alloc;
    double D_observer;

    double PixelFactor;

    vector<Object3D*> Objects;

    FP3 check_p[3];
    int condition;
public:
    image3D(int nx = 4, int ny = 4)
	{
	    if((nx%4 != 0)||(ny%4 != 0))printf("Class 'image' warning: The image sizes must be numbers divisible by 4.\n");
		Nx = nx;
		Ny = ny;
		v = new byte[Nx*Ny*3];
		Xmin = 0;
		Ymin = 0;
		Xmax = Nx;
		Ymax = Ny;
		PixelFactor = 1;
		condition = 0;
	}
	~image3D()
	{
	    for(int i = 0; i != Objects.size(); i++) delete Objects[i];
	    delete []height;
	}
	void init(int nx, int ny)
	{
	    delete []v;
	    if((nx%4 != 0)||(ny%4 != 0))printf("Class 'image' warning: The image sizes must be numbers divisible by 4.\n");
		Nx = nx;
		Ny = ny;
		v = new byte[Nx*Ny*3];
		Xmin = 0;
		Ymin = 0;
		Xmax = Nx;
		Ymax = Ny;
	}
	void PlotObjects()
	{
	    cout << "Plotting objects<";
	    int N, Np;
	    for(int i = 0; i != Objects.size(); i++)
        {
            Objects[i]->plot(this);
            N = int(1000*i/Objects.size());
            if(N != Np)cout << N/10.0 << " %" << endl;
            Np = N;
        }
	    cout << ">";
	}
	void DeleteObjects()
	{
	    for(int i = 0; i != Objects.size(); i++) delete Objects[i];
	    Objects.clear();
	}
    void init()
    {
        FP3 Z0 = FP3(0, 0, 1);
        FP3 X0 = FP3(1, 0, 0);
        FP3 Y0 = FP3(0, 1, 0);
        FP3 X = VP(Z0, viewSide);
        X = X * (-1);
        X.normalize();
        FP3 Y = VP(X, viewSide);
        Z0_x = 0;
        Z0_y = SP(Z0, Y);
        X0_x = SP(X0, X);
        X0_y = SP(X0, Y);
        Y0_x = SP(Y0, X);
        Y0_y = SP(Y0, Y);

        height = new float[Nx*Ny];
        for(int i = 0; i != Nx*Ny; i++)height[i] = -1e+10;
        height_alloc = true;
    }
    void clean()
    {
        white();
        for(int i = 0; i != Nx*Ny; i++)height[i] = -1e+10;
    }
    void project(FP3 p, double &x, double &y)
    {
        x = (p.x*X0_x + p.y*Y0_x + p.z*Z0_x);
        y = (p.x*X0_y + p.y*Y0_y + p.z*Z0_y);
        double f = D_observer/(D_observer - SP(p, viewSide));
        x *= f*PixelFactor;
        y *= f*PixelFactor;
    }
    int PutPixel_h(int x, int y, float H, int r, int g, int b, double transparency)
    {
        if((x < 0)||(x >= Nx)||(y < 0)||(y >= Ny)) return -1;
        if(H > height[Nx*y + x])
        {
            if(transparency == 0)
            {
                height[Nx*y + x] = H;
                PutPixel(x, y, r, g, b);
            } else
            {
                //height[Nx*y + x] = H;
                int R, G, B;
                GetPixel(x, y, R, G, B);
                PutPixel(x, y, r, g, b);
                PutPixel(x, y, R*transparency + r*(1 - transparency),
                               G*transparency + g*(1 - transparency),
                               B*transparency + b*(1 - transparency));
            }
        }
    }
    bool check_condition(FP3 r)
    {
        if(condition == 1)if((r.x > 0)&&(r.z > 0)) return false;
        //if(condition == 1)if((r.x > 0)) return false;
        return true;
    }
    bool check(int i_min, int i_mid, int i_max, double t1, double t2)
    {
        FP3 r = check_p[i_min] + t1*(check_p[i_mid] - check_p[i_min]) + t2*(check_p[i_max] - check_p[i_min]);
        return check_condition(r);
    }
    void triangle_h(double *x, double *y, double *h, int r, int g, int b, double transparency)
	{
	    double X0, Y0, X1, Y1, X2, Y2, H0, H1, H2, H, H_max;
	    Y0 = y[0];
	    Y2 = y[2];
	    H_max = h[0];
	    int i, i_min, i_max, i_mid;
	    i_min = 0;
	    i_max = 2;
	    for(i = 0; i != 3; i++)
	    {
	        if(y[i] < Y0)
	        {
	            i_min = i;
	            Y0 = y[i];
	        }
	        if(y[i] > Y2)
	        {
	            i_max = i;
	            Y2 = y[i];
	        }
	        if(H_max < h[i])H_max = h[i];
	    }
	    if((i_min != 0)&&(i_max != 0)) i_mid = 0;
	    if((i_min != 1)&&(i_max != 1)) i_mid = 1;
	    if((i_min != 2)&&(i_max != 2)) i_mid = 2;

	    X0 = x[i_min]; Y0 = y[i_min]; H0 = h[i_min];
	    X1 = x[i_mid]; Y1 = y[i_mid]; H1 = h[i_mid];
	    X2 = x[i_max]; Y2 = y[i_max]; H2 = h[i_max];

	    double V1x, V1y, V2x, V2y;
	    V1x = X1 - X0;
	    V1y = Y1 - Y0;
	    V2x = X2 - X0;
	    V2y = Y2 - Y0;
        double t1, t2;

	    if((Y1 - Y0) < 0.000001) Y0 -= 0.000001;
	    if((Y2 - Y1) < 0.000001) Y2 += 0.000001;
	    int k;
	    int shift;
	    int beg, end;
	    if((X0*Y2 - X0*Y1 + X2*Y1 - X2*Y0)/(Y2 - Y0) < X1) shift = 1; else shift = -1;

	    for(i = int(Y0) + 1; i <= int(Y2); i++)
	    {
	        beg = int((X0*Y2 - X0*i + X2*i - X2*Y0)/(Y2 - Y0));
            if(i <= Y1)
            {
                for(k = beg; ((X0*Y1 - X0*i + X1*i - X1*Y0)/(Y1 - Y0) - k)*shift > 0; k += shift)
                {
                    t1 = ((k - X0)*V2y - (i - Y0)*V2x)/(V2y*V1x - V1y*V2x);
                    t2 = ((k - X0)*V1y - (i - Y0)*V1x)/(V2x*V1y - V1x*V2y);
                    if((t1 + t2 < 1)&&(t1 > 0)&&(t2 > 0))
                    if(check(i_min, i_mid, i_max, t1, t2))
                    {
                        H = H0 + (H1 - H0)*t1 + (H2 - H0)*t2;
                        if(H > H_max)H = H_max;
                        PutPixel_h(k, i, H, r, g, b, transparency);
                    }

                }
            }
            if(i >= Y1)
            {
                for(k = beg; ((X1*Y2 - X1*i + X2*i - X2*Y1)/(Y2 - Y1) - k)*shift > 0; k += shift)
                {
                    t1 = ((k - X0)*V2y - (i - Y0)*V2x)/(V2y*V1x - V1y*V2x);
                    t2 = ((k - X0)*V1y - (i - Y0)*V1x)/(V2x*V1y - V1x*V2y);
                    if((t1 + t2 < 1)&&(t1 > 0)&&(t2 > 0))
                    if(check(i_min, i_mid, i_max, t1, t2))
                    {
                        H = H0 + (H1 - H0)*t1 + (H2 - H0)*t2;
                        if(H > H_max)H = H_max;
                        PutPixel_h(k, i, H, r, g, b, transparency);
                    }
                }
            }
	    }
	}
    int triangle3D(FP3 p0, FP3 p1, FP3 p2, int r, int g, int b, double transparency)
    {
        FP3 n = VP(p1 - p0, p2 - p0);
        n.normalize();
        //double factor = abs(SP(n, lightPoint));//*(SP(n, viewSide)*SP(n, lightPoint) > 0);
        double factor = 0.8*abs(SP(n, lightPoint));
        if(transparency == 0) factor *= (SP(n, lightPoint) > 0);
        /*{
            FP3 minorlightPoint(0, 1, -5);minorlightPoint.normalize();
            double minorlight_factor = 0.2*abs(SP(n, minorlightPoint))*(SP(n, viewSide)*SP(n, minorlightPoint) > 0);
            factor += minorlight_factor; if(factor > 1) factor = 1;
        }
        {
            FP3 minorlightPoint(0, -1, -5);minorlightPoint.normalize();
            double minorlight_factor = 0.2*abs(SP(n, minorlightPoint))*(SP(n, viewSide)*SP(n, minorlightPoint) > 0);
            factor += minorlight_factor; if(factor > 1) factor = 1;
        }*/
        {
            FP3 minorlightPoint = viewSide;
            double minorlight_factor = 0.3*abs(SP(n, minorlightPoint))*(SP(n, viewSide)*SP(n, minorlightPoint) > 0);
            factor += minorlight_factor; if(factor > 1) factor = 1;
        }
        //factor += 0.3;


        if(factor > 1) factor = 1;
        double x[3];
        double y[3];
        double h[3];
//if((p0.z > 0)&&(p0.y < 0)&&(p1.z > 0)&&(p1.y < 0)&&(p2.z > 0)&&(p2.y < 0)) return 0;
check_p[0] = p0;
check_p[1] = p1;
check_p[2] = p2;
        project(p0, x[0], y[0]);
        project(p1, x[1], y[1]);
        project(p2, x[2], y[2]);
        x[0] += Nx/2;
        x[1] += Nx/2;
        x[2] += Nx/2;
        y[0] = Ny/2 - y[0];
        y[1] = Ny/2 - y[1];
        y[2] = Ny/2 - y[2];
        double R, G, B;

if(transparency == -1) factor = 1;

        R = factor * double(r);
        G = factor * double(g);
        B = factor * double(b);
        FP3 R1 = VP(n, viewSide);
        FP3 R2 = VP(n, lightPoint);
        R1 = R1 * (-1);
        //R1.normalize();
        //R2.normalize();
        factor = (R1 - R2).norm()*(1 - 0.8*sqr(sqr(sqr(0.5*(R1 + R2).norm()))));
        //factor = 2*(R1 - R2).norm()/(R1 + R2).norm();
        //factor = 1 - 0.5*exp(-sqr(factor/0.5));
    //factor = 1 - 0.95*exp(-sqr(sqr(factor/0.2)));
    factor = 1 - 0.95*exp(-sqr(sqr(factor/0.4)));
        //if(SP(n, viewSide) > 0) factor = 1 - 0.95*exp(-sqr(sqr(factor/0.2))); else
        //    factor = 1 - 0.5*exp(-sqr(factor/0.5));
if(transparency == -1) factor = 1;
        if(SP(n, lightPoint) < 0)factor = 1;
        {
            double d = SP(n, lightPoint);
            double a = 10;
            if(a*d < 1)factor = 1 - (1 - factor)*a*d;
            if(d < 0)factor = 1;
        }

        R = 255 - factor*(255 - R);
        G = 255 - factor*(255 - G);
        B = 255 - factor*(255 - B);
        //h[0] = SP(viewSide, p0);
        //h[1] = SP(viewSide, p1);
        //h[2] = SP(viewSide, p2);
        h[0] = -(D_observer*viewSide - p0).norm();
        h[1] = -(D_observer*viewSide - p1).norm();
        h[2] = -(D_observer*viewSide - p2).norm();
        //cout << x[0] << " " << x[1] << " " << x[2] << " " << y[0] << " " << y[1] << " " << y[2] << " " << h[0] << " " << h[1] << " " << h[2];
        double Transparency = transparency;
        if(Transparency == -1) Transparency = 0;
        triangle_h(x, y, h, int(R), int(G), int(B), Transparency);
    }
    int triangleO(FP3 p0, FP3 p1, FP3 p2, int r, int g, int b, double transparency);
    int cylinder(FP3 p0, FP3 p1, double R, Int3 Color, double transparency = 0, int N = 32)
    {
        FP3 p = p1 - p0;
        if(p.norm() == 0) return 0; else p.normalize();
        FP3 b0 = VP(FP3(0, 0, 1), p);
        if(b0.norm() < 1e-5) b0 = VP(FP3(1, 0, 0), p);
        b0.normalize();
        FP3 b1 = VP(p, b0);
        for(int i = 0; i != N; i ++)
        {
            FP3 A0 = R*b0*cos(2*M_PI*i/double(N)) + R*b1*sin(2*M_PI*i/double(N));
            FP3 A1 = R*b0*cos(2*M_PI*(i + 1)/double(N)) + R*b1*sin(2*M_PI*(i + 1)/double(N));
            triangle3D(p0, p0 + A0, p0 + A1, Color.x, Color.y, Color.z, transparency);
            triangle3D(p1, p1 + A0, p1 + A1, Color.x, Color.y, Color.z, transparency);
            triangle3D(p0 + A0, p0 + A1, p1 + A0, Color.x, Color.y, Color.z, transparency);
            triangle3D(p0 + A1, p1 + A1, p1 + A0, Color.x, Color.y, Color.z, transparency);
        }
    }
    int cylinderO(FP3 p0, FP3 p1, double R, Int3 Color, double transparency, int N);
    int tube(FP3 p0, FP3 n0, FP3 p1, FP3 n1, Int3 Color, double transparency = 0, int N = 32)
    {
        double R0 = n0.norm();
        double R1 = n1.norm();
        FP3 n0_ = n0;
        FP3 n1_ = n1;
        if(R0 < 1e-6)n0_ = n1;
        if(R1 < 1e-6)n1_ = n0;
        if((R0 < 1e-6)&&(R1 < 1e-6)) return 0;
        n0_.normalize();
        n1_.normalize();
        FP3 a = VP(n0_, n1_);
        if(a.norm() < 1e-6)
        {
            a = VP(FP3(0, 0, 1), n0_);
            if(a.norm() < 1e-6) a = VP(FP3(1, 0, 0), n0_);
        }
        a.normalize();
        FP3 b0 = VP(a, n0_);
        FP3 b1 = VP(a, n1_);
        for(int i = 0; i != N; i ++)
        {
            FP3 A0 = R0*b0*cos(2*M_PI*i/double(N)) + R0*a*sin(2*M_PI*i/double(N));
            FP3 B0 = R0*b0*cos(2*M_PI*(i + 1)/double(N)) + R0*a*sin(2*M_PI*(i + 1)/double(N));

            FP3 A1 = R1*b1*cos(2*M_PI*i/double(N)) + R1*a*sin(2*M_PI*i/double(N));
            FP3 B1 = R1*b1*cos(2*M_PI*(i + 1)/double(N)) + R1*a*sin(2*M_PI*(i + 1)/double(N));

            triangle3D(p0 + A0, p0 + B0, p1 + A1, Color.x, Color.y, Color.z, transparency);
            triangle3D(p0 + B0, p1 + B1, p1 + A1, Color.x, Color.y, Color.z, transparency);
        }
    }
    int tubeO(FP3 p0, FP3 n0, FP3 p1, FP3 n1, Int3 Color, double transparency, int N);
    int cone(FP3 p0, FP3 p1, double R, Int3 Color, double transparency = 0, int N = 32)
    {
        FP3 p = p1 - p0;
        if(p.norm() == 0) return 0; else p.normalize();
        FP3 b0 = VP(FP3(0, 0, 1), p);
        if(b0.norm() < 1e-5) b0 = VP(FP3(1, 0, 0), p);
        b0.normalize();
        FP3 b1 = VP(p, b0);
        for(int i = 0; i != N; i ++)
        {
            FP3 A0 = R*b0*cos(2*M_PI*i/double(N)) + R*b1*sin(2*M_PI*i/double(N));
            FP3 A1 = R*b0*cos(2*M_PI*(i + 1)/double(N)) + R*b1*sin(2*M_PI*(i + 1)/double(N));
            triangle3D(p0 + A0, p0, p0 + A1, Color.x, Color.y, Color.z, transparency);
            triangle3D(p1, p0 + A0, p0 + A1, Color.x, Color.y, Color.z, transparency);
        }
    }
    int coneO(FP3 p0, FP3 p1, double R, Int3 Color, double transparency, int N);
    int sphere(FP3 p0, double R, Int3 Color, double transparency = 0, int N = 32)
    {
        for(int i = 0; i != N; i++)
        for(int k = 0; k != N; k++)
        {
            FP3 p00 = p0 + FP3(R*cos(2*M_PI*i/double(N))*sin(M_PI*k/double(N)), R*sin(2*M_PI*i/double(N))*sin(M_PI*k/double(N)), R*cos(M_PI*k/double(N)));
            i += 1;
            FP3 p10 = p0 + FP3(R*cos(2*M_PI*i/double(N))*sin(M_PI*k/double(N)), R*sin(2*M_PI*i/double(N))*sin(M_PI*k/double(N)), R*cos(M_PI*k/double(N)));
            k += 1;
            FP3 p11 = p0 + FP3(R*cos(2*M_PI*i/double(N))*sin(M_PI*k/double(N)), R*sin(2*M_PI*i/double(N))*sin(M_PI*k/double(N)), R*cos(M_PI*k/double(N)));
            i -= 1;
            FP3 p01 = p0 + FP3(R*cos(2*M_PI*i/double(N))*sin(M_PI*k/double(N)), R*sin(2*M_PI*i/double(N))*sin(M_PI*k/double(N)), R*cos(M_PI*k/double(N)));
            k -= 1;
            triangle3D(p00, p01, p11, Color.x, Color.y, Color.z, transparency);
            triangle3D(p11, p10, p00, Color.x, Color.y, Color.z, transparency);
        }
    }
    int sphereO(FP3 p0, double R, Int3 Color, double transparency, int N);
    int rectangle(FP3 p0, FP3 p1, FP3 normal, double R, Int3 Color, double transparency = 0)
    {
        FP3 nn = VP(p1 - p0, normal);
        if(nn.norm2() == 0) return 0;
        nn.normalize();
        nn = R*nn;
        triangle3D(p0 - nn, p1 + nn, p0 + nn, Color.x, Color.y, Color.z, transparency);
        triangle3D(p1 - nn, p0 - nn, p1 + nn, Color.x, Color.y, Color.z, transparency);
    }
    int rectangleO(FP3 p0, FP3 p1, FP3 normal, double R, Int3 Color, double transparency);
    int circle(FP3 p0, FP3 normal, double R, Int3 Color, double transparency = 0, int N = 32)
    {
        FP3 nn = normal;
        if(nn.norm2() == 0) return 0;
        if(nn.z != 0)nn.x += 1; else nn.z += 1;
        FP3 n1 = VP(nn, normal);
        n1.normalize();
        FP3 n2 = VP(n1, normal);
        n2.normalize();
        for(int i = 0; i != N; i++)
        {
            triangle3D(p0, p0 + R*cos(2*M_PI*i/double(N))*n1 + R*sin(2*M_PI*i/double(N))*n2, p0 + R*cos(2*M_PI*(i + 1)/double(N))*n1 + R*sin(2*M_PI*(i + 1)/double(N))*n2, Color.x, Color.y, Color.z, transparency);
        }
    }
    int circleO(FP3 p0, FP3 normal, double R, Int3 Color, double transparency, int N);
    int conditionO(int condition_);
    int CoordSys(FP3 p0, double Size);
    int Box(FP3 p0, FP3 p1, double Size);
    int lineO(FP3 p0, FP3 p1, FP3 normal, double R, Int3 Color, double transparency = 0, int N = 32)
    {
        for(int i = 0; i != N; i++)rectangleO(p0 + (i/double(N))*(p1 - p0), p0 + ((i + 1)/double(N))*(p1 - p0), normal, R, Color, transparency);
    }
    int arrow_planeO(FP3 p0, FP3 p1, FP3 normal, double R, Int3 Color, double transparency);
    int PictureO(image &Im, FP3 p0, FP3 p1, FP3 p2, double transparency = 0)
    {
        FP3 n1 = p1 - p0;
        FP3 n2 = p2 - p0;
        int r, g, b;
        for(int x = 0; x != Im.Nx; x++)
        for(int y = 0; y != Im.Ny; y++)
        {
            Im.GetPixel(x, y, r, g, b);
            if((r != 255)||(g != 255)||(b != 255))
            {
                double f = 1;r*= f; g*= f; b*= f;
                triangleO(p0 + ((x)/double(Im.Nx))*n1 + ((y)/double(Im.Ny))*n2, p0 + ((x + 1)/double(Im.Nx))*n1 + ((y)/double(Im.Ny))*n2, p0 + ((x)/double(Im.Nx))*n1 + ((y + 1)/double(Im.Ny))*n2, r, g, b, transparency);
                triangleO(p0 + ((x + 1)/double(Im.Nx))*n1 + ((y)/double(Im.Ny))*n2, p0 + ((x + 1)/double(Im.Nx))*n1 + ((y + 1)/double(Im.Ny))*n2, p0 + ((x)/double(Im.Nx))*n1 + ((y + 1)/double(Im.Ny))*n2, r, g, b, transparency);
            }
        }
    }
    int PlotO(Plot2D &P, string colorScheme, double Threshold, double Minimum, double Maximum, FP3 p0, FP3 p1, FP3 p2, double transparency, bool order);
};
class Object3D_condition: public Object3D
{
public:
    int Condition;
    void init(int condition)
    {
        Condition = condition;
    }
    virtual void plot(image3D *X)
    {
        X->condition = Condition;
    }
};
class Object3D_triangle: public Object3D
{
public:
    FP3 P0, P1, P2;
    Int3 Color;
    double Transparency;
    void init(FP3 p0, FP3 p1, FP3 p2, int r, int g, int b, double transparency)
    {
        P0 = p0;
        P1 = p1;
        P2 = p2;
        Color.x = r;
        Color.y = g;
        Color.z = b;
        Transparency = transparency;
    }
    virtual void plot(image3D *X)
    {
        X->triangle3D(P0, P1, P2, Color.x, Color.y, Color.z, Transparency);
    }
};
class Object3D_cylinder: public Object3D
{
public:
    FP3 P0, P1;
    double R;
    Int3 Color;
    double Transparency;
    int N;
    void init(FP3 p0, FP3 p1, double r, Int3 color, double transparency = 0, int n = 32)
    {
        P0 = p0;
        P1 = p1;
        R = r;
        Color = color;
        Transparency = transparency;
        N = n;
    }
    virtual void plot(image3D *X)
    {
        X->cylinder(P0, P1, R, Color, Transparency, N);
    }
};
class Object3D_tube: public Object3D
{
public:
    FP3 P0, P1;
    FP3 N0, N1;
    Int3 Color;
    double Transparency;
    int N;
    void init(FP3 p0, FP3 n0, FP3 p1, FP3 n1, Int3 color, double transparency = 0, int n = 32)
    {
        P0 = p0;
        N0 = n0;
        P1 = p1;
        N1 = n1;
        Color = color;
        Transparency = transparency;
        N = n;
    }
    virtual void plot(image3D *X)
    {
        X->tube(P0, N0, P1, N1, Color, Transparency, N);
    }
};
class Object3D_cone: public Object3D
{
public:
    FP3 P0, P1;
    double R;
    Int3 Color;
    double Transparency;
    int N;
    void init(FP3 p0, FP3 p1, double r, Int3 color, double transparency = 0, int n = 32)
    {
        P0 = p0;
        P1 = p1;
        R = r;
        Color = color;
        Transparency = transparency;
        N = n;
    }
    virtual void plot(image3D *X)
    {
        X->cone(P0, P1, R, Color, Transparency, N);
    }
};
class Object3D_sphere: public Object3D
{
public:
    FP3 P0;
    double R;
    Int3 Color;
    double Transparency;
    int N;
    void init(FP3 p0, double r, Int3 color, double transparency = 0, int n = 32)
    {
        P0 = p0;
        R = r;
        Color = color;
        Transparency = transparency;
        N = n;
    }
    virtual void plot(image3D *X)
    {
        X->sphere(P0, R, Color, Transparency, N);
    }
};
class Object3D_rectangle: public Object3D
{
public:
    FP3 P0, P1, Normal;
    double R;
    Int3 Color;
    double Transparency;
    void init(FP3 p0, FP3 p1, FP3 normal, double r, Int3 color, double transparency = 0)
    {
        P0 = p0;
        P1 = p1;
        Normal = normal;
        R = r;
        Color = color;
        Transparency = transparency;
    }
    virtual void plot(image3D *X)
    {
        X->rectangle(P0, P1, Normal, R, Color, Transparency);
    }
};
class Object3D_circle: public Object3D
{
public:
    FP3 P0, Normal;
    double R;
    Int3 Color;
    double Transparency;
    int N;
    void init(FP3 p0, FP3 normal, double r, Int3 color, double transparency = 0, int n = 32)
    {
        P0 = p0;
        Normal = normal;
        R = r;
        Color = color;
        Transparency = transparency;
        N = n;
    }
    virtual void plot(image3D *X)
    {
        X->circle(P0, Normal, R, Color, Transparency, N);
    }
};
inline int image3D::conditionO(int condition_)
{
    Object3D_condition *C; C = new Object3D_condition; C->init(condition_); Objects.push_back(C);
}
inline int image3D::triangleO(FP3 p0, FP3 p1, FP3 p2, int r, int g, int b, double transparency)
{
    Object3D_triangle *C; C = new Object3D_triangle; C->init(p0, p1, p2, r, g, b, transparency); Objects.push_back(C);
};
inline int image3D::cylinderO(FP3 p0, FP3 p1, double R, Int3 Color, double transparency = 0, int N = 32)
{
    Object3D_cylinder *C; C = new Object3D_cylinder; C->init(p0, p1, R, Color, transparency, N); Objects.push_back(C);
};
inline int image3D::tubeO(FP3 p0, FP3 n0, FP3 p1, FP3 n1, Int3 Color, double transparency = 0, int N = 32)
{
    Object3D_tube *C; C = new Object3D_tube; C->init(p0, n0, p1, n1, Color, transparency, N); Objects.push_back(C);
};
inline int image3D::sphereO(FP3 p0, double R, Int3 Color, double transparency = 0, int N = 32)
{
    Object3D_sphere *S; S = new Object3D_sphere; S->init(p0, R, Color, transparency, N); Objects.push_back(S);
};
inline int image3D::coneO(FP3 p0, FP3 p1, double R, Int3 Color, double transparency = 0, int N = 32)
{
    Object3D_cone *C; C = new Object3D_cone; C->init(p0, p1, R, Color, transparency, N); Objects.push_back(C);
};
inline int image3D::rectangleO(FP3 p0, FP3 p1, FP3 normal, double R, Int3 Color, double transparency = 0)
{
    Object3D_rectangle *C; C = new Object3D_rectangle; C->init(p0, p1, normal, R, Color, transparency); Objects.push_back(C);
};
inline int image3D::circleO(FP3 p0, FP3 normal, double R, Int3 Color, double transparency = 0, int N = 32)
{
    Object3D_circle *C; C = new Object3D_circle; C->init(p0, normal, R, Color, transparency, N); Objects.push_back(C);
};
inline int image3D::CoordSys(FP3 p0, double Size)
{
    cylinderO(p0, p0 + FP3(Size*0.7, 0, 0), Size*0.1, Int3(200, 0, 0), 0, 32);
    coneO(p0 + FP3(Size*0.7, 0, 0), p0 + FP3(Size, 0, 0), Size*0.2, Int3(200, 0, 0), 0, 32);

    cylinderO(p0, p0 + FP3(0, Size*0.7, 0), Size*0.1, Int3(0, 200, 0), 0, 32);
    coneO(p0 + FP3(0, Size*0.7, 0), p0 + FP3(0, Size, 0), Size*0.2, Int3(0, 200, 0), 0, 32);

    cylinderO(p0, p0 + FP3(0, 0, Size*0.7), Size*0.1, Int3(0, 0, 200), 0, 32);
    coneO(p0 + FP3(0, 0, Size*0.7), p0 + FP3(0, 0, Size), Size*0.2, Int3(0, 0, 200), 0, 32);

    sphereO(p0, Size*0.2, Int3(200, 200, 200), 0, 32);
};
inline int image3D::Box(FP3 p0, FP3 p1, double Size)
{
    sphereO(FP3(p0.x, p0.y, p0.z), Size, Int3(200, 200, 200));
    sphereO(FP3(p0.x, p0.y, p1.z), Size, Int3(200, 200, 200));
    sphereO(FP3(p0.x, p1.y, p0.z), Size, Int3(200, 200, 200));
    sphereO(FP3(p1.x, p0.y, p0.z), Size, Int3(200, 200, 200));
    sphereO(FP3(p0.x, p1.y, p1.z), Size, Int3(200, 200, 200));
    sphereO(FP3(p1.x, p0.y, p1.z), Size, Int3(200, 200, 200));
    sphereO(FP3(p1.x, p1.y, p0.z), Size, Int3(200, 200, 200));
    sphereO(FP3(p1.x, p1.y, p1.z), Size, Int3(200, 200, 200));

    cylinderO(p0, FP3(p0.x, p0.y, p1.z), Size, Int3(200, 200, 200), 0);
    cylinderO(p0, FP3(p0.x, p1.y, p0.z), Size, Int3(200, 200, 200), 0);
    cylinderO(p0, FP3(p1.x, p0.y, p0.z), Size, Int3(200, 200, 200), 0);

    cylinderO(FP3(p0.x, p0.y, p1.z), FP3(p0.x, p1.y, p1.z), Size, Int3(200, 200, 200), 0);
    cylinderO(FP3(p0.x, p0.y, p1.z), FP3(p1.x, p0.y, p1.z), Size, Int3(200, 200, 200), 0);

    cylinderO(FP3(p0.x, p1.y, p0.z), FP3(p1.x, p1.y, p0.z), Size, Int3(200, 200, 200), 0);
    cylinderO(FP3(p0.x, p1.y, p0.z), FP3(p0.x, p1.y, p1.z), Size, Int3(200, 200, 200), 0);

    cylinderO(FP3(p1.x, p0.y, p0.z), FP3(p1.x, p0.y, p1.z), Size, Int3(200, 200, 200), 0);
    cylinderO(FP3(p1.x, p0.y, p0.z), FP3(p1.x, p1.y, p0.z), Size, Int3(200, 200, 200), 0);

    cylinderO(p1, FP3(p0.x, p1.y, p1.z), Size, Int3(200, 200, 200), 0);
    cylinderO(p1, FP3(p1.x, p0.y, p1.z), Size, Int3(200, 200, 200), 0);
    cylinderO(p1, FP3(p1.x, p1.y, p0.z), Size, Int3(200, 200, 200), 0);

};
inline int image3D::arrow_planeO(FP3 p0, FP3 p1, FP3 normal, double R, Int3 Color, double transparency)
{
    double L = (p1 - p0).norm();
    if(L == 0) return 0;
    double r = R;
    if(L < 2*r)r = L/2; else rectangleO(p0, p0 + ((L - 2*r)/L)*(p1 - p0), normal, r, Color, transparency);
    FP3 d = p1 - p0;
    d.normalize();
    FP3 s = VP(normal, p1 - p0);
    s.normalize();
    triangleO(p1, p1 - 2*r*d + 2*r*s, p1 - 2*r*d - 2*r*s, Color.x, Color.y, Color.z, transparency);
};
class Plot2D
{
public:
    int Nx;
    int Ny;
    float *v;
    float Tmp;
    int n_, r_, g_, b_;
    double R_, G_, B_, C_;
    char *Color;
    int ri, gi, bi;

    double LineTo_xp;
    double LineTo_yp;
    double LineTo_L;
public:
    Plot2D(int nx = 4, int ny = 4)
	{
		Nx = nx;
		Ny = ny;
		v = new float[Nx*Ny];
		Color = "bgrhwBGRHWcmywpCMYkPlenuqLENUQ";
	}
	void constructor(int nx, int ny)
	{
	    delete []v;
		Nx = nx;
		Ny = ny;
		v = new float[Nx*Ny];
		Tmp = 0;
	}
    ~Plot2D()
	{
		delete []v;
	}
	int Save(string FileName)
	{
	    FILE *f;
		f = fopen(FileName.c_str(), "wb+");
		fwrite(&Nx, sizeof(int), 1, f);
		fwrite(&Ny, sizeof(int), 1, f);
		fwrite(v, sizeof(float)*Nx*Ny, 1, f);
		fclose(f);
		return 0;
	}
	int Load(string FileName)
    {
        FILE *f;
		f = fopen(FileName.c_str(),"rb+");
		delete []v;
		fread(&Nx, sizeof(int), 1, f);
		fread(&Ny, sizeof(int), 1, f);
		v = new float[Nx*Ny];
		fread(v, sizeof(float)*Nx*Ny, 1, f);
		return 0;
    }
	int Null()
	{
	    memset(v, 0, Nx*Ny*sizeof(float));
	    return 0;
	}
	float& Pixel(int x, int y)
	{
	    if((x >= 0)&&(x < Nx)&&(y >= 0)&&(y < Ny))return v[Nx*(Ny - y - 1) + x];
	    return Tmp;
	}
	void AddPoint(double x, double y, double intensity)
	{
	    Pixel(int(x), int(y)) += intensity*(1 - x + int(x))*(1 - y + int(y));
	    Pixel(int(x) + 1, int(y)) += intensity*(x - int(x))*(1 - y + int(y));
	    Pixel(int(x), int(y) + 1) += intensity*(1 - x + int(x))*(y - int(y));
	    Pixel(int(x) + 1, int(y) + 1) += intensity*(x - int(x))*(y - int(y));
	}
	void Line(double x1, double y1, double x2, double y2, double intensity)
	{
	    int i;
	    double R = sqrt(sqr(x1 - x2) + sqr(y1 - y2));
	    int N  = 2*(int(R) + 1);
	    for(i = 0; i != N; i++)
            AddPoint(x1 + (x2 - x1)*(0.5 + i)/double(N), y1 + (y2 - y1)*(0.5 + i)/double(N), intensity*R/double(N));
	}
	void FatLine(double x1, double y1, double x2, double y2, double intensity, double thickness)
	{
	    int Nth = int(thickness) + 1;
	    int i;
	    double D, B;
	    double r_1 = 1/sqrt(sqr(x2 - x1) + sqr(y2 - y1));
	    for(i = -Nth; i <= Nth; i++)
	    {
	        D = 0.5*thickness*i/double(Nth);
	        B = sqrt(sqr(0.5*thickness) - sqr(D));
	        //Line(x1 + (y2 - y1)*r_1*D, y1 - (x2 - x1)*r_1*D, x2 + (y2 - y1)*r_1*D, y2 - (x2 - x1)*r_1*D, 0.5*intensity*thickness/double(Nth));
	        Line(x1 + (y2 - y1)*r_1*D + (x2 - x1)*r_1*B, y1 - (x2 - x1)*r_1*D + (y2 - y1)*r_1*B, x2 + (y2 - y1)*r_1*D  - (x2 - x1)*r_1*B, y2 - (x2 - x1)*r_1*D  - (y2 - y1)*r_1*B, 0.5*intensity*thickness/double(Nth));
            Line(x1 + (y2 - y1)*r_1*D - (x2 - x1)*r_1*B, y1 - (x2 - x1)*r_1*D - (y2 - y1)*r_1*B, x1 + (y2 - y1)*r_1*D  + (x2 - x1)*r_1*B, y1 - (x2 - x1)*r_1*D  + (y2 - y1)*r_1*B, 0.25*intensity*thickness/double(Nth));
	        Line(x2 + (y2 - y1)*r_1*D - (x2 - x1)*r_1*B, y2 - (x2 - x1)*r_1*D - (y2 - y1)*r_1*B, x2 + (y2 - y1)*r_1*D  + (x2 - x1)*r_1*B, y2 - (x2 - x1)*r_1*D  + (y2 - y1)*r_1*B, 0.25*intensity*thickness/double(Nth));
	    }
	}
	void RectLine(double x1, double y1, double x2, double y2, double intensity, double thickness)
	{
	    int Nth = int(thickness) + 1;
	    int i;
	    double D, B;
	    double r_1 = 1/sqrt(sqr(x2 - x1) + sqr(y2 - y1));
	    for(i = -Nth; i <= Nth; i++)
	    {
	        D = 0.5*thickness*i/double(Nth);
	        B = sqrt(sqr(0.5*thickness) - sqr(D));
	        Line(x1 + (y2 - y1)*r_1*D, y1 - (x2 - x1)*r_1*D, x2 + (y2 - y1)*r_1*D, y2 - (x2 - x1)*r_1*D, 0.5*intensity*thickness/double(Nth));
	    }
	}
	void LineToSet(double x, double y)
	{
        LineTo_xp = x;
        LineTo_yp = y;
        LineTo_L = 0;
	}
	int LineTo(double x, double y, double intensity, double thickness)
	{
	    if(sqrt(sqr(x - LineTo_xp) + sqr(y - LineTo_yp)) < thickness) return 0;
	    double r = sqrt(sqr(x - LineTo_xp) + sqr(y - LineTo_yp));
        LineTo_L += r;
        FatLine(LineTo_xp, LineTo_yp, x, y, intensity, thickness);
        LineTo_xp = x;
        LineTo_yp = y;
        return 0;
	}
	int DashLineTo(double x, double y, double intensity, double thickness, double interval)
	{
	    if(sqrt(sqr(x - LineTo_xp) + sqr(y - LineTo_yp)) < 0.2*thickness) return 0;
	    double r = sqrt(sqr(x - LineTo_xp) + sqr(y - LineTo_yp));
	    bool in1 = (int(LineTo_L/interval)%2 == 0);
	    bool in2 = (int((LineTo_L + r)/interval)%2 == 0);
	    if(in1&&in2)
	    {
	        LineTo_L += r;
	        RectLine(LineTo_xp, LineTo_yp, x, y, intensity, thickness);
	        LineTo_xp = x;
	        LineTo_yp = y;
	        return 0;
	    }
        LineTo_L += r;
        LineTo_xp = x;
        LineTo_yp = y;
        return 0;
	}
    void Vector(double x1, double y1, double x2, double y2, double intensity, double size = 0.3)
	{
	    Line(x1, y1, x2, y2, intensity);
	    Line(x2, y2, x1 + (x2 - x1)*(1 - size) + (y2 - y1)*size/2, y1 + (y2 - y1)*(1 - size) - (x2 - x1)*size/2, intensity);
	    Line(x2, y2, x1 + (x2 - x1)*(1 - size) - (y2 - y1)*size/2, y1 + (y2 - y1)*(1 - size) + (x2 - x1)*size/2, intensity);
	}
	float Value(double x, double y)
	{
	    double S = 0, S1 = 0;
	    if((int(x) >= 0)&&(int(x) < Nx)&&(int(y) >= 0)&&(int(y) < Ny))
	    {
	        S += Pixel(int(x), int(y))*(1 - x + int(x))*(1 - y + int(y));
	        S1 += (1 - x + int(x))*(1 - y + int(y));
	    }
	    if((int(x) + 1 >= 0)&&(int(x) + 1 < Nx)&&(int(y) >= 0)&&(int(y) < Ny))
	    {
	        S += Pixel(int(x) + 1, int(y))*(x - int(x))*(1 - y + int(y));
	        S1 += (x - int(x))*(1 - y + int(y));
	    }
	    if((int(x) >= 0)&&(int(x) < Nx)&&(int(y) + 1 >= 0)&&(int(y) + 1 < Ny))
	    {
	        S += Pixel(int(x), int(y) + 1)*(1 - x + int(x))*(y - int(y));
	        S1 += (1 - x + int(x))*(y - int(y));
	    }
	    if((int(x) + 1 >= 0)&&(int(x) + 1 < Nx)&&(int(y) + 1 >= 0)&&(int(y) + 1 < Ny))
	    {
	        S += Pixel(int(x) + 1, int(y) + 1)*(x - int(x))*(y - int(y));
	        S1 += (x - int(x))*(y - int(y));
	    }
	    return S/S1;
	}
	void AddPoint_(double x, double y, double intensity)
	{
	    if((Round(x) >= 0)&&(Round(x) < Nx)&&(Round(y) >= 0)&&(Round(y) < Ny))Pixel(Round(x), Round(y)) += intensity;
	}
	void AddPoint_(int x, int y, double intensity)
	{
	    if((x >= 0)&&(x < Nx)&&(y >= 0)&&(y < Ny))Pixel(x, y) += intensity;
	}
	bool Plot(image &I, double Max, double r, double g, double b)
	{
	    if((I.FrameSizeX() != Nx)||(I.FrameSizeY() != Ny)) return false;
	    double R, G, B;
	    R = r;G = g;B = b;
	    if(R <= 0)R = 1;
	    if(G <= 0)G = 1;
	    if(B <= 0)B = 1;
	    double lr = log(R/255.0);
        double lg = log(G/255.0);
        double lb = log(B/255.0);
        int x, y, Ix, Iy, i;
        for(y = Ny - 1; y >= 0; y--)for(x = Nx - 1; x >= 0; x--)
        {
            Ix = I.Xmin + x;
            Iy = I.Ymin + y;
            i = 3*(I.Nx*(I.Ny - Iy - 1) + Ix);
            I.v[i] = int(I.v[i]*exp(lb*Pixel(x, y)/Max));
            I.v[i+1] = int(I.v[i+1]*exp(lg*Pixel(x, y)/Max));
            I.v[i+2] = int(I.v[i+2]*exp(lr*Pixel(x, y)/Max));
        }
        return true;
	}
    void PColor(char Name, int &r, int &g, int &b)
	{
	    if(int(Name) == int(Color[0])){r = 0; g = 0; b = 255;}
	    if(int(Name) == int(Color[1])){r = 0; g = 255; b = 0;}
	    if(int(Name) == int(Color[2])){r = 255; g = 0; b = 0;}
	    if(int(Name) == int(Color[3])){r = 127; g = 127; b = 127;}
	    if(int(Name) == int(Color[4])){r = 255; g = 255; b = 255;}
	    if(int(Name) == int(Color[5])){r = 0; g = 0; b = 127;}
	    if(int(Name) == int(Color[6])){r = 0; g = 127; b = 0;}
	    if(int(Name) == int(Color[7])){r = 127; g = 0; b = 0;}
	    if(int(Name) == int(Color[8])){r = 76; g = 76; b = 76;}
	    if(int(Name) == int(Color[9])){r = 178; g = 178; b = 178;}
	    if(int(Name) == int(Color[10])){r = 0; g = 255; b = 255;}
	    if(int(Name) == int(Color[11])){r = 255; g = 0; b = 255;}
	    if(int(Name) == int(Color[12])){r = 255; g = 255; b = 0;}
	    if(int(Name) == int(Color[13])){r = 255; g = 255; b = 255;}
	    if(int(Name) == int(Color[14])){r = 255; g = 0; b = 127;}
	    if(int(Name) == int(Color[15])){r = 0; g = 127; b = 127;}
	    if(int(Name) == int(Color[16])){r = 127; g = 0; b = 127;}
	    if(int(Name) == int(Color[17])){r = 127; g = 127; b = 0;}
	    if(int(Name) == int(Color[18])){r = 0; g = 0; b = 0;}
	    if(int(Name) == int(Color[19])){r = 127; g = 0; b = 63;}
	    if(int(Name) == int(Color[20])){r = 0; g = 255; b = 127;}
	    if(int(Name) == int(Color[21])){r = 127; g = 255; b = 0;}
	    if(int(Name) == int(Color[22])){r = 0; g = 127; b = 255;}
	    if(int(Name) == int(Color[23])){r = 127; g = 0; b = 255;}
	    if(int(Name) == int(Color[24])){r = 255; g = 127; b = 0;}
	    if(int(Name) == int(Color[25])){r = 0; g = 127; b = 63;}
	    if(int(Name) == int(Color[26])){r = 63; g = 127; b = 0;}
	    if(int(Name) == int(Color[27])){r = 0; g = 63; b = 127;}
	    if(int(Name) == int(Color[28])){r = 63; g = 0; b = 127;}
	    if(int(Name) == int(Color[29])){r = 127; g = 63; b = 0;}
	}
	int palette_line(double z, const char* Type, int &r, int &g, int &b)
	{
	    if(z > 1)return palette_line(1, Type,r, g, b);
	    if(z < 0)return palette_line(0, Type,r, g, b);
	    ri = r;
	    gi = g;
	    bi = b;
	    n_ = strlen(Type);
	    C_ = 1 - z*(n_-1) + int(z*(n_-1));
        r_ = ri; g_ = gi; b_ = bi;
        PColor(Type[int(z*(n_-1))], r_, g_, b_);
	    R_ = C_*r_;
	    G_ = C_*g_;
	    B_ = C_*b_;
	    C_ = z*(n_-1) - int(z*(n_-1));
	    r_ = ri; g_ = gi; b_ = bi;
        PColor(Type[int(z*(n_-1)) + 1], r_, g_, b_);
	    R_ += C_*r_;
	    G_ += C_*g_;
	    B_ += C_*b_;
	    r = int(R_);g = int(G_);b = int(B_);
	    if(r > 255)r = 255;
	    if(g > 255)g = 255;
	    if(b > 255)b = 255;
	    return 0;
	}
	double RateFactor(double X, double Rate)
	{
	    if(X < 0) return 0;
	    if(X > 1) return 1;
	    if(Rate == 0)return X;
        if(Rate > 0)return (1 - pow(1 - pow(X, 1 + Rate), 1/(1 + Rate)));
        if(Rate < 0)return pow(1 - pow(1 - X, 1 - Rate), 1/(1 - Rate));
	}
	bool Plot(image &I, string PaletterType, bool Transparency, double Rate = 0, double Min = 0, double Max = 1, bool sharp = true)
	{
	    if((I.FrameSizeX() != Nx)||(I.FrameSizeY() != Ny))
	    {
	        int x, y, Ix, Iy, i, r, g, b, x_, y_;
	        double V, S, P;
	        for(x = 0; x < I.FrameSizeX(); x++)for(y = 0; y < I.FrameSizeY(); y++)
	        {
	            V = 0;
	            S = 0;
	            x_ = int(x*Nx/double(I.FrameSizeX()));
	            y_ = int(y*Ny/double(I.FrameSizeY()));
	            P = (1 - x*Nx/double(I.FrameSizeX()) + x_)*(1 - y*Ny/double(I.FrameSizeY()) + y_);
	            if((x_ >= 0)&&(x_ < Nx)&&(y_ >= 0)&&(y_ < Ny)){V += Pixel(x_, y_)*P;S += P;}
	            if(!sharp)
	            {
                    x_ = int(x*Nx/double(I.FrameSizeX())) + 1;
                    y_ = int(y*Ny/double(I.FrameSizeY()));
                    P = (1 + x*Nx/double(I.FrameSizeX()) - x_)*(1 - y*Ny/double(I.FrameSizeY()) + y_);
                    if((x_ >= 0)&&(x_ < Nx)&&(y_ >= 0)&&(y_ < Ny)){V += Pixel(x_, y_)*P;S += P;}
                    x_ = int(x*Nx/double(I.FrameSizeX()));
                    y_ = int(y*Ny/double(I.FrameSizeY())) + 1;
                    P = (1 - x*Nx/double(I.FrameSizeX()) + x_)*(1 + y*Ny/double(I.FrameSizeY()) - y_);
                    if((x_ >= 0)&&(x_ < Nx)&&(y_ >= 0)&&(y_ < Ny)){V += Pixel(x_, y_)*P;S += P;}
                    x_ = int(x*Nx/double(I.FrameSizeX())) + 1;
                    y_ = int(y*Ny/double(I.FrameSizeY())) + 1;
                    P = (1 + x*Nx/double(I.FrameSizeX()) - x_)*(1 + y*Ny/double(I.FrameSizeY()) - y_);
                    if((x_ >= 0)&&(x_ < Nx)&&(y_ >= 0)&&(y_ < Ny)){V += Pixel(x_, y_)*P;S += P;}
	            }
	            V /= S;
	            Ix = I.Xmin + x;
                Iy = I.Ymin + y;
                I.GetPixel(Ix, Iy, r, g, b);
                palette_line(RateFactor((V - Min)/(Max - Min), Rate), PaletterType.c_str(), r, g, b);
                if(Transparency)I.SpotW(Ix, Iy, r, g, b); else I.PutPixel(Ix, Iy, r, g, b);
	        }
	        return true;
	    }
	    int x, y, Ix, Iy, i, r, g, b;
        for(y = Ny - 1; y >= 0; y--)for(x = Nx - 1; x >= 0; x--)
        {
            Ix = I.Xmin + x;
            Iy = I.Ymin + y;
            I.GetPixel(Ix, Iy, r, g, b);
            palette_line(RateFactor((Pixel(x, y) - Min)/(Max - Min), Rate), PaletterType.c_str(), r, g, b);
            if(Transparency)I.SpotW(Ix, Iy, r, g, b); else I.PutPixel(Ix, Iy, r, g, b);
        }
        return true;
	}
	bool Plot(image &I, int Type, bool Transparency, double Rate = 0)
	{
	    frame F;
	    F.palette_type = Type;
	    if((I.FrameSizeX() != Nx)||(I.FrameSizeY() != Ny)) return false;
	    int x, y, Ix, Iy, i, r, g, b;
        for(y = Ny - 1; y >= 0; y--)for(x = Nx - 1; x >= 0; x--)
        {
            Ix = I.Xmin + x;
            Iy = I.Ymin + y;
            I.GetPixel(Ix, Iy, r, g, b);
            F.palette_line(RateFactor(Pixel(x, y), Rate), r, g, b);
            if(Transparency)I.SpotW(Ix, Iy, r, g, b); else I.PutPixel(Ix, Iy, r, g, b);
        }
        return true;
	}
	void Wavelet(double *Data, int n, double Max = 1, double MaxK = M_PI/2)
	{
	    double Size = 5;
	    double center, wavelength, X, M;
	    complex S;
	    int x, y, i, min, max;
	    for(x = 0; x != Nx; x++)
	    for(y = 0; y != Ny; y++)
	    {
	        center = n*x/double(Nx);
	        wavelength = 2*M_PI/(MaxK*(Ny - y)/double(Ny));
	        min =  int(center - wavelength*Size/2) + 1;
	        max = int(center + wavelength*Size/2);
	        if(min < 0) min = 0;
	        if(max >= n)max = n - 1;
	        S = 0;
	        for(i = min; i <= max; i++)
	        {
	            X = i - center;
	            M = sqr(cos(M_PI*X/Size/wavelength));
	            S.Re += Data[i]*M*sin(2*M_PI*X/wavelength)/wavelength;
	            S.Im += Data[i]*M*cos(2*M_PI*X/wavelength)/wavelength;
	        }
	        S.Re = abs(S);
	        if(S.Re > Max)S = Max;
	        Pixel(x, y) = S.Re/Max;
	    }
	}
};
inline void image::PlotC(point_f &R, point_f &Col, FP3 Min, FP3 Max, string palette, double intensity, int nn)
{
    if(nn == -1)nn = R.n;
    double Xp, Yp, X, Y;
    int r, g, b;
    Plot2D Pal(4, 4);
    for(int t = 0; t < nn; t++)
    {
        X = Xmin + (Xmax - Xmin)*(R[t].Re - Min.x)/(Max.x - Min.x);
        Y = Ymax - (Ymax - Ymin)*(R[t].Im - Min.y)/(Max.y - Min.y);
        Pal.palette_line((Col[t].Re - Min.z)/(Max.z - Min.z), palette.c_str(), r, g, b);
        if(t > 0)Line(Xp, Yp, X, Y, r, g, b, intensity);
        Xp = X;
        Yp = Y;
    }
};
inline int image3D::PlotO(Plot2D &P, string colorScheme, double Threshold, double Minimum, double Maximum, FP3 p0, FP3 p1, FP3 p2, double transparency, bool order)
{
    cout << "PlotO" << endl;
    FP3 n1 = p1 - p0;
    FP3 n2 = p2 - p0;
    int r, g, b;
    double v;
    Plot2D Pal(4, 4);
    FP3 Point[8];
    double PP0, PP1;
    int iP;
    for(int x = 0; x != P.Nx-1; x++)
    for(int y = 0; y != P.Ny-1; y++)
    {
        //Im.GetPixel(x, y, r, g, b);
        //if((r != 255)||(g != 255)||(b != 255))
        /*v = P.Pixel(x, y);
        if((v >= Min)&&(v <= Max))
        {
            Pal.palette_line((v - Min)/(Max - Min), colorScheme.c_str(), r, g, b);
            //double f = 1;r*= f; g*= f; b*= f;
            triangleO(p0 + ((x)/double(P.Nx))*n1 + ((y)/double(P.Ny))*n2, p0 + ((x + 1)/double(P.Nx))*n1 + ((y)/double(P.Ny))*n2, p0 + ((x)/double(P.Nx))*n1 + ((y + 1)/double(P.Ny))*n2, r, g, b, transparency);
            triangleO(p0 + ((x + 1)/double(P.Nx))*n1 + ((y)/double(P.Ny))*n2, p0 + ((x + 1)/double(P.Nx))*n1 + ((y + 1)/double(P.Ny))*n2, p0 + ((x)/double(P.Nx))*n1 + ((y + 1)/double(P.Ny))*n2, r, g, b, transparency);
        }*/
        iP = 0;
        PP0 = P.Pixel(x, y) - Threshold; //cout << PP0 << " ";
        if(PP0 >= 0){Point[iP] = p0 + ((x)/double(P.Nx))*n1 + ((y)/double(P.Ny))*n2; iP++;}

        PP1 = P.Pixel(x, y+1) - Threshold; //cout << PP1 << " ";
        if(PP0*PP1 < 0){Point[iP] = p0 + ((x)/double(P.Nx))*n1 + ((y + PP0/(PP0 - PP1))/double(P.Ny))*n2; iP++;}
        if(PP1 >= 0){Point[iP] = p0 + ((x)/double(P.Nx))*n1 + ((y+1)/double(P.Ny))*n2; iP++;}
        PP0 = PP1;

        PP1 = P.Pixel(x+1, y+1) - Threshold;//cout << PP1 << " ";
        if(PP0*PP1 < 0){Point[iP] = p0 + ((x + PP0/(PP0 - PP1))/double(P.Nx))*n1 + ((y+1)/double(P.Ny))*n2; iP++;}
        if(PP1 >= 0){Point[iP] = p0 + ((x+1)/double(P.Nx))*n1 + ((y+1)/double(P.Ny))*n2; iP++;}
        PP0 = PP1;

        PP1 = P.Pixel(x+1, y) - Threshold;//cout << PP1 << " ";
        if(PP0*PP1 < 0){Point[iP] = p0 + ((x+1)/double(P.Nx))*n1 + ((y+1-PP0/(PP0 - PP1))/double(P.Ny))*n2; iP++;}
        if(PP1 >= 0){Point[iP] = p0 + ((x+1)/double(P.Nx))*n1 + ((y)/double(P.Ny))*n2; iP++;}
        PP0 = PP1;

        PP1 = P.Pixel(x, y) - Threshold;//cout << PP1 << " ";
        if(PP0*PP1 < 0){Point[iP] = p0 + ((x+1-PP0/(PP0 - PP1))/double(P.Nx))*n1 + ((y)/double(P.Ny))*n2; iP++;}
        //if(PP1 >= 0){P[iP] = p0 + (x+1)/double(P.Nx))*n1 + ((y)/double(P.Ny))*n2; iP++;}
        //PP0 = PP1;
        v = 0.25*(P.Pixel(x, y)+P.Pixel(x, y+1)+P.Pixel(x+1, y)+P.Pixel(x+1, y+1));
        if(v < Threshold)v = Threshold;
        if(iP > 2)for(int i = 2; i != iP; i++)
        {
            Pal.palette_line((v - Minimum)/(Maximum - Minimum), colorScheme.c_str(), r, g, b);
            if(order)triangleO(Point[0], Point[i-1], Point[i], r, g, b, transparency); else
                     triangleO(Point[i-1], Point[0], Point[i], r, g, b, transparency);
            //cout <<"*";
        }
        iP = 0;
        //cout << endl;
    }
    //exit(0);
};
#endif // FFT_A_H_INCLUDED
