#include <stdio.h>
#include <zlib.h>
#include <stdlib.h>
#include <string.h>
#include "fft_a.h"

int main(int argc, char* argv[])
{
    int nx = 1024;
    int ny = 1024;
    fprintf(stderr, "graph3d\n");
    image3D MyPicture(nx, ny);
    MyPicture.D_observer = 10000;
    MyPicture.viewSide = FP3(1, 0, 0); // вектор точки наблюдения
    MyPicture.viewSide.normalize(); // вектор должен быть отнормирован
    MyPicture.lightPoint = FP3(1, 1, 2); // вектор точки основного освещения
    MyPicture.lightPoint.normalize();// вектор должен быть отнормирован
    MyPicture.init(); // инициализация 3D 
    MyPicture.clean(); // очистка массива трехмерных объектов
    MyPicture.white(); // очистка до белого фона
    ReadVTK(int NNN, float *data, string name, double &Max, bool cutted = true)
    //MyPicture.Box(FP3(0.,0.,0.), FP3(100.,100.,100.), 1);
    //MyPicture.cylinderO(FP3(0.,0.,0.), FP3(100,50,0), 10, Int3(100,100,100), 0.5, 64);
    //MyPicture.cylinderO(FP3(1.,1.,1.), FP3(11.,11.,11.), 10, Int3(100,100,0), 0, 32);
    MyPicture.PlotObjects();
    MyPicture.SaveBMP("MyPicture.bmp");
}
