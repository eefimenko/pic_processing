#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np
import itertools
from multiprocessing.pool import ThreadPool as Pool
import os

def process(i, path, ne, npos):
    ne[i] = utils.ntrapped(path, i, 'electrons')
    npos[i] = utils.ntrapped(path, i, 'positrons')

def process_star(a_b):
    return process(*a_b)

def read_ntrapped(path,ne,np):
    f = open(path + '/ntrapped.txt', 'r')
    i = 0
    for line in f:
        values = line.split()
        i = int(values[0])
        ne[i] = float(values[1])
        np[i] = float(values[2])
    return i

def save_ntrapped(path,ne,npos,nmin = 0,nmax = 0):
    f = open(path + '/ntrapped.txt', 'a')
    if nmax == 0:
        nmax = len(ne)
    for i in range(nmin,nmax):
        f.write('%d %le %le\n'%(i,ne[i],npos[i]))
    f.close()

def main():
    num = len(sys.argv)
    picspath = 'pics'
    fig = plt.figure(num=None, figsize=(15, 5))
    
    ax1 = fig.add_subplot(2,2,1)
    ax1.set_title('Electron growth rate')
    ax2 = fig.add_subplot(2,2,2)
    ax2.set_title('Positron growth rate')
    ax3 = fig.add_subplot(2,2,3)
    ax3.set_title('Number of electrons')
    ax4 = fig.add_subplot(2,2,4)
    ax4.set_title('Number of positrons')
    ax3.set_yscale('log')
    ax4.set_yscale('log')
    ax1.set_xlabel('time,T')
    ax2.set_xlabel('time,T')
    ax3.set_xlabel('time,T')
    ax4.set_xlabel('time,T')
    for i_ in range(num-1):
        path = sys.argv[i_+1] + '/'
        print path
        config = utils.get_config(path + "/ParsedInput.txt")
    
        step = float(config['TimeStep'])*float(config['BOIterationPass'])
        omega = float(config['Omega'])
        T = 2*math.pi/omega
        n = int(T/step)
        nmin, nmax = utils.find_min_max_from_directory(path + utils.netpath)
        
        print 'Found ' + str(nmax-nmin) + ' files'
        print str(n) + ' steps per period'
        num = (nmax - nmin)
        ne_ = np.zeros(num)
        npos_ = np.zeros(num)

#        iterations = range(nmin,nmax)
#        pool = Pool()
#        pool.map(process_star, itertools.izip(iterations, 
#                                              itertools.repeat(path),
#                                              itertools.repeat(ne_),
#                                              itertools.repeat(npos_)))
        if os.path.exists(path + '/ntrapped.txt'):
            nmin = read_ntrapped(path,ne_,npos_) + 1
            
        for i in range(nmin,nmax):
            print i
            process(i, path, ne_, npos_)
        save_ntrapped(path, ne_, npos_, nmin, nmax)
    
        length = num - n
        ge_ = np.zeros(length)
        gp_ = np.zeros(length)
        for i in range(length):
            if ne_[i+n] > 0 and ne_[i] > 0:
                ge = math.log(ne_[i+n]/ne_[i])/(n*step)*T
            else:
                ge = 0.
            if npos_[i+n] > 0 and npos_[i] > 0:
                gp = math.log(npos_[i+n]/npos_[i])/(n*step)*T
            else:
                gp = 0.
            ge_[i] = ge
            gp_[i] = gp
#        norm = 1./ne_[1]
#        ne_ *= norm
#        npos_ *= norm
       
        axis = utils.create_axis(length, step/T)
        axis = utils.create_axis(length, step/T)
        pe, = ax1.plot(axis, ge_)
        pp, = ax2.plot(axis, gp_)
        pe1, = ax3.plot(axis, ne_[:length])
        pp1, = ax4.plot(axis, npos_[:length])
        
        print max(ge_), max(gp_)
        n1 = 12*int(T/step)
        n2 = 17*int(T/step)
        mult = 1.
        for i in range(n1):
            mult *= math.exp(ge_[i]*step/T)
    
        print mult, sum(ge_[n1:n2])/(n2-n1)
    plt.show()
#    plt.savefig('pics/conc_axis_t.png')
#    plt.close()
    
if __name__ == '__main__':
    main()
