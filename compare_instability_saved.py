#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils

def read_field2d(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    m = max([max(row) for row in field])
    return m

def read_conc(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    return tmp[0]

def read_field(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def create_axis(n,step,x0=0):
    axis = []
    for i in range(n):
        axis.append(i*step + x0)
    return axis

def get_color(j):
    color = ['r', 'g', 'b', 'k', 'c', 'y', 'G', 'B']
    if (j >= len(color)):
        j = j - len(color)
    return color[j]

def get_dirs_configs(array):
    cfgs = []
    dirs = []
    for j in range(1, len(array)):
        config = utils.get_config(array[j] + "/ParsedInput.txt")
        cfgs.append(config)
        dirs.append(array[j])
    return dirs, cfgs

def get_axis_and_field(path, config):
    nx = int(config['MatrixSize_X'])
    Xmax = float(config['X_Max'])
    Xmin = float(config['X_Min'])
    dx = (Xmax-Xmin)/nx
    
    fe = open(path + '/emax.txt', 'r')
    fn = open(path + '/nmax.txt', 'r')
    emax = []
    nemax = []
    axis = []

    for line in fe:
        tmp = line.split()
        axis.append(float(tmp[0]))
        emax.append(float(tmp[1]))

    for line in fn:
        tmp = line.split()
        nemax.append(float(tmp[1]))

    fn.close()
    fe.close()
    return axis, emax, nemax, dx

def sum_array(array):
    res = [0] * len(array[0])
    for i in range(len(array[0])):
        for j in range(len(array)):
            res[i] += array[j][i]
    return res

def sum_all(array):
    res = 0
    for i in range(len(array[0])):
        for j in range(len(array)):
            res += array[j][i]
    return res

def sum_arrayj(array, axis):
    res = [0] * len(array)
    for i in range(len(array[0])):
        for j in range(len(array)):
            res[j] += array[j][i]*axis[i]
    return res

def norm1(array, a=1.):
    maximum = max(array)
    for i,e in enumerate(array):
        array[i] = e/maximum*a
    return array

def main():
   
    picspath = 'pics'
    plt.rc('text', usetex=True)

    dirs, configs = get_dirs_configs(sys.argv)
    
    axis = []
    emax = []
    nmax = []
    leg = []

   
    for j in range(len(dirs)):
       
        axis_tmp, emax_tmp, nmax_tmp, dx = get_axis_and_field(dirs[j], configs[j])
        axis.append(axis_tmp)
        emax.append(emax_tmp)
        nmax.append(nmax_tmp)
        lab = '$\lambda/\Delta$ = %.0f; T/dt =  %.0f' % (0.8e-4/dx, 2.7/(axis_tmp[1]-axis_tmp[0])) 
        leg.append(lab)
        print 2.7/(axis_tmp[1]-axis_tmp[0]), 0.8e-4/dx
   
    m = 0

    for j in range(len(dirs)):
        if axis[j][-1] > m:
            m = axis[j][-1]
    print m, type(m)

    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(2,1,1)
    
   
    for j in range(len(dirs)):
        pe, = ax1.plot(axis[j], emax[j], get_color(j), label = leg[j])
    x0 = 14.6
    x1 = m
    ax1.legend(loc='upper left', shadow=True)
    ax1.set_yscale('log')
    ax1.set_xlim([x0, x1])
#    ax1.set_ylim([1e11, 1e13])
    ax1.set_ylabel('Max field')
    ax1.set_xlabel('time, fs')
    
    ax2 = fig.add_subplot(2,1,2)
    for j in range(len(dirs)):
        pe, = ax2.plot(axis[j], nmax[j], get_color(j), label = leg[j])
    ax2.set_yscale('log')
    ax2.set_xlim([x0, x1])
    ax2.set_ylim([1e25, 1e32])
    ax2.set_ylabel('Max concentration, cm^-3')
    ax2.set_xlabel('time, fs')
   

    plt.savefig(picspath + '/concentration_compare.png')

    
    
if __name__ == '__main__':
    main()
