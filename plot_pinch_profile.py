#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import sys
import math
import utils

def main():
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
    fig1 = plt.figure(figsize=(10,5))
    ax1 = fig1.add_subplot(2,1,1)
    ax2 = fig1.add_subplot(2,1,2)
    dirs=['27pw', '27pw_x2', '27pw_x4', '27pw_x8r', '27pw_x16r', '27pw_x32r']
    iterations = [23, 16, 24, 40, 70, 96]
    offset = [375, 750, 1500, 3000, 6000, 12000]
    labels = ['$n_{\lambda} = 115$','$n_{\lambda} = 230$','$n_{\lambda} = 460$','$n_{\lambda} = 920$','$n_{\lambda} = 1840$','$n_{\lambda} = 3680$'   ]
    dt = 0
    n = len(dirs)
    for i in range(n):
        path = dirs[i] + '/'
        print path
        config = utils.get_config(path + "/ParsedInput.txt")
        wl = float(config['Wavelength'])
        if 'resize' in config.keys():
            factor = int(config['resize'])
        else:
            factor = 1
        if 'rescale' in config.keys():
            rescale = int(config['rescale'])
        else:
            rescale = 1
        print rescale
        
        Xmax = float(config['X_Max'])/rescale #mkm to wavelength
        Xmin = float(config['X_Min'])/rescale #mkm to wavelength
        Ymax = float(config['Y_Max'])/rescale #mkm to wavelength
        Ymin = float(config['Y_Min'])/rescale #mkm to wavelength
        Zmax = float(config['Z_Max'])/rescale #mkm to wavelength
        Zmin = float(config['Z_Min'])/rescale #mkm to wavelength
        nx = int(config['MatrixSize_X'])
        ny = int(config['MatrixSize_Y'])
        nz = int(config['MatrixSize_Z'])
        dx = (Xmax-Xmin)/nx
        dy = (Ymax-Ymin)/ny
        dz = (Zmax-Zmin)/nz
        el = utils.bo_file_load(path + utils.nezpath,iterations[i] + offset[i],nx,ny)/(2.*dx*dy*dz)
        el_prof = el[nx/2]
#        ez = utils.bo_file_load(utils.ezpath,iterations[i] + offset[i],nx,ny)
#        ez_prof = ez[nx/2]
        bz = utils.bo_file_load(path + utils.bzpath,iterations[i] + offset[i],nx,ny)
        bz_prof = bz[nx/2]
        axis = utils.create_axis(nx, dx/wl, Xmin/wl)
        ax1.plot(axis, el_prof, label = labels[i])
        ax2.plot(axis, bz_prof, label = labels[i])

    picname = 'pics/cmp_pinch_profile.png'
    ax1.set_xlim([-0.1, 0.1])
    ax2.set_xlim([-0.1, 0.1])
    ax1.legend(loc = 'upper right')
    ax2.legend(loc = 'upper right')
    fig1.savefig(picname, dpi = 256)
    
    plt.show()

if __name__ == '__main__':
    main()
