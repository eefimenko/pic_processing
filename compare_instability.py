#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils

def read_field2d(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    m = max([max(row) for row in field])
    return m

def read_conc(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    return tmp[0]

def read_field(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def create_axis(n,step,x0=0):
    axis = []
    for i in range(n):
        axis.append(i*step + x0)
    return axis

def get_color(j):
    color = ['r', 'g', 'b', 'k', 'c', 'y', 'G', 'B']
    if (j >= len(color)):
        j = j - len(color)
    return color[j]

def get_dirs_configs(array):
    cfgs = []
    dirs = []
    for j in range(1, len(array)):
        config = utils.get_config(array[j] + "/ParsedInput.txt")
        cfgs.append(config)
        dirs.append(array[j])
    return dirs, cfgs

def get_axis_and_field(path, config, n_min=-1, n_max=-1):
    pi = 3.14159
    emax = []
    nemax = []

    BOIterationPass = int(config['BOIterationPass'])
     
    try:
        tmp = config['BOIterationStart']
    except KeyError:
        tmp = 0
    BOIterationStart = int(tmp)
    if BOIterationStart%BOIterationPass == 0:
        nmin = BOIterationStart/BOIterationPass
    else:
        nmin = BOIterationStart/BOIterationPass + 1

    step = float(config['TimeStep'])*1e15*float(config['BOIterationPass'])
 
    power = float(config['PeakPower'])*1e-22
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    Xmax = float(config['X_Max'])
    Xmin = float(config['X_Min'])
    Ymax = float(config['Y_Max'])
    Ymin = float(config['Y_Min'])
    Zmax = float(config['Z_Max'])
    Zmin = float(config['Z_Min'])
    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz
    dv = dx*dy*dz
    wl = float(config['Wavelength'])
    ezpath = path + '/data/E2z'
    nezpath = path + '/data/Electron2Dz'
        
    num = utils.num_files(nezpath)
#    nmin = 900
#    num = 100
    nmax = nmin + num
    
    if n_min != -1:
        nmin = n_min
    if n_max != -1:
        nmax = n_max

    print path, nmin, nmax        
    axis = create_axis(nmax-nmin, step, nmin*step)
    fe = open(path + '/emax.txt', 'w')
    fn = open(path + '/nmax.txt', 'w')
    for i in range(nmin,nmax):
        if (i%50 == 0):
            print i
        ezname = ezpath + '/' + "%06d.txt" % (i,)
        nezname = nezpath + '/' + "%06d.txt" % (i,)
        e = read_field2d(ezname,nx,ny)
        n = read_field2d(nezname,nx,ny, 1./dv)
        emax.append(e)
        nemax.append(n)
        fe.write('%lf %le\n' % (axis[i-nmin], e))
        fn.write('%lf %le\n' % (axis[i-nmin], n))
    fe.close()
    fn.close()
    return axis, emax, nemax

def sum_array(array):
    res = [0] * len(array[0])
    for i in range(len(array[0])):
        for j in range(len(array)):
            res[i] += array[j][i]
    return res

def sum_all(array):
    res = 0
    for i in range(len(array[0])):
        for j in range(len(array)):
            res += array[j][i]
    return res

def sum_arrayj(array, axis):
    res = [0] * len(array)
    for i in range(len(array[0])):
        for j in range(len(array)):
            res[j] += array[j][i]*axis[i]
    return res

def norm1(array, a=1.):
    maximum = max(array)
    for i,e in enumerate(array):
        array[i] = e/maximum*a
    return array

def main():
   
    picspath = 'pics'
    dirs, configs = get_dirs_configs(sys.argv)
    
    axis = []
    emax = []
    nmax = []
    leg = []

    
    for j in range(len(dirs)):
        if dirs[j] == 'inst_38_n_1':
            n_min = 340
            n_max = 355
        elif dirs[j] == 'inst_38_n_2':
            n_min = 680
            n_max = 800
        elif dirs[j] == 'inst_38_n_4':
            n_min = 1360
            n_max = 1600
        else:
            n_min = -1
            n_max = -1
#            n_min = 950
#            n_max = 1004
        axis_tmp, emax_tmp, nmax_tmp = get_axis_and_field(dirs[j], configs[j], n_min, n_max)
        axis.append(axis_tmp)
        emax.append(emax_tmp)
        nmax.append(nmax_tmp)
        leg.append(sys.argv[j])
   
    m = 0

    for j in range(len(dirs)):
        if axis[j][-1] > m:
            m = axis[j][-1]

    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(2,1,1)
    
   
    for j in range(len(dirs)):
        pe, = ax1.plot(axis[j], emax[j], get_color(j), label = dirs[j])

    ax1.legend(loc='lower left', shadow=True)
    ax1.set_yscale('log')
    ax1.set_xlim(0, m)
    ax1.set_ylabel('Max field')
    ax1.set_xlabel('time, fs')
    
    ax2 = fig.add_subplot(2,1,2)
    for j in range(len(dirs)):
        pe, = ax2.plot(axis[j], nmax[j], get_color(j), label = dirs[j])
    ax2.set_yscale('log')
    ax2.set_ylabel('Max concentration, cm^-3')
    ax2.set_xlabel('time, fs')
   

    plt.savefig(picspath + '/concentration_compare.png')

    
    
if __name__ == '__main__':
    main()
