#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_field(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)/norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis
def normalize(array):
    m = max(array)
    n = len(array)
    for i in range(n):
        array[i] /= m
    return array

def read_profiles(path,i):
    nexpath = '/data/Electron2Dx'
    nezpath = '/data/Electron2Dz'
    npxpath = '/data/Positron2Dx'
    npzpath = '/data/Positron2Dz'
    config = utils.get_config(path+"/ParsedInput.txt")
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    wl = float(config['Wavelength'])
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    Xmax = float(config['X_Max'])/wl #mkm to wavelength
    Xmin = float(config['X_Min'])/wl #mkm to wavelength
    dv = 2*dx*dy*dz

    name = path + nexpath + '/' + "%06d.txt" % (i,)
    fieldn = read_field(name,nx,ny,dv)
    profn = fieldn[:][ny/2]
    name = path+ nezpath + '/' + "%06d.txt" % (i,)
    fieldn = read_field(name,nx,ny,dv)
    profn1 = fieldn[:][ny/2]
    ax = create_axis(nx, (Xmax-Xmin)/nx, Xmin)
    return ax, normalize(profn), normalize(profn1)

def main():
    n = len(sys.argv) - 1
    
    axis_ = []
    gamma_ = []
    field_ = []
    picspath = 'pics'
    for m in range(150, 170):
        picname = picspath + '/' + "profcmp%06d.png" % (m,)
        fig = plt.figure(num=None)
        ax1 = fig.add_subplot(2,1,1)
        ax2 = fig.add_subplot(2,1,2)
        for i in range(n):
            path = sys.argv[i+1]
            axis, p, p1 = read_profiles(path,m)
        
            ax1.plot(axis, p, label = path)
            ax2.plot(axis, p1, label = path)
        
        ax1.legend()
        ax2.legend()    
        ax2.set_xlim([-1,1])
        plt.savefig(picname)
        plt.close()

if __name__ == '__main__':
    main()
