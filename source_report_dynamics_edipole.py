#!/usr/bin/python
from matplotlib import gridspec
import matplotlib as mp
#mp.use('Agg')
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import utils
import sys
import os
from tqdm import *
from pylab import *

def make_patch_spines_invisible(ax):
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)
        
def average(field):
    field = (field + field[:, ::-1] + field[::-1, :] + field[::-1, ::-1])/4.
    return field

def plot_2d(ax, b_field, el_field, ph_field, cmap_el, cmap_ph, nx, ny, xmin, xmax, ymin, ymax, bound, fontsize, x_label ='$x/\lambda$', y_label = '$z/\lambda$', pos_label = '(a)', circle_r = None):
    ax.set_xlim([-bound,bound])
    ax.set_ylim([-bound,bound])
    ax.set_xticks([-1, -0.5, 0, 0.5, 1])
    ax.set_yticks([-1, -0.5, 0, 0.5, 1])
    ax.set_xticklabels([])
    ax.set_yticklabels(['-1', '',  '0', '', '1'])
    
    max_ = np.amax(b_field)
    min_ = np.amin(b_field)
    mf = max(max_, abs(min_))
    
    ax.imshow(b_field, origin = 'lower', cmap='bwr', extent=[xmin,xmax,ymin,ymax], vmin=-mf, vmax = mf, alpha = 0.3)

    m_el = np.amax(el_field)
    m_ph = np.amax(ph_field)
    print(m_el)
    ph_field = average(ph_field)
    
    surf_ph = ax.imshow(ph_field[:,nx/2:-1], origin = 'lower', cmap=cmap_ph, vmin = 1e-4*m_ph, vmax = m_ph, norm=clr.LogNorm(), extent=[0,xmax,ymin,ymax])
    surf_el = ax.imshow(el_field[:,:nx/2], origin = 'lower', cmap=cmap_el, vmin = 1e-4*m_el, vmax = m_el, norm=clr.LogNorm(), extent=[xmin,0,ymin,ymax])

    ax.axvline(x=0, color = 'k', dashes = [2,2])
    if circle_r is None:
        ax.axvline(x=-0.25, color = 'k')
        ax.axvline(x=0.25, color = 'k')
    else:
        circ = plt.Circle((0, 0), radius=circle_r, color='k', fill=False, linewidth = 0.5)
        ax.add_patch(circ)
        
    ax.text(-bound, bound-0.25, pos_label)
    ax.text(-bound + 0.05, -bound + 0.05, '$e^{-}$', fontsize = fontsize - 4)
    ax.text(bound-0.5, -bound+0.05, '$\hbar\omega$', fontsize = fontsize - 4)
    ax.text(-bound-0.5, bound-0.25, y_label)
        
    if x_label is not None:
        ax.text(bound-0.25, -bound-0.3, x_label)
        ax.set_xticklabels(['-1', '',  '0', '', '1'])
    
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(surf_el,  orientation  = 'vertical', cax=cax)
    cbar.ax.tick_params(labelsize=fontsize-4)
    
    
    cbar.set_ticks([0.01, 0.1, 1, 10, 100])
    cbar.set_ticklabels(['0.1', '1','10','100', '1000']) # it is correct!
    cbar.ax.text(0.5,1.03, '$n_{e^-}/n_c$', fontsize = fontsize-4)
    
    cax = divider.append_axes("right", size="5%", pad=1.1)
    cbar = plt.colorbar(surf_ph,  orientation  = 'vertical', cax=cax)
    cbar.ax.tick_params(labelsize=fontsize-4)
    cbar.ax.text(1.5,1.03, '$n_{\hbar\omega}$', fontsize = fontsize-4)
    

def plot_time(axis_2, ax, ne, npos, nph, bz, lin_bound, fontsize, pos_label = '(c)', x_axis = False):
    
    axis_2.set_yscale('linear')
    axis_2.set_ylim((lin_bound, 850))
    axis_2.set_yticks([100, 200, 300, 400, 500, 600, 700])
    axis_2.set_yticklabels(['100', '', '300', '', '', '600', ''])
    axis_2.text(-6.5, 800, '$\\frac{n_{e^{-,+}}}{n_c}$', fontsize = fontsize)
    axis_2.spines['bottom'].set_visible(False)
    axis_2.yaxis.set_ticks_position('left')
    axis_2.yaxis.set_label_position('left')
    axis_2.xaxis.set_visible(False)
    ne3_, = axis_2.plot(ax, ne, 'g')
    np3_, = axis_2.plot(ax, npos, 'b')
            
    divider = make_axes_locatable(axis_2)
    axis_3 = divider.append_axes("bottom", size=.9, pad=0, sharex=axis_2)
    axis_3.set_yscale('log')
    axis_3.set_ylim((0.05, lin_bound))
    axis_3.set_yticks([0.1, 1, 10])
    axis_3.set_yticklabels(['0.1', '1', '10'])
    axis_3.spines['top'].set_visible(False)
    
    axis_3.yaxis.set_ticks_position('left')
    axis_3.yaxis.set_label_position('left')
    
    ne3__, = axis_3.plot(ax, ne, 'g')
    np3__, = axis_3.plot(ax, npos, 'b')
    
    axis_3.set_xlim([0, 35])

    axis_4 = axis_2.twinx()
    axis_4.spines["right"].set_position(("axes", 1.))
    make_patch_spines_invisible(axis_4)
    # Second, show the right spine.
    axis_4.spines["right"].set_visible(True)
    axis_4.yaxis.set_label_position('right')
    axis_4.yaxis.set_ticks_position('right')
    nph3_, = axis_4.plot(ax, nph, color = 'orange')
    axis_4.set_ylim([0, 1.35])
    axis_4.text(35.3, 1.22, '$n_{\hbar\omega}$, $cm^{-3}$', fontsize = fontsize-4)
    axis_4.set_yticks([0, 0.5,1.0])
    axis_4.set_yticklabels(['0', '$5\cdot10^{22}$', '$10^{23}$'])
    
    axis = axis_2.twinx()
    axis.yaxis.set_ticks_position('right')
    axis.yaxis.set_label_position('right')
    axis.yaxis.tick_right()
    
    bz3_, = axis.plot(ax, np.abs(bz)/np.amax(bz), 'grey', alpha=0.5)

    axis.set_xticks([0,5,10,15,20,25,30])
    axis.set_xticklabels([])
    if x_axis:
        axis.set_xlabel('$t/T$')
        axis.set_xticklabels(['','5', '', '15', '', '25', ''])
        
    axis.set_yticks([])
    axis.set_ylim([0, 1.35])
    axis.set_xlim([0, 35])
    
    axis.text(0, 1.22, pos_label)
    plt.legend([ne3_, np3_, nph3_, bz3_],
               ['$n_{e^-}$', '$n_{e^+}$', '$n_{\hbar\omega}$','|$E_z$|'],
               loc = 'lower left',
               frameon = False, ncol=2, columnspacing=0.5,
               labelspacing=0.1,
               fontsize = fontsize-2, bbox_to_anchor=(0.25,0.68),
               handlelength=1.5, handletextpad=0.1)

def create_cmap(name):
    # make the colormaps
    cmap1 = mp.cm.get_cmap(name)
   
    cmap1._init() # create the _lut array, with rgba values
    alphas = np.ones(cmap1.N+3)
    for i in range(10):
        alphas[i] = 0.
    
    cmap1._lut[:,-1] = alphas
    cmap1.set_under('w', 0)
    cmap1.set_bad('w', False)
    
    return cmap1
    
def main():
    fontsize = 32  #!!!
    picpath = '.' #!!!
    verbose = False #!!!
    bound = 1.5 #!!! Size of box in 2d plots [-bound, bound]
    iter_compression = 300 # !!! Iteration for which 2d plots are created
    mp.rcParams.update({'font.size': fontsize})
        
    #path_5pw = 'DipoleB_Wire__2021-05-14_03-53-22' #!!!
    #path_3pw = 'DipoleB_Wire__2021-05-14_03-53-22' #!!!
    #path_10pw = 'DipoleB_Wire__2021-05-14_03-53-22' #!!!
    #path_5pw = 'DipoleE_Sphere_5_2021-12-17_05-57-14' #!!!
    #path_3pw = 'DipoleE_Sphere_3_2021-12-13_19-15-42' #!!!
    #path_10pw = 'DipoleE_Sphere_10_2021-12-15_13-41-17' #!!!
    path_5pw = 'DipoleE_Wire_5_2021-12-09_04-43-25' #!!!
    path_3pw = 'DipoleE_Wire_3_2021-12-06_21-30-53' #!!!
    path_10pw = 'DipoleE_Wire_10_2021-12-07_13-37-17' #!!!
    #path_10pw = 'DipoleB_Wire__2021-05-21_17-24-09'
    #path_3pw = 'DipoleB_Wire__2021-04-22_23-51-45'
    #path_5pw = 'sph_target_5pw'
    #path_10pw = 'sph_target_10pw'
    #path_3pw = 'sph_target_3pw'
    
    config_5pw = utils.get_config(path_5pw + "/ParsedInput.txt")
    config_10pw = utils.get_config(path_10pw + "/ParsedInput.txt")
    config_3pw = utils.get_config(path_3pw + "/ParsedInput.txt")

    archive_3pw = os.path.join(path_3pw, 'BasicOutput', 'data.zip')
    archive_5pw = os.path.join(path_5pw, 'BasicOutput', 'data.zip')
    archive_10pw = os.path.join(path_10pw, 'BasicOutput', 'data.zip')
    
    wl = float(config_10pw['Wavelength'])
    nx = int(config_10pw['MatrixSize_X'])
    ny = int(config_10pw['MatrixSize_Y'])
    dt = float(config_10pw['TimeStep'])/float(config_10pw['Period'])*int(config_10pw['BOIterationPass'])
    xmax = float(config_10pw['X_Max'])/wl 
    xmin = float(config_10pw['X_Min'])/wl 
    ymax = float(config_10pw['Y_Max'])/wl
    ymin = float(config_10pw['Y_Min'])/wl
    zmax = float(config_10pw['Z_Max'])/wl
    zmin = float(config_10pw['Z_Min'])/wl

    prefix = os.path.join('data')
    bz_z_path = os.path.join(prefix, 'Bz_z')
    bz_y_path = os.path.join(prefix, 'Bz_y')
    ez_z_path = os.path.join(prefix, 'Ez_z')
    ez_y_path = os.path.join(prefix, 'Ez_y')
    ex_z_path = os.path.join(prefix, 'Ex_z')
    ex_y_path = os.path.join(prefix, 'Ex_y')
    el_z_path = os.path.join(prefix, 'Electron_z')
    pos_z_path = os.path.join(prefix, 'Positron_z')
    ph_z_path = os.path.join(prefix, 'Photon_z')
    ion_z_path = os.path.join(prefix, 'Ion_z')
    el_y_path = os.path.join(prefix, 'Electron_y')
    ph_y_path = os.path.join(prefix, 'Photon_y')
    pos_y_path = os.path.join(prefix, 'Positron_y')
    ion_y_path = os.path.join(prefix, 'Ion_y')

    bz_5pw_t = utils.ts(path_5pw, ez_z_path, name='ez_arc', ftype = 'bin', 
                        tstype='center', shape = (nx,ny), verbose=verbose,
                        archive = archive_5pw)
    ne_5pw_t = utils.ts(path_5pw, el_z_path, name='el_arc', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose,
                        archive = archive_5pw)
    np_5pw_t = utils.ts(path_5pw, pos_z_path, name='pos_arc', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose,
                        archive = archive_5pw)
    nph_5pw_t = utils.ts(path_5pw, ph_z_path, name='ph_arc', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose,
                        archive = archive_5pw)/1e23
    
    bz_10pw_t = utils.ts(path_10pw, ez_z_path, name='ez_arc', ftype = 'bin', 
                         tstype='center', shape = (nx,ny), verbose=verbose,
                        archive = archive_10pw)
    ne_10pw_t = utils.ts(path_10pw, el_z_path, name='el_arc', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose,
                        archive = archive_10pw)
    nph_10pw_t = utils.ts(path_10pw, ph_z_path, name='ph_arc', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose,
                        archive = archive_10pw)/1e23
    np_10pw_t = utils.ts(path_10pw, pos_z_path, name='pos_arc', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose,
                        archive = archive_10pw)
    
    bz_3pw_t = utils.ts(path_3pw, ez_z_path, name='ez_arc', ftype = 'bin', 
                         tstype='center', shape = (nx,ny), verbose=verbose,
                        archive = archive_3pw)
    ne_3pw_t = utils.ts(path_3pw, el_z_path, name='el_arc', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose,
                        archive = archive_3pw)
    np_3pw_t = utils.ts(path_3pw, pos_z_path, name='pos_arc', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose,
                        archive = archive_3pw)
    nph_3pw_t = utils.ts(path_3pw, ph_z_path, name='ph_arc', ftype = 'bin',
                         tstype='max', shape = (nx,ny), verbose=verbose,
                        archive = archive_3pw)/1e23
    
    ax_5pw = utils.create_axis(len(bz_5pw_t), dt)
    ax_10pw = utils.create_axis(len(bz_10pw_t), dt)
    ax_3pw = utils.create_axis(len(bz_3pw_t), dt)
    
    cmap1 = create_cmap('Greens') # colormap for electrons
    cmap2 = create_cmap('YlOrBr') # colormap for photons
        
    read, b_field_c_5pw = utils.bo_file_load(os.path.join(ez_y_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive_5pw)
    read, el_field_c_5pw = utils.bo_file_load(os.path.join(el_y_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive_5pw)
    read, ph_field_c_5pw = utils.bo_file_load(os.path.join(ph_y_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive_5pw)

    read, b_field_c_5pw_z = utils.bo_file_load(os.path.join(ez_z_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive_5pw)
    read, el_field_c_5pw_z = utils.bo_file_load(os.path.join(el_z_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive_5pw)
    read, ph_field_c_5pw_z = utils.bo_file_load(os.path.join(ph_z_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive_5pw)
    
    read, b_field_c_10pw = utils.bo_file_load(os.path.join(ez_y_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive_10pw)
    read, el_field_c_10pw = utils.bo_file_load(os.path.join(el_y_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive_10pw)
    read, ph_field_c_10pw = utils.bo_file_load(os.path.join(ph_y_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive_10pw)

    read, b_field_c_10pw_z = utils.bo_file_load(os.path.join(ez_z_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive_10pw)
    read, el_field_c_10pw_z = utils.bo_file_load(os.path.join(el_z_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive_10pw)
    read, ph_field_c_10pw_z = utils.bo_file_load(os.path.join(ph_z_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive_10pw)
    
    read, b_field_c_3pw = utils.bo_file_load(os.path.join(ez_y_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive_3pw)
    read, el_field_c_3pw = utils.bo_file_load(os.path.join(el_y_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive_3pw)
    read, ph_field_c_3pw = utils.bo_file_load(os.path.join(ph_y_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive_3pw)

    read, b_field_c_3pw_z = utils.bo_file_load(os.path.join(ez_z_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive_3pw)
    read, el_field_c_3pw_z = utils.bo_file_load(os.path.join(el_z_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive_3pw)
    read, ph_field_c_3pw_z = utils.bo_file_load(os.path.join(ph_z_path), iter_compression,nx,ny, ftype='bin', verbose=1, transpose=1, archive = archive_3pw)
        
    fig = plt.figure(figsize = (25, 15))
    
    ax2 = plt.subplot2grid((3,6),(0,0), colspan=2)
    plot_2d(ax2, b_field_c_3pw, el_field_c_3pw, ph_field_c_3pw, cmap1, cmap2, nx, ny, xmin, xmax, ymin, ymax, bound, fontsize, x_label = None, circle_r = 0.5)
    
    ax3 = plt.subplot2grid((3,6),(1,0), colspan=2, sharex = ax2)
    plot_2d(ax3, b_field_c_5pw, el_field_c_5pw, ph_field_c_5pw, cmap1, cmap2, nx, ny, xmin, xmax, ymin, ymax, bound, fontsize, pos_label = '(d)', x_label = None, circle_r = 0.5)
    
    ax4 = plt.subplot2grid((3,6),(2,0), colspan=2)
    plot_2d(ax4, b_field_c_10pw, el_field_c_10pw, ph_field_c_10pw, cmap1, cmap2, nx, ny, xmin, xmax, ymin, ymax, bound, fontsize, pos_label = '(g)', circle_r = 0.5)
    
    ax2 = plt.subplot2grid((3,6),(0,2), colspan=2)
    plot_2d(ax2, b_field_c_3pw_z, el_field_c_3pw_z, ph_field_c_3pw_z, cmap1, cmap2, nx, ny, xmin, xmax, ymin, ymax, bound, fontsize, pos_label = '(b)', y_label = '$y/\lambda$', x_label = None, circle_r = 0.5)
    
    ax3 = plt.subplot2grid((3,6),(1,2), colspan=2, sharex = ax2)
    plot_2d(ax3, b_field_c_5pw_z, el_field_c_5pw_z, ph_field_c_5pw_z, cmap1, cmap2, nx, ny, xmin, xmax, ymin, ymax, bound, fontsize,  pos_label = '(e)', y_label = '$y/\lambda$', x_label = None, circle_r = 0.5)

    ax4 = plt.subplot2grid((3,6),(2,2), colspan=2)
    plot_2d(ax4, b_field_c_10pw_z, el_field_c_10pw_z, ph_field_c_10pw_z, cmap1, cmap2, nx, ny, xmin, xmax, ymin, ymax, bound, fontsize, pos_label = '(h)', y_label = '$y/\lambda$', circle_r = 0.5)
    
    lin_bound = 10
    
    axis_2 = plt.subplot2grid((3,6),(0,4), colspan=2)
    plot_time(axis_2, ax_3pw, ne_3pw_t, np_3pw_t, nph_3pw_t, bz_3pw_t, lin_bound, fontsize)

    axis_2 = plt.subplot2grid((3,6),(1,4), colspan=2)
    plot_time(axis_2, ax_5pw, ne_5pw_t, np_5pw_t, nph_5pw_t, bz_5pw_t, lin_bound, fontsize, pos_label = '(f)')
    
    axis_2 = plt.subplot2grid((3,6),(2,4), colspan=2)
    plot_time(axis_2, ax_10pw, ne_10pw_t, np_10pw_t, nph_10pw_t, bz_10pw_t, lin_bound, fontsize, pos_label = '(i)', x_axis = True)
    
    
    plt.tight_layout()
    plt.subplots_adjust(hspace=0.1, wspace=1.5, right=0.92, left = 0.05)
    
    picname = os.path.join(picpath, 'source_dynamics_wire_arc_edip.png')
    print(picname)
    plt.savefig(picname, dpi=128)

if __name__ == '__main__':
    main()
