#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np
from scipy.interpolate import spline

def read_tau(filename):
    f = open(filename, 'r')
    ax = []
    tau = []
    for line in f:
        tmp = line.split()
        ax.append(float(tmp[0]))
        tau.append(float(tmp[1]))
    f.close()
    return ax,tau

def main():
    nmin = int(sys.argv[1])
    nmax = int(sys.argv[2])
    ax, tau = read_tau('tau_%d_%d.txt'%(nmin,nmax))
    axn, taun = read_tau('tau_norm_%d_%d.txt'%(nmin,nmax))
    
    fig, ax1 = plt.subplots()
    
    tau_smooth = np.poly1d(np.polyfit(np.array(ax), np.array(tau), 4))
    taun_smooth = np.poly1d(np.polyfit(np.array(axn), np.array(taun), 4))
    ax1.plot(ax, tau_smooth(ax), 'b-', label = 'W cascade')
    ax1.plot(axn, taun_smooth(axn), 'g-', label = 'W/o cascade')
    ax1.plot([0, max([max(tau), max(taun)])], [1000,1000], 'k:')
    ax1.set_xlim([0,3.5])
    ax1.set_ylim([0,1200])
    ax1.set_ylabel('$\\tau, as$')
    ax1.set_xlabel('$\hbar\omega, GeV$')
    plt.legend(loc = 'upper right')
    picname = 'compare_tau.png'
    plt.savefig(picname)
    plt.close()

if __name__ == '__main__':
    main()
