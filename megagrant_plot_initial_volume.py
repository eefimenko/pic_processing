#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import math
import utils
import sys
import numpy as np
print(np.__version__)
import os
from tqdm import *
print(np.__version__)
import pickle

def main():
    fontsize = 18
    mp.rcParams.update({'font.size': fontsize})
    
    picspath = '/home/evgeny/Dropbox/MG'
    path = '.'
    
    
    dirs = ['Input2D_8pw_2023-01-08_14-22-48',
            #'Input2D_9pw_2023-01-08_22-15-58',
            'Input2D_10pw_2023-01-06_10-25-24',
            'Input2D_12pw_2023-01-07_23-22-28',
            'Input2D_15pw_2023-01-06_14-02-55',
            'Input2D_17pw_2023-01-07_01-32-05',
            'Input2D_20pw_2023-01-06_01-54-48',
            'Input2D_30pw_2023-01-06_17-13-02']
    powers = []
    volumes = []
    
    for d in dirs:
        config = utils.get_config(os.path.join(d, "ParsedInput.txt"))
        BOIterationPass = float(config['BOIterationPass'])
        dt = float(config['TimeStep'])*1e15*BOIterationPass #fs
        power = int(config['PeakPowerPW'])
        ppw = int(config['PeakPower'])*1e-22 #PW
        omega = float(config['Omega'])
        dt = float(config['TimeStep'])*BOIterationPass
        T = 2 * math.pi/omega
        dtt = dt/T
        print("Power = " + str(ppw) + 'PW')
        print("dt = " + str(dt) + 'fs')
        wl = float(config['Wavelength'])
        print(np.__version__) 
        Xmax = float(config['X_Max'])/1e-4 #mkm
        Xmin = float(config['X_Min'])/1e-4 #mkm
        Ymax = float(config['Y_Max'])/1e-4 #mkm
        Ymin = float(config['Y_Min'])/1e-4 #mkm
        Zmax = float(config['Z_Max'])/1e-4 #mkm
        Zmin = float(config['Z_Min'])/1e-4 #mkm

        nx = int(config['MatrixSize_X'])
        ny = int(config['MatrixSize_Y'])
        nz = int(config['MatrixSize_Z'])

        #print('Nx = ' + str(nx)) 
        #print('Ny = ' + str(ny))
        #print('Nz = ' + str(nz))

        dx = (Xmax-Xmin)/nx
        dy = (Ymax-Ymin)/ny
        dz = (Zmax-Zmin)/nz
        mult_dv = 1/(2.*dx*dy*dz*1e-12)
        step = (Xmax-Xmin)/nx
        const_a_p = 7.81441e-9

        volume = 0.
        volume1 = 0.
    
        filename = os.path.join(d,'volume.txt')
        if os.path.exists(filename):
            with open(filename, 'r') as f:
                line = f.readline()
                volume = float(line.split()[0])
                volume1 = float(line.split()[1])
        print('Power %d Volume %lf' % (power, volume1))
        powers.append(power)
        volumes.append(volume1)
        
    fig = plt.figure(figsize=(16, 5))
    ax1 = fig.add_subplot(1, 3, 3)
    ax1.plot(powers, volumes)
    ax1.set_ylim([0, 225])
    ax1.set_xlim([7, 31])
    ax1.set_xlabel('P, PW')
    ax1.set_ylabel('V, $\mu$m$^3$')
    ax1.text(0, 225, '(c)')
    
    dirs = ['Input2D_8pw_2023-01-08_14-22-48',
            'Input2D_30pw_2023-01-06_17-13-02']
    iterations = [899, 889]
    labels = ['(a)', '(b)']
    labels1 = ['8 PW', '30 PW']
    
    for i,d in enumerate(dirs):
        config = utils.get_config(os.path.join(d, "ParsedInput.txt"))
        BOIterationPass = float(config['BOIterationPass'])
        dt = float(config['TimeStep'])*1e15*BOIterationPass #fs
        power = int(config['PeakPowerPW'])
        ppw = int(config['PeakPower'])*1e-22 #PW
        omega = float(config['Omega'])
        dt = float(config['TimeStep'])*BOIterationPass
        T = 2 * math.pi/omega
        dtt = dt/T
        print("Power = " + str(ppw) + 'PW')
        print("dt = " + str(dt) + 'fs')
        wl = float(config['Wavelength'])
        print(np.__version__) 
        Xmax = float(config['X_Max'])/1e-4 #mkm
        Xmin = float(config['X_Min'])/1e-4 #mkm
        Ymax = float(config['Y_Max'])/1e-4 #mkm
        Ymin = float(config['Y_Min'])/1e-4 #mkm
        Zmax = float(config['Z_Max'])/1e-4 #mkm
        Zmin = float(config['Z_Min'])/1e-4 #mkm

        nx = int(config['MatrixSize_X'])
        ny = int(config['MatrixSize_Y'])
        nz = int(config['MatrixSize_Z'])

        #print('Nx = ' + str(nx)) 
        #print('Ny = ' + str(ny))
        #print('Nz = ' + str(nz))

        dx = (Xmax-Xmin)/nx
        dy = (Ymax-Ymin)/ny
        dz = (Zmax-Zmin)/nz
        mult_dv = 1/(2.*dx*dy*dz*1e-12)
        step = (Xmax-Xmin)/nx
        const_a_p = 7.81441e-9
        
        iteration = iterations[i]
        read, field = utils.bo_file_load(os.path.join(d,'statdata/el/InitialPos'), iteration,nx=nx,ny=ny,ftype='txt',fmt='%d.0000',transpose = 1, verbose = 1, archive = None)
        print(field.shape)
        m = field.max()
        print(m)
        ax1 = fig.add_subplot(1, 3, i+1)
        ax1.imshow(field, origin='lower', cmap = 'Greens', extent=[Xmin, Xmax, Ymin, Ymax], norm=clr.LogNorm(clip='True', vmin = 1e-4*m, vmax = 1e-1*m))
        ax1.set_xlim([-25, 25])
        ax1.set_ylim([-15, 15])
        ax1.set_xlabel('x, $\mu$m')
        ax1.set_ylabel('z, $\mu$m')
        ax1.text(-35, 15, labels[i])
        ax1.text(13, 12, labels1[i])
    
    picname = os.path.join(picspath, 'volume.png')
    plt.tight_layout()
    plt.savefig(picname, dpi=256)            

    
if __name__ == '__main__':
    main()
