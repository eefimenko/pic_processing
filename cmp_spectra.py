#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils

def read_file(file):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    n = sum(array)
    array[0] = 0
    n1 = sum(array)
    m = max(array)
   
    if m != 0:
        for i in range(len(array)):
            array[i] /= m
    f.close()
    return array, n, n1

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def main():
    picspath = 'pics'
    
    config = utils.get_config(sys.argv[1] + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW']) 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
    print num, T
    nph_t = []
    nph1_t = []
    ne_t = []
    np_t = []
    step = x0*y0*1e15/T 
    
    iter0 = int(sys.argv[2])
    path = '/statdata/ph/EnSp/'
    nepath = '/data/NeTrap/'
    nppath = '/data/NposTrap/'
    nmax = utils.num_files(sys.argv[1] + path)
#    axis = create_axis(nmax, step)
    axis = []
    t = 0
    f = open('particles_%d.dat' % (ppw),'w')
    for i in range(nmax):
        sp1, nph, nph1 = read_file(sys.argv[1] + path + '%.4f.txt'%i)
        print sys.argv[1] + nepath + '%06d.txt'%(i)
        if i >= iter0:
            e = read_trap(sys.argv[1] + nepath + '%06d.txt'%(i))
#            p = read_trap(sys.argv[1] + nppath + '%06d.txt'%(i))
        else:
            e = 0
#            p = 0
        if nph >0:
            nph_t.append(nph)
            nph1_t.append(nph1)
            ne_t.append(e)
#            np_t.append(p)
            axis.append(t)
            t += step
            f.write('%lf %le %le %le\n'%(t,nph,nph1,e))
    #    sp2 = read_file(sys.argv[1] + path + '%.4f.txt'%(iter0+2))
    f.close()

    m = max(ne_t)
    for i in range(len(ne_t)):
        ne_t[i] /= m

    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(1,1,1)
    ax1.set_yscale('log')
    
    ax1.plot(axis, nph_t, 'r')
    ax1.plot(axis, nph1_t, 'g')
    ax2 = ax1.twinx()
    ax2.plot(axis, ne_t, 'b')
#    ax2.plot(axis, np_t, 'k')
    ax1.set_xlim([0,9])
#    ax1.plot(sp2, 'g')
#    ax1.set_ylim(1e-2, 1)
    ax2.set_yscale('log')
    plt.savefig('particles_%d.png' % (ppw))
    plt.close()
#    plt.show()
    
if __name__ == '__main__':
    main()
