#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils

def main():
    power = [3, 5, 10]
    ph_en = [150592, 268744, 867987]
    ph_en_sph = [61517.3, 91118.6, 253337]
    eff = []
    eff_sph = []
    en_1per_ph = [0.18, 0.245, 0.466]
    en_1per_el = [0.174, 0.201, 0.255]
    en_1per_sph_ph = [0.174, 0.230, 0.436]
    en_1per_sph_el = [0.151, 0.164, 0.210]
    mp.rcParams.update({'font.size': 12})
    
    picspath = '.'
    for i in range(len(power)):
        energy = 1.05 * 30e-15 * power[i] * 1e15 * 1e7 
        eff.append(ph_en[i]/energy*1e4)
        eff_sph.append(ph_en_sph[i]/energy*1e4)

    fig1 = plt.figure(num=None, figsize = (5,3.5))
    ax1 = fig1.add_subplot(1,1,1)
    
    p, = ax1.plot(power, eff, 'r', label = 'Wire')
    p, = ax1.plot(power, eff_sph, 'b', label = 'Sphere')
    ax1.set_xlabel('P, PW')
    ax1.set_ylabel('Efficiency, $\\times 10^{-4}$')
    ax1.text(1, 2.8, '(a)')
    plt.legend(loc = 'upper left', frameon=False)
    picname1 = picspath + '/' + 'Efficiency' + ".png"
    fig1.tight_layout()
    fig1.savefig(picname1, dpi = 256)

    fig1 = plt.figure(num=None, figsize = (5,3.5))
    ax1 = fig1.add_subplot(1,1,1)
    
    p, = ax1.plot(power, en_1per_ph, 'r', label = '$\hbar\omega$ (Wire)')
    p, = ax1.plot(power, en_1per_sph_ph, 'r--', label = '$\hbar\omega$ (Sphere)')
    p, = ax1.plot(power, en_1per_el, 'b', label = '$e^{-}$ (Wire)')
    p, = ax1.plot(power, en_1per_sph_el, 'b--', label = '$e^{-}$ (Sphere)')
    ax1.set_xlabel('P, PW')
    ax1.set_ylabel('W$_{1\%}$, GeV')
    ax1.text(1, 0.47, '(b)')
    plt.legend(loc = 'upper left', frameon=False)
    picname1 = picspath + '/' + 'Energy' + ".png"
    fig1.tight_layout()
    fig1.savefig(picname1, dpi = 256)
    print(picname1)

if __name__ == '__main__':
    main()
