#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def distribution(i, dx):
    x = (i+1)*dx
    return math.exp(-10*x)

def spectra(distr, dx, shift):
    res = [0]*len(distr)
    for i in range(len(distr)):
         res[i] = distr[i]*(i+shift)*dx
    return res

def main():
    xmax = 1.
    N1 = 100
    N2 = 1000
    dx1 = xmax/N1
    dx2 = xmax/N2
    distr1 = []
    distr2 = []
    
    for i in range(N1):
        distr1.append(distribution(i, dx1))
    for i in range(N2):
        distr2.append(distribution(i, dx2))
    axis1 = create_axis(N1,dx1)
    axis2 = create_axis(N2,dx2)
    sp1 = spectra(distr1, dx1, 0.5)
    sp2 = spectra(distr2, dx2, 0.5)
   
    fig, ax1 = plt.subplots()
   
    pe1, = ax1.plot(axis1, sp1, 'g')
    pe2, = ax1.plot(axis2, sp2, 'g')
    

    plt.show()
    
if __name__ == '__main__':
    main()
