#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys

LightVelocity = 2.99792e+10

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])

def edw(w, k,r,t, delay):
    r += 1e-10
    return 2. * math.cos(w*t + delay) * ( (k*k/r - 1/(r*r*r))*math.sin(k*r) + k/(r*r)*math.cos(k*r))

def hdw(w, k,r,t, delay):
    r += 1e-10
    return 2. * math.sin(w*t + delay) * ( -k*k/r*math.cos(k*r) + k/(r*r)*math.sin(k*r))

def main():
    dir1 = './'
    
    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nixpath = 'data/Proton2Dx'
    niypath = 'data/Proton2Dy'
    nizpath = 'data/Proton2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nphxpath = 'data/Photon2Dx'
    nphypath = 'data/Photon2Dy'
    nphzpath = 'data/Photon2Dz'
    picspath = 'pics'
    config1 = utils.get_config(dir1 + "/ParsedInput.txt")
   
    wl = float(config1['Wavelength'])
    Xmax = float(config1['X_Max'])/wl #mkm to wavelength
    Xmin = float(config1['X_Min'])/wl #mkm to wavelength
    Ymax = float(config1['Y_Max'])/wl #mkm to wavelength
    Ymin = float(config1['Y_Min'])/wl #mkm to wavelength

    nx1 = int(config1['MatrixSize_X'])
    ny1 = int(config1['MatrixSize_Y'])
    nz1 = int(config1['MatrixSize_Z'])
    delay = float(config1['delay'])
    k = float(config1['k'])
    w = float(config1['Omega'])
    dt = float(config1['TimeStep'])
    nn = int(config1['BOIterationPass'])

    print 'nx1 = ' + str(nx1)
    print 'ny1 = ' + str(ny1)
    print 'nz1 = ' + str(nz1)

    dx1 = float(config1['Step_X'])
    dy1 = float(config1['Step_Y'])
    dz1 = float(config1['Step_Z'])
    ppw = int(config1['PeakPowerPW'])
   
    dv1 = 2*dx1*dy1*dz1
    a0 = 3.0e11 * math.sqrt(ppw/10.)
#    a0 = math.sqrt(3. * ppw * 1e22/ LightVelocity)
    axis1 = create_axis(nx1, (Xmax-Xmin)/nx1, Xmin)
    delta = 1
    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(dir1 + expath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(dir1 + expath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    me = abs(edw(w,k,0,0,0))
    mh = abs(hdw(w,k,0.35*wl,2*math.pi/w/4.,0))
    delay = 3.14486539
    ph = []
    for i in range(nmin, nmax, delta):
        ez_t = []
        bz_t = []
        t = i * dt * nn
        fig = plt.figure()
        picname = picspath + '/' + "cmp_sprof%06d.png" % (i,)
        print picname
        
        ezx = fig.add_subplot(2,1,1)
        bxx = fig.add_subplot(2,1,2)

        for n in range(len(axis1)):
            x = axis1[n]*wl
            ez_t.append(abs(edw(w,k,x,t,delay))/me*a0)
            bz_t.append(abs(hdw(w,k,x,t,delay))/mh*a0*0.654)
        name = dir1 + '/' + bzpath + '/' + "%06d.txt" % (i,)
        fieldb1 = read_field(name,nx1,ny1)
        profb1 = fieldb1[:][ny1/2]
       
        name = dir1 + '/' + ezpath + '/' + "%06d.txt" % (i,)
        fielde1 = read_field(name,nx1,ny1)
        profe1 = fielde1[:][ny1/2]
                
        ezx.set_xlim([Xmin, Xmax])
        bxx.set_xlim([Xmin, Xmax])
        print profe1[nx1/2]/max(ez_t)
        phase1 = np.arccos(max(profe1)/a0)-w*t
#        ph.append(phase1)
        q = int(abs(phase1)/(2*math.pi))
        ph0 = np.unwrap([phase1 + (q+1)*2*math.pi])
        print phase1, ph0
        ph.append(ph0)
        ezx.plot(axis1, profe1, 'r', label = 'x1')
        ezx.plot(axis1, ez_t, 'b', label = 'th')
#        ezx.plot(axis2,profe2,'b', label = 'x2')
        
        bxx.plot(axis1,profb1,'r', label = 'x1')
        bxx.plot(axis1, bz_t, 'b', label = 'th')
#        bxx.plot(axis2,profb2,'b', label = 'x2') 

        plt.savefig(picname)
        plt.close()
#    plt.show()
    fig = plt.figure()
    ex = fig.add_subplot(1,1,1)
    ex.plot(ph)
    plt.show()
if __name__ == '__main__':
    main()
