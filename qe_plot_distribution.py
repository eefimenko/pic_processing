#!/usr/bin/python
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np
import matplotlib as mp
from matplotlib.font_manager import FontProperties
import os

def main():
    path = './'
    picspath = 'pics/'
    if len(sys.argv) == 2:
        picspath = sys.argv[1]
    working_dir = os.getcwd()
    if '12beams' in working_dir:
        profile_ = '12beams'
    else:
        profile_ = 'ideal'
    if 'mdipole' in working_dir:
        type_ = 'mdipole'
    else:
        type_ = 'edipole'
    if '1mkm' in working_dir:
        size_ = '1mkm'
    elif '2mkm' in working_dir:
        size_ = '2mkm'
    elif '3mkm' in working_dir:
        size_ = '3mkm'
    else:
        size_ = 'uns'
    fontsize = 22
    dirs = ['nc_0.001', 'nc_30', 'nc_1000']
    config = utils.get_config(dirs[0] + "/ParsedInput.txt")
    wl = float(config['Wavelength'])
    xmax = float(config['X_Max'])/wl #mkm
    xmin = float(config['X_Min'])/wl #mkm
    ymax = float(config['Y_Max'])/wl #mkm
    ymin = float(config['Y_Min'])/wl #mkm
    zmax = float(config['Z_Max'])/wl #mkm
    zmin = float(config['Z_Min'])/wl #mkm
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    power = int(config['PeakPowerPW'])
    dx = float(config['Step_X'])/wl
    dy = float(config['Step_Y'])/wl
    dz = float(config['Step_Z'])/wl
    mult = 1/(2.*dx*dy*dz*wl*wl*wl)
    step = float(config['TimeStep'])*1e15*float(config['BOIterationPass'])
    omega = float(config['Omega'])
    T = 2 * math.pi/omega
    nt = int(T*1e15/step)
    print nt
    iteration_ = 369

    figures = [utils.eypath, utils.bypath, utils.neypath, utils.nphypath]
    cmaps = ['Reds', 'Blues', 'Greens', 'RdPu']
    titles = ['Electric field', 'Magnetic field', 'Electrons', 'Photons']
    log = [False, False, True, True]
    mult = [1e-11,1e-11, mult, mult]
   

    spx_ = len(dirs)
    spy_ = len(figures)
    labels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h','i', 'j', 'k', 'l', 'm']
    
    verbose = 1
#    plt.rcParams.update({'mathtext.default':  'regular' })
    fig = plt.figure(num=None, figsize=(4.*spy_, 3.4*spx_))
    for k in range(spx_):
        dir_ = dirs[k]
        for j in range(spy_):
           
            mp.rcParams.update({'font.size': fontsize})
            yticks = [-4, -2,0,2, 4]
            xticks = [-4, -2,0,2, 4]
           
            
            if k == spx_ -1:
                xlabel = '$x/\lambda$'
                xticklabels = ['$-4$', '$-2$','$0$','$2$', '$4$']
            else:
                xlabel = ''
                xticklabels = []
                
            if j == 0:    
                ylabel = '$y/\lambda$'
                yticklabels = ['$-4$', '$-2$','$0$','$2$', '$4$']
            else:
                ylabel = ''
                yticklabels = []
                
            if figures[j] == utils.neypath or figures[j] == utils.nphypath:
                ncbarticks = None
            else:
                ncbarticks = 5
                
            if j == 0:
                
                    # fx = utils.bo_file_load(dirs[k]+utils.ezpath,
                    #                         i+n0[k],nx[k],ny[k],verbose=1)
                    # fy = utils.bo_file_load(dirs[k]+utils.bzpath,
                    #                         i+n0[k],nx[k],ny[k],verbose=1)
                    # sf = np.square(fx) + np.square(fy)
                ax = utils.subplot(fig, iteration_, dir_ + '/' + figures[j],
                                   shape = (nx,ny), position = (spx_,spy_,j+spy_*k+1),
                                   extent = [xmin, xmax, ymin, ymax],
                                   cmap = cmaps[j], title = '', #titles[j] % (power),
                                   colorbar = True, logarithmic=log[j], verbose=verbose,
                                   xlim = [-4.,4.], ylim = [-4.,4.], xticks = xticks, yticks = yticks,yticklabels = yticklabels, xticklabels = xticklabels,
                                   xlabel = xlabel, ylabel = ylabel, fontsize=fontsize, mult = mult[j], transpose = 1, ncbarticks = ncbarticks)
            else:
                ax = utils.subplot(fig, iteration_, dir_ + '/' + figures[j],
                                   shape = (nx,ny), position = (spx_,spy_,j+spy_*k+1),
                                   extent = [xmin, xmax, ymin, ymax],
                                   cmap = cmaps[j], title = '', titletype = 'simple',
                                   colorbar = True, logarithmic=log[j], verbose=verbose,
                                   xlim = [-4.,4.], ylim = [-4.,4.], xticks = xticks, yticks = yticks,yticklabels = yticklabels, xticklabels = xticklabels,
                                   xlabel = xlabel, ylabel = ylabel, vmax = 0.5, vmin=0.5,
                                   maximum = 'local', fontsize = fontsize, mult = mult[j], transpose = 1, ncbarticks = ncbarticks)
                
                                  
                
            if j == 0:
                ax.text(-8.0, 3.6, '$(%s)$'%(labels[k*spy_ + j]), fontsize = 24)
            else:
                ax.text(-5.6, 3.6, '$(%s)$'%(labels[k*spy_ + j]), fontsize = 24)
    picname = picspath + '/' + "distribution_" + profile_ + '_' + type_ + '_' + size_ + '.png'
    print picname
    plt.tight_layout()
    plt.savefig(picname, bbox_inches='tight', dpi=64)
    plt.close()

    
    # picname = picspath + '/' + "fig4_full.png"
    # # plt.tight_layout()
    # plt.savefig(picname, bbox_inches='tight', dpi=128)
    # plt.close()
    # plt.show()
    
if __name__ == '__main__':
    main()
