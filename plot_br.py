#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_br(filename):
    f = open(filename, 'r')
    time = []
    ez = []
    power = []
    max_en = []
    av_en = []
    flux_full = []
    flux_100MeV = []
    flux_1GeV = []
    br_full = []
    br_100MeV = []
    br_1GeV = []
    angle = []
    
    for line in f: 
        tmp = [float(x) for x in line.split()]
        time.append(tmp[0])
        ez.append(tmp[1])
        power.append(tmp[2])
        max_en.append(tmp[3])
        av_en.append(tmp[4])
        flux_full.append(tmp[5])
        flux_100MeV.append(tmp[6])
        flux_1GeV.append(tmp[7])
        br_full.append(tmp[8])
        br_100MeV.append(tmp[9])
        br_1GeV.append(tmp[10])
        angle.append(tmp[11])
    return time, ez, power, max_en, av_en, flux_full, flux_100MeV, flux_1GeV, br_full, br_100MeV, br_1GeV, angle 

def main():
    name = sys.argv[1]
    path = '.'
    LightVelocity = 29979245800.0
    time, ez, power, max_en, av_en, flux_full, flux_100MeV, flux_1GeV, br_full, br_100MeV, br_1GeV, angle = read_br(path + '/' + name)
    time_ez = []
    config = utils.get_config(path + "/ParsedInput.txt")
    zmin = float(config['Z_Min'])
    zmax = float(config['Z_Max'])
    r = (zmax - zmin)*0.9*0.5
    dt = r/LightVelocity*1e15
    for i in range(len(time)):
        time_ez.append(time[i] + dt)
    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(3,1,1)
    p, = ax1.plot(time, flux_1GeV, 'r')
    ax2= ax1.twinx()
    p1 = ax2.plot(time_ez, ez, 'b')
#    ax1.set_xlim([10,90])
#    ax2.set_xlim([10,90])
    ax1 = fig.add_subplot(3,1,2)
    p, = ax1.plot(time, flux_100MeV, 'r')
    ax2= ax1.twinx()
    p1 = ax2.plot(time_ez, ez, 'b')
#    ax1.set_xlim([10,90])
#    ax2.set_xlim([10,90])
    ax1 = fig.add_subplot(3,1,3)
    p, = ax1.plot(time, max_en, 'r')
    ax2= ax1.twinx()
    p1 = ax2.plot(time_ez, ez, 'b')
#    ax1.set_xlim([10,90])
#    ax2.set_xlim([10,90])
#    ax1.set_yscale('log')
#    ax1.set_xscale('log')
    plt.show()

if __name__ == '__main__':
    main()
