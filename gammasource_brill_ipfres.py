#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import shutil
import os
import scipy.signal.signaltools as sigtool
import numpy as np
import copy
from matplotlib import gridspec
import matplotlib.colors as clr
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

def make_patch_spines_invisible(ax):
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)
        
def read_ang(file,nx,ny,mult=1.):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index]*mult)
        field.append(row)
    return field

def norm(a, mult=1):
    m = max(a)
    for i in range(len(a)):
        a[i] *= mult/m
    return a


def read_field(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)/norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1e-9)
        field.append(row)
    return field

def read_file(file, de):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
    if 'tr' in file and not 'fs' in file:
        array[0] = 0
#        print 'removed'
        array, de = adjust_array(array, de)
        
    for i in range(len(array)):
        nph  += array[i]
    for i in range(len(array)):
        array[i] *= (i+0.425)
   
    return array, nph

def read_file_sph(file, de):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
    if 'tr' in file and not 'fs' in file:
        array[0] = 0
#        print 'removed'
        array, de = adjust_array(array, de)
       
    for i in range(len(array)):
        nph  += array[i]/(i+0.425)/de
    for i in range(len(array)):
        array[i] /= de
#        array[i] = array[i]/(i+0.425)/de
   
    return array, nph

def check_length(file):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
       
    return len(array)

def adjust_array(array, de):
    n = len(array)
    adj_size = 10
    m = n/adj_size
    a = [0]*m
    for i in range(m):
        tmp = 0
        for j in range(adj_size):
            tmp += array[i*adj_size + j]
        a[i] = tmp
    return a, de*adj_size

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def find_max_energy_full(array,step):
    n = len(array)
    full = 0
    imax = 0
    summ = 0
    nfull = 0
    av_en = 0
    n_av = 0
    for i in range(n):
        nfull += array[i]/(i+0.425)
        full += array[i]*step
    
    if nfull > 0:
        av_en = full/nfull
        n_av = int(av_en/step)

    for i in range(1,n):
        summ = summ + array[-i]*(n-i+0.425)*step
        if summ > 0.01*full:
            imax = i
            break
    if imax == 0:
        max_en = 0
    else:
        max_en = (n-imax+0.37) * step
#    en1 = max_en*1e-3
#    ratio = en1/step
#    print max_en, av_en
    ratio = 1
    return av_en*1000, max_en, array[n_av], array[-imax]

def read_one_spectrum_sph(path1, nmin):
    config = utils.get_config(path1 + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV']) 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
#    print num, T
    nph_t = []
    nph1_t = []
    ne_t = []
    np_t = []
    step = x0*y0*1e15/T 
    
    dt = y0*float(config['QEDstatistics.OutputIterStep'])*1e15/T

    path = '/statdata/ph/EnSpSph/'
#    nepath = '/data/NeTrap/'
#    nppath = '/data/NposTrap/'
    emin = float(config['QEDstatistics.Emin']) 
    emax = float(config['QEDstatistics.Emax']) 
    ne = int(config['QEDstatistics.OutputN_E'])
    de = (emax - emin)/ne
   
    t = 0
    
    n = check_length(path1 + path + '%.4f.txt'%nmin)
    if 'tr' in path1 and not 'fs' in path1:
        n /= 10
        de *= 10
    axis = create_axis(n, de/ev*1e-9)

    spectrum1 = [0]*n
    nn = 0
    for i in range(nmin,nmin+num):
        name = path1 + path + '%.4f.txt'%i
#        print name
        sp1, nph = read_file_sph(name, de)
        nn += nph
        for k in range(n):
            if nph > 0:
                spectrum1[k] += sp1[k]/nph
    for k in range(n):
        spectrum1[k] /= num
    return spectrum1, axis

def read_one_spectrum(path1, nmin):
    config = utils.get_config(path1 + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV']) 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
#    print num, T
    nph_t = []
    nph1_t = []
    ne_t = []
    np_t = []
    step = x0*y0*1e15/T 
    
    dt = y0*float(config['QEDstatistics.OutputIterStep'])*1e15

    path = '/statdata/ph/EnSp/'
#    nepath = '/data/NeTrap/'
#    nppath = '/data/NposTrap/'
    emin = float(config['QEDstatistics.Emin']) 
    emax = float(config['QEDstatistics.Emax']) 
    ne = int(config['QEDstatistics.OutputN_E'])
    de = (emax - emin)/ne
   
    t = 0
    
    n = check_length(path1 + path + '%.4f.txt'%nmin)
    if 'tr' in path1 and not 'fs' in path1:
        n /= 10
        de *= 10
    axis = create_axis(n, de/ev*1e-9)

    spectrum1 = [0]*n
    nn = 0
    for i in range(nmin,nmin+num):
        name = path1 + path + '%.4f.txt'%i
#        print name
        sp1, nph = read_file(name, de)
        nn += nph
        for k in range(n):
            if nph > 0:
                spectrum1[k] += sp1[k]/nph
    for k in range(n):
        spectrum1[k] /= n
    return spectrum1, axis

def max2d(array):
    return max([max(x) for x in array])

def sum_ang_energy(array):
    nx = len(array)
    ny = len(array[0])
    res = 0
    for i in range(nx):
        for j in range(ny):
            res = res + array[i][j]
    return res

def main():
    picspath = 'pics'
    path = '.'
    enpath = '/statdata/ph/EnSp/'
    angpath = '/statdata/ph/angSpSph/'
    angpath_el = '/statdata/el/angSpSph/'
    angpath_pos = '/statdata/el/angSpSph/'
    ezpath = 'data/E2z'
    nexpath = 'data/Electron2Dx'
    config = utils.get_config(path + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV'])
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])
    dv = 2*dx*dy*dz 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
    dt = (x0*y0*1e15)/T
    num = len(sys.argv)

    ax_ = []
    ax1_ = []
    # ax2_ = []
    ez_t = []
    ne_t = []
    pow_t = []
    max_t = []
    av_t = []
    br_full_t = []
    br_100mev_t = []
    br_1gev_t = []
    br_full1_t = []
    br_100mev1_t = []
    br_1gev1_t = []
   
    t = 0
    omega = float(config['Omega'])
    delay = float(config['delay'])
    duration = float(config['Duration'])
    T = 2*math.pi/omega*1e15
    phi = int(config['QEDstatistics.OutputN_phi'])
    theta = int(config['QEDstatistics.OutputN_theta'])
    nit = int(config['QEDstatistics.OutputIterStep'])
    dt1 = float(config['TimeStep'])*nit
    wl = float(config['Wavelength'])
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    ev = float(config['eV'])
    ne = int(config['QEDstatistics.OutputN_E'])
    nt = int(config['QEDstatistics.OutputN_theta'])
    tmax = float(config.get('QEDstatistics.ThetaMax',0))*180./math.pi
    tmin = float(config.get('QEDstatistics.ThetaMin',0))*180./math.pi
    
    emax = float(config['QEDstatistics.Emax'])/ev*1e-9
    emin = float(config['QEDstatistics.Emin'])/ev*1e-9
    de = (emax - emin)/ne
    dt = (tmax - tmin)/nt
    dee1 = de*1e9*ev # erg
    dtt1 = dt*math.pi/180.
    dv = 2*dx*dy*dz
    electronMass = 9.10938e-28
    electronCharge = 4.8032e-10
    ncr = electronMass * omega * omega/(8. * math.pi * electronCharge * electronCharge)
    const_a_p = 7.81441e-9
    a0 = const_a_p * math.sqrt(ppw*1e22)
    print a0, 1.18997e+8*a0
    mp.rcParams.update({'font.size': 16})
    mp.rc('font', family= 'Calibri')
    
    S = 2 * math.pi * 0.1 * 0.1 * wl * wl * 100 # square of source mm^2, r = 0.1 lambda 
    ds = 1.5e4 # mrad^2
    f = open('br_t.dat', 'r')
    i = 0
    for line in f:
        tmp = line.split()
        ax_.append(float(tmp[0])/T)
        ez_t.append(float(tmp[1])/1.18997e+8/a0)
        pow_t.append(float(tmp[2]))
        max_t.append(float(tmp[3])/(0.511e-3))
        av_t.append(float(tmp[4]))
        ax1_.append((float(tmp[0]) - 2*T)/T)
        br_full_t.append(float(tmp[5]))
        br_100mev_t.append(float(tmp[6]))
        br_1gev_t.append(float(tmp[7])/1e23)
        br_full1_t.append(float(tmp[5])/S/ds)
        br_100mev1_t.append(float(tmp[6])/S/ds)
        br_1gev1_t.append(float(tmp[7])/S/ds)
    # f = open('t_ez_ne.dat')
    # for line in f:
        # tmp = line.split()
        # ax2_.append(float(tmp[0])*T)
        # ne_t.append(float(tmp[2]))
    axt_ne_ = []
    ne_ = []
    f = open('t_ez_ne.dat', 'r')
    for line in f:
        tmp = line.split()
        axt_ne_.append(float(tmp[0]))
        if (float(tmp[0]) < 25):
            ne_.append(float(tmp[2]))
        else:
            ne_.append(0.)
            
        
    print de, int(2./de), (2.*1e-3/de)
    fraction = 2.*1e-3/de
    gev2_pos = int(2./de)
    print max(ez_t), max(br_1gev_t), max(br_1gev1_t)
    print br_1gev1_t[gev2_pos]*fraction 
    fig = plt.figure(num=None, figsize = (24/2.54,8.5/2.54))
    mp.rcParams.update({'font.size': 14})
    current_dir = os.path.realpath(path)
    field_coeff = 1
    if 'shifted' in current_dir or 'distorted' in current_dir:
        field_coeff = 0.9 
    
    ez_a = [0] * len(ez_t)
    for i in range(len(ez_t)):
        t = i*dt1
        ez_a[i] = field_coeff*math.exp(-2.*math.log(2)*(t - delay)/duration*(t - delay)/duration)
    name = path + '/spectra.txt'
    spectra = np.fromfile(name, sep = ' ')
    print spectra.shape
#    gs = gridspec.GridSpec(1, 2, width_ratios=[2, 1]) 
    ax = fig.add_subplot(1,1,1)
    fig.subplots_adjust(left=0.15)
    fig.subplots_adjust(right=0.85)
    ax2 = ax.twinx()
    ax2.spines["left"].set_position(("axes", -0.1))
    make_patch_spines_invisible(ax2)
    # Second, show the right spine.
    ax2.spines["left"].set_visible(True)
    ax2.yaxis.set_label_position('left')
    ax2.yaxis.set_ticks_position('left')
    
    f1, = ax2.plot(ax_, ez_t, 'B', linewidth = 0.5)
    f2, = ax2.plot(ax_, ez_a, 'b:')
    f, = ax.plot(ax1_, br_1gev_t, 'r', linewidth = 1.5)
    ax2.set_ylabel(u'Поле в центре $|E_z|/a_0$ ', color='B')
    ax.set_ylabel(u"Поток фотонов, $\cdot 10^{23}$ с$^{-1}$", color='r')
    #ax.set_xlabel('t/T')
    ax2.set_ylim([0,1])
    print len(br_1gev_t)
    ax1 = ax.twinx()
    
    p1, = ax1.plot(ax1_, pow_t, 'g')
    ax1.set_ylabel(u'Мощность $\gamma$ источника, ПВт', color='g')
    tm = 25
    ax1.set_xlim([0,tm])
    ax1.set_ylim([0,30])
    ax.set_xlim([0,tm])
    ax.yaxis.label.set_color(f.get_color())
    ax1.yaxis.label.set_color(p1.get_color())
    ax2.yaxis.label.set_color(f1.get_color())
    ax.tick_params(axis='y', colors=f.get_color())
    ax1.tick_params(axis='y', colors=p1.get_color())
    ax2.tick_params(axis='y', colors=f1.get_color())
    ax3 = ax.twinx()
    f3, = ax3.plot(axt_ne_, ne_, 'k')
    ncr_ = 3e24
    f4, = ax3.plot([14,25], [ncr_, ncr_], 'k--')
    ax3.set_xlim([0,tm])
    ax3.set_ylim([1e19,1e27])
    ax3.set_yscale('log')
    make_patch_spines_invisible(ax3)
    ax3.spines["right"].set_visible(True)
    ax3.yaxis.set_label_position('right')
    ax3.yaxis.set_ticks_position('right')
    
    ax1.spines["right"].set_position(("axes", 1.12))
    
    # Second, show the right spine.
   
   
    ax3.set_ylabel(u'Концентрация электронов $N_e$ , см$^{-3}$')
#    ax.set_title('Ideal dipole wave')
    plt.legend([f, f1, p1, f3, f4], [u"Поток фотонов ($\hbar \omega$ > 2 ГэВ)", u'Поле в центре $|E_z|/a_0$', u'Мощность $\gamma$ источника', u'Концентрация электронов $N_e$', u'Рел. крит. конц. $3\\times 10^{24}$ см $^{-3}$'], loc='upper left', frameon = False, fontsize = 13)
#    plt.show()
    name = "gs_power_ipfres"
    picname = picspath + '/' + name + ".png"
    
    plt.savefig(picname)
#    picname = picspath + '/' + name + ".pdf"
#    plt.savefig(picname)
    plt.close()
    fig = plt.figure(num=None, figsize = (7,5))
    mp.rcParams.update({'font.size': 16})
    axx = fig.add_subplot(1,1,1, polar=True)
    axx.set_theta_zero_location("N")
    azimuths_p = np.radians(np.linspace(tmin, tmax, nt))
    azimuths_p1 = np.radians(np.linspace(90+tmax, 90+tmin, nt))
    azimuths_n = -np.radians(np.linspace(tmin, tmax, nt))
    azimuths_n1 = -np.radians(np.linspace(90+tmax, 90+tmin, nt))
    zeniths = np.arange(emin, emax, de)
        
    r_p,thetas_p = np.meshgrid(zeniths, azimuths_p)
    r_p1,thetas_p1 = np.meshgrid(zeniths, azimuths_p1)
    r_n,thetas_n = np.meshgrid(zeniths, azimuths_n)
    r_n1,thetas_n1 = np.meshgrid(zeniths, azimuths_n1)
    el = np.reshape(spectra, (ne,nt), order = 'F')
    x = np.array(el).transpose()/(dee1*dtt1)
    print dee1, dtt1
    my_cmap = copy.copy(mp.cm.get_cmap('hot_r')) # copy the default cmap
    my_cmap.set_bad('w')
    ratio = 1e-5
    surf = axx.pcolormesh(thetas_p, r_p, x, cmap = my_cmap, norm=clr.LogNorm(x.max()*ratio, x.max()),shading='gouraud')
    axx.pcolormesh(thetas_p1, r_p1, x, cmap = my_cmap, norm=clr.LogNorm(x.max()*ratio, x.max()),shading='gouraud')
    axx.pcolormesh(thetas_n, r_n, x, cmap = my_cmap, norm=clr.LogNorm(x.max()*ratio, x.max()),shading='gouraud')
    axx.pcolormesh(thetas_n1, r_n1, x, cmap = my_cmap, norm=clr.LogNorm(x.max()*ratio, x.max()),shading='gouraud')
    axx.grid()
    axx.set_rgrids([1,2,3], labels = ['1 GeV', '2 GeV', '3 GeV'], angle=330., label = 'Gev', fontsize = 14)
#    axx.set_rtickslabels(['1 GeV', '2 GeV', '3 GeV', '4 GeV'])
#    cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8]) 
#    cbar = plt.colorbar(surf,  orientation  = 'vertical', cax = cbaxes) 
    # cbar = plt.colorbar(surf,  orientation  = 'vertical', pad = 0.15, fraction = 0.046)
    fig.subplots_adjust(left=0.1,right=0.85)
    # cbar.ax.set_ylabel('$\\frac{\partial W}{\partial(\hbar\omega) \partial \\theta}$', rotation=0)
    thetaticks = np.arange(0,360,45)
    axx.set_thetagrids(thetaticks, frac=1.18)
#    plt.tight_layout()
    picname = picspath + '/' + "gs_angles.png"
#    fig = plt.figure(num=None, figsize = (15,5))
#    mp.rcParams.update({'font.size': 14})
#    picname = picspath + '/' + "gs_spectrum.png"
#    axise = utils.axis(emin,emax,ne)
#    ax = fig.add_subplot(1,1,1)
#    ax.plot(axise, np.sum(x, axis=0))
#    ax.set_yscale('log')
    plt.savefig(picname)
    plt.close()
   
if __name__ == '__main__':
    main()
