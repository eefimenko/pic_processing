#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib as mp
mp.use("Agg")
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np

def read_dn(filename):
    f = open(filename, 'r')
    ax_ = []
    ph_ = []
    el_ = []
    pos_ = []
    for line in f:
        tmp = line.split()
        ax_.append(float(tmp[0]))
        ph_.append(float(tmp[1]))
        el_.append(float(tmp[2]))
        pos_.append(float(tmp[3]))
    f.close()
    return ax_, ph_, el_, pos_

def main():
    el = []
    ph = []
    pos = []
    ne = []
    dirs = ['pulse_nc_0.001_30', 'pulse_nc_0.01_30', 'pulse_nc_0.1_30', 'pulse_nc_1_30', 'pulse_nc_10_30','pulse_nc_30_30', 'pulse_nc_50_30', 'pulse_nc_100_30', 'pulse_nc_1000_30']
    updirs = ['1mkm', '2mkm', '3mkm']
    dashes = [[2,2], [6,1]]
    window = 9
    order = 3
    for ud in updirs:
        fig = plt.figure(figsize = (15,15))
        ax1 = fig.add_subplot(3,1,1)
        ax2 = fig.add_subplot(3,1,2)
        ax3 = fig.add_subplot(3,1,3)
#        ax1.set_yscale('log')
#        ax2.set_yscale('log')
#        ax3.set_yscale('log')
#        ax1.set_ylim([1e-8, 1e-2])
#        ax2.set_ylim([1e-8, 1e-2])
#        ax3.set_ylim([1e-8, 1e-2])
        for d in dirs:
            ax_, ph_, el_, pos_ = read_dn(ud + '/' + d + '/qe_spectra.txt')
            config = utils.get_config(ud + '/' + d + '/ParsedInput.txt')
            n_cr = float(config['n_cr'])
            ne_ = float(config['Ne'])
            Emax = float(config['QEDstatistics.Emax'])
            Emin = float(config['QEDstatistics.Emin'])
            N_E = int(config['QEDstatistics.OutputN_E'])
            de = (Emax - Emin)/N_E/1.6e-12/1e9 # erg -> eV -> GeV
            n = sum(ph_)
            ax1.plot(ax_, np.array(ph_)/n, label = d)
            n = sum(el_)
            ax2.plot(ax_, np.array(el_)/n, label = d)
            n = sum(pos_)
            ax3.plot(ax_, np.array(pos_)/n, label = d)
        plt.legend(loc = 'upper right')
        picname = 'dn_' + ud + '.png'
        plt.savefig(picname)
  
    plt.show()

if __name__ == '__main__':
    main()
