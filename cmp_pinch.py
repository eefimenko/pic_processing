#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])

def norm(array):
    m = max(array)
    a = [0]*len(array)
    for i in range(len(array)):
        a[i] = array[i]/m
    return a

def main():
    expath = 'data/E2x'
    eypath = 'data/E2y'
    ezpath = 'data/E2z'
    bxpath = 'data/B2x'
    bypath = 'data/B2y'
    bzpath = 'data/B2z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nixpath = 'data/Proton2Dx'
    niypath = 'data/Proton2Dy'
    nizpath = 'data/Proton2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nphxpath = 'data/Photon2Dx'
    nphypath = 'data/Photon2Dy'
    nphzpath = 'data/Photon2Dz'
    picspath = 'pics'

    n = len(sys.argv) - 3
    nmin = int(sys.argv[1])
    nmax = int(sys.argv[2])
    delta = 1
    for i in range(nmin, nmax, delta):
        fig = plt.figure(num=None, figsize=(20, 15), dpi=256)
        picname = picspath + '/' + "cmp_pinch%06d.png" % (i,)
        ax1 = fig.add_subplot(4,1,1)
        ax2 = fig.add_subplot(4,1,2)
        ax3 = fig.add_subplot(4,1,3)
        ax4 = fig.add_subplot(4,1,4)
       
        print picname
        for j in range(n):
            path = sys.argv[j+3] + '/'
            config = utils.get_config(path + "/ParsedInput.txt")
            wl = float(config['Wavelength'])
            
            rf = float(config['resize'])
            iteration = i*rf
            Xmax = float(config['X_Max']) #mkm to wavelength
            Xmin = float(config['X_Min']) #mkm to wavelength
            Ymax = float(config['Y_Max']) #mkm to wavelength
            Ymin = float(config['Y_Min']) #mkm to wavelength
            Zmax = float(config['Z_Max']) #mkm to wavelength
            Zmin = float(config['Z_Min']) #mkm to wavelength
            if 'pinch' in path:
                Xmax /= rf
                Xmin /= rf
                Ymax /= rf
                Ymin /= rf
                Zmax /= rf
                Zmin /= rf
            print Xmax, Xmin
            
            nx = int(config['MatrixSize_X'])
            ny = int(config['MatrixSize_Y'])
            nz = int(config['MatrixSize_Z'])

            print 'Nx = ' + str(nx) 
            print 'Ny = ' + str(ny)
            print 'Nz = ' + str(nz)

            dx = (Xmax-Xmin)/nx
            dy = (Ymax-Ymin)/ny
            dz = (Zmax-Zmin)/nz
            print dx
            dv = 2.*dx*dy*dz*rf
            axis = utils.create_axis(nx, dx/wl, Xmin/wl)
#            print axis
            name = path + bzpath + '/' + "%06d.txt" % (iteration,)
            fieldb = read_field(name,nx,ny)
            name = path + ezpath + '/' + "%06d.txt" % (iteration,)
            fielde = read_field(name,nx,ny)
            name = path + nezpath + '/' + "%06d.txt" % (iteration,)
            fieldn = read_field(name,nx,ny, 1./dv)
#            name = path + npzpath + '/' + "%06d.txt" % (iteration,)
#            fieldp = read_field(name,nx,ny, 1./dv)
            name = path + nphzpath + '/' + "%06d.txt" % (iteration,)
            fieldph = read_field(name,nx,ny, 1./dv)
           

            profn = fieldn[:][ny/2]
#            profp = norm(fieldp[:][ny/2])
            profph = fieldph[:][ny/2]
       
            profe = fielde[:][ny/2]
            profb = fieldb[:][ny/2]
            
            ax1.plot(axis,profn, label = path) 
            ax2.plot(axis,profph, label = path)
            ax3.plot(axis,profe, label = path)
            ax4.plot(axis,profb, label = path)
                   
        ax1.set_xlabel('y/lambda')
        ax1.legend(loc='upper left', shadow=True)
        ax2.set_xlabel('y/lambda')
        ax2.legend(loc='upper left', shadow=True)
        ax3.set_xlabel('y/lambda')
        ax3.legend(loc='upper left', shadow=True)
        ax4.set_xlabel('y/lambda')
        ax4.legend(loc='upper left', shadow=True)
        ax1.set_xlim([-0.2, 0.2])
        ax2.set_xlim([-0.2, 0.2])
        ax3.set_xlim([-0.2, 0.2])
        ax4.set_xlim([-0.2, 0.2])
        plt.savefig(picname)
        plt.close()
    
if __name__ == '__main__':
    main()
