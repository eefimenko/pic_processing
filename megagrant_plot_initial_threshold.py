#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import math
import utils
import sys
import numpy as np
print(np.__version__)
import os
from tqdm import *
print(np.__version__)
import pickle
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes, InsetPosition
from mpl_toolkits.axes_grid1.inset_locator import mark_inset


def main():
    fontsize = 18
    mp.rcParams.update({'font.size': fontsize})
    
    picspath = '/home/evgeny/Dropbox/MG'
    path = '.'
    
    
    dirs = ['Input2D_8pw_2023-01-08_14-22-48',
            'Input2D_9pw_2023-01-08_22-15-58',
            'Input2D_10pw_2023-01-06_10-25-24',
            'Input2D_12pw_2023-01-07_23-22-28',
            'Input2D_13pw_2023-01-07_23-20-21',
            'Input2D_15pw_2023-01-06_14-02-55',
            'Input2D_17pw_2023-01-07_01-32-05',
            'Input2D_20pw_2023-01-06_01-54-48',
            'Input2D_30pw_2023-01-06_17-13-02']
    powers = []
    ne_v = []
    np_full = []
    np_full_n0 = []
    
    for d in dirs:
        config = utils.get_config(os.path.join(d, "ParsedInput.txt"))
        BOIterationPass = float(config['BOIterationPass'])
        dt = float(config['TimeStep'])*1e15*BOIterationPass #fs
        power = int(config['PeakPowerPW'])
        ppw = int(config['PeakPower'])*1e-22 #PW
        omega = float(config['Omega'])
        dt = float(config['TimeStep'])*BOIterationPass
        T = 2 * math.pi/omega
        dtt = dt/T
        print("Power = " + str(ppw) + 'PW')
        print("dt = " + str(dt) + 'fs')
        wl = float(config['Wavelength'])
        print(np.__version__) 
        Xmax = float(config['X_Max'])/1e-4 #mkm
        Xmin = float(config['X_Min'])/1e-4 #mkm
        Ymax = float(config['Y_Max'])/1e-4 #mkm
        Ymin = float(config['Y_Min'])/1e-4 #mkm
        Zmax = float(config['Z_Max'])/1e-4 #mkm
        Zmin = float(config['Z_Min'])/1e-4 #mkm

        nx = int(config['MatrixSize_X'])
        ny = int(config['MatrixSize_Y'])
        nz = int(config['MatrixSize_Z'])

        #print('Nx = ' + str(nx)) 
        #print('Ny = ' + str(ny))
        #print('Nz = ' + str(nz))

        dx = (Xmax-Xmin)/nx
        dy = (Ymax-Ymin)/ny
        dz = (Zmax-Zmin)/nz
        mult_dv = 1/(2.*dx*dy*dz*1e-12)
        print(mult_dv*10)
        step = (Xmax-Xmin)/nx
        const_a_p = 7.81441e-9

        volume = 0.
        volume1 = 0.

        filename = os.path.join(d,'number.pkl')
        if os.path.exists(filename):
            with open(filename, 'rb') as f:
                ne_t = pickle.load(f)
                np_t = pickle.load(f)
        filename = os.path.join(d,'number_pos_full.pkl')
        if os.path.exists(filename):
            with open(filename, 'rb') as f:
                np_full_t = pickle.load(f)*mult_dv
        ne_v.append(np.amax(ne_t[0:600]))
        np_full.append(np.amax(np_full_t))

        read, ne_ = utils.bo_file_load(os.path.join(d,'BasicOutput', utils.nezpath), 0, nx=nx,ny=ny, ftype='txt',transpose = 1, verbose = 0, archive = None)

        n0 = 0
        for i_ in range(nx):
            x_ = (i_ - nx//2)*dx
            for j_ in range(ny):
                y_ = (j_ - ny//2)*dy
                if abs(x_) < 20 and abs(y_) < 20:
                    n0 += ne_[i_, j_]

        np_full_n0.append(np.amax(np_full_t)/n0)
                    
        powers.append(power)

    dirs3d = ['Input3D_12Beams_10pw_2023-01-08_22-53-27',
              'Input3D_12Beams_15pw_2023-01-08_21-32-27',
              'Input3D_12Beams_17pw_2023-01-09_22-48-37',
              'Input3D_12Beams_20pw_2023-01-08_16-23-07']
    
    powers3d = []
    ratios = []
    
    for d3 in dirs3d:
        config = utils.get_config(os.path.join(d3, "ParsedInput.txt"))
        ppw = int(config['PeakPowerPW']) #PW
        
        with open(os.path.join(d3,'ratio.txt'),'r') as f:
            line = f.readline()
            r = float(line.split()[0])
        ratios.append(r)
        powers3d.append(ppw)
        print(ratios)
        print(powers3d)
        
    fig = plt.figure(figsize=(7, 5))
    ax1 = fig.add_subplot(1, 1, 1)
    ax1.plot(powers, np_full, 'r', dashes = [], label = 'N$_{e^+}^{\Sigma}$')
    ax1.plot(powers, ne_v, 'b', dashes = [2,2], label = 'N$_{e^-}^{\lambda}$')
    plt.legend(frameon=False, loc='upper left')
    ax1.set_yscale('log')
    ax1.set_xlim([7, 31])
    ax1.set_xlabel('P, PW')
    ax1.set_ylabel('N$_{e^-}^{\lambda}$, N$_{e^+}^{\Sigma}$')
#    axins = zoomed_inset_axes(ax, 2, loc=3)
#    axins=plt.axes([0.1,0.4,0.4,0.6])
    axins = zoomed_inset_axes(ax1, 3,  loc='center right')
#                              bbox_to_anchor=(0.7, 0.75))
#                              bbox_transform=ax.figure.transFigure)
#    x1, x2, y1, y2 = 16.3, 18.3, 0, 0.12 # specify the limits

    axins.set_xlim(8, 10) # apply the x-limits
    axins.set_ylim(600, 20000)
    axins.set_xticks([8,9,10])
    axins.set_yscale('log')
    axins.plot(powers, ne_v, 'b', dashes=[2,2])
    axins.plot(powers, np_full, 'r', dashes = [])
    mark_inset(ax1, axins, loc1=2, loc2=4, linestyle='', fc="none", ec="0.5")
    picname = os.path.join(picspath, 'threshold.png')
    plt.tight_layout()
    plt.savefig(picname, dpi=256)            

    fig = plt.figure(figsize=(7, 5))
    ax1 = fig.add_subplot(1, 1, 1)
    ax1.plot(powers, np_full_n0, 'r', dashes = [], label = 'N$_{e^+}$/N$_0$')
    #ax1.plot(powers, ne_v, 'b', dashes = [2,2], label = 'N$_{e^-}^{\lambda}$')
    ax1.axhline(y=1, alpha=0.9)
    #plt.legend(frameon=False, loc='lower right')
    ax1.set_yscale('log')
    ax1.set_xlim([7, 31])
    ax1.set_xlabel('P, PW')
    ax1.set_ylabel('N$_{\mathrm{e}^+}$/N$_0$')
    ax1.set_yticks([1e-3, 1, 1e3, 1e6, 1e9])
#    axins = zoomed_inset_axes(ax, 2, loc=3)
#    axins=plt.axes([0.1,0.4,0.4,0.6])
    #axins = zoomed_inset_axes(ax1, 3,  loc='upper left', bbox_to_anchor=(0.5,0,1,1))
#                              bbox_to_anchor=(0.7, 0.75))
#                              bbox_transform=ax.figure.transFigure)
#    x1, x2, y1, y2 = 16.3, 18.3, 0, 0.12 # specify the limits

    #ip = InsetPosition(ax1, [0.4, 0.1, 0.3, 0.7]) #posx, posy, width, height
    #axins.set_axes_locator(ip)
    axins = ax1.inset_axes((0.15, 0.6, 0.3, 0.3))
    axins.set_xlim(12, 14) # apply the x-limits
    axins.set_ylim(0.1, 10)
    axins.set_xticks([12,13,14], fontsize = 16)
    axins.set_yticks([0.1, 1, 10])
    axins.set_yticklabels(['0.1', '1', '10'])
    axins.set_yscale('log')
    #axins.plot(powers, ne_v, 'b', dashes=[2,2])
    axins.plot(powers, np_full_n0, 'r', dashes = [])
    axins.axhline(y=1, alpha=0.9)
    #mark_inset(ax1, axins, loc1=2, loc2=4, linestyle='', fc="none", ec="0.5")
    picname = os.path.join(picspath, 'threshold_norm.png')
    plt.tight_layout()
    plt.savefig(picname, dpi=256)

    fig = plt.figure(figsize=(7, 5))
    ax1 = fig.add_subplot(1, 1, 1)
    msize=3
    ax1.plot(powers[:8], np_full_n0[:8], 'r', dashes = [], marker = 'd', markersize=msize, label = 'Ideal (2D)')
    ax1.plot(powers3d, ratios, 'g', dashes = [2,1], marker='o', markersize=msize, label = '12 beams (3D)')
    #ax1.set_ylim([1e-4, 5e4])
    #ax1.plot(powers, ne_v, 'b', dashes = [2,2], label = 'N$_{e^-}^{\lambda}$')
    ax1.axhline(y=1, alpha=0.9)
    plt.legend(frameon=False, loc='upper left', fontsize=14, ncol=2)
    ax1.set_yscale('log')
    ax1.set_xlim([7, 22])
    ax1.set_xlabel('P, PW')
    ax1.set_ylabel('N$_{\mathrm{e}^+}$/N$_0$')
    #ax1.set_yticks([1e-3, 1, 1e3, 1e6, 1e9])
#    axins = zoomed_inset_axes(ax, 2, loc=3)
#    axins=plt.axes([0.1,0.4,0.4,0.6])
    #axins = zoomed_inset_axes(ax1, 3,  loc='upper left', bbox_to_anchor=(0.5,0,1,1))
#                              bbox_to_anchor=(0.7, 0.75))
#                              bbox_transform=ax.figure.transFigure)
#    x1, x2, y1, y2 = 16.3, 18.3, 0, 0.12 # specify the limits

    #ip = InsetPosition(ax1, [0.4, 0.1, 0.3, 0.7]) #posx, posy, width, height
    #axins.set_axes_locator(ip)
    fontsize = 12
    mp.rcParams.update({'font.size': fontsize})
    
    axins = ax1.inset_axes((0.11, 0.54, 0.3, 0.3))
    axins.set_xlim(12, 14) # apply the x-limits
    axins.set_ylim(0.1, 10)
    axins.set_xticks([12,13,14], fontsize = 16)
    axins.set_yticks([0.1, 1, 10])
    axins.set_yticklabels(['0.1', '1', '10'])
    axins.set_yscale('log')
    #axins.plot(powers, ne_v, 'b', dashes=[2,2])
    axins.plot(powers, np_full_n0, 'r', dashes = [], marker='d', markersize=msize)
    axins.axhline(y=1, alpha=0.9)
    box, c1, c2 = mark_inset(ax1, axins, loc1=2, loc2=4, linestyle='', fc="none", ec="0.5")
    plt.setp(box, linewidth=0.1, facecolor="none")
    
    axins = ax1.inset_axes((0.68, 0.08, 0.3, 0.3))
    axins.set_xlim(16, 18) # apply the x-limits
    axins.set_ylim(0.1, 10)
    axins.set_xticks([16,17,18], fontsize = 12)
    axins.set_yticks([0.1, 1, 10])
    axins.set_yticklabels(['0.1', '1', '10'])
    axins.set_yscale('log')
    #axins.plot(powers, ne_v, 'b', dashes=[2,2])
    axins.plot(powers3d, ratios, 'g', dashes = [2,1], marker='o', markersize=msize)
    axins.axhline(y=1, alpha=0.9)
    mark_inset(ax1, axins, loc1=2, loc2=4, linestyle='', fc="none", ec="0.5")
    
    
    picname = os.path.join(picspath, 'threshold_compare1.png')
    plt.tight_layout()
    plt.savefig(picname, dpi=256)

    
if __name__ == '__main__':
    main()
