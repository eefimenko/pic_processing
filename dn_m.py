#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import utils
import sys
from pylab import *

def read_field2d(file,nx,ny,tmin,tmax,de,dt,mult = 1.):
    print file
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    dth = -(tmax-tmin)*3.14159/ny/180. # dtheta
    dph = 2*3.14159 #dphi - integrated over 2 pi
    array = tmp[0]
    print '!!', dth, dph
#    print 'theta=',nx,ny
    field = []
    field1 = []
    for i in range(nx):
        row = []
        row1 = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index]/math.sin((j+0.5)*dth)/de/(i + 0.425)/dth/dph*1e-6/dt) # to mrad^2
            row1.append(array[index]/de/(i + 0.425)/dt)
        field.append(row)
        field1.append(row1)
    return field, field1

def create_axis(n,step, x0):
    axis = []
#    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def sp_theta(a,level=0,de=1.):
    res = [0]*len(a[0])
    for i in range(len(a)):
        for j in range(len(a[0])):
            if i*de > level:
                res[j] += a[i][j]
    return res

def br_theta(a,level,dth,dph):
    print '!', dth, dph
    res = [0]*len(a[0])
    s = [0]*len(a[0])
    s[0] = math.sin((0+0.5)*dth)
    for j in range(1,len(a[0])):
        s[j] = s[j-1] + math.sin((j+0.5)*dth)
       
    for i in range(len(a)):
        for j in range(len(a[0])-1):
            for j1 in range(j+1):
                res[j] += a[i][j1]
                
    for j in range(len(res)):
        if s[j] > 0:
            res[j] /= s[j]*dth*dph*1e6
#    print res, s
    return res

def norm(a):
    m = max(a)
    if m != 0:
        for i in range(len(a)):
            a[i] /= m
    return a

def sp_energy(a):
    res = [0]*len(a)
    for i in range(len(a)):
        for j in range(len(a[0])):
            res[i] += a[i][j]
    return res

def find_max_nz(a):
    n = len(a)
    for i in range(1,len(a)):
        if a[-i] > 300*1e-10:
            n = len(a)-i
            break
    return n

def smooth(a,factor):
    n = len(a)
    n1 = n/factor
    if n1%factor != 0:
        print 'Error'
    res = [0] * n1
    for i in range(n1):
        for j in range(factor):
            res[i] += a[i*factor + j]/factor
    return res

def find_angle_max(a,dt):
    res = [0]*len(a)
    for i in range(len(a)):
        maxj = 0
        maxn = 0.
        for j in range(len(a[0])):
            if a[i][j] > maxn:
                maxj = j
                maxn = a[i][j]
#                print 'here', a[i][j], maxn, maxj, j
        res[i] = maxj*dt
#        print i, maxj, res[j]
#    print res
    return res

def smooth_2d(a,factor1,factor2):
    ne = len(a)
    nt = len(a[0])
    n1 = nt/factor1
    n2 = ne/factor2
    print ne, nt
    res = [[0 for i in range(n1)] for j in range(n2)]
#    print len(res), len(res[0])
    for j in range(n2):
        for i in range(n1):
            for k in range(factor1):
#                print i,j, i*factor+k
                for m in range(factor2):
                    res[j][i] += a[j*factor2+m][i*factor1+k]/factor1/factor2 + 1e-6
    
    return res

def add_sp(res,a):
    num = 0
    for i in range(len(a)):
        for j in range(len(a[0])):
            num += a[i][j]/(i+0.425)
    for i in range(len(a)):
        for j in range(len(a[0])):
            res[i][j] += a[i][j]/num
    return res
def max2d(array):
    return max([max(x) for x in array])
def main():
    path = '.'
    
    picspath = 'pics'
    elpath = '/statdata/el/EnAngSp/'
    phpath = '/statdata/ph/EnAngSp/'
    pospath = '/statdata/pos/EnAngSp/'

    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(path + elpath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(path + elpath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax-nmin) + ' files'
   
    config = utils.get_config(path + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    T = 2 * math.pi/omega*1e15
    step = x0*y0*1e15/T
    nT = int(1./step)
    print nT
    ev = float(config['eV'])
    ne = int(config['QEDstatistics.OutputN_E'])
    nt = int(config['QEDstatistics.OutputN_theta'])
    tmax = float(config['QEDstatistics.ThetaMax'])*180./3.14159
    tmin = float(config['QEDstatistics.ThetaMin'])*180./3.14159
    emax = float(config['QEDstatistics.Emax'])/ev*1e-9
    emin = float(config['QEDstatistics.Emin'])/ev*1e-9
    peakpower = float(config['PeakPower'])*1e-7*1e-15
    const_a_p = 7.81441e-9
    a0 = const_a_p * math.sqrt(peakpower*1e22)
    print 'tmax', tmax, 'tmin', tmin
    de = (emax - emin)/ne
    dt = (tmax - tmin)/nt
    dth = dt * 3.14159/180. 
    dph = 2*3.14159
    factor = 1
    factor2 = 1
    axe = create_axis(ne, de, 0)
    axt = create_axis(nt, dt, tmin)
    axe_f = create_axis(ne/factor2, de*factor2, 0)
    axe_f2 = create_axis(ne/factor2, de*factor2, 0)

    axt_f = create_axis(nt/factor, dt*factor, tmin)
    omega = float(config['Omega'])
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
    num = 1
    print num, T

    for n in range(nmin,nmax):
#        print path + elpath + '%.4f.txt' % (n,)
        
        picname_el = picspath + '/' + "dne%06d.png" % (n,)
        picname_ph = picspath + '/' + "dnp%06d.png" % (n,)
        el = [[0 for i in range(nt)] for j in range(ne)]
        ph = [[0 for i in range(nt)] for j in range(ne)]
#        el = read_field2d(elname,ne,nt)
#        el = read_field2d(posname,ne,nt)
#        elname = path + elpath + '%.4f.txt' % (n,)
#        phname = path + phpath + '%.4f.txt' % (n,)
#        for j in range(num):
        elname = path + elpath + '%.4f.txt' % (n)
        phname = path + phpath + '%.4f.txt' % (n)
#            posname = path + pospath + '%.4f.txt' % (n+j)
        el_,el1_ = read_field2d(elname,ne,nt,tmax,tmin,de,y0)
        ph_,ph1_ = read_field2d(phname,ne,nt,tmax,tmin,de,y0)
#            print len(el), len(el_)
#            el = add_sp(el,el_)
#            ph = add_sp(ph,ph_)
#        m = max2d(el)    

        fig = plt.figure(num=None)
        axel = fig.add_subplot(2,2,3)
        res = smooth_2d(el, factor, factor2)
#        axel.imshow(res, aspect = 'auto',  extent = [tmin, tmax, emin, emax], origin = 'lower', norm=clr.LogNorm())

        axel_t = fig.add_subplot(2,2,1)
        el_t = br_theta(el1_, 0, dth, dph)
        el_tf = smooth(el_t, factor)
        el_t1 = sp_theta(el_, 0, de)
        el_tf1 = smooth(el_t1, factor)
#        el_t2 = br_theta(el, 1, de)
#        el_tf2 = smooth(el_t2, factor)
        axel_t.plot(axt, el_t)
        axel_t.plot(axt_f, el_tf, 'r')
        axel_t.plot(axt_f, el_tf1, 'b')
#        axel_t.plot(axt_f, el_tf2, 'g')
        axel_e = fig.add_subplot(2,2,4)
        el_e = sp_energy(el)
        el_ef = smooth(el_e, factor2)
        axel_e.plot(axe, el_e)
#        axel.contourf(axt_f, axe_f, res, 20)
#        axel.set_yticks([0.1,1,2,3,4,5])
#        axel.set_yticklabels(['0.1','1','2','3','4','5'])
        r = find_angle_max(el,dt)
        r1 = smooth(r, factor)
        out = open('max_t_el%d.dat' % n, 'w')
        for i in range(len(axe_f)):
            out.write('%lf %le\n' % (axe_f[i], r1[i]))
        out.close()
        axel.plot(r1, axe_f, 'k')
#        print res, res1
#        axel.set_yscale('log')
#        axel.set_yticks([0.1,1,2,3,4,5])
#        axel.set_yticklabels(['0.1','1','2','3','4','5'])
        
        axel_e.plot(axe_f, el_ef,'r')
        axel_e.set_xlabel('Energy, GeV')
#        axel_e.set_yscale('log')
        me = find_max_nz(el_e)*de
        mt = find_max_nz(el_t)*dt
#        mt = 1
        axx = fig.add_subplot(2,2,2, polar=True)
        azimuths = np.radians(np.linspace(tmin, tmax, nt/factor))
        azimuths1 = -np.radians(np.linspace(tmin, tmax, nt/factor))
#        print azimuths, tmax
        zeniths = np.arange(emin, emax, de*factor2)
        
        r,thetas = np.meshgrid(zeniths, azimuths)
        r1,thetas1 = np.meshgrid(zeniths, azimuths1)
        x = np.array(res).transpose()
        for i in range(len(x)):
            for j in range(len(x[0])):
                if x[i][j] == 0:
                    x[i][j] = 1e-9
                if x[i][j] < 0:
                    print x[i][j]
#        print thetas.shape, r.shape, x.shape
        axx.contourf(thetas, r, x, 30, norm=clr.LogNorm())
        axx.contourf(thetas1, r1, x, 30, norm=clr.LogNorm())
        axx.set_ylim([0,me])
        
        
        out = open('sp_t_el%d.dat' % n, 'w')
        for i in range(len(axt)/factor):
            out.write('%lf %le\n' % (axt[i*factor], el_tf[i]))
        out.close()

        
        
        axel_e.set_xlim([0,me])
        axel_t.set_xlim([0,mt])
        axel.set_ylim([de*factor,me])
        axel.set_xlim([0,mt])
        axel.set_xlabel('Angle, grad')
        axel.set_ylabel('Energy, GeV')
#        plt.show()
        plt.savefig(picname_el)
        plt.close()

        fig = plt.figure(num=None)
        axel = fig.add_subplot(2,2,3)
        res = smooth_2d(ph, factor, factor2)
#        axel.imshow(res, aspect = 'auto',  extent = [tmin, tmax, emin, emax], origin = 'lower', norm=clr.LogNorm())

        axel_t = fig.add_subplot(2,2,1)
        el_t = br_theta(ph1_, 0, dth, dph)
        el_tf = smooth(el_t, factor)
        el_t1 = sp_theta(ph_, 0, de)
        el_tf1 = smooth(el_t1, factor)
#        el_t2 = br_theta(ph, 1, de)
#        el_tf2 = smooth(el_t2, factor)
        axel_t.plot(axt, el_t)
        axel_t.plot(axt_f, el_tf, 'r', label = 'av')
        axel_t.plot(axt_f, el_tf1, 'b', label = 'diff')
        axel_t.legend(loc = 'upper right')
        axel_t.set_yscale('log')
#        axel_t.plot(axt_f, el_tf2, 'g')
        axel_e = fig.add_subplot(2,2,4)
        el_e = sp_energy(ph)
        el_ef = smooth(el_e, factor)
        axel_e.plot(axe, el_e)
#        axel.contourf(axt_f, axe_f, res, 20)
#        axel.set_yticks([0.1,1,2,3,4,5])
#        axel.set_yticklabels(['0.1','1','2','3','4','5'])
        r = find_angle_max(ph,dt)
        r1 = smooth(r, factor)
        out = open('max_t_%d.dat' % n, 'w')
        for i in range(len(axe_f)):
            out.write('%lf %le\n' % (axe_f[i], r1[i]))
        out.close()
        axel.plot(r1, axe_f, 'k')
#        print res, res1
#        axel.set_yscale('log')
#        axel.set_yticks([0.1,1,2,3,4,5])
#        axel.set_yticklabels(['0.1','1','2','3','4','5'])
        
        axel_e.plot(axe_f, el_ef,'r')
        axel_e.set_xlabel('Energy, GeV')
#        axel_e.set_yscale('log')
        me = find_max_nz(el_e)*de
        mt = find_max_nz(el_t)*dt
#        mt = 1
        axx = fig.add_subplot(2,2,2, polar=True)
        azimuths = np.radians(np.linspace(tmin, tmax, nt/factor))
        azimuths1 = -np.radians(np.linspace(tmin, tmax, nt/factor))
#        print azimuths, tmax
        zeniths = np.arange(emin, emax, de*factor)
        
        r,thetas = np.meshgrid(zeniths, azimuths)
        r1,thetas1 = np.meshgrid(zeniths, azimuths1)
        x = np.array(res).transpose()
        for i in range(len(x)):
            for j in range(len(x[0])):
                if x[i][j] == 0:
                    x[i][j] = 1e-9
                if x[i][j] < 0:
                    print x[i][j]
#        print thetas.shape, r.shape, x.shape
        axx.contourf(thetas, r, x, 30, norm=clr.LogNorm())
        axx.contourf(thetas1, r1, x, 30, norm=clr.LogNorm())
        axx.set_ylim([0,me])
        
        
        out = open('sp_t_%d.dat' % n, 'w')
        for i in range(len(axt)/factor):
            out.write('%lf %le\n' % (axt[i*factor], el_tf[i]))
        out.close()
        
     

        axel_e.set_xlim([0,me])
        axel_t.set_xlim([0,mt])
        axel.set_ylim([de*factor,me])
        axel.set_xlim([0,mt])
        axel.set_xlabel('Angle, grad')
        axel.set_ylabel('Energy, GeV')
#        plt.show()
        plt.savefig(picname_ph)
        plt.close()

if __name__ == '__main__':
    main()
