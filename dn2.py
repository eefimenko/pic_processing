#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mpl
import numpy as np
import utils
import sys
from multiprocessing.dummy import Pool as ThreadPool 
from pylab import *
from matplotlib.lines import Line2D as l2d
from matplotlib.patches import Arc as arc


reload(sys)
sys.setdefaultencoding('utf8')

ppath = ''
ne = 0
nt = 0

def get_angle_plot(line1, line2, offset = 1, color = None, origin = [0,0], len_x_axis = 1, len_y_axis = 1):

    l1xy = line1.get_xydata()
    print l1xy
    # Angle between line1 and x-axis
    slope1 = (l1xy[1][1] - l1xy[0][1]) / float(l1xy[1][0] - l1xy[0][0])
    angle1 = abs(math.degrees(math.atan(slope1))) # Taking only the positive angle

    l2xy = line2.get_xydata()

    # Angle between line2 and x-axis
    slope2 = (l2xy[1][1] - l2xy[0][1]) / float(l2xy[1][0] - l2xy[0][0])
    angle2 = abs(math.degrees(math.atan(slope2)))

    theta1 = min(angle1, angle2)
    theta2 = max(angle1, angle2)

    angle = theta2 - theta1

    if color is None:
        color = line1.get_color() # Uses the color of line 1 if color parameter is not passed.

    return arc(origin, len_x_axis*offset, len_y_axis*offset, 0, theta1, theta2, color=color, label = str(angle)+u"\u00b0")

def read_field(file,nx,ny,jmin=0,jmax=sys.maxint,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            if i >= jmin and i < jmax:
                row.append(array[index])
            else:
                row.append(0.)
        field.append(row)
    return field

def read_2field(file1, file2, nx,ny,mult = 1.):
    f1 = open(file1, 'r')
    f2 = open(file2, 'r')
    tmp1 = [[float(x)*mult for x in line.split()] for line in f1]
    tmp2 = [[float(x)*mult for x in line.split()] for line in f2]
    f1.close()
    f2.close()
    array1 = tmp1[0]
    array2 = tmp2[0]
    array1 = norm(array1)
    array2 = norm(array2)
    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            if j >= 0 and j < ny/2:
                row.append(array1[index])
            else:
                row.append(array2[index])
        field.append(row)
    return field

def find_05_level(a):
    m = max(a)
    res = 0
    for i in xrange(1,len(a)):
        if a[i-1] < 0.5*m and a[i] >= 0.5*m:
            res = i
            break
    return i

def find_level(a, level=0.5):
    m = max(a)
    res = 0
    for i in xrange(1,len(a)):
        if a[i-1] < level*m and a[i] >= level*m:
            res = i
            break
    return i

def save1d(a, file):
    f = open(file, 'w')
    for i in range(len(a)):
        f.write('%e ' % a[i])
    f.close()

def read1d(file):
    f = open(file, 'r')
    tmp = f.readline().split()
    a = []
    for i in range(len(tmp)):
        a.append(float(tmp[i]))
    f.close()
    return a

def write_field2d(a, file):
    print file
    f = open(file, 'w')
    for j in range(len(a[0])):
        for i in range(len(a)):
            f.write('%e ' % a[i][j])
    f.close()

def read_field2d(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    array = np.fromfile(f, sep=' ')
    f.close()
    print file, len(array), nx,ny
    res = np.reshape(array, (nx,ny), order = 'F')
    num = 0
    for i in range(len(res)):
        num += sum(res[i])/(i+0.425)

    if num != 0:
        for i in range(len(res)):
            for j in range(len(res[0])):
                res[i][j] /= num

    return res

def read_saved(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    array = np.fromfile(f, sep=' ')
    f.close()
#    print file, len(array)
    res = np.reshape(array, (nx,ny), order = 'F')
    return res

def create_axis(n,step, x0):
    axis = []
#    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def sp_theta(a,min_energy=0, max_energy=sys.float_info.max, de=1.):
    res = [0]*len(a[0])
#    print a[0]
#    print de, level
    for i in range(len(a)):
        energy = i*de
        if energy >= min_energy and energy < max_energy:
            for j in range(len(a[0])):
                res[j] += a[i][j]
    m = max(res)
#    print res
#    print m
    if m != 0:
        for i in range(len(res)):
            res[i] /= m
    return res

def br_theta(a,level,dth,dph):
    res = [0]*len(a[0])
    s = [0]*len(a[0])
    for i in range(len(a)):
        for j in range(len(a[0])):
            for j1 in range(j):
                res[j] += a[i][j1]
                s[j] += math.sin((j1+0.5)*dth)*dth*dph*1e6
    for j in range(len(res)):
        if s[j] > 0:
            res[j] /= s[j]
    return res

def norm(a):
    m = max2d(a)
    if m != 0:
        for i in range(len(a)):
            for j in range(len(a[0])):
                a[i][j] /= m
                a[i][j] += 1e-10
    return a

def sp_energy(a):
    res = [0]*len(a)
    for i in range(len(a)):
        for j in range(len(a[0])):
            res[i] += a[i][j]
    m = max(res)
    if m != 0:
        for i in range(len(res)):
            res[i] /= m
    return res

def find_max_nz(a):
    n = len(a)
    for i in range(1,len(a)):
        if a[-i] > 1e-8:
            n = len(a)-i
            break
    return n

def smooth(a,factor):
    n = len(a)
    n1 = n/factor
#    if n1%factor != 0:
#        print 'Error'
    res = [0] * n1
    for i in range(n1):
        for j in range(factor):
            res[i] += a[i*factor + j]/factor
    return res

def find_angle_max(a,dt):
    res = [0]*len(a)
    for i in range(len(a)):
        maxj = 0
        maxn = 0.
        for j in range(len(a[0])):
            if a[i][j] > maxn:
                maxj = j
                maxn = a[i][j]
#                print 'here', a[i][j], maxn, maxj, j
        res[i] = maxj*dt
#        print i, maxj, res[j]
#    print res
    return res

def smooth_2d(a,factor_ne,factor_nt):
    ne = len(a)
    nt = len(a[0])
    nt1 = nt/factor_nt
    ne1 = ne/factor_ne
#    print ne, nt, ne1, nt1
    res = np.zeros((ne1,nt1))

    for j in range(ne1):
        for i in range(nt1):
            for k in range(factor_nt):
                for m in range(factor_ne):
                    res[j][i] += a[j*factor_ne+m][i*factor_nt+k]/factor_ne/factor_nt + 1e-15
    
    return res

def add_sp(res,a):
    for i in range(len(a)):
        for j in range(len(a[0])):
            res[i][j] += a[i][j]
    return res

def max2d(array):
    return max([max(x) for x in array])

def process(n):
    phname = ppath + '%.4f.txt' % (n)
    print 'Processing' + phname
    print 'size ', ne,nt
    res = read_field2d(phname,ne,nt)
    return res

def find_max_energy(array,step):
    n = len(array)
    full = 0
    summ = 0
    imax = 0
    max_en = 0
    for i in range(n):
        full += array[i]*step

    for i in range(1,n):
        summ = summ + array[-i]*step
        if summ > 0.01*full:
            imax = i
            break
    if imax == 0:
        max_en = 0
    else:
        max_en = (n-imax) * step

    return max_en

def norm3(a1, a2, a3):
    m1 = max(a1)
    m2 = max(a2)
    m3 = max(a3)
    m = m1
    if (m2 > m):
        m = m2
    if (m3 > m):
        m = m3
    for i in range(len(a1)):
        a1[i] /= m
        a2[i] /= m
        a3[i] /= m
    return a1, a2, a3

def smooth(a,factor):
    n = len(a)
    n1 = n/factor
    if n1%factor != 0:
        print 'Error'
    res = [0] * n1
    for i in range(n1):
        for j in range(factor):
            res[i] += a[i*factor + j]/factor
    return res

def process(ppath, n):
    phname = ppath + '%.4f.txt' % (n)
    print 'Processing' + phname
    res = read_field2d(phname,ne,nt)
    return res
    
def main():
    path = '.'
    
    picspath = 'pics'
    elpath = '/statdata/el/EnAngSp/'
    phpath = '/statdata/ph/EnAngSp/'
    pospath = '/statdata/pos/EnAngSp/'
    expath = 'data/E2x'
    nphxpath = 'data/Photon2Dx'
    nexpath = 'data/Electron2Dx'

    num = len(sys.argv) - 1
    regime = sys.argv[1]
    wtype = 1

    if regime == 'r':
        print 'read file'
    elif regime == 'rs':
        print 'read smoothed file'
    elif regime == 'c':
        print 'calculate file'
    elif regime == 'cn':
        print 'calculate new file'
    elif regime == 'cs':
        print 'calculate smoothed file'
    else:
        print 'Wrong type, correct r - read or c - calculate, rs - read smoothed file, cs - calculate smoothed file'
        sys.exit(0)

    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(path + elpath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[2] 
        nmin = int(sys.argv[2])
        nmax = utils.num_files(path + elpath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[2] + ' to ' + sys.argv[3]
        nmin = int(sys.argv[2])
        nmax = int(sys.argv[3])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[2] + ' to ' + sys.argv[3] + ' with delta ' + sys.argv[4]
        nmin = int(sys.argv[2])
        nmax = int(sys.argv[3])
        delta = int(sys.argv[4])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax-nmin) + ' files'
  
    config = utils.get_config(path + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
#    omega = float(config['Omega'])
#    T = 2 * math.pi/omega*1e15
#    step = x0*y0*1e15/T
#    nT = int(1./step)
#    print nT
    ev = float(config['eV'])
    global ne,nt
    ne = int(config['QEDstatistics.OutputN_E'])
    nt = int(config['QEDstatistics.OutputN_theta'])
    tmax = float(config['QEDstatistics.ThetaMax'])*180./3.14159
    tmin = float(config['QEDstatistics.ThetaMin'])*180./3.14159
    emax = float(config['QEDstatistics.Emax'])/ev*1e-9
    emin = float(config['QEDstatistics.Emin'])/ev*1e-9
    wl = float(config['Wavelength'])
    Xmax = float(config['X_Max'])/wl #mkm
    Xmin = float(config['X_Min'])/wl #mkm
    Ymax = float(config['Y_Max'])/wl #mkm
    Ymin = float(config['Y_Min'])/wl #mkm
    Zmax = float(config['Z_Max'])/wl #mkm
    Zmin = float(config['Z_Min'])/wl #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz
#    peakpower = float(config['PeakPower'])*1e-7*1e-15
#    const_a_p = 7.81441e-9
#    a0 = const_a_p * math.sqrt(peakpower*1e22)
    de = (emax - emin)/ne
    dt = (tmax - tmin)/nt
    dth = dt * 3.14159/180. 
    dph = 2*3.14159
    factor_nt = 1 #for mom
    factor_ne = 1 #for mom
#    factor_nt = 3 #for angular
#    factor_ne = 2 #for angular
    nt1 = nt/factor_nt
    ne1 = ne/factor_ne
    axe = create_axis(ne, de, 0)
    axt = create_axis(nt, dt, tmin)
    axe_f = create_axis(ne/factor_ne, de*factor_ne, 0)
    axe_f2 = create_axis(ne/factor_ne, de*factor_ne, 0)

    axt_f = create_axis(nt/factor_nt, dt*factor_nt, tmin)
#    omega = float(config['Omega'])
#    T = 2 * math.pi/omega*1e15
#    num = int(T/(x0*y0*1e15))
#    num = 120
#    print num, T

    global ppath
    ppath = path + phpath
#    pool = ThreadPool(8)
#    results = pool.map(process, range(nmin, nmax))
#    print len(results)
    summ = np.zeros((ne,nt)) 
    sphname = path + phpath + 'saved%d.txt' %(nmax-nmin)
    s1name = path + phpath + 'sp-1.txt'
    s2name = path + phpath + 'sp-0.1.txt'
    s3name = path + phpath + 'sp-1-0.1.txt'

    if regime == 'c':
        summ = read_saved(sphname, ne,nt)
        summ = norm(summ)

        s_t = sp_theta(summ, min_energy = 2, de=de)
        s1_t = sp_theta(summ, min_energy = 0.1, de=de)
        s2_t = sp_theta(summ, min_energy = 1, de=de)
        save1d(s_t, s1name)
        save1d(s1_t, s2name)
        save1d(s2_t, s3name)
    elif regime == 'cn':
        for i in range(nmin,nmax):
            res = process(ppath,i)
            summ = add_sp(summ, res)
        write_field2d(summ,sphname)  
    elif regime == 'r':
        s_t = read1d(s1name)
        s1_t = read1d(s2name)
        s2_t = read1d(s3name)
    else:
        print 'Wrong regime'

    s_t_sin = []
    s1_t_sin = []
    s2_t_sin = []
    en_j = []
    en1_j = []
    en2_j = []
    tmp = 0
    tmp1 = 0
    tmp2 = 0
    for j in range(len(s_t)):
        tmp += s_t[j]
        tmp1 += s1_t[j]
        tmp2 += s2_t[j]
        s_t_sin.append(s_t[j]/math.sin((j+0.5)*dth))
        s1_t_sin.append(s1_t[j]/math.sin((j+0.5)*dth))
        s2_t_sin.append(s2_t[j]/math.sin((j+0.5)*dth))
#        s_t_sin.append(s_t[j])
#        s1_t_sin.append(s1_t[j])
#        s2_t_sin.append(s2_t[j])
        en_j.append(tmp)
        en1_j.append(tmp1)
        en2_j.append(tmp2)
    for j in range(len(s_t)):
        s_t_sin.append(s_t[-1-j]/math.sin(math.pi/2 + (j+0.5)*dth))
        s1_t_sin.append(s1_t[-1-j]/math.sin(math.pi/2 + (j+0.5)*dth))
        s2_t_sin.append(s2_t[-1-j]/math.sin(math.pi/2 + (j+0.5)*dth))
#        s_t_sin.append(s_t[-1-j])
#        s1_t_sin.append(s1_t[-1-j])
#        s2_t_sin.append(s2_t[-1-j])

    fig = plt.figure(num=None, figsize=(10, 5), tight_layout = True)
    mpl.rcParams.update({'font.size': 16})
        
    factor = 5
    s_t_sin = smooth(s_t_sin, factor)
    s1_t_sin = smooth(s1_t_sin, factor)
    s2_t_sin = smooth(s2_t_sin, factor)
    s_t_sin, s1_t_sin, s2_t_sin = norm3(s_t_sin, s1_t_sin, s2_t_sin)
    
    factor_ne=1
    factor_nt=1
    axt_ = create_axis(nt/factor_nt, dt*factor_nt*math.pi/180., tmin)
    axt = create_axis(2*nt/factor_nt/factor, dt*factor_nt*factor, tmin)
    picname = picspath + '/dn%d.png'%(nmax-nmin)

    ax1 = fig.add_subplot(1,2,1)
    print len(axt), len(axt_), len(s_t_sin)
    ax1.plot(axt, s_t_sin, 'r', label = u'> 1 $GeV$')
    ax1.plot(axt, s2_t_sin, 'g', label = u'0.1-1 $GeV$')
    ax1.plot(axt, s1_t_sin, 'b', label = u'< 0.1 $GeV$')
    plt.legend(loc = 'upper center')
    ax1.set_xlim([0, 180.])
    ax1.set_ylim([1e-5, 1])
    ax1.set_xticks([0, 45, 90, 135, 180])
    ax1.set_xticklabels(['0', '$\pi/4$', '$\pi/2$', '$3\pi/4$', '$\pi$'])
    ax1.set_yscale(u'log')
    ax1.set_xlabel(u'$\\theta$, rad')
    ax1.set_ylabel('$\\frac{1}{\sin \\theta}\\frac{dI}{d\\theta d\phi}$')
    ax1 = fig.add_subplot(1,2,2)
    for i in range(len(axt)):
        axt[i] *= math.pi/180*1000
    ax1.plot(axt, s_t_sin, 'r', label = u'> 1 GeV')
    ax1.plot(axt, s2_t_sin, 'g', label = u'0.1-1 GeV')
    ax1.plot(axt, s1_t_sin, 'b', label = u'< 0.1 GeV')
    ax1.set_xlim([0, 10.])
    ax1.set_ylabel('$\\frac{1}{\sin \\theta}\\frac{dI}{d\\theta d\phi}$')
    ax1.set_xlabel('$\\theta$, mrad')
    cmap1 = mpl.cm.get_cmap('Reds')
    cmap1.set_under('w', False)
#    cmap2 = mpl.cm.get_cmap('Greens')
#    cmap2.set_under('w', False)
    a = plt.axes([.75, .5, .2, .4])
    i = 450
    amax = 1
    name = utils.neypath + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny, jmin=0, jmax=ny/2)
    surf = a.imshow(norm(field), extent=[Xmin, Xmax, Ymin, Ymax], cmap = 'Greens', vmin = 0.0001, vmax = amax, norm = clr.LogNorm())
    name = utils.eypath + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny, jmin=ny/2, jmax=ny)
   
    surf = a.imshow(norm(field), extent=[Xmin, Xmax, Ymin, Ymax], cmap = cmap1, vmin = 0.0001, vmax = amax)
    line1 = l2d([0, 0], [-1.8, 1.8], color='k', linestyle='-', linewidth=0.5)
    line2 = l2d([0, 1.], [0, math.sqrt(3)], color='k', linestyle='-', linewidth=0.5)
    a.add_line(line1)
    a.add_line(line2)
    angle_plot = get_angle_plot(line1, line2, 1)
    a.add_patch(angle_plot)
    a.set_xlim([-2,2])
    a.set_ylim([-2,2])
    a.text(0.15, 0.7, '$\\theta$', fontsize=18)
#    a.set_xlabel('x')
#    a.set_ylabel('z')
    plt.xticks([])
    plt.yticks([])

#    ax2 = ax1.twinx()
#    ax2.plot(axt, s_t1, 'r')
#    ax2.set_yscale('log')
#        ax1.plot(axt, ph_t1, label = '> 1GeV')
#        ax1.plot(axt, ph_t2, label = '> W(1%)')
#    print s_t
    te = find_max_nz(s_t)*dt*factor_nt
    
#    plt.tight_layout()
    plt.savefig(picname)
    plt.close()
    fig = plt.figure(num=None, figsize=(10, 5), tight_layout = True)
    mpl.rcParams.update({'font.size': 14})
    ax1 = fig.add_subplot(1,2,1)
    
    ax1.plot(axt_, en_j, 'r', label = u'> 1 GeV')
    ax1.plot(axt_, en2_j, 'g', label = u'0.1-1 GeV')
    ax1.plot(axt_, en1_j, 'b', label = u'< 0.1 GeV')
#    ax1.set_xlim([0, 10.])
    ax1.set_ylabel('$W, a.u.$')
    ax1.set_xlabel('$\\theta^\prime, rad$')
    ax1.set_xticks([0, math.pi/4, math.pi/2])
    ax1.set_xticklabels(['0', '$\pi/4$', '$\pi/2$'])
    plt.legend(loc = 'upper left', fontsize = 12)
    ax1 = fig.add_subplot(1,2,2)
    
    ax1.plot(axt_, en_j, 'r', label = u'> 1 GeV')
    ax1.plot(axt_, en2_j, 'g', label = u'0.1-1 GeV')
    ax1.plot(axt_, en1_j, 'b', label = u'< 0.1 GeV')
    i1 = find_level(en_j, 0.5)
    print i1
    t1 = [0, en_j[i1]]
    t2 = [axt_[i1], axt_[i1]]
    ax1.plot(t2, t1, 'r', linestyle=':')
    i2 = find_level(en1_j, 0.5)
    t1 = [0, en1_j[i2]]
    t2 = [axt_[i2], axt_[i2]]
    ax1.plot(t2, t1, 'b', linestyle=':')
    i3 = find_level(en2_j, 0.5)
    t1 = [0, en2_j[i3]]
    t2 = [axt_[i3], axt_[i3]]
    print axt_[i1], axt_[i2], axt_[i3]
    ax1.set_xticks([0, axt_[i1], axt_[i2], axt_[i3]])
    ax1.set_xticklabels(['0', '%.2f'%(axt_[i1]), '%.2f'%(axt_[i2]), '%.2f'%(axt_[i3])])
    ax1.plot(t2, t1, 'g', linestyle=':')
    ax1.set_xlim([0, 30.*math.pi/180.])
#    ax1.set_ylim([0, 2500.])
    ax1.set_ylabel('$W, a.u.$')
    ax1.set_xlabel('$\\theta^\prime, rad$')
    
    picname = picspath + '/dn1_%d.png'%(nmax-nmin)
    plt.savefig(picname)
    plt.close()

if __name__ == '__main__':
    main()

'''    
for n in range(nmin,nmax):
        picname = picspath + '/' + "ldn%06d.png" % (n,)
        picname_ph = picspath + '/' + "dnp%06d.png" % (n,)

        if 'c' in regime:
            el = np.zeros((ne,nt))
            ph = np.zeros((ne,nt))
            el1 = np.zeros((ne,nt))
            ph1 = np.zeros((ne,nt))

            for j in range(num):
#                elname = path + elpath + '%.4f.txt' % (n+j)
                phname = path + phpath + '%.4f.txt' % (n+j)
           
#                el_,el1_ = read_field2d(elname,ne,nt,dth)
#                el = add_sp(el,el_)
#                el1 = add_sp(el1,el1_)
                ph_,ph1_ = read_field2d(phname,ne,nt,dth)
                ph = add_sp(ph,ph_)
                ph1 = add_sp(ph1,ph1_)

#            selname = path + elpath + 'c%.4f.txt' % (n)
            sphname = path + phpath + 'c_%.4f.txt' % (n)
            sph1name = path + phpath + 'c1_%.4f.txt' % (n)
            if 's' in regime:
#                el = smooth_2d(el,factor_ne,factor_nt)
                ph = smooth_2d(ph, factor_ne, factor_nt)
                ph1 = smooth_2d(ph1, factor_ne, factor_nt)
#            write_field2d(el, selname)
            write_field2d(ph, sphname)
            write_field2d(ph1, sph1name)

        if 'r' in regime:
#            elname = path + elpath + 'c%.4f.txt' % (n)
            phname = path + phpath + 'c_%.4f.txt' % (n)
            ph1name = path + phpath + 'c1_%.4f.txt' % (n)
            if 's' in regime:
#                el,el1 = read_field2d(elname,ne/factor_ne,nt/factor_nt,dth)
                ph = read_field_1_2d(phname,ne/factor_ne,nt/factor_nt,dth)
                ph1 = read_field_1_2d(ph1name,ne/factor_ne,nt/factor_nt,dth)
            else:
#                el,el1 = read_field2d(elname,ne,nt,dth)
                ph = read_field_1_2d(phname,ne,nt,dth)
                ph1 = read_field_1_2d(ph1name,ne,nt,dth)       
    el = ph1'''

'''        axph = fig.add_subplot(2,5,6)
        if not 's' in regime:
            ph = smooth_2d(ph, factor_ne, factor_nt)
            ph = norm(ph)
            axph.imshow(ph, aspect = 'auto',  extent = [tmin, tmax, emin, emax], origin = 'lower', norm=clr.LogNorm(ph.max()*1e-3, ph.max()))
            axph.set_ylabel('GeV')
            axph.set_xlabel('grad')
            axph.set_xlim([0,1])
            el_e = sp_energy(el)
            ph_e = sp_energy(ph)
            onep_energy = find_max_energy(ph_e,de*factor_ne)
            onep_energy_el = find_max_energy(el_e,de*factor_ne)
            print onep_energy
            el_t = sp_theta(el)
            el_t1 = sp_theta(el, 1, de*factor_ne)
            el_t2 = sp_theta(el, onep_energy_el, de*factor_ne)
            ph_t = sp_theta(ph)
            ph_t1 = sp_theta(ph, 1, de*factor_ne)
            ph_t2 = sp_theta(ph, onep_energy, de*factor_ne)
            axe = create_axis(ne/factor_ne, de*factor_ne, 0)
            axt = create_axis(nt/factor_nt, dt*factor_nt, tmin)
            me = find_max_nz(el_e)*de*factor_ne
            mp = find_max_nz(ph_e)*de*factor_ne
            te = find_max_nz(el_t)*dt*factor_nt
            tp = find_max_nz(ph_t)*dt*factor_nt 
            axel.set_ylim([0,me])
            axph.set_ylim([0,mp])
#        axph.set_xlim([0,20])
#        axel.set_xlim([0,20])
#        fig = plt.figure(num=None)
#        plt.rc('text', usetex=True)
#        mpl.rcParams.update({'font.size': 36})
#        axph = fig.add_subplot(1,1,1)
#        if not 's' in regime:
#            ph = smooth_2d(ph, factor_ne, factor_nt)
#        ph = norm(ph)
#        axph.imshow(ph, aspect = 'auto',  extent = [tmin, tmax, emin, emax], origin = 'lower', norm=clr.LogNorm(ph.max()*1e-3, ph.max()))
#        axph.set_ylabel('$\epsilon_{\gamma}$')
#        axph.set_xlabel('$\\theta$')
#        axph.set_ylim([0,mp])
#        plt.show()

        ax1 = fig.add_subplot(2,5,3)
        ax1.plot(axe, el_e)
        ax1.set_xlim([0, me])
        ax1.set_ylim([1e-4, 1])
        ax1.set_yscale('log')
        ax1.set_xlabel('GeV')
        onep_energy = find_max_energy(ph_e,de*factor_ne)
        ax1 = fig.add_subplot(2,5,4)
        ax1.plot(axt, el_t, label = 'All')
#        ax1.plot(axt, el_t1, label = '> 1GeV')
#        ax1.plot(axt, el_t2, label = '> W(1%)')
        ax1.set_xlim([0, te])
        ax1.set_xlabel('grad')
        plt.legend(loc = 'upper right')
        ax1 = fig.add_subplot(2,5,5)
        ax1.plot(axt, el_t, label = 'All')
#        ax1.plot(axt, el_t1, label = '> 1GeV')
#        ax1.plot(axt, el_t2, label = '> W(1%)')
        ax1.set_xlim([0, 1])
        ax1.set_xlabel('grad')
        plt.legend(loc = 'upper right')

        ax1 = fig.add_subplot(2,5,8)
        ax1.plot(axe, ph_e)
        ax1.set_xlim([0, me])
        ax1.set_ylim([1e-4, 1])
        ax1.set_xlabel('GeV')
        ax1.set_yscale('log')
        onep_energy = find_max_energy(ph_e,de*factor_ne)
        print onep_energy
        ax1 = fig.add_subplot(2,5,9)
        ax1.plot(axt, ph_t, label = 'All')
#        ax1.plot(axt, ph_t1, label = '> 1GeV')
#        ax1.plot(axt, ph_t2, label = '> W(1%)')
        ax1.set_xlim([0, te])
        ax1.set_xlabel('grad')
        plt.legend(loc = 'upper right')
        ax1 = fig.add_subplot(2,5,10)
        ax1.plot(axt, ph_t, label = 'All')
#        ax1.plot(axt, ph_t1, label = '> 1GeV')
#        ax1.plot(axt, ph_t2, label = '> W(1%)')
        ax1.set_xlim([0, 1])
        ax1.set_xlabel('grad')
        plt.legend(loc = 'upper right')

#        axx = fig.add_subplot(2,5,2, polar=True)
#        azimuths_p = np.radians(np.linspace(tmin, tmax, nt/factor_nt))
#        azimuths_n = -np.radians(np.linspace(tmin, tmax, nt/factor_nt))
#        zeniths = np.arange(emin, emax, de*factor_ne)
        
#        r_p,thetas_p = np.meshgrid(zeniths, azimuths_p)
#       r_n,thetas_n = np.meshgrid(zeniths, azimuths_n)
#        x = np.array(el).transpose()
        
#        axx.pcolormesh(thetas_p, r_p, x, norm=clr.LogNorm(x.max()*1e-3, x.max()))
#        axx.pcolormesh(thetas_n, r_n, x, norm=clr.LogNorm(x.max()*1e-3, x.max()))
        
#        axx.set_rscale('log')
#        axx.set_ylim([0.1,me])
#        print max2d(el), max2d(ph), de*factor_ne

#        axx = fig.add_subplot(2,5,7, polar=True)
#        x = np.array(ph).transpose()
       
#        axx.pcolormesh(thetas_p, r_p, x, norm=clr.LogNorm(x.max()*1e-3, x.max()))
#        axx.pcolormesh(thetas_n, r_n, x, norm=clr.LogNorm(x.max()*1e-3, x.max()))
#        axx.set_rscale('log')
#        axx.set_ylim([0.1,mp])'''
