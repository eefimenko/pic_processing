#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def main():
    picspath = 'pics/'
    power = [7.5, 7.7, 8, 9, 10, 12, 15, 17, 19]
    gamma = [400, 250, 170, 110, 84, 79, 79, 78, 78]
    gamma1 = [120, 73, 60, 35, 21, 14, 11, 10, 10]
    gamma2 = []
    fig = plt.figure(num=None, figsize=(4., 3), dpi=256)
    ax1 = fig.add_subplot(1,1,1)
    msize = 6
    for i in range(len(gamma)):
        gamma[i] /= 3.
        gamma1[i] /= 3.
        gamma2.append(gamma[i] - gamma1[i])
    p, = ax1.plot(power, gamma, 'r-o', label = 'Stage 3 + stage 4 ', fillstyle = 'none', markersize = msize)
    p, = ax1.plot(power, gamma1, 'b-v', label = 'Stage 3', fillstyle = 'none', markersize = msize)
    p, = ax1.plot(power, gamma2, 'g-x', label = 'Stage 4', fillstyle = 'none', markersize = msize)
    plt.minorticks_on()
    ax1.set_xlabel('$P, PW$')
    ax1.set_ylabel('Settling time, $T$')
    ax1.set_yscale('log')
    ax1.set_ylim([0,150])
    ax1.set_xlim([7,15])
    xticks = [8,10,12,14]
    xticklabels = ['$%d$'%(x) for x in xticks]
    ax1.set_xticks(xticks)
    ax1.set_xticklabels(xticklabels)
    ticks = [3,6,12,24,48,100]
    
    ax1.set_yticks(ticks)
    ax1.set_yticklabels(ticks)
    plt.legend(loc = 'upper right', fontsize = 13, frameon=False)
#    plt.grid()
    picname = picspath + '/' + "fig5.png"
    plt.savefig(picname, dpi=256, bbox_inches='tight')
    plt.close()
#    plt.show()

if __name__ == '__main__':
    main()
