#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import os
import numpy as np

def read_profiles(path,i):
    xy2dbz_path = 'BasicOutput/data/XZ2DBz'
    xy2dby_path = 'BasicOutput/data/XZ2DBy'
    config = utils.get_config(path+"/ParsedInput.txt")
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    wl = float(config['Wavelength'])
    dx = float(config['GridStep'])
    dy = float(config['GridStep'])
    dz = float(config['GridStep'])
    Xmax = float(config['X_Max'])/wl #mkm to wavelength
    Xmin = float(config['X_Min'])/wl #mkm to wavelength
    
    path_bz = os.path.join(path, xy2dbz_path)
    path_by = os.path.join(path, xy2dby_path)
    #print(path_bz, path_by)
    read, fieldbz = utils.bo_file_load(path_bz,i,nx,ny)
    read, fieldby = utils.bo_file_load(path_by,i,nx,ny)
    nx_max, ny_max = np.unravel_index(np.argmax(fieldbz, axis=None), fieldbz.shape)
    fieldn = np.sqrt(fieldbz**2 + fieldby**2)
    #print(np.amax(fieldn)/1.19e8, nx_max, ny_max)
    prof1 = fieldn[:,ny//2]
    prof2 = fieldn[nx_max,:]
    ax = utils.create_axis(nx, (Xmax-Xmin)/nx, Xmin)
    return ax, prof1, prof2

def main():
    dirs = sys.argv[2:]
    iteration = int(sys.argv[1])
    n = len(dirs)

    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(1,2,1)
    ax2 = fig.add_subplot(1,2,2)
    
    for i in range(n):
        path = dirs[i]
        #iteration = iterations[i]
        axis, p1, p2 = read_profiles(path, iteration)
        ax1.plot(axis, p1, label = path)
        ax1.set_xlabel('z')
        ax1.set_ylabel('|B|')
        ax2.plot(axis, p2/np.amax(p2), label = path)
        ax2.set_xlabel('x')
        ax2.set_ylabel('|B|')
        print(np.argmax(p1), np.max(p1)/1.19e8, (len(p1)/2 - np.argmax(p1))*(axis[1]-axis[0]))
        #ax1.imshow(f)
    ax1.legend()
    plt.show()

if __name__ == '__main__':
    main()
