#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_file(filename):
    f = open(filename, 'r')
    axis = []
    e2max = []
    b2max = []
    nemax = []
    gammaav = []
    wp = []
    ndtwp = []
    ndxne = []
    dxne = []

    a0 = math.sqrt(27./10)*3e11
    for line in f:
        tmp = line.split()
        axis.append(float(tmp[0])/3.)
        e2max.append(float(tmp[1])/a0)
        b2max.append(float(tmp[2])/(a0*0.65))
        nemax.append(float(tmp[3]))
        gammaav.append(float(tmp[4]))
        wp.append(float(tmp[5]))
        ndtwp.append(float(tmp[6]))
        ndxne.append(float(tmp[7]))
        dxne.append(float(tmp[8])*1e4/0.9)
    t0 = axis[0]
    for i in range(len(axis)):
        axis[i] -= t0

    return axis, e2max, b2max, nemax, gammaav, wp, ndtwp, ndxne, dxne

def main():
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
    paths = ['27pw', '27pw_x2', '27pw_x4', '27pw_x8r', '27pw_x16r', '27pw_x32r']
    n = len(paths)
#    n = len(sys.argv) - 1
#    fig1 = plt.figure(figsize=(5,7.5))
    fig1, axs = plt.subplots(3, 1, sharex=True, figsize=(5,7.5))
# Remove horizontal space between axes
    fig1.subplots_adjust(hspace=0)
#    ax1 = fig1.add_subplot(3,1,1, sharex=True)
    ax1 = axs[2]
    ax1.set_ylabel('$E/E_0$')
    ax1.set_yticks([0,0.5,1,1.5,2])
    ax1.set_yticklabels(['$0$','$0.5$','$1$','$1.5$','$2$'])
    ax1.set_ylim([0,2.5])
#    ax1.set_xlabel('$t/T$')
    ax1.axhline(y=1, color = 'lightgray')
#    ax2 = fig1.add_subplot(3,1,2, sharex=True)
    ax2 = axs[0]
    ax2.set_ylabel('$B/B_0$')
    ax2.set_yticks([0,1,2,3,4,5,6,7])
    ax2.set_yticklabels(['$0$','$1$','$2$','$3$','$4$','$5$','$6$','$7$'])
    ax2.set_ylim([0,8])
#    ax2.set_xlim([-0.01,0.8])
#    ax2.set_xlabel('$t/T$')
    ax2.axhline(y=1, color = 'lightgray')
#    ax3 = fig1.add_subplot(3,1,3, sharex=True)
    ax3 = axs[1]
    ax3.set_ylabel(u'$n_e, cm^{-3}$')
    ax3.set_yscale('log')
    ax3.set_xlim([-0.01,0.8])
    ax3.set_ylim([1e25,5e28])
    ax3.set_yticks([1e25,1e26,1e27, 1e28])
    ax1.set_xticks([0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8])
    ax1.set_xticklabels(['$0$','$0.1$','$0.2$','$0.3$','$0.4$','$0.5$','$0.6$','$0.7$','$0.8$'])
    ax3.set_yticklabels(['$10^{25}$','$10^{26}$','$10^{27}$','$10^{28}$'])
    ax1.set_xlabel('$t/T$')
    fig1.subplots_adjust(left=0.15)
    colors = ['g', 'c', 'b', 'm', 'firebrick', 'r', 'k']
    iterations = [23, 16, 24, 40, 70, 112]
    labels = ['$n_{\lambda} = 115$','$n_{\lambda} = 230$','$n_{\lambda} = 460$','$n_{\lambda} = 920$','$n_{\lambda} = 1840$','$n_{\lambda} = 3680$'   ]
    dt = 0
    for i in range(n):
        path = paths[i]
        axis, e2max, b2max, nemax, gammaav, wp, ndtwp, ndxne, dxne = read_file(path + '/' + 'analyse_inst.dat')
        dt = axis[1] - axis[0]
        e1, = ax1.plot(axis, e2max, colors[i], label = labels[i])
#        e1_, = ax1.plot([iterations[i]*dt], e2max[iterations[i]], colors[i], marker = 'h') 
        e2, = ax2.plot(axis, b2max, colors[i], label = labels[i])
#        e2_, = ax2.plot([iterations[i]*dt], b2max[iterations[i]], colors[i], marker = 'h') 
        e3, = ax3.plot(axis, nemax, colors[i], label = labels[i])
#        e3_, = ax3.plot([iterations[i]*dt], nemax[iterations[i]], colors[i], marker = 'h')
    for i in range(n):
        path = paths[i]
        axis, e2max, b2max, nemax, gammaav, wp, ndtwp, ndxne, dxne = read_file(path + '/' + 'analyse_inst.dat')
        dt = axis[1] - axis[0]
#        e1, = ax1.plot(axis, e2max, colors[i], label = labels[i])
        e1_, = ax1.plot([iterations[i]*dt], e2max[iterations[i]], colors[i], marker = 'h') 
#        e2, = ax2.plot(axis, b2max, colors[i], label = labels[i])
        e2_, = ax2.plot([iterations[i]*dt], b2max[iterations[i]], colors[i], marker = 'h') 
#        e3, = ax3.plot(axis, nemax, colors[i], label = labels[i])
        e3_, = ax3.plot([iterations[i]*dt], nemax[iterations[i]], colors[i], marker = 'h')

    picname = 'pics/pinch_fig3b.png'
    print picname
    ax2.legend(ncol = 2, loc='upper right', fontsize = 12, frameon = False)
#    plt.tight_layout()
    fig1.savefig(picname, dpi = 256)

if __name__ == '__main__':
    main()
