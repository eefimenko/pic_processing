#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def max2d(array):
    return max([max(x) for x in array])

def read_conc(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    return tmp[0]

def read_concentration(file, dv):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    e = tmp[0]
    for j in range(len(e)):
            e[j] = e[j]/((j+0.5)*dv)
    return e

def read_field(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def find_max_photon_energy_1percent(array, step):
    summ = 0
    for j, e in enumerate(array):
        if e != 1e-9:
            summ += j*e
    part_sum = 0
    N = len(array)
    for i in range(N):
        if array[N-i-1] != 1e-9:
            part_sum += array[N-i-1]*(N - i - 1)
            
            if part_sum > 0.01 * summ:
                break
    
    return (N-i-1)*step


def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def create_pulse(n,step,amp, tp, delay):
    dt = step
    ans = []
#    tp = 30
    tau = tp/1.1437
#    delay = 23.1099984185576458468467817
    for i in range(n):
        t = i*dt-delay
        if t > 0 and t < math.pi * tau:
            tmp = math.sin(t/tau)
            f = amp*tmp*tmp
        else:
            f = 0.
        ans.append(f*f)
    return ans

def read_field2d(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index])
        field.append(row)
    return field

def normList(L, normalizeTo=1):
    '''normalize values of a list to make its max = normalizeTo'''

    vMax = max(L)
    return [ x/(vMax*1.0)*normalizeTo for x in L]

def read_energy(file):
    f = open(file, 'r')
    tmp = [[float(x)+1e-9 for x in line.split()] for line in f]
    f.close()
    return tmp[0]

def main():

    config = utils.get_config("ParsedInput.txt")
#    duration = float(config['Duration']) #s
    delay = float(config['delay']) #s
    BOIterationPass = float(config['BOIterationPass'])
    c = 2.99792e+10
    pi = 3.14159
    
    coeff = 10./(3.3e11*3.3e11)*0.97399

#    tp = duration*1e15
#    tdelay = delay*1e15
    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    ny = int(config['MatrixSize_Z'])
    row = -1

    stepx = 5.656854249492380319955242562457442546/nx
    dv = 2. * math.pi * stepx * stepx * 0.8e-4 * 1e-8

    step = float(config['TimeStep'])*1e15*BOIterationPass
    dt = float(config['TimeStep'])
    omega = float(config['Omega'])
    wl = float(config['Wavelength'])
#    Emax = float(config['QuantumParticles.Emax'])
#    Emin = float(config['QuantumParticles.Emin'])
#    N_E = int(config['QuantumParticles.OutputN_E'])
#    outStep = int(config['QuantumParticles.OutputIterStep'])
    peakpower = float(config['PeakPower'])*1e-7*1e-15
    r = float(config['R'])
    Ne = float(config['Ne'])
    v = 4./3.*math.pi*r*r*r
    nemax = v*Ne
    print peakpower, r, nemax
    ev = float(config['eV'])
    expath = 'data/E2x'
    bxpath = 'data/B2x'
    eypath = 'data/E2y'
    bypath = 'data/B2y'
    ezpath = 'data/E2z'
    bzpath = 'data/B2z'
    denspath = 'data/Electron2Dz'
    elxpath = 'data/Electron2Dx'
    path='statdata'
    elpath='/ph/'
    elmaxpath='/el_max/'
    posmaxpath='/pos_max/'
    phmaxpath='/ph_max/'
    nepath = 'data/NeTrap'
    nppath = 'data/NposTrap'
    picspath = 'pics'
    necpath = 'data/ne'
    npcpath = 'data/npos'
    step1 = 5.656854249492380319955242562457442546/nx
    v = 2. * math.pi * step1 * step1 * 0.8e-4 * 1e-8
    axis1 = create_axis(nx, step/0.8)
    maxf = 0.
    maxn = 0.
    nmax = 160
    T = 2 * math.pi/omega
    time = dt/T
 #   en_step = (Emax-Emin)/N_E/ev*1e-9
 #   emax = Emax/ev*1e-9
    Ne = []
    Ne_max = []
    Npos = []
    power = []
    nmax = utils.num_files(nepath)
    eprev = 0
    powerfile = open('power.dat', 'w')
    energyfile = open('energy.dat', 'w')
    energymfile = open('energym.dat', 'w')
    nefile = open('ne.dat', 'w')
    dndtfile = open('dndt.dat', 'w')
    efile = open('e.dat', 'w')
    hfile = open('h.dat', 'w')
    nmin=0 
#    nmax = 556
    print nmax
  
    power_t = []
    energy_t = []
    energym_t = []
    ne_t = []
    dndt_t = []
    conc_t = []
    e_t = []
    nect = []
    e0 = 0
    n0 = 0
    nf = 0
    i0 = 0
    i_f = nmax
#    wl = 0.8e-4
    dv1 = pi*wl*wl*0.25*wl*0.5
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    dmy = int(0.35*wl/dy)
    dv2 = 2*dx*dy*dz
    delta = 1
    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(nepath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(nepath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax) + ' files'

    for i in range(nmin,nmax,delta):
        print "%06d.txt" % (i,)
        ezname = ezpath + '/' + "%06d.txt" % (i,)
        bzname = bzpath + '/' + "%06d.txt" % (i,)
        bz = read_field2d(ezname,nx,ny)
        ez = read_field2d(bzname,nx,ny)
        ezv = ez[nx/2][ny/2]
        e_t.append(ezv)
        efile.write(str(ezv) + '\n')
        bzv = bz[nx/2][ny/2-dmy]*1.53
        tmp = (ezv*ezv + bzv*bzv)*coeff
        hfile.write(str(bzv) + '\n')
         
#        nename = nepath + '/' + "%06d.txt" % (i,)
#        e = read_field(nename)/dv1
        elxname = elxpath + '/' + "%06d.txt" % (i,)
        elx_array = read_field2d(elxname,nx,ny)
        e = max2d(elx_array)/dv2
        if i == 0:
            e0 = e
            
#        if tmp < 1.1*peakpower:
        power.append(tmp)
        Ne.append(e)
#        else:
#           power.append(0)
#            Ne.append(0)
            
        enext = e
        print tmp
        if enext != eprev:
            dndt = (enext - e0)/((enext - eprev)/step)
        else:
            dndt = 0.
        dndt_t.append(dndt)
        dndtfile.write(str(dndt) + '\n')
        powerfile.write(str(tmp)+'\n') 
        if tmp < 1.1*peakpower:
            
            
            power_t.append(tmp)
            ne_t.append(enext)
            dndt_t.append(dndt)
        else:
            power_t.append(0)
            ne_t.append(0)
            dndt_t.append(0)
                    
        print tmp, e, ezv

        eprev = enext
        power_prev = tmp
#        nename = necpath + '/' + "%06d.txt" % (i,)
#        npname = npcpath + '/' + "%06d.txt" % (i,)
#        e = read_conc(nename)
#        p = read_conc(npname)
#        for j in range(len(e)):
#            e[j] = e[j]/((j+0.5)*v)
#            p[j] = p[j]/((j+0.5)*v)
#        nect.append(max(e))
        nect.append(0.)

#        fig, ax1 = plt.subplots()
#        pe, = ax1.plot(axis1, e, 'g')
#        pp, = ax1.plot(axis1, p, 'b')
#        ax1.set_xlim(0., 1.)
#        ax1.legend([pe, pp], ["El", "Pos"], loc=1)
#        picname = picspath + '/' + "n%06d.png" % (i,)
#        plt.savefig(picname)
#        plt.close()
        
    
    powerfile.close()
    energyfile.close()
    energymfile.close()
    nefile.close()
    dndtfile.close()
   
    efile.close()
    hfile.close()
    normpower = normList(power, coeff)
#    pulse = create_pulse(nmax, step, 1, tp, tdelay)
    fig, ax1 = plt.subplots()
    axis = create_axis(nmax-nmin, step)
    pe, = ax1.plot(axis, Ne, 'g')
    pec, = ax1.plot(axis, nect, 'r')
#    pb, = ax1.plot(axis, Npos, 'b')
#    pm, = ax1.plot(axis, Ne_max, 'r')
    ax1.set_yscale('log')
    ax2 = ax1.twinx()
#    #pu, = ax2.plot(axis, pulse, 'r-')
    pp, = ax2.plot(axis, power, 'k')
    ax1.legend([pe, pec, pp], ["ne_av", "ne_max", 'power'], loc=2)
    plt.savefig('pics/dd.png')
    plt.close()

    print power_t
    fig, ax1 =  plt.subplots()
    ppp, = ax1.plot(power_t, ne_t)
    plt.savefig('pics/nn.png')
    plt.close()

    fig, ax1 =  plt.subplots()
    ppp, = ax1.plot(dndt_t)
    ax2 = ax1.twinx()
    pph, = ax2.plot(power_t,'r')
    plt.savefig('pics/conc_axis_t.png')
    plt.close()
    
if __name__ == '__main__':
    main()
