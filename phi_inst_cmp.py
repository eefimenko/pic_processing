#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def read_field(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)/norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1e-9)
        field.append(row)
    return field

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])

def extract_phi(f, dx, dy):
    r = 0.9 * 1e-4
    nx = f.shape[0]
    ny = f.shape[1]
#    print nx,ny,dx,dy
    n = 360
    profile = []
    for k in xrange(n):
        x = r*math.sin(2.*math.pi/n*k)
        y = r*math.cos(2.*math.pi/n*k)
       
        i = (int)(x/dx)
        j = (int)(y/dy)
        
        if i > 0:
            i1 = i + nx/2
            i2 = i + nx/2 + 1
        else:
            i1 = i + nx/2
            i2 = i + nx/2 - 1
        if j > 0:
            j1 = j + ny/2
            j2 = j + ny/2 + 1
        else:
            j1 = j + ny/2
            j2 = j + ny/2 - 1
        ax = abs(y - j*dy)/dy
        bx = 1 - ax
        ay = abs(x - i*dx)/dx
        by = 1 - ay

        ans = bx*by*f[i1][j1] + by*ax*f[i1][j2] + ax*ay*f[i2][j2] + ay*bx*f[i2][j1]
#        ans = 0.25*(f[i1][j1] + f[i1][j2] + f[i2][j2] + f[i2][j1]) 
        profile.append(ans)
    return profile
    
def main():
    picspath = 'pics15'

    n = len(sys.argv)
   
  
    nmin_ = []
    nmax_ = []
    configs = []
    for i in range(n-1):
        path = sys.argv[i+1] + '/'
        config = utils.get_config(path + "/ParsedInput.txt")
        configs.append(config)
        nmin,nmax = utils.find_min_max_from_directory(path + utils.ezpath)
        print 'min(%d) ='%(i), nmin, ' max(%d) ='%(i), nmax
        nmin_.append(nmin)
        nmax_.append(nmax)

    nmin = max(nmin_)
    nmax = min(nmax_)
    print 'min =', nmin, ' max =', nmax
    delta = 1
    nmin = 487
    nmax = 900
    for it in range(nmin, nmax, delta):
        fig = plt.figure(figsize = (20,10))
        ezx = fig.add_subplot(2,1,1)
        nezx = fig.add_subplot(2,1,2)
        for i in range(n-1):
            path = sys.argv[i+1] + '/'
            config = configs[i]
            dx = float(config['Step_X'])
            dy = float(config['Step_Y'])
            dz = float(config['Step_Z'])
            nx = int(config['MatrixSize_X'])
            ny = int(config['MatrixSize_Y'])
            BOIterationPass = int(config['BOIterationPass'])
            TimeStep = float(config['TimeStep'])
            dt = BOIterationPass*TimeStep*1e15
        
            dv = 2*dx*dy*dz
        
            fieldez_x = utils.bo_file_load(path + utils.ezpath,it,nx,ny)
#            fieldez_x = np.full(fieldez_x.shape, 1.)
#            for i in range(fieldez_x.shape[0]):
#                for j in range(fieldez_x.shape[1]):
#                    if i != nx/2 and j != ny/2:
#                        fieldez_x[i][j] = 1./float((i-nx/2)*(i-nx/2) + (j-ny/2)*(j-ny/2))
#                    else:
#                        fieldez_x[i][j] = 0
            ez_phi = extract_phi(fieldez_x, dx, dy)
            ne_x = utils.bo_file_load(path + utils.nezpath,it,nx,ny)/dv
            ne_phi = extract_phi(ne_x, dx, dy)

            ezx.plot(ez_phi, label = path)
            ezx.set_ylim([0,1.1*max(ez_phi)])
            nezx.plot(ne_phi, label = path)
        plt.legend()
        print picspath + '/' + "phi_%d.png"%it
        plt.savefig(picspath + '/' + "phi_%d.png"%it)
        plt.close()
if __name__ == '__main__':
    main()
