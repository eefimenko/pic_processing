#!/usr/bin/python
import matplotlib as mp
mp.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import math
import utils
import sys
import pandas as pd

def moving_average(x, w=5):
    return np.convolve(x, np.ones(w), 'valid') / w

def read_file(filename):
    f = open(filename)
    tmp = f.readline()
    labels = tmp.split()
    print labels
    N = len(labels)
    array = []
    for line in f:
        tmp = line.split()
        values = []
        for k in range(N):
            #print tmp, k
            values.append(float(tmp[k+1]))
        array.append(values)
    return labels, np.array(array).transpose()
        
def main():
    picspath = '/home/evgeny/Dropbox/Thinout/pics'
    labels, ne_sum = read_file('sum_density.txt')
    ne_sum /= 384*384
    spx = len(labels)
#    fig = plt.figure(num=None, figsize=(8., 6.))
#    ax = fig.add_subplot(1,1,1)
#    for k in range(spx):
#        ax.plot(ne_sum[k,:], label = labels[k])
#    ax.legend(loc = 'upper left', frameon = False)
#    ax.set_yscale('log')
#    picname = picspath + '/' + "compare_sum_density.png"
#    plt.tight_layout()
#    plt.savefig(picname, dpi=128, bbox_inches='tight')
#    plt.close()

    labels, ne_max = read_file('max_density.txt')
    spx = len(labels)
#    fig = plt.figure(num=None, figsize=(8., 6.))
#    ax = fig.add_subplot(1,1,1)
#    for k in range(spx):
#        ax.plot(ne_max[k,:], label = labels[k])
#    ax.legend(loc = 'upper left', frameon = False)
#    ax.set_yscale('log')
#    picname = picspath + '/' + "compare_max_density.png"
#    plt.tight_layout()
#    plt.savefig(picname, dpi=128, bbox_inches='tight')
#    plt.close()

    labels, ne_std = read_file('std_density.txt')
    spx = len(labels)
    labels, ne_harm = read_file('harm_density.txt')
    spx = len(labels)
#    fig = plt.figure(num=None, figsize=(8., 6.))
#    ax = fig.add_subplot(1,1,1)
#    for k in range(spx):
#        ax.plot(ne_max[k,:], label = labels[k])
#    ax.legend(loc = 'upper left', frameon = False)
#    ax.set_yscale('log')
#    picname = picspath + '/' + "compare_max_density.png"
#    plt.tight_layout()
#    plt.savefig(picname, dpi=128, bbox_inches='tight')
#    plt.close()

#    fig = plt.figure(num=None, figsize=(8., 6.))
#    ax = fig.add_subplot(1,1,1)
#    for k in range(spx-1):
#        ax.plot((ne_max[k,:]-ne_sum[k,:])/ne_sum[k,:], label = labels[k])
#    ax.legend(loc = 'upper left', frameon = False)
#    ax.set_yscale('log')
#    picname = picspath + '/' + "compare_diff_density.png"
#    plt.tight_layout()
#    plt.savefig(picname, dpi=128, bbox_inches='tight')
#    plt.close()

    diff = ne_max - ne_sum
    window = 3
    config = utils.get_config('no_thinout/ParsedInput.txt')
    step = float(config['TimeStep'])*float(config['BOIterationPass'])
    Gins = float(config['Gins'])
    dt = window * step
    nx, ny = diff.shape
    gamma = np.zeros((nx, ny-window))
    for k in range(nx):
        for j in range(ny-window):
            n0 = diff[k,j]
            n1 = diff[k,j+window]
            gamma[k,j] = np.log(n1/n0)/dt

    fig = plt.figure(num=None, figsize=(8., 6.))
    ax = fig.add_subplot(1,1,1)
    ax.set_ylim([-1,6])
    axis = utils.create_axis(len(moving_average(gamma[k,1:])), step*1e15)
    for k in range(1,spx):
        ax.plot(axis, moving_average(gamma[k,1:], 5)*1e-15, label = labels[k])
    ax.legend(loc = 'upper left', frameon = False)
    ax.set_ylabel('$\Gamma$, fs$^{-1}$')
    ax.set_xlabel('t, fs')
    ax.axhline(y = Gins*1e-15, label = '')
#    ax.plot(Gins*np.sqrt(ne_sum[0,1:]/ne_sum[0,0]), 'b', dashes = [2,2])
#    ax.set_yscale('log')
    picname = picspath + '/' + "gamma_max-avg.png"
    plt.tight_layout()
    plt.savefig(picname, dpi=128, bbox_inches='tight')
    plt.close()

    gamma = np.zeros((nx, ny-window))
    for k in range(nx):
        for j in range(ny-window):
            n0 = ne_std[k,j]
            n1 = ne_std[k,j+window]
            gamma[k,j] = np.log(n1/n0)/dt

    fig = plt.figure(num=None, figsize=(8., 6.))
    ax = fig.add_subplot(1,1,1)
    ax.set_ylim([-1,6])
    axis = utils.create_axis(len(moving_average(gamma[k,1:])), step*1e15)
    for k in range(1,spx):
        ax.plot(axis, moving_average(gamma[k,1:], 5)*1e-15, label = labels[k])
    ax.legend(loc = 'upper left', frameon = False)
    ax.set_ylabel('$\Gamma$, fs$^{-1}$')
    ax.set_xlabel('t, fs')
    ax.axhline(y = Gins*1e-15, label = '')
#    ax.plot(Gins*np.sqrt(ne_sum[0,1:]/ne_sum[0,0]), 'b', dashes = [2,2])
#    ax.set_yscale('log')
    picname = picspath + '/' + "gamma_std.png"
    plt.tight_layout()
    plt.savefig(picname, dpi=128, bbox_inches='tight')
    plt.close()

    gamma = np.zeros((nx, ny-window))
    for k in range(nx):
        for j in range(ny-window):
            n0 = ne_harm[k,j]
            n1 = ne_harm[k,j+window]
            gamma[k,j] = np.log(n1/n0)/dt

    fig = plt.figure(num=None, figsize=(8., 6.))
    ax = fig.add_subplot(1,1,1)
    ax.set_ylim([-1,6])
    axis = utils.create_axis(len(moving_average(gamma[k,1:])), step*1e15)
    for k in range(1,spx):
        ax.plot(axis, moving_average(gamma[k,1:], 5)*1e-15, label = labels[k])
    ax.legend(loc = 'upper left', frameon = False)
    ax.set_ylabel('$\Gamma$, fs$^{-1}$')
    ax.set_xlabel('t, fs')
    ax.axhline(y = Gins*1e-15, label = '')
#    ax.plot(Gins*np.sqrt(ne_sum[0,1:]/ne_sum[0,0]), 'b', dashes = [2,2])
#    ax.set_yscale('log')
    picname = picspath + '/' + "gamma_harm.png"
    plt.tight_layout()
    plt.savefig(picname, dpi=128, bbox_inches='tight')
    plt.close()
if __name__ == '__main__':
    main()
