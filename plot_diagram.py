#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_field(path,name):
    file = path + '/' + name
    print 'Open ' + file
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = [row[0] for row in tmp]
    return tmp1
    
def main():
    
    pathlist = ['10PW_temp', '15PW_temp', '20PW_temp', '40PW_temp','50PW_temp', '80PW_temp', '100PW_temp', '100PW_temp_1mkm', '300PW_temp']
    color = ['r', 'g', 'b']
    fig, ax1 =  plt.subplots()
    
    for i,path in enumerate(pathlist):
        power_tt = []
        ne_tt = []
        power_t = read_field(path, 'power.dat')
        ne_t = read_field(path, 'ne.dat')
        
        for j, v in enumerate(power_t):
#            if power_t[j+1] < power_t[j] and power_t[j] > 1000:
            if power_t[j] > 320:
                break
            else:
                power_tt.append(power_t[j])
                ne_tt.append(ne_t[j])
                print j
                
        print power_tt
        ppp, = ax1.plot(power_tt, ne_tt, color[i%len(color)])
        
#        ppp, = ax1.plot(power_t, ne_t, color[i+1])
    ax1.set_yscale('log')
#    ax1.set_xscale('log')
    plt.savefig('pics/nn1.png')
    plt.close()
    
if __name__ == '__main__':
    main()
