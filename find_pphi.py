#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.cm as cm
import matplotlib.colors as colors
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def read_traj(file):
    f = open(file, 'r')
    x_ = []
    y_ = []
    z_ = []
    px_ = []
    py_ = []
    pz_ = []
    pr_ = []
    pt_ = []
    pp_ = []
    p_ = []
    r0_ = []
    ev = 1.6e-12
    c = 2.99792e+10
    me = 9.10938e-28
    p0 = me*c
    for line in f:
        tmp = line.split()
        x = float(tmp[1])*1e4
        y = float(tmp[2])*1e4
        z = float(tmp[3])*1e4
        px = float(tmp[4])
        py = float(tmp[5])
        pz = float(tmp[6])
        p = math.sqrt(px*px + py*py + pz*pz)
        r = math.sqrt(x*x + y*y + z*z)
        r0 = math.sqrt(x*x + y*y)
        theta = math.acos(z/r)
        phi = math.atan(y/x)
        st = math.sin(theta)
        ct = math.cos(theta)
        sp = math.sin(phi)
        cp = math.cos(phi)
        pp = -px*sp + py*cp
        pr = px*st*cp + py*st*sp + pz*ct
        pt = px*ct*cp + py*ct*sp - pz*st
        p1 = math.sqrt(pp*pp + pr*pr + pt*pt)
#        if p1 != p:
#            print p1/p0, p/p0, p/p1
        x_.append(x)
        y_.append(y)
        z_.append(z)
        px_.append(px/p0)
        py_.append(py/p0)
        pz_.append(pz/p0) 
        pr_.append(pr/p)
        pt_.append(pt/p)
        pp_.append(pp/p)
        p_.append(p/p0)
        r0_.append(r0)
        if abs(z) > 1.7:
            break
    return x_, y_, z_, px_, py_, pz_, pr_, pt_, pp_, p_, r0_

def normalize(a, n = 1.):
    if n != 0:
        for i in range(len(a)):
            a[i] /= n
    return a

def length(ax,ay,az):
    return math.sqrt(ax*ax + ay*ay + az*az)

def check(x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4):
    ax1 = x1 - x2
    ay1 = y1 - y2
    az1 = z1 - z2
    ax2 = x1 - x3
    ay2 = y1 - y3
    az2 = z1 - z3
    ax3 = x1 - x4
    ay3 = y1 - y4
    az3 = z1 - z4
    ans = ax1 * (ay2 * az3 - ay3 * az2) - ay1 * (ax2 * az3 - ax3 * az2) + az1 * (ax2 * ay3 - ax3 * ay2)
    print ans/(length(ax1,ay1,az1) * length(ax2,ay2,az2) * length(ax3,ay3,az3))
    return ans
  
def check_trajectory(x,y,z):
    n = len(x)
    for i in xrange(n-3):
        x1 = x[i]
        x2 = x[i+1]
        x3 = x[i+2]
        x4 = x[i+3]
        y1 = y[i]
        y2 = y[i+1]
        y3 = y[i+2]
        y4 = y[i+3]
        z1 = z[i]
        z2 = z[i+1]
        z3 = z[i+2]
        z4 = z[i+3]
        check(x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4)

def main():
    expath = 'data/E2x'
    partpath = 'ParticleTracking/Electrons'
#    partpath = 'ParticleTracking' 
    picspath = 'pics'
    num = 100

    config = utils.get_config("ParsedInput.txt")

    BOIterationPass = float(config['BOIterationPass'])
    dt = float(config['TimeStep'])*1e15*BOIterationPass #fs
    maxpower = float(config['PeakPower'])*1e-7*1e-15 #PW
    peakpower = float(config['PeakPower'])*1e-7*1e-15
    const_a_p = 7.81441e-9
    a0 = const_a_p * math.sqrt(peakpower*1e22)
    print a0
    print "Power = " + str(maxpower) + 'PW'
    print "dt = " + str(dt) + 'fs'
#    print "duration = " + str(duration)
#    print "delay = " + str(delay)
    
    Xmax = float(config['X_Max'])*1e4 #mkm
    Xmin = float(config['X_Min'])*1e4 #mkm
    Ymax = float(config['Y_Max'])*1e4 #mkm
    Ymin = float(config['Y_Min'])*1e4 #mkm
    Zmax = float(config['Z_Max'])*1e4 #mkm
    Zmin = float(config['Z_Min'])*1e4 #mkm

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = (Xmax-Xmin)/nx
    dy = (Ymax-Ymin)/ny
    dz = (Zmax-Zmin)/nz

    step = (Xmax-Xmin)/nx

    mult = 1/(2.*dx*dy*dz*1e-12)

    delta = 1
    jet = plt.get_cmap('jet') 
    cNorm  = colors.Normalize(vmin=0.5, vmax=a0*1e-3*0.5)
    scalarMap = cm.ScalarMappable(norm=cNorm, cmap=jet)

    for i in range(1,num):
        fig = plt.figure(num=None, figsize=(20, 20), dpi=256)
        ax = fig.add_subplot(3,3,1)
        ax.set_title('x')
        ay = fig.add_subplot(3,3,2)
        ay.set_title('y')
        az = fig.add_subplot(3,3,3)
        az.set_title('z')
        apx = fig.add_subplot(3,3,4)
        apx.set_title('Px')
        apy = fig.add_subplot(3,3,5)
        apy.set_title('Py')
        apz = fig.add_subplot(3,3,6)
        apz.set_title('Pz')
        apr = fig.add_subplot(3,3,7)
        apr.set_title('Pr')
        apt = fig.add_subplot(3,3,8)
        apt.set_title('Pt')
        app = fig.add_subplot(3,3,9)
        app.set_title('Pp')
        ap = app.twinx()
    
        x,y,z,px,py,pz,pr,pt,pp,p,r0 = read_traj('./' + partpath + '/%d.txt' % (i))
        if len(x) == 0:
            continue
        ax.plot(x)
        ay.plot(y)
        az.plot(z)
        apx.plot(px)
        apy.plot(py)
        apz.plot(pz)
        apr.plot(pr)
        apt.plot(pt)
        app.plot(pp)
        ap.plot(r0, color='r')
        picname = picspath + '/' + "trajectory_%d.png" % i
        print picname
        plt.savefig(picname)
        plt.close()
#        check_trajectory(x,y,z)
    
if __name__ == '__main__':
    main()
