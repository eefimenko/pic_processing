#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils
import os
import numpy as np

def main():
    picspath = 'pics'
    path = './'
    n = len(sys.argv) - 1
    p = []
    jp = []
    jn = []
    jm = []
    jf = []
    jfm = []
    jmm = []
    jc = []
    bound = 100
    bmin = 500
    bmax = 620
    average = 1
    for i in range(n):
        path = sys.argv[i+1] + '/'
        config = utils.get_config(path + "/ParsedInput.txt")
       
        power = float(config['PeakPowerPW'])
        dt = float(config['TimeStep'])*1e15
        filename = 'jz_anls_new'
        filename_center = 'jz_center'
        #nmin,nmax = utils.find_min_max_from_directory(path + '/' + utils.jz_zpath)
        nmax = utils.file_len(path + '/' + filename + '.txt')
        nmin=0
        axis = utils.create_axis(nmax-nmin, dt)
        jplus = np.zeros(nmax)
        jminus = np.zeros(nmax)
        jfull = np.zeros(nmax)
        jmax = np.zeros(nmax)
        jfull1 = np.zeros(nmax)
        jcenter = np.zeros(nmax)
        
        print nmin, nmax
        
        dx = float(config['Step_X'])
        dy = float(config['Step_Y'])
        c2 = 3.33e-10*dx*dy*1e-6
        nmin = utils.read_tseries(path,filename, jplus, jminus, jfull, jmax)
        nmin = utils.read_tseries(path,filename_center, jcenter)
        jp_e = utils.env(jplus, axis)
        jm_e = utils.env(jminus, axis)
        for k in range(nmax):
            jfull1[k] = abs(jfull[k])
        jf_e = utils.env(jfull1, axis)
        p.append(power)
        jp.append(np.average(jp_e[-bound:])*c2)
        jn.append(np.average(jm_e[-bound:])*c2)
        jf.append(np.average(jf_e[-bound:])*c2)
#        jp.append(np.average(jp_e[bmin:bmax])*c2)
#        jn.append(np.average(jm_e[bmin:bmax])*c2)
#        jf.append(np.average(jf_e[bmin:bmax])*c2)
        jc.append(np.amax(jcenter))
        jf_es = sorted(jf_e)
        if not '7.1' in path:
            jfm.append(sum(jf_es[-average:])/average*c2)
        else:
            jfm.append(jf[-1])
        jm.append(np.average(jmax[-bound:]))
        jmm.append(max(jmax))

    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.plot(p, jp, 'r-o', label='positive')
    ax.plot(p, jn, 'b-o', label='negative')
    ax.plot(p, jf, 'g-o', label='full')
    ax.plot(p, jfm, 'k-o', label='full max')
#    ax2 = ax.twinx()
    ax.plot(p, jc, 'g-x', label='center')
    ax.set_ylim([0, 1.1*max(jfm)])
    ax.set_ylabel('Full current through z=0, MA')
    ax.set_xlabel('Power, PW')
    plt.legend(loc = 'upper left')
#    ax = fig.add_subplot(1,2,2)
#    ax.plot(p, jm, 'r', label = 'max stationary')
#    ax.plot(p, jmm, 'g', label = 'max')
#    ax.set_ylim([0, 1.1*max(jmm)])
#    ax.set_ylabel('Maximum current density z=0')
#    ax.set_xlabel('Power, PW')
#    plt.legend(loc = 'upper left')
    plt.show()
    plt.close()

    f = open('jpos_jneg.txt', 'w')
    for i in range(len(p)):
        f.write('%lf %le %le\n' % (p[i], jp[i], jn[i]))
    f.close()

    f = open('jfull.txt', 'w')
    for i in range(len(p)):
        f.write('%lf %le %le %le %le\n' % (p[i], jp[i], jn[i], jf[i], jfm[i]))
    f.close()

    f = open('jcenter.txt', 'w')
    for i in range(len(p)):
        f.write('%lf %le\n' % (p[i], jc[i]))
    f.close()
    
    
if __name__ == '__main__':
    main()
