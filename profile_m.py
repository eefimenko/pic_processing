#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1.)
        field.append(row)
    return field

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])

def main():
    expath = 'data/Ez_x'
    eypath = 'data/Ez_y'
    ezpath = 'data/Ez_z'
    bxpath = 'data/Bx_x'
    bypath = 'data/Bx_y'
    bzpath = 'data/Bx_z'
    nexpath = 'data/Electron2Dx'
    neypath = 'data/Electron2Dy'
    nezpath = 'data/Electron2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nixpath = 'data/Proton2Dx'
    niypath = 'data/Proton2Dy'
    nizpath = 'data/Proton2Dz'
    npxpath = 'data/Positron2Dx'
    npypath = 'data/Positron2Dy'
    npzpath = 'data/Positron2Dz'
    nphxpath = 'data/Photon2Dx'
    nphypath = 'data/Photon2Dy'
    nphzpath = 'data/Photon2Dz'
    picspath = 'pics'
    config = utils.get_config("ParsedInput.txt")
    wl = float(config['Wavelength'])
    Xmax = float(config['X_Max'])/wl #mkm to wavelength
    Xmin = float(config['X_Min'])/wl #mkm to wavelength
    Ymax = float(config['Y_Max'])/wl #mkm to wavelength
    Ymin = float(config['Y_Min'])/wl #mkm to wavelength

    nx = int(config['MatrixSize_X'])
    ny = int(config['MatrixSize_Y'])
    nz = int(config['MatrixSize_Z'])

    print 'Nx = ' + str(nx) 
    print 'Ny = ' + str(ny)
    print 'Nz = ' + str(nz)

    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    dz = float(config['Step_Z'])
    delta = 1

    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(expath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(expath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'

    dv = dx*dy*dz
    axis1 = create_axis(nx, (Xmax-Xmin)/nx, Xmin)

    for i in range(nmin, nmax, delta):
        fig = plt.figure(num=None, figsize=(20, 10), dpi=256)
        picname = picspath + '/' + "cc%06d.png" % (i,)
        print picname
        ax = fig.add_subplot(4,3,4)
        nax = fig.add_subplot(4,3,7)
        bax = fig.add_subplot(4,3,1)
        name = bxpath + '/' + "%06d.txt" % (i,)
        fieldb = read_field(name,nx,ny)
        surf = bax.imshow(fieldb, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Blues')
        name = expath + '/' + "%06d.txt" % (i,)
        fielde = read_field(name,nx,ny)
        surf = ax.imshow(fielde, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Reds')
        name = nexpath + '/' + "%06d.txt" % (i,)
        fieldn = read_field(name,nx,ny)
        surf = nax.imshow(fieldn, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Greens', norm=clr.LogNorm())
        name = npxpath + '/' + "%06d.txt" % (i,)
        fieldp = read_field(name,nx,ny)
#        surf = nax.imshow(fieldn, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Greens', norm=clr.LogNorm())
        ax.set_xlim([-1, 1])
        ax.set_ylim([-0.5, 0.5])
        ax.set_xlabel('z/lambda')
        ax.set_ylabel('y/lambda')
        bax.set_xlim([-1, 1])
        bax.set_ylim([-0.5, 0.5])
        bax.set_xlabel('z/lambda')
        bax.set_ylabel('y/lambda')
        nax.set_xlim([-1, 1])
        nax.set_ylim([-0.5, 0.5])
        nax.set_xlabel('z/lambda')
        nax.set_ylabel('y/lambda')

        profn = fieldn[:][ny/2]
        profp = fieldp[:][ny/2]

        for k in range(len(fieldn)):
            profn[k] /= dv
            profp[k] /= dv

        profe = fielde[:][ny/2]
        profb = fieldb[:][ny/2]
        ax1 = fig.add_subplot(4,3,10)
        ax1.plot(axis1,profn,'g', label = 'el')
        ax1.plot(axis1,profp,'k', label = 'pos')
        ax2 = ax1.twinx()
        ax2.plot(axis1,profe, 'r', label = 'E')
        ax2.plot(axis1,profb, 'b', label = 'B')
        ax1.set_xlim([-1, 1])
        ax1.set_xlabel('z/lambda')
        ax1.legend(loc='upper left', shadow=True)
        ax2.legend(loc='upper right', shadow=True)
        ax1 = fig.add_subplot(4,3,12)
        ax1.plot(axis1,profn,'g', label = 'el')
        ax1.plot(axis1,profp,'k', label = 'pos')
        ax2 = ax1.twinx()
        ax2.plot(axis1,profe, 'r', label = 'E')
        ax2.plot(axis1,profb, 'b', label = 'B')
#        ax1.set_xlim([-0.1, 0.1])
        ax1.set_xlabel('y/lambda')
        ax1.legend(loc='upper left', shadow=True)
        ax2.legend(loc='upper right', shadow=True)
        
        plt.figtext(0.75,0.93,str('Ne = %.1e cm^-3 E = %.1e B = %.1e' % (max2d(fieldn),max2d(fielde),max2d(fieldb))) , fontsize = 12)
        
        bay = fig.add_subplot(4,3,2)
        ay = fig.add_subplot(4,3,5)
        nay = fig.add_subplot(4,3,8)
        name = bypath + '/' + "%06d.txt" % (i,)
        fieldb = read_field(name,nx,ny)
        surf = bay.imshow(fieldb, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Blues')
        name = eypath + '/' + "%06d.txt" % (i,)
        fielde = read_field(name,nx,ny)
        surf = ay.imshow(fielde, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Reds')
        name = neypath + '/' + "%06d.txt" % (i,)
        fieldn = read_field(name,nx,ny)
        surf = nay.imshow(fieldn, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Greens', norm=clr.LogNorm())
        ay.set_xlim([-1, 1])
        ay.set_ylim([-0.5, 0.5])
        ay.set_xlabel('z/lambda')
        ay.set_ylabel('x/lambda')
        bay.set_xlim([-1, 1])
        bay.set_ylim([-0.5, 0.5])
        bay.set_xlabel('z/lambda')
        bay.set_ylabel('x/lambda')
        nay.set_xlim([-1, 1])
        nay.set_ylim([-0.5, 0.5])
        nay.set_xlabel('z/lambda')
        nay.set_ylabel('x/lambda')

        baz = fig.add_subplot(4,3,3)
        az = fig.add_subplot(4,3,6)
        naz = fig.add_subplot(4,3,9)
        name = bzpath + '/' + "%06d.txt" % (i,)
        fieldb = read_field(name,nx,ny)
        surf = baz.imshow(fieldb, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Blues')
        name = ezpath + '/' + "%06d.txt" % (i,)
        fielde = read_field(name,nx,ny)
        surf = az.imshow(fielde, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Reds')
        name = nezpath + '/' + "%06d.txt" % (i,)
        fieldn = read_field(name,nx,ny)
        surf = naz.imshow(fieldn, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Greens', norm=clr.LogNorm())
        name = npzpath + '/' + "%06d.txt" % (i,)
        fieldp = read_field(name,nx,ny)
        az.set_xlim([-0.5, 0.5])
        az.set_ylim([-0.5, 0.5])
        az.set_xlabel('x/lambda')
        az.set_ylabel('y/lambda')
        baz.set_xlim([-0.5, 0.5])
        baz.set_ylim([-0.5, 0.5])
        baz.set_xlabel('x/lambda')
        baz.set_ylabel('y/lambda')
        naz.set_xlim([-0.5, 0.5])
        naz.set_ylim([-0.5, 0.5])
        naz.set_xlabel('x/lambda')
        naz.set_ylabel('y/lambda')
        profn = fieldn[:][ny/2]
        profp = fieldp[:][ny/2]
        for k in range(len(fieldn)):
            profn[k] /= dv
            profp[k] /= dv

        profe = fielde[:][ny/2]
        profb = fieldb[:][ny/2]
        ax1 = fig.add_subplot(4,3,11)
        ax1.plot(axis1,profn,'g', label = 'el')
        ax1.plot(axis1,profp,'k', label = 'pos')
        ax2 = ax1.twinx()
        ax2.plot(axis1,profe, 'r', label = 'E')
        ax2.plot(axis1,profb, 'b', label = 'B')
        ax1.set_xlim([-1, 1])
        ax1.set_xlabel('y/lambda')
        ax1.legend(loc='upper left', shadow=True)
        ax2.legend(loc='upper right', shadow=True)

        
        plt.savefig(picname)
        plt.close()
#        plt.show()
    
if __name__ == '__main__':
    main()
