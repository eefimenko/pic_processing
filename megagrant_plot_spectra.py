#!/usr/bin/python
import matplotlib as mp
mp.use('Agg')
import os
import math
import zipfile
import numpy as np
import re
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import sys
import utils
from mpl_toolkits.axes_grid1 import make_axes_locatable

def main():
    fontsize = 14
    mp.rcParams.update({'font.size': fontsize})
    picdir = "/home/evgeny/Dropbox/reportMegagrant"
    #labels=["Ideal", "Amp",  "Shift", "Delay"]
    labels=["Ideal", "Amp 1.5", "Amp 1.25",  "Shift", "Delay"]
    simulations=[
    #    "InputUnified_4_30_2020-12-29_23-49-05_Ideal4",
    #             "DifA/InputUnified_4_30_2020-12-23_15-12-56",
    #             "DifShift/InputUnified_4_30_2020-12-25_00-34-31",
    #             "DifDelay/InputUnified_4_30_2020-12-29_15-55-44"
    #"equal_6beams/6beams_21pw",
    #"amplitude_6beams/6beams_21pw",
    #"shift_6beams/6beams_21pw",
    #"delay_6beams/6beams_21pw",
        "equal/12beams_13pw",
        "amplitude/12beams_13pw",
        "amplitude_1.25/12beams_13pw",
        "shift/12beams_13pw",
        "delay/12beams_13pw",
    ]
    fig = plt.figure(figsize=(12, 4))
    ax1 = fig.add_subplot(1, 2, 1)
    ax2 = fig.add_subplot(1, 2, 2)
    for idx, path_ in enumerate(simulations):
        print(path_)
        cfg = utils.get_config(os.path.join(path_, "ParsedInput.txt"))
        number = int(cfg["FieldGenerator.Number"])
        axis = np.loadtxt(os.path.join(path_, "en_array.txt"))* 0.511 * 1e-3
        spectra_el = np.loadtxt(os.path.join(path_, "el_spectra.txt")) 
        spectra_ph = np.loadtxt(os.path.join(path_, "ph_spectra.txt"))
        ax1.plot(axis, spectra_el, label = labels[idx])
        ax2.plot(axis, spectra_ph, label = labels[idx])
    ax1.legend(loc="upper right", frameon = False)
    ax2.legend(loc="upper right", frameon = False)
    ax1.set_yscale("log")
    ax2.set_yscale("log")
    ax1.set_xlim([0, 3.500])
    ax2.set_xlim([0, 3.500])
    ax1.set_ylim([1e-4, 10])
    ax2.set_ylim([1e-4, 10])
    ax1.set_xlabel('W, GeV')
    ax2.set_xlabel('W, GeV')
    ax1.set_ylabel('$\partial N/\partial W$, a.u.')
    ax2.set_ylabel('$\partial N/\partial W$, a.u.')
    ax1.text(-0.7, 10, '(a)')
    ax2.text(-0.7, 10, '(b)')
    plt.tight_layout()
    figname = os.path.join(picdir, "spectra_%d.png" % number)
    print(figname)
    plt.savefig(figname, dpi=128)
    plt.close()
    
if __name__ == '__main__':
    main()
