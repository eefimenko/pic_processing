#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils
import numpy as np
import matplotlib as mp

def main():
    power_p = []
    ezx_p = []
    bzx_p = []
    nex_p = []
    nemaxx_p = []
    ncrx_p = []
    jx_min_p = []
    jx_max_p = []
    powx_p = []
    pow1x_p = []
    maxx_p = []
    avx_p = []
    el_powx_p = []
    el_pow1x_p = []
    el_maxx_p = []
    el_avx_p = []
    picspath = 'pics/'
    
    f = open('stat_summary_el.txt', 'r')
    for line in f:
        tmp = line.split()
        power_p.append(float(tmp[0]))
        ezx_p.append(float(tmp[1])/1e11)
        bzx_p.append(float(tmp[2])/1e11)
        maxx_p.append(float(tmp[3]))
        avx_p.append(float(tmp[4]))
        ncrx_p.append(float(tmp[5]))
        nex_p.append(float(tmp[6])/1e25)
        powx_p.append(float(tmp[7]))
        pow1x_p.append(float(tmp[8]))
        jx_min_p.append(float(tmp[9]))
        jx_max_p.append(float(tmp[10]))
        el_maxx_p.append(float(tmp[11]))
        el_avx_p.append(float(tmp[12]))
        el_powx_p.append(float(tmp[13]))
        el_pow1x_p.append(float(tmp[14]))
        nemaxx_p.append(float(tmp[15])/1e25)
    f.close()
    nmax = 100
    dp = power_p[-1]/nmax
    p = np.zeros(nmax)
    e = np.zeros(nmax)
    b = np.zeros(nmax)
    for i in range(nmax):
        p[i] = dp*i
        e[i] = 3*math.sqrt(p[i]/10)
        b[i] = 0.653*3*math.sqrt(p[i]/10)

    thr_power = 7.25
    ep = 3*math.sqrt(thr_power/10)
    bp = 0.653*3*math.sqrt(thr_power/10)

    xticks = [x for x in range(7,16)]
    xticklabels = ['$%d$'%(x) for x in xticks]
    fontsize = 14
    fig = plt.figure(figsize = (15, 6))
    mp.rcParams.update({'font.size': fontsize})
    fig.subplots_adjust(bottom=-0.06)
    spx = 2
    spy = 3
    ax = fig.add_subplot(spx,spy,1)
    ax.plot(power_p, ezx_p, 'r-o', label='E')
    ax.plot(p, e, 'r:', label = 'E')
    ax.plot([thr_power,power_p[-1]], [ep,ep], 'r--' , label = 'E')
    # ax.plot([thr_power,power_p[-1]], [ep,ep], 'r--')
    ax.plot(power_p, bzx_p, 'b-v', label='B - Stationary')
    ax.plot(p, b, 'b:', label = 'B - Vacuum')
    ax.plot([thr_power,power_p[-1]], [bp,bp], 'b--', label = 'B - Threshold')
    # ax.plot([thr_power,power_p[-1]], [bp,bp], 'b--')
    ax.set_xlim([7,power_p[-1]+0.5])
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    plt.legend(loc = 'lower right', fontsize = 13, ncol=2)
    yticks = [1,2,3,4]
    ax.set_yticks(yticks)
    ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ax.set_xlabel('$P, PW$')
    ax.set_ylabel('$E,B, \\times 10^{11} statV/cm$')
    ax.text(5.8, 4, '(a)')
    rpow_p = []
    rpow1_p = []
    eff_p = []
    eff1_p = []
    
    for i in range(len(power_p)):
        el_powx_p[i] = power_p[i] - el_powx_p[i]
        powx_p[i] = power_p[i] - powx_p[i]
        rpow_p.append(power_p[i] - powx_p[i] - 2*el_powx_p[i])
        print power_p[i], powx_p[i], el_powx_p[i], power_p[i] - powx_p[i] - 2*el_powx_p[i]
        eff_p.append((powx_p[i] + 2*el_powx_p[i])/power_p[i])
        eff1_p.append(powx_p[i]/power_p[i])

    # power1_p = [0, thr_power] + power_p
    # rpow1_p = [0, thr_power] + rpow_p
    # powx1_p = [0, 0] + powx_p
    # el_powx1_p = [0, 0] + el_powx_p
    power1_p = power_p
    rpow1_p = rpow_p
    powx1_p = powx_p
    el_powx1_p = el_powx_p
        
    ax = fig.add_subplot(spx,spy,2)
    ax.plot(power1_p, rpow1_p, 'b-o', label = 'Reflected')
    ax.plot(power1_p, powx1_p, 'g-v', label = 'Photons')
    ax.plot(power1_p, el_powx1_p, 'r-^', label = 'Electrons')
    # ax.plot([thr_power, thr_power], [0, 8], 'b--', label = 'Threshold')
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    yticks = [2,4,6,8]
    ax.set_yticks(yticks)
    ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ax.set_xlabel('$P, PW$')
    ax.set_ylabel('Radiated power, PW')
    # ax.set_xticks([0,5,thr_power,10,15])
    plt.legend(bbox_to_anchor=(1., 0.5), fontsize = 13)
    ax.text(5.8, 8, '(b)')
    # ax = fig.add_subplot(spx,spy,3)
    # ax.plot([thr_power] + power_p, [0] + eff1_p, 'g-o', label = 'Conversion')
    # ax.plot([thr_power]+ power_p, [0] + eff_p, 'b-v', label = 'Absorption')
    # ax.set_xlabel('Power, PW')
    # ax.set_ylabel('Efficiency')
    # plt.legend(loc = 'lower right')
   
    ax = fig.add_subplot(spx,spy,4)
    ax.plot(power_p, nex_p, 'b-o', label = 'Stationary')
    ax.plot(power_p, nemaxx_p, 'r-v', label = 'Max')
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    yticks = [0,1,2,3,4,5]
    ax.set_yticks(yticks)
    ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ax.set_xlabel('$P, PW$')
    ax.set_ylabel('$N_e, \\times 10^{25} cm^{-3}$')
    plt.legend(loc = 'upper left', fontsize = 13)
    ax.text(5.8, 5, '(d)')
    # fig = plt.figure()
    # nex = fig.add_subplot(1,1,1)
    # nex.set_title('Ne')
    # nex.plot(power_p, ncrx_p)
    # nex.set_xlabel('Power, PW')
    # nex.set_ylabel('$N_e/N_{cr}$')
    # plt.show()
    # plt.close()

    maxx = fig.add_subplot(spx,spy,3)
    m, = maxx.plot(power_p, maxx_p, 'b-v')
    mel, = maxx.plot(power_p, el_maxx_p, 'b-^')
    maxx.set_ylim([0, 1.5])
    maxx.set_xlabel('$P, PW$')
    maxx.set_ylabel('Maximal energy, GeV')
    maxx.yaxis.label.set_color(m.get_color())
    maxx.spines["left"].set_color(m.get_color())
    maxx.tick_params(axis='y', colors=m.get_color())
    yticks = [0.5,0.9,1.3]
    maxx.set_yticks(yticks)
    maxx.set_yticklabels(['$%.1f$'%(x) for x in yticks])
    maxx.text(5.6, 1.5, '(c)')
    avx = maxx.twinx()
    a, = avx.plot(power_p, avx_p, 'r-v')
    ael, = avx.plot(power_p, el_avx_p, 'r-^')
    avx.set_ylim([0, 1.1*max(el_avx_p)])
    avx.set_ylabel('Average energy, GeV')
    avx.yaxis.label.set_color(a.get_color())
    avx.spines["right"].set_color(a.get_color())
    avx.spines["left"].set_color(m.get_color())
    avx.tick_params(axis='y', colors=a.get_color())
    avx.legend([m,mel,a,ael], ['$ ,$','$ ,$',' - Photons',' - Electrons'], bbox_to_anchor=(1, 0.5), fontsize = 13, ncol=2, labelspacing=0.05)
    yticks = [0.03,0.22,0.27,0.32]
    avx.set_yticks(yticks)
    avx.set_yticklabels(['$%.2f$'%(x) for x in yticks])
    avx.set_xticks(xticks)
    avx.set_xticklabels(xticklabels)


    f = open('charge.txt', 'r')
    power_p = []
    charge = []
    el_flux = []
    ph_flux = []
    for line in f:
        tmp = line.split()
        power_p.append(float(tmp[0]))
        charge.append(float(tmp[1]))
        el_flux.append(float(tmp[2]))
        ph_flux.append(float(tmp[3]))
    f.close()
    
    fl_ax = fig.add_subplot(spx,spy,6)
    fl_ax.plot(power_p, ph_flux, 'g-v', label = 'Photons')
    fl_ax.plot(power_p, el_flux, 'r-^', label = 'Electrons')
    fl_ax.set_ylabel('$Flux, \\times 10^{23} s^{-1}$')
    fl_ax.set_xlabel('$P, PW$')
    yticks = [0,2,4,6,8]
    fl_ax.set_yticks(yticks)
    fl_ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    fl_ax.text(5.8, 9, '(f)')
    plt.legend(loc = 'upper left', fontsize = 13)
    ch_ax = fl_ax.twinx()
    ch_ax.plot(power_p, charge, 'b-o', label = 'Charge')
    ch_ax.set_xticks(xticks)
    ch_ax.set_xticklabels(xticklabels)
    yticks = [10,20,30,40,50]
    ch_ax.set_yticks(yticks)
    ch_ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ch_ax.set_xlabel('$P, PW$')
    ch_ax.set_ylabel('$Charge, pC$')
    plt.legend(loc = 'lower right', fontsize = 13)
    f = open('jfull.txt', 'r')
    power_p = []
    jp = []
    jn = []
    jf = []
    jfm = []
    for line in f:
        tmp = line.split()
        power_p.append(float(tmp[0]))
        jp.append(float(tmp[1]))
        jn.append(float(tmp[2]))
        jf.append(float(tmp[3]))
        jfm.append(float(tmp[4]))
    f.close()
    
    ax = fig.add_subplot(spx,spy,5)
    ax.plot(power_p, jp, 'r-o', label='$r < 0.44 \lambda$')
    # ax.plot(power_p, jn, 'b-o', label='negative')
    ax.plot(power_p, jf, 'g-v', label='full')
    ax.plot(power_p, jfm, 'k-^', label='full max')
    ax.set_ylim([0, 17])
    ax.set_ylabel('$J_z, MA$')
    ax.set_xlabel('$P, PW$')
    yticks = [0,5,10,15]
    ax.set_yticks(yticks)
    ax.set_yticklabels(['$%d$'%(x) for x in yticks])
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    ax.text(5.8, 17, '(e)')
    plt.legend(loc = 'upper left', fontsize = 13)
    '''fig = plt.figure()
    maxx = fig.add_subplot(1,1,1)
    m, = maxx.plot(power_p, el_maxx_p, 'b')
    maxx.set_ylim([0, 1.1*max(el_maxx_p)])
    maxx.set_title('Max energy')
    maxx.set_xlabel('Power, PW')
    maxx.set_ylabel('Maximal electron energy, GeV')
    
    avx = maxx.twinx()
    a, = avx.plot(power_p, el_avx_p, 'r')
    avx.set_ylim([0, 1.1*max(el_avx_p)])
    avx.set_ylabel('Average electron energy, GeV')
    avx.legend([m,a], ['Maximal', 'Average'], loc = 'lower right')
    plt.show()
    plt.close()'''
    
    # fig = plt.figure()
    # maxx = fig.add_subplot(1,1,1)
    # m, = maxx.plot(power_p, maxx_p, 'b')
    # maxx.set_ylim([0, 1.1*max(maxx_p)])
    # maxx.set_title('Max energy')
    # maxx.set_xlabel('Power, PW')
    # maxx.set_ylabel('Maximal photon energy, GeV')
    
    # ezx = maxx.twinx()
    # a, = ezx.plot(power_p, ezx_p, 'r')
    # ezx.set_ylim([0, 1.1*max(ezx_p)])
    # ezx.set_ylabel('Stationary E field')
    # plt.legend([m,a], ['Max energy', 'Electric field'], loc = 'lower right')
    # plt.show()
    # plt.close()
    
    # power_p1 = []
    # nmin = []
    # nmax = []
    # f = open('nmin_nmax.txt')
    # for line in f:
    #     tmp = line.split()
    #     power_p1.append(float(tmp[0]))
    #     nmin.append(float(tmp[1]))
    #     nmax.append(float(tmp[2]))
    # f.close()

    # power_p2 = []
    # jneg = []
    # jpos = []
    # f = open('jpos_jneg.txt')
    # for line in f:
    #     tmp = line.split()
    #     power_p2.append(float(tmp[0]))
    #     jpos.append(float(tmp[1]))
    #     jneg.append(float(tmp[2]))
    # f.close()
    # print jpos, jneg
    # mp.rcParams.update({'font.size': 16})
    # fig = plt.figure()
    # maxx = fig.add_subplot(1,2,1)
    # m1, = maxx.plot(power_p1, nmin, 'r-o')
    # maxx.set_ylim([0, 1.05*max(nmin)])
   
    # maxx.set_xlabel('Power, PW')
    # maxx.set_ylabel('Number of particles')

    # jx = maxx.twinx()
    # j1, = jx.plot(power_p2, jpos, 'r--o')
    # jx.set_ylim([0, 1.1*max(jpos)])
    # jx.set_xlabel('Power, PW')
    # jx.set_ylabel('$+J_z, MA$')

    # maxx = fig.add_subplot(1,2,2)
    # m2, = maxx.plot(power_p1, nmax, 'b-o')
    # maxx.set_ylim([0, 1.05*max(nmax)])
    
    # maxx.set_xlabel('Power, PW')
    # # maxx.set_ylabel('Number of particles')

    # jx = maxx.twinx()
    # j2, = jx.plot(power_p2, jneg, 'b--o')
    # jx.set_ylim([0, 1.1*max(jpos)])
    # jx.set_xlabel('Power, PW')
    # jx.set_ylabel('$-J_z, MA$')
    
    # plt.legend([m1,m2,j1,j2], ['$r<0.4 \lambda$', '$r>0.4 \lambda$', '$+J_z$', '$-J_z$'], loc = 'upper left')
    
    # plt.show()
    # plt.close()
    
    # fig = plt.figure()
    # ezx = fig.add_subplot(4,2,1)
    # ezx.set_title('Electric field')
    # ezx.plot(power_p, ezx_p)
    # ezx.set_ylim([0, max(ezx_p)])
    # bzx = fig.add_subplot(4,2,2)
    # bzx.set_title('Magnetic field')
    # bzx.plot(power_p, bzx_p)
    # bzx.set_ylim([0, max(bzx_p)])
    # ncrx = fig.add_subplot(4,2,4)
    # ncrx.set_title('Ne/Ncr')
    # ncrx.plot(power_p, ncrx_p)
    # ncrx.set_ylim([0, max(ncrx_p)])
    # nex = fig.add_subplot(4,2,3)
    # nex.set_title('Ne')
    # nex.plot(power_p, nex_p)
    # nex.set_ylim([0, max(nex_p)])
    # powx = fig.add_subplot(4,2,6)
    # powx.set_title('Power')
    # powx.plot(power_p, powx_p)
    # powx.set_ylim([0, max(powx_p)])
    # pow1x = powx.twinx()
    # pow1x.plot(power_p, pow1x_p, 'g')
    # pow1x.set_ylim([0, max(pow1x_p)])
    # maxx = fig.add_subplot(4,2,7)
    # maxx.set_title('Max energy')
    # maxx.plot(power_p, maxx_p)
    # maxx.set_ylim([0, max(maxx_p)])
    # avx = fig.add_subplot(4,2,8)
    # avx.plot(power_p, avx_p)
    # avx.set_ylim([0, max(avx_p)])
    # avx.set_title('Av energy')
    # jx = fig.add_subplot(4,2,5)
    # jx.set_title('Number of particles')
    # jx.plot(power_p, jx_min_p)
    # jx.plot(power_p, jx_max_p)
    # jx.set_ylim([0, max(jx_max_p)])
    picname = picspath + '/' + "fig4.png"
    # plt.tight_layout()
    plt.savefig(picname, bbox_inches='tight', dpi=128)
    plt.close()
    plt.show()
    
if __name__ == '__main__':
    main()
