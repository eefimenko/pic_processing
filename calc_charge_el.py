#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.colors import colorConverter
import matplotlib as mp
import numpy as np
import math
import utils
import sys
import os

def read_field(file,nx,ny,norm = 1.):
    f = open(file, 'r')
    tmp = [[float(x)/norm for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for i in range(nx):
        row = []
        for j in range(ny):
            index = i + j*nx
            row.append(array[index] + 1e-9)
        field.append(row)
    return field

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def max2d(array):
    return max([max(x) for x in array])

def calc_current(array,nx,ny,dx,dy,r):
    num = 0.
    for i in range(nx):
        x = (i - nx/2)*dx
        for j in range(ny):
            y = (j-ny/2)*dy
            if x*x + y*y < r*r:
                num += array[i][j]
    return num

def main():
    picspath = 'pics'

    n = len(sys.argv)
    height = 9.66
    width = 15
    fig = plt.figure(figsize = (width/2.54, height/2.54))
    mp.rcParams.update({'font.size': 20})
    ph_flux = fig.add_subplot(1,1,1)
    ez = ph_flux.twinx()
    
      
    boundary = 1
    electronMass = 9.10938e-28
    electronCharge = 4.8032e-10
    const_a_p = 7.81441e-9
    filename = ''
    elcharge = []
    power_p = []
    elflux = []
    phflux = []
    elcharge_all = []
    elflux_all = []
    phflux_all = []
    for i in range(n-1):
        path = sys.argv[i+1] + '/'
        config = utils.get_config(path + "/ParsedInput.txt")
        dx = float(config['Step_X'])
        dy = float(config['Step_Y'])
        dz = float(config['Step_Z'])
        nx = int(config['MatrixSize_X'])
        ny = int(config['MatrixSize_Y'])
        BOIterationPass = int(config['BOIterationPass'])
        TimeStep = float(config['TimeStep'])
        dt = BOIterationPass*TimeStep*1e15
        step = float(config['TimeStep'])*float(config['BOIterationPass'])
        omega = float(config['Omega'])
       
        T = 2*math.pi/omega
        nper = int(T/step)/2
        dv = 2*dx*dy*dz
        coeffj = 2. * 1.6e-19 * 3e8 /(2*dz*1e-2)*1e-6
        wl = float(config['Wavelength'])
        print 'J = ', coeffj
        ppw = float(config['PeakPowerPW'])
        
        power_p.append(ppw)
        ncr = electronMass * omega * omega/(8. * math.pi * electronCharge * electronCharge)
        a0 = const_a_p * math.sqrt(ppw*1e22)
        
        ax_ = []
        ax1_ = []
        ez_t = []
        pow_t = []
        pow1_t = []
        max_t = []
        av_t = []
        br_full_t = []
        br_100mev_t = []
        br_1gev_t = []
        br_full1_t = []
        br_100mev1_t = []
        br_1gev1_t = []
        S = 2 * math.pi * 0.1 * 0.1 * wl * wl * 100 # square of source mm^2, r = 0.1 lambda 
        ds = 1.5e4 # mrad^2
        f = open(path + 'br_t.dat', 'r')
        i = 0
        T1 = 2*math.pi/omega*1e15
        for line in f:
            tmp = line.split()
            ax_.append(float(tmp[0])/T1)
            ez_t.append(float(tmp[1])/1e11)

            pow_t.append(ppw-float(tmp[2]))
            pow1_t.append(float(tmp[2])/ppw)
            max_t.append(float(tmp[3]))
            av_t.append(float(tmp[4]))
            ax1_.append(float(tmp[0])/T1 - 2.)
            br_full_t.append(float(tmp[5])/1e23)
            br_100mev_t.append(float(tmp[6]))
            br_1gev_t.append(float(tmp[7])/1e23)
            br_full1_t.append(float(tmp[5])/S/ds)
            br_100mev1_t.append(float(tmp[6])/S/ds)
            br_1gev1_t.append(float(tmp[7])/S/ds)

        el_ax_ = []
        el_ax1_ = []
        el_ez_t = []
        el_pow_t = []
        el_pow1_t = []
        el_max_t = []
        el_av_t = []
        el_br_full_t = []
        el_br_100mev_t = []
        el_br_1gev_t = []
        el_br_full1_t = []
        el_br_100mev1_t = []
        el_br_1gev1_t = []
        S = 2 * math.pi * 0.1 * 0.1 * wl * wl * 100 # square of source mm^2, r = 0.1 lambda 
        ds = 1.5e4 # mrad^2
        f = open(path + 'el_br_t.dat', 'r')
        i = 0
        T1 = 2*math.pi/omega*1e15
        
        nn = int(T1/dt)
        print nn
        for line in f:
            tmp = line.split()
            el_ax_.append(float(tmp[0])/T1)
            el_ez_t.append(float(tmp[1])/1.18997e+8/a0)

            el_pow_t.append(ppw-float(tmp[2]))
            el_pow1_t.append(float(tmp[2])/ppw)
            el_max_t.append(float(tmp[3]))
            el_av_t.append(float(tmp[4]))
            el_ax1_.append(float(tmp[0])/T1 - 2.)
            el_br_full_t.append(float(tmp[5])/1e23)
            el_br_100mev_t.append(float(tmp[6]))
            el_br_1gev_t.append(float(tmp[7])/1e23)
            el_br_full1_t.append(float(tmp[5])/S/ds)
            el_br_100mev1_t.append(float(tmp[6])/S/ds)
            el_br_1gev1_t.append(float(tmp[7])/S/ds)
        nperiods = 8
        start_period = -nperiods
        nbegin = int(start_period*nn) - len(el_br_1gev_t)%10
        nend = len(el_br_1gev_t)%10*(-1) - 1
        print nbegin, nend
        print max(el_br_1gev_t[nbegin:nend]), nperiods, T1, max(el_br_1gev_t[nbegin:nend])*1e23*T1*0.5*0.5*1e-15*1.6e-19
        ch = np.sum(el_br_1gev_t[nbegin:nend])/(nperiods*nn)*0.5*T1*1e23*1e-15*1.6e-19/1e-12
        ch_all = np.sum(el_br_full_t[nbegin:nend])/(nperiods*nn)*0.5*T1*1e23*1e-15*1.6e-19/1e-12
        ph_fl = br_1gev_t[nbegin:nbegin+nn]
        ez_fl = ez_t[nbegin:nbegin+nn]
        ph_fl_all = br_full_t[nbegin:nbegin+nn]
       
        phflux.append(max(ph_fl))
        phflux_all.append(max(ph_fl_all))
        ax_fl = ax_[:nn]
        print len(ph_fl), len(ax_fl)
        el_fl = el_br_1gev_t[nbegin:nbegin+nn]
        el_fl_all = el_br_full_t[nbegin:nbegin+nn]
        for i_ in range(nn):
            for j_ in range(1,nperiods):
                ph_fl[i_] += br_1gev_t[nbegin + j_*nn + i_]/nperiods
                el_fl[i_] += el_br_1gev_t[nbegin + j_*nn + i_]/nperiods
        elflux.append(max(el_fl))
        elflux_all.append(max(el_fl_all))
        elcharge.append(ch)
        elcharge_all.append(ch_all)
        ph_, = ph_flux.plot(ax_fl,ph_fl, 'b', linewidth=2.0)
#        ph_flux.set_yscale('log')
        #ph_flux.set_xlim([0,20])
        ph_flux.set_ylim([0,6])
        el_, = ph_flux.plot(ax_fl,el_fl, 'r-o', linewidth=2.0)
        #el_flux.set_xlim([0,20])
        #el_flux.set_ylim([0,7])
        
        ph_flux.set_ylabel('Flux, $10^{23} s^{-1}$')
        ez.set_ylabel('$E_z, \\times 10^{11} CGS$')
        ph_flux.set_xlabel('Time, T')
        ez.set_xlabel('Time, T')
        ph_flux.set_xticks([0,0.25,0.5,0.75,1])
        ph_flux.set_xticklabels([0,0.25,0.5,0.75,1])
        ph_flux.set_yticks([0,3,6])
        ph_flux.set_yticklabels([0,3,6])
        ez.set_yticks([0,1.25,2.5])
        ez.set_yticklabels([0,1.25,2.5])
        ez_, = ez.plot(ax_fl,ez_fl, 'g-^', linewidth=2.0)
#        el_flux.set_yscale('log')
        # ph_flux.legend([ph_, el_, ez_],['Photons', 'Electrons', 'Ez'], loc = 'upper right')
    # plt.tight_layout()
    # plt.show()
    # fig.savefig('10pw_burst.png', dpi=300, bbox_inches='tight')
    # plt.close()

    f = open('charge_.txt', 'w')
    print elcharge, elflux, phflux
    for i in range(len(power_p)):
        f.write('%lf %le %le %le\n'%(power_p[i], elcharge[i], elflux[i], phflux[i]))
    f.close()

    f = open('charge_all_.txt', 'w')
    print elcharge, elflux, phflux
    for i in range(len(power_p)):
        f.write('%lf %le %le %le\n'%(power_p[i], elcharge_all[i], elflux_all[i], phflux_all[i]))
    f.close()
    fig1 = plt.figure(figsize = (width/2.54, height/2.54))
    charge = fig1.add_subplot(1,1,1)
    charge.set_title('Total charge > 1GeV')
    charge.plot(power_p, elcharge, 'g-o')
    charge.set_xlabel('Power, PW')
    charge.set_ylabel('Total charge of one burst, pC')
    fig2 = plt.figure()
    flux = fig2.add_subplot(1,1,1) 
    flux.plot(power_p, elflux, 'r-o', label = 'Electrons')
    flux.plot(power_p, phflux, 'g-o', label = 'Photons')
    flux.set_xlabel('Power, PW')
    flux.set_ylabel('$Flux, 10^{23} s^{-1}$')
    plt.legend(loc = 'upper left')
    fig3 = plt.figure()
    charge = fig3.add_subplot(1,1,1)
    charge.set_title('Total charge')
    charge.plot(power_p, elcharge_all, 'g-o')
    charge.set_xlabel('Power, PW')
    charge.set_ylabel('Total charge of one burst, pC')
    fig4 = plt.figure()
    flux = fig4.add_subplot(1,1,1)
    flux1 = flux.twinx()
    flux.plot(power_p, elflux_all, 'r-o', label = 'Electrons')
    flux1.plot(power_p, phflux_all, 'g-o', label = 'Photons')
    flux.set_xlabel('Power, PW')
    flux.set_ylabel('$Flux, 10^{23} s^{-1}$')
    plt.legend(loc = 'upper left')
    plt.show()
if __name__ == '__main__':
    main()
