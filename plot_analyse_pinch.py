#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_file(filename):
    f = open(filename, 'r')
    axis = []
    e2max = []
    b2max = []
    nemax = []
    gammaav = []
    wp = []
    ndtwp = []
    ndxne = []
    dxne = []
#    if 'nc_2' in filename:
#        num = 4
#    else:
#        num = 1
    a0 = math.sqrt(27./10)*3e11
    for line in f:
        tmp = line.split()
        axis.append(float(tmp[0])/3.)
        e2max.append(float(tmp[1])/a0)
        b2max.append(float(tmp[2])/(a0*0.65))
        nemax.append(float(tmp[3]))
        gammaav.append(float(tmp[4]))
        wp.append(float(tmp[5]))
        ndtwp.append(float(tmp[6]))
        ndxne.append(float(tmp[7]))
        dxne.append(float(tmp[8])*1e4/0.9)
    t0 = axis[0]
    for i in range(len(axis)):
        axis[i] -= t0

    return axis, e2max, b2max, nemax, gammaav, wp, ndtwp, ndxne, dxne

def main():
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
    n = len(sys.argv) - 1
    fig1 = plt.figure(figsize=(10,5))
    ax1 = fig1.add_subplot(1,1,1)
    ax1.set_ylabel('$E_{max}/E_{vac}$')
    ax1.set_xlim([0,0.8])
    ax1.set_xlabel('$t/T$')
#    ax1.set_yscale('log')
#    ax1.set_ylim([0, 1.8])
    fig2 = plt.figure(figsize=(10,5))
    ax2 = fig2.add_subplot(1,1,1)
    ax2.set_ylabel('$B_{max}|B_{vac}$')
    ax2.set_xlim([0,0.8])
    ax2.set_xlabel('$t/T$')
#    ax2.set_ylim([0, 1.8])
#    ax2.set_yscale('log')
    fig3 = plt.figure(figsize=(10,5))
    ax3 = fig3.add_subplot(1,1,1)
    ax3.set_ylabel(u'$n_e$ см$^{-3}$')
    ax3.set_yscale('log')
    ax3.set_xlim([0,0.8])
    ax3.set_xlabel('$t/T$')
#    ax3.set_ylim([1e25, 1e28])
    fig4 = plt.figure(figsize=(10,5))
    ax4 = fig4.add_subplot(1,1,1)
    ax4.set_ylabel('$\gamma_{av}$')
    ax4.set_ylim([0,1500])
    ax4.set_xlim([0,0.8])
    ax4.set_xlabel('$t/T$')
    fig5 = plt.figure(figsize=(10,5))
    ax5 = fig5.add_subplot(1,1,1)
    ax5.set_ylabel('$\omega_p$')
    ax5.set_xlim([0,0.8])
    ax5.set_xlabel('$t/T$')
#    ax5.set_yscale('log')
#    ax7.set_yscale('log')
    fig8 = plt.figure(figsize=(10,5))
    ax8 = fig8.add_subplot(1,1,1)
    ax8.set_ylabel('$r/\lambda$')
    ax8.set_xlim([0,0.8])
    ax8.set_xlabel('$t/T$')
    fig6 = plt.figure(figsize=(10,5))
    ax6 = fig6.add_subplot(1,1,1)
    ax6.set_ylabel('$T_p/dt$')
    ax6.set_xlim([0,0.8])
    ax6.set_xlabel('$t/T$')
#    ax6.set_yscale('log')
    ax6.set_ylim([0,10])
    fig7 = plt.figure(figsize=(10,5))
    ax7 = fig7.add_subplot(1,1,1)
    ax7.set_ylabel('$r/dx$')
    ax7.set_xlim([0,0.8])
    ax7.set_xlabel('$t/T$')
    colors = ['r', 'g', 'b', 'c', 'y', 'm', 'k']
    iterations = [23, 16, 24, 40, 70, 112]
    labels = ['$n_{\lambda} = 115$','$n_{\lambda} = 230$','$n_{\lambda} = 460$','$n_{\lambda} = 920$','$n_{\lambda} = 1840$','$n_{\lambda} = 3680$'   ]
    dt = 0
    for i in range(n):
        path = sys.argv[i+1]
        axis, e2max, b2max, nemax, gammaav, wp, ndtwp, ndxne, dxne = read_file(path + '/' + 'analyse_inst.dat')
        dt = axis[1] - axis[0]
        e1, = ax1.plot(axis, e2max, colors[i], label = labels[i])
        e1_, = ax1.plot([iterations[i]*dt], e2max[iterations[i]], colors[i], marker = 'h') 
        e2, = ax2.plot(axis, b2max, colors[i], label = labels[i])
        e2_, = ax2.plot([iterations[i]*dt], b2max[iterations[i]], colors[i], marker = 'h') 
        e3, = ax3.plot(axis, nemax, colors[i], label = labels[i])
        e3_, = ax3.plot([iterations[i]*dt], nemax[iterations[i]], colors[i], marker = 'h')
        e4, = ax4.plot(axis, gammaav, colors[i], label = labels[i])
        e4_, = ax4.plot([iterations[i]*dt], gammaav[iterations[i]], colors[i], marker = 'h') 
        e5, = ax5.plot(axis, wp, colors[i], label = labels[i])
        e5_, = ax5.plot([iterations[i]*dt], wp[iterations[i]], colors[i], marker = 'h') 
        e7, = ax7.plot(axis, ndxne, colors[i], label = labels[i])
        e7_, = ax7.plot([iterations[i]*dt], ndxne[iterations[i]], colors[i], marker = 'h')
        e8, = ax8.plot(axis, dxne, colors[i], label = labels[i])
        e8_, = ax8.plot([iterations[i]*dt], dxne[iterations[i]], colors[i], marker = 'h')
        e6, = ax6.plot(axis, ndtwp, colors[i], label = labels[i])
        e6_, = ax6.plot([iterations[i]*dt], ndtwp[iterations[i]], colors[i], marker = 'h')
       
    a = 0.4
    axis_ = [0, axis[-1]]
    tmp_ = [a, a]
    e6, = ax6.plot(axis_, tmp_, 'k', label = str(a))
    a = 1
#    axis_ = [0, axis[-1]]
#    tmp_ = [a, a]
#    e7, = ax7.plot(axis_, tmp_, 'k', label = str(a))
    picname = 'pics/cmp_pinch_e.png'
    ax1.legend(loc = 'upper right')
    fig1.savefig(picname, dpi = 256)
    picname = 'pics/cmp_pinch_b.png'
    ax2.legend(loc = 'upper right')
    fig2.savefig(picname, dpi = 256)
    picname = 'pics/cmp_pinch_ne.png'
    ax3.legend(loc = 'upper right')
    fig3.savefig(picname, dpi = 256)
    picname = 'pics/cmp_pinch_gamma.png'
    ax4.legend(loc = 'upper right')
    fig4.savefig(picname, dpi = 256)
    picname = 'pics/cmp_pinch_omega.png'
    ax5.legend(loc = 'upper right')
    fig5.savefig(picname, dpi = 256)
    picname = 'pics/cmp_pinch_tp_dt.png'
    ax6.legend(loc = 'upper right')
    fig6.savefig(picname, dpi = 256)
    picname = 'pics/cmp_pinch_r_dx.png'
    ax7.legend(loc = 'upper right')
    fig7.savefig(picname, dpi = 256)
    picname = 'pics/cmp_pinch_r_lambda.png'
    ax8.legend(loc = 'upper right')
    fig8.savefig(picname, dpi = 256)
    plt.show()

if __name__ == '__main__':
    main()
