#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import shutil
import os

def read_file(file, de):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
    if 'tr' in file and not 'fs' in file:
        array[0] = 0
#        print 'removed'
        array, de = adjust_array(array, de)
        
    for i in range(len(array)):
        nph  += array[i]
    for i in range(len(array)):
        array[i] *= (i+0.425)
   
    return array, nph

def read_file_sph(file, de):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
    if 'tr' in file and not 'fs' in file:
        array[0] = 0
#        print 'removed'
        array, de = adjust_array(array, de)
       
    for i in range(len(array)):
        nph  += array[i]/(i+0.425)/de
    for i in range(len(array)):
        array[i] /= de
#        array[i] = array[i]/(i+0.425)/de
   
    return array, nph

def check_length(file):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
       
    return len(array)

def adjust_array(array, de):
    n = len(array)
    adj_size = 10
    m = n/adj_size
    a = [0]*m
    for i in range(m):
        tmp = 0
        for j in range(adj_size):
            tmp += array[i*adj_size + j]
        a[i] = tmp
    return a, de*adj_size

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def read_trap(file):
    f = open(file, 'r')
    tmp = [[float(x) for x in line.split()] for line in f]
    f.close()
    tmp1 = tmp[0]
    res = 0
    for n in tmp1:
        res = res + n
    return res

def find_max_energy_full(array,step):
    n = len(array)
    full = 0
    imax = 0
    summ = 0
    nfull = 0
    av_en = 0
    n_av = 0
    for i in range(n):
        nfull += array[i]/(i+0.425)
        full += array[i]*step
    
    if nfull > 0:
        av_en = full/nfull
        n_av = int(av_en/step)

    for i in range(1,n):
        summ = summ + array[-i]*(n-i+0.425)*step
        if summ > 0.01*full:
            imax = i
            break
    if imax == 0:
        max_en = 0
    else:
        max_en = (n-imax+0.37) * step
#    en1 = max_en*1e-3
#    ratio = en1/step
#    print max_en, av_en
    ratio = 1
    return av_en*1000, max_en, array[n_av], array[-imax]

def read_one_spectrum_sph(path1, kind, nmin):
    config = utils.get_config(path1 + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV']) 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
#    print num, T
    nph_t = []
    nph1_t = []
    ne_t = []
    np_t = []
    step = x0*y0*1e15/T 
    
    dt = y0*float(config['QEDstatistics.OutputIterStep'])*1e15

    path = '/statdata/' + kind + '/EnSpSph/'
   
    emin = float(config['QEDstatistics.Emin']) 
    emax = float(config['QEDstatistics.Emax']) 
    ne = int(config['QEDstatistics.OutputN_E'])
    de = (emax - emin)/ne
   
    t = 0
    
    n = check_length(path1 + path + '%.4f.txt'%nmin)
    if 'tr' in path1 and not 'fs' in path1:
        n /= 10
        de *= 10
    axis = create_axis(n, de/ev*1e-9)

    spectrum1 = [0]*n
    nn = 0
    for i in range(nmin,nmin+num):
        name = path1 + path + '%.4f.txt'%i
#        print name
        sp1, nph = read_file_sph(name, de)
        nn += nph
        for k in range(n):
            if nph > 0:
                spectrum1[k] += sp1[k]/nph
    for k in range(n):
        spectrum1[k] /= num
    return spectrum1, axis

def read_one_spectrum(path1, kind, nmin):
    config = utils.get_config(path1 + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV']) 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
#    print num, T
    nph_t = []
    nph1_t = []
    ne_t = []
    np_t = []
    step = x0*y0*1e15/T 
    
    dt = y0*float(config['QEDstatistics.OutputIterStep'])*1e15

    path = '/statdata/' + kind + '/EnSp/'
 
    emin = float(config['QEDstatistics.Emin']) 
    emax = float(config['QEDstatistics.Emax']) 
    ne = int(config['QEDstatistics.OutputN_E'])
    de = (emax - emin)/ne
   
    t = 0
    
    n = check_length(path1 + path + '%.4f.txt'%nmin)
    if 'tr' in path1 and not 'fs' in path1:
        n /= 10
        de *= 10
    axis = create_axis(n, de/ev*1e-9)

    spectrum1 = [0]*n
    nn = 0
    for i in range(nmin,nmin+num):
        name = path1 + path + '%.4f.txt'%i
#        print name
        sp1, nph = read_file(name, de)
        nn += nph
        for k in range(n):
            if nph > 0:
                spectrum1[k] += sp1[k]/nph
    for k in range(n):
        spectrum1[k] /= n
    return spectrum1, axis

def main():
    picspath = 'pics'
    path = '.'
    enpath = '/statdata/ph/EnSp/'
    config = utils.get_config(path + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV']) 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
    dt = (x0*y0*1e15)/T
    nmin = 0	
    nmax = utils.num_files(path + enpath)
#    nmin = 340
#    nmax = 400
    delta = 1
    num = len(sys.argv)
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(path + enpath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(path + enpath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax-nmin) + ' files'
   
    av_ = []
    max_ = []
    av_s_ = []
    max_s_ = []
    av_ph_ = []
    max_ph_ = []
    av_s_ph_ = []
    max_s_ph_ = []
    ax_ = []
    t = 0
    for k in range(nmin,nmax,delta):
        t += dt
        
        spectrum_el, axis = read_one_spectrum(path, 'el', k)
        spectrum1_el, axis1 = read_one_spectrum_sph(path, 'el', k)
        spectrum_ph, axis = read_one_spectrum(path, 'ph', k)
        spectrum1_ph, axis1 = read_one_spectrum_sph(path, 'ph', k)
        step = axis[1] - axis[0]
        step1 = axis1[1] - axis1[0]
        av_en, max_en, tmp, tmp1 = find_max_energy_full(spectrum_el,step)
        av_en_s, max_en_s, tmp_s, tmp1_s = find_max_energy_full(spectrum1_el,step1)
       
        ax_.append(t)
        max_.append(max_en)
        av_.append(av_en)
        max_s_.append(max_en_s)
        av_s_.append(av_en_s)
        av_en, max_en, tmp, tmp1 = find_max_energy_full(spectrum_ph,step)
        av_en_s, max_en_s, tmp_s, tmp1_s = find_max_energy_full(spectrum1_ph,step1)
        max_ph_.append(max_en)
        av_ph_.append(av_en)
        max_s_ph_.append(max_en_s)
        av_s_ph_.append(av_en_s)
#        fig = plt.figure(num=None)
#        ax = fig.add_subplot(1,1,1)
#        ms = max(spectrum)
#        ms1 = max(spectrum1)
#        s = [x/ms for x in spectrum]
#        s1 = [x/ms1 for x in spectrum1]
#        ax.plot(axis, s)
#        ax.plot(axis1, s1)
#        ax.set_yscale('log')
#        plt.show()
        fig = plt.figure(num=None)
        mp.rcParams.update({'font.size': 8})
        
        picname = picspath + '/' + "cmp_el_ph_sp_%d.png"%k
        print picname
        ax = fig.add_subplot(2,1,1)
        ax.set_title('Spectrum in the whole space')
        m, = ax.plot(axis, spectrum_el, 'r', label = 'el')
        m1, = ax.plot(axis, spectrum_ph, 'g', label = 'ph')
        ax.set_yscale('log')
        plt.legend(loc='upper right')
        
        ax2 = fig.add_subplot(2,1,2)
        ax2.set_title('Spectrum on sphere')
        m2, = ax2.plot(axis1, spectrum1_el, 'r', label = 'el')
        m3, = ax2.plot(axis1, spectrum1_ph, 'g', label = 'ph')
        ax2.set_yscale('log')
        ax2.set_xlabel('Energy, GeV')
        plt.legend(loc='upper right')
        plt.savefig(picname)
        plt.close()
    fig = plt.figure(num=None)
    mp.rcParams.update({'font.size': 8})
    picname = picspath + '/' + "wo_en_%d.png"%ppw
    
    ax = fig.add_subplot(2,1,1)
    m, = ax.plot(ax_, max_, 'r', label = 'max el')
    m1, = ax.plot(ax_, max_ph_, 'g', label = 'max ph')
    m2, = ax.plot(ax_, max_s_, 'b', label = 'max el s')
    m3, = ax.plot(ax_, max_s_ph_, 'k', label = 'max ph s')
    ax.set_ylabel('Max energy, GeV')
    plt.legend(loc='upper right')
    ax2 = fig.add_subplot(2,1,2)
    a, = ax2.plot(ax_, av_, 'r', label = 'av el')
    a1, = ax2.plot(ax_, av_ph_, 'g', label = 'av ph')
    a2, = ax2.plot(ax_, av_s_, 'b', label = 'av el s')
    a3, = ax2.plot(ax_, av_s_ph_, 'k', label = 'av ph s')
    ax2.set_ylabel('Average energy, MeV')
    plt.legend(loc='upper right')
#    plt.show()
    plt.savefig(picname)
    plt.close()
if __name__ == '__main__':
    main()
