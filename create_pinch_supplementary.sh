cd rect_27pw_small
pinch_fig2a_reply.py b
convert pics/pinch_fig2a.png -trim ../pics/pinch_supp_b.png
cd ..
cd pinch_27pw_nanorod
pinch_fig2a_reply.py d
convert pics/pinch_fig2a.png -trim ../pics/pinch_supp_d.png
cd ..
cd pinch_27pw_nanowire
pinch_fig2a_reply.py f
convert pics/pinch_fig2a.png -trim ../pics/pinch_supp_f.png
cd ..
convert -append pics/pinch_supp_b.png pics/pinch_supp_d.png pics/pinch_supp_f.png pics/pinch_supp_right.png
convert pics/pinch_supp_right.png -trim pics/pinch_supp_right.png
