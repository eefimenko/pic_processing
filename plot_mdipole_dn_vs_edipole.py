#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib as mp
import matplotlib.colors as clr
import sys
import math
import utils
import numpy as np

def read_spectra(d, nmin, nmax):
    fname = d + '/spectra_nT_time_norm_%d_%d.dat' % (nmin,nmax)
    f = open(fname, 'r')
    energy = []
    axis = []
    for line in f:
        tmp = line.split()
        axis.append(float(tmp[0]))
        energy.append(float(tmp[1]))
    f.close()
    return axis, energy

def main():
    picspath = 'pics'
    dirs = ['/home/evgeny/d2/mdipole/test_25pw/', '/home/evgeny/d3/results/10pw']
    nmin = [180, 200, 300]
    nmax = [270, 260, 390]
    labels = [u'(а)', u'(б)', u'e-тип 25 ПВт']
    plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
    mp.rcParams.update({'font.size': 24})
    fig = plt.figure(num=None, figsize=(10*len(dirs), 5), tight_layout = True)
    for i in range(len(dirs)):
        config = utils.get_config(dirs[i] + '/ParsedInput.txt')
        nphi = int(config['QEDstatistics.OutputN_phi'])
        ntheta = int(config['QEDstatistics.OutputN_theta'])
        tmax = float(config.get('QEDstatistics.ThetaMax', 180))
        tmin = float(config.get('QEDstatistics.ThetaMin', 0))
        pmax = 360
        pmin = 0
        sp_ = np.zeros((nphi,ntheta))
        print i+1,len(dirs) 
        axel = fig.add_subplot(1, len(dirs), i+1)
        
        for j in range(nmin[i], nmax[i]):
            sp_ = utils.bo_file_load(dirs[i] + '/' + utils.angspsphpath,j,fmt='%.4f',verbose = 1)
        
        sp_ = np.reshape(sp_, (nphi,ntheta), order = 'F')
        sp_ = np.transpose(sp_)
        mmax = np.amax(sp_)
        axel.imshow(sp_, aspect = 'auto',  extent = [pmin, pmax, tmin, tmax], origin = 'lower', norm=clr.LogNorm(mmax*1e-3, mmax))
        axel.set_xlabel('$\phi$')
        axel.set_ylabel('$\\theta$')
        pticks = [0,90,180,270,360]
        tticks = [0,90,180]
        pticklabels = ['$0$','$\pi/2$','$\pi$', '$3\pi/2$','$2\pi$']
        tticklabels = ['$0$','$\pi/2$','$\pi$']
        axel.set_xticks(pticks)
        axel.set_yticks(tticks)
        axel.set_xticklabels(pticklabels)
        axel.set_yticklabels(tticklabels)
        axel.text(-30, 180, labels[i], fontsize=24)   
    picname = picspath + '/' + "dn_mdipole_vs_edipole_spectra.png"
    plt.savefig(picname, dpi=512)
    plt.close()
   
            
            
   
if __name__ == '__main__':
    main()
