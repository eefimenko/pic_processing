# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 17:21:27 2021

@author: bashi
"""
import numpy as np
import math as ma
from ParseInput import InputDictionary
import os
import ReadZipBinTxt as rzbt
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

LV=2.9979e10
c4pi=LV/4/ma.pi


def sin2pulse(x, x0, amp):
    return amp*((np.sin((x-x0)*2*np.pi/54.9365))**2)*np.sin((x-x0)*2*np.pi)*0.5*(np.tanh(10000*(x-x0))-np.tanh(10000*(x-x0-54.9365/2)))

def TakeTimeSeries(pathSim,fileSt, fileFin, folderpath,templfile,needx,needy,nx,ny):
    arr=np.empty(fileFin-fileSt+1)
    for i in range(fileSt,fileFin+1):
        buffer=np.fromfile(pathSim+'BasicOutput/data/'+folderpath+'/'+(templfile % i),dtype=np.float32,count=ny*nx).reshape(ny,nx)
        arr[i-fileSt]=buffer[needy,needx]
    return arr

def SaveFieldComponents(pathSim,needfolds,fileStFin,angar,foldModul,replace=False):
    dIn={}
    foldpathbase=foldModul+'/'+os.path.basename(pathSim[0:-1])+'/'
    if not os.path.isdir(foldpathbase):
        os.mkdir(foldpathbase)
    fldcomponents=[]
    for fld in needfolds:
        fldcomponents.append(foldpathbase+fld[0]+fld[-1])
        if not os.path.isdir(fldcomponents[-1]):
            os.mkdir(fldcomponents[-1])
    InputDictionary(pathSim+'ParsedInput.txt', dIn)
    nx=int(dIn["%s.SetMatrixSize_0" % needfolds[0]])
    nz=int(dIn["%s.SetMatrixSize_1" % needfolds[0]])
    xmax=float(dIn["%s.SetBounds_1" % needfolds[0]])
    zmax=float(dIn["%s.SetBounds_3" % needfolds[0]])
    pmlx=int(dIn["PML.Size_X"])
    pmly=int(dIn["PML.Size_Y"])
    pmlz=int(dIn["PML.Size_Z"])
    xmax=float(dIn["%s.SetBounds_1" % needfolds[0]])
    zmax=float(dIn["%s.SetBounds_3" % needfolds[0]])
    shiftrad=4*max(pmlx,pmly,pmlz)
    if (xmax<=zmax):
        radius=2*xmax/(nx-1)*(nx/2-shiftrad)
    else:
        radius=2*zmax/(nz-1)*(nz/2-shiftrad)
    for ifld,fld in enumerate(needfolds):
        for ang in angar:
            print("Field %s ang %g" % (fldcomponents[ifld],ang))
            curZ=radius*ma.cos(ang/180*ma.pi)
            curX=radius*ma.sin(ang/180*ma.pi)
            nxneed=int((curX+xmax)/(2*xmax)*(nx-1)+0.5)
            nzneed=int((curZ+zmax)/(2*zmax)*(nz-1)+0.5)
            if replace or not os.path.exists(fldcomponents[ifld]+'/%g.txt' % ang):
                buffer=TakeTimeSeries(pathSim,fileStFin[0],fileStFin[1],fld,'%.6d.bin',nxneed,nzneed,nx,nz)
                np.savetxt(fldcomponents[ifld]+'/%g.txt' % ang,buffer)

def ApproxField(field,timearray,numapprox,initvals):
    appfield=field[0:numapprox]
    apptime=timearray[0:numapprox]
    best_vals, covar = curve_fit(sin2pulse,apptime,appfield, p0=initvals)
    print(best_vals)
    signal=sin2pulse(timearray,best_vals[0],best_vals[1])
    reflect=field-signal
    return reflect

def TakeVectPoynt(pathSim,needfolds,fileStFin,angar,foldModul,rewrite=False):
    foldpathbase=foldModul+'/'+os.path.basename(pathSim[0:-1])+'/'
    foldSv=foldpathbase+'Sv/'
    if not os.path.isdir(foldSv):
            os.mkdir(foldSv)
    dIn={}
    InputDictionary(pathSim+'ParsedInput.txt', dIn)
    nx=int(dIn["MatrixSize_X"])
    nz=int(dIn["MatrixSize_Z"])
    ny=int(dIn["MatrixSize_Y"])
    pmlx=int(dIn["PML.Size_X"])
    pmly=int(dIn["PML.Size_Y"])
    pmlz=int(dIn["PML.Size_Z"])
    xmax=float(dIn['X_Max'])
    zmax=float(dIn['Z_Max'])
    ymax=float(dIn['Y_Max'])
    dx=2*xmax/(nx)
    dy=2*ymax/(ny)
    dz=2*xmax/(nz)
    w=float(dIn['Omega'])
    T=2*ma.pi/w
    timestep=float(dIn['TimeStep'])
    passpic=int(dIn['BOIterationPass'])
    dtpic=timestep*passpic/T
    nxpic=int(dIn["%s.SetMatrixSize_0" % needfolds[0]])
    nzpic=int(dIn["%s.SetMatrixSize_1" % needfolds[0]])
    xmaxpic=float(dIn["%s.SetBounds_1" % needfolds[0]])
    zmaxpic=float(dIn["%s.SetBounds_3" % needfolds[0]])
    shiftrad=4*max(pmlx,pmly,pmlz)
    if (xmaxpic<=zmaxpic):
        radius=2*xmaxpic/(nxpic-1)*(nxpic/2-shiftrad)
    else:
        radius=2*zmaxpic/(nzpic-1)*(nzpic/2-shiftrad)
    pointstartField=ma.sqrt((zmax-pmlz*dz)**2+(xmax-pmlx*dx)**2+(ymax-pmly*dy)**2)
    numapprox=int((pointstartField+radius)/(LV*T*dtpic))
    print(numapprox)
    delaytime=(pointstartField-radius)/(LV*T)
    fldcomponents=[]
    timearray=np.arange(fileStFin[0]*dtpic,(fileStFin[1]+1)*dtpic,dtpic)
    init_vals = [delaytime, 100]
    for fld in needfolds:
        fldcomponents.append(foldpathbase+fld[0]+fld[-1])
    fig=plt.figure(figsize=(5,4))
    for ang in angar:
        print(foldSv+'%g.txt'%ang)
        if rewrite or (not os.path.exists(foldSv+'%g.txt' % ang)):
            xnorm=np.sin(ang)
            znorm=np.cos(ang)
            fields=[]
            for ifld,fld in enumerate(fldcomponents):
                fieldSum=np.loadtxt(fld+'/%g.txt' % ang)
                if (ifld==1 or ifld==3 or ifld==5):
                    fields.append(ApproxField(fieldSum, timearray, numapprox, init_vals))
                else:
                    fields.append(fieldSum)
                ax=fig.add_subplot(1,1,1)
                ax.plot(fieldSum,color='k')
                ax.plot(fields[-1],color='r')
                ax.plot(fieldSum[0:numapprox],color='g')
                plt.savefig(fld+'/%g.png'% ang,dpi=300)
                for axf in fig.axes:
                    axf.clear()
                fig.clf()
            Sx=fields[1]*fields[5]-fields[2]*fields[4]
            Sz=-fields[3]*fields[1]+fields[0]*fields[4]
            S=np.sqrt(Sx**2+Sz**2)#Sx*(xnorm)+Sz*(znorm)
            ax=fig.add_subplot(1,1,1)
            ax.plot(S,color='k')
            plt.savefig(foldSv+('%g.png'% ang),dpi=300)
            for axf in fig.axes:
                axf.clear()
            fig.clf()
            np.savetxt(foldSv+('%g.txt' % ang),S)
    plt.close()
    
def FieldFFT(pathSim,pathfold,foldModul,angrange):
    foldpathbase=foldModul+'/'+os.path.basename(pathSim[0:-1])+'/'+pathfold+'/'
    dIn={}
    InputDictionary(pathSim+'ParsedInput.txt', dIn)
    w=float(dIn['Omega'])
    T=2*ma.pi/w
    timestep=float(dIn['TimeStep'])
    passpic=int(dIn['BOIterationPass'])
    dtpic=timestep*passpic/T
    fig=plt.figure(figsize=(5,4))
    for ang in range(angrange[0],angrange[1],angrange[2]):
        print(foldpathbase+'%g.txt'%ang)
        ft=np.loadtxt(foldpathbase+'%g.txt'%ang)
        fsp=np.fft.fft(ft)
        fsp=np.absolute(fsp)
        
        n=ft.size
        freq=np.fft.fftfreq(n,dtpic)
        print(n)    
        ax=fig.add_subplot()
        taken=int(n/2)-1
        if n%2==0:
            n=int(n/2)
        ax.set_xlim(freq[0],freq[taken])
        ax.set_ylim(fsp.max()/1e3,fsp.max())
        ax.set_yscale('log')
        ax.plot(freq[0:taken+1],fsp[0:taken+1])
        print(foldpathbase+('Spes%g.png'% ang))
        plt.savefig(foldpathbase+('Spes%g.png'% ang),dpi=300)
        for axf in fig.axes:
            axf.clear()
        fig.clf()
    plt.close()
            