#include "head.h"
#include <fftw3.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <math.h>
#include <string.h>

int create_directory(const char*name)
{
#ifndef WIN
    int status = mkdir(name, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (0 == status)
    {
        fprintf(stderr,"Creating %s: %s\n", name, strerror(errno));
        return 0;
    }
    else if (EEXIST == errno)
    {
        fprintf(stderr,"Creating %s: %s\n", name, strerror(errno));
        return 0;
    }
    else
    {
        fprintf(stderr,"Error while creating %s:%s\n", name, strerror(errno));
        exit(-1);
    }
#else
    int status = CreateDirectory(name, NULL);
    if (0 != status)
    {
        info_text_s("Creating %s\n", name);
        return 0;
    }
    else
    {
        int res = GetLastError();
        if (ERROR_ALREADY_EXISTS == res)
        {
            warning_text_s("Creating %s: Already exists\n", name);
            return 0;
        }
        else if (ERROR_PATH_NOT_FOUND == res)
        {
            error_text_s("Error while creating %s: Path not found\n", name);
            exit(-1);
        }
        else
        {
            error_text_s("Error while creating %s: Unknown error\n", name);
            exit(-1);
        }
    }
#endif
    return 0;
}


void HarmAng(int FileSt, int FileEnd, int nx, int ny, float xmax, int nAng, float minAng, float maxAng, float dt, char subFold[])
{
    float angShift=32;
    cout<<"HarmAng"<<endl;
    char fileName[100];
    char *folderMod="HarmAng";
    create_directory(folderMod);
    float radius=2*xmax/(nx-1)*(nx/2-angShift);
    sprintf(fileName,"%s/%s",folderMod,subFold);
    create_directory(fileName);
    char fold1[200];
    sprintf(fold1,"%s/%s",folderMod,subFold);

    for (int ia=0; ia<nAng; ia++)
	//for (int ia=0; ia<1; ia++)
    ///for (int ia=16; ia<17; ia++)
    {
        int numFiles=FileEnd-FileSt+1;
        mglData Ft(numFiles);
        mglData spF(numFiles/2);
        float curAng=minAng+(maxAng-minAng)/(nAng-1)*ia;
        float curX=radius*cos(curAng);
        float curY=radius*sin(curAng);
        int nxTaken=(curX+xmax)/(2*xmax)*(nx-1)+0.5;
        int nyTaken=(curY+xmax)/(2*xmax)*(nx-1)+0.5;
        cout<<"radius="<<sqrt((nxTaken-nx/2)*(nxTaken-nx/2)+(nyTaken-ny/2)*(nyTaken-ny/2))<<endl;

        fftw_complex inF[numFiles],outF[numFiles];

        float* buffer1=new float[nx*ny];
        for(int i=FileSt; i<=FileEnd; i++)
        ///for(int i=540; i<=680; i++)
        {
            //cout<<i<<endl;
            ifstream is1;
	    ofstream os1;
            sprintf(fileName,"%s/%.6d.bin",subFold,i);
            is1.open(fileName, std::ifstream::binary);
            is1.read((char*)buffer1, nx*ny*sizeof(float));
            //sprintf(fileName,"%s/%.6d.txt",subFold,i);
            //is1.open(fileName);
	    //for (int idx = 0; idx < nx*ny; ++idx)
	    //{
	    //	is1 >> buffer1[idx];
	    //}
	    //sprintf(fileName,"%s/%.6d.bin",subFold,i);
            //os1.open(fileName, std::ifstream::binary);
            //os1.write((char*)buffer1, nx*ny*sizeof(float));
            //is1.read((char*)buffer1, nx*ny*sizeof(float));
            //is1.close();
            Ft.a[i-FileSt]=buffer1[nx*nyTaken+nxTaken];///3*ny/4*nx+12
            inF[i-FileSt][0]=buffer1[nx*nyTaken+nxTaken];///3*ny/4*nx+12
            inF[i-FileSt][1]=0;
	    if (i < 151)
	    {
		Ft.a[i-FileSt] = 0;///3*ny/4*nx+12
		inF[i-FileSt][0]= 0;
	    }
        }
	cout << "Here" << endl;
        fftw_plan my_planF;
        my_planF = fftw_plan_dft_1d(numFiles, inF, outF, FFTW_FORWARD, FFTW_ESTIMATE);
        fftw_execute(my_planF);
        fftw_destroy_plan(my_planF);
        for (int i=0; i<numFiles/2; i++)
        {
            spF.a[i]=sqrt(outF[i][0]*outF[i][0]+outF[i][1]*outF[i][1]);
        }
        fstream fF,fspF,fApprox;
        sprintf(fileName,"%s/Ft_%g.txt",fold1,curAng/M_PI*180);
        fF.open(fileName,ios::out);
        sprintf(fileName,"%s/spF_%g.txt",fold1,curAng/M_PI*180);
        fspF.open(fileName,ios::out);
        for (int i=0; i<numFiles/2; i++)
        {
            fF<<Ft.a[i]<<" ";
            fspF<<spF.a[i]<<" ";
        }
        fspF.close();
        for (int i=numFiles/2; i<numFiles; i++)
        {
            fF<<Ft.a[i]<<" ";
        }
        fF.close();
        sprintf(fileName,"%s/Approx_%g.txt",fold1,curAng/M_PI*180);
        cout<<fileName<<endl;
        fApprox.open(fileName,ios::out);
        fApprox<<"{";
        for (int i=0; i<150; i++)
        {
            fApprox<<"{"<<i*1./20.<<","<<Ft.a[i]<<"},";
        }
        fApprox<<"}";
        fApprox.close();

        /*mglGraph * gr=new mglGraph(0,1000,500);
        gr->SubPlot(2,1,0);
        gr->SetRange('x',FileSt*dt,FileEnd*dt);
        gr->SetRange('y',Ft.Minimal(), Ft.Maximal());
        gr->Adjust();
        gr->Axis();
        gr->Plot(Ft,"2ro");


        gr->SubPlot(2,1,1);
        gr->SetRange('x',0,0.5/dt);
        gr->SetRange('y',spF.Maximal()/1e5, spF.Maximal());
        gr->SetFunc("","lg(y)");
        gr->Adjust();
        gr->Axis();
        gr->Plot(spF,"2ro");

        sprintf(fileName,"%s/Harm_%g.png",fold1,curAng/M_PI*180);
        gr->WritePNG(fileName);
        delete gr;*/
        delete []buffer1;
    }
}
