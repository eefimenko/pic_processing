#ifndef HEAD_H_INCLUDED
#define HEAD_H_INCLUDED
//#include "Pars.cpp"
//#include "Bfield.cpp"
#include <iostream>
#include <fstream>
#include <mgl2/mgl.h>
#include <stdio.h>
//#include <windows.h>
const float LV=2.99792e+10;
const float partMass=9.10938356e-28;
const float partCharge=4.80320427e-10;

using namespace std;
void ParsedParam(int &, int &, int &,int&, int&, int&, int&, int&, int&, int&, float &, float &,  float &, float &, float &, float &, float &, float &, float &, float&, float&);
void Brhophiz(int,int,int,int,float,float,float,float,float,float);
void Erhophiz(int,int,int,int,float,float,float,float,float,float);
void Jcomp(int, int, int, int, float, float, float);
void JcompM(int, int, int, int, float, float, float, float, float);
void Bvect(int, int, int, int, float, float, float,float,float,float);
void Bline(int, int, int, int, float, float, float,float,float,float);
void Jvect(int, int, int, int, float, float, float);
void Max_EBElJz_cm(int, int, int, int, float, float,float,float,float);
void ElEzJzEzt(int, int, int, int, float, float, float, float);
void revealMax(float *, int, int, int &, int &, float &);
bool cmprPos(float, float);
bool cmprNeg(float, float);
void revealStructure(float *, int, int, bool *);
void curStruc(int, int, int, int, float, float, float, float, float);
void revealStructure01(float *, int, int, bool *);
void curStruc01(int, int, int, int, float, float, float, float, float);
void ElPos(int, int, int, int, float, float, float);
void ElPosz(int, int, int, int, int, float, float,float,float);
void ElPosJz(int, int, int, int, float, float);
void curStruc_fRes(int , int , int , int , float , float , float , float, float , float );
void revealStructure_fRes(float *, int , int , bool *, float , int &, int &);
void EBEl1D(int, int, int, int, float, float);
void PhasePlane(int, int, int, int, float);
void PrfR(int, int, int, int,int,int,float);
void PrfRAv(int,int,int,int,int,int,int,float,float,float);
void Harm(int, int, int, int, float);
void Efield(int, int, int, int, float, float, float);
void ChargeE(int, int, int, int, float, float, float);
void JB(int, int, int, int, float, float, float,float);
void Evect(int, int, int, int, float, float, float,float);
void BinToTxt(int, int, int, int, char []);
void HarmAng(int, int, int, int,float, int, float, float, float, char []);
#endif // HEAD_H_INCLUDED
