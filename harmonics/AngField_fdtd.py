# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 00:39:12 2020

@author: Алексей
"""
from numpy import pi,sin,tanh,fabs
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import numpy as np
import os
import sys
from tqdm import tqdm

def semiinfinite(x, x0, x1, amp):
    return amp * sin((x-x0) * 2 * pi) * 0.5 * (1 - tanh(-2.7312*(x-x1)))

LV=2.9979e10
cd4pi=LV/4/pi
minAng=0
maxAng=360
dAng=1
timeSignal=181
dt=1/6.4
numFiles=10
delayTime=6.1
delay=6
amp=100
alpha0 = 10
ang=[]
diagPlasma=[]
diagVac=[]
path = sys.argv[1]

def readField(path, component, angle):
    key = os.path.join(path, 'data', 'HarmAng', component)
    fileName = key + '/Ft_' + str(angle) + '.txt'
    #data = []
    #with open(fileName) as f:
    #    for line in f:
    #        data.append([float(x) for x in line.split()])
    data_ = np.loadtxt(fileName)
    #np.array([data[0][i] for i in range (0,length)])
    #print(data_)
    return data_

def calcReflField(path, component, angle, time_axis):
    field = readField(path, component, angle)
    
    return field

def calcPointingVector(ex, ey, ez, bx, by, bz, sx, sy, sz):
    px = (ey*bz - by*ez)
    py = -(ex*bz - bx*ez)
    pz = (ex*by - bx*ey)
    #print("pointing", ey, by)
    #print(px, py, pz)
    return px*sx + py*sy + pz*sz

time_axis = np.array([i*0.05 for i in range (0,numFiles)])

ang_ = np.arange(minAng,maxAng,dAng)/180.*np.pi + np.pi/2
sin2 = np.sin(ang_)**2

for ia in tqdm(range(minAng,maxAng,dAng)):
#for ia in range (45,46):
    #print(ia)
    angCur=ia/360.*2*pi
    if (angCur>2*pi):
        angCur-=2*pi
    ang.append(angCur)
    ex = calcReflField(path, 'E2yx', ia, time_axis)
    ey = calcReflField(path, 'E2yy', ia, time_axis)
    ez = calcReflField(path, 'E2yz', ia, time_axis)
    bx = calcReflField(path, 'B2y_x', ia, time_axis)
    by = calcReflField(path, 'B2y_y', ia, time_axis)
    bz = calcReflField(path, 'B2y_z', ia, time_axis)
    #print("read", ey, by)
    sy = 0
    sx = np.sin(angCur)
    sz = np.cos(angCur)
    
    pvector = calcPointingVector(ex, ey, ez, bx, by, bz, sx, sy, sz)
  
    intensityVac = cd4pi * np.sum(pvector) #[fabs(cd4pi*signal[i
    #print(pvector, intensityVac)
    diagVac.append(intensityVac)
    if ia == 0 or ia == 80 or ia == 45:
        fsp = np.fft.fft(by)
        fsp = np.absolute(fsp)
            
        freq = np.fft.fftfreq(len(fsp), dt)
        fig=plt.figure(figsize=(5,4))
        ax=fig.add_subplot(111)
        ax.plot(freq, fsp,'r-')
        ax.set_title('Spectra')
        ax.set_yscale('log')
        ax.set_xlim([0,10])
        fig.savefig(os.path.join(path, 'spectra_%d.png' % ia),dpi=300)
        ax.clear()
        fig.clf()
        plt.close()

'''    
for ia in range (minAng,maxAng,dAng):
#for ia in range (45,46):
    print(ia)
    angCur=ia/360.*2*pi+pi/2
    if (angCur>2*pi):
        angCur-=2*pi
    ang.append(angCur)
    dataVac = []
    dataPlasma=[]
    dataVacB = []
    dataPlasmaB=[]
    
    xd = [i*dt for i in range (0,timeSignal)]
    
    fileName=keyVac+str(ia)+'.txt'
    with open(fileName) as f:
        for line in f:
            dataVac.append([float(x) for x in line.split()])
    dataVacApp = [dataVac[0][i] for i in range (0,timeSignal)]
   
    fileName=keyVacB+str(ia)+'.txt'
    with open(fileName) as f:
        for line in f:
            dataVacB.append([float(x) for x in line.split()])
    dataVacBApp = [dataVacB[0][i] for i in range (0,timeSignal)]    
    
    fileName=keyPlasma+str(ia)+'.txt'
    with open(fileName) as f:
        for line in f:
            dataPlasma.append([float(x) for x in line.split()])
    dataPlasmaApp = [dataPlasma[0][i] for i in range (0,timeSignal)]
    
    fileName=keyPlasmaB+str(ia)+'.txt'
    with open(fileName) as f:
        for line in f:
            dataPlasmaB.append([float(x) for x in line.split()])
    dataPlasmaBApp = [dataPlasmaB[0][i] for i in range (0,timeSignal)]
    
    from scipy.optimize import curve_fit
    init_vals = [delayTime, delay, amp]
    best_vals, covar = curve_fit(semiinfinite, xd, dataVacApp, p0=init_vals)
    print('Best vals', best_vals)
    x=[i*0.05 for i in range (0,numFiles)]
    signal=[semiinfinite(x[i],best_vals[0],best_vals[1],best_vals[2]) for i in range (0,numFiles)]
    reflect=[dataPlasma[0][i]-signal[i] for i in range (0,numFiles)]
    #ax.plot(dataVacApp, color='b')
    #ax.plot(signal, color='g')
    #ax.plot(reflect, color='r')
    #plt.show()
    #plt.close()
    best_valsB, covarB = curve_fit(semiinfinite, xd, dataVacBApp, p0=init_vals)
    print('Best vals B', best_valsB)
    signalB=[semiinfinite(x[i],best_valsB[0],best_valsB[1],best_valsB[2]) for i in range (0,numFiles)]
    reflectB=[dataPlasmaB[0][i]-signalB[i] for i in range (0,numFiles)]
    
    intensityVac=[fabs(cd4pi*signal[i]*signalB[i]) for i in range (0,numFiles)]
    intensityPlasma=[fabs(cd4pi*reflect[i]*reflectB[i]) for i in range (0,numFiles)]
    
    diagVac.append(sum(intensityVac)/1e14)
    diagPlasma.append(sum(intensityPlasma)/1e14)
    
    #import matplotlib.pyplot as pltCur
    
    #pltCur.plot(x,signalB,'r-')
    #pltCur.plot(x,reflectB,'b-')
    #pltCur.plot(x,dataPlasmaB[0],'k:')'''
diagVac = np.array(diagVac)
diagVac /= np.amax(diagVac)    
#print(diagVac)
ang = np.array(ang)
 
import matplotlib.pyplot as plt
fig=plt.figure(figsize=(5,4))
ax=fig.add_subplot(111,projection='polar')
ax.set_theta_offset(pi/2)
ax.set_rlabel_position(290)
#ax.plot(ang, diagPlasma,'b-')
ax.plot(ang,diagVac,'r-')
ax.plot((ang[-1],ang[0]),(diagVac[-1],diagVac[0]),'r-')
ax.plot(ang_, sin2, 'g')
#ax.plot((ang[-1],ang[0]),(diagPlasma[-1],diagPlasma[0]),'b-')
ax.set_title('Radiation pattern')
fig.savefig(os.path.join(path, 'diag.png'),dpi=300)
ax.clear()
fig.clf()
plt.close()

#plt.subplot(120,polar=True)
#plt.plot(ang,diagPlasma,'r-')
#plt.plot(ang,diagVac,'b-')
#ax.plot(ang,diagVac , color='r', linewidth=1.)
#ax = fig.add_subplot(121, projection='polar')
#ax.plot(ang,diagPlasma , color='b', linewidth=1.)


#plt.plot(xd, dataApp, 'b.')
#dataFit=[sin2pulse(xd[i],*best_vals) for i in range (0,timeSignal)]
#plt.plot(xd, dataFit, 'r-')


#signal=[sin2pulse(x[i],best_vals[0],best_vals[1]) for i in range (0,741)]
#reflect=[data[0][i]-signal[i] for i in range (0,741)]

#plt.plot(x,signal,'r--')
#plt.plot(x,reflect, 'b--')
#plt.plot(x,data[0],'k-')
#print(fabs(max(signal,key=abs)),fabs(max(reflect,key=abs)))

#keyName='Ft_'

#for i in range (0,10,2):
#    fileName=keyName+str(i)+'.txt'
#    print(fileName)
# print('best_vals: {}'.format(best_vals))
#from numpy import exp, linspace, random

#def gaussian(x, amp, cen, wid):
#    return amp * exp(-(x-cen)**2 / wid)

#from scipy.optimize import curve_fit

#x = linspace(-10, 10, 101)
#y = gaussian(x, 2.33, 0.21, 1.51) + random.normal(0, 0.2, x.size)

#init_vals = [1, 0, 1]  # for [amp, cen, wid]
#best_vals, covar = curve_fit(gaussian, x, y, p0=init_vals)
#print('best_vals: {}'.format(best_vals))
#print(x)
#print(y)

