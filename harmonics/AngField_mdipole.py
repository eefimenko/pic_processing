# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 00:39:12 2020

@author: Алексей
"""
import matplotlib as mp
#mp.use('Agg')
from numpy import pi,sin,tanh,fabs
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import numpy as np
import os
from tqdm import tqdm

def semiinfinite(x, x0, x1, amp):
    return amp * sin((x-x0) * 2 * pi) * 0.5 * (1 - tanh(-2.7312*(x-x1)))

LV=2.9979e10
cd4pi=LV/4/pi
minAng=0
maxAng=360
dAng=1
timeSignal=220
dt=0.03333333
numFiles=1000
delayTime=6.1
delay=6
amp=100
alpha0 = 10
ang=[]
diagPlasma=[]
diagVac=[]
power = 17
path_nl = "pdw_%dpw_2" % power
path_vac = "pdw_%dpw_2" % power

#keyVac='../../../../rad_27pw_vac/data/HarmAng/E2yx/Ft_'
#keyVacB='../../../../rad_27pw_vac/data/HarmAng/B2y_y/Ft_'
#keyPlasma='Ft_'
#keyPlasmaB='../B2y_y/Ft_'
pics_path = '/home/evgeny/Dropbox/tmp_pics'

def readField(path, component, angle, length):
    key = os.path.join(path, 'data', 'HarmAng', component)
    fileName = key + '/Ft_' + str(angle) + '.txt'
    data = []
    with open(fileName) as f:
        for line in f:
            data.append([float(x) for x in line.split()])
    data_ = np.array([data[0][i] for i in range (0,length)])
    return data_

def calcReflField(path_vac, path_nl, component, angle, time_axis, time_fit):
    nl_field = readField(path_nl, component, angle, len(time_axis))
    vac_field = readField(path_vac, component, angle, len(time_axis))
    #signal = semiinfinite(time_axis, delayTime, delay, amp)
    
    init_vals = [delayTime, delay, amp]
    best_vals, covar = curve_fit(semiinfinite, time_fit, vac_field[:len(time_fit)], p0=init_vals)
    #print(best_vals)
    #signal = np.zeros(len(time_axis))
    signal = semiinfinite(time_axis, best_vals[0], best_vals[1], best_vals[2])
    reflect = nl_field - signal
    '''fig=plt.figure()
    ax=fig.add_subplot(121)
    ax1=fig.add_subplot(122)
    ax.plot(signal, color='g')
    ax.plot(reflect, color='r')
    ax.plot(vac_field[:len(time_fit)], color='b')
    ax1.plot(nl_field)
    ax1.plot(reflect, color='r')
    plt.show()
    plt.close()'''
    return vac_field - signal, reflect

def calcPointingVector(ex, ey, ez, bx, by, bz, sx, sy, sz):
    px = (ey*bz - by*ez)
    py = -(ex*bz - bx*ez)
    pz = (ex*by - bx*ey)
    #px = -by*ez
    #py = 0
    #pz = ex*by
    #return np.sqrt(pz*pz + py*py + px*px)
    return (px*sx + py*sy + pz*sz)

time_fit = np.array([i*dt for i in range (0,timeSignal)])
time_axis = np.array([i*dt for i in range (0,numFiles)])

ang_ = np.arange(minAng,maxAng,dAng)/180.*np.pi + np.pi/2
sin2 = np.sin(ang_)**2
#T=2*ma.pi/w

dt = 0.03333
fontsize = 14
mp.rcParams.update({'font.size': fontsize})

for ia in tqdm(range (minAng,maxAng,dAng)):
#for ia in range (47,48):
    #print(ia)
    angCur=ia/360.*2*pi#+pi/2
    if (angCur>2*pi):
        angCur-=2*pi
    ang.append(angCur)
    vac_ex, ex = calcReflField(path_vac, path_nl, 'E2yx', ia, time_axis, time_fit)
    vac_ey, ey = calcReflField(path_vac, path_nl, 'E2yy', ia, time_axis, time_fit)
    vac_ez, ez = calcReflField(path_vac, path_nl, 'E2yz', ia, time_axis, time_fit)
    vac_bx, bx = calcReflField(path_vac, path_nl, 'B2y_x', ia, time_axis, time_fit)
    vac_by, by = calcReflField(path_vac, path_nl, 'B2y_y', ia, time_axis, time_fit)
    vac_bz, bz = calcReflField(path_vac, path_nl, 'B2y_z', ia, time_axis, time_fit)
    #d = {0:'a', 45: 'c', 80: 'e'}
    d = {0:'b', 45: 'd', 80: 'f'}
    
    if ia == 0 or ia == 80 or ia == 45:
        fsp = np.fft.fft(bx)
        fsp_vac = np.fft.fft(vac_bx)
        fsp = np.absolute(fsp)
        fsp_vac = np.absolute(fsp_vac)
        taken=int(len(fsp)/2)-1
        freq = np.fft.fftfreq(len(fsp), dt)
        fig=plt.figure(figsize=(5,4))
        ax=fig.add_subplot(111)
        m = np.amax(fsp_vac[:taken])
        ax.plot(freq[:taken], fsp_vac[:taken]/m,'b-')
        ax.plot(freq[:taken], fsp[:taken]/m,'r-')
        ax.set_title('Spectra for %d$\degree$ for %d PW' % (90-ia, power))
        ax.set_yscale('log')
        ax.set_xlim([0,10])
        ax.set_ylim([1e-4, 1.1])
        ax.set_xticks([0,1,2,3,4,5,6,7,8,9,10])
        ax.set_xlabel('$\omega/\omega_0$')
        ax.set_ylabel('S[$\omega$], a.u.')
        ax.text(-2, 1, '(%s)' % d[ia])
        plt.tight_layout()
        fig.savefig(os.path.join(pics_path,'spectra_%d_%dpw.png' % (ia, power)), dpi=300)
        ax.clear()
        fig.clf()
        plt.close()
    sy = 0
    sx = np.cos(angCur)
    sz = np.sin(angCur)
    
    pvector = calcPointingVector(ex, ey, ez, bx, by, bz, sx, sy, sz)
    pvector_vac = calcPointingVector(vac_ex, vac_ey, vac_ez, vac_bx, vac_by, vac_bz, sx, sy, sz)
    intensityVac = cd4pi * np.sum(pvector_vac) #[fabs(cd4pi*signal[i]*signalB[i]) for i in range (0,numFiles)]
    intensityPlasma = cd4pi * np.sum(pvector) #[fabs(cd4pi*reflect[i]*reflectB[i]) for i in range (0,numFiles)]
    
    diagVac.append(intensityVac/1e14)
    diagPlasma.append(intensityPlasma/1e14)

    
'''for ia in range (minAng,maxAng,dAng):
#for ia in range (45,46):
    print(ia)
    angCur=ia/360.*2*pi+pi/2
    if (angCur>2*pi):
        angCur-=2*pi
    ang.append(angCur)
    dataVac = []
    dataPlasma=[]
    dataVacB = []
    dataPlasmaB=[]
    
    xd = [i*dt for i in range (0,timeSignal)]
    
    fileName=keyVac+str(ia)+'.txt'
    with open(fileName) as f:
        for line in f:
            dataVac.append([float(x) for x in line.split()])
    dataVacApp = [dataVac[0][i] for i in range (0,timeSignal)]
   
    fileName=keyVacB+str(ia)+'.txt'
    with open(fileName) as f:
        for line in f:
            dataVacB.append([float(x) for x in line.split()])
    dataVacBApp = [dataVacB[0][i] for i in range (0,timeSignal)]    
    
    fileName=keyPlasma+str(ia)+'.txt'
    with open(fileName) as f:
        for line in f:
            dataPlasma.append([float(x) for x in line.split()])
    dataPlasmaApp = [dataPlasma[0][i] for i in range (0,timeSignal)]
    
    fileName=keyPlasmaB+str(ia)+'.txt'
    with open(fileName) as f:
        for line in f:
            dataPlasmaB.append([float(x) for x in line.split()])
    dataPlasmaBApp = [dataPlasmaB[0][i] for i in range (0,timeSignal)]
    
    from scipy.optimize import curve_fit
    init_vals = [delayTime, delay, amp]
    best_vals, covar = curve_fit(semiinfinite, xd, dataVacApp, p0=init_vals)
    print('Best vals', best_vals)
    x=[i*0.05 for i in range (0,numFiles)]
    signal=[semiinfinite(x[i],best_vals[0],best_vals[1],best_vals[2]) for i in range (0,numFiles)]
    reflect=[dataPlasma[0][i]-signal[i] for i in range (0,numFiles)]
    #ax.plot(dataVacApp, color='b')
    #ax.plot(signal, color='g')
    #ax.plot(reflect, color='r')
    #plt.show()
    #plt.close()
    best_valsB, covarB = curve_fit(semiinfinite, xd, dataVacBApp, p0=init_vals)
    print('Best vals B', best_valsB)
    signalB=[semiinfinite(x[i],best_valsB[0],best_valsB[1],best_valsB[2]) for i in range (0,numFiles)]
    reflectB=[dataPlasmaB[0][i]-signalB[i] for i in range (0,numFiles)]
    
    intensityVac=[fabs(cd4pi*signal[i]*signalB[i]) for i in range (0,numFiles)]
    intensityPlasma=[fabs(cd4pi*reflect[i]*reflectB[i]) for i in range (0,numFiles)]
    
    diagVac.append(sum(intensityVac)/1e14)
    diagPlasma.append(sum(intensityPlasma)/1e14)
    
    #import matplotlib.pyplot as pltCur
    
    #pltCur.plot(x,signalB,'r-')
    #pltCur.plot(x,reflectB,'b-')
    #pltCur.plot(x,dataPlasmaB[0],'k:')'''

diagVac = np.array(diagVac)
diagVac /= np.amax(diagVac)

diagPlasma = np.array(diagPlasma)
diagPlasma /= np.amax(diagPlasma)
#print(diagVac)
#print(diagPlasma)

ang = np.array(ang) + np.pi/2

import matplotlib.pyplot as plt
fig=plt.figure(figsize=(5,4))
ax=fig.add_subplot(111,projection='polar')
ax.set_theta_offset(pi/2)
ax.set_rlabel_position(290)
ax.plot(ang, diagPlasma,'b-')
ax.plot(ang,diagVac,'r-')
#ax.plot((ang[-1],ang[0]),(diagVac[-1],diagVac[0]),'r-')
#ax.plot((ang[-1],ang[0]),(diagPlasma[-1],diagPlasma[0]),'b-')
ax.plot(ang_, sin2, 'g')
ax.set_title('(b) Radiation pattern for %d PW' % power)
plt.tight_layout()
fig.savefig(os.path.join(pics_path, 'diag_%dpw.png' % power), dpi=300)
ax.clear()
fig.clf()
plt.close()

#plt.subplot(120,polar=True)
#plt.plot(ang,diagPlasma,'r-')
#plt.plot(ang,diagVac,'b-')
#ax.plot(ang,diagVac , color='r', linewidth=1.)
#ax = fig.add_subplot(121, projection='polar')
#ax.plot(ang,diagPlasma , color='b', linewidth=1.)


#plt.plot(xd, dataApp, 'b.')
#dataFit=[sin2pulse(xd[i],*best_vals) for i in range (0,timeSignal)]
#plt.plot(xd, dataFit, 'r-')


#signal=[sin2pulse(x[i],best_vals[0],best_vals[1]) for i in range (0,741)]
#reflect=[data[0][i]-signal[i] for i in range (0,741)]

#plt.plot(x,signal,'r--')
#plt.plot(x,reflect, 'b--')
#plt.plot(x,data[0],'k-')
#print(fabs(max(signal,key=abs)),fabs(max(reflect,key=abs)))

#keyName='Ft_'

#for i in range (0,10,2):
#    fileName=keyName+str(i)+'.txt'
#    print(fileName)
# print('best_vals: {}'.format(best_vals))
#from numpy import exp, linspace, random

#def gaussian(x, amp, cen, wid):
#    return amp * exp(-(x-cen)**2 / wid)

#from scipy.optimize import curve_fit

#x = linspace(-10, 10, 101)
#y = gaussian(x, 2.33, 0.21, 1.51) + random.normal(0, 0.2, x.size)

#init_vals = [1, 0, 1]  # for [amp, cen, wid]
#best_vals, covar = curve_fit(gaussian, x, y, p0=init_vals)
#print('best_vals: {}'.format(best_vals))
#print(x)
#print(y)

