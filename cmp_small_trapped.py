#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib as mp
import sys
import math
import utils
import numpy as np

def find_gamma_av(pfield, dg):
    n = pfield.shape[0]
    s = np.sum(pfield)
    tmp = 0
    for i in range(n):
        tmp += pfield[i]*i*dg
    return tmp/s

def main():
    picspath = 'pics'
    path = './'
    plt.rc('text', usetex=True)
    config = utils.get_config(path + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = float(config['PeakPowerPW']) 
    T = 2 * math.pi/omega*1e15
    num = int(T/(x0*y0*1e15))
    print num, T
    nx1 = int(config['MatrixSize_X'])
    ny1 = int(config['MatrixSize_Y'])
    dx = float(config['Step_X'])
    dy = float(config['Step_Y'])
    #rmin = float(config['ElGammaR.SetBounds_0'])*1e4
    #rmax = float(config['ElGammaR.SetBounds_1'])*1e4
    #nr_ = int(config['ElGammaR.SetMatrixSize_0'])
    #ne_ = int(config['ElGammaR.SetMatrixSize_1'])
    #dr = (rmax - rmin)/nr_
    #Emin = float(config['ElGammaR.SetBounds_2'])
    #Emax = float(config['ElGammaR.SetBounds_3'])
    #dg = (Emax - Emin)/ne_
    ne_t = []
    np_t = []
    nes_t = []
    nps_t = []
    step = x0*y0*1e15/T
    dt = x0*y0*1e15
    n = int(1./step)
    wl = float(config['Wavelength'])
    v = math.pi * (0.5*wl)**2 * wl
    const_a_p = 7.81441e-9
    a0 = const_a_p * math.sqrt(ppw*1e22)
    print a0, 1.18997e+8*a0
    ncr = utils.ElectronMass*omega**2/(8*math.pi*utils.ElectronCharge**2)
    print ncr
    netr_t = utils.ts(path, utils.netpath, name='ne_trapped',
                         tstype='sum', verbose=True)
    npostr_t = utils.ts(path, utils.npostpath, name='npos_trapped',
                         tstype='sum', verbose=True)
    ez_t = utils.ts(path, utils.ezpath, name='ez',
                         tstype='center', shape = (nx1,ny1), verbose=True)
    
    print 'Ratio Electrons to positrons', np.amax(netr_t)/np.amax(npostr_t)
    axise = utils.create_axis(len(netr_t), 1)
    axisp = utils.create_axis(len(npostr_t), 1)
    g = np.zeros(len(npostr_t))
    g1 = np.zeros(len(npostr_t))
    g2 = np.zeros(len(npostr_t))
    g3 = np.zeros(len(npostr_t))
    ne_t = np.zeros(len(npostr_t))
    #rmin_ = 0.1*wl*1e4
    #rmax_ = 0.3*wl*1e4
    #imin = int(rmin_/dr)
    #imax = int(rmax_/dr)
    #for i in range(len(netr_t)-1):
    n1=0
    n2=len(netr_t)
    #for i in range(n1,n2):
        #pfield = utils.bo_file_load(path + utils.elgammarpath,i,nr_,ne_,verbose=1, transpose=1)
        #gr = np.sum(pfield[:,imin:imax], axis=1)
        #print pfield.shape, gr.shape
        #gamma = find_gamma_av(gr, dg)
        
        #n0 = netr_t[i]/v/ncr
        #ne_t[i] = netr_t[i]/v
        #gamma1 = a0*ez_t[i]/(3e11*math.sqrt(ppw/10.))
        #gamma2 = a0/2
        #print gamma, gamma1, gamma2
        #if netr_t[i-n] > 1e-8:
        #    g[i] = math.log(netr_t[i]/netr_t[i-n])
        #    g1[i] = (netr_t[i] - netr_t[i-n])/netr_t[i-n]
            #if gamma > 1:
            #g2[i] = math.sqrt(2*n0/gamma)*2*math.pi
    #for i in range(len(netr_t)-n):
    #    for k in range(n):
    #        g3[i] += g2[i+k]/n
    #print np.average(g[-100:]), np.average(g1[-100:])
    
    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(1,1,1)
#    ax1.set_yscale('log')
    ax1.plot(axise, netr_t, 'r', label = 'El $\lambda/2$')
    ax1.plot(axise, npostr_t, 'b', label = 'Pos $\lambda/2$')
    #ax1.plot(axisp, g, 'r', label = 'log')
    #ax1.plot(axisp, g1, 'b', label = 'diff')
    ax1.legend(loc='upper left', shadow=True)
#    ax1.set_ylim([1e8, 1e11])
    #plt.savefig(path + picspath + '/particles_%d.png' % (ppw))
    plt.grid()
    plt.show()
    plt.close()
     
#    bounds = [n1/20,n2/20]
#    fig = plt.figure(num=None)
#    ax1 = fig.add_subplot(2,1,1)
#    ax1.plot(axise, g, 'r', label = '$\Gamma$')
#    ax1.plot(axise, g1, 'g', label = '$Av.\Gamma$')
#    ax1.plot(axise, g2, 'b', label = '$\Gamma_c$')
#    ax1.plot(axise, g3, 'k', label = '$Av.\Gamma_c$')
#    ax1.set_xlim(bounds)
#    ax1.legend(loc='upper right', shadow=True)
#    ax2 = fig.add_subplot(2,1,2)
#    ax2.plot(axise, ez_t, 'r', label = '$E_z$')
#    ax2.set_xlim(bounds)
#    ax3 = ax2.twinx()
#    ax3.set_xlim(bounds)
#    ax3.plot(axise, ne_t, 'b', label = '$N_e$')
#    plt.savefig(path + picspath + '/pos_gamma_%d.png' % (ppw))
#    plt.close()
    
if __name__ == '__main__':
    main()
