#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import sys
import math
import utils

def main():
    num = len(sys.argv)
    path1  = './BasicOutput/' + utils.dphpath
    path2  = './BasicOutput/' + 'data/enPh'
    path3  = './BasicOutput/' + utils.nexpath
    delta = 1
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = utils.num_files(path1)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1] 
        nmin = int(sys.argv[1])
        nmax = utils.num_files(path1)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1] + ' to ' + sys.argv[2] + ' with delta ' + sys.argv[3]
        nmin = int(sys.argv[1])
        nmax = int(sys.argv[2])
        delta = int(sys.argv[3])
    else:
        print 'To much parameters'
    
    print 'Found ' + str(nmax) + ' files'
    config = utils.get_config("./ParsedInput.txt")
    xmin1 = float(config['diagPh.SetBounds_0'])*180./3.14159
    xmax1 = float(config['diagPh.SetBounds_1'])*180./3.14159
    nx1 = int(config['diagPh.SetMatrixSize_0'])
    xmin2 = float(config['enPh.SetBounds_0'])
    xmax2 = float(config['enPh.SetBounds_1'])
    nx2 = int(config['enPh.SetMatrixSize_0'])
    wl = float(config['Wavelength'])
    Ymin = float(config['Electron2Dx.SetBounds_0'])/wl
    Ymax = float(config['Electron2Dx.SetBounds_1'])/wl
    Zmin = float(config['Electron2Dx.SetBounds_2'])/wl
    Zmax = float(config['Electron2Dx.SetBounds_3'])/wl
    ny = int(config['Electron2Dx.SetMatrixSize_0'])
    nz = int(config['Electron2Dx.SetMatrixSize_1'])
    axis1 = utils.axis(xmin1,xmax1,nx1)
    axis2 = utils.axis(xmin2,xmax2,nx2)
    
    for i in range(nmin,nmax,delta):
        fig = plt.figure(figsize=(15,5))
        ax1 = fig.add_subplot(1,4,1)
        value1 = utils.bo_file_load(path1,i,nx1)
        ax1.plot(axis1,value1)
        print sum(value1)
        ax2 = fig.add_subplot(1,4,2)
        value2 = utils.bo_file_load(path2,i,nx2)
        ax2.plot(axis2,value2)
        en = 0
        for k in xrange(len(value2)):
            en += (k+0.5)*(axis2[1] - axis2[0])*value2[k]
        print sum(value2), en, en/sum(value2)
        if min(value2) != 0 or max(value2) != 0:
            ax2.set_yscale('log')
        ax3 = fig.add_subplot(1,4,3)
        value3 = utils.bo_file_load(path3,i,ny,nz)
        m = utils.max2d(value3)
        print m, sum(sum(value3))
        value3 += 1
        ax3.imshow(value3, extent = [Ymin, Ymax, Zmin, Zmax], norm = clr.LogNorm(), cmap = 'hot', interpolation='none')
        ax4 = fig.add_subplot(1,4,4)
        value4 = utils.fromfile('dYZ.txt',ny,nz)
        m = utils.max2d(value4)
        print m, sum(sum(value4))
        value4 += 1
        ax4.imshow(value4, extent = [Ymin, Ymax, Zmin, Zmax], norm = clr.LogNorm(), cmap = 'hot', interpolation='none')
#        ax3.set_xlim([-1.05, -0.95])
#        ax3.set_ylim([-0., 0.1])
        plt.grid()
        picname = './pics/enph_%d'%i
        plt.savefig(picname)


if __name__ == '__main__':
    main()
