#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import utils
import sys
from pylab import *

def read_file(file, de):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
    nph = 0
    
    for i in range(len(array)):
        nph  += array[i]
        
    for i in range(len(array)):
        array[i] *= (i+0.5)

    return array, nph

def read_one_spectrum(path1, nmin):
    config = utils.get_config(path1 + "/ParsedInput.txt")
    x0 = int(config['BOIterationPass'])
    y0 = float(config['TimeStep']) 
    omega = float(config['Omega'])
    ppw = int(config['PeakPowerPW'])
    ev = float(config['eV']) 
    T = 2 * math.pi/omega*1e15
    num = 2*int(T/(x0*y0*1e15))
    print num, T
    nph_t = []
    nph1_t = []
    ne_t = []
    np_t = []
    step = x0*y0*1e15/T 
    
    dt = y0*float(config['QEDstatistics.OutputIterStep'])*1e15

    path = '/statdata/ph/EnSp/'
    nepath = '/data/NeTrap/'
    nppath = '/data/NposTrap/'
    emin = float(config['QEDstatistics.Emin']) 
    emax = float(config['QEDstatistics.Emax']) 
    ne = int(config['QEDstatistics.OutputN_E'])
    de = (emax - emin)/ne
   
    t = 0
    n = check_length(path1 + path + '%.4f.txt'%nmin)
    axis = create_axis(n, de/ev*1e-9)

    spectrum1 = [0]*n
    nn = 0
    full_en = 0

    for i in range(nmin,nmin+num):
        name = path1 + path + '%.4f.txt'%i
        print name
        sp1, nph = read_file(name, de)
        nn += nph
        for k in range(n):
            spectrum1[k] += sp1[k]
            full_en += spectrum1[k]*de
    
    
    for k in range(n):
        spectrum1[k] /= nn
       
    return spectrum1, axis, full_en/ev*1e-9

def check_length(file):
    f = open(file, 'r')
    array = [float(x) for x in f.readline().split()]
    f.close()
       
    return len(array)

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def main():
    picspath = 'pics'
    num = len(sys.argv) - 1
    n = num/2
    sp = []
    ax = []
    av = []
    for k in range(n):
        spectrum, axis, av_en = read_one_spectrum(sys.argv[2*k+1], int(sys.argv[2*k+2]))
        sp.append(spectrum)
        ax.append(axis)
        av.append(av_en)
        print av_en

if __name__ == '__main__':
    main()
