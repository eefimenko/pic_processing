#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import math
import utils
import sys
import random

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def filter(w, sp, thr):
    n = len(sp)
    res = [0+0j]*n
    for i in range(n):
        if w[i] < thr or w[i] > w[-1]-thr:
            res[i] = sp[i]
    return res

def main():
    l = 1.
    pi = 3.14159
    nmax = 16
    num_periods = 0.1
    window = 24
    factor = 3
    dx = l/nmax
    freq = 2*pi/l*num_periods
    dw = 2*pi/l/freq
    wthr = 2*pi/(4*factor*dx)/freq
    
    num_waves = 100
    freqw = [0]*num_waves
    phase = [0]*num_waves
    amp = [0]*num_waves
    for k in range(num_waves):
        freqw[k] = random.uniform(wthr*freq, 10*freq) 
        phase[k] = random.uniform(0, 2*pi)
        amp[k] = random.uniform(0, 0.05)
    s = np.array([0.]*nmax)
    a = create_axis(nmax,dx)
    w = create_axis(nmax,dw)
    for i in range(nmax):
        s[i] = math.sin(freq*i*dx)
        for k in range(num_waves):
            s[i] += amp[k]*math.sin(freqw[k]*i*dx + phase[k])

    sp = np.fft.fft(s)
    sp1 = filter(w,sp,wthr)
    S = np.abs(sp)
    S1 = np.abs(sp1)
    s1 = np.fft.ifft(sp1)

    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(2,1,1)
    ax1.plot(a,s)
    ax1.plot(a,s1)
    ax2 = fig.add_subplot(2,1,2)
    ax2.plot(w,S)
    ax2.plot(w,S1)
    ax2.set_xlim([0, 10])
    plt.show()

if __name__ == '__main__':
    main()
