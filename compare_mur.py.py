#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import sys
import math
import utils

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    line = f.readline()
    tmp = [float(x)*mult for x in line.split()] 
    f.close()
    return tmp

def create_axis(n,step, x0):
    axis = []
    print n, step, x0
    for i in range(n):
        axis.append(x0+i*step)
    return axis

def main():
    epath = '/data/Btr1D'
    bpath = '/data/Etr1D'
    npath = '/data/Electron1Dn'
    picspath = 'pics'

    numdirs = int(sys.argv[1])
    dirs = []
    configs = []
    for i in range(numdirs):
        dirs.append(sys.argv[2+i])
        configs.append(utils.get_config(sys.argv[2+i] + "/ParsedInput.txt"))
    delta = 1
    n = numdirs + 1
    num = len(sys.argv) - n
    
    if num == 1:
        print 'Plotting all pics'
        nmin = 0	
        nmax = find_nmax(dirs, expath)
    elif num == 2:
        print 'Printing from pic ' + sys.argv[1 + n] 
        nmin = int(sys.argv[1 + n])
        nmax = find_nmax(dirs, expath)
    elif num == 3:
        print 'Printing pics from ' + sys.argv[1 + n] + ' to ' + sys.argv[2 + n]
        nmin = int(sys.argv[1 + n])
        nmax = int(sys.argv[2 + n])
    elif num == 4:
        print 'Printing pics from ' + sys.argv[1 + n] + ' to ' + sys.argv[2 + n] + ' with delta ' + sys.argv[3 + n]
        print sys.argv[1 + n]
        nmin = int(sys.argv[1 + n])
        nmax = int(sys.argv[2 + n])
        delta = int(sys.argv[3 + n])
    else:
        nmin = 0
        nmax = 0
        print 'Too much parameters'

    print nmin, nmax, delta
    
    xmin = []
    xmax = []
    ymin = []
    ymax = []
    zmin = []
    zmax = []
    mult = []
    nx = []
    ny = []
    nz = []
    axis = []
    for k in range(numdirs):
        xmax.append(float(configs[k]['X_Max'])*1e4) #mkm
        xmin.append(float(configs[k]['X_Min'])*1e4) #mkm
        ymax.append(float(configs[k]['Y_Max'])*1e4) #mkm
        ymin.append(float(configs[k]['Y_Min'])*1e4) #mkm
        zmax.append(float(configs[k]['Z_Max'])*1e4) #mkm
        zmin.append(float(configs[k]['Z_Min'])*1e4) #mkm
        nx.append(int(configs[k]['MatrixSize_X']))
        ny.append(int(configs[k]['MatrixSize_Y']))
        nz.append(int(configs[k]['MatrixSize_Z']))
        dx = float(configs[k]['Step_X'])*1e4
        dy = float(configs[k]['Step_Y'])*1e4
        dz = float(configs[k]['Step_Z'])*1e4
        mult.append(1/(2.*dx*dy*dz*1e-12))
        step = float(configs[k]['TimeStep'])*1e15*float(configs[k]['BOIterationPass'])
        omega = float(configs[k]['Omega'])
        T = 2 * utils.pi/omega
        nt = int(T*1e15/step)
        axis_ = create_axis(nx[k], dx, xmin[k])
        axis.append(axis)
        print nt
    delay = [0,48,48,80]
    for i in range(nmin,nmax,delta):
        print "\rSaving sc%06d.png, %.0f %% done" % (i,float(i-nmin)/(nmax-nmin)*100),
        sys.stdout.flush()

        fig = plt.figure(num=None, figsize=(20., 20.*spx/spy), dpi=256)
        mp.rcParams.update({'font.size': 8})
        ex = fig.add_subplot(3,1,1)
        bx = fig.add_subplot(3,1,2)
        ne = fig.add_subplot(3,1,3)
        for k in range(numdirs):
            fe = read_field(dirs[k] + epath + '%06d.txt' % i+delay[k])
            ex.plot(axis[k], fe, label = dirs[k])
            fb = read_field(dirs[k] + bpath + '%06d.txt' % i+delay[k])
            bx.plot(axis[k], fb, label = dirs[k])
            fn = read_field(dirs[k] + npath + '%06d.txt' % i+delay[k])
            nx.plot(axis[k], fn, label = dirs[k])
        picname = picspath + '/' + "sc%06d.png" % (i,)
        
        plt.savefig(picname)
        plt.close()

    
if __name__ == '__main__':
    main()
