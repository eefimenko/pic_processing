#!/usr/bin/python
import matplotlib.pyplot as plt
import sys
import math
import utils

def read_density(filename):
    f = open(filename, 'r')
    tmp = f.readline().split()
    return float(tmp[0]), float(tmp[1]), float(tmp[2]), float(tmp[3]), float(tmp[4]), float(tmp[5]) 

def main():
    n = len(sys.argv) - 1
    m1 = []
    m2 = []
    m3 = []
    m4 = []
    m5 = []
    t1 = []
    t2 = []
    t3 = []
    t4 = []
    t5 = []
    max_dens = []
    power = []
    pow_ = 1.2
    const_a_p = 7.81441e-9
    config = utils.get_config(sys.argv[1] + "/ParsedInput.txt")
    omega = float(config['Omega'])
    electronMass = 9.10938e-28
    electronCharge = 4.8032e-10
    ncr = electronMass * omega * omega/(8. * math.pi * electronCharge * electronCharge)
    name = 'max_dens.txt'
    out = open(name, 'w')
    for i in range(n):
        path = sys.argv[i+1]
        config = utils.get_config(path + "/ParsedInput.txt")
        pwr = float(config['PeakPowerPW'])
        m1_, m2_, m3_, m4_, m5_, md_ = read_density(path + '/' + name)
        m1.append(m1_)
        m2.append(m2_)
        m3.append(m3_)
        m4.append(m4_)
        m5.append(m5_)
        power.append(pwr)
        max_dens.append(md_)
        out.write('%lf %le %le %le %le %le %le\n' % (pwr, m1_, m2_, m3_, m4_, m5_, md_))
    out.close()
    out = open('ncr.txt','w')
    for i in range(len(power)):
        t1.append(math.pow(power[i], pow_)*m1[0]/math.pow(power[0], pow_))
        t2.append(math.pow(power[i], pow_)*m2[0]/math.pow(power[0], pow_))
        t3.append(math.pow(power[i], pow_)*m3[0]/math.pow(power[0], pow_))
        t4.append(math.pow(power[i], pow_)*m4[0]/math.pow(power[0], pow_))
        t5.append(math.pow(power[i], pow_)*m5[0]/math.pow(power[0], pow_))
        a0 = const_a_p * math.sqrt(power[i]*1e22)
        tmp = max_dens[i]/(ncr*a0)
        out.write('%lf %le %lf %lf\n' % (power[i], max_dens[i], a0, tmp))
        max_dens[i] = tmp
        print max_dens[i]
    out.close()
#    fig, ax1 = plt.subplots()
    fig = plt.figure(num=None)
    ax1 = fig.add_subplot(2,1,1)
    
   
    p, = ax1.plot(power, m1, color = 'b', label = '0.9')
    p, = ax1.plot(power, m2, color = 'r',label = '0.8')
    p, = ax1.plot(power, m3, color = 'g',label = '0.7')
    p, = ax1.plot(power, m4, color = 'k',label = '0.6')
    p, = ax1.plot(power, m5, color = 'c',label = '0.5')
    p, = ax1.plot(power, t1, ':', color = 'b')
    p, = ax1.plot(power, t2, ':', color = 'r')
    p, = ax1.plot(power, t3, ':', color = 'g')
    p, = ax1.plot(power, t4, ':', color = 'k')
    p, = ax1.plot(power, t5, ':', color = 'c')
    ax1.set_yscale('log')
    ax1.set_xscale('log')
    ax1.set_xlim([power[0], power[-1]])
    ax1.set_xlabel('Power, PW')
    ax1.set_ylabel('Density, $cm^{-3}$')
    ax1.legend(loc='lower right', shadow=True)
    ax2 = fig.add_subplot(2,1,2)
    p, = ax2.plot(power, max_dens)
    plt.show()

if __name__ == '__main__':
    main()
