#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import utils
import sys

def read_field(file,nx,ny,mult = 1.):
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]

    field = []
    for j in range(ny):
        row = []
        for i in range(nx):
            index = i + j*nx
            row.append(array[index] + 1.e-10)
        field.append(row)
    return field

def find_max(file, mult = 1.):
    max = 0
    f = open(file, 'r')
    tmp = [[float(x)*mult for x in line.split()] for line in f]
    f.close()
    array = tmp[0]
    for p in array:
        if p > max:
            max = p
    return max

def create_subplot(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, xl, yl, color, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
    name = path + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny, mult)
    maxe = max([max(row) for row in field])
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap = color, norm=clr.LogNorm())
    ax.text(-5, 4.5, 'min=%.1le max=%.1le' % (v_min, maxe), style='italic')
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)
    plt.colorbar(surf,  orientation  = 'vertical', ticks = [v_min, v_max])
    return surf

def create_subplot_f(fig,i,path,nx,ny,spx,spy,spi, Xmin, Xmax, Ymin, Ymax, v_min, v_max, mult=1):
    ax = fig.add_subplot(spx,spy,spi)
    name = path + '/' + "%06d.txt" % (i,)
    field = read_field(name,nx,ny, mult)
    maxe = max([max(row) for row in field])
    surf = ax.imshow(field, extent=[Xmin, Xmax, Ymin, Ymax], cmap='Reds', vmin = v_min, vmax = v_max, norm=clr.LogNorm())
    ax.text(-3, 4.5, '%.1le' % maxe, style='italic')
    plt.colorbar(surf, orientation  = 'vertical', ticks = [v_min, v_max])
    return surf

def norm(array, a=1.):
    maximum = 0
    summ = 0
    for j in range(len(array)):
        for i in range(len(array[0])):
            summ += array[j][i]
    
    for j,line in enumerate(array):
        for i,e in enumerate(line):
            array[j][i] = e/summ

    maximum = max([max(line) for line in array])
    for j,line in enumerate(array):
        for i,e in enumerate(line):
            array[j][i] = e/maximum*a
    return array

def create_axis(n,step):
    axis = []
    for i in range(n):
        axis.append(i*step)
    return axis

def sum_array(array):
    res = [0] * len(array[0])
    for i in range(len(array[0])):
        for j in range(len(array)):
            res[i] += array[j][i]
    return res

def sum_arrayj(array):
    res = [0] * len(array)
    for i in range(len(array[0])):
        for j in range(len(array)):
            res[j] += array[j][i]*i
    return res

def norm1(array, a=1.):
    maximum = max(array)
    for i,e in enumerate(array):
        array[i] = e/maximum*a
    return array

def main():
    dir1 = sys.argv[1]
    iter1 = int(sys.argv[2])
    dir2 = sys.argv[3]
    iter2 = int(sys.argv[4])
    expath = '/data/diagPh/'
    picspath = 'pics'
    config = utils.get_config(dir1 + "/ParsedInput.txt")
    ev = float(config['eV'])
    Xmax = 180
    Xmin = 0
    Ymax = 0.02/ev*1e-9
    Ymin = 0
    nx = 1000
    ny = 180
    stepx = (Xmax - Xmin)/nx
    stepy = (Ymax - Ymin)/ny 
    Emax = 0.02/ev*1e-9
    de = Emax/nx
    axis = create_axis(nx,de)
    axis1 = create_axis(ny,1.)
    name1 = dir1 + expath + "%06d.txt" % (iter1,)
    name2 = dir2 + expath + "%06d.txt" % (iter2,)
    
    field1 = read_field(name1,nx,ny)
    field2 = read_field(name2,nx,ny)
    field1 = norm(field1,1)
    field2 = norm(field2,1)
    
    sum1 = sum_array(field1)
    sum2 = sum_array(field2)
    
    sumj1 = norm1(sum_arrayj(field1))
    sumj2 = norm1(sum_arrayj(field2))
    fig = plt.figure(num=None)
    ax = fig.add_subplot(2,1,1)
    ax.imshow(field1)
    ax = fig.add_subplot(2,1,2)
    ax.imshow(field2)
    plt.show()
    fig = plt.figure(num=None)
    mp.rcParams.update({'font.size': 8})
    ax = fig.add_subplot(2,2,1)
    plt.text(0.1,0.6,'Photon spectra for theta = 0')
    p1, = ax.plot(axis, field1[0], 'r', label = 'QP')
    p2, = ax.plot(axis, field2[0], 'g', label = 'QP x8')
    ax.set_xlabel('Energy, GeV')
    ax.set_ylabel('Number of photons')
    ax.set_yscale('log') 
    ax.legend(loc='upper right', shadow=True)
    plt.ylim([2e-4, 1])
    plt.xlim([0, 9])
    ax = fig.add_subplot(2,2,2)
    plt.text(0.1,0.6,'Photon spectra for theta = 90')
    p1, = ax.plot(axis, field1[ny/2], 'r', label = 'QP')
    p2, = ax.plot(axis, field2[ny/2], 'g', label = 'QP x8')
    ax.set_xlabel('Energy, GeV')
    ax.set_ylabel('Number of photons')
    ax.set_yscale('log') 
    ax.legend(loc='upper right', shadow=True)
    plt.ylim([2e-4, 1])
    plt.xlim([0, 3])
    ax = fig.add_subplot(2,2,3)
    plt.text(0.1,400,'Photon spectra for all theta')
    p1, = ax.plot(axis, sum1, 'r', label = 'QP')
    p2, = ax.plot(axis, sum2, 'g', label = 'QP x8')
    ax.set_xlabel('Energy, GeV')
    ax.set_ylabel('Number of photons')
    ax.set_yscale('log') 
    ax.legend(loc='upper right', shadow=True)
    plt.ylim([2e-4, 1e3])
    plt.xlim([0, 9])
    ax = fig.add_subplot(2,2,4)
    plt.text(0.3,0.9,'Radiation pattern')
    p1, = ax.plot(axis1, sumj1, 'r', label = 'QP')
    p2, = ax.plot(axis1, sumj2, 'g', label = 'QP x8')
    ax.set_xlabel('Angle')
    ax.set_ylabel('Photon energy')
#    ax.set_yscale('log') 
    ax.legend(loc='upper right', shadow=True)
#    plt.ylim([2e-4, 1e3])
    plt.xlim([0, 90])
    picname = 'diagph_compare.png'
    plt.savefig(picname)
#    plt.show()
if __name__ == '__main__':
    main()
